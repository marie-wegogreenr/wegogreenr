export const colors = {
  black: '#363636',
  white: '#FFFFFF',
  disable: '#9D9D9D',
  primary: '#445136',
  secondary: '#C56246',
  background: 'rgba(239,231,219,0.3)',
  yellow: '#E4BE74',
  darkBlue: '#325565',
  crema: '#D4C8B6',
  gray: '#E9E9E9',
  graySecondary: '#e8e8e8'
}

export const sizes = {
  'text-xs': '0.75rem',
  'text-sm': '0.85rem',
  'text-base': '1rem',
  'text-lg': '1.125rem',
  'text-xl': '1.25rem',
  'text-2xl': '1.5rem',
  'text-3xl': '1.875rem',
  'text-4xl': '2.25rem',
  'text-5xl': '3rem',
  'text-6xl': '3.75rem',
  'text-7xl': '4.5rem',
  'text-8xl': '6rem',
  'text-9xl': '8rem'
}

export const fonts = {
  fraunces: '"Fraunces", serif',
  montserrat: "'Montserrat', sans-serif;"
}

export const screens = {
  sm: '640px',
  md: '768px',
  lg: '1024px',
  xl: '1280px',
  '2xl': '1536px'
}

export const shadows = {
  sm: '0 1px 2px 0 rgba(0, 0, 0, 0.05)',
  DEFAULT: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
  md: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
  lg: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
  xl: '0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)',
  '2xl': '0 25px 50px -12px rgba(0, 0, 0, 0.25)',
  '3xl': '0 35px 60px -15px rgba(0, 0, 0, 0.3)',
  inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
  none: 'none',
  horizontal: 'rgb(0, 0, 0) 0px 0px 1024px, rgba(0, 0, 0, .3) 0px 0px 20px'
}

export const transitions = {
  DEFAULT: 'ease 300ms',
  fast: 'ease 50ms'
}
