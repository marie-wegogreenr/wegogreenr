import { dateFromDb } from './timeHelper'

export function calcelReservationDays(maxDays, initDate) {
  const cancelDays = 86400000 * (maxDays - 1)
  const today = new Date()

  if (dateFromDb(initDate).getTime() - cancelDays >= today.getTime()) {
    return true
  } else return false
}
