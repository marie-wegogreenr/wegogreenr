export const handleDiscount = ({
  totalDays,
  total_price,
  month_discount,
  seven_days_discount
}) => {
  let totalPrice = total_price
  let discountMessage = false
  let discountAmmount
  if (totalDays >= 28 && month_discount > 0) {
    discountAmmount = (total_price * (month_discount / 100)).toFixed(2)
    totalPrice -= (total_price * month_discount) / 100
    discountMessage = 'Réduction +28 jours'
  } else if (totalDays >= 7 && seven_days_discount > 0) {
    discountAmmount = (total_price * (seven_days_discount / 100)).toFixed(2)
    totalPrice -= discountAmmount
    discountMessage = 'Réduction +7 jours'
  } else {
    return [totalPrice, discountMessage, discountAmmount]
  }
}
