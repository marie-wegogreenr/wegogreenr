import { toast } from 'react-toastify'

export function notify(message, type) {
  switch (type) {
    case 'success':
      return toast.success(message, {
        position: toast.POSITION.BOTTOM_LEFT
      })
    case 'error':
      return toast.error(message, {
        position: toast.POSITION.BOTTOM_LEFT
      })
    case 'warning':
      return toast.warn(message, {
        position: toast.POSITION.BOTTOM_LEFT
      })
    case 'warn':
      return toast.warn(message, {
        position: toast.POSITION.BOTTOM_LEFT
      })
    case 'info':
      return toast.info(message, {
        position: toast.POSITION.BOTTOM_LEFT
      })
    default:
      return toast(message, {
        position: toast.POSITION.BOTTOM_LEFT
      })
  }
}
