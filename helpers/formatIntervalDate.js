/* moment js*/
import moment from 'moment'

export const intervalDateFormat = (
  _initial_date,
  _final_date,
  firstFormat = 'DD MMM',
  secondFormat = 'DD MMM'
) => {
  if (!_initial_date && !_final_date) return 'Jours flexibles'
  var start = _initial_date
    ? moment(_initial_date).format(firstFormat)
    : 'souple'
  var final = _final_date ? moment(_final_date).format(secondFormat) : 'souple'
  return `${start} - ${final}`
}
