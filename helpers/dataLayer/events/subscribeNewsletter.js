const subscribeObject = {
  event: 'subscribe_newsletter'
}

/**
 *
 * @function : This triggers sign_up event for Google Tag Manager
 */
export const subscribeNewsletter = () => {
  if (typeof window !== undefined) {
    const currentUrl = window.location.href
    /*  Validate for local environment */
    if (currentUrl.includes('local')) {
      return
    }
    /*  Validate properties for pageview object */
    window.dataLayer.push({ ...subscribeObject })
  }
}
