const checkoutObject = {
  event: null,
  ecommerce: {
    items: []
  }
}

/**
 *
 * @function : Adjust the object with the required parameters for GTM
 * @param {Object} data : It takes the data  which come from /checkout or /checkout/confirmation-de-payement page
 */
export const assignObjectValues = (data) => {
  checkoutObject.event = data.event
  checkoutObject.ecommerce = {
    transaction_id: data.reservation_id,
    value: data.total_price,
    currency: 'EUR'
  }

  const itemsObject = {
    item_name: data.room?.public_name,
    price: data.room?.basic_price,
    accommodation_type: data.establishment?.type.name,
    accommodation_city: data.establishment?.city_name,
    accommodation_region: data.establishment?.region_name
  }
  if (data.room?.id) {
    itemsObject.item_id = (data.room?.id).toString()
  }

  checkoutObject.ecommerce.items = []
  checkoutObject.ecommerce.items.push(itemsObject)
}

/**
 *
 * @function : It forms the object to trigger the purchase and begin_checkout events for Google Tag Manager
 * @param {Object} data : It takes the data  which come from /checkout or /checkout/confirmation-de-payement page
 */
export const checkoutEvent = (data) => {
  if (typeof window !== undefined) {
    const currentUrl = window.location.href
    /*  Validate for local environment */
    if (currentUrl.includes('local')) {
      return
    }

    assignObjectValues(data)

    /*  Validate properties for pageview object */
    window.dataLayer.push({ ecommerce: null })
    window.dataLayer.push({ ...checkoutObject })
  }
}
