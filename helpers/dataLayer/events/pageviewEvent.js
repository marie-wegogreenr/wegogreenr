const pageViewObject = {
  event: 'page_view',
  environment: {
    name: null
  },
  page: {
    title: null,
    category: null
  },
  user: {
    login_status: null
  }
}

/**
 *
 * @param {string} url : The current url
 * @returns {string} environment : The environment that is being visited
 */
const defineEnvironment = (url) => {
  let environment
  const environments = [
    { name: 'production', content: 'wegogreenr.com' },
    { name: 'staging', content: 'staging' },
    { name: 'hotfix', content: 'hotfix' },
    { name: 'develop', content: 'develop' },
    { name: 'local', content: 'local' }
  ]

  environments.map((item) => {
    if (url.includes(item.content)) {
      environment = item.name
    }
  })

  return environment
}

/**
 *
 * @param {string} url : The current url
 * @returns {string} splittedCategory : Page category
 */
const definePageCategory = (url) => {
  let splittedCategory
  const splittedUrl = url.split('/')

  if (!splittedUrl[3]) {
    return 'homepage'
  } else {
    if (splittedUrl[3].includes('-')) {
      splittedCategory = splittedUrl[3].split('-').join(' ')
    } else if (splittedUrl[3].includes('_')) {
      splittedCategory = splittedUrl[3].split('_').join(' ')
    } else {
      splittedCategory = splittedUrl[3]
    }

    if (splittedCategory === 'host') {
      splittedCategory = 'host dashboard'
    }

    return splittedCategory
  }
}

/**
 * NOT USED FOR NOW => It could be use in future evolutions
 * @param {string} url : The current url
 * @returns {string} splittedSubcategory : Page subcategory
 */
const definePageSubcategory = (url) => {
  let splittedSubcategory
  const splittedUrl = url.split('/')

  if (splittedUrl[4]) {
    if (splittedUrl[4].includes('-')) {
      splittedSubcategory = splittedUrl[4].split('-').join(' ')
    } else {
      splittedSubcategory = splittedUrl
    }
  }

  return splittedSubcategory
}

/**
 *
 * @function : It stablishes if user is logged in or logged out
 * @param {Object} user : It takes user data
 * @returns {boolean} logged
 */
const defineUser = (user) => {
  let logged
  if (user) {
    if (Object.values(user).length) {
      if (user.error) {
        logged = 'logged out'
      } else {
        logged = 'logged in'
      }
    } else {
      logged = 'logged out'
    }
  }
  return logged
}

/**
 *
 * @function : It forms the object to trigger the page_view event for Google Tag Manager
 * @param {Object} user : It takes the user data
 */
export const pageViewEvent = (user) => {
  if (typeof window !== undefined) {
    const currentUrl = window.location.href

    /*  Validate for local environment */
    if (currentUrl.includes('local')) {
      return
    }
    /*  Validate properties for pageview object */
    pageViewObject.environment.name = defineEnvironment(currentUrl)
    pageViewObject.page.category = definePageCategory(currentUrl)
    pageViewObject.page.title = definePageCategory(currentUrl)
    pageViewObject.user.login_status = defineUser(user)

    if (pageViewObject) {
      if (pageViewObject.page) {
        if (
          pageViewObject.page.category === 'hebergements' ||
          pageViewObject.page.category === 'establishment'
        ) {
          pageViewObject.page.category = `single ${pageViewObject.page.category}`
        }
      }
    }

    window.dataLayer.push({ ...pageViewObject })
  }
}
