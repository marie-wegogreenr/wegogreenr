/**
 * @function : This triggers user id data for google tag manager
 */
export const loggedUserInfo = (id) => {
  if (typeof window !== undefined) {
    const currentUrl = window.location.href
    /*  Validate for local environment */
    if (currentUrl.includes('local')) {
      return
    }
    /*  Validate properties for pageview object */
    window.dataLayer.push({ user_id: id })
  }
}
