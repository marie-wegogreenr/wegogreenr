const beginBookingObject = {
  event: 'begin_booking'
}

/**
 *
 * @function : This triggers begin_booking event for google tag manager
 */
export const beginBookingEvent = () => {
  if (typeof window !== undefined) {
    const currentUrl = window.location.href
    /*  Validate for local environment */
    if (currentUrl.includes('local')) {
      return
    }
    /*  Validate properties for pageview object */
    window.dataLayer.push({ ...beginBookingObject })
  }
}
