import types from 'constants/establishmentTypes'

/**
 *
 * @function : It stablishes a type of stablishment or room
 * @param {number} value : It takes the room or stablishment origin
 * @returns {string} _item_type : Assign the type of stablishment or room as null, owned, partner or widget
 */
const stablishItemType = (value) => {
  let _item_type = null
  const values = [
    { value: 1, type: 'owned' },
    { value: 2, type: 'partner' },
    { value: 3, type: 'widget' },
    { value: 4, type: 'widget' }
  ]

  values.map((item) => {
    if (value) {
      if (item.value === value) _item_type = item.type
    }
  })

  return _item_type
}

const selectItemObject = {
  event: 'select_item',
  ecommerce: {
    items: []
  }
}

/**
 *
 * @function : Adjust Google Tag Manager Object
 * @param {Object} data : It takes the data from the specific room or stablishment
 */
const assignObjectValues = (data) => {
  const object = {
    item_name: data.publicName,
    item_id: data.id.toString(),
    item_type: stablishItemType(data.origin),
    price: data.basicPrice,
    accommodation_type: data.typeName,
    accommodation_city: data.cityName,
    accommodation_region: data.regionName
  }

  selectItemObject.ecommerce.items = []
  selectItemObject.ecommerce.items.push(object)
}

/**
 *
 * @function : Adjust Google Tag Manager Object
 * @param {Object} data : It takes the data from the specific room or stablishment
 */
const assignObjectListValues = (data) => {
  const typeName = data.types.find((t) => t.id === data.type_id)?.name || ''

  const object = {
    item_name: data.public_name,
    item_id: data.id ? data.id?.toString() : undefined,
    item_type: stablishItemType(data.origin),
    price: data.basic_price,
    accommodation_type: typeName,
    accommodation_city: data.establishment.city_name,
    accommodation_region: data.establishment.region_name
  }
  selectItemObject.ecommerce.items = []
  selectItemObject.ecommerce.items.push(object)
}

/**
 *
 * @function : Adjust Google Tag Manager Object
 * @param {Object} data : It takes the data from the specific room or stablishment
 */
const assignObjectEstablishmentValues = (data) => {
  const object = {
    item_name: data.name,
    item_id: data.id.toString(),
    item_type: stablishItemType(data.origin),
    price: data.price,
    accommodation_type: types[data.type_id - 1],
    accommodation_city: data.city_name ? data.city_name : data.address,
    accommodation_region: data.region_name
  }
  selectItemObject.ecommerce.items = []
  selectItemObject.ecommerce.items.push(object)
}

/**
 *
 * @function : It forms the object to trigger the page_view event for Google Tag Manager
 * @param {Object} data : It takes the data from the specific room or stablishment
 */
export const selectItemEvent = (data) => {
  if (typeof window !== undefined) {
    const currentUrl = window.location.href
    /*  Validate for local environment */
    if (currentUrl.includes('local')) {
      return
    }
    if (data.from === 'establishment-card') {
      assignObjectEstablishmentValues(data)
    } else if (data.from === 'card-list') {
      assignObjectListValues(data)
    } else {
      assignObjectValues(data)
    }

    window.dataLayer.push({ ecommerce: null })
    window.dataLayer.push({ ...selectItemObject })
  }
}
