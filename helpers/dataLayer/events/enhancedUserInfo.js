/**
 * @function : Adjust the object with the required parameters for GTM
 * @param {Object} data : It takes the data  which come from /checkout or /checkout/confirmation-de-payement page
 */
export const assignObjectValues = (data) => {
  const userInfo = {
    enhanced_conversion_data: {
      email: data.email,
      phone_number: data.phone
    }
  }

  return userInfo
}

/**
 *
 * @function : It forms the object to trigger after the purchase event, user info for Google Tag Manager
 * @param {Object} data : It takes the data from /checkout/confirmation-de-payement page
 */
export const enhancedUserInfo = (data) => {
  if (typeof window !== undefined) {
    const currentUrl = window.location.href
    /*  Validate for local environment */
    if (currentUrl.includes('local')) {
      return
    }

    const userData = assignObjectValues(data)

    /*  Finally send object for Google Tag Manager */
    window.dataLayer.push({ ...userData })
  }
}
