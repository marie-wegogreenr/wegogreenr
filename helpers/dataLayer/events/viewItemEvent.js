import types from 'constants/establishmentTypes'

const viewItemObject = {
  event: 'view_item',
  ecommerce: {
    items: []
  }
}

/**
 *
 * @function : Adjust Google Tag Manager Object
 * @param {Object} data : It takes the data from the specific room or stablishment
 */
export const assignObjectValues = (data) => {
  const object = {
    item_name: data.public_name,
    item_id: data.id.toString(),
    price: data.basic_price,
    accommodation_type: data.establishment.type.name,
    accommodation_city: data.establishment.city_name,
    accommodation_region: data.establishment.region_name
  }

  viewItemObject.ecommerce.items = []
  viewItemObject.ecommerce.items.push(object)
}

/**
 *
 * @function : Adjust Google Tag Manager Object
 * @param {Object} data : It takes the data from the specific room or stablishment
 */
export const assignObjectValuesEstablishment = (data) => {
  const object = {
    item_name: data.name,
    item_id: data.id.toString(),
    price: data.ar_widget.price,
    accommodation_type: types[data.type_id - 1],
    accommodation_city: data.city_name,
    accommodation_region: data.region_name
  }

  viewItemObject.ecommerce.items = []
  viewItemObject.ecommerce.items.push(object)
}

/**
 *
 * @function : It forms the object to trigger the view_item event for Google Tag Manager
 * @param {Object} data : It takes the data from the specific room or stablishment
 */
export const viewItemEvent = (data) => {
  if (typeof window !== undefined) {
    const currentUrl = window.location.href
    const splittedUrl = currentUrl.split('/')
    /*  Validate for local environment */
    if (currentUrl.includes('local')) {
      return
    }

    /*  Validate room and establishment types  */
    if (splittedUrl[3] === 'hebergements') {
      assignObjectValues(data)
    } else {
      assignObjectValuesEstablishment(data)
    }
    window.dataLayer.push({ ecommerce: null })
    window.dataLayer.push({ ...viewItemObject })
  }
}
