const authViewObject = {
  event: 'authentication_view'
}

/**
 *
 * @function : This triggers authentication_view event for Google Tag Manager
 */
export const authViewEvent = () => {
  if (typeof window !== undefined) {
    const currentUrl = window.location.href
    /*  Validate for local environment */
    if (currentUrl.includes('local')) {
      return
    }
    /*  Validate properties for pageview object */
    window.dataLayer.push({ ...authViewObject })
  }
}
