const loginObject = {
  event: 'login'
}

/**
 *
 * @function : This triggers login event for Google Tag Manager
 */
export const loginEvent = () => {
  if (typeof window !== undefined) {
    const currentUrl = window.location.href
    /*  Validate for local environment */
    if (currentUrl.includes('local')) {
      return
    }
    /*  Validate properties for pageview object */
    window.dataLayer.push({ ...loginObject })
  }
}
