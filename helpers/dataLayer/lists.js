import { authViewEvent } from './events/authViewEvent'
import { beginBookingEvent } from './events/beginBooking'
import { checkoutEvent } from './events/checkoutEvent'
import { enhancedUserInfo } from './events/enhancedUserInfo'
import { loggedUserInfo } from './events/loggedUserInfo'
import { loginEvent } from './events/loginEvent'
import { pageViewEvent } from './events/pageviewEvent'
import { selectItemEvent } from './events/selectItemEvent'
import { signUpEvent } from './events/signUpEvent'
import { subscribeNewsletter } from './events/subscribeNewsletter'
import { viewItemEvent } from './events/viewItemEvent'

/**
 * @typedef {Object} eventListObject : Object for page_view and authentication_view events for Google Tag Manager
 * @property {Object} eventListObject.pageView : Object for page_view event
 * @property {Object} eventListObject.authView : Object for authentication_view event
 * @property {Array.<string>} eventListObject.pageView.list : List of possible pages for page_view event
 * @property {Array.<string>} eventListObject.authView.list : List of possible pages for authentication_view event
 * @property {function} eventListObject.pageView.callback : Callback function to execute for page_view event
 * @property {function} eventListObject.authView.callback : Callback function to execute for authentication_view event
 */
export const eventListObject = {
  pageView: {
    list: [
      'conditions-generales',
      'contacter',
      'demarche',
      'donnees-personnelles',
      'search',
      'carte-cadeau',
      'press',
      'regions',
      'hebergements',
      'establishment'
    ],
    callback: pageViewEvent
  },
  authView: {
    list: ['login'],
    callback: authViewEvent
  }
}

/**
 * @typedef {Object} eventListObjectTriggered : It has a list of different
 * @property {Array.<string>} eventListObject.[property].list : List of possible pages for the event
 * @property {function} eventListObject.[property].callback : Callback function to execute for the event
 */
export const eventListObjectTriggered = {
  beginBooking: {
    list: ['hebergements'],
    callback: beginBookingEvent
  },
  login: {
    list: ['login'],
    callback: loginEvent
  },
  loggedUser: {
    callback: loggedUserInfo
  },
  signUp: {
    list: ['register'],
    callback: signUpEvent
  },
  beginCheckout: {
    list: ['checkout'],
    callback: checkoutEvent
  },
  purchase: {
    list: ['confirmation-de-payement'],
    callback: checkoutEvent,
    userCallback: enhancedUserInfo
  },
  viewItem: {
    list: ['hebergements', 'establishment'],
    callback: viewItemEvent
  },
  selectItem: {
    list: ['search'],
    callback: selectItemEvent
  },
  subscribeNewsletter: {
    list: [''],
    callback: subscribeNewsletter
  }
}
