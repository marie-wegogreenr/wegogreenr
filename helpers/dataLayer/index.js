import { eventListObject } from './lists'

/**
 * @function : Evaluates possible events to trigger to Google Tag Manager
 * @param {Object} data : Object with specific event data to evaluate
 */
export const evaluateEvent = (data) => {
  if (typeof window !== undefined) {
    const splittedUrl = window.location.href.split('/')

    if (splittedUrl[3] === '') {
      eventListObject.pageView.callback(data)
    }

    Object.values(eventListObject).map((item) => {
      item.list.map((element) => {
        if (splittedUrl[3].includes(element)) {
          item.callback(data)
        }
      })
    })
  }
}
