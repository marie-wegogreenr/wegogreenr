import { DEFAULT_FRANCE, DEFAULT_REGION_ZOOM } from 'constants/index'
import { handleRegions } from 'constants/regions'

export const handleSearchProps = (path) => {
  let resp = {
    lat: DEFAULT_FRANCE.lat,
    lng: DEFAULT_FRANCE.lng,
    locationName: DEFAULT_FRANCE.locationName,
    zoom: DEFAULT_FRANCE.zoom
  }

  switch (path) {
    case 'chambre-hote':
      resp['types'] = ['15']
      break
    case 'hotel':
      resp['types'] = ['21']
      break
    case 'ecolodge':
      resp['types'] = ['18']
      break
    case 'insolite':
      resp['types'] = ['3']
      break
    case 'tiny-house':
      resp['types'] = ['30']
      break
    case 'cabane':
      resp['types'] = ['8']
      break
    case 'cabane-arbre':
      resp['types'] = ['9']
      break
    case 'chalet':
      resp['types'] = ['13']
      break
    case 'selection':
      resp['origin'] = '1'
      break
    case 'la-selection':
      resp['accept_coupon'] = '1'
      break
    case 'auvergne-rhone-alpes':
    case 'bourgogne-franche-comte':
    case 'bretagne':
    case 'centre-val-de-loire':
    case 'corse':
    case 'grand-est':
    case 'hauts-de-france':
    case 'ile-de-france':
    case 'provence-alpes-cote-azur':
    case 'normandie':
    case 'nouvelle-aquitaine':
    case 'occitanie':
      const region = handleRegions(path)

      resp = {
        ...resp,
        zoom: DEFAULT_REGION_ZOOM,
        locationName: region.lat && region.name ? region.name : 'Autre site',
        lat: region.lat,
        lng: region.lng
      }
      break

    default:
      resp
      break
  }
  return resp
}

export const searchPaths = {
  'chambre-hote': '15',
  hotel: '21',
  ecolodge: '18',
  insolite: '3',
  'tiny-house': '30',
  cabane: '8',
  'cabane-arbre': '9',
  chalet: '13'
}

export const locationTextResponse = (total, location) => {
  let quantityText = 0
  let roomsText = 'hébergements'
  let locationText = 'éco-responsables en France'

  if (total) {
    quantityText = total
  }

  if (quantityText === 1) {
    roomsText = 'hébergement'
  }

  if (
    !location ||
    location?.toLowerCase() === 'france' ||
    location?.toLowerCase() === 'autre site'
  ) {
    locationText = 'éco-responsables en France'
  } else if (location) {
    if (location.includes(',')) {
      const locationSplitted = location.split(',')
      locationText = `près de ${locationSplitted[0]}`
    } else {
      locationText = `près de ${location}`
    }
  }

  return `${quantityText} ${roomsText} ${locationText}`
}
