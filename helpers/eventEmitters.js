/* dispatch event to all subscribers to type event*/
export function dispatchEvent(type, event) {
  let data = JSON.parse(event)
  var newEvent = new CustomEvent(type, { detail: data })
  return document.dispatchEvent(newEvent)
}
/* generic subscribe to type event*/
export function suscribe(type, callback) {
  document.addEventListener(type, (event) => {
    const data = event.detail
    return callback(data)
  })
}
/* subscribe to marker popup state events */
export const subscribeMarkerPopUpState = (callback) =>
  suscribe(EVENT_TYPES.MarkerPopUp, callback)

/* Type custom event */
export const EVENT_TYPES = {
  MarkerPopUp: 'PopUpStateEvent'
}
