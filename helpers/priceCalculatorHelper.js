// export function priceWithAditionalServices(services, totalDays, totalPersons) {
//   let total_price = 0
//   services.forEach((s) => {
//     if (s.checked) {
//       switch (s.modality) {
//         case 1:
//           total_price = totalDays
//             ? total_price + totalDays * Number(s.price)
//             : total_price
//           break
//         case 2:
//           total_price = totalDays ? total_price + Number(s.price) : total_price
//           break
//         case 3:
//           total_price = totalPersons
//             ? total_price + totalPersons * Number(s.price)
//             : total_price
//           break

//         default:
//           total_price
//           break
//       }
//     }
//   })
//   return total_price
// }

export const priceWithExtraPeople = (
  extra_people_capacity,
  extra_people_price,
  totalPersons,
  totalDays
) => {
  if (totalPersons <= extra_people_capacity || !totalDays) return 0
  return totalDays * extra_people_price * (totalPersons - extra_people_capacity)
}
