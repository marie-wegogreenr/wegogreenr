export const findBreakfastCategory = (services) => {
  const breakfastServices = services.filter((s) => s.breakfast_type_id !== null)
  if (breakfastServices.length === 0) return 'Non inclus'
  const hasFreebreakfast = breakfastServices.some((s) => s.price === '0')
  if (hasFreebreakfast) return 'Inclus'
  return 'optionnel'
}

export const mapNormalServices = (services = []) => {
  return services.map((s) => {
    return {
      id: s.id,
      modality: s.modality,
      price: s.price,
      name: `${s.service.name} ${
        s.breakfast_type_name ? s.breakfast_type_name : ''
      } `,
      typeService: 'normal',
      checked: false
    }
  })
}

export const mapCustomServices = (services = []) => {
  return services.map((s) => {
    return {
      id: s.id,
      modality: s.modality,
      name: s.name,
      price: s.price,
      typeService: 'custom',
      checked: false
    }
  })
}
