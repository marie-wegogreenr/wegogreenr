export const FilterReservations = (r, filterBy) => {
  if (filterBy === 'toutes') return true
  else if (filterBy === 'annulé') {
    return r.status === 2 || r.status === 4 || r.status === 5
  } else if (filterBy === 'en_ligne') {
    return (
      new Date(r.arrival_date) >= new Date() &&
      !(r.status === 2 || r.status === 4 || r.status === 5)
    )
  } else if (filterBy === 'passées') {
    return (
      new Date(r.departure_date) <= new Date() &&
      !(r.status === 2 || r.status === 4 || r.status === 5)
    )
  }
}
