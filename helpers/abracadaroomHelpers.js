export const isSomeItemChecked = (selectedRows) => {
  if (selectedRows) {
    if (selectedRows.length === 0) {
      notify(
        'vous devez sélectionner au moins un élément dans le tableau',
        'warning'
      )
      return false
    } else {
      return true
    }
  }
}

export const showNotify = (code) => {
  if (code === 200) {
    notify('bonne mise à jour', 'success')
  } else {
    notify('il y a eu un problème, veuillez réessayer plus tard', 'danger')
  }
}
