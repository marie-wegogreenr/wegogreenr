import { getDaysInMonth } from './calendarHelper'

export function mapRooms(rooms) {
  return rooms?.map((room) => {
    let images = []
    if (room.origin === null || room.origin === 1) {
      images = room.room_image
    } else if (room.origin === 2) {
      images = room.external_images
    }
    return {
      origin: room.origin,
      id: room.id,
      public_name: room.public_name,
      people_capacity: room.people_capacity,
      basic_price: room.basic_price,
      slug: room.slug,
      type_id: room.type_id,
      establishment: room.establishment,
      isVisible: room.isVisible,
      room_image: images,
      origin_url: room.origin_url
    }
  })
}

export function formatSlug(_value) {
  let slug = _value.split(' ').join('-')
  slug = slug
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .toLowerCase()

  const expressionSymbols =
    /[\u0021-\u002C || \u002E-\u002F || \u003A-\u0040 || \u005B-\u0060 || \u007B-\u007E || \u00A0-\u00FF || \u0100-\u024F || \u1E02-\u1EF3 || \u0250-\u02AF || \u02B0-\u02FF || \u0300-\u036F || \u0370-\u03FF || \u0400-\u04FF || \u2012-\u204D || \u20A0-\u20BF || \u2100-\u213A || \u2153-\u2183 || \u2190-\u21F3 || \uFE50-\uFE6B]+/

  if (expressionSymbols.test(slug)) {
    const splittedSlug = slug.split('')
    const cleanSlug = splittedSlug.filter(
      (letter) => !expressionSymbols.test(letter)
    )
    slug = cleanSlug.join('')
  }

  return slug
}

export function getSortedDates(availabilities = []) {
  let array_dates = []
  let sorted_array_dates = []

  availabilities.forEach((availability) => {
    array_dates.push({
      date: `${new Date(availability.date).getFullYear()}-${
        new Date(availability.date).getMonth() + 1
      }-${new Date(availability.date).getDate()}`,
      isChecked: true,
      id: availability.id
    })
    sorted_array_dates = array_dates.sort((a, b) => new Date(a) - new Date(b))
  })

  return {
    array_dates,
    sorted_array_dates
  }
}

export function getArrayCalendars(quantityMonths) {
  let array = []
  var date_base = new Date()
  let next_date = new Date(date_base.getFullYear(), date_base.getMonth() - 1, 1)
  for (let i = 0; i < quantityMonths; i++) {
    array.push({
      date: next_date,
      min_date: date_base,
      status_block: false
    })
    let date = new Date(next_date.setMonth(next_date.getMonth() + 1))
    next_date = date
  }

  return array
}

export function changeArrayCalendars(functionArguments, callback) {
  const { index, status_block, array_calendars = [] } = functionArguments

  let dates = array_calendars
  let days = getDaysInMonth(
    dates[index].date.getMonth(),
    dates[index].date.getFullYear()
  )

  days.forEach((value, index) => {
    callback(value)
  })

  if (dates[index].status_block == true) {
    dates[index].status_block = false
  } else {
    dates[index].status_block = true
  }

  return dates
}

export function getDatesNotAvailabilities(event, dates_not_availabities) {
  let array_dates = dates_not_availabities
  let acum = 0
  let position = 0

  array_dates.forEach((array, index) => {
    if (
      array.date ===
      `${event.getFullYear()}-${event.getMonth() + 1}-${event.getDate()}`
    ) {
      acum = acum + 1
      position = index
    }
  })

  if (acum > 0) {
    array_dates[position].isChecked = !array_dates[position].isChecked
  } else {
    array_dates.push({
      date: `${event.getFullYear()}-${event.getMonth() + 1}-${event.getDate()}`,
      isChecked: true
    })
  }

  let sorted_array_dates = array_dates.sort((a, b) => new Date(a) - new Date(b))

  return {
    array_dates,
    sorted_array_dates
  }
}
