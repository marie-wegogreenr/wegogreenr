import moment from 'moment'
import { formatDate } from './timeHelper'
import * as _ from 'lodash'

export const mapUnavailableDays = (unavailableDays, icals) => {
  const filteredDays = unavailableDays.filter((day) => day.type !== 1)
  return filteredDays.map((day) => {
    let name
    if (day.event_name) {
      name = day.event_name
    } else if (day.title) {
      name = day.title
    } else if (day.type === 4 && day.title === null) {
      name = ''
    } else {
      name = 'Reservation'
    }

    const price =
      day.room_special_availability_price !== undefined
        ? day.room_special_availability_price
        : []

    return {
      ...price,
      title: name,
      start: moment(day.date).toDate(),
      end: moment(day.date).toDate(),
      isChecked: true,
      type: day.type,
      id: day.id,
      classification: day.classification,
      color:
        day.type == 4
          ? '#E4BE74'
          : !day.ical_name
          ? '#445136'
          : icals.find((ical) => ical.ical_link == day.ical_link).color
    }
  })
}

export const mapCalendar = (unavailableDays) => {
  return unavailableDays.map((day) => {
    let name
    if (day.event_name) {
      name = day.event_name
    } else if (day.title) {
      name = day.title
    } else {
      name = day.type == 1 ? 'Disabled' : 'Reservation'
    }
    return {
      title: name,
      start: moment(day.date).toDate(),
      end: moment(day.date).toDate(),
      isChecked: true,
      type: day.type,
      id: day.id,
      special_price: day.room_special_availability_price
        ? day.room_special_availability_price.price
        : ''
    }
  })
}

export const addColor = (icals) => {
  return icals.map((ical) => {
    return {
      ...ical,
      color: (ical.color = `#${Math.floor(Math.random() * 16777215).toString(
        16
      )}`)
    }
  })
}

export const getArrayofDates = (startDate, stopDate) => {
  let dateArray = new Array()
  let currentDate = startDate
  while (currentDate <= stopDate) {
    dateArray.push(new Date(currentDate).toISOString())
    currentDate = currentDate.addDays(1)
  }
  return dateArray
}

Date.prototype.addDays = function (days) {
  var date = new Date(this.valueOf())
  date.setDate(date.getDate() + days)
  return date
}

export const ifDateInArray = (specialPrices, arrDates) => {
  let flag = false
  specialPrices.forEach((sp) => {
    const date = moment(sp.start).format('YYYY-MM-DD')
    if (
      arrDates.some((element) => moment(element).format('YYYY-MM-DD') === date)
    ) {
      flag = true
    }
  })
  return flag
}

export const mapNewUnavailability = (data) => {
  return {
    title: data.title,
    start: moment(data.date).toDate(),
    end: moment(data.date).toDate(),
    isChecked: true,
    type: data.type,
    id: data.id,
    special_price: ''
  }
}

export const mapNewSpecialPrice = (data) => {
  return data.map((d) => {
    return {
      ...d.room_special_availability_price,
      title: d.title,
      start: moment(d.date).toDate(),
      end: moment(d.date).toDate(),
      isChecked: true,
      type: 4,
      id: d.id,
      color: '#E4BE74'
    }
  })
}

export const getDuplicateItem = (arrItems) => {
  return _.map(arrItems, (o, i) => {
    var eq = _.find(arrItems, (e, ind) => {
      if (i > ind) {
        return _.isEqual(e.date, o.date)
      }
    })
    if (eq) {
      o.isDuplicate = true
      return o
    } else {
      return o
    }
  }).filter((dup) => dup.isDuplicate)
}

export const getDaysInMonth = (month, year) => {
  var date = new Date(year, month, 1)
  var days = []
  while (date.getMonth() === month) {
    days.push(new Date(date))
    date.setDate(date.getDate() + 1)
  }
  return days
}

export const setMonthName = (mon) => {
  return [
    'Janvier',
    'Février',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Août',
    'Septembre',
    'Octobre',
    'Novembre',
    'Décembre'
  ][mon - 1]
}

export const calendarAddMonths = (_quantityMonths) => {
  let array = []
  var date_base = new Date()
  let next_date = new Date(date_base.getFullYear(), date_base.getMonth() - 1, 1)
  let quantityMonths = _quantityMonths
  quantityMonths = quantityMonths + 4
  for (let i = 0; i < quantityMonths; i++) {
    array.push({
      date: next_date,
      min_date: date_base,
      status_block: false
    })
    let date = new Date(next_date.setMonth(next_date.getMonth() + 1))
    next_date = date
  }
  return {
    array,
    quantityMonths
  }
}
