import Resizer from 'react-image-file-resizer'

export const ChangePositionImage = (image_change, new_index_in, image_u) => {
  let image_url = image_u

  let index_image_actual
  let index_image_actual_position

  let index_image_new
  let index_image_new_position

  image_url.forEach((image, index) => {
    if (image.position == new_index_in) {
      index_image_actual = index
      index_image_actual_position = image.position
    }

    if (index == image_change) {
      index_image_new = index
      index_image_new_position = image.position
    }
  })

  image_url.forEach((image, index) => {
    if (index_image_new == index) {
      image.position = index_image_actual_position
      image.isModified = true
    } else if (index_image_actual == index) {
      image.position = index_image_new_position
      image.isModified = true
    }
  })

  return image_url
}

export const orderImagesByPosition = (images, index) => {
  try {
    return images.sort((a, b) => (b.order < a.order ? 1 : -1))
  } catch (error) {
    return []
  }
}

export const resizeFile = (file) => {
  return new Promise((resolve) => {
    Resizer.imageFileResizer(
      file,
      1240,
      640,
      'JPEG',
      80,
      0,
      (uri) => {
        resolve(uri)
      },
      'blob'
    )
  })
}
