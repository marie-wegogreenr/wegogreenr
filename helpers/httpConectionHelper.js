/** @format */
const urlBase = process.env.NEXT_PUBLIC_API_URL

/**
 * @param {string} url url to consult
 * this function detects if it is a new base url (starts with http: // or https: //).
 * If so, return the url. otherwise, it is assumed to be a fragment
 * from path so it is concatenated with the export constant urlBase
 **/
export const readUrl = (url = '') =>
  url.startsWith('http://') || url.startsWith('https://')
    ? url
    : `${urlBase}${url}`

export const get = (url = '', headers = {}) =>
  fetch(readUrl(url), {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',

      ...headers
    }
  }).then((response) => response.json())

export const generalGet = (url = '', headers = {}) =>
  fetch(readUrl(url), {
    method: 'GET',
    headers: {
      Accept: 'application/json',

      ...headers
    }
  }).then((response) => response.json())

export const getWithStatus = async (url = '', headers = {}) => {
  const response = await fetch(readUrl(url), {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...headers
    }
  })

  return { data: await response.json(), code: response.status }
}

export const post = (url = '', body = {}, headers = {}) =>
  fetch(readUrl(url), {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...headers
    }
  }).then((response) => response.json())

export const postExport = (url = '', body = {}, headers = {}, name = 'file') =>
  fetch(readUrl(url), {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      ...headers
    }
  })
    .then((response) => response.blob())
    .then(async (res) => {
      const url = window.URL.createObjectURL(res)
      const link = document.createElement('a')
      link.href = url
      link.setAttribute('download', `${name}.xlsx`)
      document.body.appendChild(link)
      link.click()
      link.remove()
    })

export const improvedPost = ({
  url = '',
  body = {},
  headers = {},
  toString = true
}) =>
  fetch(readUrl(url), {
    method: 'POST',
    body: toString ? JSON.stringify(body) : body,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...headers
    }
  }).then((response) => response.json())

export const postWithStatus = async (url = '', body = {}, headers = {}) => {
  const response = await fetch(readUrl(url), {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...headers
    }
  })

  return { data: await response.json(), code: response.status }
}
export const postFiles = async (url = '', body = {}, headers = {}) => {
  const response = await fetch(readUrl(url), {
    method: 'POST',
    body: body,
    headers: {
      Accept: 'application/json',
      ...headers
    }
  })

  return { data: await response.json(), code: response.status }
}

export const put = (url = '', body = {}, headers = {}) =>
  fetch(readUrl(url), {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...headers
    }
  }).then((response) => response.json())

export const putWithStatus = async (url = '', body = {}, headers = {}) => {
  const response = await fetch(readUrl(url), {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...headers
    }
  })

  return { data: await response.json(), code: response.status }
}

export const patch = (url = '', body = {}, headers = {}, toString = true) =>
  fetch(readUrl(url), {
    method: 'PATCH',
    body: toString ? JSON.stringify(body) : body,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...headers
    }
  }).then((response) => response.json())

export const del = (url = '', body = {}, headers = {}) =>
  fetch(readUrl(url), {
    method: 'DELETE',
    body: JSON.stringify(body),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...headers
    }
  }).then((response) => response.json())

export const delWithStatus = async (url = '', headers = {}) => {
  const response = await fetch(readUrl(url), {
    method: 'DELETE',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...headers
    }
  })

  return { data: await response.json(), code: response.status }
}
