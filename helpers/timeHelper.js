import moment from 'moment'

/**
 * parses the dates coming from the database
 * @param {string} date date with format of the database
 * @returns js Date
 */

export function dateFromDb(date) {
  const format = date.split('-')
  return new Date(format[0], format[1] - 1, format[2].slice(0, 2))
}

/**
 * Rounds the time to nearest integer
 * @param {string} time with 24 hour format
 * @returns hour rounded to integer
 */

export function roundTimeToNearest(time) {
  const splitHour = time.split(':')
  const minutes = parseInt(splitHour[1]) / 60
  const hours = parseInt(splitHour[0])
  const numberTime = Math.round(hours + minutes)

  return numberTime
}

/**
 *
 * @param {dateObject} date
 * @returns date in text format 'Monday 3 may 2021'
 */

export function dateToText(date) {
  if (date == undefined) return 'bad date'
  const dateParsed = new Date(date)
  const options = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  }

  return new Intl.DateTimeFormat('default', options).format(dateParsed)
}

/**
 *
 * @param {string} date date object js
 * @returns string
 */

export function formatDate(date) {
  const day = `0${date.getDate()}`.slice(-2)
  const month = `0${date.getMonth() + 1}`.slice(-2)
  const year = date.getFullYear()

  return `${year}-${month}-${day}`
}

export function getADayLater(date) {
  const ONE_DAY_IN_MILLISECONDS = 1000 * 60 * 60 * 24

  const { 0: year, 1: month, 2: day } = date.split('-')

  const initialDateInMilliseconds = new Date(
    year,
    Number(month) - 1,
    day
  ).getTime()
  const finalDateInMilliseconds =
    initialDateInMilliseconds + ONE_DAY_IN_MILLISECONDS

  const newFinalDate = new Date(finalDateInMilliseconds)

  return newFinalDate
}

/**
 * convert any date to an american format DD/MM/YYYY
 * @param {jsdate} date Js date object
 * @returns string
 */

export function getAmericanDate(date) {
  const newDate = new Date(date)
  return `${newDate.getDate()}/${
    newDate.getMonth() + 1
  }/${newDate.getFullYear()}`
}

/**
 * given an starting and ending date returns the weekdays and weekends
 * @param {string} startDate starting date
 * @param {string} endDate end date
 * @returns [weekdays, weekends]
 */

export function getDaysAndWeekends(
  startDate = '2021-03-01',
  endDate = '2021-03-07'
) {
  let init = new Date(startDate).getTime()
  let end = new Date(endDate).getTime() - 86400000
  let weekdays = 0
  let weekends = 0

  if (init <= end) {
    while (init <= end) {
      let dayOfWeekInNumber = new Date(init).getDay()
      if (dayOfWeekInNumber === 5 || dayOfWeekInNumber === 6) weekends++
      else weekdays++
      init += 86400000
    }
  }

  return [weekdays, weekends]
}

export function hasTimeToShow(time) {
  return time !== '' ? new Date(time) : ''
}

export function formatMoment(date) {
  return moment(date).format('YYYY-MM-DD')
}

export const formatSafari = (date) =>
  new Date(moment(date).format('YYYY/MM/DD 00:00:00'))
