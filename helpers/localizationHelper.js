export const findRegionAndCity = (arrLocalization) => {
  const adressComponents = arrLocalization.filter(
    (ac) =>
      ac.types[0] === 'administrative_area_level_1' ||
      ac.types[0] === 'locality'
  )

  const region_name =
    adressComponents.find((ac) => ac.types[0] === 'administrative_area_level_1')
      ?.long_name || ''

  const city_name =
    adressComponents.find((ac) => ac.types[0] === 'locality')?.long_name || ''

  return { region_name, city_name }
}
