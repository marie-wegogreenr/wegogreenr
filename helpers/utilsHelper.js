import { toast } from 'react-toastify'
import Cookies from 'universal-cookie'

/**
 *
 * @param {string} text text to cut
 * @param {number} limit numer of characters to cut
 * @returns string
 */

export function cutText(text, limit) {
  if (typeof text == 'string' && text !== undefined) {
    if (text.length > limit) return text.slice(0, limit) + '...'
    return text.slice(0, limit)
  }
  return ''
}

export const formatDateRegister = (date = '') => {
  let finalDate = ''

  if (date.includes('/')) {
    finalDate = date.replaceAll('/', '-')
  } else if (date.includes(' ')) {
    finalDate = date.replaceAll(' ', '-')
  } else {
    finalDate = date
  }

  if (
    finalDate.split('-')[1].length > 2 ||
    finalDate.split('-')[2].length > 2
  ) {
    toast.error(`Vous devez introduire la date au format YYYY-MM-DD`, {
      position: toast.POSITION.BOTTOM_LEFT
    })

    return {
      finalDate,
      error: true
    }
  } else {
    return {
      finalDate,
      error: false
    }
  }
}

export const validateOnlyNumber = (value) => {
  const pattern = /[^\d]/g
  return !pattern.test(value)
}

export const inputOnlyNumberComma = (value) => {
  const pattern = /[^\d,]/g
  return !pattern.test(value)
}

export const validatePrices = (priceBase, priceWeekend) => {
  const priceBaseCommas = priceBase.match(/[,]/g)?.length
  const priceWeekendCommas = priceWeekend?.match(/[,]/g)?.length

  if (priceWeekendCommas) {
    return (
      (priceBaseCommas <= 1 && priceWeekendCommas <= 1) ||
      priceBaseCommas === undefined ||
      priceWeekendCommas === undefined
    )
  } else {
    return priceBaseCommas <= 1 || priceBaseCommas === undefined
  }
}

export const convertPrices = (priceBase, priceWeekend) => {
  let _priceBase = priceBase
  let _priceWeekend = priceWeekend
  if (typeof priceBase === 'number') {
    _priceBase = priceBase.toString()
  }
  if (typeof priceWeekend === 'number') {
    _priceWeekend = priceWeekend.toString()
  }
  const priceBaseFormatted = _priceBase.replace(',', '.')
  const priceWeekendFormatted = _priceWeekend?.replace(',', '.')

  if (priceWeekendFormatted) {
    return { priceBaseFormatted, priceWeekendFormatted }
  } else {
    return priceBaseFormatted
  }
}

export const adjustTypeToNumber = (value) => {
  if (typeof value === 'string') {
    return parseFloat(value.replace(',', '.'))
  } else {
    return value
  }
}

export const getLastArticles = async () => {
  const articles = await blogArticlePosts()
  return articles.filter((item, index) => {
    item.position = index + 1
    return index <= 3
  })
}
/**
 * @param {number} price received
 * @returns string price formatted
 */
export const adjustNumberWithComma = (number = 0) => {
  if (number === null || Object.is(number, NaN)) {
    return '-'
  }
  const numberFixed = parseFloat(number).toFixed(2)
  let numberComma = numberFixed.replace('.', ',')
  if (numberComma.split(',')[1] === '00') {
    numberComma = numberComma.split(',')[0]
  }
  return numberComma
}

export function getSourceIcon(number) {
  return `/images/onboarding/greenscore/final_score/result_greenscore_${number}.png`
}

/**
 *
 * @param {number} score greenScore
 * @returns imgUrl
 */

export function getGreenScoreSourceIcon(score) {
  if (score >= 45 && score <= 69) {
    return getSourceIcon('1')
  }

  if (score >= 70 && score <= 99) {
    return getSourceIcon('2')
  }

  if (score >= 100 && score <= 129) {
    return getSourceIcon('3')
  }

  if (score >= 130 && score <= 179) {
    return getSourceIcon('4')
  }

  if (score >= 180) {
    return getSourceIcon('5')
  }

  return getSourceIcon('1')
}

export function getImageURL(image) {
  if (!image) {
    return 'https://picsum.photos/400/300'
  }

  return `${process.env.NEXT_PUBLIC_AMAZON_IMGS}${image}`
}

/**
 *
 * @returns user token from cookie
 */

export function getTokenFromCookie() {
  const cookie = new Cookies()
  const jwt = cookie.get('tk_user')
  if (!jwt) console.error('missing jwt')

  return jwt
}

/**
 *
 * @param {latLng object} point
 * @param {latLng object} bounds
 * @returns boolean {boolean}
 */
export function inBounds(point, bounds) {
  let eastBound = point.long < bounds.NE.long
  let westBound = point.long > bounds.SW.long
  let inLong

  if (bounds.NE.long < bounds.SW.long) {
    inLong = eastBound || westBound
  } else {
    inLong = eastBound && westBound
  }

  let inLat = point.lat > bounds.SW.lat && point.lat < bounds.NE.lat
  return inLat && inLong
}

export const isEmpty = (value) => {
  if (!value) return true
  if (isNaN(value)) return true
  if (Array.isArray(value)) return value.length === 0
  if (typeof value === 'object') return Object.keys(value) === 0
  return false
}

const IS_PRODUCTION = false || process.env.NODE_ENV !== 'production'

/**
 * Show all fg messages of debuuging in same application, only if IS_PRODUCTION is true
 * @param {any} logMessage
 * @param {any} data
 */
export const consoleLog = (logMessage, data = {}) => {
  if (!IS_PRODUCTION) console.log(logMessage, data)
}

export const headerWithToken = (token = null) => {
  return {
    authorization: `Bearer ${token === null ? getTokenFromCookie() : token}`
  }
}

export const getStatusStyle = (id) => {
  let style = ''
  switch (id) {
    case 0:
      style = ''
      break
    case 1:
      style = 'location-card__status-green'
      break
    case 2:
      style = 'location-card__status-red'
      break
    case 3:
      style = `location-card__status-yellow`
      break

    default:
      style = ''
      break
  }

  return style
}

export const getBackgroundStyle = (id) => {
  let style = ['', 'success', 'danger', 'warning', 'danger', 'danger']

  return style[id]
}

export const getStatusMessage = (id) => {
  let Message = ''
  switch (id) {
    case 0:
      Message = 'En attente de paiement'
      break
    case 1:
      Message = 'Approuvé'
      break
    case 2:
      Message = 'Annulée'
      break
    case 3:
      Message = `En attente d'approbation`
      break
    case 4:
      Message = 'Annulée'
      break
    case 5:
      Message = 'Annulée'
      break

    default:
      Message = `En attente de paiement`
      break
  }

  return Message
}

export const getServiceType = (id) => {
  let Message = ''
  switch (id) {
    case 0:
      Message = ''
      break
    case 1:
      Message = 'Jour'
      break
    case 2:
      Message = 'Séjour'
      break
    case 3:
      Message = `Personne`
      break

    default:
      Message = ''
      break
  }

  return Message
}

export const getStatusType = (id) => {
  let Message = ''
  switch (Number(id)) {
    case 1:
      Message = 'nouveau'
      break
    case 2:
      Message = 'en ligne'
      break
    case 3:
      Message = 'hors ligne'
      break
    case 4:
      Message = `archivé`
      break

    default:
      Message = ''
      break
  }

  return Message
}
