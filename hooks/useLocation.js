import * as React from 'react'
import { consoleLog } from '/utils/logConsole'
import { sessionStorageItems } from 'constants/index'

const localState = {
  lon: null,
  lat: null
}

function useLocation(initialState = localState) {
  const [location, setLocation] = React.useState(initialState)
  const [locationAccess, setLocationAccess] = React.useState(false)

  React.useEffect(() => {
    const locationOfSessionStorage = JSON.parse(
      sessionStorage.getItem(sessionStorageItems.LOCATION) || '{}'
    )

    if (locationOfSessionStorage.lon && locationOfSessionStorage.lat) {
      return setLocation(locationOfSessionStorage)
    }

    const success = ({ coords }) => {
      const location = {
        lat: coords.latitude,
        lon: coords.longitude
      }

      setLocation(location)
      setLocationAccess(true)
      sessionStorage.setItem(
        sessionStorageItems.LOCATION,
        JSON.stringify(location)
      )
    }

    const error = (err) => {
      // console.error(err)

      const location = {
        lat: null,
        lon: null
      }

      setLocation(location)
    }

    if ('geolocation' in navigator) {
      navigator.geolocation.watchPosition(success, error)
    }
  }, [location?.lat, location?.lon])

  return { lat: location.lat, lon: location.lon, locationAccess }
}

export default useLocation
