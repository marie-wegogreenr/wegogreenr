import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

function useFocus() {
  const [isFocus, setIsFocus] = React.useState(false)

  const handleFocus = () => {
    setIsFocus(true)
  }
  const handleBlur = () => {
    setIsFocus(false)
  }

  return { isFocus, handleBlur, handleFocus, setIsFocus }
}

export default useFocus
