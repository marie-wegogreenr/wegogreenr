import React, { useEffect, useState } from 'react'
import {
  setAdminTab,
  setClearLoading,
  setFullLoading
} from '@ducks/Config/actions'
import { useDispatch } from 'react-redux'
import { Pagination } from '@components/dashboard_admin/Pagination'

export default function useDataWithPagination(page, searchData) {
  const dispatch = useDispatch()
  const [rooms, setRooms] = useState([])
  const [query, setQuery] = useState({})
  const [pag, setPag] = useState({})
  useEffect(async () => {
    dispatch(setAdminTab(page))
    dispatch(setFullLoading(true))
    const { data, ...pagination } = await searchData(1, {})
    setPag(pagination)
    setRooms(data)
    dispatch(setFullLoading(false))
  }, [])

  const handlePagination = async (pag) => {
    dispatch(setClearLoading(true))
    const { data, ...pagination } = await searchData(pag, query)
    setPag(pagination)
    setRooms(data)
    dispatch(setClearLoading(false))
  }

  return [rooms, setRooms, pag, setPag, handlePagination, query, setQuery]
}
