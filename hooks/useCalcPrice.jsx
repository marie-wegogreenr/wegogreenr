import { getMinMaxNights } from '@components/Hebergements/Single/Form/matchSpecialDates'
import * as React from 'react'

const CalcPrice = ({ fields, month_discount, total_price }) => {
  if (fields.checkIn && fields.checkOut && fields.checkIn < fields.checkOut) {
    if (specialDates.length > 0) {
      const [
        specialWeekdays, // special days
        specialWeekends, // special findes
        specialWeekDaysPrice,
        specialWeekendsPrice
      ] = getSpecialDatesPrices(
        fields.checkIn,
        fields.checkOut,
        specialDates,
        basic_price,
        weekend_price
      )

      const normalDays =
        (weekdays - specialWeekdays) * basic_price +
        (weekends - specialWeekends) * wknd_price

      const specialDays = specialWeekDaysPrice + specialWeekendsPrice
      total_price = normalDays + specialDays
    } else {
      total_price = basic_price * weekdays + wknd_price * weekends
    }
  }
}

export default CalcPrice
