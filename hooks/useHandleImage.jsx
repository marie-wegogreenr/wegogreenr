/** @format */

import { resizeFile } from 'helpers/imagesHelper'
import moment from 'moment'
import { useEffect, useReducer } from 'react'
import { saveRoomImage2 } from 'services/imagesService'

const initialState = {
  files: [
    {
      id: '278',
      image:
        'httidps://wegogreenr-laravel.s3.eu-west-2.amazonaws.com/11396/room_332_Cuisine G12022020105584834.jfif',
      position: 2,
      chargePercent: 100
    }
  ]
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'load':
      return { ...state, files: { ...state.files, ...action.files } }

    default:
      return state
  }
}

const useHandleImage = (initImages = []) => {
  const [state, dispatch] = useReducer(reducer, {
    ...initialState,
    files: initImages
  })

  useEffect(() => {}, [state.files])

  const onChange = (uploadFiles) => {
    if (uploadFiles.length) {
      const arrFiles = Array.from(uploadFiles)
      const files = arrFiles.map((file, index) => {
        return MapFile(file, state.files.length + index)
      })
      dispatch({ type: 'load', files })

      // SaveNewImage(file)
    }
  }

  return {
    ...state,
    onChange
  }
}

const MapFile = async (file, position) => {
  const resp = await resizeFile(file)
  const image = URL.createObjectURL(resp)
  const blobNewFile = resp.slice(0, resp.size, resp.type)

  const name = file.name
  const lastDot = name.lastIndexOf('.')
  const fileName = name.substring(0, lastDot)
  const ext = name.substring(lastDot + 1)
  const reducedNewFile = new File(
    [blobNewFile],
    fileName + moment().format('YYYYMMDDhhmmssSS') + '.' + ext,
    { type: blobNewFile.type }
  )

  return {
    id: null,
    image: image,
    file: reducedNewFile,
    position,
    chargePercent: 0
  }
}

export default useHandleImage
