import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

function useIsInViewport() {
  const elementRef = React.useRef()
  const [boundingClientRect, setBoundingClientRect] = React.useState({})

  React.useEffect(() => {
    const handleScroll = () => {
      if (elementRef.current) {
        setBoundingClientRect(elementRef.current.getBoundingClientRect())
      }
    }
    document.addEventListener('scroll', handleScroll)

    return () => document.removeEventListener('scroll', handleScroll)
  }, [])

  return { elementRef, boundingClientRect }
}

export default useIsInViewport
