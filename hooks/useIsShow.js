import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

export function useIsShow() {
  const ref = React.useRef()
  const [isShow, setIsShow] = React.useState(false)

  const handleScroll = () => {
    const element = ref.current

    if (ref.current) {
      const { y } = element.getBoundingClientRect()

      if (y < -200) {
        setIsShow(true)
      } else {
        setIsShow(false)
      }
    }
  }

  React.useEffect(() => {
    window.addEventListener('scroll', handleScroll)

    return () => window.removeEventListener('scroll', handleScroll)
  }, [])

  return { ref, isShow }
}

export default useIsShow
