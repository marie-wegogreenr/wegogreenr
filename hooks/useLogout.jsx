import { deleteEstablishment } from '@ducks/host/establishment/actions'
import { deleteUser } from '@ducks/userDuck'
import { cleanReservationAction } from '@ducks/checkoutDuck'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { logOut } from 'services/userServices'
import Router from 'next/router'
import Cookies from 'universal-cookie'

export default function useLogout() {
  const dispatch = useDispatch()
  return async () => {
    try {
      const cookies = new Cookies()
      const resp = await logOut()
      if (resp.code !== 200) {
        cookies.remove('tk_user', { path: '/', expires: new Date() })
        setTimeout(() => {
          Router.push('/login')
        }, 1000)
      } else {
        cookies.remove('tk_user', { path: '/', expires: new Date() })
        setTimeout(() => {
          Router.push('/')
        }, 1000)
      }

      dispatch(deleteUser())
      dispatch(deleteEstablishment())
      dispatch(cleanReservationAction())
    } catch (error) {
      document.cookie = 'tk_user=;path=/;expires=' + new Date()
      Router.push('/login')
    }
  }
}
