import { getMinMaxNights } from '@components/Hebergements/Single/Form/matchSpecialDates'
import * as React from 'react'

const useCalcMinMaxNight = ({
  fields,
  specialDates,
  min_nigth,
  max_nigth,
  totalDays,
  setDisableBtn
}) => {
  const [minNights, maxNights] = getMinMaxNights(
    fields.checkIn,
    fields.checkOut,
    min_nigth,
    max_nigth,
    specialDates
  )

  React.useEffect(() => {
    if (totalDays < minNights && minNights !== null)
      return setDisableBtn(`Durée minimale de séjour : ${minNights} nuit(s)`)
    if (totalDays > maxNights && maxNights !== null)
      return setDisableBtn(`Durée maximale de séjour : ${maxNights} nuit(s)`)
    return setDisableBtn('')
  }, [totalDays, minNights, maxNights])
  return [minNights, maxNights]
}

export default useCalcMinMaxNight
