import * as React from 'react'
/* Next */
import { useRouter } from 'next/router'
/* Constants */
import { routes } from 'constants/index'

export function useRedirect({ pathName, query }) {
  const [isFocus, setIsFocus] = React.useState(false)

  const router = useRouter()
  if (!routes[pathName]) return
  return router.replace(
    {
      pathname: routes[pathName],
      query
    },
    undefined,
    { scroll: false }
  )
}
