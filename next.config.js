const withImages = require('next-images')

module.exports = withImages({
  // env: {
  //   API_URL: process.env.API_URL_ENV,
  BROWSER: JSON.stringify(true),
  //   AMAZON_IMGS: process.env.AMAZON_IMGS_ENV,
  //   GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID_ENV,
  //   GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET_ENV,
  //   NEXTAUTH_URL: process.env.NEXTAUTH_URL_ENV,
  //   ALGOLIA_APP_ID: process.env.ALGOLIA_APP_ID_ENV,
  //   ALGOLIA_SECRET: process.env.ALGOLIA_SECRET_ENV,
  //   APP_SECRET: process.env.APP_SECRET_ENV,
  //   GOOGLE_MAPS_KEY: process.env.GOOGLE_MAPS_KEY_ENV,
  //   BLOG_URL: process.env.BLOG_URL_ENV
  // },
  future: {
    webpack5: true
  },
  images: {
    // loader: "imgix",
    disableStaticImages: true,
    domains: [
      'dummyimage.com',
      'wegogreenr-laravel.s3.eu-west-2.amazonaws.com',
      'picsum.photos',
      'gotripslk.com',
      'vacation-imgproxy.unicstay.com.',
      'blog.wegogreenr.com'
    ]
  },

  async redirects() {
    return [
      {
        source: '/green-housing/1576',
        destination:
          '/hebergements/tiny-house-au-cur-des-vignes-champenoises-513',
        permanent: true
      },
      {
        source: '/green-housing/1535',
        destination: '/hebergements/studio-lumiere',
        permanent: true
      },
      {
        source: '/green-housing/1625',
        destination: '/hebergements/moment-ensoleille-en-auvergne-196',
        permanent: true
      },
      {
        source: '/green-housing/1386',
        destination: '/hebergements/mieux-etre--les-rosees-de-kapha-26',
        permanent: true
      },
      {
        source: '/green-housing/1385',
        destination: '/hebergements/mieux-etre--les-caresses-de-vata-24',
        permanent: true
      },
      {
        source: '/green-housing/1348',
        destination: `/hebergements/eco-gite-le-vallon-darmandine-1`,
        permanent: true
      },
      {
        source: '/green-housing/1692',
        destination: '/hebergements/La-Chambre-Nicoya-226',
        permanent: true
      },
      {
        source: '/green-housing/1693',
        destination: '/hebergements/la-chambre-ikaria',
        permanent: true
      },
      {
        source: '/green-housing/1598',
        destination: '/hebergements/Helios---Touquet-155',
        permanent: true
      },
      {
        source: '/green-housing/1380',
        destination: '/hebergements/le-gite-de-la-ferme-20',
        permanent: true
      },
      {
        source: '/green-housing/1612',
        destination:
          '/hebergements/evasion-insolite-face-a-la-chaine-des-puys-195',
        permanent: true
      },
      {
        source: '/green-housing/1578',
        destination: '/hebergements/douce-campagne',
        permanent: true
      },
      {
        source: '/green-housing/1503',
        destination: '/hebergements/Coquillages-&-crustaces-77',
        permanent: true
      },
      {
        source: '/green-housing/1427',
        destination: '/hebergements/chambre-vegetale',
        permanent: true
      },
      {
        source: '/green-housing/1504',
        destination: '/hebergements/Chambre-bleue-nature-Vercors-112',
        permanent: true
      },
      {
        source: '/green-housing/1379',
        destination: '/hebergements/chalet-vert',
        permanent: true
      },
      {
        source: '/=B5:C58',
        destination: '/hebergements',
        permanent: true
      },
      {
        source: '/notre-demarche',
        destination: '/demarche',
        permanent: true
      },
      {
        source: '/contact',
        destination: '/contacter',
        permanent: true
      },
      {
        source: '/experience',
        destination: '/',
        permanent: true
      },
      {
        source: '/selection-vip',
        destination: '/',
        permanent: true
      }
    ]
  }
})
