import axios from 'axios'

/**
 * axios helper method
 * @param {object} options AXIOS options object
 * @returns data
 */

export async function getData(options) {
  return axios({
    ...options,
    baseURL: process.env.NEXT_PUBLIC_API_URL
  })
    .then(({ data }) => {
      return data
    })
    .catch((error) => {
      console.error(error)
      const { message, name, stack, lineNumber, columnNumber, ...rest } = error
      return Promise.reject({
        message,
        name,
        lineNumber,
        columnNumber,
        ...rest
      })
    })
}

export default getData
