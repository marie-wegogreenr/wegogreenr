import Cookies from 'universal-cookie'

/**
 *
 * @returns user token from cookie
 */

export function getTokenFromCookie() {
  const cookie = new Cookies()
  const jwt = cookie.get('tk_user')
  if (!jwt) console.error('missing jwt')
  // throw new Error('Unauthorized: missing JWT')
  //I removed the error because it breaks the execution in many pages, so i couldn't redirect the user

  return jwt
}

export default getTokenFromCookie
