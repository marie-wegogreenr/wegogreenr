/**
 *
 * @param {string} text text to cut
 * @param {number} limit numer of characters to cut
 * @returns string
 */

function cutText(text, limit) {
  if (typeof text == 'string' && text !== undefined) {
    if (text.length > limit) return text.slice(0, limit) + '...'
    return text.slice(0, limit)
  }
  return ''
}

export default cutText
