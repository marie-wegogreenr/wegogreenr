/**
 * convert any date to an american format DD/MM/YYYY
 * @param {jsdate} date Js date object
 * @returns string
 */

export default function getAmericanDate(date) {
  const newDate = new Date(date)
  return `${newDate.getDate()}/${
    newDate.getMonth() + 1
  }/${newDate.getFullYear()}`
}
