function getSourceIcon(number) {
  return `/images/onboarding/greenscore/final_score/result_greenscore_${number}.png`
}

/**
 *
 * @param {number} score greenScore
 * @returns imgUrl
 */

function getGreenScoreSourceIcon(score) {
  if (score >= 45 && score <= 69) {
    return getSourceIcon('1')
  }

  if (score >= 70 && score <= 99) {
    return getSourceIcon('2')
  }

  if (score >= 100 && score <= 129) {
    return getSourceIcon('3')
  }

  if (score >= 130 && score <= 179) {
    return getSourceIcon('4')
  }

  if (score >= 180) {
    return getSourceIcon('5')
  }

  return getSourceIcon('1')
}

export default getGreenScoreSourceIcon
