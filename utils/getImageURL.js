/**
 * concatenates the url from the database images
 * @param {string} image string from image
 * @returns string - img_url
 */

import noImage from '/public/images/not-image.svg'

function getImageURL(image) {
  if (!image) {
    return noImage
  }

  return `${process.env.NEXT_PUBLIC_AMAZON_IMGS}${image}`
}

export default getImageURL
