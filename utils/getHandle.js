function getHandle(text) {
  const handle = text.toLowerCase().replace(/\s/g, '_')

  return handle
}

export default getHandle
