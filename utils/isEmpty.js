export const isEmpty = (value) => {
  if (!value) return true
  if (isNaN(value)) return true
  if (Array.isArray(value)) return value.length === 0
  if (typeof value === 'object') return Object.keys(value) === 0
  return false
}
