import Resizer from 'react-image-file-resizer'

export const resize = ({
  file,
  maxWidth,
  maxHeight,
  minWidth = 10,
  minHeight = 10,
  compressFormat = 'JPEG',
  quality = 100,
  rotation = 0,
  outputType = 'base64'
}) =>
  new Promise((resolve) => {
    Resizer.imageFileResizer(
      file,
      maxWidth,
      maxHeight,
      compressFormat,
      quality,
      rotation,
      (uri) => {
        resolve(uri)
      },
      outputType,
      minWidth,
      minHeight
    )
  })
