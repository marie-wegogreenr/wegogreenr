const IS_PRODUCTION = false || process.env.NODE_ENV !== 'production'

/**
 * Show all console.log messages of debuuging in same application, only if IS_PRODUCTION is true
 * @param {any} logMessage
 * @param {any} data
 */
export const consoleLog = (logMessage, data = {}) => {
  if (!IS_PRODUCTION) console.log(logMessage, data)
}
