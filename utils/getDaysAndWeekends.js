/**
 * given an starting and ending date returns the weekdays and weekends
 * @param {string} startDate starting date
 * @param {string} endDate end date
 * @returns [weekdays, weekends]
 */

function getDaysAndWeekends(startDate = '', endDate = '') {
  if (startDate === '' || endDate === '' || startDate > endDate)
    return [0, 0, 0]

  let init = new Date(startDate).getTime()
  let end = new Date(endDate).getTime()
  let weekdays = 0
  let weekends = 0

  if (init <= end) {
    while (init < end) {
      let dayOfWeekInNumber = new Date(init).getDay()
      if (dayOfWeekInNumber === 5 || dayOfWeekInNumber === 6) weekends++
      else weekdays++
      init += 86400000
    }
  }

  return [weekdays, weekends, weekdays + weekends]
}

export default getDaysAndWeekends
