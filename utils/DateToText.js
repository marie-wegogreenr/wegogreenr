/**
 *
 * @param {dateObject} date
 * @returns date in text format 'Monday 3 may 2021'
 */

function dateToText(date) {
  if (date == undefined) return 'bad date'
  const dateParsed = new Date(date)
  const options = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  }

  return new Intl.DateTimeFormat('default', options).format(dateParsed)
}

export default dateToText
