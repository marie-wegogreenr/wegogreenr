function getADayLater(date) {
  const ONE_DAY_IN_MILLISECONDS = 1000 * 60 * 60 * 24

  const { 0: year, 1: month, 2: day } = date.split('-')

  const initialDateInMilliseconds = new Date(
    year,
    Number(month) - 1,
    day
  ).getTime()
  const finalDateInMilliseconds =
    initialDateInMilliseconds + ONE_DAY_IN_MILLISECONDS

  const newFinalDate = new Date(finalDateInMilliseconds)

  return newFinalDate
}

export default getADayLater
