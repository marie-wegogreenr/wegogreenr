export const DEFAULT_COUNTRY_ZOOM = 6

export const DEFAULT_REGION_ZOOM = 9

export const DEFAULT_STATE_ZOOM = 9

export const DEFAULT_CITY_ZOOM = 10

export const DEFAULT_FRANCE = {
  zoom: DEFAULT_COUNTRY_ZOOM,
  locationName: 'France',
  lat: 46.543749602738565,
  lng: 2.1423339843750004
}

export const DEFAULT_PARIS = {
  zoom: DEFAULT_CITY_ZOOM,
  locationName: 'France',
  lat: 48.856614,
  lng: 2.3522219,
  nelat: 49.29288913249991,
  nelng: 2.8083801269531254,
  swlat: 48.41735304952526,
  swlng: 1.8951416015625002
}

export const zoomByPlaceTypes = {
  country: DEFAULT_COUNTRY_ZOOM,
  administrative_area_level_1: DEFAULT_REGION_ZOOM,
  administrative_area_level_2: DEFAULT_CITY_ZOOM, // adm area lvl 2 and locality are same
  locality: DEFAULT_CITY_ZOOM
}

export const defaultZoom = {
  country: DEFAULT_COUNTRY_ZOOM,
  region: DEFAULT_REGION_ZOOM,
  state: DEFAULT_STATE_ZOOM,
  city: DEFAULT_CITY_ZOOM
}
