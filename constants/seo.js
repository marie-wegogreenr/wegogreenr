const handleSEO = (path) => {
  let resp = {}
  switch (path) {
    case 'chambre-hote':
      resp = {
        metaTitle: 'Chambre d’hôte',
        searchTitle: 'Chambre d’hôte',

        title:
          'Location de Chambre d’hôte écologiques & responsables en France - WegogreenR',
        description:
          'We Go GreenR a minutieusement choisi ses plus belles chambres d’hôtes en France pour vous permettre de passer un séjour plein de tradition et  de partage.'
      }
      break
    case 'hotel':
      resp = {
        metaTitle: 'Hotel',
        searchTitle: 'Hotel',

        title:
          'Location de Hotel écologiques & responsables en France - WegogreenR',
        description:
          'We Go GreenR a sélectionné les plus beaux hôtels écologiques de France pour vous permettre de passer un agréable séjour sans prise de tête. '
      }
      break
    case 'ecolodge':
      resp = {
        metaTitle: 'Ecolodge',
        searchTitle: 'Ecolodge',

        title:
          'Location de Ecolodge écologiques & responsables en France - WegogreenR',
        description:
          'We Go GreenR a sélectionné les plus beaux écolodges de France pour vous permettre de passer des vacances mémorables : bulle, dojo, guest house, cabane dans la forêt, tentes lodges, tiny house…'
      }
      break
    case 'insolite':
      resp = {
        metaTitle: 'Insolite qui correspond aux cabanes',
        searchTitle: 'Insolite qui correspond aux cabanes',

        title:
          'Location de Insolite qui correspond aux cabanes écologiques & responsables en France - WegogreenR',
        description:
          ' We Go GreenR a sélectionné les plus beaux <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">hébergements insolites</a></b>  et écologiques de France pour vous permettre de passer des instants inoubliables en couple, en famille ou entre amis.'
      }
      break
    case 'gite':
      resp = {
        metaTitle: 'Gite',
        searchTitle: 'Gite',

        title:
          'Location de Gite écologiques & responsables en France - WegogreenR',
        description:
          'We Go GreenR a sélectionné les plus beaux gites écologiques de France pour vous permettre de passer des moments inoubliables en toute liberté avec votre famille ou vos amis.'
      }
      break
    case 'auvergne-rhone-alpes':
      resp = {
        metaTitle: 'Auvergne-Rhône-Alpes',
        searchTitle: 'Auvergne-Rhône-Alpes',

        title: `Location d'hébergements Écolo & Responsables en Auvergne-Rhône-Alpes - WegogreenR`,
        description:
          'Grâce à ses vallées verdoyantes, ses villages pleins de charme et ses spécialités à s’en rouler par terre, vous ne regretterez pas votre escapade auvergnate !'
      }
      break
    case 'bourgogne-franche-comte':
      resp = {
        metaTitle: 'Bourgogne-Franche-Comté',
        searchTitle: 'Bourgogne-Franche-Comté',

        title: `Location d'hébergements Écolo & Responsables en Bourgogne-Franche-Comté - WegogreenR`,
        description:
          'Avec son patrimoine naturel et architectural réputé, des atouts touristiques, une gastronomie appréciée à travers le monde, cette région bourguignonne est unique !'
      }
      break
    case 'bretagne':
      resp = {
        metaTitle: 'Bretagne',
        searchTitle: 'Bretagne',

        title: `Location d'hébergements Écolo & Responsables en Bretagne - WegogreenR`,
        description:
          'Entre balades et découvertes, la Bretagne vous propose de sortir des sentiers battus et de vivre un voyage qui a du sens ! La région est riche en destinations pour un vrai break au vert.'
      }
      break
    case 'centre-val-de-loire':
      resp = {
        metaTitle: 'Centre-Val de Loire',
        searchTitle: 'Centre-Val de Loire',

        title: `Location d'hébergements Écolo & Responsables en Centre-Val de Loire - WegogreenR`,
        description: `Territoire riche d'histoire et de patrimoine, partez à la découverte des charmants villages du Centre Val de Loire et bénéficier d'un séjour en plein coeur de la nature.`
      }
      break
    case 'corse':
      resp = {
        metaTitle: 'Corse',
        searchTitle: 'Corse',

        title: `Location d'hébergements Écolo & Responsables en Corse - WegogreenR`,
        description: `Lovée entre mer et montagne, la Corse est la preuve qu’il ne faut pas partir loin pour profiter d’un paysage époustouflant et bénéficier d'un séjour inoubliable.`
      }
      break
    case 'grand-est':
      resp = {
        metaTitle: 'Grand Est',
        searchTitle: 'Grand Est',

        title: `Location d'hébergements Écolo & Responsables en Grand Est - WegogreenR`,
        description: `Vivre des vacances dans le Grand Est de la France, c’est vivre un séjour dépaysant avec des coutumes et des traditions propres au territoire qui ne vous laisseront pas indifférent !`
      }
      break
    case 'hauts-de-france':
      resp = {
        metaTitle: 'Hauts-de-France',
        metaTitle: 'Hauts-de-France',

        title: `Location d'hébergements Écolo & Responsables en Hauts-de-France - WegogreenR`,
        description: `► Découvez les Hauts de France avec ses villes médiévales, ses plages immenses, ses festivités, ses marais, dunes et falaises qui témoignent de son passé étonnant !`
      }
      break
    case 'ile-de-france':
      resp = {
        metaTitle: 'Île-de-France',
        searchTitle: 'Île-de-France',

        title: ` Location d'hébergements Écolo & Responsables en Île-de-France - WegogreenR`,
        description: `Un séjour à proximité de la capitale pour un véritable city-break, c’est ça le crédo de l’Île-de-France ! La région regorge de nombreux lieux touristiques qui valent le détour.`
      }
      break
    case 'normandie':
      resp = {
        metaTitle: 'Normandie',
        searchTitle: 'Normandie',

        title: `Location d'hébergements Écolo & Responsables en Normandie - WegogreenR`,
        description: ` Entre ses plus beaux villages de France, ses phares, ses coins nature et ses nombreuses randonnées, la Normandie est la destination parfaite pour vos prochaines vacances !`
      }
      break
    case 'nouvelle-aquitaine':
      resp = {
        metaTitle: 'Nouvelle-Aquitaine',
        searchTitle: 'Nouvelle-Aquitaine',

        title: `Location d'hébergements Écolo & Responsables en Nouvelle-Aquitaine - WegogreenR`,
        description: ` Connue pour ses vignobles et ses jolies plages, l’Aquitaine propose un séjour d’exception en pleine nature. Vivez un vrai break dans des endroits aussi verdoyants qu'inspirants.`
      }
      break
    case 'occitanie':
      resp = {
        metaTitle: 'Occitanie',
        searchTitle: 'Occitanie',

        title: ` Location d'hébergements Écolo & Responsables en Occitanie - WegogreenR`,
        description: `Entre montagnes, plages de sable, Grands Causses et étangs, la richesse de paysages permet aisément de passer des vacances mémorables dans la région occitane.`
      }
      break
    case 'pays-de-la-loire':
      resp = {
        metaTitle: 'Pays de la Loire',
        searchTitle: 'Pays de la Loire',

        title: ` Location d'hébergements Écolo & Responsables en Pays de la Loire - WegogreenR`,
        description: ` Le Pays de la Loire est la région idéale pour un séjour dépaysant et ressourçant en solo ou avec toute la tribu. `
      }
      break
    case 'provence-alpes-cote-azur':
      resp = {
        metaTitle: `Provence-Alpes-Côte d'Azur`,
        searchTitle: `Provence-Alpes-Côte d'Azur`,

        title: `Location d'hébergements Écolo & Responsables en Provence-Alpes-Côte d'Azur - WegogreenR`,
        description: `Partez à la rencontre des sites incontournables de la Provence-Alpes-Côte d’Azur et vivez des expériences exceptionnelles et ressourçante entre mer et montagnes.`
      }
      break

    default:
      resp = {
        metaTitle: `Nos hébergements`,
        searchTitle: `We Go GreenR`,

        title: `We Go GreenR, des voyages qui ont du sens`,
        description: `Séjournez dans des lieux de qualité pour des vacances et des weekends éco-responsables en France.`
      }
      break
  }
  return resp
}

export default handleSEO
