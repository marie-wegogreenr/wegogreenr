export const DEFAULT_LOCATION = {
  lat: 48.856614,
  lng: 2.3522219,
  page: 1,
  locationName: 'París, Francia',
  initial_date: '',
  final_date: '',
  travellers_quantity: 0,
  nelat: 49.29288913249991,
  nelng: 2.8083801269531254,
  swlat: 48.41735304952526,
  swlng: 1.8951416015625002
}
