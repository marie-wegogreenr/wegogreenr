export const exampleData = [
  {
    basic_price: 230,
    public_name: 'Emplacement tent',
    people_capacity: 3,
    establishment: {
      city_name: `Lyon`,
      region_name: `Auvergne-Rhône-Alpes`,
      green_score: {
        score: 50
      }
    },
    room_image: [
      {
        image: {
          url: `https://www.plateamagazine.com/images/prueba/foto2_400x300.jpg`
        }
      }
    ]
  },
  {
    basic_price: 180,
    public_name: 'Chambre Tent',
    people_capacity: 2,
    establishment: {
      city_name: `Grenoble`,
      region_name: `Auvergne-Rhône-Alpes`,
      green_score: {
        score: 80
      }
    },
    room_image: [
      {
        order: 1,
        image: {
          url: `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSX9bXOLRPiTS6GUApoD_hhK8mJ5LlzvC4WEFJxjtSSzE4A_uO49V7-hNDCt_h_ly3ot1s&usqp=CAU`
        }
      }
    ]
  },
  {
    basic_price: 99.99,
    public_name: 'Tipi a montagne',
    people_capacity: 2,
    establishment: {
      city_name: `Saint-Étienne`,
      region_name: `Auvergne-Rhône-Alpes`,
      green_score: {
        score: 100
      }
    },
    room_image: [
      {
        order: 1,
        image: {
          url: `https://cdn.dribbble.com/users/7149761/screenshots/15243893/media/67d9ddd85c1681acd1f62e96c6e5dad9.jpg?compress=1&resize=400x300&vertical=top`
        }
      }
    ]
  },
  {
    basic_price: 199.99,
    public_name: 'Modern arabic',
    people_capacity: 4,
    establishment: {
      city_name: `Chambéry`,
      region_name: `Auvergne-Rhône-Alpes`,
      green_score: {
        score: 120
      }
    },
    room_image: [
      {
        order: 1,
        image: {
          url: `https://i.pinimg.com/474x/5b/75/c8/5b75c8a3f6035a077e4d461c5cbba8fc.jpg`
        }
      }
    ]
  },
  {
    basic_price: 249.99,
    public_name: 'Beautiful room',
    people_capacity: 4,
    establishment: {
      city_name: `Chambéry`,
      region_name: `Auvergne-Rhône-Alpes`,
      green_score: {
        score: 75
      }
    },
    room_image: [
      {
        order: 1,
        image: {
          url: `https://timesofindia.indiatimes.com/thumb/msid-79761421,imgsize-406357,width-400,resizemode-4/79761421.jpg`
        }
      }
    ]
  },
  {
    basic_price: 88.99,
    public_name: 'Shared room',
    people_capacity: 5,
    establishment: {
      city_name: `Ardèche`,
      region_name: `Auvergne-Rhône-Alpes`,
      green_score: {
        score: 180
      }
    },
    room_image: [
      {
        order: 1,
        image: {
          url: `https://housinganywhere.imgix.net/room/1758697/1054870f-5868-458d-bff5-c7e027afe8c4.jpg?ixlib=react-9.2.0&auto=format&h=300&w=490`
        }
      }
    ]
  }
]

export const exampleActivitiesData = [
  {
    basic_price: 230,
    public_name: 'Activité du lac',
    duration: 2,
    establishment: {
      city_name: `Lyon`,
      region_name: `Auvergne-Rhône-Alpes`
    },
    room_image: [
      {
        image: {
          url: `https://www.france-voyage.com/visuals/photos/ferte-mace-sports-recreation-park-19207_w400.webp`
        }
      }
    ]
  },
  {
    basic_price: 180,
    public_name: 'Rapel',
    duration: 1,
    establishment: {
      city_name: `Grenoble`,
      region_name: `Auvergne-Rhône-Alpes`
    },
    room_image: [
      {
        order: 1,
        image: {
          url: `https://media.manawa.com/cache/activity_header/media/2016/04/7f1c1e56ecd52a75e0b8002bdbb2eb8b.png`
        }
      }
    ]
  },
  {
    basic_price: 99.99,
    public_name: 'Obstacles',
    duration: 2,
    establishment: {
      city_name: `Saint-Étienne`,
      region_name: `Auvergne-Rhône-Alpes`
    },
    room_image: [
      {
        order: 1,
        image: {
          url: `https://i.pinimg.com/600x315/4c/b2/88/4cb2885a7411d5801b52605699868689.jpg`
        }
      }
    ]
  },
  {
    basic_price: 199.99,
    public_name: 'Hiking',
    duration: 1,
    establishment: {
      city_name: `Chambéry`,
      region_name: `Auvergne-Rhône-Alpes`
    },
    room_image: [
      {
        order: 1,
        image: {
          url: `https://conservemc.org/wp-content/uploads/2020/04/71601b6fd7fc74a9f4eea8e6c1b43d35.jpg`
        }
      }
    ]
  },
  {
    basic_price: 249.99,
    public_name: 'Balloon',
    duration: 2,
    establishment: {
      city_name: `Chambéry`,
      region_name: `Auvergne-Rhône-Alpes`
    },
    room_image: [
      {
        order: 1,
        image: {
          url: `https://bloximages.newyork1.vip.townnews.com/waow.com/content/tncms/assets/v3/editorial/8/a9/8a95c50e-e2d9-5d61-9a07-8087913cf357/6140be6b9a219.image.jpg?resize=400%2C300`
        }
      }
    ]
  },
  {
    basic_price: 88.99,
    public_name: 'Natural park',
    duration: 1,
    establishment: {
      city_name: `Ardèche`,
      region_name: `Auvergne-Rhône-Alpes`
    },
    room_image: [
      {
        order: 1,
        image: {
          url: `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSaeKhGGq3lQzOYi4cwK8Ob7P-iHDN_pqFFaQ&usqp=CAU`
        }
      }
    ]
  }
]
