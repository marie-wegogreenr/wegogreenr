import { isSomeItemChecked } from 'helpers/abracadaroomHelpers'
import {
  activateRooms,
  changeAbracadaroomStatus,
  updateGreenScore
} from 'services/abracadaroomServices'
import { highlightHebergement } from 'services/hebergementsService'

export const filterTypes = [
  {
    name: 'Équipements',
    label: 'equipments'
  },
  {
    name: 'A faire sur place',
    label: 'activities'
  },
  {
    name: 'Type de logement',
    label: 'types'
  },
  {
    name: 'Prix',
    label: 'max_price'
  },
  {
    name: 'Thématique',
    label: 'thematic'
  },
  {
    name: 'Label',
    label: 'label'
  }
]

export const greenScoreTypes = [
  { text: '-', value: '' },
  { text: `Baby GreenR`, value: 1 },
  { text: `GreenR en Herbe`, value: 2 },
  { text: `GreenR en ascension`, value: 3 },
  { text: `Captain GreenR`, value: 4 },
  { text: `GreenR au sommet`, value: 5 }
]

export const greenScoreAbracadaroom = {
  id: 'greenscore',
  label: `Green Score`,
  data: [
    { text: `(1) Baby GreenR`, value: 1 },
    { text: `(2) GreenR en Herbe`, value: 2 },
    { text: `(3) GreenR en ascension`, value: 3 },
    { text: `(4) Captain GreenR`, value: 4 },
    { text: `(5) GreenR au sommet`, value: 5 }
  ]
}

export const optionsStablishments = {
  active: {
    id: 'active',
    label: `Activé`,
    data: [
      { text: '-', value: '' },
      { text: 'Oui', value: 1 },
      { text: 'Non', value: 0 }
    ]
  },
  greenscore: {
    id: 'greenscore',
    label: `Green Score`,
    data: greenScoreTypes
  },
  origin: {
    id: 'origin',
    label: `Origine`,
    data: [
      { text: '-', value: '' },
      { text: 'Test1', value: 0 },
      { text: 'WGG', value: 1 },
      { text: 'Abracadaroom', value: 2 },
      { text: 'Test2', value: 3 },
      { text: 'Alliance Reseaux', value: 4 }
    ]
  }
}

export const optionsReservations = {
  id: `status`,
  label: `Statut`,
  data: [
    { text: '-', value: '' },
    { text: 'En attente de paiement', value: 0 },
    { text: 'Approuvé', value: 1 },
    { text: 'Annulée par le voyageur', value: 2 },
    { text: `En attente d'approbation`, value: 3 },
    { text: `Annulée par l'hôte`, value: 4 },
    { text: `Annulée par admin`, value: 5 }
  ]
}

export const optionsRols = {
  id: 'role',
  label: 'Role',
  data: [
    { text: '-', value: '' },
    { text: 'Voyageur', value: 1 },
    { text: 'Hôte', value: 2 },
    { text: 'Admin', value: 3 }
  ]
}

export const optionsSelectAbracadaroom = [
  { text: `Sélectionnez`, value: `void` },
  {
    text: `Publier`,
    value: `publish`,
    callback: activateRooms,
    check: isSomeItemChecked(),
    body: {
      atrribute: 'activate',
      value: 1
    },
    changeStatus: {
      atrribute: '',
      value: ''
    }
  },
  {
    text: `Dépublier`,
    value: `unpublish`,
    callback: activateRooms,
    check: isSomeItemChecked(),
    body: {
      atrribute: 'activate',
      value: 0
    },
    changeStatus: {
      atrribute: '',
      value: ''
    }
  },
  {
    text: `Changement de statut`,
    value: `change Status`,
    callback: changeAbracadaroomStatus,
    check: isSomeItemChecked(),
    body: {
      atrribute: '',
      value: ''
    },
    changeStatus: {
      atrribute: '',
      value: ''
    }
  },
  {
    text: `Changement greenscore`,
    value: `change greenscore`,
    callback: updateGreenScore,
    check: isSomeItemChecked(),
    body: {
      atrribute: '',
      value: ''
    },
    changeStatus: {
      atrribute: '',
      value: ''
    }
  },
  {
    text: `Surligner`,
    value: `mark`,
    callback: highlightHebergement,
    check: true,
    body: {
      atrribute: '',
      value: ''
    },
    changeStatus: {
      atrribute: '',
      value: ''
    }
  },
  {
    text: `Ne pas souligner`,
    value: `unmark`,
    callback: highlightHebergement,
    check: true,
    body: {
      atrribute: '',
      value: '',
      changeStatus: {
        atrribute: '',
        value: ''
      }
    }
  }
]

export const optionsFilterAbracadaroom = {
  id: 'status',
  label: 'Statut',
  data: [
    { text: '-', value: '' },
    { text: 'nouveau', value: 1 },
    { text: 'en ligne', value: 2 },
    { text: 'hors ligne', value: 3 },
    { text: 'archivé', value: 4 }
  ]
}
