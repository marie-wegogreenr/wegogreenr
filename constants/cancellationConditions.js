export const annulationQuestions = {
  question1:
    'Jusqu’à quel moment les voyageurs peuvent-ils annuler leur séjour gratuitement ?',
  question2:
    'Quel montant souhaitez-vous facturer aux voyageurs pour une annulation tardive ?'
}

export const fixedAnswer = `La réservation est remboursée intégralement, moins 2% pour couvrir les frais de notre prestataire de paiement.`
export const fixedAnswerForUser = `Votre réservation est remboursée intégralement, moins 2% pour couvrir les frais de notre prestataire de paiement.`

export const fixedAnswerByDefault = {
  option1: `Annulation gratuite jusqu’à J-7 (avec retenue de 2% pour les frais
    bancaires, donc remboursement de 98%)`,
  option2: `Non remboursable à moins de 7 jours avant le début du séjour ou
  non-présentation`
}

export const optionsQuestion1 = [
  { key: 1, option: 'Très flexibles - 1 jour' },
  { key: 7, option: 'Flexibles - 7 jours' },
  { key: 14, option: 'Modérées - 14 jours' },
  { key: 30, option: 'Strictes - 30 jours' },
  { key: 60, option: 'Très strictes - 60 jours' }
]

export const optionsQuestion2 = [
  { key: 25, option: '25% du montant total de la réservation (choix 1)' },
  { key: 50, option: '50% du montant total de la réservation (choix 2)' },
  { key: 75, option: '75% du montant total de la réservation (choix 3)' },
  { key: 100, option: '100% du montant total de la réservation (choix 4)' }
]

export const textCovid = `Remboursement intégral jusqu'à 24h avant la date d'arrivée pour toute incapacité de déplacement liée à une annonce officielle en rapport avec la COVID-19.`

export const textsCheckout = {
  title: `Parce que vous avez le droit de changer d’avis, nos conditions
  d’annulation sont pensées pour vous :`,
  textCovid: `Chez nous, le remboursement est intégral jusqu’à 24h avant la date
  d’arrivée pour toute incapacité de déplacement liée à une annonce
  officielle en rapport avec la COVID19 !`
}
