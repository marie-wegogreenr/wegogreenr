export const handleRegions = (path) => {
  let resp = {}
  switch (path) {
    case 'auvergne-rhone-alpes':
      resp = {
        id: 1,
        name: 'Auvergne-Rhône-Alpes',
        country_id: 1,
        lng: 4.53806,
        lat: 45.5158
      }
      break
    case 'bourgogne-franche-comte':
      resp = {
        id: 2,
        name: 'Bourgogne-Franche-Comté',
        country_id: 1,
        lng: 4.80917,
        lat: 47.2353
      }
      break
    case 'bretagne':
      resp = {
        id: 3,
        name: 'Bretagne',
        country_id: 1,
        lng: -2.8386,
        lat: 48.1797
      }
      break
    case 'centre-val-de-loire':
      resp = {
        id: 4,
        name: 'Centre-Val de Loire',
        country_id: 1,
        lng: 1.7853,
        lat: 47.4806
      }
      break
    case 'corse':
      resp = { id: 5, name: 'Corse', country_id: 1, lng: 9.10528, lat: 42.1497 }
      break
    case 'grand-est':
      resp = {
        id: 6,
        name: 'Grand Est',
        country_id: 1,
        lng: 5.65994,
        lat: 48.6988
      }
      break
    case 'hauts-de-france':
      resp = {
        id: 7,
        name: 'Hauts-de-France',
        country_id: 1,
        lng: 2.78473,
        lat: 50.0213
      }
      break
    case 'ile-de-france':
      resp = {
        id: 8,
        name: 'Île-de-France',
        country_id: 1,
        lng: 2.50472,
        lat: 48.7092
      }
      break
    case 'normandie':
      resp = {
        id: 9,
        name: 'Normandie',
        country_id: 1,
        lng: 0.106667,
        lat: 49.1211
      }
      break
    case 'nouvelle-aquitaine':
      resp = {
        id: 10,
        name: 'Nouvelle-Aquitaine',
        country_id: 1,
        lng: 0.122403,
        lat: 45.3191
      }
      break
    case 'occitanie':
      resp = {
        id: 11,
        name: 'Occitanie',
        country_id: 1,
        lng: 2.26428,
        lat: 43.7915
      }
      break
    case 'pays-de-la-loire':
      resp = {
        id: 12,
        name: 'Pays de la Loire',
        country_id: 1,
        lng: -0.823889,
        lat: 47.4747
      }
      break
    case 'provence-alpes-cote-azur':
      resp = {
        id: 13,
        name: 'provence-alpes-cote-azur',
        country_id: 1,
        lng: 6.05333,
        lat: 43.955
      }
      break
    case 'martinique':
      resp = {
        id: 14,
        name: 'Martinique',
        country_id: 1,
        lng: 60.9959,
        lat: 14.6719
      }
      break
    case 'guadeloupe':
      resp = {
        id: 15,
        name: 'Guadeloupe',
        country_id: 1,
        lng: -61.5756,
        lat: 16.2536
      }
      break
    case 'la-reunion':
      resp = {
        id: 16,
        name: 'La Réunion',
        country_id: 1,
        lng: 0.123543,
        lat: 44.2859
      }
      break
    case 'guyane':
      resp = {
        id: 17,
        name: 'Guyane',
        country_id: 1,
        lng: -53.119,
        lat: 4.10799
      }
      break
    case 'mayotte':
      resp = {
        id: 18,
        name: 'Mayotte',
        country_id: 1,
        lng: 45.1633,
        lat: -12.7985
      }
      break
    default:
      resp = { id: 19, name: 'Other', country_id: 1, lng: null, lat: null }
      break
  }
  return resp
}
