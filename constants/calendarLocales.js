const MONTHS = [
  'janvier',
  'février',
  'mars',
  'avril ',
  'mai',
  'juin',
  'juillet',
  'août',
  'septembre',
  'octobre',
  'novembre',
  'décembre'
]

const WEEKDAYS_SHORT = ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa']

export { WEEKDAYS_SHORT, MONTHS }
