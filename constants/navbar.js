export const optionsNavbar = [
  {
    btnText: `Devenir hôte`,
    url: '/devenir-hote'
  },
  {
    btnText: `Concept`,
    url: '/demarche'
  },
  {
    btnText: `GreenScore`,
    url: '/green-score'
  },
  {
    btnText: `Press`,
    url: '/press'
  }
]
