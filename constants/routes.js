const routes = {
  HOME: '/',
  HOST_DASHBOARD: '/host/dashboard',
  HOST_ESTABLISHMENT: '/host/etablissement',
  HOST_MY_ACTIVITIES: '/host/mes-ativites',
  HOST_MY_HEBERGEMENT_BY_ID: '/host/mes-hebergements/[id]',
  ADMIN_MY_HEBERGEMENT_BY_ID: '/admin/hebergements/[id]',
  HOST_MY_HEBERGEMENTS: '/host/mes-hebergements',
  HOST_MY_MESSAGERS: '/host/mes-messager',
  HOST_MY_RESERVATIONS: '/host/mes-reservations',
  USER_DASHBOARD: '/user/dashboard',
  USER_MY_RESERVATIONS: '/user/mes-reservations',
  USER_MY_CONVERSATIONS: '/user/mes-conversations',
  ADMIN_DASHBOARD: '/admin',
  ADMIN_ETABLISHMENT: '/admin/etablissement',
  ADMIN_ETABLISHMENT_BY_ID: '/admin/etablissement/[id]',
  ADMIN_MY_HEBERGEMENTS: '/admin/hebergements',
  ADMIN_MY_RESERVATIONS: '/admin/reservations',
  LOGIN: '/login',
  SEARCHED_ROOMS: '/search',
  SINGLE: '/hebergements/[slug]'
}

export default routes
