const statuses = {
  FAILURE: 'FAILURE',
  IDLE: 'IDLE',
  INIT: 'INIT',
  LOADING: 'LOADING',
  SUCCESS: 'SUCCESS'
}

export default statuses
