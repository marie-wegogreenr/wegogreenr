const handleSearchTexts = (path) => {
  let resp = {}
  switch (path) {
    case 'chambre-hote':
      resp = {
        type: 'type',
        searchTitle: 'nos chambres d’hôtes éco-responsables',
        text: `La chambre d’hôtes est le type de location qui convient parfaitement pour des vacances entre partage et tradition : solo, avec son compagnon ou sa petite famille. La petite capacité des chambres d’hôtes permet généralement d’accueillir entre 1 et 4 personnes en toute intimité. Les voyageurs ont à leur disposition une chambre confortable, ainsi qu’un espace de détente collectif et partagé, et parfois même des installations extérieures telles qu’un jardin, une piscine ou un barbecue permettant à chacun de se reposer au vert après une journée de vadrouille. Le petit-déjeuner est généralement compris dans la nuitée ce qui nous permet de vraiment profiter des vacances en toute farniente.\n\n Contrairement aux <b><a href="${process.env.NEXT_PUBLIC_URL}/hotel">chambres d’hôtels</a></b>, les locations de vacances écologiques de type chambre d’hôtes nous permettent de favoriser le partage et d’en apprendre plus sur les cultures, origines et modes de vie de chacun. On choisit généralement une chambre chez l’habitant pour des séjours immersifs, qui favorisent le contact humain, dans le but de découvrir les traditions et savoir-faire de nos hôtes et de faire des activités inattendues. We Go GreenR a minutieusement choisi ses plus belles chambres d’hôtes en France pour vous permettre de passer des moments inoubliables. Détente, rencontre et partage seront les maîtres-mots de votre séjour dans une de nos chambres d’hôtes .`,

        questions: {
          rows: [
            {
              title: 'Comment fonctionne une chambre d’hôtes?',
              content: `La chambre d’hôtes est un hébergement à la nuitée qui propose les mêmes services qu’un <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">hôtel</a></b>: lit fait, forfait ménage, salle de bain, WC et généralement petit-déjeuner inclus. À la différence de l’hôtel, une chambre d’hôtes se situe directement chez l’habitant, il est donc nécessaire de respecter les règles de la maison. En plus du petit déjeuner et de la nuit, les hébergeurs peuvent vous proposer en supplément de prendre le repas avec eux, on parle alors de tables d’hôtes.`
            },
            {
              title: 'Pourquoi choisir de passer une nuit en chambre d’hôtes ?',
              content: `On choisit de passer une nuit en chambre d’hôtes pour séjourner dans un cadre plein de charme et de caractère, généralement à l’écart des villes et des endroits trop touristiques. Sur place, les propriétaires nous réservent un accueil chaleureux, authentique et unique qui passe par des petites attentions telles que des conseils de visites ou un panier de bienvenue. En plus de partager leurs maisons, les hôtes partagent leurs bonnes adresses, leurs histoires et leur petit-déjeuner fétiche et fait-maison. En résumé, on va dans une chambre d’hôtes pour vivre plus qu’un séjour, une expérience, inhabituelle et inoubliable.`
            },
            {
              title:
                'De quelle façon We Go GreenR sélectionne les propriétaires de ses chambres d’hôtes ?',
              content: `Les propriétaires de nos chambres d’hôtes se doivent de vous proposer un accueil unique et chaleureux dans le partage, l’écoute et la bienveillance. En plus de ces qualités essentielles, l’engagement écologique de nos hôtes est une priorité. Nous sélectionnons toujours des hébergeurs qui mettent en place des actions sérieuses pour une gestion efficace de leur empreinte écologique et qui font leur nécessaire pour proposer du slow tourisme. `
            }
          ]
        },
        redirections: [
          { title: 'Hôtel', link: 'hotel' },
          { title: 'Gîte', link: 'gite' },
          { title: 'Insolite ', link: 'insolite' }
        ]
      }
      break
    case 'hotel':
      resp = {
        type: 'type',
        searchTitle: 'Nos hôtels éco-responsables',
        text: `L’Hôtel est le type de location qui convient parfaitement pour des vacances détente : seul, en couple ou avec sa petite famille. Accueillant en général entre 1 et 4 personnes par chambre, les hôtels nous permettent de se créer des souvenirs en petit comité. Dotés d’une literie de qualité, d’une salle de bain et de toilettes fonctionnels, d’une prestation ménage et parfois même d’un petit coin cuisine, une chambre en hôtel nous procure ce sentiment de se sentir « réellement en vacances ». Les voyageurs ont à leur disposition une chambre ou une suite toute équipée et confortable, ce qui leur permet de voyager sans se prendre la tête. \n\n Contrairement aux <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">chambres d’hôtes</a></b>  ou aux <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">gîtes</a></b>, les locations de vacances écologiques de type hôtel sont avant tout pratiques, on y va surtout parce qu’on a juste à poser ses valises, qu’ils sont plus abondants et généralement mieux situés que les autres hébergements de vacances. We Go GreenR a sélectionné les plus beaux hôtels écologiques de France pour vous permettre de passer des moments inoubliables. On sélectionne un hôtel haut de gamme avec piscine, spa et vue sur la mer pour se faire plaisir, un hôtel confort avec petit déjeuner inclus pour un séjour sans prise de tête et un hôtel en plein centre-ville pour sa localisation parfaite pour visiter les alentours.`,
        questions: {
          rows: [
            {
              title:
                'Comment We Go GreenR sélectionne ses hôtels écoresponsables ?',
              content: `Pour sélectionner nos hôtels écoresponsables, nous avons mis en place un indice nommé « GreenScore ». Avec cet indicateur nous mesurons si les établissements que nous avons sélectionnés mettent en place des actions concrètes pour une gestion raisonnée de leurs hôtels et pour préserver la planète. Pour pouvoir être proposé aux voyageurs, chaque hôtel doit recevoir la note minimum de 1/5 au Greenscore, plus son engagement est important et plus sa note progresse. Le Green Score est disponible sur chaque page établissement et consultable facilement par tout le monde.`
            },
            {
              title: 'Quels sont les équipements des hôtels eco-friendly?',
              content: `Si nos hôtels sont tous équipés d’une literie de qualité, d’une douche, d’un WC et la plupart du temps de la télévision.  Pour que nos hôtels soient qualifiés d’écoresponsables les hébergeurs doivent mettre en place un certain nombre d’aménagements tels qu’utiliser des produits d’entretien et d’hygiène écologiques, favoriser la mobilité douce, proposer une literie biologique, préparer un petit déjeuner à base de produits locaux et de saison …`
            },
            {
              title:
                'De quelle manière We Go GreenR sélectionne les propriétaires de ses hôtels ?',
              content: `We Go GreenR a minutieusement sélectionné des hôtels où les propriétaires sont éco-responsables et prennent soin de contrôler leur impact environnemental. Ils mettent en place des actions au quotidien pour développer un tourisme plus green. Leur accueil doit être également irréprochable et de qualité pour que vos vacances soient mémorables.`
            }
          ]
        },
        redirections: [
          { title: 'Chambre d’hôtes', link: 'chambre-hote' },
          { title: 'Gîte', link: 'gite' },
          { title: 'Insolite ', link: 'insolite' }
        ]
      }
      break
    case 'ecolodge':
      resp = {
        type: 'type',
        searchTitle: 'Notre sélection d’écolodges',
        text: `L’écolodge est le type de location qui convient parfaitement pour des vacances au cœur de la nature en couple, avec ses amis ou sa famille. La plupart des écolodges ont des capacités variables pouvant accueillir entre 2 et 50 personnes. Afin de limiter leur impact sur l’environnement, les écolodges possèdent souvent des formes insolites, un système de gestion des déchets et des composants peu énergivores pour s’insérer dans le cadre où ils sont construits. Dotés d’équipements de première nécessité, sans lésiner sur le confort, les écolodges nous font vivre une expérience unique même en vacances. Les voyageurs ont à leur disposition de nombreuses ressources permettant de limiter l’impact de leurs vacances sur la planète et de sensibiliser toute la famille au tourisme durable.\n\n Contrairement aux <b><a href="${process.env.NEXT_PUBLIC_URL}/gites">gîtes</a></b> et <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">aux chambres d’hôtes</a></b>, les locations de vacances écologiques de type écolodge doivent forcément s’insérer dans leur milieu, afin de préserver la faune et la flore locale. On choisit généralement l’écolodge pour déconnecter de son quotidien, sortir de sa routine et vivre un véritable voyage vers une nouvelle aire. We Go GreenR a sélectionné les plus beaux écolodges de France pour vous permettre de passer des moments inoubliables : bulle, dojo, guest house, cabane dans la forêt, tentes lodges, tiny house…. Et plein d’autres hébergements remplis de charme vous permettront de vivre des vacances mémorables.`,
        questions: {
          rows: [
            {
              title: 'Qu’est-ce qu’une écolodge  ?',
              content: `Une écolodge  est un hébergement qui a pour objectif de préserver la faune et la flore, en s’insérant parfaitement dans son environnement de sa création à son utilisation par les touristes. Un hébergeur possédant une écolodge se doit de privilégier l’économie locale, d’inciter les voyageurs à respecter la nature en limitant la production de déchets et de maîtriser la consommation d’énergie plutôt renouvelable de son hébergement.`
            },
            {
              title: 'Quels sont les équipements d’une écolodge ?',
              content: `La majorité des écolodge possèdent au minimum un lit, des toilettes, une salle de bain et un extérieur. Mais beaucoup de nos hébergeurs proposent également un coin cuisine, un bain nordique, un hamac, un barbecue… En plus de cela, des aménagements tels que des nichoirs, des lampes solaires, un compost, un potager, une literie naturelle et biologique sont régulièrement mis en place par nos hôtes toujours dans l’objectif de renforcer leurs engagements environnementaux.`
            },
            {
              title: 'Comment sélectionne-t-on nos écolodges  ?',
              content: `L’ensemble des écolodges que vous retrouvez sur notre site ont été mis à l’épreuve du Greenscore, un indicateur créé par We Go GreenR, estimant les actions en faveur de l’environnement menées au quotidien par l’ensemble des hôtes. C’est une manière pour tous, de s’assurer que son séjour sera bien écoresponsable et en accord avec les valeurs du slow tourisme.`
            }
          ]
        },
        redirections: [
          { title: 'Chambre d’hôtes', link: 'chambre-hote' },
          { title: 'Gîte', link: 'gite' },
          { title: 'Insolite ', link: 'insolite' }
        ]
      }
      break
    case 'insolite':
      resp = {
        type: 'type',
        searchTitle: 'Nos hébergements insolites  et éco-responsables',
        text: `L’insolite est le type de location qui convient parfaitement pour des vacances étonnantes : en couple, avec ses amis ou sa famille. Divers et variés, les logements insolites sont des alternatives aux locations de vacances traditionnelles. Les vacances sont avant tout expérientielles, sans même partir loin, on ressent un dépaysement total par rapport à notre quotidien.  Vivre un moment hors du commun, est le maître-mot du séjour. Les voyageurs ont à leur disposition un logement entier, généralement bien équipé, souvent localisé en pleine nature et possédant de nombreuses installations pour profiter de l’extérieur en été. En Hiver, ce type d’hébergement est parfait pour casser la routine qui s’est installée depuis la rentrée et admirer les paysages alentour.\n\n Contrairement aux <b><a href="${process.env.NEXT_PUBLIC_URL}/hotel">hôtels</a></b>  et aux <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">gîtes</a></b> , les locations de vacances écologiques de type insolite permettent de sortir des sentiers battus. On choisit généralement un logement insolite pour des séjours de courte durée, pour vivre une expérience et se ressourcer le temps d’un week-end.  On sélectionne une bulle pour une soirée romantique en amoureux, une cabane dans les arbres pour vivre son rêve d’enfant, une yourte pour rapprocher toute la famille en se reconnectant à la nature…`,
        questions: {
          rows: [
            {
              title:
                'Pourquoi passer une nuit dans un logement insolite et écologique ?',
              content: `Nous vous conseillons de passer une nuit dans un de nos logements écologiques, si vous êtes en manque d’aventure et que vous souhaitez casser votre routine. Dormir dans un de nos établissements insolites et écologiques permet de vivre une expérience unique, qui chamboule nos habitudes et nous permet de vraiment s’évader, en respectant l’environnement.`
            },
            {
              title: 'Qu’est-ce qu’un logement insolite ?',
              content: `Un logement insolite est un logement qui casse les codes, exit les matériaux traditionnels, bonjour la surprise et l’étonnement. L’architecture n’est plus typique, ni classique mais pensée pour nous étonner et nous surprendre. Dormir dans un hébergement insolite, c’est somnoler dans une roulotte, admirer les étoiles dans une bulle, se réchauffer en famille au cœur d’une yourte, passer la nuit d’halloween dans une cabane de sorcière et pleins d’autres expériences mémorables.`
            },
            {
              title:
                'Comment We Go GreenR sélectionne-t-il ses locations de vacances insolites et eco-friendly ?',
              content: `Pour sélectionner l’ensemble de ses hébergements insolites We Go GreenR se base sur le GreenScore. Un indicateur conçu par l’équipe We Go GreenR pour mesurer et sélectionner les hébergements les plus engagés dans la transition écologique. Pour cela, chaque établissement doit prouver ses actions en termes de durabilité, dans la gestion de son eau et de son énergie, dans la réduction de ses déchets, dans les transports, la mise en place d’actions éco-responsables et l’alimentation qu’il propose.`
            }
          ]
        },
        redirections: [
          { title: 'Chambre d’hôtes', link: 'chambre-hote' },
          { title: 'Gîte', link: 'gite' },
          { title: 'Hôtel ', link: 'hotel' }
        ]
      }
      break
    case 'gite':
      resp = {
        type: 'type',
        searchTitle: 'Nos gites éco-responsables',
        text: `Le gîte est le type de location qui convient parfaitement pour des vacances en toute liberté avec ses amis ou sa famille. La grande capacité des gîtes permet en effet de recevoir de nombreuses personnes en toute convivialité. Dotés de nombreux équipements et généralement tout confort, les gîtes nous donnent l’impression « d’être comme chez nous », même en vacances. Les voyageurs ont à leur disposition un logement entier souvent pourvu d’un jardin, d’un barbecue et de nombreuses installations pour profiter de l’extérieur en été. En Hiver, ce type d’hébergement est parfait pour se réchauffer au coin du feu, autour d’un bon repas en toute convivialité avec les personnes qu’on aime. \n\n Contrairement aux <b><a href="${process.env.NEXT_PUBLIC_URL}/ecolodge">écolodges</a></b>  et <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">aux chambres d’hôtes</a></b>, les locations de vacances écologiques de type gite permettent de vivre ses séjours dans le partage et l’intimité. On choisit généralement le gite pour des séjours de longue durée afin d’avoir le temps de s’y installer et d’y vivre son voyage pleinement.  We Go GreenR a sélectionné les plus beaux gites écologiques de France pour vous permettre de passer des moments inoubliables. On sélectionne un gîte avec piscine pour se détendre avec ses amis, un gîte  rural et à grande capacité pour des retrouvailles en famille et un gîte  atypique et plein de charme pour vivre des vacances mémorables.`,
        questions: {
          rows: [
            {
              title:
                'Comment We Go GreenR sélectionne ses gîtes écoresponsables ?',
              content: `Pour sélectionner nos gîtes écoresponsables, nous avons mis en place le « GreenScore » pour mesurer et qualifier l’engagement éco-responsable réel des hébergements présents sur notre site. Positionné sur toutes les pages d'hébergement, il est symbolisé par une barre de progression vous permettant de visualiser très simplement leur niveau d'implication. Il est gratuit et obligatoire pour permettre à l’hébergement d’être référencé. Ainsi, les hébergements n’atteignant pas la note minimale de 1 sur 5 ne sont pas sélectionnés sur notre site.`
            },
            {
              title: 'Quels sont les équipements des gîtes eco-friendly ?',
              content: `En plus des équipements classiques d’<b><a href="${process.env.NEXT_PUBLIC_URL}/gite">un gîte</a></b>  : coin cuisine, salle de bain, WC, chambres et la plupart du temps jardin. Pour que nos gites soient qualifiés d’écoresponsables les hébergeurs doivent mettre en place un certain nombre d’aménagements tels que des économiseurs d’eau, des ampoules à économie d’énergie, posséder un environnement pollinisateur, utiliser des produits d’entretien et d’hygiène écologiques…`
            },
            {
              title:
                'Comment We Go GreenR sélectionne les propriétaires de ses gîtes ?',
              content: `We Go GreenR sélectionne des gîtes où les propriétaires ont mis l’écologie au cœur de leurs priorités. Ils mettent en place des actions en faveur du tourisme écologique et durable. En plus d’être accueillant et chaleureux, ils sont passionnés et souhaitent transmettre l’ensemble de leurs savoir-faire avec vous. `
            }
          ]
        },
        redirections: [
          { title: 'Chambre d’hôtes', link: 'chambre-hote' },
          { title: 'Hôtel ', link: 'hotel' },
          { title: 'Insolite ', link: 'insolite' }
        ]
      }
      break
    case 'auvergne-rhone-alpes':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Auvergne-Rhône-Alpes',
        text: `Entre montagnes et lacs, la région Auvergne- Rhône-Alpes est la destination préférée des amoureux de randonnée et de loisirs en plein air. Que ce soit pour ses paysages de hautes-montagnes, ses grandes métropoles culturelles ou ses fleuves majestueux, le territoire permet à tous de lâcher prise et de profiter de la nature. Depuis 2018 la région s’engage pour un tourisme bienveillant en valorisant un tourisme durable, incitant aux mobilités douces en créant du lien entre les habitants de la région et les visiteurs.\n\n Partez à la découverte des stations thermales Ardéchoises : Saint-Laurent-les-Bains, Neyrac-les-Bains et Vals les Bains pour passer des vacances sur le thème du repos et de la relaxation, en logeant dans l’un de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos hôtels eco-friendly et de charme</a></b> du département.  Dans le Cantal, on s’endort dans un de <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">nos gîtes éco-responsables</a></b>  près des parcs naturels régionaux des volcans d’Auvergne : Plomb du Cantal, Puy Mary, Griou, Violent et Puys de Chavaroche . Envie d’être un peu plus au calme ? On vous propose de vous détendre en vivant une expérience unique dans un de <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">nos logements insolites et eco-friendly</a></b> en Haute-Loire et d’être émerveillé par des paysages rayonnants : Mézenc, Vivarais-Lignon, le lac du Bouchet et la Cascade de la Baume.`,
        questions: {
          rows: [
            {
              title:
                'Témoignage de nos hôtes : Pourquoi passer ses futures vacances en Auvergne-Rhône-Alpes ?',
              content: `Janette,<b><a href="${process.env.NEXT_PUBLIC_URL}/hebergements/eco-gite-le-vallon-darmandine-1">hôte du Vallon d'Amandine</a></b> nous parle de sa région : « L’Auvergne-Rhône-Alpes est une région qui a su préserver son identité, je suis très fière de voir que sa population y est donc très attachée. Ses espaces naturels et le volcanisme sont deux aspects très présents dans la région et selon moi, c’est ce qui la différencie des autres. Les paysages que l’on trouve ici sont uniques et on ne pourra pas les trouver ailleurs en France et en Europe !  Quand on me demande quel est le plus joli village de l’Auvergne, j’ai souvent du mal à y répondre car la région compte de nombreux villages authentiques à découvrir. Mais si je devais en choisir un seul, ce serait le village de Montpeyroux qui n’est pas très loin de chez moi. Il se trouve à la limite entre le Puy-de-Dôme et la Haute-Loire. On ne peut pas le louper, il se trouve sur un petit mont, ce qui nous permet de le voir de loin et heureusement car il est classé comme l’un des plus beaux villages de France ! »`
            },
            {
              title:
                'Un voyage en Auvergne-Rhône-Alpes, pour quel type de séjour ?',
              content: `Pour les amateurs de randonnées ou de VTT, l'Auvergne-Rhône-Alpes est une excellente destination. Ses paysages montagneux offrent une vraie façon de se dépasser. Pour ceux qui veulent prendre le temps de profiter de leur voyage et apprécier les grands espaces verts, l'Auvergne-Rhône-Alpes est un vrai coin de paradis ! Venir en en famille ou entre amis c’est découvrir une région féerique et des activités en plein air qui plairont aux petits comme aux grands. Pour créer des souvenirs, la région propose différents cadres. Des activités à sensations fortes ou des randonnées pour les sportifs du dimanche, vous aurez le choix pour votre séjour ! De plus, les différentes spécialités de la région sauront mettre les papilles de tout le monde d’accord.`
            },
            {
              title:
                'Quelles sont les villes incontournables à visiter lors du voyage ?',
              content: `Lyon et son centre historique inscrit au patrimoine mondial de l’UNESCO sont à ne pas manquer. Découvrez le Vieux Lyon, le parc de la Tête d’Or, ou les berges du Rhône pour un city tour à couper le souffle. Vous pourrez aussi monter au sommet de la colline de Fourvière (à pied ou en funiculaire) pour profiter d’une incroyable vue sur toute la ville. La seconde ville immanquable est bien évidemment Annecy : ressourcez-vous dans celle que l’on surnomme la Venise des Alpes grâce à ses charmants canaux fleuris ou sa promenade au bord du lac. Une architecture d’époque, avec ses belles façades colorées et ses vieilles pierres vous fera voyager dans le temps. Pour mieux découvrir le lac et ses montagnes, détendez-vous grâce à la location de pédalos proposée aux visiteurs.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'bretagne':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Bretagne',
        text: `Entre balades et découvertes, la Bretagne vous propose de sortir des sentiers battus et de vivre un voyage qui a du sens ! La région aux multiples facettes est riche en destinations plus surprenantes les unes que les autres pour un vrai break au vert. Son patrimoine naturel unique se démarque grâce à un fort engagement dans une démarche de qualité, de durabilité et de respect de l’environnement. « Degemer mat ! »\n \nDirection le Morbihan pour vivre une nuit dans un <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">gîte éco-responsable</a></b> en bord de mer après avoir passé sa journée à explorer Belle-ile-en-Mer et ses criques paradisiaques. On va dormir dans une <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">chambre d’hôtes</a></b> en Ille-et-Vilaine pour admirer le joyau de la Côte Emeraude, autrement appelé Saint Malo. Nos <b><a href="${process.env.NEXT_PUBLIC_URL}/hotel">hôtels eco-friendly</a></b>  du Finistère vous accueilleront pour découvrir la ville close Concarneau, l'île aux multiples facettes d’Ouessant et l’un des plus beaux villages de France nommé Locronan. Et si on optait pour les Côtes d’Armor ? On y va durant le festival du chant des marins, la fête de la coquille Saint-Jacques ou tout simplement pour cueillir des champignons dans la forêt de Beffou. Avant de partir, on n’oublie pas de goûter aux galettes traditionnelles bretonnes, aux fameux sablés bretons et aux Kouign amann. Mais surtout, il serait dommage de terminer son séjour sans prendre un cours de cuisine pour apprendre à faire le meilleur caramel au beurre salé de France.`,
        questions: {
          rows: [
            {
              title:
                'Quelles activités faire en mode slow pendant son voyage en Bretagne ?',
              content: `Pour les amateurs de vélo, la Bretagne compte plus de 9 parcours de véloroutes et de voies vertes sur son territoire. Découvrez près de 2000 km d’itinéraires pour tous les niveaux. De cette manière, vous aurez la liberté de découvrir la région à votre rythme, en solo, en famille ou entre amis. Plutôt envie de profiter des côtes ?  Laissez-vous porter par une immersion totale à bord d’un bateau sur les canaux de la région. De début avril à fin octobre les embarcations sont autorisées sur les voies d’eau bretonnes. Des petits bateaux électriques aux voiliers, vous pourrez choisir comment vous voulez embarquer. Un vrai moment de détente pour tous ceux qui ont le pied marin. Plutôt besoin de vous ressourcer au contact de la nature ?  La forêt de Beffou qui est la plus haute forêt de Bretagne, est une véritable invitation pour une chasse aux champignons. Bolets, cèpes et girolles, tout au long de l’année, les visiteurs peuvent s’armer de leur opinel et de leur panier en espérant trouver les meilleurs du coin !`
            },
            {
              title: 'La Bretagne, pour quel type de séjour ? ',
              content: `La Bretagne est la région vraiment accessible à tout le monde. Pour les sportifs, la Bretagne est un vrai coin de paradis. Dans cette région vous pourrez sortir des sentiers battus à travers différentes activités comme le surf, la planche à voile ou encore le parapente. Pour les personnes qui aiment le farniente, Les différentes plages et sentiers préservés offrent un vrai séjour nature et détente. Vous pourrez prendre le temps de découvrir l’histoire de la région et ses traditions tout en profitant d’une vraie parenthèse verte. En famille ou entre amis, la Bretagne vous accueille dans ses villes et villages qui raviront petits et grands. La diversité des paysages à découvrir mettra tout le monde d’accord et vous donnera envie de revenir pour apprendre encore plus de choses sur cette merveilleuse région.`
            },
            {
              title:
                'Témoignage de nos hôtes :  pourquoi passer ses prochaines vacances éco-responsables en Bretagne ? ',
              content: `Marie, hôte des Gîtes du Kerouzec nous parle de sa région : « Lorsque l’on me demande ce qui me rend fière de ma région, je réponds sans hésiter que son patrimoine, sa culture et ses paysages sont les plus grands atouts de la Bretagne. Je trouve que c’est une région très vivante (même dans les campagnes !) et selon moi c’est ce qui la différencie des autres régions de France. Il y a tellement de jolis villages à visiter que pour moi c’est très difficile d’en choisir un, mais Gâvres reste une des plus belles destinations à connaître. Pour les voyageurs qui cherchent un spot moins connu pour se ressourcer, je conseille la forêt de Camors. » `
            }
          ]
        },
        redirections: null
      }
      break
    // case 'centre-val-de-loire':
    //   resp = {
    //     metaTitle: 'Centre-Val de Loire',
    //     searchTitle: 'Centre-Val de Loire',

    //     title: `Location d'hébergements Écolo & Responsables en Centre-Val de Loire - WegogreenR`,
    //     description: `Territoire riche d'histoire et de patrimoine, partez à la découverte des charmants villages du Centre Val de Loire et bénéficier d'un séjour en plein coeur de la nature.`
    //   }
    //   break
    case 'corse':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Corse',
        text: `Lovée entre mer et montagne, la Corse est la preuve qu’il ne faut pas partir loin pour profiter d’un paysage époustouflant. Sa riche gastronomie et sa nature préservée vous feront tomber sous son charme et vous offriront un séjour inoubliable, qu’il soit sportif ou farniente.\n\n Direction la Haute-Corse pour vivre une nuit dans <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">une chambre d’hôtel éco-responsable</a></b>  en bord de mer après avoir passé la journée à se baigner sur l’une des plages du Désert des Agriates, explorer les mignons petits villages de montagne de Pigna et Nonza, affronter le vent de l’Ile Rousse, siroter un cocktail à Calvi ou découvrir la traditionnelle ville Corse « Corte ». On va dormir dans <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">un gîte</a></b>  en Corse du Sud pour se prélasser sur deux des plus belles plages de France : Palombaggia et Santa Julia, faire une randonnée en pleine montagne à la découverte des somptueuses cascades de Pucaraccia et admirer les impressionnantes falaises de Bonifacio le joyau de la Corse du Sud. En termes de gastronomie, chaque coin Corse à sa spécialité : Aubergine à la Bonifacienne, Fiadone, calistrelli, nuciola, brocciu ou encore charcuterie Corse sont des incontournables que vous pourrez découvrir sur la majorité de nos tables d’hôtes eco-friendly.`,
        questions: {
          rows: [
            {
              title: 'Quelles balades faire en Corse ?',
              content: `La Corse est la destination parfaite pour s’évader à travers ses nombreux sentiers pédestres ou cyclables. La forêt d’Esra située dans le Cap Corse, est faite de nombreux chênes verts pour une immersion complète dans la nature. Durant les 35 minutes qui composent la promenade, on traverse de jolis coins de nature sauvage et d'aulnes, pour une escapade dans la nature qui n’est autre qu’une bulle de fraîcheur lors de votre séjour. Roulez vers les plus belles plages de Corse grâce aux différents itinéraires cyclables possibles. Pédalez à votre rythme et selon vos envies (tours sportifs ou balades en famille, il y en a pour tous les goûts). Ici on prend le temps d’apprécier le paysage littoral de la région, on part à la découverte des golfes de Pinarellu et Ventilegne. On découvre une magnifique côte sauvage qui s’étend sur plus de 100km. Les lacs de Mello et Capitello entourés de végétations sont accessibles via un sentier balisé. C’est le lieu parfait pour ensuite découvrir le col de la Bocca a Soglia, qui récompense les curieux par une magnifique vue sur les différents sommets de l’île. La randonnée dure environ 3h30 et est un vrai bol de nature locale.`
            },
            {
              title: 'Quelles fêtes locales Corse, ne faut-il pas louper ? ',
              content: `La foire du Vin à Lubri se déroule chaque année entre avril et juillet, c’est le moment pour les voyageurs de faire la rencontre de viticulteurs de la région. Sur la place de Luri, on trouve des produits régionaux et artisanaux à déguster, comme de l’huile d’olive, de la charcuterie ou encore les fameux vins corses. Blancs parfumés ou muscats, tous les amateurs de raisin y trouveront leur compte ! Cette occasion est alors le meilleur moyen de prendre le pouls de la Corse autrement tant cette fête est conviviale. Calvi on the rocks est un autre événement à ne pas manquer : chaque année au mois de juillet on redécouvre un festival à la programmation dynamique et diversifiée. Les mélomanes pourront profiter des choses simples de la vie au son de musiques house, soul ou encore disco. Soleil, mer, plage et musique, que demander de plus ! Enfin, le dernier immanquable est le pèlerinage de Notre-Dame des Neiges : situé dans le site de Bavella à 1 300 m d'altitude, ce pèlerinage rend hommage à Notre-Dame des Neiges, connue pour protéger les randonneurs qui s’aventurent dans le GR20. En famille ou entre amis, partagez un bon moment grâce à des festivités authentiques, et prenez le temps de vivre autour d’un pique-nique au cœur de la nature Corse.`
            },
            {
              title: 'Quels coins de nature faut-il visiter en Corse ?',
              content: `Près du village de l’Ospédale, la rivière de Bala est un vrai coin de paradis ressourçant et apaisant. Vous pourrez alors choisir entre trois grandes vasques d'eau et vous détendre dans ce cadre féerique entouré de nature. Ces chutes d’eau sont véritablement hors du commun. Un moment dans la rivière va mettre tout le monde d’accord : que vous préfériez flâner au bord de l’eau ou sauter de 5 mètres de haut pour une bonne dose de sensations fortes, ici tout est possible ! La Réserve de Scandola mérite un stop, classé au patrimoine mondial de l’Unesco, elle n’est accessible que par la mer. On y découvre alors un paysage unique entre terre et mer, avec des roches rougeâtres qui se reflètent sur l’eau turquoise, accompagnée par le vert de la forêt de pins. Un spectacle de couleurs au cœur de la nature.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'grand-est':
      resp = {
        type: 'region',
        searchTitle: 'Escapade dans le Grand-Est',
        text: `À deux pas de l’Allemagne, la région Grand-Est a su garder son histoire et son charme d’antan. Son atmosphère mystérieuse, ses vestiges du Moyen-âge, ses légendes locales et ses forêts denses et profondes font du territoire, une destination unique. Contrastée et authentique, la région vous promet des vacances mémorables à coup sûr ! \n\n Direction le Bas-Rhin pour passer <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">une nuit insolite</a></b>  sous les étoiles, découvrir la route des vins d’Alsace et randonner dans l’un des magnifiques sites de la région : le parc naturel régional des Vosges du Nord, le Grand Ried ou bien les Vosges moyennes. Dans l’Aube, on s’endort dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/ecolodge">nos écolodges</a></b>  puis on part découvrir l’histoire du Champagne à La Côte des Bar, haut lieu de production de cette boisson, on reprend la voiture pour se promener sur la route du prestigieux, où l’on peut découvrir d’anciennes cabanes de vignerons, puis on s’échappe dans la forêt d’Orient là où se cache le champagne humide. Et si cette année vous logiez dans <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">un de nos gites</a></b> en Meurthe-et-Moselle pour vos vacances d’été avec les enfants ? Ce département riche en culture est l’endroit parfait pour se replonger dans l’histoire du pays à travers la découverte du château de Jaulny, des forts de Vauban et de la Basilique Saint-Nicolas-de-Port. Si on aime le relief, on s’en va dans les Vosges dormir dans  <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">une de nos chambres d’hôtes</a></b>  écologiques au pied du parc naturel régional des Ballons des Vosges, de la route des Crêtes ou de la forêt de Sionne à la recherche de paysages inoubliables.`,
        questions: {
          rows: [
            {
              title:
                'Quelle est la période de l’année la plus favorable pour partir dans le Grand-Est ?',
              content: `Toutes les saisons sont agréables, elles ont toutes leurs charmes : paysages, villes et villages changeant au gré des mois de l’année. Cependant, le climat étant continental, le coin peut être enneigé et plutôt froid en hiver et au contraire, étouffant et très fréquenté en été. Selon nous, la meilleure saison pour s’y rendre reste tout de même l’hiver puisque le manteau blanc et les nombreuses décorations de Noël dont la région se pare lui donne un charme unique`
            },
            {
              title:
                'Dans quelle ville se rendre pour un city-trip dans la région ?',
              content: `Pour découvrir une ville riche en patrimoine, on vous propose Nancy : on y explore la place Stanislas, le palais Ducal, l’église et le couvent des Cordeliers ainsi que la basilique Saint-Epvre. Envie de changement ? direction Metz, cette ville datant d’il y a deux milles ans possède de nombreux trésors. Là-bas on flâne pendant des heures à travers les expositions d’art moderne et contemporaine du coin, on se dépayse devant l’architecture de l’opéra-théâtre de Metz Métropole qui nous rappelle le manoir de Harry-Potter et on prend une bonne dose de vitamine D en sirotant une boisson fraîche sur les terrasses de la Place Saint-Louis. Pour être envouté, rien ne vaut Strasbourg et son aura unique, à la fois capitale de l’Alsace et de l’Europe, la ville vaut bien quelques jours d’exploration.`
            },
            {
              title: 'Peut-on se déplacer à vélo dans le Grand-Est  ?',
              content: `Avec ses nombreuses voies cyclables et ses petites routes à faible circulation, la région Grand-Est représente un véritable terrain de jeu pour les cyclistes. En plus d’être green et bon pour la santé, le vélo nous permet de voyager d’une autre manière pour vivre des expériences encore plus fortes. La Meuse à vélo est par exemple un itinéraire très intéressant reliant Langres à la Mer du Nord en longeant la Meuse, paysages époustouflants à la clef. Le champagne à vélo et les Ardennes à vélo sont également des voies vertes s’étendant sur plus de 400 km qui permettent de traverser la région.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'hauts-de-france':
      resp = {
        type: 'region',
        searchTitle: 'Escapade dans les Hauts-de-France ',
        text: `À la frontière de la Belgique et à deux pas de la mer, la région des Hauts-de-France est bien loin de l’image grise et maussade qu’on lui donne souvent. Là-bas attendez-vous plutôt à être étonné par ses villes médiévales, ses plages immenses, ses festivités, ses marais, dunes et falaises qui témoignent de son passé étonnant ! \n\n Envie de découvrir des paysages époustouflants ? Logez dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos chambres d’hôtes</a></b>  écologiques dans le Nord, où nos hébergeurs vous délivreront leurs meilleures adresses pour vous sentir vraiment loin de chez vous, telles que le parc naturel français de la Scarpe et de l’Escaut, Avesnes-sur-Helpe, la forêt de Mormal et les monts des Flandres à Cassel… Vous aimez les sports de glisse ? On vous propose de dormir dans un de <b><a href="${process.env.NEXT_PUBLIC_URL}/gite"> nos gîtes eco-friendly</a></b>  sur le bord de la Manche pour découvrir Le Touquet-Paris-Plage, Berck-sur-Mer, Etaples-sur-Mer et les stations balnéaires de Hardelot, Wimereux, Ambleteuse, Audresselles et Escales. Partons dans l’Oise, pour vivre un séjour de vraie déconnexion, on commencera par choisir un <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">logement insolite et écoresponsable</a></b> puis on partira à la découverte de la réserve naturelle de Saint-Pierre-ès-Champs où l’on peut apercevoir des orchidées en pleine nature. Pour vous sentir vraiment au vert pendant votre séjour, rendez-vous dans la forêt de Compiègne où entend la brame des cerfs.`,
        questions: {
          rows: [
            {
              title:
                'Quelles sont les activités kids friendly à faire dans les Hauts-de-France ?',
              content: `Avec les enfants, on se réserve une après-midi à la ferme pédagogique des petits pas Lillers pour qu’ils multiplient leurs connaissances sur les animaux et leur élevage. À Boulogne-sur-Mer, c’est Nausicaa qui révèlera tous les fonds marins aux bambins. Dans cet aquarium, ils découvriront comment les espèces aquatiques évoluent dans leur milieu de vie.  Pour une activité en extérieur épatante, on grimpe sur le dragon de Calais, une construction en acier et bois produite à Nantes qui s’articule en fonction de ses envies, promettant une balade insolite qui ravira petits et grands.`
            },
            {
              title: 'Que visiter à Lille dans les Hauts-de-France ?',
              content: `Lille est la ville parfaite pour faire un city trip mêlant food et découvertes. On vous conseille de visiter l’incontournable grande place, le musée de Marguerite Yourcenar, ses jardins magnifiques et de manger sur l’une des tables des grands chefs : le Resto du Bloempot (Florent Ladeyn), Chez mon cousin (Nicolas Pourcheresse), Au Sébastopol (Damien Laforce). Pour dynamiser son voyage, on va se faire les bords de la Deûle à vélo, direction la Flandre ou le Parc de la Citadelle pour un écrin de verdure. Vous restez plus longtemps ? Partez de la gare d’Eau à Lomme et allez jusqu’à Wambrechies où vous pourrez visiter la distillerie qui produit le célèbre alcool du coin « Le Genièvre ».`
            },
            {
              title:
                'Quelles sont les spécialités locales à déguster dans les Hauts-de-France ?',
              content: `Premier petit plat à déguster et pas des moindres, la flamiche aux poireaux. Une sorte de tourte avec une fondue de poireaux à l’intérieur, un vrai délice pour nos papilles. Parlons maintenant de la ficelle picarde, une crêpe fine fourrée au jambon et champignons de Paris, gratinée au four avec crème fraîche et fromage. Pour les amateurs de fromages, la région en regorge : Maroilles, Bergues, Boulette d’Avesnes, Mont des Cats, Samer, Vieux-Boulogne, Cœur d’Arras, Belval…. Côté sucré, le macaron d’Amiens est un vrai délice composé de sucre, miel, blanc d’œuf et extrait d’amande il accompagne parfaitement les petites pause-café. En ce qui concerne les boissons, les bières locales telles que la Castelain ou l’Atrébate, sont très réputées, tout comme l’alcool du coin : le genièvre.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'ile-de-france':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Ile-de-France',
        text: `Un séjour à proximité de la capitale pour un véritable city-break, c’est ça le crédo de l’Île-de-France ! La région regorge de nombreux lieux touristiques qui valent le détour. Entre pique-nique au bord d’un lac ou promenade en forêt, profitez d’une escapade nature qui a du sens. \n\n Direction l’Essonne, pour passer une nuit dans un de nos gîtes écologiques et canons à proximité de Evry, la ville riche en patrimoine religieux ou Etampes nationalement connu comme étant un haut-lieu d’art et d’histoire. La capitale de la France, Paris, vaut bien évidemment le déplacement, dormez dans un de <b><a href="${process.env.NEXT_PUBLIC_URL}/hotel">nos éco-hôtels</a></b>  le soir et arpentez les Champs-Elysées, Montmartre et le Louvre en journée. Dans la Seine-et-Marne, on part à la rencontre des jardins de Fontainebleau, de ceux de Vaux-le-Vicomte et de la forêt de Crécy-la-Chapelle. Si on veut passer des vacances en mode slow tourisme jusqu’au bout, on s’en va dans les Bois de Vincennes, le parc zoologique de Paris et dans la Roseraie du Val-de-Marne pour une véritable pause green dans un de nos <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">hébergements insolites</a></b>  du coin. Dans les Yvelines, on ne manquera pas de jouer les touristes à Versailles (château, parc, écuries, opéra), Marly-le-Roi (maisons d’artistes et le château de Monte-Cristo) et Saint-Germain-en-Laye ( Château, musées Claude Debussy et Maurice Denis).`,
        questions: {
          rows: [
            {
              title:
                'Quels villages faut-il absolument visiter en Ile-de-France ?',
              content: `On commence par Provins en Seine-et-Marne : une cité médiévale classée au patrimoine mondial de l’Unesco est idéale pour les amateurs d’histoire. Vous allez tomber amoureux de ce village à la splendide architecture, de ses ruelles pittoresques et des souterrains de la commune. Vous pourrez ensuite admirer l’attraction incontournable de la citadelle : la majestueuse Tour César. Une immersion totale sur les traces de l’histoire, c’est la promesse faite par ce joli village. Lové dans des vallées verdoyantes, Milly-la-Forêt est réputée pour ses plantes médicinales depuis le XIXème siècle. Mais le village est aussi connu pour le passage de différents artistes, il abrite notamment l’ancienne maison de repos du chanteur Claude François et celle du poète Jean Cocteau. Il est alors facile de se laisser emporter par l’univers accueillant et plein de charme de cet emblématique village d’Île-de-France.  Pour finir, on vous propose Rochefort-en-Yvelines : aussi charmant que pittoresque, ce village au sud du Parc naturel régional de la Haute Vallée de Chevreuse cache les vestiges de son histoire. En se baladant autour des ruines du châteaufort et à travers ses petites rues fleuries, on note que le village est sur la route de Saint-Jacques de Compostelle. Une véritable parenthèse culturelle en somme !`
            },
            {
              title:
                'Où aller dans la région Ile-de-France pour se mettre au vert ?',
              content: `Pour commencer, direction la forêt de Dourdan : devenue domaniale en 1870, c’est un ancien territoire de chasse des rois de France. Aujourd’hui elle permet à tous de découvrir la splendide abbaye de l’Ouye en plein cœur de la forêt. Les riverains apprécient sa richesse écologique et son calme absolu qui donnent lieu à un moment ressourçant à ceux qui viennent s’y promener. Le secret ? Partir à la recherche du majestueux « chêne des 6 frères », un arbre unique possédant 6 troncs ! Notre petit favori, c'est le poumon vert du Mantois, autrement dit la forêt de Rosny. Le lieu est parfait pour admirer une riche biodiversité constituée de chênes, hêtres, pins, bouleaux, érables, arbres fruitiers et autres châtaigniers. Une superbe vue sur la vallée de la Seine s’offre aux voyageurs les plus curieux. Entre le Belvédère de Châtillon et les vestiges du château des Beurons, cette escapade dans la forêt est une promenade historique qui promet d’être riche en surprises.`
            },
            {
              title:
                'Quelles fêtes locales ne faut-il pas manquer dans la région Ile-de-France ?',
              content: `Chaque année au mois de décembre, la ville de Provins fête Noël lors d’un week-end. Marché médiéval, crèche vivante, visites guidées et spectacles de rue, tout est fait pour mettre de bonne humeur. C’est le bon endroit pour découvrir des produits artisanaux, déguster des plats locaux et profiter d’un véritable moment de festivité en plein hiver. Au mois d’octobre la confrérie des Clos de Rueil-Buzenval organise la fête des vendanges à Rueil-Malmaison. Cette fête anime la ville depuis plus de trente ans, il ne faut donc pas hésiter ! Au programme : fanfare, défilé, brocante et manèges. Pour les gourmands, vins blancs et autres produits du terroir sont proposés par la ville. La fête des Roses en Seine-et-Marne à vu le jour en 1947 ! Prenez alors le temps de découvrir cet événement incontournable du printemps en Île-de-France. Il s’agit d’une des plus vieilles traditions de la région, l’occasion idéale de festoyer le premier week-end de juin sous l’odeur parfumée de jolies roses.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'normandie':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Normandie',
        text: `Ses falaises de craie blanche et ses célèbres plages font de la Normandie une région unique. Vous pourrez y découvrir de nombreux coins exceptionnels pour un séjour près de la nature qui vous ressourcera assurément. Entre ses plus beaux villages de France, ses phares, ses coins nature à ne pas manquer et ses nombreuses randonnées et pistes cyclables, la Normandie est la destination parfaite pour vos prochaines vacances !\n\nOn part dans l’Eure pour passer une nuit dans <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">un logement insolite et éco-responsable</a></b>  après avoir visité l’un des plus beaux villages de France : Le Bec-Hellouin et son Abbaye classée au patrimoine mondial de l’Unesco. Au passage on découvre le Marais Vernier à vélo, dans le Parc Naturel des Boucles de la Seine, sa faune et sa flore sauront rendre l'expérience authentique et inoubliable pour les petits comme pour les grands.  Sur la route des falaises d’Etretat on passe une nuit dans un de nos écogîtes avant de s’aventurer sur le circuit des étangs de Nesle-Normandeuse, pour une balade d’une heure trente entre découvertes nature et villages authentiques.  Et si on optait pour l’Orne ? Après une douce nuitée dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos chambres d’hôtes</a></b> , reconnectez-vous à la nature dans le parc naturel de Ré-Perche où vous pourrez observer chevaux, grandes forêts et jolis villages authentiques.`,
        questions: {
          rows: [
            {
              title:
                'Quelles fêtes locales ne faut-il pas manquer lors d’un séjour en Normandie ?',
              content: `Plusieurs fêtes sont des incontournables en Normandie. La fête des Normands dans l’Eure, en fait partie. Tous les ans lors de la Saint-Michel, la Normandie invite locaux et voyageurs à fêter sa culture au cœur de la vallée de l’Isle.  Le programme est animé : musique, danses, jeux et chants traditionnels normands. Dans l’Eure, il existe aussi la fête de la Pomme et de la tradition paysanne. Au mois d’octobre lors de cette fête à Tourville-sur-Pont-Audemer, les promeneurs voyagent dans le temps grâce à un ancien pressoir à pommes actionné par un cheval ! Des démonstrations de battage de blé font aussi partie du programme ainsi que la découverte d'anciens métiers paysans. Découvrez cette fête authentique pour une expérience unique ! Autre immanquable, la fête des marins dans le Calvados. Ce rendez-vous exceptionnel se déroule tous les ans lors du week-end de la Pentecôte. Au programme, la bénédiction de la mer et le pèlerinage à Notre-Dame de Grâce à travers un défilé unique de maquettes de bateaux.`
            },
            {
              title: ' La Normandie, pour quel type de séjour ?',
              content: `La Normandie est le territoire parfait pour les sportifs : un paysage unique et dépaysant qui offre de nombreuses randonnées et activités sportives dans un cadre exceptionnel. Entre plages et falaises, prenez un véritable bol d’air frais à travers des activités outdoor. Pour un séjour ressourçant, on s’empare des paysages uniques de la région pour se balader ou flâner au bord de l’eau. Un séjour nature en mode slowlife s’offre à vous dans ce joli coin de France où il fait bon vivre. Prenez le temps de vous relaxer et de profiter d’un voyage qui a du sens. L’endroit convient aussi parfaitement pour des séjours avec toute votre tribu. Entre les activités à faire et les villages à découvrir, tout le monde sera conquis par la région. Un paysage unique vous attend pour un séjour hors des sentiers battus en famille, en couple ou entre amis.`
            },
            {
              title:
                'Témoignage de nos hôtes :  pourquoi passer ses prochaines vacances eco-friendly en Normandie ?',
              content: `Lise, hôte du Domaine de l'Hostellerie nous parle de sa région : « Beaucoup de nos voyageurs nous disent qu’ils sont surpris de la diversité de paysage et d’architecture qu’ils rencontrent en Normandie. Vous pouvez passer votre après-midi à la plage et profiter d’une ambiance bord de mer, puis quelques kilomètres plus loin être en pleine campagne et plonger dans une tout autre atmosphère. L’architecture est également très différente selon les secteurs, vous pouvez dans un rayon de 30km admirer des maisons en colombages, des bâtisses en pierre de Caen ou de Creully et des fermes en pierre de schiste. »`
            }
          ]
        },
        redirections: null
      }
      break
    case 'nouvelle-aquitaine':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Nouvelle - Aquitaine ',
        text: `Connue pour ses vignobles et ses jolies plages, l’Aquitaine propose un séjour d’exception. Plus que de découvrir les vallées de la Dordogne et de la Vézère, d’autres endroits aussi verdoyants qu’inspirants attendent les voyageurs pour un séjour ressourçant en pleine nature.\n\nEt si vous commenciez votre séjour en Aquitaine par un city-trip à Bordeaux, la ville de la gastronomie et du bon vin, où vous pourrez loger dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos chambres d’hôtes à proximité.</a></b> Pas fan de la ville ? Pas de soucis, la Gironde regorge de paysages diversifiés : direction le Bassin d’Arcachon pour vous reposer dans un <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">de nos gîtes eco-friendly</a></b>  après avoir gravi la dune du Pilat et explorer la ville d’hiver d’Arcachon. Pour un séjour farniente et détente, rendez-vous dans un de <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">nos logements insolites</a></b>  dans les Landes, de nombreuses stations balnéaires à proximité vous permettront de vous sentir vraiment en vacances : Biscarrosse, Mimizan, Hossegor… Pour plus de patrimoine, on vous suggère de vous diriger vers le Lot-et-Garonne : la tour du Chapelet à Agen, le musée du Pruneau à Laffite-sur-Lot et les châteaux de Nérac et Duras, vous en mettront plein les yeux. Envie d’un séjour plus sportif ? On  part dans les Pyrénées-Atlantiques qui allient à la fois mer et montagne. Biarritz, Bayonne et Anglet avec leur mer agitée sont des localités appréciables pour surfer, tandis que la Vallée d'Ossau, Arette et la Pierre-Saint-Martin sont des destinations parfaites pour se réchauffer dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/ecolodge">nos écolodges</a></b>  après une journée de sports d’hiver.`,
        questions: {
          rows: [
            {
              title:
                'Quelles sont les spécialités culinaires qu’il faut absolument goûter lors d’un séjour en Aquitaine ?',
              content: `Pour commencer, nous vous proposons de goûter aux fameux canelés de Bordeaux, des petites douceurs à déguster avec un café en terrasse sur les quais de la métropole. Si vous faites escale à St Emilion, ne manquez pas de ramener des macarons portant le nom de la ville, des gourmandises à l’amande dont la recette est gardée dans le plus grand des secrets.  Direction les Pyrénées pour se laisser tenter par un bon gâteau basque réalisé avec la recette ancestrale à base de crème ou de cerise qui plaira à tous les goûts.`
            },
            {
              title:
                'Quels sont les plus beaux villages de France à visiter dans la région Aquitaine ?',
              content: `Pour commencer, direction Saint-Emilion, le village est réputé pour ses vins renommés dans le monde entier ! Mais Saint-Emilion ce n’est pas que ça, c’est aussi admirer une architecture médiévale et des monuments historiques à couper le souffle. En sillonnant les ruelles du village, on aime contempler les petites maisons aux tons pastel et finir la balade en dégustant un bon verre de vin. Un peu plus loin, dans les Pyrénées-Atlantiques, on se dirige vers Espelette, le plus gourmand de tous les villages ! Au cœur de la Nouvelle-Aquitaine, Espelette est connu par les amateurs de piment. Mais plus qu’une ville de goût, c’est aussi une terre d’histoire, que l’on découvre en flânant dans les jolies rues du centre-ville. En septembre, des guirlandes de piments habillent le village et lui donnent un air unique. Et pour finir, si on partait à Ainhoa ? Village typique au bord de l’océan qui est reconnaissable grâce à ses petites maisons aux couleurs vives qui enchantent tous les voyageurs. Ce village à la frontière espagnole séduit par son charme basque et ses recoins authentiques.`
            },
            {
              title:
                'Témoignage de nos hôtes : pourquoi passer un séjour en Aquitaine ?',
              content: `Florence, <b><a href="${process.env.NEXT_PUBLIC_URL}/hebergements/week-end-romantique-avec-spa-privatise-271">hôte du domaine de Garat</a></b>, nous parle de sa région : « L’architecture authentique entourée de verdure et d’espaces verts nous permet de respirer et de prendre l’air. Pour moi, ce qui différencie la Nouvelle-Aquitaine des autres régions, c’est son mélange de mer, montagne et vin. Je trouve que c’est un bon combo ! Il y a certains des plus beaux villages de France qui sont dans la région, je ne les connais pas tous mais je trouve que Saint-Émilion et Arcachon ont le mérite d’être classés parmi eux. Le choix est difficile !  Côté gastronomie, en Nouvelle-Aquitaine le canard se prépare sous toutes ses formes et c’est toujours très bon. Magret, foie-gras… ce sont les plats typiques et emblématiques de la région.  Personnellement, j’ai décidé de devenir hôte pour une raison très simple : rendre les gens heureux ! »`
            }
          ]
        },
        redirections: null
      }
      break
    case 'occitanie':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Occitanie',
        text: `À deux pas de l’Espagne, au bord de la Méditerranée et dans les Pyrénées, on découvre l’une des régions les plus diversifiées de France. Entre montagnes, plages de sable, Grands Causses et étangs, la richesse de paysages permet aisément de passer des vacances mémorables en choisissant ses activités favorites !\n\n Passez une <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">nuit insolite dans un de nos hébergements éco-responsables</a></b>   avant d’aller visiter la Côte Vermeille et ses jolis trésors : Collioure, Le sentier du Littoral, Argelès sur Mer et Port Vendres. En manque de patrimoine ? On se dirige dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/ecolodge">nos écolodges</a></b>  dans l’Aude ou la Montagne Noire. Aux alentours, on visite le célèbre Château de Castelnau, les quatre châteaux de Lastours ou encore la passerelle de Mazamet. Dans les Hautes-Pyrénées, on passe des vacances à la montagne revigorantes en dormant dans <b><a href="${process.env.NEXT_PUBLIC_URL}/hotel">un Hotel</a></b> avec une vue exceptionnelle et à proximité des stations de ski telles que : Luz-Saint-Sauveur, Bagnères-de-Luchon, Bagnères-de-Bigorre, Argelès-Gazost… Crapahutons un peu plus loin, en Aveyron, pour découvrir le gigantesque Viaduc de Millau, l’étrange plateau de l’Aubrac, les sportives gorges de la Dourbie, la typique abbaye de Sylvanès ainsi que l’indescriptible site géologique du canyon de Bozouls. Tout autour de ces endroits exceptionnels nos <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">gîtes</a></b> vous permettront de vous reposer en famille avant d’attaquer votre prochaine escapade.
`,
        questions: {
          rows: [
            {
              title:
                'Quelles sont les spécialités culinaires à déguster lors d’un voyage en Occitanie ?',
              content: `La région possède de nombreuses spécialités diverses et variées, on peut commencer par citer le fameux cassoulet de Castelnaudary nationalement connu ou bien l’Aligot spécialité née en Aubrac à base de purée, fromage et ail. En ce qui concerne le sucré là aussi, il existe de quoi vous régaler : crème catalane, gâteau à la broche, réglisse d’Uzès, fougasse, tarte aux myrtilles ou encore Rousquilles du Vallespir : des petits biscuits ronds, recouverts d’un beau glaçage, qui fondent en bouche.`
            },
            {
              title: 'Pourquoi passer des vacances en Occitanie ?',
              content: `L’Occitanie propose des paysages à couper le souffle, vous pourrez vous dépenser lors des randonnées, balades nature ou sorties à vélo !  Prenez le temps de vous ressourcer dans cette région aux paysages uniques. Vous pourrez vous reposer ou vous balader au cœur de la nature ou en sillonnant les rues des jolies villes de la région.  Toute la tribu sera conquise : ses lacs, ses montagnes et ses cascades sauront satisfaire tous les goûts ! Un moment de bonheur en famille ou entre amis fera de votre séjour un voyage inoubliable hors des sentiers battus.`
            },
            {
              title: 'Que faire dans la région Occitanie en famille ?',
              content: `Le territoire de l’Occitanie se prête parfaitement aux visites en famille. Le château de Carcassonne avec son circuit en calèche sera divertir les enfants, la recherche des loups de Gévaudan dans le parc animalier de Sainte-Lucie ou le Seaquarium du Grau du Roi sont des activités dont les enfants raffolent ! Direction le musée des bonbons pour faire plaisir aux bambins tout en les instruisant sur l’histoire des friandises, leur packaging et leur fabrication. Pour des vacances en famille slow, on n’hésite pas à prendre le train, celui à vapeur des Cévennes ou encore le train jaune entre Villefranche-de-Conflent et Latour-de-Carol qui permet aux petits de découvrir les incroyables paysages de la région sans faire d’effort !`
            }
          ]
        },
        redirections: null
      }
      break
    case 'pays-de-la-loire':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Pays de la Loire',
        text: `Le Pays de la Loire est la région idéale pour un séjour dépaysant et ressourçant. Grâce à ses spécialités locales, son patrimoine historique et ses magnifiques paysages, un voyage en solo ou avec toute la tribu est forcément placé sous le signe de l’inoubliable.\n\nDirection la Loire-Atlantique pour dormir dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos chambres d’hôtes éco-responsables</a></b>   et découvrir l’une des cités de caractères, l’un des ports ou l’une des stations balnéaires du coin : Piriac-sur-Mer, Pornic, La Turballe, Guérande et La Baule. Le Maine-et-Loire est le territoire des châteaux par excellence, on adore passer une <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">nuit insolite</a></b>  et au petit matin découvrir : le château d’Angers, celui de Saumur, Brissac, Serrant, Baugé ou bien celui de Montgeoffroy. Au nord-ouest des Pays de la Loire, on passe son séjour dans <b><a href="${process.env.NEXT_PUBLIC_URL}/ecolodge">une écolodge</a></b>  en Mayenne, un territoire parfait pour les amoureux de la nature en quête de balades forestières, de navigation ou encore de pêche.  À mi-route entre le Bassin parisien et le Massif armoricain, la Sarthe s’impose comme le coin où loger dans <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">un gîte éco responsable</a></b>  , en manque de découvertes :  on vous conseille le musée de la musique mécanique avec orgue de barbarie et piano bastringue. Pour les aventuriers, c’est le circuit par autorails et l’ancienne digne des ducs qui dictera votre voyage.
`,
        questions: {
          rows: [
            {
              title:
                'Un voyage en Pays de la Loire, c’est quel type de séjour ?',
              content: `Cette région riche en patrimoine naturel offre une véritable parenthèse sportive au cœur de la nature. C’est le moment de se dépenser et de profiter du séjour pour se lancer dans des activités qui font du bien au corps comme à l’esprit. En mode slow life, vivez un moment qui a du sens en découvrant les merveilles de cette région, au cœur de la nature ou à la recherche de jolis villages. Prenez le temps de vous ressourcer et de profiter d’un séjour à votre rythme. Tout le monde est conquis par ce coin de paradis ! Un séjour entre amis ou en famille dans les Pays de la Loire permet de vivre hors du temps, l’histoire de quelques jours. La région est donc idéale pour profiter d’une véritable escapade en famille ou entre amis.`
            },
            {
              title: 'Que faire à pied ou à vélo dans le Pays de la Loire ?',
              content: `Partez à l’aventure sur le Mont des Avaloirs à 416 mètres de haut ! Pour découvrir une vue imprenable à 360° sur les Alpes Mancelles, la campagne d’Alençon et peut-être le Mont Saint-Michel si le temps est dégagé., le spot est parfait Il s’agit là de l’une des plus belles vues de la région, que l’on peut observer après l’ascension de 108 marches. Les voyageurs en quête d’une randonnée magique en pleine forêt ne vont pas être déçus. Au cœur de 5000 hectares de la forêt de Mervent-Vouvant, on se balade à l’ombre de chênes hauts de 35 mètres ! Le must reste le splendide lac de barrage et son cadre féerique pour une véritable parenthèse nature comme on les aime. Le vélo a une place importante aux Pays de la Loire, vous pourrez découvrir 3 grands itinéraires qui traversent la région : La Loire à Vélo, La Vélodyssée et La Vélo Francette. En solo ou en famille, ces 3 000 km de voies cyclables vous feront passer un moment de bonheur hors des sentiers battus en profitant d’un paysage hors norme !`
            },
            {
              title:
                'Quels sont les coins natures à ne pas manquer dans la région Pays de la Loire ?',
              content: `Le parc oriental de Maulévrier, plus grand parc japonais d’Europe est dans le Maine-et-Loire ! Avec plus de 29 hectares, le parc n’abrite pas moins de 400 espèces végétales. Ce que l’on préfère c’est d’emprunter le pont rouge typique qui traverse le cours d’eau du lieu, et de découvrir un salon de thé japonais authentique caché juste derrière. Cette atmosphère magique séduit petits et grands à tous les coups. Le parc oriental de Maulévrier, plus grand parc japonais d’Europe est dans le Maine-et-Loire ! Avec plus de 29 hectares, le parc n’abrite pas moins de 400 espèces végétales. Ce que l’on préfère c’est d’emprunter le pont rouge typique qui traverse le cours d’eau du lieu, et de découvrir un salon de thé japonais authentique caché juste derrière. Cette atmosphère magique séduit petits et grands à tous les coups.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'provence-alpes-cote-azur':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Provence-Alpes-Côte d’Azur',
        text: `Le sud de la France et son patrimoine rempli de richesses sont surprenants. Pour passer un séjour unique et ressourçant entre mer et montagnes, la destination est toute trouvée. Partez à la rencontre des sites incontournables de la Provence-Alpes-Côte d’Azur et vivez des expériences exceptionnelles.\n\nDormez dans une <b><a href="${process.env.NEXT_PUBLIC_URL}/hotel">chambre d’hôtel</a></b> face à la mer dans le Var avant de partir à la rencontre des magnifiques coins qui l’entourent : L'île de Porquerolles, les gorges du Verdon et la rade de Toulon. Si vous êtes à la recherche de patrimoine, dirigez-vous vers Barcelonnette, le village de montagne à deux pas de l’Italie, Sisteron et son immense citadelle ou Moustiers-Sainte-Marie enclavé dans les montagnes. On loge dans un <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">gîte éco-responsable</a></b> et authentique à deux pas de Marseille pour se laisser le temps d’explorer les Calanques de Cassis, Aix-en-Provence, les Îles du Frioul et Salon-de-Provence des endroits marqués par leur fabuleuse histoire. Dans les Hautes-Alpes, on dort en plein milieu de la forêt dans l’une de nos écolodge, véritable havre de paix en été et cocon pour se réchauffer après avoir passé la journée à dévaler les pistes de Puy St Vincent, Risoul ou Montgenèvre, en hiver. Le Vaucluse possède aussi sa dose de trésor : entre le Palais des Papes à Avignon, le sentier des Ocres ou le théâtre antique d’Orange chacun pourra découvrir la région au gré de ses envies avant d’aller se reposer dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos chambres d’hôtes</a></b>  éco-responsables.`,
        questions: {
          rows: [
            {
              title:
                'Quels sont les plus beaux villages à visiter en Provence-Alpes-Côte d’Azur ?',
              content: `Sainte-Agnès est parfait pour découvrir l’arrière-pays-niçois autrement ! À quelques kilomètres de la frontière italienne, ce village authentique avec ses ruelles pavées et ses façades colorées est le comble d’un charmant séjour dans la Riviera. Entre son allure médiévale et sa splendide vue sur la Côte-d’Azur, impossible d’être déçu. Pour être encore plus étonné, direction Venasque dans le Vaucluse : un joli village au cœur de la Provence qui enchantera petits et grands grâce à son ambiance médiévale et sa sérénité. Au pied du Mont Ventoux, le Venasque est entouré de vignes, de cerisiers et de garrigues. L’ancienne cité profite d’un ensoleillement optimal, c’est donc l’occasion de profiter pleinement d’une balade au cœur du bourg pour découvrir les fontaines et les maisons authentiques qui font tout son charme. On ne cite plus Les-Baux-de-Provence, en plein cœur du Parc naturel régional des Alpilles. En sillonnant les ruelles surplombées par les vestiges d’un château, ses ruines, ses tours et son donjon classés monument historique, on apprécie la parenthèse douceur offerte par Les-Beaux-de-Provence et son panorama incroyable.`
            },
            {
              title:
                'Témoignage de nos hôtes :  pourquoi choisir la région PACA pour son prochain séjour ?',
              content: `Floriane, hôte du Camping Ecolodge L'Étoile d'Argens nous parle de sa région : « La première chose qui me vient à l’esprit quand je parle de ma région, c’est son climat agréable. Il fait bon vivre en Provence-Alpes-Côte-d’Azur mais d’autres aspects la différencient aussi des autres régions. Notamment son côté naturel, ses différents vignobles et la diversité des paysages entre les zones touristiques en bord de mer d’un côté et les vallées un peu moins fréquentées de l’autre. Il y a de nombreux villages à découvrir aux alentours mais pour moi le plus joli, c’est Roquebrune-sur-Argens. C’est un village typiquement provençal et médiéval où l’on peut apercevoir un très beau rocher. »`
            },
            {
              title:
                'Quelles sont les adresses à ne pas manquer lors d’un séjour en PACA ?',
              content: `Le Jardin du Lautaret est un jardin unique au cœur du Parc naturel régional des Écrins, dans le massif de la Meije. Plus de 2300 espèces de plantes des montagnes et des régions froides sont regroupées dans ce jardin botanique qui est l’un des plus anciens d'Europe. Impressionnant ! Partez randonner à la Madone de Fenestre, dans un circuit à couper le souffle ! Vous découvrirez une balade alpine autour des plus beaux paysages de la région. Vous arriverez dans le col de la Fenestre avec un décor très minéral et dépaysant. Pour les plus chanceux, il sera possible d’apercevoir des chamois ou des bouquetins… À vos jumelles, prêts ? Randonnez !  Dernière option : les balades sur le chemin du Mont Ventoux. Dans ce coin du Luberon, à côté des vignobles de la Vallée du Rhône, de nombreux sentiers sont proposés, dont le plus célèbre est celui de la Méditerranée à vélo. Cette vélo-route du Calavon propose 37 km de pistes entre Coustellet et Saint-Martin-de-Castillon, une véritable parenthèse nature à la force des mollets.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'languedoc-roussillon':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Languedoc-Roussillon',
        text: `À deux pas de l’Espagne, au bord de la Méditerranée et dans les Pyrénées, on découvre l’une des régions la plus diversifiée de France. Entre montagnes, plages de sable, Grands Causses et étangs, la richesse de paysages permet aisément de passer des vacances mémorables dans le Languedoc-Roussillon en choisissant ses activités favorites !\n\nPassez une <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">nuit insolite</a></b>  dans un de nos hébergements éco-responsables avant d’aller visiter la Côte Vermeille et ses jolis trésors : Collioure, Le sentier du Littoral, Argelès sur Mer et Port Vendres. En manque de patrimoine ? On se dirige dans une de nos écolodge dans l’Aude ou la Montagne Noire. Aux alentours, on visite le célèbre Château de Castelnau, les quatre châteaux de Lastours ou encore la passerelle de Mazamet. Dans l’Hérault on se fait un city-trip à Montpellier en dormant dans un de nos hôtels éco-responsables à proximité, direction la Grande-Motte pour passer la soirée au bord de l’eau à regarder le coucher de soleil. Pour encore plus de découvertes on se dirige vers le Gard. On visite le célèbre pont du même nom, les salins d’Aigues-Mortes, Uzès et les Arènes de Nîmes et on s’endort après avoir dégusté un bon repas à base de spécialités locales concoctées par les propriétaires de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos chambres d’hôtes</a></b> .`,
        questions: {
          rows: [
            {
              title:
                'Quelles sont les spécialités culinaires à déguster lors d’un voyage dans le Languedoc-Roussillon ?',
              content: `La région possède de nombreuses spécialités diverses et variées, on peut commencer par citer le fameux cassoulet de Castelnaudary nationalement connu ou bien l’Aligot spécialité née en Aubrac à base de purée, fromage et ail. En ce qui concerne le sucré là aussi, il existe de quoi vous régaler : crème catalane, réglisse d’Uzès, fougasse ou encore Rousquilles de Vallespir : des petits biscuits ronds, recouverts d’un beau glaçage, qui fondent en bouche.`
            },
            {
              title: 'Que faire dans le Languedoc-Roussillon en famille ?',
              content: `Le territoire du Languedoc-Roussillon se prête parfaitement aux visites en famille. Le château de Carcassonne avec son circuit en calèche sera divertir les enfants, la recherche des loups de Gévaudan dans le parc animalier de Sainte-Lucie ou le Seaquarium du Grau du Roi sont des activités dont les enfants raffolent ! Direction le musée des bonbons pour faire plaisir aux bambins tout en les instruisant sur l’histoire des friandises, leur packaging et leur fabrication. Pour des vacances en famille slow, on n’hésite pas à prendre le train, celui à vapeur des Cévennes ou encore le train jaune entre Villefranche-de-Conflent et Latour-de-Carol qui permet aux petits de découvrir les incroyables paysages de la région sans faire d’effort !`
            },
            {
              title:
                'Quels sont les sites naturels à ne pas manquer dans le Languedoc-Roussillon ?',
              content: `L'abîme de Bramabiau ainsi que sa rivière souterraine sont des incontournables, tout comme le parc national des Cévennes qui fait partie des 11 parcs nationaux de France. Un peu plus loin, le Cirque de Navacelles est un autre paysage à découvrir, proposant une vue panoramique époustouflante. Avant de partir, ne manquez pas les Orgues d’Ille-sur-tet, des petites cheminées de fée forgées par l’érosion des roches au fil des ans.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'midi-pyrenees':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Midi-Pyrénées',
        text: `La région, riche en monuments incontournables et en patrimoine naturel, regorge de sites touristiques exceptionnels. Ses spots naturels sont le témoignage de parenthèses nature au cœur d’un coin aux multiples merveilles. Dans les Midi-Pyrénées, on se lance dans un séjour qui a du sens ! \n\nPassez une nuit inoubliable dans l’un de <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">nos gîtes éco-responsables</a></b> avant d’aller visiter les plus beaux villages de la région : Saint-Cirq-Lapopie, Rocamadour, Cordes-sur-Ciel, Conques et bien d’autres encore. En manque de patrimoine ? On se dirige dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos chambres d’hôtes</a></b>  près de Montauban. Dans cette ville, on découvre : le quartier Villebourbon, la cathédrale Notre-Dame-de-l ’Assomption, la place Nationale et de nombreux musées : musée Ingres Bourdelle, Victor Brun, des jouets… Dans les Hautes-Pyrénées, on passe des vacances à la montagne revigorantes en dormant dans un hébergement insolite et éco-responsable avec une vue exceptionnelle et à proximité des stations de ski telles que : Luz-Saint-Sauveur, Bagnères-de-Luchon, Bagnères-de-Bigorre, Argelès-Gazost… Crapahutons un peu plus loin, en Aveyron, pour découvrir le gigantesque Viaduc de Millau, l’étrange plateau de l’Aubrac, les sportives gorges de la Dourbie, la typique abbaye de Sylvanès ainsi que l’indescriptible site géologique du canyon de Bozouls. Tout autour de ces endroits exceptionnels <b><a href="${process.env.NEXT_PUBLIC_URL}/ecolodge">nos écolodges</a></b>  vous permettront de vous reposer avant d’attaquer votre prochaine escapade.`,
        questions: {
          rows: [
            {
              title:
                'Quelles sont les choses à faire à pied ou à vélo dans les Midi-Pyrénées ?',
              content: `On vous propose une randonnée d’environ 2h dans les Pyrénées qui vous fera arriver sur un spot incroyable : le lac d’Oô, un plan d’eau artificiel à couper le souffle. Pour y parvenir, il faut suivre un parcours balisé en pleine forêt, au cœur de la nature. À l’arrivée on admire une cascade de 300 mètres de haut et on en prend plein les yeux.  Un peu plus loin, direction le pont d’Espagne, c’est parti pour l’aventure dans le chemin des cascades. Durant une balade d’environ 3h, 6 cascades sont à découvrir. C'est le moment d’observer les Pyrénées autrement et d’admirer un paysage où l’eau, la roche et les arbres se rencontrent pour former un cadre hors du commun. Dans le Lot, on s’empare du circuit de Saint-Gignac, un circuit d’environ 1h dans les Gorges de l’Aveyron à faire en VTT sur environ 8 kilomètres. Depuis Najac, on arrive au cœur de la ville de Saint-Gignac en longeant la rivière de l’Aveyron.`
            },
            {
              title:
                'Quels sont les événements incontournables à faire lors d’un séjour dans la région Midi-Pyrénées ?',
              content: `À Bagnères-de-Luchon, la fête des fleurs s’installe chaque année au mois d’août. De nombreuses animations sont prévues, notamment un défilé de chars fleuris au cœur de la ville. Cette grande fête locale et dépaysante est un véritable moment de bonheur à ne pas manquer. Dans le Gers, le soir du Nouvel An, un événement unique et authentique vous attend ! Quelques parcelles de terre sont sélectionnées pour être vendangées le jour du réveillon. Cette tradition existe depuis un gèle en 1991 qui a poussé les vignerons à faire la récolte au mois de décembre et à leur grande surprise, la récolte a été impressionnante ! Lors des vendanges de la Saint-Sylvestre on profite alors de nombreuses animations et concerts qui mettent de bonne humeur.`
            },
            {
              title: 'Pourquoi passer des vacances dans les Midi-Pyrénées ?',
              content: `Les Midi-Pyrénées proposent des paysages à couper le souffle, vous pourrez vous dépenser lors des randonnées, balades nature ou sorties à vélo !  Prenez le temps de vous ressourcer dans cette région aux paysages uniques. Vous pourrez vous reposer ou vous balader au cœur de la nature ou en sillonnant les rues des jolies villes de la région.  Toute la tribu sera conquise : ses lacs, ses montagnes et ses cascades sauront satisfaire tous les goûts ! Un moment de bonheur en famille ou entre amis fera de votre séjour un voyage inoubliable hors des sentiers battus.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'auvergne':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Auvergne',
        text: `Bienvenue au pays des paysages volcaniques à gogo ! L’Auvergne est the place to be quand l’appel du grand air se fait ressentir. Grâce à ses vallées verdoyantes, ses villages pleins de charme et ses spécialités à s’en rouler par terre, vous ne regretterez pas votre escapade auvergnate !\n\nPassez une nuit dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos chambres d’hôtes</a></b>  dans l’Allier, au petit-matin, vous pourrez partir à la rencontre des villes et villages du coin :  le château de Lapalisse, Vichy, Moulins et les mignonnes communes de Charroux et Le Mayet-de-montagne. Si vous aimez les séjours sportifs alors l’Auvergne est la destination qu’il vous faut : pratiquer la voile à Vichy, le ski dans La Loge-des-Gardes et le canoë-kayak aux alentours d’Ebreuil. Dans le Cantal, on s’endort dans un de <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">nos gîtes éco-responsables</a></b> près des parcs naturels régionaux des volcans d’Auvergne : Plomb du Cantal, Puy Mary, Griou, Violent et Puys de Chavaroche . Envie d’être un peu plus au calme ? On vous propose de vous détendre en vivant une expérience unique dans un de <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite"><b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">nos logements insolites</a></b>  et eco-friendly</a></b> en Haute-Loire et d’être émerveillé par des paysages rayonnants : Mézenc, Vivarais-Lignon, le lac du Bouchet et la Cascade de la Baume.`,
        questions: {
          rows: [
            {
              title:
                'Témoignage de nos hôtes : Pourquoi passer ses futures vacances en Auvergne ?',
              content: `Janette, hôte du Vallon d'Amandine nous parle de sa région : « L’Auvergne-Rhône-Alpes est une région qui a su préserver son identité, je suis très fière de voir que sa population y est donc très attachée. Ses espaces naturels et le volcanisme sont deux aspects très présents dans la région et selon moi, c’est ce qui la différencie des autres. Les paysages que l’on trouve ici sont uniques et on ne pourra pas les trouver ailleurs en France et en Europe !  Quand on me demande quel est le plus joli village de l’Auvergne, j’ai souvent du mal à y répondre car la région compte de nombreux villages authentiques à découvrir. Mais si je devais en choisir un seul, ce serait le village de Montpeyroux qui n’est pas très loin de chez moi. Il se trouve à la limite entre le Puy-de-Dôme et la Haute-Loire. On ne peut pas le louper, il se trouve sur un petit mont, ce qui nous permet de le voir de loin et heureusement car il est classé comme l’un des plus beaux villages de France ! »`
            },
            {
              title: 'Un voyage en Auvergne, pour quel type de séjour ?',
              content: `Pour les amateurs de randonnées ou de VTT, l’Auvergne est une excellente destination. Ses paysages montagneux offrent une vraie façon de se dépasser. Entre montagnes et lacs, les paysages de la région permettent de passer un incroyable séjour nature. Pour ceux qui veulent prendre le temps de profiter de leur voyage et apprécier les grands espaces verts, l'Auvergne est un vrai coin de paradis ! Venir en en famille ou entre amis c’est découvrir une région féerique et des activités en plein air qui plairont aux petits comme aux grands. Pour créer des souvenirs, la région propose différents cadres. Des activités à sensations fortes ou des randonnées pour les sportifs du dimanche, vous aurez le choix pour votre séjour ! De plus, les différentes spécialités de la région sauront mettre les papilles de tout le monde d’accord.`
            },
            {
              title:
                'Quelles sont les spécialités culinaires à déguster en Auvergne ?',
              content: `La Truffade est bien évidemment le premier-plat qui nous vient à l’esprit lorsqu’on pense à l’Auvergne. Ce mélange de tomme fraîche d’Auvergne avec des pommes de terre et un peu d’ail donne un plat divin. Les Bourriols, des sortes de crêpes au blé noir paysannes sont également délicieuses. Du côté sucré, on teste les Pompe aux pommes : une sorte de tourte aux pommes ou bien la tarte Vic, une tarte réalisée avec du lait caillé ou de la tomme.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'bourgogne':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Bourgogne',
        text: `Avec son patrimoine naturel et architectural réputé, des atouts touristiques, une gastronomie appréciée à travers le monde, cette région est unique ! Du bon vin, du bœuf bourguignon et de belles architectures à visiter, pas besoin de plus pour être heureux ! \n\n On se rend dans le département de la Côte-d’Or pour dormir dans l’une de <b><a href="${process.env.NEXT_PUBLIC_URL}/ecolodge">nos écolodges</a></b>  avant de partir sur la route des grands crus de Bourgogne à la recherche du patrimoine vinicole entre le Château du Clos de Vougeot, la petite ville de Nuits-Saint-Georges ou encore la capitale viticole de la Bourgogne : Beaune. Besoin de vous reconnecter avec la nature ? Dormez dans un de nos gîtes d’exception et écologiques avant de vous lancer dans l’une des magnifiques randonnées que possède la Nièvre. On se balade à travers la réserve naturelle du Val de Loire, la forêt des Bertranges et entre Haut Folin et Mont Beuvray où de nombreux sentiers sont tracés. En termes de dépaysement, il n’y a certainement pas mieux que de passer une nuit dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos chambres d’hôtes</a></b>  en Saône-et-Loire où se trouve le somptueux parc naturel régional du Morvan, le Mont Mâconnais et Chalonnais. Pour des vacances encore plus sportives, c’est direction l’Yonne où l’on peut facilement pratiquer le Nautisme, les randonnées équestres, l’escalade et le golf avant de rentrer se reposer dans une de nos chambres d’hôtels tout-confort.
`,
        questions: {
          rows: [
            {
              title:
                'Témoignage de nos hôtes : pourquoi aller en Bourgogne pour ses prochaines vacances ?',
              content: `Annette,<b><a href="${process.env.NEXT_PUBLIC_URL}/hebergements/La-Ferme-Des-Ruats-174">hôte de la Ferme des Ruats</a></b>, nous parle de sa région : « Quand on parle de la Bourgogne, on pense tout de suite à la gastronomie et au vin alors que pour moi c’est surtout la nature, notamment dans le parc régional du Morvan, où je me trouve. Même s’il n’y a pas la mer, le territoire se distingue par sa nature préservée, il n’y a pas d’agriculture intensive donc les forêts sont protégées, et à la fois c’est un territoire assez proche des grands axes. Le plus joli village de la région, pour moi c’est Noyers-sur-Serein, c’est un joli village médiéval très bien préservé qui vaut le détour ! Mais si l’on veut découvrir des spots secrets, je conseille le lac de Saint-Agnan, aussi appelé « le petit Canada », c’est un lac qui a été très préservé. Il n’y a pas d’infrastructure, juste le silence et la nature. »`
            },
            {
              title: 'Partir en Bourgogne, c’est vivre quel type de vacances ?',
              content: `Si vous aimez le sport en pleine nature, vous allez adorer cette région qui propose de nombreuses randonnées et plusieurs véloroutes. Pour un séjour sportif au cœur des reliefs, c’est la Bourgogne qu’il vous faut. Pour une parenthèse nature, il n’y a rien de mieux que de se retrouver dans un cadre féerique où l’on peut prendre du temps pour soi. La Bourgogne propose de nombreux vignobles d’exception pour une immersion en douceur dans la région du vin. Les activités en plein air proposées par la région vous permettront de passer un bon moment en famille et de profiter de la nature tout en s’amusant. Vivre l’aventure entre amis est aussi idéale pour un vrai moment de bonheur. Que vous préfériez les sensations fortes ou bien flâner au bord de l’eau, tout est possible en Bourgogne.`
            },
            {
              title:
                'Quelles sont les villes idéales pour un city-trip dans le coin ?',
              content: `Dijon et ses nombreuses richesses architecturales et culturelles se prêtent parfaitement pour un petit week-end découverte : l’hôtel de ville, le musée des Beaux-arts, la tour Philippe le Bon, la cathédrale Saint-Bénigne ou encore l’hôtel Chambellan sont des richesses à ne pas manquer. Dans l’Yonne, Auxerre est à faire, on y visite l’abbaye Saint-Germain, la cathédrale Saint-Etienne, le quartier médiéval et le vieux centre piétonnier de la commune où on décèle la tour de l’Horloge.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'bourgogne-franche-comte':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Bourgogne-Franche-Comté',
        text: `Avec son patrimoine naturel et architectural réputé, des atouts touristiques, une gastronomie appréciée à travers le monde, cette région est unique ! Du bon vin, du bœuf bourguignon et de belles bâtisses à visiter, pas besoin de plus pour être heureux ! A la lisière de la Suisse, terre de forêts, de reliefs, de lacs et de cascades, la région Bourgogne-Franche-Comté possède deux visages : nature et patrimoine ! \n\n On se rend dans le département de la Côte-d’Or pour dormir dans l’une de nos <b><a href="${process.env.NEXT_PUBLIC_URL}/ecolodge">écolodges</a></b>  avant de partir sur la route des grands crus de Bourgogne à la recherche du patrimoine vinicole entre le Château du Clos de Vougeot, la petite ville de Nuits-Saint-Georges ou encore la capitale viticole de la Bourgogne : Beaune. Besoin de vous reconnecter avec la nature ? Direction le Doubs, dans un de <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">nos gîtes éco-responsables</a></b>  pour passer un moment dans le département le plus vert de France. Là-bas, on s’émerveille du Massif du Mont-d’or, du Lac Saint-Point et du lac de Remoray. En termes de dépaysement, il n’y a certainement pas mieux que de passer une nuit dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos chambres d’hôtes</a></b>  en Saône-et-Loire où se trouve le somptueux parc naturel régional du Morvan, le Mont Mâconnais et Chalonnais. Dormir dans un de <b><a href="${process.env.NEXT_PUBLIC_URL}/hotel">nos hôtels eco-friendly</a></b>  et pépites près de Belfort, c’est se laisser le temps de parcourir la route des villages fleuris, le parc régional naturel des Ballons des Vosges, le Ballon d’Alsace et le lac de Malsaucy.
`,
        questions: {
          rows: [
            {
              title:
                'Témoignage de nos hôtes : pourquoi aller en Bourgogne-Franche-Comté pour ses prochaines vacances ?',
              content: `Annette, hôte de la Ferme des Ruats, nous parle de sa région : « Quand on parle de la Bourgogne, on pense tout de suite à la gastronomie et au vin alors que pour moi c’est surtout la nature, notamment dans le parc régional du Morvan, où je me trouve. Même s’il n’y a pas la mer, le territoire se distingue par sa nature préservée, il n’y a pas d’agriculture intensive donc les forêts sont protégées, et à la fois c’est un territoire assez proche des grands axes. Le plus joli village de la région, pour moi c’est Noyers-sur-Serein, c’est un joli village médiéval très bien préservé qui vaut le détour ! Mais si l’on veut découvrir des spots secrets, je conseille le lac de Saint-Agnan, aussi appelé « le petit Canada », c’est un lac qui a été très préservé. Il n’y a pas d’infrastructure, juste le silence et la nature. »`
            },
            {
              title:
                'Partir en Bourgogne-Franche-Comté, c’est vivre quel type de vacances ?',
              content: `Si vous aimez le sport en pleine nature, vous allez adorer cette région qui propose de nombreuses randonnées et plusieurs véloroutes. Pour un séjour sportif au cœur des reliefs, c’est la Bourgogne qu’il vous faut. Pour une parenthèse nature, il n’y a rien de mieux que de se retrouver dans un cadre féerique où l’on peut prendre du temps pour soi. La Bourgogne-Franche-Comté propose de nombreux vignobles d’exception pour une immersion en douceur dans la région du vin. Les activités en plein air proposées par la région vous permettront de passer un bon moment en famille et de profiter de la nature tout en s’amusant. Vivre l’aventure entre amis est aussi idéale pour un vrai moment de bonheur. Que vous préfériez les sensations fortes ou bien flâner au bord de l’eau, tout est possible en Bourgogne-Franche-Comté.`
            },
            {
              title: 'Que faire avec ses enfants en Bourgogne-Franche-Comté ?',
              content: `Pour profiter de vos vacances en famille, rendez-vous dans la grotte d’Osselle où un parcours insolite dans la plus ancienne nécropole d’ours des cavernes du monde vous attend. Pour toujours plus de fun, on se rend dans le musée du Jouet de Moirans-en-Montagne qui propose une collection de plus de 20000 jouets du monde entier. Pour les chefs cuisiniers en herbe, on optera pour les expositions sur la maison du Comté où les enfants apprendront de quelle manière est fabriqué ce fromage si célèbre.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'alsace-lorraine':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Alsace-Lorraine',
        text: `À deux pas de l’Allemagne, la région Alsace-Lorraine a su garder son histoire et son charme d’antan. Marquée par le passage des Allemands, la région nous dévoile une culture et un paysage unique en France. Vivre des vacances en Alsace-Lorraine, c’est vivre un séjour dépaysant avec des coutumes et des traditions propres au territoire qui ne vous laisseront pas indifférent !\n\nDirection le Bas-Rhin pour passer une <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">nuit insolite</a></b>  sous les étoiles, découvrir la route des vins d’Alsace et randonner dans l’un des magnifiques sites de la région : le parc naturel régional des Vosges du Nord, le Grand Ried ou bien les Vosges moyennes. À quelques kilomètres de là, c’est le Haut-Rhin qui nous accueille pour un petit séjour à l’hôtel dans un de ses villages pittoresques : Riquewihr, Kientzheim, Turckheim, Kaysersberg et Munster. Et si cette année vous logiez dans <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">un de nos gites</a></b> en Meurthe-et-Moselle pour vos vacances d’été avec les enfants ? Ce département riche en culture est l’endroit parfait pour se replonger dans l’histoire du pays à travers la découverte du château de Jaulny, des forts de Vauban et de la Basilique Saint-Nicolas-de-Port. Si on aime le relief, on s’en va dans les Vosges dormir dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos chambres d’hôtes</a></b>  écologiques au pied du parc naturel régional des Ballons des Vosges, de la route des Crêtes ou de la forêt de Sionne à la recherche de paysages inoubliables.
`,
        questions: {
          rows: [
            {
              title:
                'Quelle est la période de l’année la plus favorable pour partir en Alsace-Lorraine ?',
              content: `Toutes les saisons sont agréables, elles ont toutes leurs charmes : paysages, villes et villages changeant au gré des mois de l’année. Cependant, le climat étant continental, le coin peut être enneigé et plutôt froid en hiver et au contraire, étouffant et très fréquenté en été. Selon nous, la meilleure saison pour s’y rendre reste tout de même l’hiver puisque le manteau blanc et les nombreuses décorations de Noël dont la région se pare lui donne un charme unique`
            },
            {
              title:
                'Dans quelle ville se rendre pour un city-trip dans la région ?',
              content: `Pour découvrir une ville riche en patrimoine, on vous propose Nancy : on y explore la place Stanislas, le palais Ducal, l’église et le couvent des Cordeliers ainsi que la basilique Saint-Epvre. Envie de changement ? direction Metz, cette ville datant d’il y a deux milles ans possède de nombreux trésors. Là-bas on flâne pendant des heures à travers les expositions d’art moderne et contemporaine du coin, on se dépayse devant l’architecture de l’opéra-théâtre de Metz Métropole qui nous rappelle le manoir de Harry-Potter et on prend une bonne dose de vitamine D en sirotant une boisson fraîche sur les terrasses de la Place Saint-Louis. Pour être envouté, rien ne vaut Strasbourg et son aura unique, à la fois capitale de l’Alsace et de l’Europe, la ville vaut bien quelques jours d’exploration.`
            },
            {
              title:
                'Quelles sont les spécialités culinaires à déguster lors d’un voyage en Alsace ?',
              content: `Pour commencer, on s’empare de la traditionnelle et conviviale flammekueche, une base crème-lardons-oignons pour les plus routiniers ou un mélange sucré pour les plus curieux. Que serait l’Alsace sans son iconique bretzel qui la symbolise mondialement, gourmand ou minimaliste qu’avec du sel, le bretzel trouvera toujours sa place sur nos tables. Aussi connu que son précédent, le Kougelhopf souvent dégusté sucré existe aussi en version salé, avec sa forme en poterie typique, il réchauffe le cœur de nos invités au moment des premières fraicheurs de l’année.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'poitou-charentes':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Poitou-Charentes',
        text: `Entre patrimoine d’exception, îles sauvages et savoir-faire unique, la Poitou-Charentes est une destination qui a beaucoup à nous dévoiler.  Que ce soit pour des vacances insolites ou authentiques, un séjour en bord de mer, des vacances gourmandes ou encore un trip à la recherche du patrimoine, vous trouverez forcément de quoi vous occuper en Poitou-Charentes !\n\nDormir dans une de nos gîtes en Charente-Maritime cela signifie être à deux pas de l’Océan Atlantique, pouvoir facilement visiter les villes côtières environnantes telles que La Rochelle ou encore Royan et déguster des huîtres bien fraîches dans l’un des petits ports de pêche d’une des îles du coin. On s’en va à Angoulême pour loger dans un de <b><a href="${process.env.NEXT_PUBLIC_URL}/hotel">nos éco-hôtels</a></b>  et prendre un bain de patrimoine dans la capitale de la Charente qui s’impose à travers ses beautés architecturales, son circuit de murs peints et la cité internationale de la BD. Passez une nuit en écolodge dans l’un des départements qui reflète le mieux l’histoire de la France, autrement appelé la Vienne. Là-bas, on découvre de nombreux paysages qui valent le détour tels que le village d’Angles-sur-Anglin, la réserve naturelle de Pinail, la forêt de Scévole, la vallée du Clain et le site gallo-romain de Sanxay.

`,
        questions: {
          rows: [
            {
              title:
                'Quelles sont les activités kids- friendly, à faire en région Poitou-Charentes ?',
              content: `La région regorge de parcs d’attractions en tout genre : Crocodile Planet, le Futuroscope ou encore le Puy du fou qui plairont assurément à vos bambins. La Charente a l’avantage d’être pourvue de nombreuses pistes cyclables qui font d’elle une région parfaite pour le slow tourisme, pourquoi ne pas s’organiser une petite escapade à la découverte des merveilles du coin à l’aide de ce transport qui a tout pour plaire ! Enfin, on vous propose de leur apprendre l’histoire de la récolte du sel et des huîtres à Marennes.`
            },
            {
              title:
                'Quelles sont les îles incontournables à visiter en Poitou-Charentes ?',
              content: `Entre Rochefort et la Rochelle se dévoile 4 îles avec des paysages bien différents mais tous aussi magiques, les uns que les autres. On adore l'île de Ré pour ses petits villages aux ruelles blanches et typiques, ses nombreuses pistes cyclables qui nous permettent d’explorer l’entièreté de l'île à vélo et son célèbre phare des Baleines, grand héritier de l’histoire. L'île d’Oléron, la deuxième plus grande île de France derrière la Corse, nous laisse entrevoir sa nature sauvage : plages de rêve, bassins ostréicoles et petites cabanes de pêcheurs colorées seront au programme de vos visites. Envie d’un coin un peu plus secret ? Alors, direction l’Ile Madame, une île quasi-déserte où l’exploration mêle pêche à pied, carrelets et cadre époustouflant.  Pour un séjour en famille, c’est l'île d’Aix qu’il vous faut ! Cette île accessible seulement en bateau est un vrai petit coin de paradis pour se relaxer tous ensemble à quelques kilomètres des côtes françaises.`
            },
            {
              title: 'Quels sont les plus beaux villages de Poitou-Charentes ?',
              content: `Verteuil-sur-Charente dominé par son château, au bord d’un fleuve, fait partie des plus beaux villages de Charente. La Charente-Maritime regorge de petits trésors à découvrir sans attendre : Meschers sur gironde est célèbre pour sa promenade le long des carrelets, Talmont-sur-Gironde est une cité fortifiée aux ruelles pavées de restaurants, Brouage possède une citadelle datant du XVII e siècle. En Vienne aussi les pépites sont nombreuses, à commencer par La Roche-Posay, la ville nationalement connue pour son eau thermale de grande qualité.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'limousin':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Limousin',
        text: `Dans l’écrin vert de ses forêts, la région Limousin s’impose pour vivre une parenthèse hors-du-temps où nature et culture sont intimement liées. Un terroir, des villages pittoresques, de grands espaces et des expositions, voilà ce qui vous attend pour vos prochaines vacances dans la région Limousin.\n\nPassez une <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">nuit insolite</a></b>  en Creuse c’est se laisser l’opportunité d’admirer des paysages uniques : les monts de Guéret, le site des pierres Jaumâtres, la vallée de la Creuse et le parc naturel régional de Millevaches. Autre département du limousin, la Corrèze est la destination parfaite pour partager des moments inédits et inoubliables avec nos hébergeurs dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos chambres d’hôtes</a></b>  écologiques : randonner sur les voies trans-corréziennes, faire une balade à cheval sur la route des haras de Pompadour à Camps, faire du ski de fond à Bonnefond ou Saint-setiers. La Haute-Vienne est le territoire d’histoire par excellence. On se repose dans un de nos gîtes canons et écoresponsables avant d’aller observer les lieux de mémoire du coin : Oradour-sur-Glane, Peyrat-le-Château, Saint-Gilles-les-Forêts, Limoges… Pour toujours plus d’histoire, les châteaux de la Haute-Vienne sont des visites captivantes retraçant le patrimoine français qu’il ne faut pas manquer lors d’un petit séjour dans la région : Roche-chouart, Lavauguyon, Montbrun, Châlus, Lastours, Aixe-sur-Vienne, Verneuil-sur-Vienne, Bonneval et Châlucet…
`,
        questions: {
          rows: [
            {
              title: 'Quelles sont les villes à visiter dans le Limousin ?',
              content: `Direction Tulle pour un city-trip en mode local, entre sa charpente et son architecture unique, la capitale de la dentelle et de l’accordéon ne vous laissera pas indifférente. En Haute-Vienne c’est à Limoges qu’il faudra vous rendre pour vous promener dans le quartier historique et apercevoir la cathédrale Saint-Etienne, les halles, la gare des bénédictins et s’instruire sur l’histoire de la porcelaine au musée de la Porcelaine Adrien-Dubouché. Pour de l’insolite, on va en Creuse à Aubusson, capitale de la tapisserie, le patrimoine de la ville est important : pont, château, châtelet de la terrade, église Sainte-Croix, vestiges féodaux et maisons à tourelle.`
            },
            {
              title:
                'Quelles sont les spécialités locales à déguster lors d’un séjour dans le Limousin ?',
              content: `En Corrèze, ce sont les truffes, les pommes du limousin, les noix, les châtaignes, le millassou (galette de pommes de terre râpées, ail, persil et lard) et la feuille du limousin (un chèvre fermier en forme de feuille) les spécialités à déguster. En Creuse, on optera pour les fruits rouges, le champignon, la châtaigne, le chou farci et le Gouzon (fromages de chèvre) pour passer un moment convivial autour de bons produits locaux. En ce qui concerne la Haute-Vienne, on ne manquera pas de s’attabler pour se régaler autour du pâté de pommes de terre, du galetou, du massepain et du borgou (pâtisserie à base de châtaigne).`
            },
            {
              title:
                'Partir dans le Limousin, c’est vivre quel type de vacances ?',
              content: `Pour les personnes à la recherche de nouveautés, la randonnée sur le sentier des mégalithes ou le parcours dans la haute-vallée de la Vienne avec le train Vienne Vézère vapeur sont des incontournables. Besoin de bouger ? À vélo, parcourez l’itinéraire Raymond Poulidor, en canoë-kayak, affrontez la descente du Taurion ou de la Vienne, en wakeboard, explorez les fonds marins du lac de Sainte Hélène. Vous rêvez d’un séjour un peu plus insolite ? Alors rendez-vous au musée des distilleries limougeaudes.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'nord-pas-de-calais':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Nord-Pas-de-Calais',
        text: `À la frontière de la Belgique et à deux pas de la mer, le Nord-Pas-de-Calais est bien loin de l’image grise et maussade qu’on lui donne souvent. Là-bas attendez-vous plutôt à être étonné par ses villes médiévales, ses plages immenses, ses festivités, ses marais, dunes et falaises qui témoignent de son passé étonnant !\n\nEnvie de découvrir des paysages époustouflants ? Logez dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos chambres d’hôtes</a></b>  écologiques dans le Nord, où nos hébergeurs vous délivreront leurs meilleures adresses pour vous sentir vraiment loin de chez vous, telles que le parc naturel français de la Scarpe et de l’Escaut, Avesnes-sur-Helpe, la forêt de Mormal et les monts des Flandres à Cassel… Vous aimez les sports de glisse ? On vous propose de dormir dans un <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">de nos gîtes eco-friendly</a></b>  sur le bord de la Manche pour découvrir Le Touquet-Paris-Plage, Berck-sur-Mer, Etaples-sur-Mer et les stations balnéaires de Hardelot, Wimereux, Ambleteuse, Audresselles et Escales.  Pour un séjour insolite de A à Z, on passe une nuit dans un de nos hébergements atypiques et on va skier à Loisinord, se perdre dans le labyrinthe géant des 7 vallées, et être étonné par les quilles en l’air des habitations faites avec des coques de bateaux retournées.
`,
        questions: {
          rows: [
            {
              title: 'Que visiter à Lille dans le Nord-Pas-de-Calais ?',
              content: `Lille est la ville parfaite pour faire un city trip mêlant food et découvertes.  On vous conseille de visiter l’incontournable grande place, le musée de Marguerite Yourcenar, ses jardins magnifiques et de manger sur l’une des tables des grands chefs : le Resto du Bloempot (Florent Ladeyn), Chez mon cousin (Nicolas Pourcheresse), Au Sébastopol (Damien Laforce). Pour dynamiser le voyage, on va se faire les bords de la Deûle à vélo, direction Flandre ou le Parc de la Citadelle pour un écrin de verdure. Vous restez plus longtemps ? Partez de la gare d’Eau à Lomme et allez jusqu’à Wambrechies où vous pourrez visiter la distillerie qui produit le célèbre alcool du coin « Le Genièvre ».`
            },
            {
              title:
                'Quelles sont les activités kidsfriendly à faire dans le Nord-Pas-de-Calais ?',
              content: `Avec les enfants, on se réserve une après-midi à la ferme pédagogique des petits pas Lillers pour qu’ils multiplient leurs connaissances sur les animaux et leur élevage. À Boulogne-sur-Mer, c’est Nausicaa qui révèlera tous des fonds marins aux bambins. Dans cet aquarium, ils découvriront comment les espèces aquatiques évoluent dans leur milieu de vie.  Pour une activité en extérieur épatante, on grimpe sur le dragon de Calais, une construction en acier et bois produite à Nantes qui s’articule en fonction de ses envies, promettant une balade insolite qui ravira petits et grands.`
            },
            {
              title:
                'Quels sont les produits locaux à ramener du Nord-Pas-de-Calais ?',
              content: `Pour les amateurs de fromages, la région en regorge : Maroilles, Bergues, Boulette d’Avesnes, Mont des Cats, Samer, Vieux-Boulogne, Cœur d’Arras, Belval…. Les sucreries sont aussi de la partie dans ce coin de la France : gaufres, chocolats, tartes aux sucres et bonbons (bêtises de Cambrai, babeluttes de Lille) raviront tous les âges. En ce qui concerne les boissons, les bières locales telles que la Castelain ou l’Atrébate, sont très réputées, tout comme l’alcool du coin : le genièvre.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'centre':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Centre-Val de Loire',
        text: `Traversée par la vallée de la Loire, cette région offre un cadre magique à travers ses paysages médiévaux et ses charmants châteaux. Entre visites guidées, vignobles et itinéraires nature, le Centre-Val de Loire est l’idéal pour un pour profiter d’une expérience unique. De plus, la région souhaite mettre en place un futur programme de mobilisation citoyenne en faveur de la biodiversité, pour vous proposer un séjour qui a toujours plus de sens.\n\n Avec ses paysages dignes des plus beaux jardins, ses voies d’eau, son bocage et ses domaines viticoles, le Cher est la région parfaite pour passer du temps en famille dans un de nos <b><a href="${process.env.NEXT_PUBLIC_URL}/gite">gîtes</a></b> écologiques et ruraux. Là-bas, on sillonne les routes pour découvrir le village des potiers dit La Borne, celui des antiquaires dit Nançay et la route des porcelaines. Dans l’Eure-et-Loir, on se la coule douce dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/ecolodge">nos écolodges</a></b>  avant d’aller visiter les monuments aux alentours : la cathédrale de Chartres, le château d’Anet et de Maintenon et les moulins à vent de Frouville-Pensier et Pelard. Direction l’Indre pour passer une <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">nuit insolite</a></b>  et au petit matin découvrir les châteaux : Valençay, Bouges, Ars, Crozant et les églises : Gargilesse, Neuvy, Vic, Méobecq et Fontgombault.`,
        questions: {
          rows: [
            {
              title:
                'Quels sont les châteaux à visiter dans la région Centre ?',
              content: `Le Centre-Val de Loire est la région avec le plus grand nombre de châteaux en France, en couple, en famille ou entre amis, tout le monde voudra découvrir ces incontournables ! Plusieurs activités sont proposées autour des visites. Réalité augmentée, animations médiévales, escape game, visites guidées ou même visites privées... La région fait de son patrimoine un instant inoubliable. Découvrez alors le château de Rigny-Ussé, le célèbre château de La Belle au Bois Dormant ou le parc Leonardo da Vinci du château du Clos Lucé. Des châteaux médiévaux aux châteaux de la Renaissance, vos visites seront magiques !`
            },
            {
              title:
                'Passer un séjour dans la région centre, c’est vivre quel type de séjour ?',
              content: `La région propose plusieurs itinéraires à vélo, à pied et même en kayak pour découvrir différents coins de verdure. Plus de 34 communes de la région ont reçu le label « commune sportive Centre-Val de Loire 2020-​2024 », c’est alors le moment d’en profiter ! L’ambiance médiévale de la région est un vrai dépaysement. Prendre le temps de voyager entre les siècles et découvrir l’histoire de la région avec ses mystères promet une bouffée d’authenticité. Le Centre-Val de Loire propose de nombreuses activités à faire en tribu. Les visites des châteaux plairont à toute la famille pour un voyage dans le temps assuré. Les petits villages typiques d’époque vous emmènent dans d’autres siècles pour vous faire vivre une expérience unique.`
            },
            {
              title:
                'Quelles randonnées faire à pied ou à vélo dans la région Centre ?',
              content: `Au départ de Montrieux-en-Sologne, le joli parcours pédagogique des étangs de Beaumont mène au cœur de la Grande Sologne en environ 3 heures. Entre forêt et étang, la randonnée montre le visage des grands champs de fraises, d’asperges, de framboises et de légumes... La balade est riche en découvertes naturelles et gourmandes ! Dans le Cher, le sentier découverte au bord du canal de Berry offre un patrimoine typique des bords du Cher. Vous y trouverez un espace naturel avec une grande biodiversité. Le patrimoine naturel de la région, la richesse de sa faune et sa flore vous surprendra et rendra votre expérience encore plus immersive ! Pour parcourir la vallée du Loir à vélo, l'itinéraire débute à d'Illiers-Combray et se poursuit jusqu'à Angers. Ici on découvre plusieurs petits bistrots chics à la cuisine généreuse. Chacun à son rythme, on explore le département en famille ou entre amis pour une parenthèse nature sur deux roues ! Les magnifiques points de vue sur l'histoire et sur la nature viennent compléter l’expérience`
            }
          ]
        },
        redirections: null
      }
      break
    case 'picardie':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Picardie',
        text: `Bienvenue en Picardie, un territoire à deux pas de Paris où villages de pierres blanches, vastes forêts et charmantes petites communes s’entremêlent avec étangs, dunes et prés-salés. Entre ces trésors architecturaux et ses vastes espaces verts, la Picardie est la destination idéale pour prendre un bol d’air le temps d’un week-end.\n\n Besoin de vous défouler ? On vous propose de passer un séjour sportif dans L’Aisne, en randonnant à travers cette terre d’histoire et de souvenirs. De nombreuses balades aux paysages époustouflants vous attendent dans ce département : les sources de la Somme et de l’Escaut, l’axe vert, les forêts de Saint Gobain, Retz et Hirson. Après une journée d’effort intensif, vous allez adorer pouvoir vous réchauffer et reprendre des forces dans l’une de <b><a href="${process.env.NEXT_PUBLIC_URL}/ecolodge">nos écolodges</a></b> . Partons dans l’Oise, pour vivre un séjour de vraie déconnexion, on commencera par choisir un <b><a href="${process.env.NEXT_PUBLIC_URL}/insolite">logement insolite et écoresponsable</a></b> puis on partira à la découverte de la réserve naturelle de Saint-Pierre-ès-Champs où l’on peut apercevoir des orchidées en pleine nature. Pour vous sentir vraiment au vert pendant votre séjour, rendez-vous dans la forêt de Compiègne où s’entend la brame des cerfs.  Autre expérience à vivre dans la Somme, passez une nuit dans un de nos gîtes écologiques en Somme avant de vous aventurer sur les lieux de mémoire du coin : le musée Somme 1916, l’historial de la Grande guerre, le belvédère de Vaux…
 
`,
        questions: {
          rows: [
            {
              title: 'Quelles activités faire en Picardie avec des enfants ?',
              content: `Passionné par l’histoire ? On vous propose de vous rendre au cœur de la vallée de la Somme pour partager un moment unique en famille dans les 30 hectares de nature qui composent le parc Samara. Là-bas la préhistoire est au cœur des animations. Direction le parc Carisiolas pour vous replonger dans l’histoire médiévale et le moyen âge. Apprenez à vos enfants à faire du torchis, de la poterie ou à tailler une pierre. Pour une vraie bulle verte, on se rend dans le parc du Marquenterre, composé de 200 hectares de marais au cœur de la baie de Somme, l’endroit est parfait pour observer la faune et la flore.`
            },
            {
              title:
                'Quelles sont les spécialités locales à déguster en Picardie ?',
              content: `Premier petit plat à déguster et pas des moindres, la flamiche aux poireaux. Une sorte de tourte avec une fondue de poireaux à l’intérieur, un vrai délice pour nos papilles. Parlons maintenant de la ficelle picarde, une crêpe fine fourrée au jambon et champignons de Paris, gratinée au four avec crème fraiche et fromage. Côté sucré, le macaron d’Amiens est un vrai délice composé de sucre, miel, blanc d’œuf et extrait d’amande il accompagne parfaitement les petites pause-café.`
            },
            {
              title: 'Quels sont les événements à ne pas manquer en Picardie ?',
              content: `On commencera par le réputé « Champagne et Vous ! », un festival oeno touristique qui met en avant le champagne et son art de vivre dans le Château-Thierry, en octobre. L’occasion unique d’allier patrimoine et festivité la même journée. Chaque année, le quatrième week-end de juillet, on célèbre la fête des baigneurs dans la ville de Mers-Les-Bains. Au programme : parade costumée, expositions de véhicules anciens, promenades en calèche… Direction Compiègne pour la traditionnelle fête du muguet qui ramène chaque année des milliers de personnes. La veille du premier mai, un concert regroupant les artistes en vogue du moment agite la ville. Le lendemain, une reine du muguet est élue, elle colore la journée au fil de ses déplacements.`
            }
          ]
        },
        redirections: null
      }
      break
    case 'champagne-ardenne':
      resp = {
        type: 'region',
        searchTitle: 'Escapade en Champagne-Ardenne',
        text: `Son atmosphère mystérieuse, ses vestiges du Moyen-âge, ses légendes locales et ses forêts denses et profondes font de la Champagne-Ardenne, une destination unique.  Contrastée et authentique, la région vous promet des vacances mémorables à coup sûr ! \n\n Les marcheurs ne seront pas en reste dans les Ardennes. Après s’être fait chouchouter par nos hébergeurs dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/chambre-hote">nos chambres d’hôtes</a></b>  responsables, vous pourrez affronter aisément les quatre chemins de grandes randonnées du territoire : GR14, GR122, GR12 et GR654 .  Dans l’Aube, on s’endort dans une de <b><a href="${process.env.NEXT_PUBLIC_URL}/ecolodge">nos écolodges</a></b>  puis on part découvrir l’histoire du Champagne à La Côte des Bar, haut lieu de production de cette boisson, on reprend la voiture pour se promener sur la route du prestigieux, où l’on peut découvrir d’anciennes cabanes de vignerons, puis on s’échappe dans la forêt d’Orient là où se cache le champagne humide. Attrayante pour ses villes et monuments historiques à visiter, la Haute-Marne est l’endroit parfait pour loger dans un de nos greens hôtel et passer du temps dans les pays du Der, un lieu d’exception pour observer les oiseaux dans leur milieu naturel. En Marne, c’est la visite des villes souterraines qui sont les caves de grandes maisons de champagne qui vous captivera.

 
`,
        questions: {
          rows: [
            {
              title:
                'Quelles activités éco-responsables faire en Champagne-Ardenne ?',
              content: `En été, direction la Meuse pour une balade en canoë-kayak, conté d’histoire, sur ce fleuve historique, où petits et grands s’amuseront sans aucun doute. Une chasse au trésor version truffe ça vous tente ? C’est ce que propose la famille Dumont, une activité qui permet vraiment d’être immergé au milieu de la nature dans la forêt de Clairvaux. En Haute-Marne, le chemin d’essences est un atelier encadré qui vous permettra de passer un moment au milieu de plantes en tous genres. Votre guide, vous aidera à nommer chacune de ces fleurs et vous expliquera quels sont les usages médicinaux et cosmétiques qu’elles peuvent avoir pour que vous soyez incollable sur le sujet !`
            },
            {
              title:
                'Quelles villes faut-il visiter lors d’un voyage en Champagne-Ardenne ?',
              content: `Pour un séjour placé sous le signe du patrimoine, on vous conseille de partir à Reims où vous pourrez découvrir les 4 sites classés au patrimoine mondial de l’Unesco : la cathédrale, le palais du Tau, l’ancienne Abbaye et la basilique Saint-Rémi et les six caves de Champagne depuis 2015. Et si on partait dans la capitale historique du Champagne ? Troyes a su se réinventer aujourd’hui la ville d’art et d’histoire et un lieu parfait pour faire du shopping dû à son industrie de la maille omniprésente. Son centre-ville, « le bouchon de champagne » qui abrite de nombreuses maisons de colombages vaut également le détour. Fan de culture ? Rendez-vous à Charleville -Mézières, la ville connue pour son festival de marionnette est aussi munie de nombreux musées (musée de l’Ardenne, musée de la métallurgie, musée Arthur Rimbaud…)`
            },
            {
              title: 'Peut-on se déplacer à vélo en Champagne-Ardenne ?',
              content: `Avec ses nombreuses voies cyclables et ses petites routes à faible circulation, la Champagne Ardenne représente un véritable terrain de jeu pour les cyclistes. En plus d’être green et bon pour la santé, le vélo nous permet de voyager d’une autre manière pour vivre des expériences encore plus fortes. La Meuse à vélo est par exemple un itinéraire très intéressant reliant Langres à la Mer du Nord en longeant la Meuse, paysages époustouflants à la clef. Le champagne à vélo et les Ardennes à vélo sont également des voies vertes s’étendant sur plus de 400 km qui permettent de traverse
`
            }
          ]
        },
        redirections: null
      }
      break
    default:
      resp = {
        searchTitle: `Nos hébergements`,
        text: null,
        questions: null,
        redirections: null
      }
      break
  }
  return resp
}

export default handleSearchTexts
