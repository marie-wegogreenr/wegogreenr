// HOC/withAuth.jsx
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
// import verifyToken from 'services/verifyToken'
import { getUserDataWithStatus } from 'services/userServices'
import Cookies from 'universal-cookie'
import { getGreenscorePoints } from '../services/greenScoreService'

const withGreenScore = (WrappedComponent) => {
  return (props) => {
    const Router = useRouter()
    const [verified, setVerified] = useState(false)

    // const accessToken = cookies.get('tk_user')

    useEffect(async () => {
      if (!props.user.id) {
        return
      }
      const { greenscore } = await getGreenscorePoints(props.user.id)
      if (greenscore < 44) {
        return Router.replace('/onboarding/greenscore/final-score')
      }
      setVerified(true)
    }, [props.user.id])

    if (verified) {
      return <WrappedComponent {...props} />
    } else {
      return null
    }
  }
}

export default withGreenScore
