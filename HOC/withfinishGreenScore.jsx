// HOC/withAuth.jsx
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
// import verifyToken from 'services/verifyToken'
import { getUserDataWithStatus } from 'services/userServices'
import Cookies from 'universal-cookie'
import { checkGreenscore } from '../services/greenScoreService'

const withFinishGreenScore = (WrappedComponent) => {
  return (props) => {
    const Router = useRouter()
    const [verified, setVerified] = useState(false)

    // const accessToken = cookies.get('tk_user')

    useEffect(async () => {
      if (!props.user.id) {
        return Router.replace('/login')
      }
      const resp = await checkGreenscore(props.user.id)
      if (Array.isArray(resp)) {
        return Router.replace('/onboarding/greenscore/final-score')
      }
      setVerified(true)
    }, [])

    if (verified) {
      return <WrappedComponent {...props} />
    } else {
      return null
    }
  }
}

export default withFinishGreenScore
