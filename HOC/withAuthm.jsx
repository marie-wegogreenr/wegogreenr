// HOC/withAuth.jsx
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
// import verifyToken from 'services/verifyToken'
import { useSelector } from 'react-redux'
import { getUserDataWithStatus } from 'services/userServices'
import Cookies from 'universal-cookie'

const withAuth = (WrappedComponent) => {
  return (props) => {
    const cookies = new Cookies()
    const Router = useRouter()
    const [verified, setVerified] = useState(false)
    // const user = useSelector((state) => state.user.user)

    const accessToken = cookies.get('tk_user')

    useEffect(async () => {
      // if (!accessToken) {
      //   return Router.replace('/login')
      // }
      // const userVerify = await getUserDataWithStatus(accessToken)
      // if (userVerify.code !== 200) {
      //   return Router.replace('/login')
      // }
      setVerified(true)
    }, [])

    if (verified) {
      return <WrappedComponent {...props} />
    } else {
      return null
    }
  }
}

export default withAuth
