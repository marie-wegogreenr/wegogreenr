import { ENABLE_EDITION_TOOLS, DISABLE_EDITION_TOOLS } from './types'

const initialState = {
  isEditionToolsEnabled: false
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case ENABLE_EDITION_TOOLS:
      return {
        isEditionToolsEnabled: true
      }

    case DISABLE_EDITION_TOOLS:
      return {
        isEditionToolsEnabled: false
      }

    default:
      return state
  }
}

export default reducer
