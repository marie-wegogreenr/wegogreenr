import { ENABLE_EDITION_TOOLS, DISABLE_EDITION_TOOLS } from './types'

export function enabledEditionTools() {
  return (dispatch) => {
    dispatch({
      type: ENABLE_EDITION_TOOLS,
      payload: true
    })
  }
}

export function disableEditionTools() {
  return (dispatch) => {
    dispatch({
      type: DISABLE_EDITION_TOOLS,
      payload: false
    })
  }
}
