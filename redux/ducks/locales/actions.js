import { ActionTypes } from './types'
import { getData } from 'utils'

export const getTextByIdAction = (id) => async (dispatch) => {
  const options = {
    url: `${process.env.NEXT_PUBLIC_API_URL}site-texts/${id}`
  }

  const data = await getData(options)

  dispatch({
    type: ActionTypes.GET_TEXT_BY_ID,
    payload: {
      data,
      id
    }
  })
}
