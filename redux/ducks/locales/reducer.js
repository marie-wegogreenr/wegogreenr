import { statuses } from 'constants/index'
import { initialState, ActionTypes } from './types'

function reducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.GET_TEXT_BY_ID:
      return {
        ...initialState,
        data: {
          ...state.data,
          [action.payload.id]: action.payload.data
        }
      }

    default:
      return state
  }
}

export default reducer
