import { statuses } from 'constants/index'

export const initialState = {
  data: null,
  error: null,
  status: statuses.IDLE
}

export const ActionTypes = {
  GET_TEXT_BY_ID: 'GET_TEXT_BY_ID'
}

export default ActionTypes
