import { ActionTypes } from './types'

export const setUrl = (url) => async (dispatch) => {
  dispatch({
    type: ActionTypes.SET_URL,
    payload: url
  })
}

export const removeUrl = () => async (dispatch) => {
  dispatch({
    type: ActionTypes.REMOVE_URL
  })
}
