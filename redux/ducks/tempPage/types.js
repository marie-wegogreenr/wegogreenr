export const initialState = {
  url: ''
}

export const ActionTypes = {
  SET_URL: 'SET_URL',
  REMOVE_URL: 'REMOVE_URL'
}
