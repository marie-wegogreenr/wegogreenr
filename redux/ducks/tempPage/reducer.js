import { ActionTypes, initialState } from './types'

function reducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.SET_URL:
      return {
        url: action.payload
      }

    case ActionTypes.REMOVE_URL:
      return {
        url: ''
      }

    default:
      return state
  }
}

export default reducer
