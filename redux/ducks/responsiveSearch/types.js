export const initialState = {
  top: 'min'
}

export const ActionTypes = {
  SET_CONTAINER_UP: 'SET_CONTAINER_UP',
  SET_CONTAINER_DOWN: 'SET_CONTAINER_DOWN'
}
