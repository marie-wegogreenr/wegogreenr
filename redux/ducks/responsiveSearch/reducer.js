import { initialState, ActionTypes } from './types'

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.SET_CONTAINER_UP:
      return {
        top: 'min'
      }
    case ActionTypes.SET_CONTAINER_DOWN:
      return {
        top: 'max'
      }
    default:
      return initialState
  }
}
