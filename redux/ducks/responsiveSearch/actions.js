import { ActionTypes } from './types'

export const setContainerUp = () => async (dispatch) => {
  dispatch({
    type: ActionTypes.SET_CONTAINER_UP
  })
}

export const setContainerDown = () => async (dispatch) => {
  dispatch({
    type: ActionTypes.SET_CONTAINER_DOWN
  })
}
