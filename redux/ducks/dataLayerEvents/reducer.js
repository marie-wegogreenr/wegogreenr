import { ActionTypes, initialState } from './types'

function reducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.SET_PAGE_VIEW_SENT:
      return {
        ...state,
        pageView: true
      }

    case ActionTypes.SET_PAGE_VIEW_HOLD:
      return {
        ...state,
        pageView: false
      }

    default:
      return state
  }
}

export default reducer
