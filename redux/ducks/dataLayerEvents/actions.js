import { ActionTypes } from './types'

export const setPageViewSent = () => async (dispatch) => {
  dispatch({
    type: ActionTypes.SET_PAGE_VIEW_SENT
  })
}

export const setPageViewHold = () => async (dispatch) => {
  dispatch({
    type: ActionTypes.SET_PAGE_VIEW_HOLD
  })
}
