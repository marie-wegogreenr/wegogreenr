export const initialState = {
  pageView: false
}

export const ActionTypes = {
  SET_PAGE_VIEW_SENT: 'SET_PAGE_VIEW_SENT',
  SET_PAGE_VIEW_HOLD: 'SET_PAGE_VIEW_HOLD'
}
