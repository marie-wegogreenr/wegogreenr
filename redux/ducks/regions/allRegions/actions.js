import { ActionTypes } from '.'

export const getAllRegionsLoading = () => ({
  type: ActionTypes.GET_ALL_REGIONS_LOADING
})

export const getAllRegionsSuccess = (establishment) => ({
  type: ActionTypes.GET_ALL_REGIONS_SUCCESS,
  payload: establishment
})

export const getAllRegionsFailure = (error) => ({
  type: ActionTypes.GET_ALL_REGIONS_FAILURE,
  payload: error
})

export const getAllRegionsIdle = () => ({
  type: ActionTypes.GET_ALL_REGIONS_IDLE
})
