import { statuses } from 'constants/index'

export const initialState = {
  data: null,
  error: null,
  status: statuses.IDLE
}

export const ActionTypes = {
  GET_ALL_REGIONS_LOADING: 'GET_ALL_REGIONS_LOADING',
  GET_ALL_REGIONS_SUCCESS: 'GET_ALL_REGIONS_SUCCESS',
  GET_ALL_REGIONS_FAILURE: 'GET_ALL_REGIONS_FAILURE',
  GET_ALL_REGIONS_IDLE: 'GET_ALL_REGIONS_IDLE'
}

export default ActionTypes
