import { endpoints } from 'constants/index'
import { getData } from 'utils'
import {
  getAllRegionsLoading,
  getAllRegionsSuccess,
  getAllRegionsFailure
} from '.'

export function getAllRegions() {
  return async (dispatch) => {
    dispatch(getAllRegionsLoading())

    const options = {
      url: endpoints.REGIONS,
      method: 'GET'
    }

    return getData(options)
      .then((data) => {
        dispatch(getAllRegionsSuccess(data))
      })
      .catch((err) => {
        dispatch(getAllRegionsFailure(err))
      })
  }
}
