import { SET_LOADING, SET_ADMIN_TAB, SET_CLEAR_LOADING } from './types'

export const setFullLoading = (loading) => ({
  type: SET_LOADING,
  payload: loading
})

export const setAdminTab = (tab) => ({
  type: SET_ADMIN_TAB,
  payload: tab
})

export const setClearLoading = (tab) => ({
  type: SET_CLEAR_LOADING,
  payload: tab
})
