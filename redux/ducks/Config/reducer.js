/// reducer

import { SET_ADMIN_TAB, SET_LOADING, SET_CLEAR_LOADING } from './types'

const initialState = {
  isLoading: false,
  isClear: false,
  aTab: 'usagers'
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: action.payload
      }
    case SET_CLEAR_LOADING:
      return {
        ...state,
        isLoading: action.payload,
        isClear: action.payload
      }
    case SET_ADMIN_TAB:
      return {
        ...state,
        aTab: action.payload
      }
    default:
      return state
  }
}

export default reducer
