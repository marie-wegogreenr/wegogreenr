import { statuses } from 'constants/index'
import { initialState, ActionTypes } from './types'

function reducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.SET_USER_TO_EDIT:
      return {
        ...action.payload
      }

    default:
      return state
  }
}

export default reducer
