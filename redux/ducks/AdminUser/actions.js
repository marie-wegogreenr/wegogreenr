import { ActionTypes } from './types'

export const setUserToEdit = (user) => ({
  type: ActionTypes.SET_USER_TO_EDIT,
  payload: user
})
