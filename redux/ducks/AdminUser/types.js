import { statuses } from 'constants/index'

export const initialState = {}

export const ActionTypes = {
  SET_USER_TO_EDIT: 'SET_USER_TO_EDIT'
}

export default ActionTypes
