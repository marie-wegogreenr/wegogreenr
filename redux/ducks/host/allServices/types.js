import { statuses } from 'constants/index'

export const initialState = {
  data: null,
  error: null,
  status: statuses.IDLE
}

export const ActionTypes = {
  GET_ALL_SERVICES_LOADING: 'GET_ALL_SERVICES_LOADING',
  GET_ALL_SERVICES_SUCCESS: 'GET_ALL_SERVICES_SUCCESS',
  GET_ALL_SERVICES_FAILURE: 'GET_ALL_SERVICES_FAILURE',
  GET_ALL_SERVICES_IDLE: 'GET_ALL_SERVICES_IDLE'
}

export default ActionTypes
