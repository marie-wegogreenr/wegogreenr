import { ActionTypes } from '.'

export const getAllServicesLoading = () => ({
  type: ActionTypes.GET_ALL_SERVICES_LOADING
})

export const getAllServicesSuccess = (services) => ({
  type: ActionTypes.GET_ALL_SERVICES_SUCCESS,
  payload: services
})

export const getAllServicesFailure = (error) => ({
  type: ActionTypes.GET_ALL_SERVICES_FAILURE,
  payload: error
})

export const getAllServicesIdle = () => ({
  type: ActionTypes.GET_ALL_SERVICES_IDLE
})
