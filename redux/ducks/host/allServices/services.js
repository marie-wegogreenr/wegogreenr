import { endpoints } from 'constants/index'
import { getData, getTokenFromCookie } from 'utils'
import {
  getAllServicesLoading,
  getAllServicesSuccess,
  getAllServicesFailure
} from '.'

export function getAllServices() {
  return async (dispatch) => {
    dispatch(getAllServicesLoading())

    const jwt = getTokenFromCookie()
    const options = {
      headers: {
        Authorization: `Bearer ${jwt}`
      },
      method: 'GET'
    }

    return getData({
      ...options,
      url: `${endpoints.GET_ALL_SERVICES}`
    })
      .then((data) => {
        dispatch(getAllServicesSuccess(data))
      })
      .catch((err) => {
        dispatch(getAllServicesFailure(err))
      })
  }
}
