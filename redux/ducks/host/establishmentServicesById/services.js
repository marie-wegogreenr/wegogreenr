import { endpoints } from 'constants/index'
import { getData, getTokenFromCookie } from 'utils'
import {
  getEstablishmentServicesByIdLoading,
  getEstablishmentServicesByIdSuccess,
  getEstablishmentServicesByIdFailure
} from '.'

export function getEstablishmentServicesById(establishmentId) {
  return async (dispatch) => {
    dispatch(getEstablishmentServicesByIdLoading())

    const jwt = getTokenFromCookie()
    const options = {
      headers: {
        Authorization: `Bearer ${jwt}`
      },
      method: 'GET'
    }

    return getData({
      ...options,
      url: `${endpoints.GET_ESTABLISHMENT_SERVICES_BY_ID}${establishmentId}`
    })
      .then((data) => {
        dispatch(getEstablishmentServicesByIdSuccess(data))
      })
      .catch((err) => {
        dispatch(getEstablishmentServicesByIdFailure(err))
      })
  }
}

export function updateEstablishmentServicesById(
  establishmenId,
  services,
  serviceId
) {
  return async (dispatch) => {
    const jwt = getTokenFromCookie()

    const promisesArray = []

    for (const service of Object.keys(services)) {
      const {
        establishmentServiceId,
        price,
        checked,
        breakfastTypeId = ''
      } = services[service]

      function whatDoing() {
        if (checked && establishmentServiceId) return 'UPDATE'
        if (!checked && establishmentServiceId) return 'DELETE'
        if (checked && !establishmentServiceId) return 'ADD'
      }

      const methods = {
        UPDATE: {
          data: {
            establishment_id: establishmenId,
            service_id: serviceId,
            price,
            breakfast_type_id: breakfastTypeId
          },
          url: `${endpoints.UPDATE_ESTABLISHMENT_SERVICE_BY_ID}${establishmentServiceId}`,
          method: 'POST'
        },
        DELETE: {
          url: `${endpoints.DELETE_ESTABLISHMENT_SERVICE_BY_ID}${establishmentServiceId}`,
          method: 'DELETE'
        },
        ADD: {
          data: {
            establishment_id: establishmenId,
            service_id: serviceId,
            price,
            breakfast_type_id: breakfastTypeId
          },
          method: 'POST',
          url: endpoints.ADD_ESTABLISHMENT_SERVICE
        }
      }

      const options = {
        headers: {
          Authorization: `Bearer ${jwt}`
        },
        ...methods[whatDoing()]
      }

      if (!('url' in options)) continue

      promisesArray.push(getData(options).catch(console.error))
    }

    return Promise.allSettled(promisesArray).then(() => {
      dispatch(getEstablishmentServicesById(establishmenId))
    })
  }
}
