import { ActionTypes } from '.'

export const getEstablishmentServicesByIdLoading = () => ({
  type: ActionTypes.GET_ESTABLISHMENT_SERVICES_BY_ID_LOADING
})

export const getEstablishmentServicesByIdSuccess = (establishment) => ({
  type: ActionTypes.GET_ESTABLISHMENT_SERVICES_BY_ID_SUCCESS,
  payload: establishment
})

export const getEstablishmentServicesByIdFailure = (error) => ({
  type: ActionTypes.GET_ESTABLISHMENT_SERVICES_BY_ID_FAILURE,
  payload: error
})

export const getEstablishmentServicesByIdIdle = () => ({
  type: ActionTypes.GET_ESTABLISHMENT_SERVICES_BY_ID_IDLE
})
