import { ActionTypes } from '.'

export const getReservationsByHostIdLoading = () => ({
  type: ActionTypes.GET_RESERVATIONS_BY_HOST_ID_LOADING
})

export const getReservationsByHostIdSuccess = (establishment) => ({
  type: ActionTypes.GET_RESERVATIONS_BY_HOST_ID_SUCCESS,
  payload: establishment
})

export const getReservationsByHostIdFailure = (error) => ({
  type: ActionTypes.GET_RESERVATIONS_BY_HOST_ID_FAILURE,
  payload: error
})

export const getReservationsByHostIdIdle = () => ({
  type: ActionTypes.GET_RESERVATIONS_BY_HOST_ID_IDLE
})

export const updateReservationStatus = (id) => ({
  type: ActionTypes.UPDATE_RESERVATION_STATUS,
  payload: id
})
