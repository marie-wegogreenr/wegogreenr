import { endpoints } from 'constants/index'
import { getData, getTokenFromCookie } from 'utils'
import {
  getReservationsByHostIdLoading,
  getReservationsByHostIdSuccess,
  getReservationsByHostIdFailure
} from '.'

export function getReservationsByHostId(hostId) {
  return async (dispatch) => {
    dispatch(getReservationsByHostIdLoading())

    const jwt = getTokenFromCookie()
    const options = {
      headers: {
        Authorization: `Bearer ${jwt}`
      },
      method: 'GET'
    }

    return getData({
      ...options,
      url: `${endpoints.RESERVATIONS_BY_HOST}/${hostId}`
    })
      .then((data) => {
        dispatch(getReservationsByHostIdSuccess(data))
      })
      .catch((err) => {
        dispatch(getReservationsByHostIdFailure(err))
      })
  }
}
