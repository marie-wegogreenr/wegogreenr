import { statuses } from 'constants/index'
import { ActionTypes, initialState } from '.'

function reducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.GET_RESERVATIONS_BY_HOST_ID_LOADING:
      return {
        ...state,
        status: statuses.LOADING
      }

    case ActionTypes.GET_RESERVATIONS_BY_HOST_ID_SUCCESS:
      return {
        ...state,
        status: statuses.SUCCESS,
        data: action.payload
      }
    case ActionTypes.UPDATE_RESERVATION_STATUS:
      const newData = state.data.map((pags) => {
        return pags.map((d) => {
          if (d.id === action.payload.id) {
            console
            return { ...d, status: action.payload.status }
          } else {
            return d
          }
        })
      })

      return {
        ...state,
        data: newData,
        changes: !state.changes
      }

    case ActionTypes.GET_RESERVATIONS_BY_HOST_ID_FAILURE:
      return {
        ...state,
        status: statuses.FAILURE,
        error: action.payload
      }

    default:
      return state
  }
}

export default reducer
