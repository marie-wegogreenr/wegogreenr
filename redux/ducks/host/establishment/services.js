import { endpoints } from 'constants/index'
import { getData, getTokenFromCookie } from 'utils'
import {
  getEstablishmentLoading,
  getEstablishmentSuccess,
  getEstablishmentFailure
} from '.'

export function getEstablishment(userId, from = 'host') {
  return (dispatch) => {
    dispatch(getEstablishmentLoading())
    const jwt = getTokenFromCookie()
    const options = {
      headers: {
        Authorization: `Bearer ${jwt}`
      },
      method: 'GET'
    }
    if (from == 'admin') {
      getData({
        ...options,
        url: `${endpoints.ESTABLISHMENT_BY_ESTABLISHMENT_ID}${userId}`
      })
        .then((data) => {
          const { 0: establishment } = data
          dispatch(getEstablishmentSuccess(establishment))
        })
        .catch((err) => {
          dispatch(getEstablishmentFailure(err))
        })
    } else {
      getData({
        ...options,
        url: `${endpoints.ESTABLISHMENT_BY_USER_ID}${userId}`
      })
        .then((data) => {
          if (data.length > 0) {
            dispatch(getEstablishmentSuccess(data[0]))
          }
        })
        .catch((err) => {
          dispatch(getEstablishmentFailure(err))
        })
    }
  }
}

export function updateEstablishment(establishmentId, data) {
  return async (dispatch) => {
    const jwt = getTokenFromCookie()

    return getData({
      method: 'PATCH',
      data: data,
      url: `${endpoints.ESTABLISHMENT_BY_USER_ID}${establishmentId}`,
      headers: {
        Authorization: `Bearer ${jwt}`
      }
    })
      .then(() => {
        dispatch(getEstablishmentSuccess(data))
      })
      .catch(Promise.reject)
  }
}
