import { ActionTypes } from '.'

export const getEstablishmentLoading = () => ({
  type: ActionTypes.GET_ESTABLISHMENT_LOADING
})

export const getEstablishmentSuccess = (establishment) => ({
  type: ActionTypes.GET_ESTABLISHMENT_SUCCESS,
  payload: establishment
})

export const getEstablishmentFailure = (error) => ({
  type: ActionTypes.GET_ESTABLISHMENT_FAILURE,
  payload: error
})

export const getEstablishmentIdle = () => ({
  type: ActionTypes.GET_ESTABLISHMENT_IDLE
})
export const deleteEstablishment = () => ({
  type: ActionTypes.DELETE_ESTABLISHMENT
})

export const setEstablishmentId = (id) => ({
  type: ActionTypes.SET_ESTABLISHMENT_ID,
  payload: id
})
