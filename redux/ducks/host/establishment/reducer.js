import { statuses } from 'constants/index'
import { ActionTypes, initialState } from '.'

function reducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.GET_ESTABLISHMENT_LOADING:
      return {
        ...state,
        status: statuses.LOADING
      }

    case ActionTypes.GET_ESTABLISHMENT_SUCCESS:
      return {
        ...state,
        status: statuses.SUCCESS,
        data: action.payload
      }
    case ActionTypes.SET_ESTABLISHMENT_ID:
      return {
        ...state,
        id: action.payload
      }
    case ActionTypes.DELETE_ESTABLISHMENT:
      return {
        ...state,
        id: null
      }
    case ActionTypes.GET_ESTABLISHMENT_FAILURE:
      return {
        ...state,
        status: statuses.FAILURE,
        error: action.payload
      }

    default:
      return state
  }
}

export default reducer
