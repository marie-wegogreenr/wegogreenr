import { endpoints } from 'constants/index'
import { getData, getTokenFromCookie } from 'utils'
import {
  getCountriesLoading,
  getCountriesSuccess,
  getCountriesFailure
} from '.'

export function getCountries() {
  return (dispatch) => {
    dispatch(getCountriesLoading())

    const jwt = getTokenFromCookie()
    const options = {
      headers: {
        Authorization: `Bearer ${jwt}`
      },
      method: 'GET'
    }

    getData({
      ...options,
      url: endpoints.COUNTRIES
    })
      .then((data) => {
        dispatch(getCountriesSuccess(data))
      })
      .catch((err) => {
        dispatch(getCountriesFailure(err))
      })
  }
}
