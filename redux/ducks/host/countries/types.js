import { statuses } from 'constants/index'

export const initialState = {
  data: null,
  error: null,
  status: statuses.IDLE
}

export const ActionTypes = {
  GET_COUNTRIES_LOADING: 'GET_COUNTRIES_LOADING',
  GET_COUNTRIES_SUCCESS: 'GET_COUNTRIES_SUCCESS',
  GET_COUNTRIES_FAILURE: 'GET_COUNTRIES_FAILURE',
  GET_COUNTRIES_IDLE: 'GET_COUNTRIES_IDLE'
}

export default ActionTypes
