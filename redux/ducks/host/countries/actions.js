import { ActionTypes } from '.'

export const getCountriesLoading = () => ({
  type: ActionTypes.GET_COUNTRIES_LOADING
})

export const getCountriesSuccess = (establishment) => ({
  type: ActionTypes.GET_COUNTRIES_SUCCESS,
  payload: establishment
})

export const getCountriesFailure = (error) => ({
  type: ActionTypes.GET_COUNTRIES_FAILURE,
  payload: error
})

export const getCountriesIdle = () => ({
  type: ActionTypes.GET_COUNTRIES_IDLE
})
