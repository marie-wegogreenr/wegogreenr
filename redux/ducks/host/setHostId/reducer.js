import { types } from './types'

export function setHostIdReducer(state = null, action) {
  switch (action.type) {
    case types.HOST_ID_SET:
      return {
        id: action.payload.id
      }
    default:
      return state
  }
}
