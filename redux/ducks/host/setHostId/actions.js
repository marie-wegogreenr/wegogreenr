import { types } from './types'

export const set_host_id_action = (_id) => async (dispatch) => {
  dispatch({
    type: types.HOST_ID_SET,
    payload: {
      id: _id
    }
  })
}
