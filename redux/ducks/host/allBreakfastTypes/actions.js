import { ActionTypes } from '.'

export const getAllBreakfastTypesLoading = () => ({
  type: ActionTypes.GET_ALL_BREAKFAST_TYPES_LOADING
})

export const getAllBreakfastTypesSuccess = (establishment) => ({
  type: ActionTypes.GET_ALL_BREAKFAST_TYPES_SUCCESS,
  payload: establishment
})

export const getAllBreakfastTypesFailure = (error) => ({
  type: ActionTypes.GET_ALL_BREAKFAST_TYPES_FAILURE,
  payload: error
})

export const getAllBreakfastTypesIdle = () => ({
  type: ActionTypes.GET_ALL_BREAKFAST_TYPES_IDLE
})
