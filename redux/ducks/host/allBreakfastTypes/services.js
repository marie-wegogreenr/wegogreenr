import { endpoints } from 'constants/index'
import { getData, getTokenFromCookie } from 'utils'
import {
  getAllBreakfastTypesLoading,
  getAllBreakfastTypesSuccess,
  getAllBreakfastTypesFailure
} from '.'

export function getAllBreakfastTypes() {
  return async (dispatch) => {
    dispatch(getAllBreakfastTypesLoading())

    const jwt = getTokenFromCookie()
    const options = {
      headers: {
        Authorization: `Bearer ${jwt}`
      },
      method: 'GET'
    }

    return getData({
      ...options,
      url: `${endpoints.GET_ALL_BREAKFAST_TYPES}`
    })
      .then((data) => {
        dispatch(getAllBreakfastTypesSuccess(data))
      })
      .catch((err) => {
        dispatch(getAllBreakfastTypesFailure(err))
      })
  }
}
