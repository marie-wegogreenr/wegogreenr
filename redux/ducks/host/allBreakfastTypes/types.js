import { statuses } from 'constants/index'

export const initialState = {
  data: null,
  error: null,
  status: statuses.IDLE
}

export const ActionTypes = {
  GET_ALL_BREAKFAST_TYPES_LOADING: 'GET_ALL_BREAKFAST_TYPES_LOADING',
  GET_ALL_BREAKFAST_TYPES_SUCCESS: 'GET_ALL_BREAKFAST_TYPES_SUCCESS',
  GET_ALL_BREAKFAST_TYPES_FAILURE: 'GET_ALL_BREAKFAST_TYPES_FAILURE',
  GET_ALL_BREAKFAST_TYPES_IDLE: 'GET_ALL_BREAKFAST_TYPES_IDLE'
}

export default ActionTypes
