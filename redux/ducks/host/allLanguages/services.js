import { endpoints } from 'constants/index'
import { getData, getTokenFromCookie } from 'utils'
import {
  getAllLanguagesLoading,
  getAllLanguagesSuccess,
  getAllLanguagesFailure
} from '.'

export function getAllLanguages() {
  return async (dispatch) => {
    dispatch(getAllLanguagesLoading())

    const jwt = getTokenFromCookie()
    const options = {
      headers: {
        Authorization: `Bearer ${jwt}`
      },
      method: 'GET'
    }

    return getData({
      ...options,
      url: endpoints.GET_ALL_LANGUAGES
    })
      .then((data) => {
        dispatch(getAllLanguagesSuccess(data))
      })
      .catch((err) => {
        dispatch(getAllLanguagesFailure(err))
      })
  }
}
