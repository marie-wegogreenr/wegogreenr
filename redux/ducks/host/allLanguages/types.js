import { statuses } from 'constants/index'

export const initialState = {
  data: null,
  error: null,
  status: statuses.IDLE
}

export const ActionTypes = {
  GET_ALL_LANGUAGES_LOADING: 'GET_ALL_LANGUAGES_LOADING',
  GET_ALL_LANGUAGES_SUCCESS: 'GET_ALL_LANGUAGES_SUCCESS',
  GET_ALL_LANGUAGES_FAILURE: 'GET_ALL_LANGUAGES_FAILURE',
  GET_ALL_LANGUAGES_IDLE: 'GET_ALL_LANGUAGES_IDLE'
}

export default ActionTypes
