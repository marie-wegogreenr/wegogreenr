import { ActionTypes } from '.'

export const getAllLanguagesLoading = () => ({
  type: ActionTypes.GET_ALL_LANGUAGES_LOADING
})

export const getAllLanguagesSuccess = (languages) => ({
  type: ActionTypes.GET_ALL_LANGUAGES_SUCCESS,
  payload: languages
})

export const getAllLanguagesFailure = (error) => ({
  type: ActionTypes.GET_ALL_LANGUAGES_FAILURE,
  payload: error
})

export const getAllLanguagesIdle = () => ({
  type: ActionTypes.GET_ALL_LANGUAGES_IDLE
})
