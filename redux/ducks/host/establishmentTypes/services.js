import { endpoints } from 'constants/index'
import { getData, getTokenFromCookie } from 'utils'
import {
  getEstablishmentTypesLoading,
  getEstablishmentTypesSuccess,
  getEstablishmentTypesFailure
} from '.'

export function getEstablishmentTypes() {
  return (dispatch) => {
    dispatch(getEstablishmentTypesLoading())

    const jwt = getTokenFromCookie()
    const options = {
      headers: {
        Authorization: `Bearer ${jwt}`
      },
      method: 'GET'
    }

    getData({
      ...options,
      url: `${endpoints.ESTABLISHMENT_TYPES}`
    })
      .then((data) => {
        dispatch(getEstablishmentTypesSuccess(data))
      })
      .catch((err) => {
        dispatch(getEstablishmentTypesFailure(err))
      })
  }
}
