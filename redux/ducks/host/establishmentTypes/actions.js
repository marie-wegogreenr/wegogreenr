import { ActionTypes } from '.'

export const getEstablishmentTypesLoading = () => ({
  type: ActionTypes.GET_ESTABLISHMENT_TYPES_LOADING
})

export const getEstablishmentTypesSuccess = (establishment) => ({
  type: ActionTypes.GET_ESTABLISHMENT_TYPES_SUCCESS,
  payload: establishment
})

export const getEstablishmentTypesFailure = (error) => ({
  type: ActionTypes.GET_ESTABLISHMENT_TYPES_FAILURE,
  payload: error
})

export const getEstablishmentTypesIdle = () => ({
  type: ActionTypes.GET_ESTABLISHMENT_TYPES_IDLE
})
