import { statuses } from 'constants/index'
import { ActionTypes, initialState } from '.'

function reducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.GET_ESTABLISHMENT_TYPES_LOADING:
      return {
        ...state,
        status: statuses.LOADING
      }

    case ActionTypes.GET_ESTABLISHMENT_TYPES_SUCCESS:
      return {
        ...state,
        status: statuses.SUCCESS,
        data: action.payload
      }

    case ActionTypes.GET_ESTABLISHMENT_TYPES_FAILURE:
      return {
        ...state,
        status: statuses.FAILURE,
        error: action.payload
      }

    default:
      return state
  }
}

export default reducer
