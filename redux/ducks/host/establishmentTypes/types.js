import { statuses } from 'constants/index'

export const initialState = {
  data: null,
  error: null,
  status: statuses.IDLE
}

export const ActionTypes = {
  GET_ESTABLISHMENT_TYPES_LOADING: 'GET_ESTABLISHMENT_TYPES_LOADING',
  GET_ESTABLISHMENT_TYPES_SUCCESS: 'GET_ESTABLISHMENT_TYPES_SUCCESS',
  GET_ESTABLISHMENT_TYPES_FAILURE: 'GET_ESTABLISHMENT_TYPES_FAILURE',
  GET_ESTABLISHMENT_TYPES_IDLE: 'GET_ESTABLISHMENT_TYPES_IDLE'
}

export default ActionTypes
