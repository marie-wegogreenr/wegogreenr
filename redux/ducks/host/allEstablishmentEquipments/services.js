import { endpoints } from 'constants/index'
import { getData, getTokenFromCookie } from 'utils'
import {
  getAllEstablishmentEquipmentsLoading,
  getAllEstablishmentEquipmentsSuccess,
  getAllEstablishmentEquipmentsFailure
} from '.'

export function getAllEstablishmentEquipments() {
  return async (dispatch) => {
    dispatch(getAllEstablishmentEquipmentsLoading())

    const jwt = getTokenFromCookie()
    const options = {
      headers: {
        Authorization: `Bearer ${jwt}`
      },
      method: 'GET'
    }

    return getData({
      ...options,
      url: `${endpoints.GET_ALL_ESTABLISHMENT_EQUIPMENTS}`
    })
      .then((data) => {
        dispatch(getAllEstablishmentEquipmentsSuccess(data))
      })
      .catch((err) => {
        dispatch(getAllEstablishmentEquipmentsFailure(err))
      })
  }
}
