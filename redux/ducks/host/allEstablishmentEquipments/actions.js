import { ActionTypes } from '.'

export const getAllEstablishmentEquipmentsLoading = () => ({
  type: ActionTypes.GET_ALL_ESTABLISHMENT_EQUIPMENTS_LOADING
})

export const getAllEstablishmentEquipmentsSuccess = (establishment) => ({
  type: ActionTypes.GET_ALL_ESTABLISHMENT_EQUIPMENTS_SUCCESS,
  payload: establishment
})

export const getAllEstablishmentEquipmentsFailure = (error) => ({
  type: ActionTypes.GET_ALL_ESTABLISHMENT_EQUIPMENTS_FAILURE,
  payload: error
})

export const getAllEstablishmentEquipmentsIdle = () => ({
  type: ActionTypes.GET_ALL_ESTABLISHMENT_EQUIPMENTS_IDLE
})
