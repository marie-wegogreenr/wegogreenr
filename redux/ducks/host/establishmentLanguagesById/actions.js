import { ActionTypes } from '.'

export const getEstablishmentLanguagesByIdLoading = () => ({
  type: ActionTypes.GET_ESTABLISHMENT_LANGUAGES_BY_ID_LOADING
})

export const getEstablishmentLanguagesByIdSuccess = (languages) => ({
  type: ActionTypes.GET_ESTABLISHMENT_LANGUAGES_BY_ID_SUCCESS,
  payload: languages
})

export const getEstablishmentLanguagesByIdFailure = (error) => ({
  type: ActionTypes.GET_ESTABLISHMENT_LANGUAGES_BY_ID_FAILURE,
  payload: error
})

export const getEstablishmentLanguagesByIdIdle = () => ({
  type: ActionTypes.GET_ESTABLISHMENT_LANGUAGES_BY_ID_IDLE
})
