import { endpoints } from 'constants/index'
import { getData, getTokenFromCookie } from 'utils'
import {
  getEstablishmentLanguagesByIdLoading,
  getEstablishmentLanguagesByIdSuccess,
  getEstablishmentLanguagesByIdFailure
} from '.'

export function getEstablishmentLanguagesById(establishmentId) {
  return async (dispatch) => {
    dispatch(getEstablishmentLanguagesByIdLoading())

    const jwt = getTokenFromCookie()
    const options = {
      headers: {
        Authorization: `Bearer ${jwt}`
      },
      method: 'GET'
    }

    return getData({
      ...options,
      url: `${endpoints.GET_ESTABLISHMENT_LANGUAGES_BY_ID}${establishmentId}`
    })
      .then((data) => {
        dispatch(getEstablishmentLanguagesByIdSuccess(data))
      })
      .catch((err) => {
        dispatch(getEstablishmentLanguagesByIdFailure(err))
      })
  }
}

export function updateEstablishmentLanguagesById(
  establishmentId,
  languages = {}
) {
  return async function (dispatch) {
    const languagePromises = []
    const jwt = getTokenFromCookie()

    for (const id in languages) {
      const checked = languages[id]

      const options = {
        method: 'POST',
        headers: {
          authorization: `Bearer ${jwt}`
        },
        data: { establishment_id: establishmentId, language_id: id },
        url: checked
          ? endpoints.ADD_ESTABLISHMENT_LANGUAGE_BY_ID
          : endpoints.DELETE_ESTABLISHMENT_LANGUAGE_BY_ID
      }

      languagePromises.push(getData(options))
    }

    return Promise.allSettled(languagePromises).then(() => {
      dispatch(getEstablishmentLanguagesById(establishmentId))
    })
  }
}
