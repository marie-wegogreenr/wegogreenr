import { statuses } from 'constants/index'
import { ActionTypes, initialState } from '.'

function reducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.GET_ESTABLISHMENT_EQUIPMENTS_BY_ID_LOADING:
      return {
        ...state,
        status: statuses.LOADING
      }

    case ActionTypes.GET_ESTABLISHMENT_EQUIPMENTS_BY_ID_SUCCESS:
      return {
        ...state,
        status: statuses.SUCCESS,
        data: action.payload
      }

    case ActionTypes.GET_ESTABLISHMENT_EQUIPMENTS_BY_ID_FAILURE:
      return {
        ...state,
        status: statuses.FAILURE,
        error: action.payload
      }

    default:
      return state
  }
}

export default reducer
