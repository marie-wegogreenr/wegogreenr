import { endpoints } from 'constants/index'
import { getData, getTokenFromCookie } from 'utils'
import {
  getEstablishmentEquipmentsByIdLoading,
  getEstablishmentEquipmentsByIdSuccess,
  getEstablishmentEquipmentsByIdFailure
} from '.'

export function getEstablishmentEquipmentsById(establishmentId) {
  return async (dispatch) => {
    dispatch(getEstablishmentEquipmentsByIdLoading())

    const jwt = getTokenFromCookie()
    const options = {
      headers: {
        Authorization: `Bearer ${jwt}`
      },
      method: 'GET'
    }

    return getData({
      ...options,
      url: `${endpoints.GET_ESTABLISHMENT_EQUIPMENTS_BY_ID}${establishmentId}`
    })
      .then((data) => {
        dispatch(getEstablishmentEquipmentsByIdSuccess(data))
      })
      .catch((err) => {
        dispatch(getEstablishmentEquipmentsByIdFailure(err))
      })
  }
}

export function updateEstablishmentEquipmentsById(establishmentId, equipments) {
  return () => {
    const jwt = getTokenFromCookie()

    const promiseArray = []

    for (const e of Object.keys(equipments)) {
      const options = {
        headers: {
          Authorization: `Bearer ${jwt}`
        },
        data: {
          establishment_id: establishmentId,
          equipment_id: Number(e)
        },
        method: 'POST',
        url: equipments[e]
          ? endpoints.ADD_ESTABLISHMENT_EQUIPMENTS_BY_ID
          : endpoints.DELETE_ESTABLISHMENT_EQUIPMENTS_BY_ID
      }

      promiseArray.push(getData(options).catch(console.error))
    }

    return Promise.allSettled(promiseArray)
  }
}
