import { ActionTypes } from '.'

export const getEstablishmentEquipmentsByIdLoading = () => ({
  type: ActionTypes.GET_ESTABLISHMENT_EQUIPMENTS_BY_ID_LOADING
})

export const getEstablishmentEquipmentsByIdSuccess = (establishment) => ({
  type: ActionTypes.GET_ESTABLISHMENT_EQUIPMENTS_BY_ID_SUCCESS,
  payload: establishment
})

export const getEstablishmentEquipmentsByIdFailure = (error) => ({
  type: ActionTypes.GET_ESTABLISHMENT_EQUIPMENTS_BY_ID_FAILURE,
  payload: error
})

export const getEstablishmentEquipmentsByIdIdle = () => ({
  type: ActionTypes.GET_ESTABLISHMENT_EQUIPMENTS_BY_ID_IDLE
})
