import Cookies from 'universal-cookie'
import fetch from 'isomorphic-fetch'
import { toast } from 'react-toastify'
//CONSTANTES
const initialData = {
  status: 404,
  user: {},
  error: {},
  auth: false,
  role: '',
  reservations: [],
  all_reservations: []
}

//TYPES
const USER_GET_DATA = 'USER_GET_DATA'
const USER_GET_RESERVATIONS = 'USER_GET_RESERVATIONS'
const USER_UPDATE_INFO = 'USER_UPDATE_INFO'
const USER_AUTH = 'USER_AUTH'
const SET_ROLE = 'SET_ROLE'
const USER_UNAUTH = 'USER_UNAUTH'
const USER_UPDATING = 'USER_UPDATING'
const USER_UPDATED = 'USER_UPDATED'
const USER_UPDATE_MESSAGE = 'USER_UPDATE_MESSAGE'
const USER_GET_ALL_RESERVATIONS = 'USER_GET_ALL_RESERVATIONS'
const USER_UPDATE_RESERVATIONS_INFO = 'USER_UPDATE_RESERVATIONS_INFO'
const USER_UPDATE_RESERVATIONS_ESTABLISHMENT =
  'USER_UPDATE_RESERVATIONS_ESTABLISHMENT'
const USER_HOST_GET_ESTABLISHMENT = 'USER_HOST_GET_ESTABLISHMENT'
const USER_HOST_GET_ROOMS = 'USER_HOST_GET_ROOMS'
const DELETE_USER = 'DELETE_USER'

//REDUCER

export default function userReducer(state = initialData, action) {
  const reservations = state.reservations

  switch (action.type) {
    case DELETE_USER:
      return {
        ...state,
        user: {}
      }
    case USER_AUTH:
      return {
        ...state,
        auth: true
      }
    case SET_ROLE:
      return {
        ...state,
        role: action.payload
      }
    case USER_UNAUTH:
      return {
        ...state,
        auth: false
      }
    case USER_GET_DATA:
      return {
        ...state,
        user: action.payload,
        status: 200
      }
    case USER_GET_RESERVATIONS:
      return {
        ...state,
        reservations: action.payload
      }
    case USER_UPDATE_INFO:
      return {
        ...state,
        user: action.payload
      }
    case USER_UPDATING:
      return {
        ...state,
        status: 'UPDATING'
      }
    case USER_UPDATE_MESSAGE:
      return {
        ...state,
        status: action.payload.message
      }
    case USER_UPDATED:
      return {
        ...state,
        status: `${action.payload} updated succesfully` ?? 'UPDATED'
      }
    case USER_GET_ALL_RESERVATIONS:
      return {
        ...state,
        all_reservations: action.payload
      }
    case USER_UPDATE_RESERVATIONS_INFO:
      let { id, images } = action.payload
      //consoleLog(images, 'images')
      for (let i = 0; i < reservations.length; i++) {
        if (reservations[i].room.id === id) {
          reservations[i] = { ...reservations[i], images }
        }
      }
      return {
        ...state,
        reservations,
        status: 'IMAGES UPDATED'
      }
    case USER_UPDATE_RESERVATIONS_ESTABLISHMENT:
      const { establishment } = action.payload

      for (let i = 0; i < reservations.length; i++) {
        if (reservations[i].room.id === action.payload.id) {
          reservations[i] = {
            ...reservations[i],
            establishment: establishment[0]
          }
        }
      }
      if (reservations.every((res) => res.establishment !== undefined)) {
        return {
          ...state,
          reservations,
          status: 'UPDATED'
        }
      }
      return {
        ...state,
        reservations,
        status: 'UPDATING ESTABLISHMENT'
      }

    case USER_HOST_GET_ESTABLISHMENT:
      return {
        ...state,
        user: {
          ...state.user,
          establishment: action.payload
        }
      }

    case USER_HOST_GET_ROOMS:
      return {
        ...state,
        user: {
          ...state.user,
          establishmentRooms: action.payload
        }
      }

    default:
      return state
  }
}

export const deleteUser = (status) => (dispatch) => {
  dispatch({
    type: DELETE_USER,
    payload: ''
  })
}

export const setRole = (role) => (dispatch) => {
  dispatch({
    type: SET_ROLE,
    payload: role
  })
}

export const setUserData = (user) => (dispatch) => {
  dispatch({
    type: USER_GET_DATA,
    payload: user
  })
}

export const getUserDataAction = () => (dispatch, getState) => {
  const cookies = new Cookies()

  const userToken = cookies.get('tk_user')

  if (userToken) {
    fetch(`${process.env.NEXT_PUBLIC_API_URL}auth/user`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`
      },
      method: 'GET'
    })
      .then((res) => res.json())
      .then((data) =>
        dispatch({
          type: USER_GET_DATA,
          payload: data
        })
      )
  }
}

export const getUserReservationsAction = () => (dispatch, getState) => {
  const cookies = new Cookies()
  const userToken = cookies.get('tk_user')
  let store = getState().user

  fetch(
    `${process.env.NEXT_PUBLIC_API_URL}user-reservations/${store.user.id}`,
    {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`
      },
      method: 'GET'
    }
  )
    .then((res) => res.json())
    .then((data) => {
      if (JSON.stringify(store.reservations) !== JSON.stringify(data)) {
        dispatch({
          type: USER_GET_RESERVATIONS,
          payload: data
        })
      }
    })
}

export const updateUserInfoAction =
  (field, value, id_user = '', admin, userData) =>
  async (dispatch, getState) => {
    const cookies = new Cookies()
    const userToken = cookies.get('tk_user')
    const user = admin ? userData : getState().user.user

    if (field !== 'password') {
      user[field] = value
    }

    const {
      name,
      email,
      birthday,
      lastname,
      address,
      country_id,
      phone,
      postal_code,
      gender,
      city
    } = user

    const form = {
      phone,
      name,
      email,
      birthday,
      lastname,
      address,
      country_id,
      postal_code,
      gender,
      city,
      [field]: value,
      user_id: id_user
    }

    const options = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`
      },
      method: 'POST',
      body: JSON.stringify(form)
    }

    dispatch({
      type: USER_UPDATING
    })

    if (userToken) {
      try {
        const res = await fetch(
          `${process.env.NEXT_PUBLIC_API_URL}update-user`,
          options
        )
        const data = await res.json()

        dispatch({
          type: USER_UPDATED,
          payload: field
        })
      } catch (error) {
        dispatch({
          type: USER_UPDATED,
          payload: 'Error Updating'
        })
      }

      dispatch({
        type: USER_UPDATE_INFO,
        payload: user
      })
    }
  }

export const logUserAction = (data) => async (dispatch, getData) => {
  const { password } = data
  const user = getData().user.user

  const form = {
    password,
    email: user.email
  }

  const options = {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'POST',
    body: JSON.stringify(form)
  }

  try {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_API_URL}auth/login`,
      options
    )
    const resData = await res.json()
    dispatch({
      type: USER_AUTH,
      payload: resData
    })
  } catch (err) {
    toast.error('mot de passe invalide', {
      position: toast.POSITION.BOTTOM_LEFT
    })
  }
}

export const unAuthUserAction = () => (dispatch) => {
  dispatch({
    type: USER_UNAUTH,
    payload: false
  })
}

export const getUserAllReservationsAction = () => (dispatch, getState) => {
  const cookies = new Cookies()
  const userToken = cookies.get('tk_user')

  fetch(`${process.env.NEXT_PUBLIC_API_URL}all-reservations`, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${userToken}`
    },
    method: 'GET'
  })
    .then((res) => res.json())
    .then((data) =>
      dispatch({
        type: USER_GET_ALL_RESERVATIONS,
        payload: data
      })
    )
}

export const getUserReservationsAditionalAction =
  () => async (dispatch, getState) => {
    const cookies = new Cookies()
    const userToken = cookies.get('tk_user')
    const reservations = getState().user.reservations

    if (userToken) {
      const options = {
        headers: {
          Authorization: `Bearer ${userToken}`
        }
      }

      reservations.forEach((res) => {
        fetch(
          `${process.env.NEXT_PUBLIC_API_URL}room-images/${res.room.id}`,
          options
        )
          .then((response) => response.json())
          .then((data) => {
            dispatch({
              type: USER_UPDATE_RESERVATIONS_INFO,
              payload: { id: res.room.id, images: data }
            })
          })
          .finally(() => {
            dispatch({
              type: USER_UPDATE_MESSAGE,
              payload: {
                message: 'IMAGE_UPDATED'
              }
            })
          })
      })
    }
  }

export const getHostEstablishmentAction = () => async (dispatch, getState) => {
  const userId = getState().user.user.id
  const cookies = new Cookies()
  const userToken = cookies.get('tk_user')
  const reservations = getState().user.reservations

  if (userToken) {
    const options = {
      headers: {
        Authorization: `Bearer ${userToken}`
      }
    }
    fetch(`${process.env.NEXT_PUBLIC_API_URL}establishments/${userId}`, options)
      .then((res) => res.json())
      .then((data) =>
        dispatch({
          type: USER_HOST_GET_ESTABLISHMENT,
          payload: data
        })
      )
  }
}

export const getHostRoomsAction = () => async (dispatch, getState) => {
  let establishmentId
  if (getState().user.user.establishment.length) {
    establishmentId = getState().user.user.establishment[0].id
  }
  const cookies = new Cookies()
  const userToken = cookies.get('tk_user')

  //consoleLog(establishmentId, 'estID')
  if (userToken) {
    const options = {
      headers: {
        Authorization: `Bearer ${userToken}`
      }
    }
    fetch(
      `${process.env.NEXT_PUBLIC_API_URL}establishment-rooms/${establishmentId}`,
      options
    )
      .then((res) => res.json())
      .then((data) =>
        dispatch({
          type: USER_HOST_GET_ROOMS,
          payload: data
        })
      )
  }
}
