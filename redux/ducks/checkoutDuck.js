import fetch from 'isomorphic-fetch'

//CONSTANTES
const initialData = {
  reservation: {
    total_price: 0,
    adults: 0,
    children: 0,
    giftcard_value: 0,
    additionals: {},
    discount: 0
  },
  paymentData: {
    reservation_id: '',
    total_nigths: '',
    payment_intent: ''
  },
  loaded: true,
  valid: true,
  ssr: false
}

//TYPES
const POST_RESERVATION = 'POST_RESERVATION'
const CLEAN_RESERVATION = 'CLEAN_RESERVATION'
const SAVE_PAYMENT_DATA = 'SAVE_PAYMENT_DATA'
const CLEAN_PAYMENT_DATA = 'CLEAN_PAYMENT_DATA'
const UPDATE_RESERVATION_DATES = 'UPDATE_RESERVATION_DATES'
const UPDATE_RESERVATION_USERS = 'UPDATE_RESERVATION_USERS'
const UPDATE_RESERVATION_PRICE = 'UPDATE_RESERVATION_PRICE'
const UPDATE_RESERVATION_STATUS = 'UPDATE_RESERVATION_STATUS'

//REDUCER

export default function checkoutReducer(state = initialData, action) {
  switch (action.type) {
    case POST_RESERVATION:
      return {
        ...state,
        reservation: { ...state.reservation, ...action.payload },
        loaded: true
      }
    case SAVE_PAYMENT_DATA:
      return {
        ...state,
        paymentData: action.payload
      }
    case CLEAN_PAYMENT_DATA:
      return {
        ...state,
        paymentData: initialData.paymentData
      }
    case CLEAN_RESERVATION:
      return {
        ...state,
        reservation: initialData.reservation
      }
    case UPDATE_RESERVATION_DATES:
      const { checkIn, checkOut } = action.payload
      return {
        ...state,
        reservation: {
          ...state.reservation,
          checkIn,
          checkOut
        }
      }
    case UPDATE_RESERVATION_USERS:
      return {
        ...state,
        reservation: action.payload
      }
    case UPDATE_RESERVATION_PRICE:
      return {
        ...state,
        reservation: action.payload
      }
    case UPDATE_RESERVATION_STATUS:
      return {
        ...state,
        valid: action.payload
      }

    default:
      return state
  }
}

//ACTIONS

export const postReservationAction = (reservation) => (dispatch, getState) => {
  dispatch({
    type: POST_RESERVATION,
    payload: reservation
  })
}

export const cleanReservationAction = () => (dispatch, getState) => {
  dispatch({
    type: CLEAN_RESERVATION
  })
}

export const savePaymentData = (paymentData) => (dispatch, getState) => {
  dispatch({
    type: SAVE_PAYMENT_DATA,
    payload: paymentData
  })
}

export const cleanPaymentData = () => (dispatch, getState) => {
  dispatch({
    type: CLEAN_PAYMENT_DATA
  })
}

export const updateReservationDatesAction = (dates) => (dispatch, getState) => {
  const { checkIn, checkOut } = dates
  const store = getState().checkoutReducer.reservation

  dispatch({
    type: UPDATE_RESERVATION_DATES,
    payload: { checkIn, checkOut }
  })
}

export const updateReservationUsersAction = (users) => (dispatch, getState) => {
  const { adults, children } = users
  const store = getState().checkoutReducer.reservation
  store['adults'] = Number(adults)
  store['children'] = Number(children)

  dispatch({
    type: UPDATE_RESERVATION_USERS,
    payload: store
  })
}

export const updateReservationPriceAction = (price) => (dispatch, getState) => {
  dispatch({
    type: UPDATE_RESERVATION_PRICE,
    payload: price
  })
}

export const updateReservationStatusAction = (status) => (dispatch) => {
  dispatch({
    type: UPDATE_RESERVATION_STATUS,
    payload: status
  })
}
