import { consoleLog } from 'helpers/utilsHelper'
import {
  ADD_AVAILABILITY,
  ADD_SPECIAL_AVAILABILITY,
  DELETE_AVAILABILITY,
  DELETE_SPECIAL_AVAILABILITY,
  SET_CALENDAR_INFO,
  UPDATE_SPECIAL_AVAILABILITY
} from './types'

const initialState = {
  unavailableDays: [],
  customDays: [],
  icals: [],
  specialDates: [],
  isDataLoaded: false
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_CALENDAR_INFO:
      return {
        ...state,
        unavailableDays: action.payload.calendar,
        customDays: action.payload.mappedUnavailableDays,
        icals: action.payload.icals,
        specialDates: action.payload.specialDates,
        isDataLoaded: true
      }

    case DELETE_AVAILABILITY:
      const filter = state.unavailableDays.filter(
        (day) => day.id !== action.payload
      )
      return {
        ...state,
        unavailableDays: filter
      }
    case ADD_AVAILABILITY:
      return {
        ...state,
        unavailableDays: [...state.unavailableDays, action.payload]
      }
    case ADD_SPECIAL_AVAILABILITY:
      return {
        ...state,
        specialDates: [...state.specialDates, ...action.payload]
        // customDays: [...state.customDays, ...action.payload]
      }
    case UPDATE_SPECIAL_AVAILABILITY:
      const specialDatesFiltered = state.specialDates.filter(
        (sd) =>
          sd.room_special_availability_price.id !==
          action.payload.room_special_availability_price.id
      )
      return {
        ...state,
        specialDates: [...specialDatesFiltered, action.payload]
      }
    case DELETE_SPECIAL_AVAILABILITY:
      const filterSpecialDates = state.specialDates.filter(
        (day) => day.id !== action.payload
      )
      return {
        ...state,
        specialDates: filterSpecialDates
      }
    default:
      return state
  }
}

export default reducer
