import {
  addColor,
  mapUnavailableDays,
  mapCalendar,
  mapNewUnavailability,
  mapNewSpecialPrice
} from 'helpers/calendarHelper'
import {
  getUnavailableDays,
  getIcals,
  setToAvailable,
  deleteSpecialAvailabilities,
  setToUnavailable,
  addSpecialAvailability,
  deleteSpecialAvailability,
  updateSpecialAvailability
} from 'services/hebergementsService'
import {
  ADD_AVAILABILITY,
  ADD_SPECIAL_AVAILABILITY,
  DELETE_AVAILABILITY,
  DELETE_SPECIAL_AVAILABILITY,
  SET_CALENDAR_INFO,
  UPDATE_SPECIAL_AVAILABILITY
} from './types'

import { toast } from 'react-toastify'

export function setInitialCalendar(id) {
  return async (dispatch) => {
    try {
      const icals = await getIcals(id)
      const unavailableDays = await getUnavailableDays(id)

      const specialDates = unavailableDays.filter((sd) => sd.type === 4)
      let filteredData = unavailableDays.filter((sd) => sd.type !== 4)
      const mappedIcals = addColor(icals)
      const mappedUnavailableDays = mapUnavailableDays(
        filteredData,
        mappedIcals
      )

      const calendar = mapCalendar(filteredData)
      dispatch({
        type: SET_CALENDAR_INFO,
        payload: { mappedUnavailableDays, icals, calendar, specialDates }
      })
    } catch (error) {
      toast.error('Il y a eu une erreur :(', {
        position: toast.POSITION.BOTTOM_LEFT
      })
    }
  }
}

export function changeToAvailable(body, id) {
  return async (dispatch) => {
    try {
      await setToAvailable(body, id)

      dispatch({
        type: DELETE_AVAILABILITY,
        payload: id
      })
    } catch (error) {
      toast.error('Il y a eu une erreur  :(', {
        position: toast.POSITION.BOTTOM_LEFT
      })
    }
  }
}

export function changeToUnavailable(body) {
  return async (dispatch) => {
    try {
      const resp = await setToUnavailable(body)
      dispatch({
        type: ADD_AVAILABILITY,
        payload: mapNewUnavailability(resp.data)
      })
    } catch (error) {
      console.error(error)
    }
  }
}

export function deleteSpecialPrice(id) {
  return async (dispatch) => {
    try {
      await deleteSpecialAvailability(id)

      dispatch({
        type: DELETE_SPECIAL_AVAILABILITY,
        payload: id
      })
    } catch (error) {
      // dispatch(getHebergementTypesFailure(error))
    }
  }
}

export function addSpecialPrice(body) {
  return async (dispatch) => {
    try {
      const { data } = await addSpecialAvailability(body)

      dispatch({
        type: ADD_SPECIAL_AVAILABILITY,
        payload: mapNewSpecialPrice(data)
      })
    } catch (error) {
      toast.error('Il y a eu une erreur  :(', {
        position: toast.POSITION.BOTTOM_LEFT
      })
    }
  }
}

export function addRangeSpecialPrice(body) {
  return async (dispatch) => {
    try {
      const { data } = await addSpecialAvailability(body)

      dispatch({
        type: ADD_SPECIAL_AVAILABILITY,
        payload: data
      })

      toast.success('Prix personnalisé created', {
        position: toast.POSITION.BOTTOM_LEFT
      })
    } catch (error) {
      toast.error('Il y a eu une erreur  :(', {
        position: toast.POSITION.BOTTOM_LEFT
      })
    }
  }
}

export function updateRangeSpecialPrice(body) {
  const { id, ...rest } = body
  return async (dispatch) => {
    try {
      const { data } = await updateSpecialAvailability(rest, id)
      dispatch({
        type: UPDATE_SPECIAL_AVAILABILITY,
        payload: data[0]
      })

      toast.success('Prix personnalisé édité', {
        position: toast.POSITION.BOTTOM_LEFT
      })
    } catch (error) {
      toast.error('Il y a eu une erreur  :(', {
        position: toast.POSITION.BOTTOM_LEFT
      })
    }
  }
}
