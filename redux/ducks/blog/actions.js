import { ActionTypes } from './types'
import { getData } from 'utils'

export const getPostsFromBlogAction = () => async (dispatch) => {
  const options = {
    url: `${process.env.NEXT_PUBLIC_BLOG_URL}wp-json/wp/v2/posts`
  }

  const data = await getData(options)

  dispatch({
    type: ActionTypes.GET_BLOG_ARTICLES,
    payload: data
  })
}
