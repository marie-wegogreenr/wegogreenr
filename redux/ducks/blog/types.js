import { statuses } from 'constants/index'

export const initialState = {
  data: null,
  error: null,
  status: statuses.IDLE
}

export const ActionTypes = {
  GET_BLOG_ARTICLES: 'GET_BLOG_ARTICLES'
}

export default ActionTypes
