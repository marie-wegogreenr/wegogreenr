import { statuses } from 'constants/index'
import { initialState, ActionTypes } from './types'

function reducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.GET_BLOG_ARTICLES:
      return {
        ...initialState,
        data: action.payload
      }

    default:
      return state
  }
}

export default reducer
