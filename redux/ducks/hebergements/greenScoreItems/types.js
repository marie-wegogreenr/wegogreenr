import { statuses } from 'constants/index'

export const initialState = {
  data: null,
  error: null,
  status: statuses.IDLE
}

export const ActionTypes = {
  GET_GREEN_SCORE_ITEMS_BY_USER_ID_LOADING:
    'GET_GREEN_SCORE_ITEMS_BY_USER_ID_LOADING',
  GET_GREEN_SCORE_ITEMS_BY_USER_ID_SUCCESS:
    'GET_GREEN_SCORE_ITEMS_BY_USER_ID_SUCCESS',
  GET_GREEN_SCORE_ITEMS_BY_USER_ID_FAILURE:
    'GET_GREEN_SCORE_ITEMS_BY_USER_ID_FAILURE',
  GET_GREEN_SCORE_ITEMS_BY_USER_ID_IDLE: 'GET_GREEN_SCORE_ITEMS_BY_USER_ID_IDLE'
}

export default ActionTypes
