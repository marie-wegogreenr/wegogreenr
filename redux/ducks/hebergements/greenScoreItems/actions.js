import { ActionTypes } from '.'

export const getGreenScoreItemsByIdLoading = () => ({
  type: ActionTypes.GET_GREEN_SCORE_ITEMS_BY_USER_ID_LOADING
})

export const getGreenScoreItemsByIdSuccess = (items) => ({
  type: ActionTypes.GET_GREEN_SCORE_ITEMS_BY_USER_ID_SUCCESS,
  payload: items
})

export const getGreenScoreItemsByIdFailure = (error) => ({
  type: ActionTypes.GET_GREEN_SCORE_ITEMS_BY_USER_ID_FAILURE,
  payload: error
})

export const getGreenScoreItemsByIdIdle = () => ({
  type: ActionTypes.GET_GREEN_SCORE_ITEMS_BY_USER_ID_IDLE
})
