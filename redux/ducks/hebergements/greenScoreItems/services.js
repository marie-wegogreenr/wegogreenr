import { endpoints } from 'constants/index'
import { getData } from 'utils'
import {
  getGreenScoreItemsByIdLoading,
  getGreenScoreItemsByIdSuccess,
  getGreenScoreItemsByIdFailure
} from '.'

export function getGreenScoreItemsById(userId) {
  return async (dispatch) => {
    dispatch(getGreenScoreItemsByIdLoading())

    const options = {
      url: `${endpoints.GREEN_SCORE_ITEMS}/${userId}`,
      method: 'GET'
    }

    return getData(options)
      .then((data) => {
        dispatch(getGreenScoreItemsByIdSuccess(data))
      })
      .catch((err) => {
        dispatch(getGreenScoreItemsByIdFailure(err))
      })
  }
}
