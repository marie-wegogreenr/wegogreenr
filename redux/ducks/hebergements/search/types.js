/* FILTERS */
export const ADD_ACTIVITIES_FILTER = 'ADD_ACTIVITIES_FILTER'
export const ADD_EQUIPMENTS_FILTER = 'ADD_EQUIPMENTS_FILTER'
export const ADD_PRICE_FILTER = 'ADD_PRICE_FILTER'
export const ADD_TYPES_FILTER = 'ADD_TYPES_FILTER'
export const ADD_GIFTCARD_FILTER = 'ADD_GIFTCARD_FILTER'

/* CLEAN FILTERS */
export const CLEAN_FILTERS = 'CLEAN_FILTERS'
export const CLEAN_ONLY_FILTERS = 'CLEAN_ONLY_FILTERS'
export const CLEAN_SELECTION = 'CLEAN_SELECTION'

/* RUN FILTERS */
export const FILTER_ROOMS = 'FILTER_ROOMS'
export const FILTER_ROOMS_MAP = 'FILTER_ROOMS_MAP'

/* GET ROOMS */
export const SET_INIT_SEARCHING = 'SET_INIT_SEARCHING_TYPE'
export const GET_ROOMS_FAILURE = 'GET_ROOMS_FAILURE'
export const GET_ROOMS_IDLE = 'GET_ROOMS_IDLE'
export const GET_ROOMS_LOADING = 'GET_ROOMS_LOADING'
export const GET_ROOMS_SUCCESS = 'GET_ROOMS_SUCCESS'

/* INPUTS */
export const SET_INIT_MAP_LOCATION = 'SET_INIT_MAP_LOCATION_TYPE'
export const SELECT_SEARCH_TYPE = 'SELECT_SEARCH_TYPE'
export const SET_CITY = 'SET_CITY'
export const GET_CITY = 'GET_CITY'
export const SET_FINAL_DATE = 'SET_FINAL_DATE'
export const SET_INITITAL_DATE = 'SET_INITITAL_DATE'
export const SET_TRAVELLERS_QUANTITY = 'SET_TRAVELLERS_QUANTITY'
export const CLEAN_INPUTS = 'CLEAN_INPUTS'

/* MODAL SEARCH */
export const IS_MODAL_OPEN = 'SET_STATE_MODAL_SEARCH'
