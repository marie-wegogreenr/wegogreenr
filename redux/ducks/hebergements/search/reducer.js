import {
  ADD_ACTIVITIES_FILTER,
  ADD_EQUIPMENTS_FILTER,
  ADD_PRICE_FILTER,
  ADD_TYPES_FILTER,
  CLEAN_FILTERS,
  CLEAN_INPUTS,
  FILTER_ROOMS,
  FILTER_ROOMS_MAP,
  SET_INIT_SEARCHING,
  GET_ROOMS_FAILURE,
  GET_ROOMS_IDLE,
  GET_ROOMS_LOADING,
  GET_ROOMS_SUCCESS,
  SET_FINAL_DATE,
  SET_INITITAL_DATE,
  SET_INIT_MAP_LOCATION,
  SET_TRAVELLERS_QUANTITY,
  IS_MODAL_OPEN,
  CLEAN_ONLY_FILTERS,
  CLEAN_SELECTION,
  ADD_GIFTCARD_FILTER
} from './types'

import { statuses, DEFAULT_FRANCE } from 'constants/index'
import { orderImagesByPosition } from 'helpers/imagesHelper'
import { consoleLog } from 'helpers/utilsHelper'

const initialState = {
  data: {
    inputs: {
      city: {
        name: '',
        id: null
      },
      initial_date: '',
      final_date: '',
      travellers_quantity: 0
    },
    searchParams: {
      lat: DEFAULT_FRANCE.lat,
      lng: DEFAULT_FRANCE.lng,
      page: 1,
      zoom: DEFAULT_FRANCE.zoom,
      locationName: DEFAULT_FRANCE.locationName,
      initial_date: '',
      final_date: '',
      travellers_quantity: {
        adults: 0,
        children: 0,
        babies: 0,
        total: 0
      },
      types: [],
      equipments: [],
      activities: [],
      min_price: 0,
      max_price: null,
      accept_coupon: null
    },
    rooms: [],
    establishment: [],
    pagedRooms: [],
    filteredRooms: [],
    mapRooms: [],
    filters: {
      equipments: [],
      activities: [],
      price: {
        min: 0,
        max: null
      },
      types: [],
      accept_coupon: null
    },
    isOpenModal: false
  },
  error: null,
  status: statuses.IDLE,
  filterCount: 0
}
function reducer(state = initialState, action) {
  const cleanState = {
    ...state,
    data: {
      ...state.data,
      searchParams: {
        ...state.data.searchParams,
        equipments: [],
        activities: [],
        min_price: 0,
        max_price: null,
        accept_coupon: null
      },
      filters: {
        equipments: [],
        activities: [],
        accept_coupon: null
      }
    }
  }

  switch (action.type) {
    case SET_INIT_SEARCHING:
      return {
        ...state,
        status: statuses.INIT
      }
    case GET_ROOMS_IDLE:
      return {
        ...state,
        status: statuses.IDLE
      }

    case GET_ROOMS_LOADING:
      return {
        ...state,
        status: statuses.LOADING
      }

    case GET_ROOMS_FAILURE:
      const { error } = action
      return {
        ...state,
        status: statuses.FAILURE,
        error
      }

    case GET_ROOMS_SUCCESS: {
      const { rooms } = action

      const establishments = rooms.data.filter((r) => r.origin === 4)

      let roomsWithOrderImage = rooms.data
        .filter((r) => r.origin !== 4)
        .map((r, index) => {
          return {
            ...r,
            room_image: orderImagesByPosition(r.room_image, index)
          }
        })

      return {
        ...state,
        status: statuses.SUCCESS,
        data: {
          ...state.data,
          rooms: roomsWithOrderImage,
          establishments,
          pagedRooms: rooms
        }
      }
    }

    case FILTER_ROOMS_MAP:
      return {
        ...state,
        data: {
          ...state.data,
          filteredRooms: action.rooms
        }
      }

    case ADD_EQUIPMENTS_FILTER: {
      return {
        ...state,
        data: {
          ...state.data,
          searchParams: {
            ...state.data.searchParams,
            equipments: action.equipments
          },
          filters: {
            ...state.data.filters,
            equipments: action.equipments
          }
        }
      }
    }

    case ADD_ACTIVITIES_FILTER: {
      return {
        ...state,
        data: {
          ...state.data,
          searchParams: {
            ...state.data.searchParams,
            activities: action.activities
          },
          filters: {
            ...state.data.filters,
            activities: action.activities
          }
        }
      }
    }

    case ADD_PRICE_FILTER: {
      return {
        ...state,
        data: {
          ...state.data,
          searchParams: {
            ...state.data.searchParams,
            ...action.price
          },
          filters: {
            ...state.data.filters,
            price: action.price
          }
        }
      }
    }

    case ADD_GIFTCARD_FILTER: {
      return {
        ...state,
        data: {
          ...state.data,
          searchParams: {
            ...state.data.searchParams,
            accept_coupon: action.accept_coupon
          },
          filters: {
            ...state.data.filters,
            accept_coupon: action.accept_coupon
          }
        }
      }
    }

    case ADD_TYPES_FILTER: {
      const { types } = action
      return {
        ...state,
        data: {
          ...state.data,
          searchParams: {
            ...state.data.searchParams,
            types: types
          },
          filters: {
            ...state.data.filters,
            types: types
          }
        }
      }
    }

    case CLEAN_FILTERS: {
      return {
        ...cleanState,
        data: {
          ...cleanState.data,
          searchParams: {
            ...cleanState.data.searchParams,
            types: [],
            origin: '',
            accept_coupon: null
          },
          filters: {
            ...cleanState.data.filters,
            types: [],
            accept_coupon: null
          }
        }
      }
    }

    case CLEAN_ONLY_FILTERS: {
      return {
        ...cleanState,
        data: {
          ...cleanState.data,
          searchParams: {
            ...cleanState.data.searchParams,
            origin: '',
            accept_coupon: null
          }
        }
      }
    }

    case CLEAN_SELECTION: {
      return {
        ...cleanState,
        data: {
          ...cleanState.data,
          searchParams: {
            ...cleanState.data.searchParams,
            types: [],
            accept_coupon: null
          },
          filters: {
            ...cleanState.data.filters,
            types: [],
            accept_coupon: null
          }
        }
      }
    }

    case SET_INIT_MAP_LOCATION: {
      return {
        ...state,
        data: {
          ...state.data,
          searchParams: {
            ...state.data.searchParams,
            ...action.params
          }
        }
      }
    }
    case SET_INITITAL_DATE: {
      return {
        ...state,
        data: {
          ...state.data,
          searchParams: {
            ...state.data.searchParams,
            initial_date: action.initial_date
          }
        }
      }
    }

    case SET_FINAL_DATE: {
      return {
        ...state,
        data: {
          ...state.data,
          searchParams: {
            ...state.data.searchParams,
            final_date: action.final_date
          }
        }
      }
    }

    case SET_TRAVELLERS_QUANTITY: {
      const { adults, children } = action.travellers_quantity
      return {
        ...state,
        data: {
          ...state.data,
          searchParams: {
            ...state.data.searchParams,
            travellers_quantity: {
              ...action.travellers_quantity,
              total: adults + children
            }
          }
        }
      }
    }

    case CLEAN_INPUTS: {
      return {
        ...state,
        data: {
          ...state.data,

          searchParams: initialState.data.searchParams
        }
      }
    }

    case IS_MODAL_OPEN:
      return {
        ...state,
        data: {
          ...state.data,
          isOpenModal: action.modalState
        }
      }

    default:
      return state
  }
}

export default reducer
