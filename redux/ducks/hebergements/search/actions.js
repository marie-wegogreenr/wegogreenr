import { endpoints } from 'constants/index'
import { formatDate, getADayLater, getData, isEmpty } from 'utils'
import {
  ADD_ACTIVITIES_FILTER,
  ADD_EQUIPMENTS_FILTER,
  ADD_PRICE_FILTER,
  ADD_TYPES_FILTER,
  SET_INIT_SEARCHING,
  CLEAN_FILTERS,
  CLEAN_INPUTS,
  FILTER_ROOMS,
  FILTER_ROOMS_MAP,
  GET_ROOMS_FAILURE,
  GET_ROOMS_IDLE,
  GET_ROOMS_LOADING,
  GET_ROOMS_SUCCESS,
  SELECT_SEARCH_TYPE,
  GET_CITY,
  SET_FINAL_DATE,
  SET_INITITAL_DATE,
  SET_TRAVELLERS_QUANTITY,
  SET_INIT_MAP_LOCATION,
  IS_MODAL_OPEN,
  CLEAN_ONLY_FILTERS,
  CLEAN_SELECTION,
  ADD_GIFTCARD_FILTER
} from './types'

/* GET ROOMS */
export const initSearching = () => ({
  type: SET_INIT_SEARCHING
})

export const getRoomsLoading = () => ({
  type: GET_ROOMS_LOADING
})

export const getRoomsSuccess = (rooms = []) => ({
  type: GET_ROOMS_SUCCESS,
  rooms
})

export const getRoomsFailure = (error) => ({
  type: GET_ROOMS_FAILURE,
  error
})

export const getRoomsIdle = () => ({
  type: GET_ROOMS_IDLE
})

/* INPUTS */
export const setSearchLocation = (params) => ({
  type: SET_INIT_MAP_LOCATION,
  params
})

export const getCity = () => ({
  type: GET_CITY
})

export const setInitialDate = (initial_date) => ({
  type: SET_INITITAL_DATE,
  initial_date
})

export const setFinalDate = (final_date) => ({
  type: SET_FINAL_DATE,
  final_date
})

export const setTraverllersQuantity = (travellers_quantity) => ({
  type: SET_TRAVELLERS_QUANTITY,
  travellers_quantity
})

/* FILTERS */
export const addEquipmentsFilter = (equipments) => ({
  type: ADD_EQUIPMENTS_FILTER,
  equipments
})

export const addActivitiesFilter = (activities) => ({
  type: ADD_ACTIVITIES_FILTER,
  activities
})

export const addGiftCardFilter = (accept_coupon) => ({
  type: ADD_GIFTCARD_FILTER,
  accept_coupon
})

export const addPriceFilter = (price) => ({
  type: ADD_PRICE_FILTER,
  price
})

export const addTypeFilter = (types) => ({
  type: ADD_TYPES_FILTER,
  types
})

export const filterRooms = () => (dispatch, getState) => {
  const filterCount = getState().searchedRooms?.filterCount

  return dispatch({
    type: FILTER_ROOMS,
    filterCount: filterCount + 1
  })
}

export const filterMapRooms = (rooms) => (dispatch, getState) => {
  return dispatch({
    type: FILTER_ROOMS_MAP,
    rooms
  })
}

export const cleanFilters = () => ({
  type: CLEAN_FILTERS
})

export const cleanOnlyFilters = () => ({
  type: CLEAN_ONLY_FILTERS
})

export const cleanSelection = () => ({
  type: CLEAN_SELECTION
})

export const cleanInputs = () => ({
  type: CLEAN_INPUTS
})

function getOptions(state) {
  if (state['type']) {
    return {
      url: `${endpoints.ROOMS_FOR_TYPE}/${state['type']}`,
      method: 'GET'
    }
  }

  if (state['region_id']) {
    return {
      url: `${endpoints.ROOMS_BY_REGION}/${state['region_id']}`,
      method: 'GET'
    }
  }
  if (state['nelat']) {
    return {
      url: `publication-availabilities2`,
      method: 'POST',
      data: state
    }
  }
  //consoleLog(state)
  return {
    url: endpoints.PUBLICATION_AVAILABILITIES,
    method: 'POST',
    data: state
  }
}

export function getRooms(state) {
  return async (dispatch) => {
    dispatch(getRoomsLoading())

    try {
      const rooms = await getData(getOptions(state))
      if (rooms.length > 0) {
        dispatch(getRoomsSuccess(rooms))
      }
    } catch (error) {
      dispatch(getRoomsFailure(error))
    }
  }
}

export function searchRoomsByMapBounds(state) {
  return async (dispatch) => {
    dispatch(getRoomsLoading())
    let mergeEquipementActivities = [...state.activities, ...state.equipments]
    let preformatParams = {
      ...state,
      travellers_quantity: state.travellers_quantity.total,
      equipments: mergeEquipementActivities.join(),
      activities: '',
      types: state.types.join()
    }
    try {
      const rooms = await getData(getOptions(preformatParams))
      if (rooms[0] === 'Message') {
        const roomsByDefault = {
          current_page: 1,
          data: [],
          first_page_url: '/?page=1',
          from: null,
          last_page: 1,
          last_page_url: '/?page=1',
          next_page_url: null,
          path: '/',
          per_page: 20,
          prev_page_url: null,
          to: null,
          total: 0
        }
        dispatch(getRoomsSuccess(roomsByDefault))
      } else {
        dispatch(getRoomsSuccess(rooms || []))
      }
    } catch (error) {
      dispatch(getRoomsFailure(error))
    }
  }
}

export function searchOnMapMove(state) {
  return async (dispatch) => {
    try {
      const rooms = await getData(getOptions(state))

      if (rooms.length > 0) {
        dispatch(getRoomsSuccess(rooms))
      }
    } catch (error) {}
  }
}

export const setSearchModalState = (modalState) => ({
  type: IS_MODAL_OPEN,
  modalState
})
