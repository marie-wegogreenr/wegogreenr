import { endpoints } from 'constants/index'
import { getData } from 'utils'
import {
  GET_ALL_CATEGORIES_FAILURE,
  GET_ALL_CATEGORIES_IDLE,
  GET_ALL_CATEGORIES_LOADING,
  GET_ALL_CATEGORIES_SUCCESS
} from './types'

export const getAllCategoriesIdle = () => ({
  type: GET_ALL_CATEGORIES_IDLE
})
export const getAllCategoriesLoading = () => ({
  type: GET_ALL_CATEGORIES_LOADING
})
export const getAllCategoriesSuccess = (categories = []) => ({
  categories,
  type: GET_ALL_CATEGORIES_SUCCESS
})
export const getAllCategoriesFailure = (error) => ({
  type: GET_ALL_CATEGORIES_FAILURE,
  error
})

export function getAllCategories() {
  return async (dispatch) => {
    try {
      dispatch(getAllCategoriesLoading())

      const options = {
        method: 'GET',
        url: endpoints.ALL_EQUIPMENTS
      }

      const categories = await getData(options)

      dispatch(getAllCategoriesSuccess(categories))
    } catch (error) {
      dispatch(getAllCategoriesFailure(error))
    }
  }
}
