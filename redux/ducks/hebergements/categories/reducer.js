import { statuses } from 'constants/index'
import {
  GET_ALL_CATEGORIES_FAILURE,
  GET_ALL_CATEGORIES_IDLE,
  GET_ALL_CATEGORIES_LOADING,
  GET_ALL_CATEGORIES_SUCCESS
} from './types'

const initialState = {
  status: statuses.IDLE,
  error: null,
  data: { equipments: [], activities: [] }
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_CATEGORIES_IDLE:
      return {
        ...state,
        status: statuses.IDLE
      }

    case GET_ALL_CATEGORIES_LOADING:
      return {
        ...state,
        status: statuses.LOADING
      }

    case GET_ALL_CATEGORIES_FAILURE:
      return {
        ...state,
        status: statuses.FAILURE,
        error: action.error
      }

    case GET_ALL_CATEGORIES_SUCCESS: {
      const sortedByName = (a, b) => {
        const nameA = a.name.toUpperCase()
        const nameB = b.name.toUpperCase()
        if (nameA < nameB) {
          return -1
        } else if (nameA > nameB) {
          return 1
        }

        return 0
      }

      const equipments = action.categories
        .filter((category) => category.category_id <= 5)
        .sort(sortedByName)

      const activities = action.categories
        .filter((category) => category.category_id > 5)
        .sort(sortedByName)

      return {
        ...state,
        status: statuses.SUCCESS,
        data: { ...state.data, equipments, activities }
      }
    }
    default:
      return state
  }
}

export default reducer
