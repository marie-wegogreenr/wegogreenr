import { statuses } from 'constants/index'
import {
  GET_HEBERGEMENT_TYPES_FAILURE,
  GET_HEBERGEMENT_TYPES_IDLE,
  GET_HEBERGEMENT_TYPES_LOADING,
  GET_HEBERGEMENT_TYPES_SUCCESS
} from './types'

const initialState = {
  data: {
    types: []
  },
  error: null,
  status: statuses.IDLE
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_HEBERGEMENT_TYPES_IDLE:
      return {
        ...state,
        status: statuses.IDLE
      }

    case GET_HEBERGEMENT_TYPES_LOADING:
      return {
        ...state,
        status: statuses.LOADING
      }

    case GET_HEBERGEMENT_TYPES_FAILURE:
      const { error } = action
      return {
        ...state,
        status: statuses.FAILURE,
        error
      }

    case GET_HEBERGEMENT_TYPES_SUCCESS:
      const { types } = action
      return {
        ...state,
        status: statuses.SUCCESS,
        data: {
          ...state.data,
          types
        }
      }

    default:
      return state
  }
}

export default reducer
