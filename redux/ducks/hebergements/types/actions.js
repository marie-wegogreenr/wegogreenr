import { endpoints } from 'constants/index'
import { getData } from 'utils'
import {
  GET_HEBERGEMENT_TYPES_FAILURE,
  GET_HEBERGEMENT_TYPES_IDLE,
  GET_HEBERGEMENT_TYPES_LOADING,
  GET_HEBERGEMENT_TYPES_SUCCESS
} from './types'

/* Action creators */
const getHebergementTypesIdle = () => ({
  type: GET_HEBERGEMENT_TYPES_IDLE
})

const getHebergementTypesLoading = () => ({
  type: GET_HEBERGEMENT_TYPES_LOADING
})

const getHebergementTypesFailure = (error) => ({
  type: GET_HEBERGEMENT_TYPES_FAILURE,
  error
})

const getHebergementTypesSuccess = (types) => ({
  type: GET_HEBERGEMENT_TYPES_SUCCESS,
  types
})

/*  */

export function getHebergementTypes() {
  return async (dispatch) => {
    try {
      dispatch(getHebergementTypesLoading())

      const options = {
        method: 'GET',
        url: endpoints.ROOM_TYPES
      }

      const types = await getData(options)

      dispatch(getHebergementTypesSuccess(types))
    } catch (error) {
      dispatch(getHebergementTypesFailure(error))
    }
  }
}
