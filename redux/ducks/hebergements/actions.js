import {
  CLEAN_HEBERGEMENT,
  SET_HEBERGEMENT,
  UPDATE_HEBERGEMENT,
  UPDATE_HEBERGEMENT_PROP,
  SET_MAIN_IMAGE
} from './types'

export const setHebergement = (hebergement) => ({
  type: SET_HEBERGEMENT,
  payload: hebergement
})

export const updateHebergement = (hebergement) => ({
  type: UPDATE_HEBERGEMENT,
  payload: hebergement
})

export const updateHebergementProp = (data) => ({
  type: UPDATE_HEBERGEMENT_PROP,
  payload: data
})

export const cleanHebergement = (hebergement) => ({
  type: CLEAN_HEBERGEMENT
})

export const setMainImage = (image) => ({
  type: SET_MAIN_IMAGE,
  payload: image
})
