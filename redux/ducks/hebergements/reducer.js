import {
  CLEAN_HEBERGEMENT,
  SET_HEBERGEMENT,
  UPDATE_HEBERGEMENT,
  UPDATE_HEBERGEMENT_PROP,
  SET_MAIN_IMAGE
} from './types'

const initialState = {
  establishment: { green_score: {} },
  price: null,
  id: null,
  establishment_id: null,
  type_id: null,
  internal_name: '',
  public_name: '',
  description: '',
  total_area: null,
  people_capacity: null,
  strengths: '',
  bathroom_quantity: null,
  rooms_quantity: null,
  host_quantity: null,
  basic_price: null,
  weekend_price: null,
  seven_days_discount: null,
  month_discount: null,
  min_nigth: null,
  max_nigth: null,
  slug: '',
  reservation_mode: null,
  complete: null,
  outstanding: null,
  active: null,
  created_at: '',
  updated_at: '2021-08-10T14:31:16.000000Z',
  isLoading: true,
  mainImage: null
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_HEBERGEMENT:
      return {
        ...state,
        ...action.payload,
        mainImage:
          process.env.NEXT_PUBLIC_AMAZON_IMGS +
          action.payload.room_image.find((i) => i.order === 1)?.image.url,
        isLoading: false
      }

    case UPDATE_HEBERGEMENT:
      return {
        ...state,
        ...action.payload
      }
    case UPDATE_HEBERGEMENT_PROP:
      return {
        ...state,
        ...action.payload
      }
    case SET_MAIN_IMAGE:
      return {
        ...state,
        mainImage: action.payload
      }

    case CLEAN_HEBERGEMENT:
      return initialState

    default:
      return state
  }
}

export default reducer
