import { ActionTypes } from '.'

export const getRoomsNearMeLoading = () => ({
  type: ActionTypes.GET_ROOMS_NEAR_ME_LOADING
})

export const getRoomsNearMeSuccess = (establishment) => ({
  type: ActionTypes.GET_ROOMS_NEAR_ME_SUCCESS,
  payload: establishment
})

export const getRoomsNearMeFailure = (error) => ({
  type: ActionTypes.GET_ROOMS_NEAR_ME_FAILURE,
  payload: error
})

export const getRoomsNearMeIdle = () => ({
  type: ActionTypes.GET_ROOMS_NEAR_ME_IDLE
})
