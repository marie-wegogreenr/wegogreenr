import { endpoints } from 'constants/index'
import { getData } from 'utils'
import {
  getRoomsNearMeLoading,
  getRoomsNearMeSuccess,
  getRoomsNearMeFailure
} from '.'

export function getRoomsNearMe({ lng, lat }) {
  return async (dispatch) => {
    dispatch(getRoomsNearMeLoading())

    const options = {
      url: endpoints.PUBLICATION_AVAILABILITIES,
      method: 'POST',
      data: { lng, lat, distance: '100' }
    }

    return getData(options)
      .then((data) => {
        dispatch(getRoomsNearMeSuccess(data))
      })
      .catch((err) => {
        dispatch(getRoomsNearMeFailure(err))
      })
  }
}
