import { statuses } from 'constants/index'

export const initialState = {
  data: null,
  error: null,
  status: statuses.IDLE
}

export const ActionTypes = {
  GET_ROOMS_NEAR_ME_LOADING: 'GET_ROOMS_NEAR_ME_LOADING',
  GET_ROOMS_NEAR_ME_SUCCESS: 'GET_ROOMS_NEAR_ME_SUCCESS',
  GET_ROOMS_NEAR_ME_FAILURE: 'GET_ROOMS_NEAR_ME_FAILURE',
  GET_ROOMS_NEAR_ME_IDLE: 'GET_ROOMS_NEAR_ME_IDLE'
}

export default ActionTypes
