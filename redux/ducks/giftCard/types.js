export const initialState = {}

export const ActionTypes = {
  ADD_GIFT_CARD_DATA: 'ADD_GIFT_CARD_DATA',
  CLEAN_GIFT_CARD_DATA: 'CLEAN_GIFT_CARD_DATA'
}
