import { ActionTypes } from './types'

export const addGiftCardData = (payload) => async (dispatch) => {
  dispatch({
    type: ActionTypes.ADD_GIFT_CARD_DATA,
    payload
  })
}

export const cleanGiftCardData = () => async (dispatch) => {
  dispatch({
    type: ActionTypes.CLEAN_GIFT_CARD_DATA
  })
}
