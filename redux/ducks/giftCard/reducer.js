import { ActionTypes, initialState } from './types'

function reducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.ADD_GIFT_CARD_DATA:
      return {
        ...state,
        ...action.payload
      }

    case ActionTypes.CLEAN_GIFT_CARD_DATA:
      return {}

    default:
      return state
  }
}

export default reducer
