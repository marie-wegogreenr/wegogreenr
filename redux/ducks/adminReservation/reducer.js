import { initialState, types } from './types'

function reducer(state = initialState, action) {
  switch (action.type) {
    case types.SELECT_RESERVATION:
      return {
        show: true,
        reservation: action.payload
      }
    case types.CLOSE_RESERVATION:
      return {
        show: false
      }
    default:
      return state
  }
}

export default reducer
