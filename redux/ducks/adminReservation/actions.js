import { types } from './types'

export const select_reservation_action = (reservation) => async (dispatch) => {
  dispatch({
    type: types.SELECT_RESERVATION,
    payload: reservation
  })
}

export const close_reservation_action = () => async (dispatch) => {
  dispatch({
    type: types.CLOSE_RESERVATION
  })
}
