export const initialState = {
  show: false
}

export const types = {
  SELECT_RESERVATION: 'SELECT_RESERVATION',
  CLOSE_RESERVATION: 'CLOSE_RESERVATION'
}
