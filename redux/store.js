import { useMemo } from 'react'
import { consoleLog } from '/utils/logConsole'
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import reducers from './reducers'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import createFilter from 'redux-persist-transform-filter'

let store
const saveSubsetFilter = createFilter('searchedRooms', ['data.searchParams'])
const persistConfig = {
  key: 'persist',
  storage: storage,
  whitelist: [
    'user',
    'establishment',
    'searchedRooms',
    'checkoutReducer',
    'tempPage',
    'adminReservation',
    'dataLayerEvent',
    'adminUser',
    'giftCardReducer'
  ],
  transforms: [saveSubsetFilter]
}
const pReducer = persistReducer(persistConfig, reducers)
function initStore(initialState) {
  return createStore(
    pReducer,
    initialState,
    composeWithDevTools(applyMiddleware(thunkMiddleware))
  )
}

export const initializeStore = (preloadedState) => {
  let _store = store ?? initStore(preloadedState)

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState
    })
    // Reset the current store
    store = undefined
  }

  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') return _store
  // Create the store once in the client
  if (!store) store = _store

  return _store
}

export function useStore(initialState) {
  const store = useMemo(() => initializeStore(initialState), [initialState])
  const persistor = persistStore(store)
  return { store, persistor }
}
