import { combineReducers } from 'redux'

//REDUCERS
import { allBreakfastTypesReducer } from './ducks/host/allBreakfastTypes'
import { allEstablishmentEquipmentsReducer } from './ducks/host/allEstablishmentEquipments'
import { allLanguagesReducer } from './ducks/host/allLanguages'
import { allRegionsReducer } from './ducks/regions/allRegions'
import { allServicesReducer } from './ducks/host/allServices'
import { countriesReducer } from './ducks/host/countries'
import { establishmentEquipmentsByIdReducer } from './ducks/host/establishmentEquipmentsById'
import { establishmentLanguagesByIdReducer } from './ducks/host/establishmentLanguagesById'
import { establishmentReducer } from './ducks/host/establishment'
import { establishmentServicesByIdReducer } from './ducks/host/establishmentServicesById'
import { establishmentTypesReducer } from './ducks/host/establishmentTypes'
import { reservationsByHostIdReducer } from './ducks/host/reservationsByHostId'
import { greenScoreItemsByIdReducer } from './ducks/hebergements/greenScoreItems'
import { roomsNearMeReducer } from './ducks/hebergements/nearMe'
import categoriesReducer from './ducks/hebergements/categories/reducer'
import checkoutReducer from './ducks/checkoutDuck'
import searchRoomsReducer from './ducks/hebergements/search/reducer'
import typesReducer from './ducks/hebergements/types/reducer'
import userReducer from './ducks/userDuck'
import blogReducer from './ducks/blog/reducer'
import textsReducer from './ducks/locales/reducer'
import hebergementReducer from './ducks/hebergements/reducer'
import hostCalendar from './ducks/hostCalendar/reducer'
import Config from './ducks/Config/reducer'
import ResponsiveSearch from './ducks/responsiveSearch/reducer'
import adminUserReducer from './ducks/AdminUser/reducer'
import tempPage from './ducks/tempPage/reducer'
import adminReservation from './ducks/adminReservation/reducer'
import dataLayerEvent from './ducks/dataLayerEvents/reducer'
import giftCardReducer from './ducks/giftCard/reducer'
// COMBINED REDUCERS
const reducers = {
  allBreakfastTypes: allBreakfastTypesReducer,
  allEstablishmentEquipments: allEstablishmentEquipmentsReducer,
  allLanguages: allLanguagesReducer,
  allRegions: allRegionsReducer,
  allServices: allServicesReducer,
  categories: categoriesReducer,
  checkoutReducer,
  countries: countriesReducer,
  establishment: establishmentReducer,
  establishmentEquipmentsById: establishmentEquipmentsByIdReducer,
  establishmentLanguagesById: establishmentLanguagesByIdReducer,
  establishmentServicesById: establishmentServicesByIdReducer,
  establishmentTypes: establishmentTypesReducer,
  greenScoreItems: greenScoreItemsByIdReducer,
  hebergementTypes: typesReducer,
  reservationsByHostId: reservationsByHostIdReducer,
  roomsNearMe: roomsNearMeReducer,
  searchedRooms: searchRoomsReducer,
  user: userReducer,
  blog: blogReducer,
  siteTexts: textsReducer,
  hebergement: hebergementReducer,
  hostCalendar: hostCalendar,
  configuration: Config,
  responsiveSearch: ResponsiveSearch,
  adminUser: adminUserReducer,
  tempPage: tempPage,
  adminReservation: adminReservation,
  dataLayerEvent: dataLayerEvent,
  giftCardReducer
}

export default combineReducers(reducers)
