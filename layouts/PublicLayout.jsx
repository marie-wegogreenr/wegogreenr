import { Footer } from '../components/footer'

export const PublicLayout = ({ children }) => {
  return (
    <>
      {children}
      <Footer />
    </>
  )
}
