import * as React from 'react'
import fetch from 'isomorphic-fetch'
/* Components */
import { CardList, Layout } from 'components'
import { Gallery, Navbar, Form, Sections } from 'components/Hebergements/Single'

/* Hooks */
import { useIsShow } from 'hooks'
import { endpoints, routes } from 'constants/index'

/* Utils */
import { getData, isEmpty } from 'utils'
import { orderImagesByPosition } from 'helpers/imagesHelper'
import { findBreakfastCategory } from 'helpers/roomServicesHelper'
import {
  getDescriptionConditions,
  getDescriptionConditionsStablishment
} from 'services/hebergementsService'
import TicTacTripBanner from '@components/Hebergements/Single/TicTacTripBanner'
import { eventListObjectTriggered } from 'helpers/dataLayer/lists'
import { useDispatch, useSelector } from 'react-redux'
import { removeUrl } from '@ducks/tempPage/actions'
import { setFullLoading } from '@ducks/Config/actions'
import { setPageViewHold } from '@ducks/dataLayerEvents/actions'
import { addGiftCardData, cleanGiftCardData } from '@ducks/giftCard/actions'
// import { NavbarProvider } from 'context/navbar'

/* google maps */
const libraries = ['places']

function Single(
  {
    room,
    categories,
    descriptionConditions,
    descriptionConditionsStablishment,
    establishment,
    unavailableDates,
    specialDates,
    roomsNear,
    establishmentServices,
    googleUnavailable
  },
  props
) {
  try {
    const dispatch = useDispatch()
    const giftCardObject = useSelector((store) => store.giftCardReducer)
    const { pageView } = useSelector((store) => store.dataLayerEvent)

    const [eventSent, setEventSent] = React.useState(false)

    React.useEffect(() => {
      if (giftCardObject?.giftcard_code) {
        dispatch(cleanGiftCardData())
      }
    }, [giftCardObject])
    React.useEffect(() => {
      dispatch(setFullLoading(false))

      if (!eventSent && pageView) {
        eventListObjectTriggered.viewItem.callback(room)
        setEventSent(true)
        dispatch(setPageViewHold())
      }

      return () => {
        dispatch(removeUrl())
      }
    }, [pageView])

    const { ref, isShow } = useIsShow()
    const { public_name, type_id } = room
    const { type, green_score, region_name, city_name } = establishment || {}
    const BreakFastCategory = findBreakfastCategory(establishmentServices)

    const combinedImages = [
      ...orderImagesByPosition(room.room_image),
      ...orderImagesByPosition(establishment?.establishment_image)
    ]

    return (
      <Layout>
        <Navbar sticky hidden={!isShow} />
        <div className="bg-yellow-light pb-4" style={{ paddingTop: '6rem' }}>
          <div className="container-xxl">
            {combinedImages.length > 0 && (
              <Gallery
                roomType={type?.name}
                roomTypeId={type_id}
                public_name={public_name}
                cityName={city_name}
                regionName={region_name}
                greenScore={green_score?.score}
                images={combinedImages}
                breakfast={BreakFastCategory}
                room={room}
              />
            )}
          </div>
          <div className="container-xxl my-4">
            <div className="row w-100 m-0">
              <div className="col-lg-8 p-0 single__info">
                <Navbar navRef={ref} sticky={true} />
                <Sections
                  establishment={establishment}
                  room={room}
                  categories={categories}
                  conditions={descriptionConditions}
                  conditionsStablishment={descriptionConditionsStablishment}
                  services={establishmentServices}
                />

                {Boolean(roomsNear?.length) && (
                  <div className="py-5">
                    <CardList
                      slidesToShow={3}
                      rooms={roomsNear}
                      title="Autres chambres de ce même hébergement"
                      from="single"
                    />
                  </div>
                )}

                <TicTacTripBanner />
              </div>
              <div className="col-lg-4 pr-0 single__form">
                <Form
                  establishment={establishment}
                  room={room}
                  unavailableDates={unavailableDates}
                  specialDates={specialDates}
                  googleUnavailable={googleUnavailable}
                />
              </div>
            </div>
          </div>
        </div>
      </Layout>
    )
  } catch (error) {
    return (
      <Layout>
        <div className="d-flex justify-content-center align-items-center w-100 h-100 my-5 py-5">
          <p className="fs-1 text-secondary my-5 py-5 fw-bolder">
            Il n'y a rien à montrer
          </p>
        </div>
      </Layout>
    )
  }
  // </NavbarProvider>
}

export async function getServerSideProps({ query }) {
  try {
    const { slug } = query

    const options = {
      method: 'GET'
    }

    const room = await getData({
      ...options,
      url: `${endpoints.HEBERGEMENT}${slug}`
    }).catch((e) => {
      throw new Error(e)
    })

    const { establishment, id: roomId } = room || {}

    if (!roomId || !establishment.id) {
      return {
        redirect: { destination: routes.HOME, permanent: false }
      }
    }

    let descriptionConditions = []
    let descriptionConditionsStablishment = []

    try {
      descriptionConditions = await getDescriptionConditions()
      descriptionConditionsStablishment =
        await getDescriptionConditionsStablishment(room.establishment_id)
    } catch (e) {
      throw new Error(e)
    }

    const categories = await getData({
      ...options,
      url: `${endpoints.EQUIPMENTS_BY_ID}${roomId}`
    }).catch((e) => {
      throw new Error(e)
    })

    if (!categories) {
      throw new Error('Missing categories')
    }

    if (!Array.isArray(categories)) {
      throw new Error('Categories are not array')
    }

    const roomsNear = await getData({
      ...options,
      url: `${endpoints.ROOMS_NEAR}${roomId}`
    }).catch((e) => {
      throw new Error(e)
    })

    if (!Array.isArray(roomsNear)) {
      throw new Error('Rooms Near are not array')
    }

    //google or native calendar
    const calendarType = await getData({
      ...options,
      url: `${endpoints.CALENDAR_OR_NOT}${roomId}`
    })

    let googleUnavailable = []
    let unavailableDates
    if (calendarType.Message !== 'Not managed by google calendar') {
      const body = {
        calendar_id: calendarType.calendar_id
      }
      const calendarOptions = {
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(body)
      }
      fetch(
        `${process.env.NEXT_PUBLIC_API_URL}${endpoints.CALENDAR}`,
        calendarOptions
      )
        .then((res) => res.json())
        .then((data) => (googleUnavailable = data))
        .catch((err) => console.error(err))
    }
    unavailableDates = await getData({
      ...options,
      url: `${endpoints.ROOM_UNAVAILABILITIES}${roomId}`
    })

    const specialDates = await getData({
      ...options,
      url: `${endpoints.ROOM_SPECIAL_AVAILABILITIES}${roomId}`
    })

    const establishmentServices = await getData({
      ...options,
      url: `${endpoints.ESTABLISHMENT_SERVICES}${establishment.id}`
    }).catch(() => [])

    if (
      !specialDates ||
      !unavailableDates ||
      !roomsNear ||
      !establishmentServices
    ) {
      throw new Error('Missing data')
    }

    return {
      props: {
        room,
        categories,
        descriptionConditions,
        descriptionConditionsStablishment,
        establishment,
        unavailableDates,
        specialDates,
        roomsNear,
        establishmentServices,
        googleUnavailable
      }
    }
  } catch (error) {
    console.warn(error)
    let mockObject = {
      room: {},
      categories: [],
      establishment: {},
      unavailableDates: [],
      specialDates: [],
      roomsNear: [],
      establishmentServices: [],
      googleUnavailable: []
    }
    return {
      // if there is some error then redirect to a secure place (home)
      props: mockObject
    }
  }
}

export default Single
