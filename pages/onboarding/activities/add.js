import React from 'react'
import Image from 'next/image'
import Router from 'next/router'
import Link from 'next/link'

import Cookies from 'universal-cookie'

import ProgressBar from '../../../components/progressbar'
import Container from '../../../components/container'
import Steper from '../../../components/steper'

class Add extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isEnableButton: true,
      isShowSidebar: '',
      showSidebar: true
    }
  }

  async componentDidMount() {}

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={5} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-9">
              <div className="row background-white container-onboarding">
                <div className="col-12">
                  <h1>Activités</h1>
                </div>
              </div>
              <div className="container-fluid container-onboarding">
                <h2>Ajouter un activitie</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla
                  quam velit, vulputate eu pharetra nec, mattis ac
                </p>
                <div className="card-white">
                  <div className="row">
                    <div className="col-1"></div>
                    <div className="col-8">
                      <h4>Ajouter un activite</h4>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Nulla quam velit, vulputate eu pharetra nec, mattis ac
                      </p>
                    </div>
                    <div className="col-3">
                      <div className="row">
                        <div className="col-12">
                          <Link href="/onboarding/hebergements/step-one">
                            <button className="button mb-2">Ajouter</button>
                          </Link>
                        </div>
                        <div className="col-12 mt-2">
                          <Link href="/host/dashboard">
                            <a>Sauter pour l'instant</a>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

export default Add
