import React from 'react'
import Router from 'next/router'
import Cookies from 'universal-cookie'

import ProgressBar from '@components/progressbar'
import Container from '@components/container'
import Steper from '@components/steper'
import { Loader } from 'components/'
import withFinishGreenScore from 'HOC/withfinishGreenScore'
import { connect } from 'react-redux'
import { setClearLoading } from '@ducks/Config/actions'
import { notify } from 'helpers/notificationClassHelper'

class StepOne extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isShowSidebar: '',
      showSidebar: true,
      first_text: 'Habitat',
      last_text: 'Gestion des ressources',
      step: 1,
      //10
      materials: [],
      //9
      inside: [],
      //7
      outside: [],
      //6
      workout: [],
      //8
      installations: [],
      user_data: [],
      establishment_response: [],
      isShowContent: false,
      savingStepButton: 'init'
    }

    this.handleStepOne = this.handleStepOne.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
    this.handleChekedPositionMaterials =
      this.handleChekedPositionMaterials.bind(this)
    this.handleChekedPositionInside = this.handleChekedPositionInside.bind(this)
    this.handleChekedPositionOutside =
      this.handleChekedPositionOutside.bind(this)
    this.handleChekedPositionWorkout =
      this.handleChekedPositionWorkout.bind(this)
    this.handleChekedPositionInstallations =
      this.handleChekedPositionInstallations.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()

    //Get data user

    let _this = this
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get establishment response
    const res_establishment_response = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishment-response/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_establishment_response = await res_establishment_response.json()
    this.setState({
      ...this.state,
      establishment_response: json_establishment_response
    })

    //Get information materials
    const res_materials = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/10',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_materials = await res_materials.json()
    json_materials.forEach((materials) => {
      materials.isChecked = false
      if (this.state.establishment_response.length > 0) {
        this.state.establishment_response.forEach((response) => {
          if (materials.id == response.response_id) {
            materials.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, materials: json_materials })

    //Get information inside
    const res_inside = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/9',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_inside = await res_inside.json()
    json_inside.forEach((inside) => {
      inside.isChecked = false
      if (this.state.establishment_response.length > 0) {
        this.state.establishment_response.forEach((response) => {
          if (inside.id == response.response_id) {
            inside.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, inside: json_inside })

    //Get information outside
    const res_outside = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/7',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_outside = await res_outside.json()
    json_outside.forEach((outside) => {
      outside.isChecked = false
      if (this.state.establishment_response.length > 0) {
        this.state.establishment_response.forEach((response) => {
          if (outside.id == response.response_id) {
            outside.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, outside: json_outside })

    //Get information workout
    const res_workout = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/6',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_workout = await res_workout.json()
    json_workout.forEach((workout) => {
      workout.isChecked = false
      if (this.state.establishment_response.length > 0) {
        this.state.establishment_response.forEach((response) => {
          if (workout.id == response.response_id) {
            workout.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, workout: json_workout })

    //Get information installations
    const res_installations = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/8',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_installations = await res_installations.json()
    json_installations.forEach((installations) => {
      installations.isChecked = false
      if (this.state.establishment_response.length > 0) {
        this.state.establishment_response.forEach((response) => {
          if (installations.id == response.response_id) {
            installations.isChecked = true
          }
        })
      }
    })
    this.setState({
      ...this.state,
      installations: json_installations,
      isShowContent: true
    })
  }

  handleStepOne(event) {
    // this.props.setClearLoading(true)
    event.preventDefault()
    this.setState({ ...this.state, savingStepButton: 'loading' })
    const cookies = new Cookies()
    let array_of_arrays = [
      { name: 'materials', question: 10 },
      { name: 'inside', question: 9 },
      { name: 'outside', question: 7 },
      { name: 'workout', question: 6 },
      { name: 'installations', question: 8 }
    ]
    let _this = this
    array_of_arrays.forEach((value) => {
      _this.state[value.name].forEach((condition) => {
        if (condition.isChecked == true) {
          fetch(process.env.NEXT_PUBLIC_API_URL + 'establishment-response', {
            body: JSON.stringify({
              user_id: _this.state.user_data.id,
              question_id: value.question,
              response_id: condition.id
            }),
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'POST'
          })
        } else {
          fetch(
            process.env.NEXT_PUBLIC_API_URL + 'delete-establishment-response',
            {
              body: JSON.stringify({
                user_id: _this.state.user_data.id,
                question_id: value.question,
                response_id: condition.id
              }),
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'POST'
            }
          )
        }
      })
    })
    fetch(process.env.NEXT_PUBLIC_API_URL + 'last-page', {
      body: JSON.stringify({
        page_url: '/onboarding/greenscore/step-two',
        page: 2
      }),
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'POST'
    })
      .then(() => {
        this.setState({ ...this.state, savingStepButton: 'saved' })
        notify(`Données sauvegardées avec succès`, 'success')
        Router.push('/onboarding/greenscore/step-two')
      })
      .catch((error) => {
        this.setState({ ...this.state, savingStepButton: 'error' })
        notify(`Une erreur s'est produite, réessayez plus tard.`, 'error')
      })
    // this.props.setClearLoading(false)
    // this.setState({ ...this.state, isLoading: false })
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  async handleChekedPositionMaterials(event) {
    let materials = this.state.materials
    materials.forEach((material) => {
      if (material.id === parseInt(event.target.value))
        material.isChecked = event.target.checked
    })
    this.setState({ materials: materials })
  }

  async handleChekedPositionInside(event) {
    let inside = this.state.inside
    inside.forEach((item) => {
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked
    })
    this.setState({ inside: inside })
  }

  async handleChekedPositionOutside(event) {
    let outside = this.state.outside
    outside.forEach((item) => {
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked
    })
    this.setState({ outside: outside })
  }

  async handleChekedPositionWorkout(event) {
    let workout = this.state.workout
    workout.forEach((item) => {
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked
    })
    this.setState({ workout: workout })
  }

  async handleChekedPositionInstallations(event) {
    let installations = this.state.installations
    installations.forEach((item) => {
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked
    })
    this.setState({ installations: installations })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={2} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div
                className="row background-white container-onboarding"
                id="headerOnboarding"
              >
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="establissement"
                  title="GreenScore"
                />
              </div>
              <div className="row container-onboarding">
                <div className="subttitle-container">
                  {/* <h2>Introduction</h2> */}
                  <p className="greenscore-text">
                    Pas besoin d’être parfait pour commencer quelque chose de
                    bien !
                  </p>
                  <p>
                    Le GreenScore est un indice interne allant de 1 à 5. Il vous
                    permettra de mesurer vos avancées en terme
                    d’écoresponsabilité rendra visibles et lisibles vos
                    initiatives durables.
                  </p>
                  <p>
                    PS : Il n’y a pas de mauvaise réponse, juste des points
                    positifs que nous valoriserons.
                  </p>
                  <h2>Habitat</h2>
                  <p>
                    De la construction à l'entretien en passant par les
                    installations de l'établissement, We Go GreenR s'intéresse
                    aux alternatives écoresponsables mises en place par vos
                    soins
                  </p>
                </div>
                {this.state.isShowContent ? (
                  <form className="form" onSubmit={this.handleStepOne}>
                    <div className="card-white">
                      <div className="row card-head">
                        <div className="col-12 col-xs-3 col-md-2 col-lg-3 col-xl-2 d-flex mb-2 mb-md-0 justify-content-start mr-2 image-greenscore">
                          <img src="/images/onboarding/greenscore/step_one/house_greenscore.png"></img>
                        </div>
                        <div className="col-12 col-xs-9 col-md-10 col-lg-9 col-xl-10 row d-flex align-items-center">
                          <h3
                            style={{ marginBottom: 0 }}
                            className="col-12 text-justify"
                          >
                            Construction / rénovation
                          </h3>
                        </div>
                      </div>
                      <hr className="mt-1" />
                      <div className="row">
                        {this.state.materials.map((value, index) => {
                          return (
                            <div
                              className="ps-0 ps-md-3 col-12 col-md-6 col-xl-6"
                              key={index}
                            >
                              <div className="form-check">
                                <input
                                  className="form-check-input input-check-blue-dark"
                                  type="checkbox"
                                  value={value.id}
                                  id={value.id}
                                  onClick={this.handleChekedPositionMaterials}
                                  checked={value.isChecked}
                                />
                                <div>
                                  <label
                                    className="form-check-label"
                                    htmlFor={value.id}
                                  >
                                    {value.response}
                                  </label>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="card-white">
                      <div className="row card-head">
                        <div className="col-12 col-xs-3 col-md-2 col-lg-3 col-xl-2 d-flex mb-2 mb-md-0 justify-content-start mr-2 image-greenscore">
                          <img src="/images/onboarding/greenscore/step_one/bed_greenscore.png"></img>
                        </div>
                        <div className="col-12 col-xs-9 col-md-10 col-lg-9 col-xl-10 row d-flex align-items-center">
                          <h3
                            style={{ marginBottom: 0 }}
                            className="col-12 text-justify"
                          >
                            Intérieur
                          </h3>
                        </div>
                      </div>
                      <hr className="mt-1" />
                      <div className="row">
                        {this.state.inside.map((value, index) => {
                          return (
                            <div
                              className="ps-0 ps-md-3 col-12 col-md-6 col-xl-6"
                              key={index}
                            >
                              <div className="form-check">
                                <input
                                  className="form-check-input input-check-blue-dark"
                                  type="checkbox"
                                  value={value.id}
                                  id={value.id}
                                  onClick={this.handleChekedPositionInside}
                                  checked={value.isChecked}
                                />
                                <div>
                                  <label
                                    className="form-check-label"
                                    htmlFor={value.id}
                                  >
                                    {value.response}
                                  </label>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="card-white">
                      <div className="row card-head">
                        <div className="col-12 col-xs-3 col-md-2 col-lg-3 col-xl-2 d-flex mb-2 mb-md-0 justify-content-start mr-2 image-greenscore">
                          <img src="/images/onboarding/greenscore/step_one/out_greenscore.png"></img>
                        </div>
                        <div className="col-12 col-xs-9 col-md-10 col-lg-9 col-xl-10 row d-flex align-items-center">
                          <h3
                            style={{ marginBottom: 0 }}
                            className="col-12 text-justify"
                          >
                            Extérieur
                          </h3>
                        </div>
                      </div>
                      <hr className="mt-1" />
                      <div className="row">
                        {this.state.outside.map((value, index) => {
                          return (
                            <div
                              className="ps-0 ps-md-3 col-12 col-md-6 col-xl-6"
                              key={index}
                            >
                              <div className="form-check">
                                <input
                                  className="form-check-input input-check-blue-dark"
                                  type="checkbox"
                                  value={value.id}
                                  id={value.id}
                                  onClick={this.handleChekedPositionOutside}
                                  checked={value.isChecked}
                                />
                                <div>
                                  <label
                                    className="form-check-label"
                                    htmlFor={value.id}
                                  >
                                    {value.response}
                                  </label>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="card-white">
                      <div className="row card-head">
                        <div className="col-12 col-xs-3 col-md-2 col-lg-3 col-xl-2 d-flex mb-2 mb-md-0 justify-content-start mr-2 image-greenscore">
                          <img src="/images/onboarding/greenscore/step_one/bottle_greenscore.png"></img>
                        </div>
                        <div className="col-12 col-xs-9 col-md-10 col-lg-9 col-xl-10 row d-flex align-items-center">
                          <h3
                            style={{ marginBottom: 0 }}
                            className="col-12 text-justify"
                          >
                            Entretien
                          </h3>
                        </div>
                      </div>
                      <hr className="mt-1" />
                      <div className="row">
                        {this.state.workout.map((value, index) => {
                          return (
                            <div
                              className="ps-0 ps-md-3 col-12 col-md-6 col-xl-6"
                              key={index}
                            >
                              <div className="form-check">
                                <input
                                  className="form-check-input input-check-blue-dark"
                                  type="checkbox"
                                  value={value.id}
                                  id={value.id}
                                  onClick={this.handleChekedPositionWorkout}
                                  checked={value.isChecked}
                                />
                                <div>
                                  <label
                                    className="form-check-label"
                                    htmlFor={value.id}
                                  >
                                    {value.response}
                                  </label>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="card-white">
                      <div className="row card-head">
                        <div className="col-12 col-xs-3 col-md-2 col-lg-3 col-xl-2 d-flex mb-2 mb-md-0 justify-content-start mr-2 image-greenscore">
                          <img src="/images/onboarding/greenscore/step_one/instalations_greenscore.png"></img>
                        </div>
                        <div className="col-12 col-xs-9 col-md-10 col-lg-9 col-xl-10 row d-flex align-items-center">
                          <h3
                            style={{ marginBottom: 0 }}
                            className="col-12 text-justify"
                          >
                            Installations écologiques
                          </h3>
                        </div>
                      </div>
                      <hr className="mt-1" />
                      <div className="row">
                        {this.state.installations.map((value, index) => {
                          return (
                            <div
                              className="ps-0 ps-md-3 col-12 col-md-6 col-xl-6"
                              key={index}
                            >
                              <div className="form-check">
                                <input
                                  className="form-check-input input-check-blue-dark"
                                  type="checkbox"
                                  value={value.id}
                                  id={value.id}
                                  onClick={
                                    this.handleChekedPositionInstallations
                                  }
                                  checked={value.isChecked}
                                />
                                <div>
                                  <label
                                    className="form-check-label"
                                    htmlFor={value.id}
                                  >
                                    {value.response}
                                  </label>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="row mt-5 buttons-onboarding">
                      <div className="col-12 col-xl-6 button-back-onboarding">
                        <a
                          className="button button-outline-black button-long"
                          type="button"
                          href="/onboarding/hotes"
                        >
                          Retour
                        </a>
                      </div>
                      <div className="col-12 col-xl-6 justify-content-end d-flex">
                        <button
                          type="submit"
                          className="button button-long button--save-onboarding"
                          disabled={
                            this.state.savingStepButton !== 'loading' &&
                            this.state.savingStepButton !== 'saved'
                              ? false
                              : true
                          }
                        >
                          {this.state.savingStepButton === 'loading'
                            ? 'En cours...'
                            : this.state.savingStepButton === 'saved'
                            ? 'Enregistré'
                            : 'Sauvegarder et continuer'}
                        </button>
                      </div>
                    </div>
                  </form>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}

const mapDispatchToProps = {
  setClearLoading
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withFinishGreenScore(StepOne))
