import React from 'react'
import { consoleLog } from '/utils/logConsole'

import Image from 'next/image'
import Router from 'next/router'
import Link from 'next/link'

import Cookies from 'universal-cookie'

import ProgressBar from '@components/progressbar'
import Container from '@components/container'
import Steper from '@components/steper'
import { Loader } from 'components/'
import { connect } from 'react-redux'
import withFinishGreenScore from 'HOC/withfinishGreenScore'
import { setClearLoading } from '@ducks/Config/actions'
import { notify } from 'helpers/notificationClassHelper'

class StepFive extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isShowSidebar: '',
      showSidebar: true,
      first_text: 'Actions écoresponsable',
      last_text: 'Labels',
      step: 5,
      //1
      activity: [],
      //2
      another_actions: [],
      //3
      transports: [],
      user_data: [],
      establishment_response: [],
      isShowContent: false,
      savingStepButton: 'init'
    }

    this.handleStepFive = this.handleStepFive.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
    this.handleChekedPositionActivity =
      this.handleChekedPositionActivity.bind(this)
    this.handleChekedPositionAnotherActions =
      this.handleChekedPositionAnotherActions.bind(this)
    this.handleChekedPositionTransports =
      this.handleChekedPositionTransports.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()

    //Get data user

    let _this = this
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        //consoleLog(response.status);
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get establishment response
    const res_establishment_response = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishment-response/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_establishment_response = await res_establishment_response.json()
    this.setState({
      ...this.state,
      establishment_response: json_establishment_response
    })

    //Get information activity
    const res_activity = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/1',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_activity = await res_activity.json()
    json_activity.forEach((value) => {
      value.isChecked = false
      if (this.state.establishment_response.length > 0) {
        this.state.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, activity: json_activity })

    //Get information another_actions
    const res_another_actions = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/2',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_another_actions = await res_another_actions.json()
    json_another_actions.forEach((value) => {
      value.isChecked = false
      if (this.state.establishment_response.length > 0) {
        this.state.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, another_actions: json_another_actions })

    //Get information transports
    const res_transports = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/3',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_transports = await res_transports.json()
    json_transports.forEach((value) => {
      value.isChecked = false
      if (this.state.establishment_response.length > 0) {
        this.state.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({
      ...this.state,
      transports: json_transports,
      isShowContent: true
    })
  }

  handleStepFive(event) {
    // this.props.setClearLoading(true)
    event.preventDefault()
    this.setState({ ...this.state, savingStepButton: 'loading' })
    const cookies = new Cookies()
    let array_of_arrays = [
      { name: 'activity', question: 1 },
      { name: 'another_actions', question: 2 },
      { name: 'transports', question: 3 }
    ]
    let _this = this
    array_of_arrays.forEach((value) => {
      //consoleLog(value)
      //consoleLog(_this.state)
      _this.state[value.name].forEach((condition) => {
        if (condition.isChecked == true) {
          fetch(process.env.NEXT_PUBLIC_API_URL + 'establishment-response', {
            body: JSON.stringify({
              user_id: _this.state.user_data.id,
              question_id: value.question,
              response_id: condition.id
            }),
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'POST'
          })
        } else {
          fetch(
            process.env.NEXT_PUBLIC_API_URL + 'delete-establishment-response',
            {
              body: JSON.stringify({
                user_id: _this.state.user_data.id,
                question_id: value.question,
                response_id: condition.id
              }),
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'POST'
            }
          )
        }
      })
    })
    fetch(process.env.NEXT_PUBLIC_API_URL + 'last-page', {
      body: JSON.stringify({
        page_url: '/onboarding/greenscore/step-six',
        page: 6
      }),
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'POST'
    })
      .then(() => {
        this.setState({ ...this.state, savingStepButton: 'saved' })
        notify(`Données sauvegardées avec succès`, 'success')
        Router.push('/onboarding/greenscore/step-six')
      })
      .catch((error) => {
        this.setState({ ...this.state, savingStepButton: 'error' })
        notify(`Une erreur s'est produite, réessayez plus tard.`, 'error')
      })
    // this.setState({ ...this.state, isLoading: false })
    // this.props.setClearLoading(false)
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  async handleChekedPositionActivity(event) {
    let activity = this.state.activity
    activity.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(item);
    })
    this.setState({ activity: activity })
    //consoleLog(this.state.activity)
  }

  async handleChekedPositionAnotherActions(event) {
    let another_actions = this.state.another_actions
    another_actions.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(item);
    })
    this.setState({ another_actions: another_actions })
    //consoleLog(this.state.activity)
  }

  async handleChekedPositionTransports(event) {
    let transports = this.state.transports
    transports.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(item);
    })
    this.setState({ transports: transports })
    //consoleLog(this.state.activity)
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={2} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div
                className="row background-white container-onboarding"
                id="headerOnboarding"
              >
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="establissement"
                  title="GreenScore"
                />
              </div>
              <div className="row container-onboarding">
                <div className="subttitle-container">
                  <h2>Actions écoresponsables</h2>
                  <p>
                    Stages bien-être, activités slow, mobilité douce ou encore
                    proximité avec un espace naturel protégé, We Go GreenR suit
                    la vie de votre établissement.
                  </p>
                </div>
                {this.state.isShowContent ? (
                  <form className="form" onSubmit={this.handleStepFive}>
                    <div className="card-white">
                      <div className="row card-head">
                        <div className="col-12 col-xs-3 col-md-2 col-lg-3 col-xl-2 mb-2 mb-md-0 d-flex justify-content-start mr-2 image-greenscore">
                          <img src="/images/onboarding/greenscore/step_five/activity_greenscore.png"></img>
                        </div>
                        <div className="col-12 col-xs-9 col-md-10 col-lg-9 col-xl-10 row d-flex align-items-center">
                          <h3
                            style={{ marginBottom: 0 }}
                            className="col-12 text-justify"
                          >
                            éco-positives
                          </h3>
                          {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
                        </div>
                      </div>
                      <hr className="mt-1" />
                      <div className="row">
                        {this.state.activity.map((value, index) => {
                          return (
                            <div
                              className="ps-0 ps-md-3 col-12 col-md-6 col-xl-6"
                              key={index}
                            >
                              <div className="form-check">
                                <input
                                  className="form-check-input input-check-blue-dark"
                                  type="checkbox"
                                  value={value.id}
                                  id={value.id}
                                  onClick={this.handleChekedPositionActivity}
                                  checked={value.isChecked}
                                />
                                <div>
                                  <label
                                    className="form-check-label"
                                    htmlFor={value.id}
                                  >
                                    {value.response}
                                  </label>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="card-white">
                      <div className="row card-head">
                        <div className="col-12 col-xs-3 col-md-2 col-lg-3 col-xl-2 d-flex mb-2 mb-md-0 justify-content-start mr-2 image-greenscore">
                          <img src="/images/onboarding/greenscore/step_five/another_actions_greenscore.png"></img>
                        </div>
                        <div className="col-12 col-xs-9 col-md-10 col-lg-9 col-xl-10 row d-flex align-items-center">
                          <h3
                            style={{ marginBottom: 0 }}
                            className="col-12 text-start"
                          >
                            Autres actions écoresponsables
                          </h3>
                          {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
                        </div>
                      </div>
                      <hr className="mt-1" />
                      <div className="row">
                        {this.state.another_actions.map((value, index) => {
                          return (
                            <div
                              className="ps-0 ps-md-3 col-12 col-md-6 col-xl-6"
                              key={index}
                            >
                              <div className="form-check">
                                <input
                                  className="form-check-input input-check-blue-dark"
                                  type="checkbox"
                                  value={value.id}
                                  id={value.id}
                                  onClick={
                                    this.handleChekedPositionAnotherActions
                                  }
                                  checked={value.isChecked}
                                />
                                <div>
                                  <label
                                    className="form-check-label"
                                    htmlFor={value.id}
                                  >
                                    {value.response}
                                  </label>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="card-white">
                      <div className="row card-head">
                        <div className="col-12 col-xs-3 col-md-2 col-lg-3 col-xl-2 d-flex mb-2 mb-md-0 justify-content-start mr-2 image-greenscore">
                          <img src="/images/onboarding/greenscore/step_five/transport_greenscore.png"></img>
                        </div>
                        <div className="col-12 col-xs-9 col-md-10 col-lg-9 col-xl-10 row d-flex align-items-center">
                          <h3
                            style={{ marginBottom: 0 }}
                            className="col-12 text-justify"
                          >
                            Transports
                          </h3>
                          {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
                        </div>
                      </div>
                      <hr className="mt-1" />
                      <div className="row">
                        {this.state.transports.map((value, index) => {
                          return (
                            <div
                              className="ps-0 ps-md-3 col-12 col-md-6 col-xl-6"
                              key={index}
                            >
                              <div className="form-check">
                                <input
                                  className="form-check-input input-check-blue-dark"
                                  type="checkbox"
                                  value={value.id}
                                  id={value.id}
                                  onClick={this.handleChekedPositionTransports}
                                  checked={value.isChecked}
                                />
                                <div>
                                  <label
                                    className="form-check-label"
                                    htmlFor={value.id}
                                  >
                                    {value.response}
                                  </label>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="row mt-5 buttons-onboarding">
                      <div className="col-12 col-xl-6 button-back-onboarding">
                        <a
                          className="button button-outline-black button-long"
                          type="button"
                          href="/onboarding/greenscore/step-four"
                        >
                          Retour
                        </a>
                      </div>
                      <div className="col-12 col-xl-6 justify-content-end d-flex">
                        <button
                          type="submit"
                          className="button button-long button--save-onboarding"
                          disabled={
                            this.state.savingStepButton !== 'loading' &&
                            this.state.savingStepButton !== 'saved'
                              ? false
                              : true
                          }
                        >
                          {this.state.savingStepButton === 'loading'
                            ? 'En cours...'
                            : this.state.savingStepButton === 'saved'
                            ? 'Enregistré'
                            : 'Sauvegarder et continuer'}
                        </button>
                      </div>
                    </div>
                  </form>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}

const mapDispatchToProps = {
  setClearLoading
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withFinishGreenScore(StepFive))
