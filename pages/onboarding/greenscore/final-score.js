import React from 'react'
import Image from 'next/image'
import Router from 'next/router'
import Link from 'next/link'

import Cookies from 'universal-cookie'

import ProgressBar from '@components/progressbar'
import Container from '@components/container'
import Steper from '@components/steper'
import { Loader } from 'components/'
import { getGreenscorePoints } from 'services/greenScoreService'
import { getEtablissement } from 'services/etablissementService'
import {
  createNewEstablishment,
  setGreenScore
} from 'services/onBoardingService'

class StepSix extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isShowSidebar: '',
      showSidebar: true,
      first_text: 'Score final',
      last_text: '',
      step: 7,
      greenscore_status: null,
      user_data: [],
      greenscore_level: 0,
      isShoContent: false
    }

    this.handleStepFinalScore = this.handleStepFinalScore.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
    this.handleChekedPositionLabel = this.handleChekedPositionLabel.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()

    //Get data user

    let _this = this
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        //consoleLog(response.status);
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    const json_final_score = await getGreenscorePoints(this.state.user_data.id)
    let greenscore_status

    if (json_final_score.greenscore > 44) {
      greenscore_status = true
    } else {
      greenscore_status = false
    }

    // Get establishment

    const stablishments = await getEtablissement(this.state.user_data.id)

    if (stablishments.length === 0) {
      const body = {
        name: '',
        type_id: '',
        clasification_id: '',
        address: '',
        lat: 0,
        lng: 0,
        street_number: 0,
        country_id: 1,
        zip_code: '',
        contact_name: '',
        contact_lastname: '',
        contact_email: '',
        contact_phone: '',
        type_id: 1
      }
      const result = await createNewEstablishment(body)
      const greenScoreBody = {
        greenscore: json_final_score.greenscore,
        level: json_final_score.greenscore_level,
        establishment_id: result.establishment_id
      }
      await setGreenScore(greenScoreBody)
    }

    this.setState({
      ...this.state,
      greenscore: json_final_score.greenscore,
      greenscore_status: greenscore_status,
      greenscore_level: json_final_score.greenscore_level,
      isShowContent: true,
      stablishments
    })
  }

  async handleStepFinalScore(event) {
    const cookies = new Cookies()
    event.preventDefault()
    fetch(process.env.NEXT_PUBLIC_API_URL + 'last-page', {
      body: JSON.stringify({
        page_url: '/onboarding/etablissement/step-one',
        page: 8
      }),
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'POST'
    }).then(() => {
      Router.push('/onboarding/etablissement/step-one')
    })
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  async handleChekedPositionLabel(event) {
    let label = this.state.label
    label.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(item);
    })
    this.setState({ label: label })
    //consoleLog(this.state.label)
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={2} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div
                className="row background-white container-onboarding"
                id="headerOnboarding"
              >
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="establissement"
                  title="GreenScore"
                />
              </div>
              <div className="row container-onboarding">
                {this.state.isShowContent ? (
                  <form className="form" onSubmit={this.handleStepFinalScore}>
                    <div className="messagge-onboarding">
                      {this.state.greenscore_status == true && (
                        <>
                          <img src="/images/onboarding/greenscore/final_score/congrat_greenscore.png"></img>
                          <h1 className="congrat-greenscore">
                            Félicitations, vous pouvez <br />
                            créer votre établissement
                          </h1>
                          <p>Votre GreenScore est de :</p>
                          <p>{this.state.greenscore_level}/5</p>
                          {this.state.greenscore_level >= 1 ||
                          this.state.greenscore_level <= 4 ? (
                            <img
                              src={
                                '/images/onboarding/greenscore/final_score/result_greenscore_' +
                                this.state.greenscore_level +
                                '.png'
                              }
                              style={{ marginTop: 30 + 'px' }}
                              className="finish-onboarding__grid-greenscore"
                              alt={`final-green-score-img`}
                            />
                          ) : (
                            <img
                              src="/images/onboarding/greenscore/final_score/result_greenscore_5.png"
                              style={{ marginTop: 30 + 'px' }}
                              className="finish-onboarding__grid-greenscore"
                              alt={`final-green-score-img`}
                            />
                          )}

                          <div className="col-12 col-md-12 col-xl-12 justify-content-center d-flex">
                            <button
                              type="submit"
                              className="button button--save-onboarding"
                            >
                              Créez votre établissement
                            </button>
                          </div>
                        </>
                      )}
                      {this.state.greenscore_status == false && (
                        <>
                          <img src="/images/onboarding/greenscore/final_score/no_greenscore.png"></img>
                          <h1 className="no-greenscore">Nous sommes désolés</h1>
                          <p className="subtitle-h1">
                            <span className="onboarding__final-span">
                              {`Pour le moment, votre GreenScore est insuffisant pour pouvoir intégrer`}
                            </span>
                            <span className="onboarding__final-span">
                              {` notre communauté d'hôtes We Go GreenR`}
                            </span>
                          </p>
                          <div className="row d-flex justify-content-center">
                            <hr className="col-1" />
                          </div>
                          <p className="subtitle-no">
                            Besoin d'aide ? Notre solution révolutionnaire
                            "GreenFlow" accompagne déjà des
                            <br />
                            centaines d'établissements touristiques dans leur
                            transition écologique !
                          </p>
                          <div className="col-12 col-md-12 col-xl-12 justify-content-center d-flex mb-5">
                            <Link href="http://www.greenflow.pro/">
                              <a className="button" type="button">
                                En savoir plus
                              </a>
                            </Link>
                          </div>
                          <div className="col-12">
                            <Link href="/user/dashboard">
                              <a className="link-black">
                                Je n'ai pas besoin d'aide, merci
                              </a>
                            </Link>
                          </div>
                        </>
                      )}
                      {this.state.greenscore_status == null && <></>}
                    </div>
                  </form>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

export default StepSix
