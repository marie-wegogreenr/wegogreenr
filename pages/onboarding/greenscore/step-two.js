import React from 'react'
import Image from 'next/image'
import Router from 'next/router'
import Link from 'next/link'

import Cookies from 'universal-cookie'

import ProgressBar from '@components/progressbar'
import Container from '@components/container'
import Steper from '@components/steper'
import { Loader } from 'components/'
import { connect } from 'react-redux'
import withFinishGreenScore from 'HOC/withfinishGreenScore'
import { setClearLoading } from '@ducks/Config/actions'
import { notify } from 'helpers/notificationClassHelper'

class StepTwo extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isShowSidebar: '',
      showSidebar: true,
      first_text: 'Gestion des ressources',
      last_text: 'Alimentation',
      step: 2,
      //16
      performance: [],
      //15
      electricity: [],
      //14
      energy: [],
      //17
      air: [],
      //13
      energy_economy: [],
      //12
      water_economy: [],
      user_data: [],
      establishment_response: [],
      isShowContent: false,
      savingStepButton: 'init'
    }

    this.handleStepTwo = this.handleStepTwo.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
    this.handleChekedPositionEnergy = this.handleChekedPositionEnergy.bind(this)
    this.handleChekedPositionAir = this.handleChekedPositionAir.bind(this)
    this.handleChekedPositionEnergyEconomy =
      this.handleChekedPositionEnergyEconomy.bind(this)
    this.handleChekedPositionWaterEconomy =
      this.handleChekedPositionWaterEconomy.bind(this)
    this.handleChekedPositionPerformance =
      this.handleChekedPositionPerformance.bind(this)
    this.handleChekedPositionElectricity =
      this.handleChekedPositionElectricity.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()

    //Get data user

    let _this = this
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get establishment response
    const res_establishment_response = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishment-response/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_establishment_response = await res_establishment_response.json()
    this.setState({
      ...this.state,
      establishment_response: json_establishment_response
    })

    //Get information performance
    const res_performance = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/16',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_performance = await res_performance.json()
    json_performance.forEach((value) => {
      value.isChecked = false
      if (this.state.establishment_response.length > 0) {
        this.state.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, performance: json_performance })

    //Get information electricity
    const res_electricity = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/15',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_electricity = await res_electricity.json()
    json_electricity.forEach((value) => {
      value.isChecked = false
      if (this.state.establishment_response.length > 0) {
        this.state.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, electricity: json_electricity })

    //Get information energy
    const res_energy = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/14',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_energy = await res_energy.json()
    json_energy.forEach((value) => {
      value.isChecked = false
      if (this.state.establishment_response.length > 0) {
        this.state.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, energy: json_energy })

    //Get information air
    const res_air = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/17',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_air = await res_air.json()
    json_air.forEach((value) => {
      value.isChecked = false
      if (this.state.establishment_response.length > 0) {
        this.state.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, air: json_air })

    //Get information energy_economy
    const res_energy_economy = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/13',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_energy_economy = await res_energy_economy.json()
    json_energy_economy.forEach((value) => {
      value.isChecked = false
      if (this.state.establishment_response.length > 0) {
        this.state.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, energy_economy: json_energy_economy })

    //Get information water_economy
    const res_water_economy = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/12',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_water_economy = await res_water_economy.json()
    json_water_economy.forEach((value) => {
      value.isChecked = false
      if (this.state.establishment_response.length > 0) {
        this.state.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({
      ...this.state,
      water_economy: json_water_economy,
      isShowContent: true
    })
  }

  handleStepTwo(event) {
    // this.props.setClearLoading(true)
    event.preventDefault()
    this.setState({ ...this.state, savingStepButton: 'loading' })
    const cookies = new Cookies()
    let array_of_arrays = [
      { name: 'performance', question: 16 },
      { name: 'electricity', question: 15 },
      { name: 'energy', question: 14 },
      { name: 'air', question: 17 },
      { name: 'energy_economy', question: 13 },
      { name: 'water_economy', question: 12 }
    ]
    let _this = this
    array_of_arrays.forEach((value) => {
      _this.state[value.name].forEach((condition) => {
        if (condition.isChecked == true) {
          fetch(process.env.NEXT_PUBLIC_API_URL + 'establishment-response', {
            body: JSON.stringify({
              user_id: _this.state.user_data.id,
              question_id: value.question,
              response_id: condition.id
            }),
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'POST'
          })
        } else {
          fetch(
            process.env.NEXT_PUBLIC_API_URL + 'delete-establishment-response',
            {
              body: JSON.stringify({
                user_id: _this.state.user_data.id,
                question_id: value.question,
                response_id: condition.id
              }),
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'POST'
            }
          )
        }
      })
    })
    fetch(process.env.NEXT_PUBLIC_API_URL + 'last-page', {
      body: JSON.stringify({
        page_url: '/onboarding/greenscore/step-three',
        page: 3
      }),
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'POST'
    })
      .then(() => {
        this.setState({ ...this.state, savingStepButton: 'saved' })
        notify(`Données sauvegardées avec succès`, 'success')
        Router.push('/onboarding/greenscore/step-three')
      })
      .catch((error) => {
        this.setState({ ...this.state, savingStepButton: 'error' })
        notify(`Une erreur s'est produite, réessayez plus tard.`, 'error')
      })
    // this.setState({ ...this.state, isLoading: false })
    // this.props.setClearLoading(false)
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  async handleChekedPositionPerformance(event) {
    let performance = this.state.performance

    performance.forEach((performance) => {
      if (performance.id === parseInt(event.target.value)) {
        performance.isChecked = !performance.isChecked
      }
    })

    this.setState({ performance: performance })
  }

  async handleChekedPositionElectricity(event) {
    let electricity = this.state.electricity

    electricity.forEach((electricity) => {
      if (electricity.isChecked == true) {
        electricity.isChecked = false
      }
      if (electricity.id === parseInt(event.target.value)) {
        electricity.isChecked = true
      }
    })
    this.setState({ electricity: electricity })
  }

  async handleChekedPositionMaterials(event) {
    let materials = this.state.materials
    materials.forEach((material) => {
      if (material.id === parseInt(event.target.value))
        material.isChecked = event.target.checked
    })
    this.setState({ materials: materials })
  }

  async handleChekedPositionEnergy(event) {
    let energy = this.state.energy
    energy.forEach((item) => {
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked
    })
    this.setState({ energy: energy })
  }

  async handleChekedPositionAir(event) {
    let air = this.state.air
    air.forEach((item) => {
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked
    })
    this.setState({ air: air })
  }

  async handleChekedPositionEnergyEconomy(event) {
    let energy_economy = this.state.energy_economy
    energy_economy.forEach((item) => {
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked
    })
    this.setState({ energy_economy: energy_economy })
  }

  async handleChekedPositionWaterEconomy(event) {
    let water_economy = this.state.water_economy
    water_economy.forEach((item) => {
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked
    })
    this.setState({ water_economy: water_economy })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={2} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div
                className="row background-white container-onboarding"
                id="headerOnboarding"
              >
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="establissement"
                  title="GreenScore"
                />
              </div>
              <div className="row container-onboarding">
                <div className="subttitle-container">
                  <h2>Gestion des ressources</h2>
                  <p>
                    Électricité, gaz ou eau courante, dites-nous quelles sont
                    vos pratiques quotidiennes pour utiliser les énergies à bon
                    escient.
                  </p>
                </div>
                {this.state.isShowContent ? (
                  <form className="form" onSubmit={this.handleStepTwo}>
                    <div className="card-white">
                      <div className="row card-head">
                        <div className="col-12 col-xs-3 col-md-2 col-lg-3 col-xl-2 d-flex mb-2 mb-md-0 justify-content-start mr-2 image-greenscore">
                          <img src="/images/onboarding/greenscore/step_two/performance_greenscore.png"></img>
                        </div>
                        <div className="col-12 col-xs-9 col-md-10 col-lg-9 col-xl-10 row d-flex align-items-center">
                          <h3
                            style={{ marginBottom: 0 }}
                            className="col-12 text-justify"
                          >
                            Performances énergétiques
                          </h3>
                        </div>
                      </div>
                      <hr className="mt-1" />
                      <div className="row">
                        {this.state.performance.map((value, index) => {
                          return (
                            <div
                              className="ps-0 ps-md-3 col-12 col-md-6 col-xl-6"
                              key={index}
                            >
                              <div className="form-check">
                                <input
                                  className="form-check-input input-check-blue-dark"
                                  type="checkbox"
                                  value={value.id}
                                  id={value.id}
                                  onClick={this.handleChekedPositionPerformance}
                                  checked={value.isChecked}
                                />
                                <div>
                                  <label
                                    className="form-check-label"
                                    htmlFor={value.id}
                                  >
                                    {value.response}
                                  </label>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="card-white">
                      <div className="row card-head">
                        <div className="col-12 col-xs-3 col-md-2 col-lg-3 col-xl-2 d-flex mb-2 mb-md-0 justify-content-start mr-2 image-greenscore">
                          <img src="/images/onboarding/greenscore/step_two/electricity_greenscore.png"></img>
                        </div>
                        <div className="col-12 col-xs-9 col-md-10 col-lg-9 col-xl-10 row d-flex align-items-center">
                          <h3
                            style={{ marginBottom: 0 }}
                            className="col-12 text-justify"
                          >
                            Fournisseur d'électricité
                          </h3>
                        </div>
                      </div>
                      <div className="col-12" style={{ padding: 0 }}>
                        <hr className="mt-1" />
                      </div>
                      <div>
                        <select
                          className="form-control mb-0"
                          id="how_know_us"
                          onChange={this.handleChekedPositionElectricity}
                          defaultValue="91"
                        >
                          {this.state.electricity.map((value, index) => {
                            if (value.isChecked) {
                              return (
                                <option value={value.id} key={index} selected>
                                  {value.response}
                                </option>
                              )
                            } else {
                              return (
                                <option value={value.id} key={index}>
                                  {value.response}
                                </option>
                              )
                            }
                          })}
                        </select>
                      </div>
                    </div>
                    <div className="card-white">
                      <div className="row card-head">
                        <div className="col-12 col-xs-3 col-md-2 col-lg-3 col-xl-2 d-flex mb-2 mb-md-0 justify-content-start mr-2 image-greenscore">
                          <img src="/images/onboarding/greenscore/step_two/energy_greenscore.png"></img>
                        </div>
                        <div className="col-12 col-xs-9 col-md-10 col-lg-9 col-xl-10 row d-flex align-items-center">
                          <h3
                            style={{ marginBottom: 0 }}
                            className="col-12 text-justify"
                          >
                            Energie
                          </h3>
                        </div>
                      </div>
                      <hr className="mt-1" />
                      <div className="row">
                        {this.state.energy.map((value, index) => {
                          return (
                            <div
                              className="ps-0 ps-md-3 col-12 col-md-6 col-xl-6"
                              key={index}
                            >
                              <div className="form-check">
                                <input
                                  className="form-check-input input-check-blue-dark"
                                  type="checkbox"
                                  value={value.id}
                                  id={value.id}
                                  onClick={this.handleChekedPositionEnergy}
                                  checked={value.isChecked}
                                />
                                <div>
                                  <label
                                    className="form-check-label"
                                    htmlFor={value.id}
                                  >
                                    {value.response}
                                  </label>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="card-white">
                      <div className="row card-head">
                        <div className="col-12 col-xs-3 col-md-2 col-lg-3 col-xl-2 d-flex mb-2 mb-md-0 justify-content-start mr-2 image-greenscore">
                          <img src="/images/onboarding/greenscore/step_two/air_greenscore.png"></img>
                        </div>
                        <div className="col-12 col-xs-9 col-md-10 col-lg-9 col-xl-10 row d-flex align-items-center">
                          <h3
                            style={{ marginBottom: 0 }}
                            className="col-12 text-justify"
                          >
                            Refroidissement de l'air
                          </h3>
                        </div>
                      </div>
                      <hr className="mt-1" />
                      <div className="row">
                        {this.state.air.map((value, index) => {
                          return (
                            <div
                              className="ps-0 ps-md-3 col-12 col-md-6 col-xl-6"
                              key={index}
                            >
                              <div className="form-check">
                                <input
                                  className="form-check-input input-check-blue-dark"
                                  type="checkbox"
                                  value={value.id}
                                  id={value.id}
                                  onClick={this.handleChekedPositionAir}
                                  checked={value.isChecked}
                                />
                                <div>
                                  <label
                                    className="form-check-label"
                                    htmlFor={value.id}
                                  >
                                    {value.response}
                                  </label>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="card-white">
                      <div className="row card-head">
                        <div className="col-12 col-xs-3 col-md-2 col-lg-3 col-xl-2 d-flex mb-2 mb-md-0 justify-content-start mr-2 image-greenscore">
                          <img src="/images/onboarding/greenscore/step_two/energy_economy_greenscore.png"></img>
                        </div>
                        <div className="col-12 col-xs-9 col-md-10 col-lg-9 col-xl-10 row d-flex align-items-center">
                          <h3
                            style={{ marginBottom: 0 }}
                            className="col-12 text-justify"
                          >
                            Economies d'énergie
                          </h3>
                        </div>
                      </div>
                      <hr className="mt-1" />
                      <div className="row">
                        {this.state.energy_economy.map((value, index) => {
                          return (
                            <div
                              className="ps-0 ps-md-3 col-12 col-md-6 col-xl-6"
                              key={index}
                            >
                              <div className="form-check">
                                <input
                                  className="form-check-input input-check-blue-dark"
                                  type="checkbox"
                                  value={value.id}
                                  id={value.id}
                                  onClick={
                                    this.handleChekedPositionEnergyEconomy
                                  }
                                  checked={value.isChecked}
                                />
                                <div>
                                  <label
                                    className="form-check-label"
                                    htmlFor={value.id}
                                  >
                                    {value.response}
                                  </label>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="card-white">
                      <div className="row card-head">
                        <div className="col-12 col-xs-3 col-md-2 col-lg-3 col-xl-2 d-flex mb-2 mb-md-0 justify-content-start mr-2 image-greenscore">
                          <img src="/images/onboarding/greenscore/step_two/water_economy_greenscore.png"></img>
                        </div>
                        <div className="col-12 col-xs-9 col-md-10 col-lg-9 col-xl-10 row d-flex align-items-center">
                          <h3
                            style={{ marginBottom: 0 }}
                            className="col-12 text-justify"
                          >
                            Economies d'eau
                          </h3>
                        </div>
                      </div>
                      <hr className="mt-1" />
                      <div className="row">
                        {this.state.water_economy.map((value, index) => {
                          return (
                            <div
                              className="ps-0 ps-md-3 col-12 col-md-6 col-xl-6"
                              key={index}
                            >
                              <div className="form-check">
                                <input
                                  className="form-check-input input-check-blue-dark"
                                  type="checkbox"
                                  value={value.id}
                                  id={value.id}
                                  onClick={
                                    this.handleChekedPositionWaterEconomy
                                  }
                                  checked={value.isChecked}
                                />
                                <div>
                                  <label
                                    className="form-check-label"
                                    htmlFor={value.id}
                                  >
                                    {value.response}
                                  </label>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="row mt-5 buttons-onboarding">
                      <div className="col-12 col-xl-6 button-back-onboarding">
                        <a
                          className="button button-outline-black button-long"
                          type="button"
                          href="/onboarding/greenscore/step-one"
                        >
                          Retour
                        </a>
                      </div>
                      <div className="col-12 col-xl-6 justify-content-end d-flex">
                        <button
                          type="submit"
                          className="button button-long button--save-onboarding"
                          disabled={
                            this.state.savingStepButton !== 'loading' &&
                            this.state.savingStepButton !== 'saved'
                              ? false
                              : true
                          }
                        >
                          {this.state.savingStepButton === 'loading'
                            ? 'En cours...'
                            : this.state.savingStepButton === 'saved'
                            ? 'Enregistré'
                            : 'Sauvegarder et continuer'}
                        </button>
                      </div>
                    </div>
                  </form>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}

const mapDispatchToProps = {
  setClearLoading
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withFinishGreenScore(StepTwo))
