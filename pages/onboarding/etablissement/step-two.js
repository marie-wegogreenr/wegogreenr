import React from 'react'
import { consoleLog } from '/utils/logConsole'

import Image from 'next/image'
import Router from 'next/router'
import Link from 'next/link'

import Cookies from 'universal-cookie'

import ProgressBar from '../../../components/progressbar'
import Container from '../../../components/container'
import Steper from '../../../components/steper'
import { Loader } from 'components/'
import { toast } from 'react-toastify'
import withGreenScore from 'HOC/withGreenScore'
import { connect } from 'react-redux'
import { notify } from 'helpers/notificationClassHelper'

class StepTwo extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      equipments: [],
      step: 2,
      first_text: 'À propos',
      last_text: 'Services',
      stablishments: [],
      establishment_equipments: [],
      user_data: [],
      isShowSidebar: '',
      showSidebar: true,
      isShowContent: false,
      savingStepButton: 'init'
    }

    this.handleStepTwo = this.handleStepTwo.bind(this)
    this.handleChekedPosition = this.handleChekedPosition.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    let _this = this
    //Get user data
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        //consoleLog(response.status);
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get stablishment
    const res_stablishments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments = await res_stablishments.json()
    if (json_stablishments.length === 0) {
      notify(`Établissement non trouvé. sera redirigé pour création`, 'warning')
      return setTimeout(() => {
        window.location.replace(
          `${process.env.NEXT_PUBLIC_URL}onboarding/etablissement/step-one`
        )
      }, 3000)
    }

    this.setState({ ...this.state, stablishments: json_stablishments })

    //Get establishment equipments
    const res_establishment_equipments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishment-equipments/' +
        this.state.stablishments[0].id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_establishment_equipments =
      await res_establishment_equipments.json()
    this.setState({
      ...this.state,
      establishment_equipments: json_establishment_equipments
    })

    //Get equipments
    const res_equipments = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'equipments',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_equipments = await res_equipments.json()
    this.setState({ ...this.state, equipments: json_equipments })

    let equipments = this.state.equipments

    equipments.forEach((equipment) => {
      equipment.isChecked = false
    })

    this.setState({ ...this.state, equipments: equipments })
    //consoleLog(this.state.equipments);
    let equipments_2 = this.state.equipments

    this.state.establishment_equipments.forEach((establishment_equipment) => {
      equipments_2.forEach((equipment) => {
        if (equipment.id == establishment_equipment.equipment_id) {
          equipment.isChecked = true
        }
      })
    })

    this.setState({
      ...this.state,
      equipments: equipments,
      isShowContent: true
    })
  }

  //POST save information establishment
  async handleChekedPosition(event) {
    let equipments = this.state.equipments
    equipments.forEach((equipment) => {
      //consoleLog(equipment.id, event.target.value);
      if (equipment.id === parseInt(event.target.value))
        equipment.isChecked = event.target.checked

      //consoleLog(equipment);
    })
    this.setState({ equipments: equipments })
    //consoleLog(this.state.equipments);
  }

  async handleStepTwo(event) {
    event.preventDefault()
    this.setState({ ...this.state, savingStepButton: 'loading' })
    const cookies = new Cookies()

    const res = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.stablishments[0].id,
      {
        body: JSON.stringify({
          is_active: this.state.stablishments[0]?.origin === 4 ? false : true,
          name: this.state.stablishments[0].name,
          type_id: this.state.stablishments[0].type_id,
          clasification_id: this.state.stablishments[0].clasification_id,
          address: this.state.stablishments[0].address,
          lat: this.state.stablishments[0].lat,
          lng: this.state.stablishments[0].lng,
          country_id: this.state.stablishments[0].country_id,
          contact_name: this.state.stablishments[0].contact_name,
          contact_lastname: this.state.stablishments[0].contact_lastname,
          contact_email: this.state.stablishments[0].contact_email,
          contact_phone: this.state.stablishments[0].contact_phone,
          description: event.target.place_description.value,
          min_entry_time: this.state.stablishments[0].min_entry_time,
          max_entry_time: this.state.stablishments[0].max_entry_time,
          max_exit_time: this.state.stablishments[0].max_exit_time,
          tva: this.state.stablishments[0].tva,
          decl_inc_pro: this.state.stablishments[0].decl_inc_pro,
          amount_taxe: this.state.stablishments[0].amount_taxe,
          slug: this.state.stablishments[0].slug,
          region_name: this.state.stablishments[0].region_name,
          city_name: this.state.stablishments[0].city_name,
          page_url: '/onboarding/etablissement/step-three',
          page: 10
        }),
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'PUT'
      }
    )

    const result = await res.json()
    const status = await res.status

    if (status == 200) {
      this.state.equipments.forEach((equipment) => {
        if (equipment.isChecked == true) {
          fetch(process.env.NEXT_PUBLIC_API_URL + 'establishment-equipments', {
            body: JSON.stringify({
              establishment_id: this.state.stablishments[0].id,
              equipment_id: equipment.id
            }),
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'POST'
          })
        } else {
          fetch(
            process.env.NEXT_PUBLIC_API_URL + 'delete-establishment-equipment',
            {
              body: JSON.stringify({
                establishment_id: this.state.stablishments[0].id,
                equipment_id: equipment.id
              }),
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'POST'
            }
          )
        }
      })
      this.setState({ ...this.state, savingStepButton: 'saved' })
      notify(`Données sauvegardées avec succès`, 'success')
      Router.push('/onboarding/etablissement/step-three')
    } else {
      this.setState({ ...this.state, savingStepButton: 'error' })
      notify(`Une erreur s'est produite, réessayez plus tard.`, 'error')
    }
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={3} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div className="row background-white container-onboarding">
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="establissement"
                  title="Établissement"
                />
              </div>
              <div className="row container-onboarding">
                <div className="subttitle-container">
                  <h2>Informations détaillées de l'établissement</h2>
                </div>
                {this.state.isShowContent ? (
                  <form className="form" onSubmit={this.handleStepTwo}>
                    <div className="card-white">
                      {/* <h3 style={{ marginBottom: 20 + 'px' }}>
                        Description générale de votre établissement
                      </h3> */}
                      <div className="form-group">
                        <textarea
                          className="form-control"
                          name="place_description"
                          id="place_description"
                          defaultValue={
                            this.state.stablishments.length > 0
                              ? this.state.stablishments[0].description
                              : ''
                          }
                        ></textarea>
                      </div>
                      <hr />
                      <div className="card-advice">
                        <h4>Conseil :</h4>
                        <p>
                          Facultatif mais grandement apprécié par nos voyageurs
                          : ce qu’ils vont retrouver chez vous, l’ambiance du
                          lieu, ses atouts, ce qui se trouve à proximité...{' '}
                        </p>
                      </div>
                    </div>
                    <div className="card-white">
                      <h3 style={{ marginBottom: 20 + 'px' }}>
                        Équipements à disposition de tous les voyageurs
                      </h3>
                      <div className="row">
                        {this.state.equipments.map((value, index) => {
                          return (
                            <div
                              className="col-12 col-md-6 col-xl-6"
                              key={index}
                            >
                              <div className="form-check d-flex align-items-center">
                                <input
                                  className="form-check-input input-check-blue-dark input-check-size-18"
                                  type="checkbox"
                                  value={value.id}
                                  id={value.id}
                                  onClick={this.handleChekedPosition}
                                  checked={value.isChecked}
                                />
                                <div>
                                  <label
                                    className="form-check-label"
                                    htmlFor={value.id}
                                  >
                                    <img
                                      src={`${process.env.NEXT_PUBLIC_AMAZON_IMGS}${value.icon}`}
                                      alt="Icon"
                                      width="50px"
                                      height="50px"
                                      style={{ marginRight: '.5rem' }}
                                    />
                                    {value.name}
                                  </label>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                      <hr />
                      <div className="card-advice">
                        <h4>Conseil :</h4>
                        <p>
                          Si un équipement n’apparaît pas, il est peut être dans
                          la partie “Hébergements”. Sinon n’hésitez pas à nous
                          le faire savoir en nous écrivant à
                          hebergeurs@wegogreenr.com.{' '}
                        </p>
                      </div>
                    </div>
                    <div className="row mt-5 buttons-onboarding">
                      <div className="col-12 col-xl-6 button-back-onboarding">
                        <a
                          className="button button-outline-black button-long"
                          type="button"
                          href="/onboarding/etablissement/step-one"
                        >
                          Retour
                        </a>
                      </div>
                      <div className="col-12 col-xl-6 justify-content-end d-flex">
                        <button
                          type="submit"
                          className="button button-long button--save-onboarding"
                          disabled={
                            this.state.savingStepButton !== 'loading' &&
                            this.state.savingStepButton !== 'saved'
                              ? false
                              : true
                          }
                        >
                          {this.state.savingStepButton === 'loading'
                            ? 'En cours...'
                            : this.state.savingStepButton === 'saved'
                            ? 'Enregistré'
                            : 'Sauvegarder et continuer'}
                        </button>
                      </div>
                    </div>
                  </form>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}
// const mapDispatchToProps = {
// }
export default connect(mapStateToProps, null)(withGreenScore(StepTwo))
