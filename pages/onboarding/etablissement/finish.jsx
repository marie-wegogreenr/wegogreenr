import React from 'react'
import { consoleLog } from '/utils/logConsole'
import Link from 'next/link'
import Router from 'next/router'
import Image from 'next/image'
import Head from 'next/head'
import Cookies from 'universal-cookie'

import { Loader } from 'components/'
import withGreenScore from 'HOC/withGreenScore'
import { connect } from 'react-redux'
import Container from '@components/container'
import { activeAREtablissement } from 'services/ARService'
// import FarSection from '@components/Onboarding/FarSection'

class FinishOnboarding extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      host_informations: [],
      user_data: [],
      stablishments: [],
      rooms: [],
      quantityRooms: 0,
      establishment_types: [],
      greenscore_level: 0,
      isShowContent: false
    }

    this.handleConvertHost = this.handleConvertHost.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    let _this = this

    _this.setState({ user_data: this.props.user })

    //Get type stablishment
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'establishment-types', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        return response.json()
      })
      .then(function (establishment_types) {
        _this.setState({ establishment_types: establishment_types })
      })

    //Get host information
    await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'host-informations/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
      .then(function (response) {
        return response.json()
      })
      .then(function (host_informations) {
        _this.setState({
          ..._this.state,
          host_informations: host_informations
        })
      })

    //Get establishment
    const res_stablishments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments = await res_stablishments.json()
    this.setState({ ...this.state, stablishments: json_stablishments })

    const res_final_score = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'calculate-points/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_final_score = await res_final_score.json()
    let greenscore_status

    this.setState({
      ...this.state,
      greenscore_level: json_final_score.greenscore_level,
      isShowContent: true
    })
  }

  async handleConvertHost(event) {
    event.preventDefault()
    const cookies = new Cookies()
    fetch(process.env.NEXT_PUBLIC_API_URL + 'becomeToHost', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    }).then((response) => {
      if (response.status !== 200) {
        if (cookies.get('tk_user')) {
          cookies.remove('tk_user')
        }
        Router.push('/login')
      }
    })
    const body = {
      activate: 1,
      ids: this.state.stablishments[0].id
    }

    const resp = await activeAREtablissement(body)
    Router.push('/host/dashboard')
  }

  render() {
    return (
      <Container>
        {/*  CONTAINER  */}
        <div className="col-12 header-onboarding">
          <Link href="/host/dashboard">
            <img
              src="/images/logo.svg"
              alt="Logo"
              height={84}
              width={72}
              style={{ marginTop: 5 + 'px', cursor: 'pointer' }}
            />
          </Link>
        </div>
        <div className="container-xxl">
          <Head>
            <title>We go greenr</title>
          </Head>
          <div className="row mb-5">
            <div className="finish-onboarding__grid d-lg-flex pb-5">
              <div className="col-12 col-lg-6 finish-onboardign-left d-flex justify-content-center align-items-center mb-4 mb-lg-0">
                <div className="row justify-content-center">
                  <img
                    src="/images/check.png"
                    alt="Check"
                    height={50}
                    width={50}
                    className="col-auto"
                  />
                  <h1>Félicitations</h1>
                  <p style={{ fontWeight: 'bold' }}>
                    Votre établissement a été créé
                  </p>

                  <div className="col-12">
                    <button
                      className="button text-center mt-3 w-100"
                      type="button"
                      onClick={this.handleConvertHost}
                    >
                      Publier
                    </button>
                  </div>
                </div>
              </div>
              {this.state.isShowContent ? (
                <div className="col-12 col-lg-6 finish-onboardign-right">
                  <div className="card-white p-4">
                    {this.state.stablishments.length > 0 && (
                      <div className="row">
                        <div className="col-12 row align-items-center pe-0">
                          <h2 className="col-12 col-sm-6 col-lg-12 col-xl-6 order-1 order-sm-0 order-lg-1 order-xl-0 mb-0">
                            Établissement
                          </h2>
                          <Link href="/onboarding/etablissement/step-one">
                            <a className="col-12 col-sm-6 col-lg-12 col-xl-6 order-0 order-sm-1 order-lg-0 order-xl-1 text-start text-sm-end text-lg-start text-xl-end mb-2 mb-sm-0 mb-lg-2 mb-xl-0">
                              Modifier Établissement
                            </a>
                          </Link>
                        </div>
                        <div
                          className="col-12"
                          style={{ marginTop: '10px', fontSize: '20px' }}
                        >
                          <h3>{this.state.stablishments[0].name}</h3>
                          <hr />
                        </div>
                        <div className="col-12">
                          <p>
                            <strong>Hôte : </strong>{' '}
                            {this.state.stablishments[0].contact_name}{' '}
                            {this.state.stablishments[0].contact_lastname}
                          </p>
                          <p>
                            <strong>Catégorie : </strong>
                            {this.state.establishment_types.map(
                              (value, index) => {
                                if (
                                  value.id ==
                                  this.state.stablishments[0].type_id
                                )
                                  return value.name
                              }
                            )}
                          </p>

                          <p style={{ marginBottom: '10px' }}>
                            <strong>Adresse : </strong>
                          </p>
                          <p>
                            {this.state.stablishments[0].address}
                            {', '}
                            {this.state.stablishments[0].city_name}{' '}
                          </p>
                          <p style={{ marginBottom: '10px' }}>
                            <strong>Description : </strong>
                          </p>
                          <p>{this.state.stablishments[0].description}</p>
                          <hr />
                        </div>

                        <div className="col-12 col-sm-6 d-flex align-items-center">
                          <p style={{ margin: '0' }}>
                            <strong>GreenScore</strong>
                          </p>
                        </div>
                        <div className="col-12 col-sm-6 d-flex justify-content-start justify-content-sm-end">
                          {this.state.greenscore_level >= 1 ||
                          this.state.greenscore_level <= 4 ? (
                            <img
                              src={
                                '/images/onboarding/greenscore/final_score/result_greenscore_' +
                                this.state.greenscore_level +
                                '.png'
                              }
                              alt={`final-green-score-img`}
                              className="finish-onboarding__grid-greenscore"
                            />
                          ) : (
                            <img
                              src="/images/onboarding/greenscore/final_score/result_greenscore_5.png"
                              alt={`final-green-score-img`}
                              className="finish-onboarding__grid-greenscore"
                            />
                          )}
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              ) : (
                <Loader />
              )}
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}

// const mapDispatchToProps = {
// }

export default connect(mapStateToProps, null)(withGreenScore(FinishOnboarding))
