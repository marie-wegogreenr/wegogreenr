import React from 'react'
import Router from 'next/router'

import Cookies from 'universal-cookie'
import ProgressBar from '../../../components/progressbar'
import Container from '../../../components/container'
import Steper from '../../../components/steper'
import { Loader } from 'components/'
import withGreenScore from 'HOC/withGreenScore'
import { connect } from 'react-redux'
import SelectQuestion from '@components/Onboarding/SelectQuestion'
import {
  annulationQuestions,
  optionsQuestion1,
  optionsQuestion2,
  textCovid
} from 'constants/cancellationConditions'
import { notify } from 'helpers/notificationClassHelper'
import {
  addCancellationConditions,
  deleteCancellationConditions,
  getCancellationConditions
} from 'services/etablissementService'

class StepSix extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      step: 6,
      first_text: `Conditions d' annulation`,
      last_text: 'Photos',
      user_data: [],
      stablishments: [],
      isShowSidebar: '',
      showSidebar: true,
      text_anulation: '',
      isShowContent: false,
      savingStepButton: 'init',
      conditionId: null,
      valueQuestion1: 0,
      valueQuestion2: 0,
      cancellationConditions: []
    }

    this.handleChangeQuestion1 = this.handleChangeQuestion1.bind(this)
    this.handleChangeQuestion2 = this.handleChangeQuestion2.bind(this)
    this.handleStepSix = this.handleStepSix.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
    this.getCancellationConditions = this.getCancellationConditions.bind(this)
    this.addCancellationConditions = this.addCancellationConditions.bind(this)
    this.deleteCancellationConditions =
      this.deleteCancellationConditions.bind(this)
    //  Annulation questions
    this.annulationQuestions = annulationQuestions
    this.optionsQuestion1 = optionsQuestion1
    this.optionsQuestion2 = optionsQuestion2
    this.textCovid = textCovid
  }

  async componentDidMount() {
    const cookies = new Cookies()
    let _this = this
    //Get user data
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get stablishment
    const res_stablishments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments = await res_stablishments.json()

    if (json_stablishments.length === 0) {
      notify(`Établissement non trouvé. sera redirigé pour création`, 'warning')
      return setTimeout(() => {
        window.location.replace(
          `${process.env.NEXT_PUBLIC_URL}onboarding/etablissement/step-one`
        )
      }, 3000)
    }

    this.setState({ ...this.state, stablishments: json_stablishments })

    //  Get cancellation conditions
    this.getCancellationConditions()

    //Get text anulation
    const res_text_anulation = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'site-texts/1',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_text_anulation = await res_text_anulation.json()
    const json_text_anulation_split = json_text_anulation.text
      .split('* ')
      .splice(1, 2)
    this.setState({ ...this.state, text_anulation: json_text_anulation_split })

    //Get text covid
    const res_text_covid = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'site-texts/2',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_text_covid = await res_text_covid.json()
    this.setState({
      ...this.state,
      text_covid: json_text_covid,
      isShowContent: true
    })
  }

  handleChangeQuestion1(event) {
    this.setState({ ...this.state, valueQuestion1: event.target.value })
  }
  handleChangeQuestion2(event) {
    this.setState({ ...this.state, valueQuestion2: event.target.value })
  }

  async getCancellationConditions() {
    let _this = this

    try {
      const cancellationConditions = await getCancellationConditions(
        this.state.stablishments[0].id
      )

      _this.setState({
        ..._this.state,
        cancellationConditions: cancellationConditions
      })

      if (_this.state.cancellationConditions?.length > 0) {
        _this.setState({
          ..._this.state,
          conditionId: _this.state.cancellationConditions[0].id,
          valueQuestion1: _this.state.cancellationConditions[0].days_quantity,
          valueQuestion2:
            _this.state.cancellationConditions[0].penality_percentage
        })
      }
    } catch (error) {
      notify(
        `Il y a eu une erreur dans le processus. Essayez de recharger la page.`,
        'error'
      )
    }
  }

  async addCancellationConditions() {
    let status

    try {
      const body = {
        establishment_id: this.state.stablishments[0].id,
        days_quantity: this.state.valueQuestion1,
        penality_percentage: this.state.valueQuestion2
      }
      const add_cancellation_conditions = await addCancellationConditions(body)
      status = add_cancellation_conditions.code
    } catch (error) {
      status = 500
    }
    return status
  }

  async deleteCancellationConditions() {
    let status

    try {
      const delete_cancellation_conditions = await deleteCancellationConditions(
        this.state.conditionId
      )
      status = delete_cancellation_conditions.code
    } catch (error) {
      status = 500
    }
    return status
  }

  async handleStepSix(event) {
    event.preventDefault()
    this.setState({ ...this.state, savingStepButton: 'loading' })
    let _this = this
    const cookies = new Cookies()

    if (
      this.state.valueQuestion1 === '' ||
      this.state.valueQuestion2 === '' ||
      this.state.valueQuestion1 === 0 ||
      this.state.valueQuestion2 === 0 ||
      Object.is(NaN, this.state.valueQuestion1) ||
      Object.is(NaN, this.state.valueQuestion2)
    ) {
      notify(`Vous devez sélectionner les questions`, 'warning')
      _this.setState({ ..._this.state, stateSaveBtn: 'init' })
    } else {
      const res = await fetch(
        process.env.NEXT_PUBLIC_API_URL +
          'establishments/' +
          this.state.stablishments[0].id,
        {
          body: JSON.stringify({
            ...this.state.stablishments[0],
            city: this.state.stablishments[0]?.city?.id,
            is_active: true,
            page_url: '/onboarding/etablissement/step-seven',
            page: 14
          }),
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + cookies.get('tk_user')
          },
          method: 'PUT'
        }
      )

      const result = await res.json()
      const status = await res.status

      if (_this.state.cancellationConditions.length > 0) {
        ;(async () => {
          const deleteStatus = await this.deleteCancellationConditions()

          if (deleteStatus === 200) {
            const addStatus = await this.addCancellationConditions()

            if (addStatus === 200) {
              notify(
                `Les modifications ont été enregistrées avec succès`,
                'success'
              )
              if (status === 200) {
                Router.push('/onboarding/etablissement/step-seven')
              }
            } else {
              notify(
                `Il y a eu une erreur dans le processus. Réessayer`,
                'error'
              )
            }
          } else {
            notify(`Il y a eu une erreur dans le processus. Réessayer`, 'error')
          }
        })()
      } else if (_this.state.cancellationConditions.length === 0) {
        ;(async () => {
          const addStatus = await this.addCancellationConditions()

          if (addStatus === 200) {
            notify(
              `Les modifications ont été enregistrées avec succès`,
              'success'
            )
            if (status === 200) {
              Router.push('/onboarding/etablissement/step-seven')
            }
          } else {
            notify(`Il y a eu une erreur dans le processus. Réessayer`, 'error')
          }
        })()
      }
    }
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={3} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div className="row background-white container-onboarding">
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="establissement"
                  title="Établissement"
                />
              </div>
              {this.state.isShowContent ? (
                <div className="row container-onboarding">
                  <div className="card-white">
                    <h2 style={{ fontSize: 25 + 'px' }}>
                      Conditions d'annulation
                    </h2>

                    <SelectQuestion
                      id="days"
                      question={this.annulationQuestions.question1}
                      options={this.optionsQuestion1}
                      handleChange={this.handleChangeQuestion1}
                      defaultValue={
                        this.state.cancellationConditions.length > 0
                          ? this.state.valueQuestion1
                          : ''
                      }
                    />

                    <SelectQuestion
                      id="percentage"
                      question={this.annulationQuestions.question2}
                      options={this.optionsQuestion2}
                      handleChange={this.handleChangeQuestion2}
                      defaultValue={
                        this.state.cancellationConditions.length > 0
                          ? this.state.valueQuestion2
                          : ''
                      }
                    />
                  </div>
                  <div className="card-white">
                    <h2 style={{ fontSize: 25 + 'px' }}>
                      Politiques et COVID-19
                    </h2>
                    <ul className="text-wrap text-justify">
                      <li>{this.textCovid}</li>
                    </ul>
                  </div>
                  <div className="row mt-5 buttons-onboarding p-0 mx-0">
                    <div className="col-12 col-xl-6 button-back-onboarding p-0">
                      <a
                        className="button button-outline-black button-long me-xl-3"
                        type="button"
                        href="/onboarding/etablissement/step-four"
                      >
                        Retour
                      </a>
                    </div>
                    <div className="col-12 col-xl-6 justify-content-end d-flex ps-0 ps-xl-3 pe-0">
                      <button
                        type="button"
                        className="button button-long button--save-onboarding"
                        onClick={this.handleStepSix}
                        disabled={
                          this.state.savingStepButton !== 'loading' &&
                          this.state.savingStepButton !== 'saved'
                            ? false
                            : true
                        }
                      >
                        {this.state.savingStepButton === 'loading'
                          ? 'En cours...'
                          : this.state.savingStepButton === 'saved'
                          ? 'Enregistré'
                          : 'Sauvegarder et continuer'}
                      </button>
                    </div>
                  </div>
                </div>
              ) : (
                <Loader />
              )}
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}
export default connect(mapStateToProps, null)(withGreenScore(StepSix))
