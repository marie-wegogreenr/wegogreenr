import React from 'react'

/* Libraries */
import Autocomplete from 'react-google-autocomplete'
import Cookies from 'universal-cookie'
import Router from 'next/router'
import ProgressBar from '../../../components/progressbar'
import Container from '../../../components/container'
import Steper from '../../../components/steper'
import { Loader } from 'components/'
import Map from '@components/Onboarding/Map'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux'
import {
  createNewEstablishment,
  createNewEstablishmentLang,
  deleteEstablishmentLang,
  establismentTypes,
  getEstablishmentClasifications,
  setGreenScore,
  updateEstablishment
} from 'services/onBoardingService'
import { setEstablishmentId } from '@ducks/host/establishment/actions'
import withGreenScore from 'HOC/withGreenScore'
import debounce from 'lodash.debounce'
import { getAddressCoordinates } from 'services/etablissementService'
import { ArIntegration } from '@components/Onboarding/ArIntegration'
import { notify } from 'helpers/notificationClassHelper'
import { findRegionAndCity } from 'helpers/localizationHelper'

class StepOne extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      canSearch: false,
      region_name: '',
      city_name: '',
      citySelected: '',
      clasification: [],
      contactEmail: '',
      contactLastName: '',
      contactName: '',
      contactPhone: '',
      establishment_languages: [],
      first_text: 'Infos générales',
      isDisableContact: false,
      isEnableButton: true,
      isShowSidebar: '',
      languages: [],
      last_text: 'À propos',
      regions: [],
      lat: '',
      lng: '',
      showAlgolia: false,
      showSidebar: true,
      stablishments_types: [],
      stablishments: [],
      status_stablishment: 'create',
      step: 1,
      user_data: '',
      ville: '',
      isShowContent: false,
      showMap: true,
      address_stablishment: '',
      number_rue: '',
      cp: '',
      savingStepButton: 'init'
    }

    library.add(faInfoCircle)

    /* this.handleChangeRegion = this.handleChangeRegion.bind(this) */
    this.handleStepOne = this.handleStepOne.bind(this)
    this.updateStepOne = this.updateStepOne.bind(this)
    this.handleChekedPosition = this.handleChekedPosition.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
    this.handleChangeData = this.handleChangeData.bind(this)
    this.handleChangeAddres = this.handleChangeAddres.bind(this)
    this.setState = this.setState.bind(this)
    this.handleSearchAddress = debounce(this.handleSearchAddress, 3000)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    let _this = this

    const establishmentClasifications = await getEstablishmentClasifications()

    const establishmentTypes = await establismentTypes()
    this.setState({
      ...this.state,
      clasification: establishmentClasifications,
      stablishments_types: establishmentTypes
    })

    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get establishment
    const res_stablishments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments = await res_stablishments.json()
    this.setState(
      { ...this.state, stablishments: json_stablishments },
      async () => {
        let stablishments = this.state.stablishments
        if (
          stablishments.length > 0 &&
          stablishments[0]?.city?.id !== undefined
        ) {
          const res_city = await fetch(
            process.env.NEXT_PUBLIC_API_URL +
              'city/' +
              stablishments[0]?.city?.id,
            {
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'GET'
            }
          )
          const json_city = await res_city.json()
          stablishments[0].name_city = json_city.name
          this.setState(
            {
              ...this.state,
              stablishments: stablishments,
              ville: stablishments[0].name_city
            },
            () => {
              this.setState({ ...this.state, showAlgolia: true })
            }
          )
        } else {
          this.setState({ ...this.state, showAlgolia: true })
        }
      }
    )

    if (this.state.stablishments.length > 0) {
      const { lat, lng } = this.state.stablishments[0]

      this.setState({
        ...this.state,
        status_stablishment: 'update',
        citySelected: {
          id: this.state.stablishments[0]?.city?.id,
          region_id: this.state.stablishments[0].region_id
        },
        lat: lat || 0,
        lng: lng || 0,
        address_stablishment: this.state.stablishments[0].address,
        number_rue: this.state.stablishments[0].street_number,
        cp: this.state.stablishments[0].zip_code,
        region_name: this.state.stablishments[0].region_name,
        city_name: this.state.stablishments[0].city_name
      })
      //Get establishment language
      const res_stablishments = await fetch(
        process.env.NEXT_PUBLIC_API_URL +
          'establishment-languages/' +
          this.state.stablishments[0].id,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + cookies.get('tk_user')
          },
          method: 'GET'
        }
      )

      const json_stablishments = await res_stablishments.json()
      this.setState({
        ...this.state,
        establishment_languages: json_stablishments
      })

      let id_country = {
        target: {
          value: this.state.stablishments[0].country_id
        }
      }
      /* this.handleChangeRegion(id_country) */
    }

    //Get final score
    const res_final_score = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'calculate-points/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_final_score = await res_final_score.json()
    this.setState({
      ...this.state,
      greenscore: json_final_score.greenscore,
      greenscore_level: json_final_score.greenscore_level
    })

    //Get languages
    const res_language = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'languages',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_languages = await res_language.json()
    this.setState({ ...this.state, languages: json_languages })

    let languages = this.state.languages

    languages.forEach((language) => {
      language.isChecked = false
    })

    this.setState({ ...this.state, languages: languages })

    let languages_2 = this.state.languages

    this.state.establishment_languages.forEach((establishment_language) => {
      languages_2.forEach((language) => {
        if (language.id == establishment_language.language_id) {
          language.isChecked = true
        }
      })
    })

    this.setState({ ...this.state, languages: languages, isShowContent: true })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.address_stablishment !== this.state.address_stablishment) {
      this.handleSearchAddress({
        address: this.state.address_stablishment,
        origin: this.state.stablishments[0]?.origin || 0,
        canSearch: !(this.state.stablishments[0]?.origin === 4)
      })
    }
  }

  handleChangeAddres(value, name) {
    this.setState({
      ...this.state,
      [name]: value,
      canSearch: !(this.state.stablishments[0]?.origin === 4)
    })
  }

  async handleChekedPosition(event) {
    let languages = this.state.languages
    languages.forEach((language) => {
      if (language.id === parseInt(event.target.value))
        language.isChecked = event.target.checked
    })
    this.setState({ ...this.state, languages: languages })
  }

  async handleSearchAddress(data) {
    if (data.canSearch) {
      let address = `${data.address !== '' ? `${data.address}+` : ''}}France`
      try {
        const response = await getAddressCoordinates(address)
        if (response.status !== 'ZERO_RESULTS') {
          const {
            address_components,
            geometry: { location }
          } = response.results[0]

          const { region_name, city_name } =
            findRegionAndCity(address_components)
          this.setState({
            ...this.state,
            lat: location.lat,
            lng: location.lng,
            region_name,
            city_name
          })
        } else {
          notify(`Aucune adresse trouvée`, 'error')
        }
      } catch (error) {
        notify(`Une erreur s'est produite, réessayez plus tard.`, 'error')
      }
    }
  }

  //POST save information establishment
  async handleStepOne(event) {
    event.preventDefault()
    if (event.key === 'Enter') return

    this.setState({ ...this.state, savingStepButton: 'loading' })

    try {
      const body = {
        active: this.state.stablishments[0]?.origin === 4 ? false : true,
        name: event.target.name_stablishment.value,
        type_id: event.target.type_stablishment.value,
        clasification_id: event.target.stars_stablishment.value,
        address: event.target.address_stablishment.value,
        lat: this.state.lat,
        lng: this.state.lng,
        street_number: null,
        city: this.state.citySelected.id,
        country_id: 1,
        region_id: this.state.citySelected.region_id,
        zip_code: null,
        contact_name: event.target.name_host.value,
        contact_lastname: event.target.last_name.value,
        contact_email: event.target.email_host.value,
        contact_phone: event.target.phone_host.value,
        city_name: this.state.city_name,
        region_name: this.state.region_name
      }

      const result = await createNewEstablishment(body)
      this.props.setEstablishmentId(result.establishment_id)
      const greenScoreBody = {
        greenscore: this.state.greenscore,
        level: this.state.greenscore_level,
        establishment_id: result.establishment_id
      }
      await setGreenScore(greenScoreBody)

      this.state.languages.forEach((language) => {
        if (language.isChecked == true) {
          createNewEstablishmentLang({
            establishment_id: result.establishment_id,
            language_id: language.id
          })
        }
      })
      this.setState({ ...this.state, savingStepButton: 'saved' })
      notify(`Données sauvegardées avec succès`, 'success')
      Router.push('/onboarding/etablissement/step-two')
    } catch (error) {
      this.setState({ ...this.state, savingStepButton: 'error' })
      notify(`Une erreur s'est produite, réessayez plus tard.`, 'error')
    }
  }

  async updateStepOne(event) {
    event.preventDefault()
    if (event.key === 'Enter') return

    this.setState({ ...this.state, savingStepButton: 'loading' })

    try {
      const body = {
        is_active: this.state.stablishments[0]?.origin === 4 ? false : true,
        name: event.target.name_stablishment.value,
        type_id: event.target.type_stablishment.value,
        clasification_id: event.target.stars_stablishment.value,
        address: event.target.address_stablishment.value,
        lat: this.state.lat,
        lng: this.state.lng,
        country_id: 1,
        region_id: this.state.citySelected.region_id,
        contact_name: event.target.name_host.value,
        contact_lastname: event.target.last_name.value,
        contact_email: event.target.email_host.value,
        contact_phone: event.target.phone_host.value,
        description: this.state.stablishments[0].description,
        min_entry_time: this.state.stablishments[0].min_entry_time,
        max_entry_time: this.state.stablishments[0].max_entry_time,
        max_exit_time: this.state.stablishments[0].max_exit_time,
        tva: this.state.stablishments[0].tva,
        decl_inc_pro: this.state.stablishments[0].decl_inc_pro,
        amount_taxe: this.state.stablishments[0].amount_taxe,
        slug: this.state.stablishments[0].slug,
        city_name: this.state.city_name,
        region_name: this.state.region_name
        // active: 1
        /*page_url: '/onboarding/etablissement/step-two',
              page: 9*/
      }

      if (this.state.stablishments[0]?.origin === 4) {
        body['prix'] = event.target.prix.value
      }

      const res = await updateEstablishment(
        body,
        this.state.stablishments[0].id
      )

      this.state.languages.forEach((language) => {
        const langBody = {
          establishment_id: this.state.stablishments[0].id,
          language_id: language.id
        }
        if (language.isChecked == true) {
          createNewEstablishmentLang(langBody)
        } else {
          deleteEstablishmentLang(langBody)
        }
      })
      this.setState({ ...this.state, savingStepButton: 'saved' })
      notify(`Données sauvegardées avec succès`, 'success')
      Router.push('/onboarding/etablissement/step-two')
    } catch (error) {
      this.setState({ ...this.state, savingStepButton: 'error' })
      notify(`Une erreur s'est produite, réessayez plus tard.`, 'error')
    }
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  setPlace(address_stablishment, LocationData) {
    const {
      address_components,
      geometry: { location }
    } = LocationData

    const lat = location.lat()
    const lng = location.lng()
    const { region_name, city_name } = findRegionAndCity(address_components)
    this.setState({
      ...this.state,
      lat,
      lng,
      address_stablishment,
      region_name,
      city_name,
      canSearch: false,
      center: {
        lat,
        lng
      }
    })
  }

  handleChangeData(event) {
    if (this.state.stablishments.length > 0) {
      if (event.target.checked == true) {
        let stablishment = this.state.stablishments
        stablishment[0].contact_name = this.state.user_data.name
        stablishment[0].contact_lastname = this.state.user_data.lastname
        stablishment[0].contact_email = this.state.user_data.email
        stablishment[0].contact_phone = this.state.user_data.phone
        this.setState({
          ...this.state,
          stablishment: stablishment,
          isDisableContact: true
        })
      } else {
        let stablishment = this.state.stablishments
        stablishment[0].contact_name = ''
        stablishment[0].contact_lastname = ''
        stablishment[0].contact_email = ''
        stablishment[0].contact_phone = ''
        this.setState({
          ...this.state,
          stablishment: stablishment,
          isDisableContact: false
        })
      }
    } else {
      if (event.target.checked == true) {
        let stablishment = this.state.stablishments
        const name = this.state.user_data.name
        const lastname = this.state.user_data.lastname
        const email = this.state.user_data.email
        const phone = this.state.user_data.phone
        this.setState({
          ...this.state,
          stablishment: stablishment,
          isDisableContact: true,
          contactName: name,
          contactLastName: lastname,
          contactEmail: email,
          contactPhone: phone
        })
      } else {
        this.setState({
          ...this.state,
          isDisableContact: false,
          contactName: '',
          contactLastName: '',
          contactEmail: '',
          contactPhone: ''
        })
      }
    }
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={3} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div
                className="row background-white container-onboarding"
                id="headerOnboarding"
              >
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="establissement"
                  title="Établissement"
                />
              </div>
              <div className="row container-onboarding">
                <div className="subttitle-container">
                  <h2>Informations générales</h2>
                  {this.state.stablishments[0]?.origin === 4 && (
                    <h4>(Intégré à Alliance Réseaux) </h4>
                  )}
                  <p className="p-subtitle">
                    Vous pourrez modifier vos réponses dans votre compte We Go
                    GreenR.
                  </p>
                </div>

                {this.state.isShowContent ? (
                  <>
                    {' '}
                    {this.state.stablishments[0]?.origin !== 4 && (
                      <ArIntegration
                        email={this.state.user_data.email}
                        updateData={this.setState}
                        greenscore={this.state.greenscore}
                        level={this.state.greenscore_level}
                      />
                    )}
                    <form
                      className="form"
                      onSubmit={
                        this.state.status_stablishment == 'create'
                          ? this.handleStepOne
                          : this.updateStepOne
                      }
                      method="POST"
                      autocomplete="off"
                    >
                      <div className="card-white">
                        <div className="form-group">
                          <h4 htmlFor="name_stablishment">
                            Nom de votre établissement *{' '}
                          </h4>
                          <input
                            onKeyPress={(e) => {
                              e.key === 'Enter' && e.preventDefault()
                            }}
                            type="text"
                            className="form-control"
                            id="name_stablishment"
                            name="name_stablishment"
                            aria-describedby="emailHelp"
                            placeholder=""
                            defaultValue={
                              this.state.stablishments.length > 0
                                ? this.state.stablishments[0].name
                                : ''
                            }
                            required
                          />
                        </div>
                        <div className="form-group">
                          <h4 htmlFor="type_stablishment">
                            Type d'établissement *{' '}
                          </h4>
                          <select
                            className="form-control"
                            id="type_stablishment"
                            required
                          >
                            <option value=""></option>
                            {this.state.stablishments.length > 0
                              ? this.state.stablishments_types.map(
                                  (value, index) => {
                                    if (
                                      this.state.stablishments[0].type_id ==
                                      value.id
                                    ) {
                                      return (
                                        <option
                                          key={index}
                                          value={value.id}
                                          selected
                                        >
                                          {value.name}
                                        </option>
                                      )
                                    } else {
                                      return (
                                        <option key={index} value={value.id}>
                                          {value.name}
                                        </option>
                                      )
                                    }
                                  }
                                )
                              : this.state.stablishments_types.map(
                                  (value, index) => {
                                    return (
                                      <option key={index} value={value.id}>
                                        {value.name}
                                      </option>
                                    )
                                  }
                                )}
                          </select>
                        </div>
                        <div className="form-group">
                          <h4 htmlFor="stars_stablishment">
                            Une reconnaissance spécifique ?{' '}
                          </h4>
                          <select
                            className="form-control"
                            id="stars_stablishment"
                          >
                            <option defaultValue=""></option>
                            {this.state.stablishments.length > 0
                              ? this.state.clasification.map((value, index) => {
                                  if (
                                    this.state.stablishments[0]
                                      .clasification_id == value.id
                                  ) {
                                    return (
                                      <option
                                        value={value.id}
                                        key={index}
                                        selected
                                      >
                                        {value.name}
                                      </option>
                                    )
                                  } else {
                                    return (
                                      <option value={value.id} key={index}>
                                        {value.name}
                                      </option>
                                    )
                                  }
                                })
                              : this.state.clasification.map((value, index) => {
                                  return (
                                    <option value={value.id} key={index}>
                                      {value.name}
                                    </option>
                                  )
                                })}
                          </select>
                        </div>

                        {this.state.stablishments[0]?.origin === 4 && (
                          <div className="form-group">
                            <h4 htmlFor="prix">Prix de référence * </h4>
                            <input
                              onKeyPress={(e) => {
                                e.key === 'Enter' && e.preventDefault()
                              }}
                              type="number"
                              onWheel={(e) => e.target.blur()}
                              className="form-control"
                              id="prix"
                              name="prix"
                              aria-describedby="prix"
                              placeholder=""
                              defaultValue={
                                this.state.stablishments.length > 0
                                  ? this.state.stablishments[0].prix
                                  : ''
                              }
                              required
                            />
                          </div>
                        )}
                      </div>
                      <div className="card-white">
                        <div className="row form-group">
                          <div className="col-12">
                            <h4 htmlFor="address_stablishment">Adresse * </h4>
                            <Autocomplete
                              apiKey={process.env.NEXT_PUBLIC_GOOGLE_MAPS_KEY}
                              name="address_stablishment"
                              onKeyPress={(e) => {
                                e.key === 'Enter' && e.preventDefault()
                              }}
                              options={{
                                componentRestrictions: { country: 'fr' },
                                types: ['geocode']
                              }}
                              onPlaceSelected={({
                                formatted_address,
                                ...rest
                              }) => {
                                this.setPlace(formatted_address, rest)
                              }}
                              onChange={(event) => {
                                this.handleChangeAddres(
                                  event.target.value,
                                  'address_stablishment'
                                )
                              }}
                              defaultValue={
                                this.state.stablishments.length > 0
                                  ? this.state.stablishments[0].address
                                  : ''
                              }
                              value={this.state.address_stablishment}
                              className="form-control"
                              placeholder="25 Rue des Pyramides, 75001"
                              required
                              type="text"
                            />
                          </div>
                        </div>
                        <div className="row form-group">
                          <div className="col-12 col-md-6">
                            <h4>Région </h4>
                            <input
                              className="form-control"
                              type="text"
                              value={this.state.region_name}
                              readOnly
                            />
                          </div>
                          <div className="col-12 col-md-6">
                            <h4>Ville </h4>
                            <input
                              className="form-control"
                              type="text"
                              value={this.state.city_name}
                              readOnly
                            />
                          </div>
                        </div>

                        {(this.state.lat !== 0 || this.state.lng !== 0) && (
                          <div className="card-map">
                            <Map
                              center={{
                                lat: this.state.lat,
                                lng: this.state.lng
                              }}
                            />
                          </div>
                        )}

                        <hr />
                        <div className="card-advice">
                          <h4>Conseil :</h4>
                          <p>
                            Renseignez ici l’adresse du bâtiment principal (où
                            se situe l’accueil par exemple). Par la suite, vous
                            pourrez ajouter une adresse spécifique à chaque
                            hébergement si besoin.{' '}
                          </p>
                        </div>
                      </div>
                      <div className="card-white">
                        <h3
                          className="card-white__responsive-title-2"
                          style={{ marginBottom: 20 + 'px' }}
                        >
                          Gestionnaire{' '}
                        </h3>
                        <div className="row">
                          <div className="col-12 col-xl-6">
                            <div className="form-group">
                              <h4 htmlFor="name_host">Prénom</h4>
                              <input
                                onKeyPress={(e) => {
                                  e.key === 'Enter' && e.preventDefault()
                                }}
                                type="text"
                                className="form-control"
                                id="name_host"
                                name="name_host"
                                aria-describedby="emailHelp"
                                placeholder=""
                                required
                                defaultValue={
                                  this.state.stablishments.length > 0
                                    ? this.state.stablishments[0].contact_name
                                    : this.state.contactName
                                }
                                value={
                                  this.state.stablishments.length > 0
                                    ? this.state.stablishments[0].contact_name
                                    : this.state.contactName
                                }
                                onChange={(event) => {
                                  if (this.state.stablishments.length > 0) {
                                    let stablishments = this.state.stablishments
                                    stablishments[0].contact_name =
                                      event.target.value
                                    this.setState({
                                      ...this.state,
                                      stablishments: stablishments
                                    })
                                  } else {
                                    let contactName = this.state.contactName
                                    contactName = event.target.value
                                    this.setState({
                                      ...this.state,
                                      contactName: contactName
                                    })
                                  }
                                }}
                                readOnly={this.state.isDisableContact}
                              />
                            </div>
                          </div>
                          <div className="col-12 col-xl-6">
                            <div className="form-group">
                              <h4 htmlFor="last_name">Nom</h4>
                              <input
                                onKeyPress={(e) => {
                                  e.key === 'Enter' && e.preventDefault()
                                }}
                                type="text"
                                className="form-control"
                                id="last_name"
                                name="last_name"
                                aria-describedby="emailHelp"
                                placeholder=""
                                required
                                defaultValue={
                                  this.state.stablishments.length > 0
                                    ? this.state.stablishments[0]
                                        .contact_lastname
                                    : this.state.contactLastName
                                }
                                value={
                                  this.state.stablishments.length > 0
                                    ? this.state.stablishments[0]
                                        .contact_lastname
                                    : this.state.contactLastName
                                }
                                onChange={(event) => {
                                  if (this.state.stablishments.length > 0) {
                                    let stablishments = this.state.stablishments
                                    stablishments[0].contact_lastname =
                                      event.target.value
                                    this.setState({
                                      ...this.state,
                                      stablishments: stablishments
                                    })
                                  } else {
                                    let contactLastName =
                                      this.state.contactLastName
                                    contactLastName = event.target.value
                                    this.setState({
                                      ...this.state,
                                      contactLastName: contactLastName
                                    })
                                  }
                                }}
                                readOnly={this.state.isDisableContact}
                              />
                            </div>
                          </div>
                          <div className="col-12 col-xl-6">
                            <div className="form-group">
                              <h4 htmlFor="email_host">Email</h4>
                              <input
                                onKeyPress={(e) => {
                                  e.key === 'Enter' && e.preventDefault()
                                }}
                                type="text"
                                className="form-control"
                                id="email_host"
                                name="email_host"
                                aria-describedby="emailHelp"
                                placeholder=""
                                required
                                defaultValue={
                                  this.state.stablishments.length > 0
                                    ? this.state.stablishments[0].contact_email
                                    : this.state.contactEmail
                                }
                                value={
                                  this.state.stablishments.length > 0
                                    ? this.state.stablishments[0].contact_email
                                    : this.state.contactEmail
                                }
                                onChange={(event) => {
                                  if (this.state.stablishments.length > 0) {
                                    let stablishments = this.state.stablishments
                                    stablishments[0].contact_email =
                                      event.target.value
                                    this.setState({
                                      ...this.state,
                                      stablishments: stablishments
                                    })
                                  } else {
                                    let contactEmail = this.state.contactEmail
                                    contactEmail = event.target.value
                                    this.setState({
                                      ...this.state,
                                      contactEmail: contactEmail
                                    })
                                  }
                                }}
                                readOnly={this.state.isDisableContact}
                              />
                            </div>
                          </div>
                          <div className="col-12 col-xl-6">
                            <div className="form-group">
                              <h4 htmlFor="phone_host">Téléphone</h4>
                              <input
                                onKeyPress={(e) => {
                                  e.key === 'Enter' && e.preventDefault()
                                }}
                                type="text"
                                className="form-control"
                                id="phone_host"
                                name="phone_host"
                                aria-describedby="emailHelp"
                                placeholder=""
                                required
                                defaultValue={
                                  this.state.stablishments.length > 0
                                    ? this.state.stablishments[0].contact_phone
                                    : this.state.contactPhone
                                }
                                value={
                                  this.state.stablishments.length > 0
                                    ? this.state.stablishments[0].contact_phone
                                    : this.state.contactPhone
                                }
                                onChange={(event) => {
                                  if (this.state.stablishments.length > 0) {
                                    let stablishments = this.state.stablishments
                                    stablishments[0].contact_phone =
                                      event.target.value
                                    this.setState({
                                      ...this.state,
                                      stablishments: stablishments
                                    })
                                  } else {
                                    let contactPhone = this.state.contactPhone
                                    contactPhone = event.target.value
                                    this.setState({
                                      ...this.state,
                                      contactPhone: contactPhone
                                    })
                                  }
                                }}
                                readOnly={this.state.isDisableContact}
                              />
                            </div>
                          </div>
                          <div className="col-12 col-xl-6">
                            <div className="form-check">
                              <input
                                type="checkbox"
                                className="form-check-input input-check-blue-dark"
                                id="use_same_data"
                                onChange={this.handleChangeData}
                              />
                              <label
                                className="form-check-label mt-1 ml-1"
                                htmlFor="use_same_data"
                              >
                                Vous êtes aussi le gestionnaire
                              </label>
                            </div>
                          </div>
                        </div>
                        <hr />
                        <div className="card-advice">
                          <h4>Conseil :</h4>
                          <p>
                            La personne qui sera le point de contact privilégié
                            des voyageurs{' '}
                          </p>
                        </div>
                      </div>
                      <div className="card-white">
                        <h3
                          className="card-white__responsive-title-2"
                          style={{ marginBottom: 20 + 'px' }}
                        >
                          Langues parlées dans votre établissement{' '}
                        </h3>
                        <div className="row">
                          {this.state.languages.map((value, index) => {
                            return (
                              <div
                                className="col-12 col-sm-6 col-md-6 col-xl-4"
                                key={index}
                              >
                                <div className="form-check">
                                  <input
                                    onKeyPress={(e) => {
                                      e.key === 'Enter' && e.preventDefault()
                                    }}
                                    className="form-check-input input-check-blue-dark"
                                    type="checkbox"
                                    value={value.id}
                                    id={value.id}
                                    onClick={this.handleChekedPosition}
                                    checked={value.isChecked}
                                  />
                                  <label
                                    className="form-check-label mt-1 ml-1"
                                    htmlFor={value.id}
                                  >
                                    {value.name}
                                  </label>
                                </div>
                              </div>
                            )
                          })}
                        </div>
                      </div>
                      <div className="row mt-5 buttons-onboarding">
                        <div className="col-12 col-xl-6 button-back-onboarding">
                          <a
                            className="button button-outline-black button-long"
                            type="button"
                            href="/onboarding/greenscore/final-score"
                          >
                            Retour
                          </a>
                        </div>
                        <div className="col-12 col-xl-6 justify-content-end d-flex">
                          <button
                            type="submit"
                            onKeyPress={(e) => {
                              e.key === 'Enter' && e.preventDefault()
                            }}
                            className="button button-long button--save-onboarding"
                            disabled={
                              this.state.savingStepButton !== 'loading' &&
                              this.state.savingStepButton !== 'saved'
                                ? false
                                : true
                            }
                          >
                            {this.state.savingStepButton === 'loading'
                              ? 'En cours...'
                              : this.state.savingStepButton === 'saved'
                              ? 'Enregistré'
                              : 'Sauvegarder et continuer'}
                          </button>
                        </div>
                      </div>
                    </form>
                  </>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}

const mapDispatchToProps = {
  setEstablishmentId
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withGreenScore(StepOne))
