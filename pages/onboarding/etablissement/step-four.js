import React from 'react'
import { toast } from 'react-toastify'
import Image from 'next/image'
import Router from 'next/router'
import Link from 'next/link'

import Cookies from 'universal-cookie'

import ProgressBar from '../../../components/progressbar'
import Container from '../../../components/container'
import Steper from '../../../components/steper'
import { Loader } from 'components/'
import withGreenScore from 'HOC/withGreenScore'
import { connect } from 'react-redux'
import { notify } from 'helpers/notificationClassHelper'

class StepFour extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      step: 4,
      first_text: 'Conditions générales',
      last_text: 'Taxe de séjour',
      conditions: [],
      stablishments: [],
      establishment_conditions: [],
      status_stablishment: 'create',
      user_data: [],
      isShowSidebar: '',
      showSidebar: true,
      timeMax: '',
      isShowContent: false,
      savingStepButton: 'init'
    }
    this.handleStepFour = this.handleStepFour.bind(this)
    this.handleChekedPosition = this.handleChekedPosition.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
    this.handleTimeMax = this.handleTimeMax.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    let _this = this

    //Get user data
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get conditions
    const res_conditions = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'conditions',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_conditions = await res_conditions.json()
    this.setState({ ...this.state, conditions: json_conditions })

    //Get stablishment
    const res_stablishments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments = await res_stablishments.json()

    if (json_stablishments.length === 0) {
      notify(`Établissement non trouvé. sera redirigé pour création`, 'warning')
      return setTimeout(() => {
        window.location.replace(
          `${process.env.NEXT_PUBLIC_URL}onboarding/etablissement/step-one`
        )
      }, 3000)
    }

    this.setState({ ...this.state, stablishments: json_stablishments })

    if (this.state.stablishments.length > 0) {
      this.setState({
        ...this.state,
        timeMax: this.state.stablishments[0].max_entry_time
      })
    }

    //Get establishment conditions
    const res_establishment_conditions = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishment-conditions/' +
        this.state.stablishments[0].id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_establishment_conditions =
      await res_establishment_conditions.json()
    this.setState({
      ...this.state,
      establishment_conditions: json_establishment_conditions
    })

    if (this.state.establishment_conditions.length > 0) {
      this.setState({ ...this.state, status_stablishment: 'update' })
    }

    let conditions = this.state.conditions

    conditions.forEach((equipment) => {
      equipment.isCheckedNo = true
      equipment.isCheckedYes = false
    })

    this.setState({ ...this.state, conditions: conditions })
    let conditions_2 = this.state.conditions

    this.state.establishment_conditions.forEach((establishment_condition) => {
      conditions_2.forEach((condition) => {
        if (condition.id == establishment_condition.condition_id) {
          condition.isCheckedYes = true
          condition.isCheckedNo = false
          condition.establishment_id = establishment_condition.id
        }
      })
    })
    this.setState({
      ...this.state,
      conditions: conditions,
      isShowContent: true
    })
  }

  async handleChekedPosition(event) {
    let conditions = this.state.conditions
    conditions.forEach((condition) => {
      if (condition.id === parseInt(event.target.value)) {
        condition.isCheckedYes = !condition.isCheckedYes
        condition.isCheckedNo = !condition.isCheckedNo
      }
    })
    this.setState({ conditions: conditions })
  }

  //POST save information establishment
  async handleStepFour(event) {
    event.preventDefault()
    this.setState({ ...this.state, savingStepButton: 'loading' })
    const cookies = new Cookies()
    if (this.state.status_stablishment == 'create') {
      const res = await fetch(
        process.env.NEXT_PUBLIC_API_URL +
          'establishments/' +
          this.state.stablishments[0].id,
        {
          body: JSON.stringify({
            is_active: this.state.stablishments[0]?.origin === 4 ? false : true,
            name: this.state.stablishments[0].name,
            type_id: this.state.stablishments[0].type_id,
            clasification_id: this.state.stablishments[0].clasification_id,
            address: this.state.stablishments[0].address,
            lat: this.state.stablishments[0].lat,
            lng: this.state.stablishments[0].lng,
            country_id: this.state.stablishments[0].country_id,
            contact_name: this.state.stablishments[0].contact_name,
            contact_lastname: this.state.stablishments[0].contact_lastname,
            contact_email: this.state.stablishments[0].contact_email,
            contact_phone: this.state.stablishments[0].contact_phone,
            description: this.state.stablishments[0].description,
            min_entry_time: event.target.mini_arrive.value,
            max_entry_time: event.target.max_arrive.value,
            max_exit_time: event.target.max_go.value,
            tva: this.state.stablishments[0].tva,
            decl_inc_pro: this.state.stablishments[0].decl_inc_pro,
            amount_taxe: this.state.stablishments[0].amount_taxe,
            region_name: this.state.stablishments[0].region_name,
            city_name: this.state.stablishments[0].city_name,
            slug: this.state.stablishments[0].slug,
            page_url: '/onboarding/etablissement/step-six',
            page: 12
          }),
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + cookies.get('tk_user')
          },
          method: 'PUT'
        }
      )

      const result = await res.json()
      const status = await res.status

      if (status == 200) {
        this.state.conditions.forEach((condition) => {
          if (condition.isCheckedYes == true) {
            fetch(
              process.env.NEXT_PUBLIC_API_URL + 'establishment-conditions',
              {
                body: JSON.stringify({
                  establishment_id: this.state.stablishments[0].id,
                  condition_id: condition.id
                }),
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: 'Bearer ' + cookies.get('tk_user')
                },
                method: 'POST'
              }
            )
          }
        })
        this.setState({ ...this.state, savingStepButton: 'saved' })
        notify(`Données sauvegardées avec succès`, 'success')
        if (this.state.stablishments[0].origin === 4) {
          return Router.push('/onboarding/etablissement/finish ')
        }
        Router.push('/onboarding/etablissement/step-six')
      } else {
        this.setState({ ...this.state, savingStepButton: 'error' })
        notify(`Une erreur s'est produite, réessayez plus tard.`, 'error')
      }
    } else {
      const res = await fetch(
        process.env.NEXT_PUBLIC_API_URL +
          'establishments/' +
          this.state.stablishments[0].id,
        {
          body: JSON.stringify({
            is_active: this.state.stablishments[0]?.origin === 4 ? false : true,
            name: this.state.stablishments[0].name,
            type_id: this.state.stablishments[0].type_id,
            clasification_id: this.state.stablishments[0].clasification_id,
            address: this.state.stablishments[0].address,
            country_id: this.state.stablishments[0].country_id,
            contact_name: this.state.stablishments[0].contact_name,
            contact_lastname: this.state.stablishments[0].contact_lastname,
            contact_email: this.state.stablishments[0].contact_email,
            contact_phone: this.state.stablishments[0].contact_phone,
            description: this.state.stablishments[0].description,
            min_entry_time: event.target.mini_arrive.value,
            max_entry_time: event.target.max_arrive.value,
            max_exit_time: event.target.max_go.value,
            tva: this.state.stablishments[0].tva,
            decl_inc_pro: this.state.stablishments[0].decl_inc_pro,
            amount_taxe: this.state.stablishments[0].amount_taxe,
            slug: this.state.stablishments[0].slug,
            page_url: '/onboarding/etablissement/step-six',
            page: 12
          }),
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + cookies.get('tk_user')
          },
          method: 'PUT'
        }
      )

      const result = await res.json()
      const status = await res.status
      if (status == 200) {
        this.state.conditions.forEach((condition) => {
          if (condition.isCheckedYes == true) {
            fetch(
              process.env.NEXT_PUBLIC_API_URL + 'establishment-conditions',
              {
                body: JSON.stringify({
                  establishment_id: this.state.stablishments[0].id,
                  condition_id: condition.id
                }),
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: 'Bearer ' + cookies.get('tk_user')
                },
                method: 'POST'
              }
            )
          } else {
            fetch(
              process.env.NEXT_PUBLIC_API_URL +
                'establishment-conditions/' +
                condition.establishment_id,
              {
                /* body: JSON.stringify({
                                    establishment_id: this.state.stablishments[0].id,
                                    condition_id: condition.id
                                }) ,*/
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: 'Bearer ' + cookies.get('tk_user')
                },
                method: 'DELETE'
              }
            )
          }
        })
        this.setState({ ...this.state, savingStepButton: 'saved' })
        notify(`Données sauvegardées avec succès`, 'success')
        if (this.state.stablishments[0].origin === 4) {
          return Router.push('/onboarding/etablissement/finish ')
        }
        Router.push('/onboarding/etablissement/step-six')
      } else {
        this.setState({ ...this.state, savingStepButton: 'error' })
        notify(`Une erreur s'est produite, réessayez plus tard`, 'error')
      }
    }
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  handleTimeMax(event) {
    this.setState({ ...this.state, timeMax: event.target.value })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={3} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div className="row background-white container-onboarding">
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="establissement"
                  title="Établissement"
                />
              </div>
              <div className="row container-onboarding">
                <h2>Conditions générales</h2>
                {this.state.isShowContent ? (
                  <form className="form" onSubmit={this.handleStepFour}>
                    <div className="card-white">
                      <h3 style={{ marginBottom: 25 + 'px' }}>
                        Heure d'arrivée et de départ
                      </h3>
                      <div className="row">
                        <div className="col-12 col-xl-6">
                          <h4 htmlFor="mini_arrive">Heure d'arrivée *</h4>
                          <div className="input-group mb-4">
                            <div className="input-group-prepend container-onboarding__time-label">
                              <div className="input-group-text">MINI</div>
                            </div>
                            <input
                              onKeyPress={(e) => {
                                e.key === 'Enter' && e.preventDefault()
                              }}
                              type="time"
                              className="form-control container-onboarding__time-input"
                              id="mini_arrive"
                              name="mini_arrive"
                              max={this.state.timeMax}
                              required
                              defaultValue={
                                this.state.stablishments.length > 0
                                  ? this.state.stablishments[0].min_entry_time
                                  : ''
                              }
                            />
                          </div>
                        </div>
                        <div className="col-12 col-xl-6">
                          <h4 htmlFor="mini_arrive">Heure d'arrivée *</h4>
                          <div className="input-group mb-4">
                            <div className="input-group-prepend container-onboarding__time-label">
                              <div className="input-group-text">MAX</div>
                            </div>
                            <input
                              onKeyPress={(e) => {
                                e.key === 'Enter' && e.preventDefault()
                              }}
                              onChange={this.handleTimeMax}
                              type="time"
                              className="form-control container-onboarding__time-input"
                              id="max_arrive"
                              name="max_arrive"
                              required
                              defaultValue={
                                this.state.stablishments.length > 0
                                  ? this.state.stablishments[0].max_entry_time
                                  : this.state.timeMax
                              }
                            />
                          </div>
                        </div>
                        <div className="col-12 col-xl-6">
                          <h4 htmlFor="max_leave">Heure de départ *</h4>
                          <div className="input-group mb-4">
                            <div className="input-group-prepend container-onboarding__time-label">
                              <div className="input-group-text">MAX</div>
                            </div>
                            <input
                              onKeyPress={(e) => {
                                e.key === 'Enter' && e.preventDefault()
                              }}
                              type="time"
                              className="form-control container-onboarding__time-input"
                              id="max_go"
                              name="max_go"
                              required
                              defaultValue={
                                this.state.stablishments.length > 0
                                  ? this.state.stablishments[0].max_exit_time
                                  : ''
                              }
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card-white">
                      <h3 style={{ marginBottom: 25 + 'px' }}>Accessibilité</h3>
                      <div className="row">
                        {this.state.conditions.map((value, index) => {
                          return (
                            <div key={index} className="col-12 row">
                              <div className="col-12 col-xl-8">
                                <p>{value.name}</p>
                              </div>
                              <div className="col-12 col-xl-2">
                                <div className="card-border-radio d-flex justify-content-center">
                                  <div className="form-check">
                                    <input
                                      onKeyPress={(e) => {
                                        e.key === 'Enter' && e.preventDefault()
                                      }}
                                      className="form-check-input"
                                      type="radio"
                                      name={value.id}
                                      id={value.id}
                                      value={value.id}
                                      onClick={this.handleChekedPosition}
                                      checked={value.isCheckedYes}
                                    />
                                    <label
                                      className="form-check-label"
                                      htmlFor={value.id}
                                    >
                                      Oui
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div className="col-12 col-xl-2">
                                <div className="card-border-radio d-flex justify-content-center">
                                  <div className="form-check">
                                    <input
                                      onKeyPress={(e) => {
                                        e.key === 'Enter' && e.preventDefault()
                                      }}
                                      className="form-check-input"
                                      type="radio"
                                      name={value.id}
                                      id={value.id}
                                      value={value.id}
                                      onClick={this.handleChekedPosition}
                                      checked={value.isCheckedNo}
                                    />
                                    <label
                                      className="form-check-label"
                                      htmlFor={value.id}
                                    >
                                      Non
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <br />
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    <div className="row mt-5 buttons-onboarding">
                      <div className="col-12 col-xl-6 button-back-onboarding">
                        <a
                          className="button button-outline-black button-long"
                          type="button"
                          href="/onboarding/etablissement/step-three"
                        >
                          Retour
                        </a>
                      </div>
                      <div className="col-12 col-xl-6 justify-content-end d-flex">
                        <button
                          type="submit"
                          className="button button-long button--save-onboarding"
                          disabled={
                            this.state.savingStepButton !== 'loading' &&
                            this.state.savingStepButton !== 'saved'
                              ? false
                              : true
                          }
                        >
                          {this.state.savingStepButton === 'loading'
                            ? 'En cours...'
                            : this.state.savingStepButton === 'saved'
                            ? 'Enregistré'
                            : 'Sauvegarder et continuer'}
                        </button>
                      </div>
                    </div>
                  </form>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}
// const mapDispatchToProps = {
// }
export default connect(mapStateToProps, null)(withGreenScore(StepFour))
