import React from 'react'
import Image from 'next/image'
import Router from 'next/router'
import Link from 'next/link'

import Cookies from 'universal-cookie'

import ProgressBar from '../../../components/progressbar'
import Container from '../../../components/container'
import Steper from '../../../components/steper'
import { Loader } from 'components/'
import withGreenScore from 'HOC/withGreenScore'
import { connect } from 'react-redux'
import { toast } from 'react-toastify'
import { notify } from 'helpers/notificationClassHelper'
class StepThree extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      step: 3,
      first_text: 'Services',
      last_text: 'Conditions générales',
      services: [],
      type_breakfast: [],
      establishment_services: [],
      stablishments: [],
      user_data: [],
      status_services: 'create',
      isShowSidebar: '',
      showSidebar: true,
      isShowContent: false,
      savingStepButton: 'init'
    }

    this.handleStepThree = this.handleStepThree.bind(this)
    this.handleChekedPosition = this.handleChekedPosition.bind(this)
    this.handleChekedPositionBreakfast =
      this.handleChekedPositionBreakfast.bind(this)
    this.handleChangePrixBreakfast = this.handleChangePrixBreakfast.bind(this)
    this.handleChangePrixServices = this.handleChangePrixServices.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()

    let _this = this

    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get services
    const res_services = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'services',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_services = await res_services.json()
    const pettitdDeyuner = json_services.find((s) => s.id === 1)
    const deyuner = json_services.find((s) => s.id === 5)
    const dinner = json_services.find((s) => s.id === 6)
    const servicesFiltered = json_services.filter((s) => {
      return !(s.id === 1 || s.id === 5 || s.id === 6)
    })

    this.setState({
      ...this.state,
      services: [pettitdDeyuner, deyuner, dinner, ...servicesFiltered]
    })

    //Get type breakfast
    const res_type_breakfast = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'breakfast-types',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_type_breakfast = await res_type_breakfast.json()
    this.setState({ ...this.state, type_breakfast: json_type_breakfast })

    //Get establishment
    const res_stablishments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments = await res_stablishments.json()

    if (json_stablishments.length === 0) {
      notify(`Établissement non trouvé. sera redirigé pour création`, 'warning')
      return setTimeout(() => {
        window.location.replace(
          `${process.env.NEXT_PUBLIC_URL}onboarding/etablissement/step-one`
        )
      }, 3000)
    }

    this.setState({ ...this.state, stablishments: json_stablishments })

    //Get establishment services
    const res_establishment_services = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishment-services/' +
        this.state.stablishments[0].id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_establishment_services = await res_establishment_services.json()
    this.setState({
      ...this.state,
      establishment_services: json_establishment_services
    })

    if (this.state.establishment_services.length > 0) {
      this.setState({ ...this.state, status_services: 'update' })
    }

    let services = this.state.services

    services.forEach((service) => {
      service.isCheckedNo = true
      service.isCheckedYes = false
      service.price = 0
    })

    this.setState({ ...this.state, services: services })

    let services_2 = this.state.services

    this.state.establishment_services.forEach((establishment_service) => {
      services_2.forEach((service) => {
        if (service.id == establishment_service.service_id) {
          service.isCheckedYes = true
          service.isCheckedNo = false
          service.price = establishment_service.price
          service.establishment_service_id = establishment_service.id
        }
      })
    })

    this.setState({ ...this.state, services: services_2 })

    let type_breakfast = this.state.type_breakfast

    type_breakfast.forEach((service) => {
      service.isCheckedNo = true
      service.isCheckedYes = false
      service.price = 0
    })

    this.setState({ ...this.state, type_breakfast: type_breakfast })

    let type_breakfast_2 = this.state.type_breakfast

    this.state.establishment_services.forEach((establishment_service) => {
      type_breakfast_2.forEach((service) => {
        if (service.id == establishment_service.breakfast_type_id) {
          service.isCheckedYes = true
          service.isCheckedNo = false
          service.price = establishment_service.price
          service.establishment_service_id = establishment_service.id
        }
      })
    })

    this.setState({
      ...this.state,
      type_breakfast: type_breakfast,
      isShowContent: true
    })
    //consoleLog(this.state.type_breakfast, this.state.services);
  }

  async handleChekedPosition(event) {
    let services = this.state.services
    services.forEach((service) => {
      if (service.id === parseInt(event.target.value)) {
        service.isCheckedYes = !service.isCheckedYes
        service.isCheckedNo = !service.isCheckedNo
      }
    })
    this.setState({ ...this.state, services: services })
  }

  async handleChekedPositionBreakfast(event) {
    let type_breakfast = this.state.type_breakfast
    type_breakfast.forEach((service) => {
      if (service.id === parseInt(event.target.value)) {
        service.isCheckedYes = !service.isCheckedYes
        service.isCheckedNo = !service.isCheckedNo
      }
    })
    this.setState({ ...this.state, type_breakfast: type_breakfast })
  }

  handleChangePrixBreakfast(event) {
    let type_breakfast = this.state.type_breakfast
    type_breakfast.forEach((service) => {
      if (service.id === parseInt(event.target.id)) {
        service.price = event.target.value
      }
    })
    this.setState({ ...this.state, type_breakfast: type_breakfast })
  }

  handleChangePrixServices(event) {
    let services = this.state.services
    services.forEach((service) => {
      if (service.id === parseInt(event.target.id)) {
        service.price = event.target.value
      }
    })
    this.setState({ ...this.state, services: services })
  }

  //POST save information establishment
  async handleStepThree(event) {
    event.preventDefault()
    this.setState({ ...this.state, savingStepButton: 'loading' })
    const cookies = new Cookies()
    const res = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.stablishments[0].id,
      {
        body: JSON.stringify({
          is_active: this.state.stablishments[0]?.origin === 4 ? false : true,
          name: this.state.stablishments[0].name,
          type_id: this.state.stablishments[0].type_id,
          clasification_id: this.state.stablishments[0].clasification_id,
          address: this.state.stablishments[0].address,
          lat: this.state.stablishments[0].lat,
          lng: this.state.stablishments[0].lng,
          country_id: this.state.stablishments[0].country_id,
          contact_name: this.state.stablishments[0].contact_name,
          contact_lastname: this.state.stablishments[0].contact_lastname,
          contact_email: this.state.stablishments[0].contact_email,
          contact_phone: this.state.stablishments[0].contact_phone,
          description: this.state.stablishments[0].description,
          min_entry_time: this.state.stablishments[0].min_entry_time,
          max_entry_time: this.state.stablishments[0].max_entry_time,
          max_exit_time: this.state.stablishments[0].max_exit_time,
          tva: this.state.stablishments[0].tva,
          decl_inc_pro: this.state.stablishments[0].decl_inc_pro,
          amount_taxe: this.state.stablishments[0].amount_taxe,
          slug: this.state.stablishments[0].slug,
          region_name: this.state.stablishments[0].region_name,
          city_name: this.state.stablishments[0].city_name,
          page_url: '/onboarding/etablissement/step-four',
          page: 11
        }),
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'PUT'
      }
    )
    const result = await res.json()
    const status = await res.status
    if (status == 200) {
      if (this.state.status_services == 'create') {
        this.state.services.forEach((service) => {
          if (service.id == 1) {
            this.state.type_breakfast.forEach((breakfast) => {
              if (breakfast.isCheckedYes == true) {
                fetch(
                  process.env.NEXT_PUBLIC_API_URL + 'establishment-services',
                  {
                    body: JSON.stringify({
                      establishment_id: this.state.stablishments[0].id,
                      service_id: service.id,
                      price: breakfast.price,
                      breakfast_type_id: breakfast.id
                    }),
                    headers: {
                      'Content-Type': 'application/json',
                      Authorization: 'Bearer ' + cookies.get('tk_user')
                    },
                    method: 'POST'
                  }
                )
              }
            })
          } else {
            if (service.isCheckedYes == true) {
              fetch(
                process.env.NEXT_PUBLIC_API_URL + 'establishment-services',
                {
                  body: JSON.stringify({
                    establishment_id: this.state.stablishments[0].id,
                    service_id: service.id,
                    price: service.price,
                    breakfast_type_id: ''
                  }),
                  headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + cookies.get('tk_user')
                  },
                  method: 'POST'
                }
              )
            }
          }
        })
        this.setState({ ...this.state, savingStepButton: 'saved' })
        notify(`Données sauvegardées avec succès`, 'success')
        Router.push('/onboarding/etablissement/step-four')
      } else {
        this.state.services.forEach((service) => {
          if (service.id == 1) {
            this.state.type_breakfast.forEach((breakfast) => {
              if (breakfast.isCheckedYes == true) {
                if (breakfast.establishment_service_id !== undefined) {
                  fetch(
                    process.env.NEXT_PUBLIC_API_URL +
                      'update-service/' +
                      breakfast.establishment_service_id,
                    {
                      body: JSON.stringify({
                        establishment_id: this.state.stablishments[0].id,
                        service_id: service.id,
                        price: breakfast.price,
                        breakfast_type_id: breakfast.id
                      }),
                      headers: {
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + cookies.get('tk_user')
                      },
                      method: 'POST'
                    }
                  )
                } else {
                  fetch(
                    process.env.NEXT_PUBLIC_API_URL + 'establishment-services',
                    {
                      body: JSON.stringify({
                        establishment_id: this.state.stablishments[0].id,
                        service_id: service.id,
                        price: breakfast.price,
                        breakfast_type_id: breakfast.id
                      }),
                      headers: {
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + cookies.get('tk_user')
                      },
                      method: 'POST'
                    }
                  )
                }
              }
            })
          } else {
            if (service.isCheckedYes == true) {
              if (service.establishment_service_id !== undefined) {
                fetch(
                  process.env.NEXT_PUBLIC_API_URL +
                    'update-service/' +
                    service.establishment_service_id,
                  {
                    body: JSON.stringify({
                      establishment_id: this.state.stablishments[0].id,
                      service_id: service.id,
                      price: service.price,
                      breakfast_type_id: ''
                    }),
                    headers: {
                      'Content-Type': 'application/json',
                      Authorization: 'Bearer ' + cookies.get('tk_user')
                    },
                    method: 'POST'
                  }
                )
              } else {
                fetch(
                  process.env.NEXT_PUBLIC_API_URL + 'establishment-services',
                  {
                    body: JSON.stringify({
                      establishment_id: this.state.stablishments[0].id,
                      service_id: service.id,
                      price: service.price,
                      breakfast_type_id: ''
                    }),
                    headers: {
                      'Content-Type': 'application/json',
                      Authorization: 'Bearer ' + cookies.get('tk_user')
                    },
                    method: 'POST'
                  }
                )
              }
            }
          }
        })
        this.setState({ ...this.state, savingStepButton: 'saved' })
        notify(`Données sauvegardées avec succès`, 'success')
        Router.push('/onboarding/etablissement/step-four')
      }
    }
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={3} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div className="row background-white container-onboarding">
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="establissement"
                  title="Établissement"
                />
              </div>
              <div className="row container-onboarding">
                <div className="subttitle-container">
                  <h2>Services que vous offrez à vos invités</h2>
                  <p className="p-subtitle">
                    Si le service proposé est déjà inclus (offert) dans le prix
                    de la nuitée, indiquez “OUI” et un prix à 0€.
                  </p>
                </div>
                {this.state.isShowContent ? (
                  <form className="form" onSubmit={this.handleStepThree}>
                    {this.state.services.map((value_services, index) => {
                      return (
                        <div key={index} className="card-white">
                          <div className="row">
                            <h3
                              className="col-12 px-3 card-white__responsive-title--line-height"
                              style={{ marginBottom: 25 + 'px' }}
                            >
                              {value_services.name}
                            </h3>
                            {value_services.id == 1 &&
                              this.state.type_breakfast.map((value, index) => {
                                return (
                                  <div key={index} className="col-12">
                                    <div className="row">
                                      <div className="col-12 col-md-6">
                                        <h4>{value.name}</h4>
                                        <div className="row col-12 mb-3">
                                          <div className="col-5 col-sm-4 col-md-5 col-xl-5 card-radio justify-content-center d-flex">
                                            <div className="form-check mt-3">
                                              <input
                                                className="form-check-input"
                                                type="radio"
                                                name={'breakfast_' + value.id}
                                                id={value.id}
                                                value={value.id}
                                                onClick={
                                                  this
                                                    .handleChekedPositionBreakfast
                                                }
                                                checked={value.isCheckedYes}
                                              />
                                              <label
                                                className="form-check-label"
                                                htmlFor={value.id}
                                              >
                                                Oui
                                              </label>
                                            </div>
                                          </div>
                                          <div className="col-5 col-sm-4 col-md-5 col-xl-5 card-radio justify-content-center d-flex">
                                            <div className="form-check mt-3">
                                              <input
                                                className="form-check-input"
                                                type="radio"
                                                name={'breakfast_' + value.id}
                                                id={value.id}
                                                value={value.id}
                                                onClick={
                                                  this
                                                    .handleChekedPositionBreakfast
                                                }
                                                checked={value.isCheckedNo}
                                              />
                                              <label
                                                className="form-check-label"
                                                htmlFor={value.id}
                                              >
                                                Non
                                              </label>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="col-12 col-md-6 col-xl-6">
                                        <h4
                                          htmlFor={'prix_breakfast_' + value.id}
                                        >
                                          Prix/Personne
                                        </h4>
                                        <input
                                          onKeyPress={(e) => {
                                            e.key === 'Enter' &&
                                              e.preventDefault()
                                          }}
                                          type="number"
                                          onWheel={(e) => e.target.blur()}
                                          className="form-control"
                                          id={value.id}
                                          name={'prix_breakfast_' + value.id}
                                          aria-describedby="emailHelp"
                                          placeholder=""
                                          onChange={
                                            this.handleChangePrixBreakfast
                                          }
                                          defaultValue={value.price}
                                          readOnly={value.isCheckedNo == true}
                                          min="0"
                                          step=".01"
                                        />
                                      </div>
                                    </div>
                                  </div>
                                )
                              })}
                            {value_services.id !== 1 && (
                              <div className="col-12">
                                <div className="row">
                                  <div className="col-12 col-md-6">
                                    {/* <h4>C'est inclus?</h4> */}
                                    <div
                                      className="form-group"
                                      style={{ marginTop: '2rem' }}
                                    >
                                      <div className="row col-12">
                                        <div className="col-5 col-sm-4 col-md-5 col-xl-5 card-radio justify-content-center d-flex">
                                          <div className="form-check mt-3">
                                            <input
                                              className="form-check-input"
                                              type="radio"
                                              name={
                                                'service_' + value_services.id
                                              }
                                              id={value_services.id}
                                              value={value_services.id}
                                              onClick={
                                                this.handleChekedPosition
                                              }
                                              checked={
                                                value_services.isCheckedYes
                                              }
                                            />
                                            <label className="form-check-label">
                                              Oui
                                            </label>
                                          </div>
                                        </div>
                                        <div className="col-5 col-sm-4 col-md-5 col-xl-5 card-radio justify-content-center d-flex">
                                          <div className="form-check mt-3">
                                            <input
                                              className="form-check-input"
                                              type="radio"
                                              name={
                                                'service_' + value_services.id
                                              }
                                              id={value_services.id}
                                              value={value_services.id}
                                              onClick={
                                                this.handleChekedPosition
                                              }
                                              checked={
                                                value_services.isCheckedNo
                                              }
                                            />
                                            <label
                                              className="form-check-label"
                                              htmlFor={value_services.id}
                                            >
                                              Non
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-12 col-md-6 col-xl-6">
                                    <h4
                                      htmlFor={
                                        'prix_services_' + value_services.id
                                      }
                                    >
                                      Prix/Personne
                                    </h4>
                                    <input
                                      onKeyPress={(e) => {
                                        e.key === 'Enter' && e.preventDefault()
                                      }}
                                      type="number"
                                      onWheel={(e) => e.target.blur()}
                                      className="form-control"
                                      id={value_services.id}
                                      name={
                                        'prix_services_' + value_services.id
                                      }
                                      aria-describedby="emailHelp"
                                      placeholder=""
                                      onChange={this.handleChangePrixServices}
                                      defaultValue={value_services.price}
                                      readOnly={
                                        value_services.isCheckedNo == true
                                      }
                                      min="0"
                                      step=".01"
                                    />
                                  </div>
                                </div>
                              </div>
                            )}
                            {/* <hr />
                            <div className="card-advice">
                              <h4>Conseil :</h4>
                              <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Nulla quam velit, vulputate eu
                                pharetra nec, mattis ac neque. Duis vulputate
                                commodo lectus, ac blandit elit tincidunt id.
                                Sed rhoncus, tortor sed eleifend tristique,
                                tortor mauris molestie elit, et{' '}
                              </p>
                            </div> */}
                          </div>
                        </div>
                      )
                    })}

                    <div className="row mt-5 buttons-onboarding">
                      <div className="col-12 col-xl-6 button-back-onboarding">
                        <a
                          className="button button-outline-black button-long"
                          type="button"
                          href="/onboarding/etablissement/step-two"
                        >
                          Retour
                        </a>
                      </div>
                      <div className="col-12 col-xl-6 justify-content-end d-flex">
                        <button
                          type="submit"
                          className="button button-long button--save-onboarding"
                          disabled={
                            this.state.savingStepButton !== 'loading' &&
                            this.state.savingStepButton !== 'saved'
                              ? false
                              : true
                          }
                        >
                          {this.state.savingStepButton === 'loading'
                            ? 'En cours...'
                            : this.state.savingStepButton === 'saved'
                            ? 'Enregistré'
                            : 'Sauvegarder et continuer'}
                        </button>
                      </div>
                    </div>
                  </form>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}
// const mapDispatchToProps = {
// }
export default connect(mapStateToProps, null)(withGreenScore(StepThree))
