import React from 'react'
import Image from 'next/image'
import Router from 'next/router'
import Link from 'next/link'

import Cookies from 'universal-cookie'

import ProgressBar from '../../../components/progressbar'
import Container from '../../../components/container'
import Steper from '../../../components/steper'
import { Loader } from 'components/'
import { connect } from 'react-redux'
import withGreenScore from 'HOC/withGreenScore'
import { toast } from 'react-toastify'
import { notify } from 'helpers/notificationClassHelper'

class StepFive extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      step: 5,
      first_text: 'Taxe de séjour',
      last_text: `Conditions d' annulation`,
      tva: [],
      revenues: [],
      user_data: [],
      stablishments: [],
      isShowSidebar: '',
      showSidebar: true,
      isShowContent: false
    }

    this.handleChangeTva = this.handleChangeTva.bind(this)
    this.handleChangehandleChangeDeclIncPro =
      this.handleChangehandleChangeDeclIncPro.bind(this)
    this.handleStepFive = this.handleStepFive.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
  }

  async componentDidMount() {
    Router.push('/onboarding/etablissement/step-six')
    const cookies = new Cookies()
    let _this = this

    //Get user data
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get stablishment
    const res_stablishments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments = await res_stablishments.json()
    if (json_stablishments.length === 0) {
      toast.warning(`établissement non trouvé. sera redirigé pour création`, {
        position: toast.POSITION.BOTTOM_LEFT
      })
      return setTimeout(() => {
        window.location.replace(
          `${process.env.NEXT_PUBLIC_URL}onboarding/etablissement/step-one`
        )
      }, 3000)
    }
    this.setState({ ...this.state, stablishments: json_stablishments })

    let stablishments = this.state.stablishments

    stablishments.forEach((stablishment) => {
      if (stablishment.tva !== null) {
        stablishment.isCheckedTvaYes = true
        stablishment.isCheckedTvaNo = false
      } else {
        stablishment.isCheckedTvaYes = false
        stablishment.isCheckedTvaNo = true
      }
      if (stablishment.decl_inc_pro !== null) {
        if (stablishment.decl_inc_pro == 1) {
          stablishment.isCheckedDeclIncProYes = true
          stablishment.isCheckedDeclIncProNo = false
        } else {
          stablishment.isCheckedDeclIncProYes = false
          stablishment.isCheckedDeclIncProNo = true
        }
      } else {
        stablishment.isCheckedDeclIncProYes = false
        stablishment.isCheckedDeclIncProNo = true
      }
    })

    this.setState({
      ...this.state,
      stablishments: stablishments,
      isShowContent: true
    })
  }

  handleChangeTva(event) {
    let stablishments = this.state.stablishments

    stablishments.forEach((stablishment) => {
      stablishment.isCheckedTvaYes = !stablishment.isCheckedTvaYes
      stablishment.isCheckedTvaNo = !stablishment.isCheckedTvaNo
    })

    this.setState({ ...this.state, stablishments: stablishments })
  }

  handleChangehandleChangeDeclIncPro() {
    let stablishments = this.state.stablishments

    stablishments.forEach((stablishment) => {
      stablishment.isCheckedDeclIncProYes = !stablishment.isCheckedDeclIncProYes
      stablishment.isCheckedDeclIncProNo = !stablishment.isCheckedDeclIncProNo
    })

    this.setState({ ...this.state, stablishments: stablishments })
  }

  async handleStepFive(event) {
    event.preventDefault()
    const cookies = new Cookies()
    const res = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.stablishments[0].id,
      {
        body: JSON.stringify({
          is_active: this.state.stablishments[0]?.origin === 4 ? false : true,
          name: this.state.stablishments[0].name,
          type_id: this.state.stablishments[0].type_id,
          clasification_id: this.state.stablishments[0].clasification_id,
          address: this.state.stablishments[0].address,
          lat: this.state.stablishments[0].lat,
          lng: this.state.stablishments[0].lng,
          country_id: this.state.stablishments[0].country_id,
          contact_name: this.state.stablishments[0].contact_name,
          contact_lastname: this.state.stablishments[0].contact_lastname,
          contact_email: this.state.stablishments[0].contact_email,
          contact_phone: this.state.stablishments[0].contact_phone,
          description: this.state.stablishments[0].description,
          min_entry_time: this.state.stablishments[0].min_entry_time,
          max_entry_time: this.state.stablishments[0].max_entry_time,
          max_exit_time: this.state.stablishments[0].max_exit_time,
          region_name: this.state.stablishments[0].region_name,
          city_name: this.state.stablishments[0].city_name,
          tva: event.target.tva.value,
          decl_inc_pro: event.target.decl_inc_pro.value,
          //amount_taxe: event.target.amount_taxe.value,

          slug: this.state.stablishments[0].slug,
          page_url: '/onboarding/etablissement/step-six',
          page: 13
        }),
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'PUT'
      }
    )

    const result = await res.json()
    const status = await res.status

    notify(`Données sauvegardées avec succès`, 'success')
    if (this.state.stablishments[0].origin === 4) {
      return Router.push('/onboarding/etablissement/finish ')
    }

    if (status == 200) {
      Router.push('/onboarding/etablissement/step-six')
    }
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={3} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div className="row background-white container-onboarding">
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="establissement"
                  title="Établissement"
                />
              </div>
              <div className="row container-onboarding">
                <div className="subttitle-container">
                  <h2>Taxe de séjour</h2>
                  <p>
                    En tant que plateforme de réservation, We Go GreenR doit
                    récolter et reverser à l’État la taxe de séjour des
                    réservations effectuées auprès d’hébergeurs
                    non-professionnels.
                  </p>
                  <p>
                    Afin de savoir si vous êtes concerné·e par cette mesure,
                    veuillez répondre aux question ci-dessous.
                  </p>
                </div>
                {this.state.isShowContent ? (
                  <form className="form" onSubmit={this.handleStepFive}>
                    <div className="card-white">
                      <h3>Informations fiscales</h3>

                      <div className="row">
                        <div className="col-12 col-md-6 col-xl-6">
                          <h4>N° TVA</h4>
                          <div className="form-group col-12">
                            <div className="row">
                              <div className="col-12 col-md-5 col-xl-5 card-radio justify-content-center d-flex">
                                <div className="form-check mt-3">
                                  <input
                                    className="form-check-input"
                                    type="radio"
                                    name="tva_establissement"
                                    id="tva_establissement"
                                    value="tva_establissement"
                                    onClick={this.handleChangeTva}
                                    checked={
                                      this.state.stablishments.length > 0 &&
                                      this.state.stablishments[0]
                                        .isCheckedTvaYes
                                    }
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor="tva_establissement"
                                  >
                                    Oui
                                  </label>
                                </div>
                              </div>
                              <div className="col-12 col-md-5 col-xl-5 card-radio justify-content-center d-flex">
                                <div className="form-check mt-3">
                                  <div className="form-check">
                                    <input
                                      className="form-check-input"
                                      type="radio"
                                      name="tva_establissement"
                                      id="tva_establissement"
                                      value="tva_establissement"
                                      onClick={this.handleChangeTva}
                                      checked={
                                        this.state.stablishments.length > 0 &&
                                        this.state.stablishments[0]
                                          .isCheckedTvaNo
                                      }
                                    />
                                    <label
                                      className="form-check-label"
                                      htmlFor="tva_establissement"
                                    >
                                      Non
                                    </label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-12 col-md-6 col-xl-6">
                          <h4 htmlFor="prix_tva">Le numéro *</h4>
                          <input
                            type="text"
                            className="form-control"
                            id="tva"
                            name="tva"
                            aria-describedby="emailHelp"
                            placeholder=""
                            step="any"
                            defaultValue={
                              this.state.stablishments.length > 0
                                ? this.state.stablishments[0].tva
                                : ''
                            }
                            min="0"
                            required={
                              this.state.stablishments.length > 0 &&
                              this.state.stablishments[0].isCheckedTvaYes
                            }
                            readOnly={
                              this.state.stablishments.length > 0 &&
                              this.state.stablishments[0].isCheckedTvaNo
                            }
                          />
                        </div>
                      </div>
                      <hr />
                    </div>
                    <div className="card-white">
                      <h3>
                        Revenus déclarés en tant que professionnel pour les
                        impots directs ?
                      </h3>

                      <div className="row">
                        <div className="col-12 col-md-6 col-xl-6">
                          <div className="form-group col-12">
                            <div className="row">
                              <div className="col-12 col-md-5 col-xl-5 card-radio justify-content-center d-flex">
                                <div className="form-check mt-3">
                                  <input
                                    className="form-check-input"
                                    type="radio"
                                    name="decl_inc_pro"
                                    id="decl_inc_pro"
                                    value={1}
                                    onClick={
                                      this.handleChangehandleChangeDeclIncPro
                                    }
                                    checked={
                                      this.state.stablishments.length > 0 &&
                                      this.state.stablishments[0]
                                        .isCheckedDeclIncProYes
                                    }
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor="decl_inc_pro"
                                  >
                                    Oui
                                  </label>
                                </div>
                              </div>
                              <div className="col-12 col-md-5 col-xl-5 card-radio justify-content-center d-flex">
                                <div className="form-check mt-3">
                                  <input
                                    className="form-check-input"
                                    type="radio"
                                    name="decl_inc_pro"
                                    id="decl_inc_pro"
                                    value={0}
                                    onClick={
                                      this.handleChangehandleChangeDeclIncPro
                                    }
                                    checked={
                                      this.state.stablishments.length > 0 &&
                                      this.state.stablishments[0]
                                        .isCheckedDeclIncProNo
                                    }
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor="decl_inc_pro"
                                  >
                                    Non
                                  </label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row mt-5 buttons-onboarding">
                      <div className="col-12 col-xl-6 button-back-onboarding">
                        <a
                          className="button button-outline-black button-long"
                          type="button"
                          href="/onboarding/etablissement/step-two"
                        >
                          Retour
                        </a>
                      </div>
                      <div className="col-12 col-xl-6 justify-content-end d-flex">
                        <button type="submit" className="button button-long">
                          Sauvegarder et continuer
                        </button>
                      </div>
                    </div>
                  </form>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}

// const mapDispatchToProps = {
// }

export default connect(mapStateToProps, null)(withGreenScore(StepFive))
