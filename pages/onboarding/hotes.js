import React from 'react'
import { consoleLog } from '/utils/logConsole'
import { toast } from 'react-toastify'
//Next Libraries
import Image from 'next/image'
import Router from 'next/router'
import Link from 'next/link'

//External Libraries
import Cookies from 'universal-cookie'

//Components
import { Loader } from 'components/'
import Steper from '../../components/steper'
import Container from '../../components/container'
import { ArIntegration } from '@components/Onboarding/ArIntegration'

class Hotes extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      how_heard: [],
      user_data: '',
      host_informations: '',
      status: 'create',
      default_value_how_know: 0,
      isEnableButton: true,
      isShowSidebar: '',
      showSidebar: true,
      isShowContent: false,
      integrateAR: false
    }
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
    this.handleHotes = this.handleHotes.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    //Get how heard about options
    const res_about = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'how-heard-about-options',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_about = await res_about.json()
    this.setState({ ...this.state, how_heard: json_about })

    let _this = this
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        //consoleLog(response.status);
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'host-informations/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
      .then(function (response) {
        //consoleLog(response.status);
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (host_informations) {
        _this.setState({
          host_informations: host_informations,
          isShowContent: true
        })
        if (_this.state.host_informations.length > 0) {
          _this.setState({ status: 'update' })
        }
      })

    const header = document.getElementById('headerOnboarding')

    return () => {
      window.removeEventListener('scroll', scrollCallBack)
    }
  }

  async handleHotes(event) {
    if (this.state.status == 'create') {
      const cookies = new Cookies()
      event.preventDefault()
      let form_data = new FormData()
      const res = await fetch(
        process.env.NEXT_PUBLIC_API_URL + 'host-informations',
        {
          body: JSON.stringify({
            presentation: event.target.introduction.value,
            additional_information: event.target.to_travelers.value,
            how_heard_about_id: event.target.how_know_us.value,
            user_phone: event.target.telephone.value
            //image: event.target.profile_picture.files.item(0)
          }),
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + cookies.get('tk_user')
          },
          method: 'POST'
        }
      )

      const result = await res.json()
      const status = await res.status
      //consoleLog(result);
      //consoleLog(status);
      if (status == 200) {
        Router.push('/onboarding/greenscore/step-one')
      } else {
        toast.error(`Une erreur s'est produite, réessayez plus tard.`, {
          position: toast.POSITION.BOTTOM_LEFT
        })
      }
    } else {
      const cookies = new Cookies()
      event.preventDefault()
      let form_data = new FormData()
      const res = await fetch(
        process.env.NEXT_PUBLIC_API_URL +
          'host-informations/' +
          this.state.host_informations[0].id,
        {
          body: JSON.stringify({
            presentation: event.target.introduction.value,
            additional_information: event.target.to_travelers.value,
            how_heard_about_id: event.target.how_know_us.value,
            user_phone: event.target.telephone.value
            //image: event.target.profile_picture.files.item(0)
          }),
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + cookies.get('tk_user')
          },
          method: 'PUT'
        }
      )

      const result = await res.json()
      const status = await res.status
      //consoleLog(result);
      //consoleLog(status);
      if (status == 200) {
        fetch(process.env.NEXT_PUBLIC_API_URL + 'last-page', {
          body: JSON.stringify({
            page_url: '/onboarding/greenscore/step-one',
            page: 1
          }),
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + cookies.get('tk_user')
          },
          method: 'POST'
        }).then(() => {
          Router.push('/onboarding/greenscore/step-one')
        })
      } else {
        toast.error(`Une erreur s'est produite, réessayez plus tard`, {
          position: toast.POSITION.BOTTOM_LEFT
        })
      }
    }
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={1} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-md-9 col-xl-9">
              <div
                className="row background-white container-onboarding"
                id="headerOnboarding"
              >
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <div className="col-12 col-xl-6">
                  <h1 className="background-white">GreenScore</h1>
                </div>
              </div>
              <div className="row container-onboarding">
                <div className="subttitle-container">
                  <h2>Hôte</h2>
                  <p>
                    Si vous souhaitez renseigner un contact privilégié pour la
                    gestion de votre établissement, vous pourrez l’indiquer lors
                    de l’étape 3.
                  </p>
                </div>
                {this.state.isShowContent ? (
                  <form
                    className="form"
                    onSubmit={this.handleHotes}
                    method="POST"
                  >
                    <div className="card-white">
                      <h3>Informations générales sur l'hôte</h3>
                      <div className="row">
                        <div className="col-12 col-md-6 col-xl-6">
                          <div className="form-group">
                            <h4 htmlFor="prenom_gestionnaire">Prénom</h4>
                            <input
                              type="text"
                              className="form-control"
                              id="prenom_gestionnaire"
                              name="prenom_gestionnaire"
                              aria-describedby="emailHelp"
                              placeholder=""
                              value={this.state.user_data.name}
                              readOnly
                            />
                          </div>
                        </div>
                        <div className="col-12 col-md-6 col-xl-6">
                          <div className="form-group">
                            <h4 htmlFor="nom_gestionnaire">Nom</h4>
                            <input
                              type="text"
                              className="form-control"
                              id="nom_gestionnaire"
                              name="nom_gestionnaire"
                              aria-describedby="emailHelp"
                              placeholder=""
                              value={this.state.user_data.lastname}
                              readOnly
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 col-md-6 col-xl-6">
                          <div className="form-group">
                            <h4 htmlFor="email">Email</h4>
                            <input
                              type="email"
                              className="form-control"
                              id="email"
                              name="email"
                              aria-describedby="emailHelp"
                              placeholder=""
                              value={this.state.user_data.email}
                              readOnly
                            />
                          </div>
                        </div>
                        <div className="col-12 col-md-6 col-xl-6">
                          <div className="form-group">
                            <h4 htmlFor="telephone">Téléphone</h4>
                            <input
                              type="number"
                              onWheel={(e) => e.target.blur()}
                              className="form-control"
                              id="telephone"
                              name="telephone"
                              aria-describedby="emailHelp"
                              placeholder=""
                              value={this.state.user_data.phone}
                              onChange={(e) => {
                                this.setState({
                                  ...this.state,
                                  user_data: {
                                    ...this.user_data,
                                    phone: e.target.value
                                  }
                                })
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="tip-container">
                          <h4 htmlFor="introduction">
                            Un petit mot pour vous présenter ?
                          </h4>
                          <div className="tip-text">
                            <span>
                              Grandement apprécié par nos voyageurs : vos
                              passions, vos valeurs...
                            </span>
                          </div>
                        </div>
                        <textarea
                          required
                          className="form-control"
                          name="introduction"
                          placeholder="Grandement apprécié..."
                          id="introduction"
                          defaultValue={
                            this.state.host_informations.length > 0
                              ? this.state.host_informations[0].presentation
                              : ''
                          }
                        ></textarea>
                      </div>
                      <div className="form-group">
                        <div className="tip-container">
                          <h4 htmlFor="to_travelers">
                            Prenez quelques instants pour expliquer votre
                            démarche écoresponsable
                          </h4>
                          <div className="tip-text">
                            <span>
                              Grandement apprécié par nos voyageurs : la genèse,
                              le sens de votre démarche, ce qui vous fait
                              vibrer...
                            </span>
                          </div>
                        </div>
                        <textarea
                          className="form-control select-without-margin-bottom"
                          name="to_travelers"
                          id="to_travelers"
                          required
                          defaultValue={
                            this.state.host_informations.length > 0
                              ? this.state.host_informations[0]
                                  .additional_information
                              : ''
                          }
                        ></textarea>
                      </div>
                    </div>
                    {/* <ArIntegration email={this.state.user_data.email} /> */}
                    <div className="card-white">
                      <div>
                        <h4 htmlFor="how_know_us">
                          Comment avez-vous entendu parler de nous ?
                        </h4>
                        <div
                          style={{
                            marginBottom: 10 + 'px',
                            marginTop: 32 + 'px'
                          }}
                        >
                          <span style={{ fontWeight: 'bold' }}>
                            Choisir une option
                          </span>
                        </div>
                        <select
                          className="form-control select-without-margin-bottom"
                          id="how_know_us"
                          required
                        >
                          <option value=""></option>
                          {this.state.host_informations.length > 0
                            ? this.state.how_heard.map((value, index) => {
                                if (
                                  this.state.host_informations[0]
                                    .how_heard_about_id == value.id
                                ) {
                                  return (
                                    <option
                                      value={value.id}
                                      key={index}
                                      selected
                                    >
                                      {value.name}
                                    </option>
                                  )
                                } else {
                                  return (
                                    <option value={value.id} key={index}>
                                      {value.name}
                                    </option>
                                  )
                                }
                              })
                            : this.state.how_heard.map((value, index) => {
                                return (
                                  <option value={value.id} key={index}>
                                    {value.name}
                                  </option>
                                )
                              })}
                        </select>
                      </div>
                    </div>
                    <div className="row mt-5 buttons-onboarding">
                      <div className="col-12 col-md-8 col-xl-8 button-back-onboarding">
                        <Link href="/onboarding/presentation">
                          <a
                            className="button button-outline-black"
                            type="button"
                          >
                            Retour
                          </a>
                        </Link>
                      </div>
                      <div className="col-12 col-md-4 col-xl-4 justify-content-end d-flex">
                        <button type="submit" className="button button-long">
                          Sauvegarder et continuer
                        </button>
                      </div>
                    </div>
                  </form>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

export default Hotes
