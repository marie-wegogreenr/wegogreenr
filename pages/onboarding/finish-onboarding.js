import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import Image from 'next/image'
import Head from 'next/head'
import noImage from '../../public/images/not-image.svg'

import Cookies from 'universal-cookie'

import Container from '../../components/container'
import { Loader } from 'components/'
import withGreenScore from 'HOC/withGreenScore'
import { connect } from 'react-redux'
import { notify } from 'helpers/notificationClassHelper'
// import FarSection from '@components/Onboarding/FarSection'

class FinishOnboarding extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      host_informations: [],
      user_data: [],
      stablishments: [],
      rooms: [],
      quantityRooms: 0,
      establishment_types: [],
      greenscore_level: 0,
      isShowContent: false
    }

    this.handleConvertHost = this.handleConvertHost.bind(this)
    this.handleCreateRoom = this.handleCreateRoom.bind(this)
    this.handleStripeConect = this.handleStripeConect.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    let _this = this

    //Get user data
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get type stablishment
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'establishment-types', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        return response.json()
      })
      .then(function (establishment_types) {
        _this.setState({ establishment_types: establishment_types })
      })

    //Get host information
    await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'host-informations/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
      .then(function (response) {
        return response.json()
      })
      .then(function (host_informations) {
        _this.setState({
          ..._this.state,
          host_informations: host_informations
        })
      })

    //Get establishment
    const res_stablishments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments = await res_stablishments.json()
    this.setState({ ...this.state, stablishments: json_stablishments })

    //Get rooms
    const res_rooms = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishment-rooms/' +
        this.state.stablishments[0].id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_rooms = await res_rooms.json()

    let acum = 0
    let room = json_rooms
    let bar = new Promise((resolve, reject) => {
      room.forEach(async (room, index, array) => {
        //Get images room
        const res_images = await fetch(
          process.env.NEXT_PUBLIC_API_URL + 'room-images/' + room.id,
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'GET'
          }
        )
        let first_image
        const json_images = await res_images.json()
        json_images.forEach((image) => {
          if (image.order == 1) {
            first_image = image.image.url
          }
        })
        room.image = first_image
        acum = acum + 1
        if (index === array.length - 1) resolve()
      })
    })

    bar.then(() => {
      this.setState({ ...this.state, rooms: room, quantityRooms: acum })
    })

    //Get final score
    const res_final_score = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'calculate-points/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_final_score = await res_final_score.json()
    let greenscore_status

    this.setState({
      ...this.state,
      greenscore_level: json_final_score.greenscore_level,
      isShowContent: true
    })
  }

  async handleConvertHost(event) {
    event.preventDefault()
    const cookies = new Cookies()
    fetch(process.env.NEXT_PUBLIC_API_URL + 'becomeToHost', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    }).then((response) => {
      if (response.status !== 200) {
        if (cookies.get('tk_user')) {
          cookies.remove('tk_user')
        }
        Router.push('/login')
      }

      Router.push('/host/dashboard')
    })
  }

  async handleCreateRoom(event) {
    event.preventDefault()
    const cookies = new Cookies()
    var result = []
    var characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    var charactersLength = characters.length
    for (var i = 0; i < 32; i++) {
      result.push(
        characters.charAt(Math.floor(Math.random() * charactersLength))
      )
    }
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'rooms', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      body: JSON.stringify({
        slug: result.join('') + '-' + Math.floor(Math.random() * 101)
      }),
      method: 'POST'
    })
      .then((response) => {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then((room) => {
        Router.push('/onboarding/hebergements/step-one?id=' + room.id)
      })
  }

  async handleStripeConect() {
    const cookies = new Cookies()
    const options = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      }
    }

    fetch(`${process.env.NEXT_PUBLIC_API_URL}becomeToHost`, options)
      .then((res) => res.json())
      .then((data) => {
        if (data.login_url) {
          window.open(`${data.login_url}`, '_blank')
        } else {
          window.open(`${data.url}`, '_blank')
        }
      })
      .catch((err) => {
        notify(`Opps! nous avons une erreur`, 'error')
      })
  }

  render() {
    return (
      <Container>
        {/*  CONTAINER  */}
        <div className="col-12 header-onboarding">
          <Link href="/host/dashboard">
            <img
              src="/images/logo.svg"
              alt="Logo"
              height={84}
              width={72}
              style={{ marginTop: 5 + 'px', cursor: 'pointer' }}
            />
          </Link>
        </div>
        <div className="container-xxl">
          <Head>
            <title>We go greenr</title>
          </Head>
          <div className="row mb-5">
            <div className="finish-onboarding__grid d-lg-flex pb-5">
              <div className="col-12 col-lg-6 finish-onboardign-left d-flex justify-content-center align-items-center mb-4 mb-lg-0">
                <div className="row justify-content-center">
                  <img
                    src="/images/check.png"
                    alt="Check"
                    height={50}
                    width={50}
                    className="col-auto"
                  />
                  <h1>Félicitations</h1>
                  <p style={{ fontWeight: 'bold' }}>
                    Votre hébergement est créé
                  </p>
                  <div className="col-12">
                    <button
                      className="button text-center w-100"
                      type="button"
                      onClick={this.handleStripeConect}
                    >
                      Renseignez dès maintenant vos informations bancaires
                    </button>
                  </div>
                  <br></br>
                  <div className="col-12">
                    <button
                      className="button text-center mt-3 w-100"
                      type="button"
                      onClick={this.handleConvertHost}
                    >
                      Sauter informations bancaires
                    </button>
                  </div>
                </div>
              </div>
              {this.state.isShowContent ? (
                <div className="col-12 col-lg-6 finish-onboardign-right">
                  <div className="card-white p-4">
                    {this.state.stablishments.length > 0 && (
                      <div className="row">
                        <div className="col-12 row align-items-center pe-0">
                          <h2 className="col-12 col-sm-6 col-lg-12 col-xl-6 order-1 order-sm-0 order-lg-1 order-xl-0 mb-0">
                            Établissement
                          </h2>
                          <Link href="/onboarding/etablissement/step-one">
                            <a className="col-12 col-sm-6 col-lg-12 col-xl-6 order-0 order-sm-1 order-lg-0 order-xl-1 text-start text-sm-end text-lg-start text-xl-end mb-2 mb-sm-0 mb-lg-2 mb-xl-0">
                              Modifier Établissement
                            </a>
                          </Link>
                        </div>
                        <div
                          className="col-12"
                          style={{ marginTop: '10px', fontSize: '20px' }}
                        >
                          <h3>{this.state.stablishments[0].name}</h3>
                          <hr />
                        </div>
                        <div className="col-12">
                          <p>
                            <strong>Hôte : </strong>{' '}
                            {this.state.stablishments[0].contact_name}{' '}
                            {this.state.stablishments[0].contact_lastname}
                          </p>
                          <p>
                            <strong>Catégorie : </strong>
                            {this.state.establishment_types.map(
                              (value, index) => {
                                if (
                                  value.id ==
                                  this.state.stablishments[0].type_id
                                )
                                  return value.name
                              }
                            )}
                          </p>
                          <p>
                            <strong>Nombre d'hébergements : </strong>&nbsp;
                            {this.state.quantityRooms}
                          </p>
                          <p style={{ marginBottom: '10px' }}>
                            <strong>Adresse : </strong>
                          </p>
                          <p>
                            {this.state.stablishments[0].address}
                            {', '}
                            {this.state.stablishments[0].city_name}{' '}
                          </p>
                          <p style={{ marginBottom: '10px' }}>
                            <strong>Description : </strong>
                          </p>
                          <p>{this.state.stablishments[0].description}</p>
                          <hr />
                        </div>

                        <div className="col-12 col-sm-6 d-flex align-items-center">
                          <p style={{ margin: '0' }}>
                            <strong>GreenScore</strong>
                          </p>
                        </div>
                        <div className="col-12 col-sm-6 d-flex justify-content-start justify-content-sm-end">
                          {this.state.greenscore_level >= 1 ||
                          this.state.greenscore_level <= 4 ? (
                            <img
                              src={
                                '/images/onboarding/greenscore/final_score/result_greenscore_' +
                                this.state.greenscore_level +
                                '.png'
                              }
                              className="finish-onboarding__grid-greenscore"
                              alt={`final-green-score-img`}
                            />
                          ) : (
                            <img
                              src="/images/onboarding/greenscore/final_score/result_greenscore_5.png"
                              className="finish-onboarding__grid-greenscore"
                              alt={`final-green-score-img`}
                            />
                          )}
                        </div>
                      </div>
                    )}
                  </div>
                  <div className="card-white p-4">
                    <div className="row">
                      <div className="col-12 mb-4">
                        <h2>Hébergements créés</h2>
                      </div>
                      {this.state.rooms?.map((value, index) => {
                        return (
                          <div className="col-12" key={index}>
                            <div className="row">
                              <div className="col-12 col-sm-3 col-xl-3">
                                {/* <img
                                  src={
                                    value.image
                                      ? process.env.NEXT_PUBLIC_AMAZON_IMGS +
                                        value.image
                                      : noImage
                                  }
                                  alt="Logo"
                                  style={{
                                    width: '100px',
                                    height: '100px'
                                  }}
                                  className="rounded"
                                /> */}
                                <div
                                  style={{
                                    backgroundImage: `url(
                                      ${
                                        value.image
                                          ? process.env
                                              .NEXT_PUBLIC_AMAZON_IMGS +
                                            value.image
                                          : noImage
                                      }
                                      )`
                                  }}
                                  className="finish-onboarding__bg-image"
                                ></div>
                              </div>
                              <div className="col-12 col-sm-9 col-xl-5">
                                <h3 className="mb-3 mt-3 mt-sm-0 card-white__responsive-title--medium">
                                  {value.public_name
                                    ? value.public_name
                                    : 'Room Name'}
                                </h3>
                                {/* <p className="mb-2">Disponible du </p> */}
                                <p>Personnes : {value.people_capacity}</p>
                              </div>
                              <div className="col-12 col-xl-4 text-left text-xl-right mb-3 mb-xl-0">
                                <p>
                                  <strong>
                                    {value.basic_price ? value.basic_price : 0}{' '}
                                    €/nuit
                                  </strong>
                                </p>
                                <Link
                                  href={
                                    '/onboarding/hebergements/step-one?id=' +
                                    value.id
                                  }
                                >
                                  <a className="text-decoration-none">
                                    Modifier hébergement
                                  </a>
                                </Link>
                              </div>
                              <div className="col-12">
                                <p className="my-0 bg-light rounded-pill px-4">
                                  <small style={{ fontWeight: 'normal' }}>
                                    Hébergements similaires:{' '}
                                    <span>{value.host_quantity}</span>
                                  </small>
                                </p>
                              </div>
                            </div>
                            <hr />
                          </div>
                        )
                      })}
                      <div className="col-12 text-center">
                        <a onClick={this.handleCreateRoom} href="#">
                          Ajouter un autre hébergement
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <Loader />
              )}
            </div>
          </div>
          {/* <FarSection /> */}
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}

// const mapDispatchToProps = {
// }

export default connect(mapStateToProps, null)(withGreenScore(FinishOnboarding))
