import React from 'react'
import Image from 'next/image'
import Router from 'next/router'
import Link from 'next/link'

import Cookies from 'universal-cookie'

import ProgressBar from '../../../components/progressbar'
import Container from '../../../components/container'
import Steper from '../../../components/steper'
import { connect } from 'react-redux'
import withGreenScore from 'HOC/withGreenScore'
import { toast } from 'react-toastify'

class Add extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user_data: [],
      stablishments: [],
      rooms: [],
      isEnableButton: true,
      isShowSidebar: '',
      showSidebar: true
    }

    this.handleCreateRoom = this.handleCreateRoom.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    let _this = this
    //Get user data
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get establishment
    const res_stablishments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments = await res_stablishments.json()
    if (json_stablishments.length === 0) {
      toast.warning(`établissement non trouvé. sera redirigé pour création`, {
        position: toast.POSITION.BOTTOM_LEFT
      })
      return setTimeout(() => {
        window.location.replace(
          `${process.env.NEXT_PUBLIC_URL}onboarding/etablissement/step-one`
        )
      }, 3000)
    }

    this.setState({ ...this.state, stablishments: json_stablishments })

    //Get rooms
    const res_rooms = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishment-rooms/' +
        this.state.stablishments[0].id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_rooms = await res_rooms.json()
    this.setState({ ...this.state, rooms: json_rooms })
  }

  async handleCreateRoom(event) {
    event.preventDefault()
    const cookies = new Cookies()
    var result = []
    var characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    var charactersLength = characters.length
    for (var i = 0; i < 32; i++) {
      result.push(
        characters.charAt(Math.floor(Math.random() * charactersLength))
      )
    }

    await fetch(process.env.NEXT_PUBLIC_API_URL + 'rooms', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      body: JSON.stringify({
        slug: result.join('') + '-' + Math.floor(Math.random() * 101)
      }),
      method: 'POST'
    })
      .then((response) => {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then((room) => {
        //consoleLog(room);
        Router.push('/onboarding/hebergements/step-one?id=' + room.id)
      })
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={4} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div className="row background-white container-onboarding">
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <div className="col-12 col-xl-4">
                  <h1>Hébergements</h1>
                </div>
              </div>
              <div className="container-fluid container-onboarding">
                <h2>Ajouter un hébergement</h2>
                <p>On rentre dans le vif du sujet.</p>
                <p>
                  Commençons par le premier hébergement. Quand vous aurez saisi
                  toutes les informations nécessaires, vous pourrez renseigner
                  les hébergements suivants.
                </p>
                <div className="card-white">
                  <div className="row">
                    <div className="col-12 col-md-12 col-xl-2 d-flex justify-content-center">
                      <Image
                        src="/images/logo_hebergements.png"
                        alt="Logo"
                        width={90}
                        height={100}
                      />
                    </div>
                    <div className="col-12 col-md-12 col-xl-7">
                      <h3>Ajouter un hébergement</h3>
                    </div>
                    <div className="col-12 col-md-12 col-xl-3">
                      <div className="row">
                        <div className="col-12 mb-4">
                          <button
                            className="button button-long button--save-onboarding"
                            onClick={this.handleCreateRoom}
                          >
                            Ajouter
                          </button>
                        </div>
                        <div className="col-12">
                          <Link href="/host/dashboard">
                            <a className="link-black">Sauter pour l'instant</a>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}
// const mapDispatchToProps = {
// }
export default connect(mapStateToProps, null)(withGreenScore(Add))
