/*  Dependencies  */
import React from 'react'
import Router from 'next/router'
import Cookies from 'universal-cookie'
import Calendar from 'react-calendar'
/*  Components  */
import Container from '../../../components/container'
import ProgressBar from '../../../components/progressbar'
import Steper from '../../../components/steper'
import { Loader } from 'components/'
import PopupInfoGoogle from '../../../components/Onboarding/PopupInfoGoogle'
import withGreenScore from 'HOC/withGreenScore'
/*  Redux  */
import { connect } from 'react-redux'
/*  Styles  */
import 'react-calendar/dist/Calendar.css'
/*  Icons  */
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { ChevronUp } from '@components/Icons/ChevronUp'
/*  Helpers  */
import { notify } from 'helpers/notificationClassHelper'
import {
  calendarAddMonths,
  getDaysInMonth,
  setMonthName
} from 'helpers/calendarHelper'
import {
  getCalendarOrNot,
  getRoomAvailabilities,
  getRoomsById
} from 'services/roomServicesService'
import {
  changeArrayCalendars,
  getArrayCalendars,
  getDatesNotAvailabilities,
  getSortedDates
} from 'helpers/roomHelper'
import GoBackBtn from '@components/Onboarding/GoBackBtn'
import SaveBtn from '@components/Onboarding/SaveBtn'
import Description from '@components/Onboarding/StepFour/Description'
import Legend from '@components/Onboarding/StepFour/Legend'
import moment from 'moment'
class StepFour extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      step: 4,
      first_text: 'Calendrier et disponibilités',
      last_text: 'Prix',
      id: '',
      dates_not_availabities: [],
      availabilities: [],
      status_calendar: 'create',
      array_calendars: [],
      isEnableButton: true,
      isShowSidebar: '',
      showSidebar: true,
      rooms: [],
      quantityMonths: 4,
      selectedCalendar: '1',
      id_calendar: '',
      google_calendar_authorization: false,
      calendar_or_not: '',
      showSelectTypeCalendar: false,
      typeCalendar: [
        { name: 'Manual', value: '1', selected: false },
        { name: 'Google Calendar', value: '2', selected: false }
      ],
      isShowContent: false,
      isShowCalendar: true,
      showModalGoogle: false,
      savingStepButton: 'init',
      showArrowUp: false
    }

    this.handleStepFour = this.handleStepFour.bind(this)
    this.handleChangeDateOnClick = this.handleChangeDateOnClick.bind(this)
    this.handleChangeCalendarAvailabilitie =
      this.handleChangeCalendarAvailabilitie.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
    this.handleAddMonths = this.handleAddMonths.bind(this)
    this.handleSelectCalendarType = this.handleSelectCalendarType.bind(this)
    this.handleChangeIdCalendar = this.handleChangeIdCalendar.bind(this)
    this.handleTestAuthGoogleCalendar =
      this.handleTestAuthGoogleCalendar.bind(this)
    this.handleShowModal = this.handleShowModal.bind(this)
    this.validateStatus = this.validateStatus.bind(this)
    this.handleArrowClick = this.handleArrowClick.bind(this)

    library.add(faInfoCircle)
  }

  async componentDidMount() {
    const queryString = window.location.search
    const urlParams = new URLSearchParams(queryString)

    window.onscroll = (e) => {
      const { pageYOffset } = window
      this.setState({ ...this.state, scroll: pageYOffset })
    }

    this.setState({ ...this.state, id: urlParams.get('id') }, async () => {
      try {
        /*  Get room availabilities  */
        const res_availabilities = await getRoomAvailabilities(this.state.id)

        if (res_availabilities.code === 200) {
          this.setState({
            ...this.state,
            availabilities: res_availabilities.data
          })
        } else {
          notify(
            `Il y a eu une erreur dans les données. Essayez de recharger la page`,
            'error'
          )
        }
        /*  Update calendar status  */
        if (this.state.availabilities.length > 0) {
          this.setState({ ...this.state, status_calendar: 'update' })
        }

        let res_calendar_or_not = await getCalendarOrNot(this.state.id)
        res_calendar_or_not = res_calendar_or_not.code

        this.setState({
          ...this.state,
          calendar_or_not: res_calendar_or_not
        })

        /*  Calendar type  */
        this.state.typeCalendar.map((calendar) => {
          if (
            calendar.value == '1' &&
            res_calendar_or_not == 203 &&
            this.state.availabilities.length > 0
          ) {
            calendar.selected = true
            this.setState({
              ...this.state,
              selectedCalendar: '1',
              status_calendar: 'update',
              showSelectTypeCalendar: true
            })
          } else if (calendar.value == '2' && res_calendar_or_not == 200) {
            calendar.selected = true
            this.setState({
              ...this.state,
              selectedCalendar: '2',
              status_calendar: 'update',
              id_calendar: res_calendar_or_not.calendar_id,
              google_calendar_authorization: true,
              showSelectTypeCalendar: true
            })
          } else {
            this.setState({ ...this.state, showSelectTypeCalendar: true })
          }
        })

        /*  Get dates and sort them  */
        const { array_dates, sorted_array_dates } = getSortedDates(
          this.state.availabilities
        )

        this.setState({
          ...this.state,
          dates_not_availabities: sorted_array_dates
        })

        /*  Get array calendars  */
        const array = getArrayCalendars(this.state.quantityMonths)
        this.setState({ ...this.state, array_calendars: array })

        /*  Get rooms  */
        const res_room = await getRoomsById(this.state.id)

        if (res_room.code === 200) {
          this.setState({
            ...this.state,
            rooms: res_room.data,
            showSelectTypeCalendar: true,
            isShowContent: true
          })
        } else {
          notify(
            `Il y a eu une erreur dans les données. Essayez de recharger la page`,
            'error'
          )
        }
      } catch (error) {
        console.error(error)
        notify(
          `Il y a eu une erreur dans les données. Essayez de recharger la page`,
          'error'
        )
      }
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.scroll) {
      if (prevState.scroll > 100) {
        if (!this.state.showArrowUp) {
          this.setState({ ...this.state, showArrowUp: true })
        }
      } else {
        if (this.state.showArrowUp) {
          this.setState({ ...this.state, showArrowUp: false })
        }
      }
    }
  }

  handleArrowClick() {
    window.scrollTo(0, 0)
    this.setState({ ...this.state, showArrowUp: false })
  }

  handleChangeDateOnClick(event) {
    const { array_dates, sorted_array_dates } = getDatesNotAvailabilities(
      event,
      this.state.dates_not_availabities
    )
    this.setState({ ...this.state, dates_not_availabities: sorted_array_dates })
  }

  async handleChangeCalendarAvailabilitie(index, status_block) {
    this.setState({ ...this.state, isShowCalendar: false })

    const functionArguments = {
      index,
      status_block,
      array_calendars: this.state.array_calendars
    }
    const dates = changeArrayCalendars(
      functionArguments,
      this.handleChangeDateOnClick
    )
    this.setState({
      ...this.state,
      array_calendars: dates,
      isShowCalendar: true
    })
  }

  async handleAddMonths() {
    const { array, quantityMonths } = calendarAddMonths(
      this.state.quantityMonths
    )
    this.setState({
      ...this.state,
      array_calendars: array,
      quantityMonths: quantityMonths
    })
  }

  async handleSelectCalendarType(event) {
    this.setState({ ...this.state, selectedCalendar: event.target.value })
  }

  async handleChangeIdCalendar(event) {
    this.setState({
      ...this.state,
      id_calendar: event.target.value,
      google_calendar_authorization: false
    })
  }

  async handleTestAuthGoogleCalendar() {
    const res_google_calendar = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'calendar',
      {
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify({
          calendar_id: this.state.id_calendar
        })
      }
    )

    const status = await res_google_calendar.status
    if (status == 203) {
      notify(`L'autorisation n'est pas faite.`, 'error')
    } else {
      notify(`Succès de l'autorisation`, 'success')
      this.setState({ ...this.state, google_calendar_authorization: true })
    }
  }

  /*  POST save information establishment  */
  async handleStepFour(event) {
    event.preventDefault()
    this.setState({ ...this.state, savingStepButton: 'loading' })
    const cookies = new Cookies()
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'rooms/' + this.state.id, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'PUT',
      body: JSON.stringify({
        active: this.state.rooms[0].active,
        internal_name: this.state.rooms[0].internal_name,
        public_name: this.state.rooms[0].public_name,
        description: this.state.rooms[0].description,
        type_id: this.state.rooms[0].type_id,
        host_quantity: this.state.rooms[0].host_quantity,
        total_area: this.state.rooms[0].total_area,
        rooms_quantity: this.state.rooms[0].rooms_quantity,
        people_capacity: this.state.rooms[0].people_capacity,
        bathroom_quantity: this.state.rooms[0].bathroom_quantity,
        strengths: this.state.rooms[0].strengths,

        establishment_id: this.state.rooms[0].establishment_id,
        basic_price: this.state.rooms[0].basic_price,
        weekend_price: this.state.rooms[0].weekend_price,
        min_nigth: this.state.rooms[0].min_nigth,
        max_nigth: this.state.rooms[0].max_nigth,
        seven_days_discount: this.state.rooms[0].seven_days_discount,
        month_discount: this.state.rooms[0].month_discount,
        slug: this.state.rooms[0].slug,
        page_url: '/onboarding/hebergements/step-five?id=' + this.state.id,
        page: 20
      })
    })
    if (this.state.selectedCalendar == '1') {
      if (this.state.status_calendar == 'create') {
        this.state.dates_not_availabities.forEach((value, index) => {
          let date_parsed =
            new Date(value.date.replaceAll('-', '/')).getFullYear() +
            '/' +
            (
              '0' +
              (new Date(value.date.replaceAll('-', '/')).getMonth() + 1)
            ).slice(-2) +
            '/' +
            ('0' + new Date(value.date.replaceAll('-', '/')).getDate()).slice(
              -2
            )
          fetch(process.env.NEXT_PUBLIC_API_URL + 'availabilities', {
            body: JSON.stringify({
              room_id: this.state.id,
              date: date_parsed
            }),
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'POST'
          })
        })

        const res_delete_google_calendar = await fetch(
          process.env.NEXT_PUBLIC_API_URL +
            'delete-google-calendar/' +
            this.state.id,
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'DELETE'
          }
        )

        const status = await res_delete_google_calendar.status
        this.validateStatus(status)
      } else {
        this.state.dates_not_availabities.forEach((value, index) => {
          let date_parsed =
            new Date(value.date.replaceAll('-', '/')).getFullYear() +
            '/' +
            (
              '0' +
              (new Date(value.date.replaceAll('-', '/')).getMonth() + 1)
            ).slice(-2) +
            '/' +
            ('0' + new Date(value.date.replaceAll('-', '/')).getDate()).slice(
              -2
            )
          if (value.isChecked) {
            fetch(process.env.NEXT_PUBLIC_API_URL + 'availabilities', {
              body: JSON.stringify({
                room_id: this.state.id,
                date: date_parsed
              }),
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'POST'
            })
          } else {
            fetch(
              process.env.NEXT_PUBLIC_API_URL + 'availabilities/' + value.id,
              {
                body: JSON.stringify({
                  room_id: this.state.id,
                  date: date_parsed
                }),
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: 'Bearer ' + cookies.get('tk_user')
                },
                method: 'DELETE'
              }
            )
          }
        })

        const res_delete_google_calendar = await fetch(
          process.env.NEXT_PUBLIC_API_URL +
            'delete-google-calendar/' +
            this.state.id,
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'DELETE'
          }
        )

        const status = await res_delete_google_calendar.status
        this.validateStatus(status)
      }
    } else {
      if (this.state.google_calendar_authorization == true) {
        if (this.state.status_calendar == 'create') {
          const res_google = await fetch(
            process.env.NEXT_PUBLIC_API_URL + 'associate-calendar',
            {
              body: JSON.stringify({
                room_id: this.state.id,
                calendar_id: this.state.id_calendar
              }),
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'POST'
            }
          )

          const status = await res_google.status

          if (status == 200) {
            const res_delete = await fetch(
              process.env.NEXT_PUBLIC_API_URL +
                'delete-unavailabilities/' +
                this.state.id,
              {
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: 'Bearer ' + cookies.get('tk_user')
                },
                method: 'GET'
              }
            )

            const status = await res_delete.status
            this.validateStatus(status)
          }
        } else {
          if (this.state.availabilities.length > 0) {
            const res_google = await fetch(
              process.env.NEXT_PUBLIC_API_URL + 'associate-calendar',
              {
                body: JSON.stringify({
                  room_id: this.state.id,
                  calendar_id: this.state.id_calendar
                }),
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: 'Bearer ' + cookies.get('tk_user')
                },
                method: 'POST'
              }
            )

            const status = await res_google.status

            if (status == 200) {
              const res_delete = await fetch(
                process.env.NEXT_PUBLIC_API_URL +
                  'delete-unavailabilities/' +
                  this.state.id,
                {
                  headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + cookies.get('tk_user')
                  },
                  method: 'GET'
                }
              )

              const status = await res_delete.status
              this.validateStatus(status)
            }
          } else {
            const res_edit_calendar = await fetch(
              process.env.NEXT_PUBLIC_API_URL +
                'edit-calendar-id/' +
                this.state.id,
              {
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: 'Bearer ' + cookies.get('tk_user')
                },
                method: 'POST',
                body: JSON.stringify({
                  calendar_id: this.state.id_calendar
                })
              }
            )

            const status = await res_edit_calendar.status

            if (status == 200) {
              const res_delete = await fetch(
                process.env.NEXT_PUBLIC_API_URL +
                  'delete-unavailabilities/' +
                  this.state.id,
                {
                  headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + cookies.get('tk_user')
                  },
                  method: 'GET'
                }
              )

              const status = await res_delete.status
              this.validateStatus(status)
            }
          }
        }
      } else {
        notify(`Autorisation requise`, 'warning')
      }
    }
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  handleShowModal() {
    this.setState({
      ...this.state,
      showModalGoogle: !this.state.showModalGoogle
    })
  }

  validateStatus(status) {
    if (status == 200) {
      this.setState({ ...this.state, savingStepButton: 'saved' })
      notify(`Données sauvegardées avec succès`, 'success')
      Router.push('/onboarding/hebergements/step-five?id=' + this.state.id)
    } else {
      this.setState({ ...this.state, savingStepButton: 'error' })
      notify(`Une erreur s'est produite, réessayez plus tard.`, 'error')
    }
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={4} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div className="row background-white container-onboarding">
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="hebergement"
                  title="Hébergements"
                />
              </div>
              <div className="container-fluid container-onboarding">
                <Description />
                {this.state.isShowContent ? (
                  <form className="form" onSubmit={this.handleStepFour}>
                    <div className="row justify-content-center">
                      {this.state.selectedCalendar == '1' && (
                        <>
                          <Legend />
                          <div className="col-12"></div>
                          {this.state.isShowCalendar ? (
                            this.state.array_calendars.map((value, index) => {
                              return (
                                <div
                                  className="col-12 col-xl-6 card-white__calendar onBoardingHebergements__calendar"
                                  key={index}
                                >
                                  <div className="row">
                                    <p className="col-6">
                                      {setMonthName(value.date.getMonth() + 1)}{' '}
                                      {value.date.getFullYear()}
                                    </p>
                                    <a
                                      className="col-6 text-right link"
                                      href="#"
                                      style={{ fontSize: '10px' }}
                                      onClick={() => {
                                        this.handleChangeCalendarAvailabilitie(
                                          index,
                                          value.status_block
                                        )
                                      }}
                                    >
                                      {value.status_block == false
                                        ? 'Bloquer ce mois-ci'
                                        : 'Débloquer ce mois-ci'}
                                    </a>
                                  </div>
                                  <Calendar
                                    onClickDay={this.handleChangeDateOnClick}
                                    value={value.date}
                                    minDate={value.min_date}
                                    showNavigation={false}
                                    showNeighboringMonth={false}
                                    className="calendar-onboarding"
                                    tileClassName={({ date, view }) => {
                                      if (
                                        view === 'month' && // Block day tiles only
                                        this.state.dates_not_availabities.some(
                                          (disabledDate) => {
                                            const arrDate =
                                              disabledDate.date.split('-')
                                            const disabledDateFormatted =
                                              disabledDate.date
                                                .split('-')
                                                .join('/')
                                            return (
                                              date.getFullYear() ===
                                                new Date(
                                                  disabledDateFormatted
                                                ).getFullYear() &&
                                              date.getMonth() ===
                                                new Date(
                                                  disabledDateFormatted
                                                ).getMonth() &&
                                              date.getDate() ===
                                                parseInt(arrDate[2]) &&
                                              disabledDate.isChecked
                                            )
                                          }
                                        )
                                      ) {
                                        return 'date date-disabled'
                                      } else {
                                        return 'date'
                                      }
                                    }}
                                  />
                                </div>
                              )
                            })
                          ) : (
                            <Loader />
                          )}
                          <div className="col-12 d-flex justify-content-center onBoardingHebergements__addCalendar">
                            <span onClick={this.handleAddMonths}>
                              Charger plus de mois
                            </span>
                          </div>
                        </>
                      )}
                      {this.state.selectedCalendar == '2' && (
                        <>
                          <div className="card-white">
                            <div className="form-group">
                              <h4 htmlFor="address_stablishment">Info</h4>
                              <button
                                className="button"
                                type="button"
                                onClick={this.handleShowModal}
                              >
                                More info
                              </button>
                            </div>
                          </div>
                          {this.state.showModalGoogle && (
                            <PopupInfoGoogle
                              closeDialog={this.handleShowModal}
                            />
                          )}
                          <div className="card-white">
                            <div className="form-group">
                              <h4 htmlFor="address_stablishment">
                                ID calendar *
                              </h4>
                              <input
                                type="text"
                                className="form-control"
                                id="address_stablishment"
                                name="address_stablishment"
                                aria-describedby="emailHelp"
                                placeholder=""
                                required
                                defaultValue={this.state.id_calendar}
                                valuealue={this.state.id_calendar}
                                onChange={this.handleChangeIdCalendar}
                              />
                            </div>
                          </div>
                          {this.state.id_calendar !== '' && (
                            <div className="card-white">
                              <div className="form-group">
                                <div className="form-group">
                                  <h4 htmlFor="address_stablishment">
                                    Authorization
                                  </h4>
                                  <button
                                    className="button"
                                    onClick={this.handleTestAuthGoogleCalendar}
                                    type="button"
                                  >
                                    Test authorization
                                  </button>
                                  <FontAwesomeIcon
                                    icon="info-circle"
                                    size="5x"
                                    data-toggle="tooltip"
                                    data-placement="right"
                                    title="If you already check Google Calendar ID, can skip this step, but if you change Google Calendar ID you need validate authorization again."
                                    className="icon-check"
                                  />
                                </div>
                              </div>
                            </div>
                          )}
                        </>
                      )}
                      <div className="row mt-5 buttons-onboarding p-0">
                        <GoBackBtn
                          link={`/onboarding/hebergements/step-three?id=${this.state.id}`}
                        />
                        <SaveBtn statusBtn={this.state.savingStepButton} />
                      </div>
                    </div>
                    {this.state.showArrowUp && (
                      <div
                        className="onboarding__arrow-up"
                        onClick={this.handleArrowClick}
                      >
                        <ChevronUp width="35" height="35" />
                      </div>
                    )}
                  </form>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}
export default connect(mapStateToProps, null)(withGreenScore(StepFour))
