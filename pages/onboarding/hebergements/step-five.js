import React from 'react'

import Router from 'next/router'
import Link from 'next/link'

import Cookies from 'universal-cookie'
import { toast } from 'react-toastify'
import ProgressBar from '../../../components/progressbar'
import Container from '../../../components/container'
import Steper from '../../../components/steper'
import { Loader } from 'components/'
import withGreenScore from 'HOC/withGreenScore'
import { connect } from 'react-redux'
import { notify } from 'helpers/notificationClassHelper'
import {
  convertPrices,
  inputOnlyNumberComma,
  validatePrices
} from 'helpers/utilsHelper'
import { updateStep5 } from 'services/hebergementsService'
import RadioButtonSection from '@components/Onboarding/rooms/RadioButtonSection'

class StepFive extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      step: 5,
      first_text: 'Prix',
      last_text: 'Photos',
      id: '',
      min_night: 1,
      max_night: 180,
      room: [],
      rooms: [],
      isEnableButton: true,
      isShowSidebar: '',
      showSidebar: true,
      isShowContent: false,
      savingStepButton: 'init',
      accept_coupon: '0'
    }

    this.handleStepFive = this.handleStepFive.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.changeCheckValue = this.changeCheckValue.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    const queryString = window.location.search
    const urlParams = new URLSearchParams(queryString)
    this.setState({ ...this.state, id: urlParams.get('id') }, async () => {
      //Get room availabilities
      fetch(process.env.NEXT_PUBLIC_API_URL + 'rooms/' + this.state.id, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      })
        .then((res) => {
          return res.json()
        })
        .then((room_response) => {
          const priceBaseResponse =
            room_response.length > 0 && room_response[0].basic_price
              ? room_response[0].basic_price.toString()
              : ''
          const priceWeekendResponse =
            room_response.length > 0 && room_response[0].weekend_price
              ? room_response[0].weekend_price.toString()
              : ''

          this.setState({
            ...this.state,
            room: room_response,
            isShowContent: true,
            price_base: priceBaseResponse.replace('.', ','),
            price_week_end: priceWeekendResponse.replace('.', ',')
          })
          if (this.state.room[0].min_nigth !== null) {
            this.setState({
              ...this.state,
              min_night: this.state.room[0].min_nigth,
              max_night: this.state.room[0].max_nigth
            })
          }
        })
    })
  }

  async handleStepFive(event) {
    event.preventDefault()
    this.setState({ ...this.state, savingStepButton: 'loading' })

    const valid = validatePrices(
      event.target.price_base.value,
      event.target.price_week_end.value
    )
    let prices = {
      priceBaseFormatted: null,
      priceWeekendFormatted: null
    }

    if (!valid) {
      this.setState({ ...this.state, savingStepButton: 'error' })
      notify(
        `Corrigez les prix. Vous ne pouvez pas écrire plus d'une virgule.`,
        'error'
      )
      return
    } else {
      if (
        event.target.price_week_end.value === '' ||
        event.target.price_week_end.value === null
      ) {
        prices.priceBaseFormatted = convertPrices(
          event.target.price_base.value,
          event.target.price_week_end.value
        )
      } else {
        prices = convertPrices(
          event.target.price_base.value,
          event.target.price_week_end.value
        )
      }
    }

    if (this.state.min_night > 0) {
      const cookies = new Cookies()
      const body = {
        basic_price: prices.priceBaseFormatted,
        weekend_price: prices.priceWeekendFormatted,
        seven_days_discount: event.target.seven_days_discount.value,
        month_discount: event.target.month_discount.value,
        min_nigth: this.state.min_night,
        max_nigth: this.state.max_night,
        accept_coupon: this.state.accept_coupon
      }
      const { code, data } = await updateStep5(this.state.room[0].id, body)

      if (code === 400) {
        this.setState({ ...this.state, savingStepButton: 'init' })
        const { basic_price } = data
        if (typeof basic_price !== null) {
          notify(`veuillez vérifier que le prix de base est correct.`, 'error')
        }
        return
      }

      this.setState({ ...this.state, savingStepButton: 'saved' })
      notify(`Données sauvegardées avec succès`, 'success')
      Router.push('/onboarding/hebergements/step-six?id=' + this.state.id)
    } else {
      this.setState({ ...this.state, savingStepButton: 'error' })
      notify(`Les nuits minimales ne peuvent pas être 0`, 'error')
    }
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  handleInputChange(e) {
    this.setState({ ...this.state, savingStepButton: 'init' })
    const isNumberWithComma = inputOnlyNumberComma(e.target.value)

    if (isNumberWithComma) {
      this.setState({ ...this.state, [e.target.name]: e.target.value })
    } else {
      this.setState({
        ...this.state,
        [e.target.name]: e.target.value.slice(0, -1)
      })
    }
  }

  changeCheckValue() {
    if (this.state.accept_coupon === '0' || !this.state.accept_coupon) {
      this.setState({ ...this.state, accept_coupon: '1' })
    } else {
      this.setState({ ...this.state, accept_coupon: '0' })
    }
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={4} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div className="row background-white container-onboarding">
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="hebergement"
                  title="Hébergements"
                />
              </div>
              <div className="container-fluid container-onboarding">
                <h2>Tarif par nuit</h2>
                <p className="text-wrap text-justify">
                  Tarif payé par les voyageurs (taxes, commission et frais
                  compris). Vous pourrez toujours modifier ces prix par la
                  suite, même après avoir publié l’annonce.
                </p>
                {this.state.isShowContent ? (
                  <form className="form" onSubmit={this.handleStepFive}>
                    <div className="card-white__responsive">
                      <h3 className="card-white__responsive-title">
                        Prix base *
                      </h3>
                      <p>
                        Le prix par défaut d’une nuitée dans votre hébergement.
                      </p>
                      <div className="row">
                        <div className="input-group mb-2 input-card col-12 col-xl-6">
                          <input
                            onChange={this.handleInputChange}
                            onKeyPress={(e) => {
                              e.key === 'Enter' && e.preventDefault()
                            }}
                            type="text"
                            onWheel={(e) => e.target.blur()}
                            className="form-control"
                            id="price_base"
                            name="price_base"
                            placeholder=""
                            value={this.state.price_base}
                            defaultValue={this.state.price_base}
                            min="0"
                            required
                          />
                          <div className="input-group-prepend">
                            <div className="input-group-text">/ Nuit</div>
                          </div>
                        </div>
                      </div>
                      <hr />
                      <div className="card-advice">
                        <h4>Conseil :</h4>
                        <p>
                          Indiquez votre prix “bas” (souvent un jour de semaine
                          en basse saison).{' '}
                        </p>
                      </div>
                    </div>
                    <div className="card-white__responsive">
                      <h3 className="card-white__responsive-title">
                        Prix week-end <span>Optionnel</span>
                      </h3>
                      <p>
                        Le prix par défaut d’une nuitée les vendredis soir et
                        samedis soir.
                      </p>
                      <div className="row">
                        <div className="input-group mb-2 input-card col-12 col-xl-6">
                          <input
                            onChange={this.handleInputChange}
                            onKeyPress={(e) => {
                              e.key === 'Enter' && e.preventDefault()
                            }}
                            type="text"
                            onWheel={(e) => e.target.blur()}
                            className="form-control"
                            id="price_week_end"
                            name="price_week_end"
                            placeholder=""
                            value={this.state.price_week_end}
                            defaultValue={
                              this.state.room.length > 0
                                ? this.state.room[0].weekend_price
                                : ''
                            }
                            min="0"
                          />
                          <div className="input-group-prepend">
                            <div className="input-group-text">/ Nuit</div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <RadioButtonSection
                      title="Carte cadeau"
                      description="Vous pouvez activer la carte cadeau pour cette hebergement"
                      updateState={this.changeCheckValue}
                      acceptCoupon={this.state.accept_coupon}
                    />

                    <div className="card-white__responsive">
                      <h3 className="card-white__responsive-title">
                        Combien de temps les voyageurs peuvent-ils rester ?
                      </h3>
                      <p>
                        Définissez le nombre minimum et maximum de nuits
                        réservables lors d’une unique réservation. (Vous devez
                        ajouter des nuits maximum pour pouvoir ajouter des nuits
                        minimum)
                      </p>
                      <div className="row col-12">
                        <div className="col-12 card-button-nights mt-3">
                          <div className="row">
                            <div className="col-8 mt-3 input-night">
                              <p>
                                <input
                                  onChange={this.handleInputChange}
                                  onKeyPress={(e) => {
                                    e.key === 'Enter' && e.preventDefault()
                                  }}
                                  value={this.state.min_night}
                                  defaultValue={this.state.min_night}
                                  type="number"
                                  onWheel={(e) => e.target.blur()}
                                  onChange={(e) => {
                                    this.setState({
                                      ...this.state,
                                      min_night: e.target.value
                                    })
                                  }}
                                />{' '}
                                {this.state.min_night > 1 ? 'Nuits' : 'Nuit'}{' '}
                                minimum
                              </p>
                            </div>
                            <div className="col-4 buttons-night">
                              <button
                                className="button-left"
                                type="button"
                                name="nights_min"
                                onClick={(e) => {
                                  let input = this.state.min_night
                                  if (input > 1) input = input -= 1
                                  this.setState({
                                    ...this.state,
                                    min_night: input
                                  })
                                }}
                              >
                                -
                              </button>
                              <button
                                className="button-right"
                                type="button"
                                name="nights_min"
                                onClick={(e) => {
                                  let input = this.state.min_night
                                  if (
                                    this.state.max_night > this.state.min_night
                                  )
                                    input = input += 1
                                  this.setState({
                                    ...this.state,
                                    min_night: input
                                  })
                                }}
                              >
                                +
                              </button>
                            </div>
                          </div>
                        </div>
                        <div className="col-12 card-button-nights mt-3">
                          <div className="row">
                            <div className="col-8 mt-3 input-night">
                              <p>
                                <input
                                  onChange={this.handleInputChange}
                                  onKeyPress={(e) => {
                                    e.key === 'Enter' && e.preventDefault()
                                  }}
                                  value={this.state.max_night}
                                  type="number"
                                  onWheel={(e) => e.target.blur()}
                                  onChange={(e) => {
                                    this.setState({
                                      ...this.state,
                                      max_night: e.target.value
                                    })
                                  }}
                                />
                                {this.state.max_night > 1 ? 'Nuits' : 'Nuit'}{' '}
                                maximum
                              </p>
                            </div>
                            <div className="col-4 buttons-night">
                              <button
                                className="button-left"
                                type="button"
                                name="nights_max"
                                onClick={(e) => {
                                  let input = this.state.max_night
                                  if (input > 0) input = input -= 1
                                  this.setState({
                                    ...this.state,
                                    max_night: input
                                  })
                                }}
                              >
                                -
                              </button>
                              <button
                                className="button-right"
                                type="button"
                                name="nights_max"
                                onClick={(e) => {
                                  let input = this.state.max_night
                                  input = input += 1
                                  this.setState({
                                    ...this.state,
                                    max_night: input
                                  })
                                }}
                              >
                                +
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr />
                      <div className="card-advice">
                        <h4>Conseil :</h4>
                        <p>
                          Avec l’avènement du travail à distance, certains
                          voyageurs vont privilégier des lieux où ils pourront
                          rester plusieurs semaines.{' '}
                        </p>
                      </div>
                    </div>
                    <div className="card-white__responsive">
                      <h3 className="card-white__responsive-title">
                        Réductions pour longue durée <span>Optionnel</span>
                      </h3>
                      <p style={{ marginBottom: 24 + 'px' }}>
                        Incitez vos voyageurs à rester plus longtemps en
                        proposant des tarifs nuitées dégressifs.
                      </p>
                      <div className="row">
                        <div className="col-12 col-xxl-6 col-xl-6 ">
                          <div className="form-group">
                            <h4 htmlFor="seven_days_discount">
                              Réduction à la semaine
                            </h4>
                            <input
                              onChange={this.handleInputChange}
                              onKeyPress={(e) => {
                                e.key === 'Enter' && e.preventDefault()
                              }}
                              type="number"
                              onWheel={(e) => e.target.blur()}
                              className="form-control"
                              id="seven_days_discount"
                              name="seven_days_discount"
                              aria-describedby="seven_days_discount_help"
                              placeholder=""
                              defaultValue={
                                this.state.room.length > 0
                                  ? this.state.room[0].seven_days_discount
                                  : ''
                              }
                              min="0"
                            />
                            <small
                              id="seven_days_discount_help"
                              className="form-text text-muted"
                            >
                              Conseil : 21%
                            </small>
                          </div>
                        </div>
                        <div className="col-12 col-xxl-6 col-xl-6 ">
                          <div className="form-group">
                            <h4 htmlFor="month_discount">Réduction au mois</h4>
                            <input
                              onChange={this.handleInputChange}
                              onKeyPress={(e) => {
                                e.key === 'Enter' && e.preventDefault()
                              }}
                              type="number"
                              onWheel={(e) => e.target.blur()}
                              className="form-control"
                              id="month_discount"
                              name="month_discount"
                              aria-describedby="month_discount_help"
                              placeholder=""
                              defaultValue={
                                this.state.room.length > 0
                                  ? this.state.room[0].month_discount
                                  : ''
                              }
                              min="0"
                            />
                            <small
                              id="month_discount_help"
                              className="form-text text-muted"
                            >
                              Conseil : 45%
                            </small>
                          </div>
                        </div>
                      </div>
                      <hr />
                      <div className="card-advice">
                        <h4>Conseil :</h4>
                        <p>
                          La réduction à la semaine se déclenche dès la 8ème
                          nuitée et jusqu’à la 27ème nuitée.
                        </p>
                        <p>
                          La réduction au mois se déclenche dès la 28ème nuitée.{' '}
                        </p>
                      </div>
                    </div>
                    <div className="row mt-5 buttons-onboarding">
                      <div className="col-12 col-xl-6 button-back-onboarding">
                        <a
                          className="button button-outline-black button-long"
                          type="button"
                          href={
                            '/onboarding/hebergements/step-four?id=' +
                            this.state.id
                          }
                        >
                          Retour
                        </a>
                      </div>
                      <div className="col-12 col-xl-6 justify-content-end d-flex">
                        <button
                          type="submit"
                          className="button button-long button--save-onboarding"
                          disabled={
                            this.state.savingStepButton !== 'loading' &&
                            this.state.savingStepButton !== 'saved'
                              ? false
                              : true
                          }
                        >
                          {this.state.savingStepButton === 'loading'
                            ? 'En cours...'
                            : this.state.savingStepButton === 'saved'
                            ? 'Enregistré'
                            : 'Sauvegarder et continuer'}
                        </button>
                      </div>
                    </div>
                    <br />
                  </form>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}
// const mapDispatchToProps = {
// }
export default connect(mapStateToProps, null)(withGreenScore(StepFive))
