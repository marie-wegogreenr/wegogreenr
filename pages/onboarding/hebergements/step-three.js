import React from 'react'
import Image from 'next/image'
import Router from 'next/router'
import Link from 'next/link'

import Cookies from 'universal-cookie'

import ProgressBar from '../../../components/progressbar'
import Container from '../../../components/container'
import Steper from '../../../components/steper'
import { Loader } from 'components/'
import withGreenScore from 'HOC/withGreenScore'
import { connect } from 'react-redux'
import { notify } from 'helpers/notificationClassHelper'

class StepThree extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      step: 3,
      first_text: 'Équipements',
      last_text: 'Calendrier et dispolobilités',
      room_equipments_type_categories: [],
      rooms_equipments_types: [],
      rooms_equipments: [],
      rooms: [],
      status_equipment: 'create',
      id: '',
      isEnableButton: true,
      isShowSidebar: '',
      showSidebar: true,
      isShowContent: false,
      savingStepButton: 'init'
    }

    this.handleChekedPosition = this.handleChekedPosition.bind(this)
    this.handleStepThree = this.handleStepThree.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()

    const queryString = window.location.search
    const urlParams = new URLSearchParams(queryString)
    this.setState({ ...this.state, id: urlParams.get('id') })

    //Get room equipment types
    const res_rooms_equipments_types = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'room-equipment-types',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_rooms_equipments_types = await res_rooms_equipments_types.json()
    this.setState({
      ...this.state,
      rooms_equipments_types: json_rooms_equipments_types
    })

    //Get room equipment type categories
    const res_room_equipments_type_categories = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'room-equipment-type-categories',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_room_equipments_type_categories =
      await res_room_equipments_type_categories.json()
    this.setState({
      ...this.state,
      room_equipments_type_categories: json_room_equipments_type_categories
    })

    //Get room equipment
    const res_room_equipments = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'room-equipments/' + this.state.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_room_equipments = await res_room_equipments.json()
    this.setState({ ...this.state, room_equipments: json_room_equipments })

    //Get rooms
    const res_room = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'rooms/' + this.state.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_rooms = await res_room.json()
    this.setState({ ...this.state, rooms: json_rooms })

    if (this.state.room_equipments.length > 0) {
      this.setState({ ...this.state, status_equipment: 'update' })
    }

    let rooms_equipments_types = this.state.rooms_equipments_types

    rooms_equipments_types.forEach((room_equipment_type) => {
      room_equipment_type.isChecked = false
    })

    this.setState({
      ...this.state,
      rooms_equipments_types: rooms_equipments_types
    })

    let rooms_equipments_types_2 = this.state.rooms_equipments_types

    this.state.room_equipments.forEach((room_equipment) => {
      rooms_equipments_types_2.forEach((room_equipment_type) => {
        if (room_equipment_type.id == room_equipment.type_id) {
          room_equipment_type.isChecked = true
          room_equipment_type.room_equipment_id = room_equipment.id
        }
      })
    })

    this.setState({
      ...this.state,
      rooms_equipments_types: rooms_equipments_types_2,
      isShowContent: true
    })
  }

  async handleChekedPosition(event) {
    let rooms_equipments_types = this.state.rooms_equipments_types
    rooms_equipments_types.forEach((condition) => {
      if (condition.id === parseInt(event.target.value)) {
        condition.isChecked = event.target.checked
      }
    })
    this.setState({ rooms_equipments_types: rooms_equipments_types })
  }

  //POST save information establishment
  async handleStepThree(event) {
    event.preventDefault()
    this.setState({ ...this.state, savingStepButton: 'loading' })
    const cookies = new Cookies()
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'rooms/' + this.state.id, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'PUT',
      body: JSON.stringify({
        active: this.state.rooms[0].active,
        internal_name: this.state.rooms[0].internal_name,
        public_name: this.state.rooms[0].public_name,
        description: this.state.rooms[0].description,
        type_id: this.state.rooms[0].type_id,
        host_quantity: this.state.rooms[0].host_quantity,
        total_area: this.state.rooms[0].total_area,
        rooms_quantity: this.state.rooms[0].rooms_quantity,
        people_capacity: this.state.rooms[0].people_capacity,
        bathroom_quantity: this.state.rooms[0].bathroom_quantity,
        strengths: this.state.rooms[0].strengths,

        establishment_id: this.state.rooms[0].establishment_id,
        basic_price: this.state.rooms[0].basic_price,
        weekend_price: this.state.rooms[0].weekend_price,
        min_nigth: this.state.rooms[0].min_nigth,
        max_nigth: this.state.rooms[0].max_nigth,
        seven_days_discount: this.state.rooms[0].seven_days_discount,
        month_discount: this.state.rooms[0].month_discount,
        slug: this.state.rooms[0].slug,
        page_url: '/onboarding/hebergements/step-four?id=' + this.state.id,
        page: 19
      })
    })

    if (this.state.status_equipment == 'create') {
      this.state.rooms_equipments_types.forEach((room_equipment_type) => {
        if (room_equipment_type.isChecked == true) {
          fetch(process.env.NEXT_PUBLIC_API_URL + 'room-equipments', {
            body: JSON.stringify({
              room_id: this.state.id,
              type_id: room_equipment_type.id
            }),
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'POST'
          })
        }
      })
      this.setState({ ...this.state, savingStepButton: 'saved' })
      notify(`Données sauvegardées avec succès`, 'success')
      Router.push('/onboarding/hebergements/step-four?id=' + this.state.id)
    } else {
      this.state.rooms_equipments_types.forEach((room_equipment_type) => {
        if (room_equipment_type.isChecked == true) {
          fetch(process.env.NEXT_PUBLIC_API_URL + 'room-equipments', {
            body: JSON.stringify({
              room_id: this.state.id,
              type_id: room_equipment_type.id
            }),
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'POST'
          })
        } else {
          if (room_equipment_type.room_equipment_id !== undefined) {
            fetch(
              process.env.NEXT_PUBLIC_API_URL +
                'room-equipments/' +
                room_equipment_type.room_equipment_id,
              {
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: 'Bearer ' + cookies.get('tk_user')
                },
                method: 'DELETE'
              }
            )
          }
        }
      })
      this.setState({ ...this.state, savingStepButton: 'saved' })
      notify(`Données sauvegardées avec succès`, 'success')
      Router.push('/onboarding/hebergements/step-four?id=' + this.state.id)
    }
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  handleInputChange(e) {
    this.setState({ ...this.state, savingStepButton: 'init' })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={4} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div className="row background-white container-onboarding">
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="hebergement"
                  title="Hébergements"
                />
              </div>
              <div className="container-fluid container-onboarding">
                <h2>Équipements</h2>
                {this.state.isShowContent ? (
                  <form className="form" onSubmit={this.handleStepThree}>
                    {this.state.room_equipments_type_categories.map(
                      (value_room_equipments_type_category, index) => {
                        return (
                          <div key={index} className="card-white">
                            <h3 style={{ marginBottom: 25 + 'px' }}>
                              {value_room_equipments_type_category.name}
                            </h3>
                            <div className="row">
                              {this.state.rooms_equipments_types.map(
                                (value_room, index) => {
                                  if (
                                    value_room_equipments_type_category.id ==
                                    value_room.category_id
                                  ) {
                                    return (
                                      <div
                                        className="col-12 col-sm-6 col-xl-4"
                                        key={index}
                                      >
                                        <div className="form-check">
                                          <input
                                            onChange={this.handleInputChange}
                                            className="form-check-input input-check-blue-dark"
                                            type="checkbox"
                                            value={value_room.id}
                                            id={value_room.id}
                                            onClick={this.handleChekedPosition}
                                            checked={value_room.isChecked}
                                          />
                                          <label
                                            className="form-check-label"
                                            htmlFor={value_room.id}
                                          >
                                            {value_room.name}
                                          </label>
                                        </div>
                                      </div>
                                    )
                                  }
                                }
                              )}
                            </div>
                          </div>
                        )
                      }
                    )}
                    <div className="row mt-5 buttons-onboarding">
                      <div className="col-12 col-xl-6 button-back-onboarding">
                        <a
                          className="button button-outline-black button-long"
                          type="button"
                          href={
                            '/onboarding/hebergements/step-two?id=' +
                            this.state.id
                          }
                        >
                          Retour
                        </a>
                      </div>
                      <div className="col-12 col-xl-6 justify-content-end d-flex">
                        <button
                          type="submit"
                          className="button button-long button--save-onboarding"
                          disabled={
                            this.state.savingStepButton !== 'loading' &&
                            this.state.savingStepButton !== 'saved'
                              ? false
                              : true
                          }
                        >
                          {this.state.savingStepButton === 'loading'
                            ? 'En cours...'
                            : this.state.savingStepButton === 'saved'
                            ? 'Enregistré'
                            : 'Sauvegarder et continuer'}
                        </button>
                      </div>
                    </div>
                    <br />
                  </form>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}
// const mapDispatchToProps = {
// }
export default connect(mapStateToProps, null)(withGreenScore(StepThree))
