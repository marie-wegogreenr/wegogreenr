import React from 'react'
import Image from 'next/image'
import Router from 'next/router'
import Link from 'next/link'

import Cookies from 'universal-cookie'

import ProgressBar from '../../../components/progressbar'
import Container from '../../../components/container'
import Steper from '../../../components/steper'
import { Loader } from 'components/'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMinus, faPlus } from '@fortawesome/free-solid-svg-icons'
import withGreenScore from 'HOC/withGreenScore'
import { connect } from 'react-redux'
import { notify } from 'helpers/notificationClassHelper'

class StepTwo extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      step: 2,
      first_text: 'Lits et chambres',
      last_text: 'Équipements',
      bed_types: [],
      quantityBathroom: 0,
      quantityTravelers: 0,
      quantityRooms: 0,
      id: null,
      rooms: [],
      room_beds: [],
      status_room_beds: 'create',
      isEnableButton: true,
      isShowSidebar: '',
      showSidebar: true,
      isShowContent: false,
      savingStepButton: 'init',
      strengths: ''
    }

    this.handleChangeQuantity = this.handleChangeQuantity.bind(this)
    this.handleStepTwo = this.handleStepTwo.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleStrengthsChange = this.handleStrengthsChange.bind(this)

    library.add(faMinus, faPlus)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    const queryString = window.location.search
    const urlParams = new URLSearchParams(queryString)
    this.setState({ ...this.state, id: urlParams.get('id') })

    //Get bed types
    const res_bed_types = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'bed_types',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_bed_types = await res_bed_types.json()
    this.setState({ ...this.state, bed_types: json_bed_types })

    //Get room beds
    const res_room_bed = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'room-beds/' + this.state.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_room_beds = await res_room_bed.json()
    this.setState({ ...this.state, room_beds: json_room_beds })

    //Get rooms
    const res_room = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'rooms/' + this.state.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_rooms = await res_room.json()
    this.setState({ ...this.state, rooms: json_rooms })

    if (this.state.rooms.length > 0 && this.state.rooms[0].rooms_quantity) {
      this.setState({
        ...this.state,
        quantityRooms: this.state.rooms[0].rooms_quantity
      })
    }

    if (this.state.rooms.length > 0 && this.state.rooms[0].bathroom_quantity) {
      this.setState({
        ...this.state,
        quantityBathroom: this.state.rooms[0].bathroom_quantity
      })
    }

    if (this.state.rooms.length > 0 && this.state.rooms[0].people_capacity) {
      this.setState({
        ...this.state,
        quantityTravelers: this.state.rooms[0].people_capacity
      })
    }

    let bed_types = this.state.bed_types

    bed_types.forEach((bed) => {
      bed.quantity = 0
    })

    this.setState({ ...this.state, bed_types: bed_types })

    let bed_types_2 = this.state.bed_types

    this.state.room_beds.forEach((room_bed) => {
      bed_types_2.forEach((bed_type) => {
        if (bed_type.id == room_bed.bed_type_id) {
          bed_type.quantity = room_bed.quantity
          bed_type.room_bed_id = room_bed.id
        }
      })
    })

    this.setState({ ...this.state, bed_types: bed_types, isShowContent: true })

    if (this.state.room_beds.length > 0) {
      this.setState({ ...this.state, status_room_beds: 'update' })
    }

    if (this.state.rooms[0].strengths) {
      this.setState({ ...this.state, strengths: this.state.rooms[0].strengths })
    }
  }

  async handleChangeQuantity(event) {
    event.preventDefault()
    let bed_types = this.state.bed_types
    if (event.target.id == 'plus') {
      bed_types.forEach((bed) => {
        if (bed.id == parseInt(event.target.name)) {
          //if (bed.quantity < bed.capacity) {
          bed.quantity += 1
          //}
        }
      })
    } else {
      bed_types.forEach((bed) => {
        if (bed.id == parseInt(event.target.name)) {
          if (bed.quantity > 0) {
            bed.quantity -= 1
          }
        }
      })
    }
    this.setState({ ...this.state, bed_types: bed_types })
  }

  async handleStepTwo(event) {
    event.preventDefault()

    if (this.state.strengths.length > 255) {
      notify(
        'La longueur maximale des points forts est de 255 caractères',
        'error'
      )
      return
    }

    this.setState({ ...this.state, savingStepButton: 'loading' })
    const cookies = new Cookies()
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'rooms/' + this.state.id, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'PUT',
      body: JSON.stringify({
        active: this.state.rooms[0].active,
        internal_name: this.state.rooms[0].internal_name,
        public_name: this.state.rooms[0].public_name,
        description: this.state.rooms[0].description,
        type_id: this.state.rooms[0].type_id,
        host_quantity: this.state.rooms[0].host_quantity,
        total_area: this.state.rooms[0].total_area,
        rooms_quantity: this.state.quantityRooms,
        people_capacity: this.state.quantityTravelers,
        bathroom_quantity: this.state.quantityBathroom,
        strengths: this.state.strengths,

        establishment_id: this.state.rooms[0].establishment_id,
        basic_price: this.state.rooms[0].basic_price,
        weekend_price: this.state.rooms[0].weekend_price,
        min_nigth: this.state.rooms[0].min_nigth,
        max_nigth: this.state.rooms[0].max_nigth,
        seven_days_discount: this.state.rooms[0].seven_days_discount,
        month_discount: this.state.rooms[0].month_discount,
        slug: this.state.rooms[0].slug,
        page_url: '/onboarding/hebergements/step-three?id=' + this.state.id,
        page: 18
      })
    })
      .then(() => {
        this.state.bed_types.forEach((bed) => {
          if (this.state.status_room_beds == 'create') {
            fetch(process.env.NEXT_PUBLIC_API_URL + 'room-beds', {
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'POST',
              body: JSON.stringify({
                room_id: this.state.id,
                bed_type_id: bed.id,
                quantity: bed.quantity
              })
            })
          } else {
            fetch(
              process.env.NEXT_PUBLIC_API_URL + 'room-beds/' + bed.room_bed_id,
              {
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: 'Bearer ' + cookies.get('tk_user')
                },
                method: 'PUT',
                body: JSON.stringify({
                  room_id: this.state.id,
                  bed_type_id: bed.id,
                  quantity: bed.quantity
                })
              }
            )
          }
        })
      })
      .then(() => {
        this.setState({ ...this.state, savingStepButton: 'saved' })
        notify(`Données sauvegardées avec succès`, 'success')
        Router.push('/onboarding/hebergements/step-three?id=' + this.state.id)
      })
      .catch((error) => {
        this.setState({ ...this.state, savingStepButton: 'error' })
        notify(`Une erreur s'est produite, réessayez plus tard.`, 'error')
      })
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  handleInputChange() {
    this.setState({ ...this.state, savingStepButton: 'init' })
  }

  handleStrengthsChange(e) {
    this.setState({ ...this.state, strengths: e.target.value })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={4} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div className="row background-white container-onboarding">
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="hebergement"
                  title="Hébergements"
                />
              </div>
              <div className="container-fluid container-onboarding">
                <h2>Lits et chambres</h2>
                {this.state.isShowContent ? (
                  <form className="form" onSubmit={this.handleStepTwo}>
                    <div className="card-white">
                      <h3 className="card-white__responsive-title">
                        Nombre de chambres
                      </h3>
                      <div className="card-background-gray-beds mt-5">
                        <div className="row ">
                          <div className="col-12 col-sm-6 col-md-12 col-lg-6 col-xl-8 mb-3 mb-sm-0 mb-md-3 mb-lg-0">
                            <h4 className="card-white__responsive-subtitle">
                              Chambres
                            </h4>
                          </div>
                          <div className="col-12 col-sm-6 col-md-12 col-lg-6 col-xl-4 container-buttons">
                            <div className="row buttons m-0 p-0 align-items-center m-md-0">
                              <div className="col-4 col-sm-4 col-md-4 col-xl-4 button-min-max p-0">
                                <button
                                  className="button button-circle-minus"
                                  type="button"
                                  name="rooms"
                                  onClick={(e) => {
                                    let input = this.state.quantityRooms
                                    if (input > 0) input = input -= 1
                                    this.setState({
                                      ...this.state,
                                      quantityRooms: input
                                    })
                                  }}
                                >
                                  -
                                </button>
                              </div>
                              <div className="form-control col-2 col-sm-2 col-md-2 col-xl-2 no-border text-center input-min-max px-0 py-2">
                                <p>{this.state.quantityRooms}</p>
                              </div>
                              <div className="col-4 col-sm-4 col-md-4 col-xl-4 button-min-max p-0">
                                <button
                                  className="button button-circle"
                                  name="rooms"
                                  onClick={(e) => {
                                    let input = this.state.quantityRooms
                                    input = input += 1
                                    this.setState({
                                      ...this.state,
                                      quantityRooms: input
                                    })
                                  }}
                                  type="button"
                                >
                                  +
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card-white">
                      <h3 className="card-white__responsive-title">
                        Nombre de voyageurs
                      </h3>
                      <div className="card-background-gray-beds mt-5">
                        <div className="row">
                          <div className="col-12 col-sm-6 col-md-12 col-lg-6 col-xl-8 mb-3 mb-sm-0 mb-md-3 mb-lg-0">
                            <h4 className="card-white__responsive-subtitle">
                              Voyageurs
                            </h4>
                          </div>
                          <div className="col-12 col-sm-6 col-md-12 col-lg-6 col-xl-4 container-buttons">
                            <div className="row buttons m-0 p-0 align-items-center m-md-0">
                              <div className="col-4 col-sm-4 col-md-4 col-xl-4 button-min-max p-0">
                                <button
                                  className="button button-circle-minus"
                                  type="button"
                                  name="travelers"
                                  onClick={(e) => {
                                    let input = this.state.quantityTravelers
                                    if (input > 0) input = input -= 1
                                    this.setState({
                                      ...this.state,
                                      quantityTravelers: input
                                    })
                                  }}
                                >
                                  -
                                </button>
                              </div>
                              <div className="form-control col-2 col-sm-2 col-md-2 col-xl-2 no-border text-center input-min-max px-0 py-2">
                                <p>{this.state.quantityTravelers}</p>
                              </div>
                              <div className="col-4 col-sm-4 col-md-4 col-xl-4 button-min-max p-0">
                                <button
                                  className="button button-circle"
                                  type="button"
                                  name="travelers"
                                  onClick={(e) => {
                                    let input = this.state.quantityTravelers
                                    input = input += 1
                                    this.setState({
                                      ...this.state,
                                      quantityTravelers: input
                                    })
                                  }}
                                >
                                  +
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card-white">
                      <h3 className="card-white__responsive-title">
                        Lits et invités
                      </h3>
                      {this.state.bed_types.map((value, index) => {
                        return (
                          <div
                            className="card-background-gray-beds mt-5"
                            key={index}
                          >
                            <div className="row">
                              <div className="col-12 col-sm-6 col-md-12 col-lg-6 col-xl-8 mb-3 mb-sm-0 mb-md-3 mb-lg-0">
                                <h4 className="card-white__responsive-subtitle">
                                  {value.name}
                                </h4>
                              </div>
                              <div className="col-12 col-sm-6 col-md-12 col-lg-6 col-xl-4 container-buttons">
                                <div className="row buttons m-0 p-0 align-items-center m-md-0">
                                  <div className="col-4 col-sm-4 col-md-4 col-xl-4 button-min-max p-0">
                                    <button
                                      className="button button-circle-minus"
                                      type="button"
                                      name={value.id}
                                      id="minus"
                                      onClick={this.handleChangeQuantity}
                                    >
                                      -
                                    </button>
                                  </div>
                                  <div className="form-control col-2 col-sm-2 col-md-2 col-xl-2 no-border text-center input-min-max px-0 py-2">
                                    <p>{value.quantity}</p>
                                  </div>
                                  <div className="col-4 col-sm-4 col-md-4 col-xl-4 button-min-max p-0">
                                    <button
                                      className="button button-circle"
                                      type="button"
                                      name={value.id}
                                      id="plus"
                                      onClick={this.handleChangeQuantity}
                                    >
                                      +
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        )
                      })}
                    </div>
                    <div className="card-white">
                      <h3 className="card-white__responsive-title">
                        Nombre de salles de bains
                      </h3>
                      <div className="card-background-gray-beds mt-5">
                        <div className="row">
                          <div className="col-12 col-sm-6 col-md-12 col-lg-6 col-xl-8 mb-3 mb-sm-0 mb-md-3 mb-lg-0">
                            <h4 className="card-white__responsive-subtitle">
                              Salles de bain
                            </h4>
                          </div>
                          <div className="col-12 col-sm-6 col-md-12 col-lg-6 col-xl-4 container-buttons">
                            <div className="row buttons m-0 p-0 align-items-center m-md-0">
                              <div className="col-4 col-sm-4 col-md-4 col-xl-4 button-min-max p-0">
                                <button
                                  className="button button-circle-minus"
                                  type="button"
                                  name="bathroom"
                                  onClick={(e) => {
                                    let input = this.state.quantityBathroom
                                    if (input > 0) input = input -= 1
                                    this.setState({
                                      ...this.state,
                                      quantityBathroom: input
                                    })
                                  }}
                                >
                                  -
                                </button>
                              </div>
                              <p className="form-control col-2 col-sm-2 col-md-2 col-xl-2 no-border text-center input-min-max px-0 py-2">
                                {this.state.quantityBathroom}
                              </p>
                              <div className="col-4 col-sm-4 col-md-4 col-xl-4 button-min-max p-0">
                                <button
                                  className="button button-circle"
                                  type="button"
                                  name="bathroom"
                                  onClick={(e) => {
                                    let input = this.state.quantityBathroom
                                    input = input += 1
                                    this.setState({
                                      ...this.state,
                                      quantityBathroom: input
                                    })
                                  }}
                                >
                                  +
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card-white">
                      <div className="form-group">
                        <h3
                          className="card-white__responsive-title"
                          style={{ marginBottom: 25 + 'px' }}
                        >
                          Points forts *
                        </h3>
                        <span
                          className={`form-room__question-edit-char ${
                            this.state.strengths.length > 255 ? 'color-red' : ''
                          }`}
                        >{`${this.state.strengths.length}/255`}</span>
                        <textarea
                          onChange={(e) => {
                            this.handleInputChange()
                            this.handleStrengthsChange(e)
                          }}
                          className="form-control"
                          name="strengths"
                          id="strengths"
                          required
                          defaultValue={
                            this.state.rooms.length > 0 &&
                            this.state.rooms[0].strengths
                              ? this.state.rooms[0].strengths
                              : ''
                          }
                        ></textarea>
                      </div>
                    </div>
                    <div className="row mt-5 buttons-onboarding">
                      <div className="col-12 col-xl-6 button-back-onboarding">
                        <a
                          className="button button-outline-black button-long"
                          type="button"
                          href={
                            '/onboarding/hebergements/step-one?id=' +
                            this.state.id
                          }
                        >
                          Retour
                        </a>
                      </div>
                      <div className="col-12 col-xl-6 justify-content-end d-flex">
                        <button
                          type="submit"
                          className="button button-long button--save-onboarding"
                          disabled={
                            this.state.savingStepButton !== 'loading' &&
                            this.state.savingStepButton !== 'saved'
                              ? false
                              : true
                          }
                        >
                          {this.state.savingStepButton === 'loading'
                            ? 'En cours...'
                            : this.state.savingStepButton === 'saved'
                            ? 'Enregistré'
                            : 'Sauvegarder et continuer'}
                        </button>
                      </div>
                    </div>
                  </form>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}
// const mapDispatchToProps = {
// }
export default connect(mapStateToProps, null)(withGreenScore(StepTwo))
