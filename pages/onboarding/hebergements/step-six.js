import React from 'react'
import Router from 'next/router'
import Cookies from 'universal-cookie'
import moment from 'moment'
import { setClearLoading } from '@ducks/Config/actions'
import ProgressBar from '../../../components/progressbar'
import Container from '../../../components/container'
import Steper from '../../../components/steper'
import { Loader } from 'components/'
import ModalManualReservations from '@components/Onboarding/ModalManualReservations'
import { toast } from 'react-toastify'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import withGreenScore from 'HOC/withGreenScore'
import { connect } from 'react-redux'
import { ChangePositionImage, resizeFile } from '../../../helpers/imagesHelper'
import { notify } from 'helpers/notificationClassHelper'
import {
  deleteRoomImages,
  getRoomImages,
  saveRoomImage,
  saveRoomImage2,
  updateRoomImagePosition
} from 'services/imagesService'
import {
  GridContextProvider,
  GridDropZone,
  GridItem,
  swap
} from 'react-grid-dnd'
import { getHebergement } from 'services/hebergementsService'

class StepSix extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      step: 6,
      first_text: 'Photos',
      last_text: '',
      image_url: [],
      user_data: [],
      stablishments: [],
      image_url_delete: [],
      isEnableButton: true,
      isShowSidebar: '',
      showSidebar: true,
      rooms: [],
      arrayPositions: [],
      isShowContent: false,
      showModal: true,
      savingStepButton: 'init',
      toUpdate: false
    }
    this.inputFileRef = React.createRef()
    this.handleUploadImage = this.handleUploadImage.bind(this)
    this.handleDeleteImage = this.handleDeleteImage.bind(this)
    this.handleStepSix = this.handleStepSix.bind(this)
    this.handleChangePositionImage = this.handleChangePositionImage.bind(this)
    this.onChange = this.onChange.bind(this)
    library.add(faStar)
  }

  async componentDidMount() {
    const queryString = window.location.search
    const urlParams = new URLSearchParams(queryString)
    const json_images = await getRoomImages(urlParams.get('id'))
    const room = await getHebergement(urlParams.get('id'))
    let images_url = []
    let arrayPositions = []
    json_images.forEach(async (images, index) => {
      images_url.push({
        id: images.id,
        image: process.env.NEXT_PUBLIC_AMAZON_IMGS + images.image.url,
        position: images.order,
        uploadPercent: 100
      })
      arrayPositions.push(index + 1)
    })
    this.setState({
      ...this.state,
      id: urlParams.get('id'),
      image_url: images_url,
      image_url_original: images_url,
      arrayPositions: arrayPositions,
      isShowContent: true,
      rooms: room
    })
  }

  async handleChangePositionImage(image_change, new_index_in) {
    const new_image_url = ChangePositionImage(
      image_change,
      new_index_in,
      this.state.image_url
    )
    this.setState({ ...this.state, image_url: new_image_url })
  }

  async handleUploadImage(event) {
    if (event.target.files.length > 0) {
      const saveImages = Array.from(event.target.files).map(async (file) => {
        // for (let i = 0; i < event.target.files.length; i++) {
        if (file.size >= 5000000) {
          toast.warn(
            'Photo ' +
              file.name +
              ' en grand, veuillez charger des photos de moins de 5Mo',
            {
              position: toast.POSITION.BOTTOM_LEFT
            }
          )
          return {}
        } else {
          const resp = await resizeFile(file)
          const image = URL.createObjectURL(resp)
          const blobNewFile = resp.slice(0, resp.size, resp.type)

          const name = file.name
          const lastDot = name.lastIndexOf('.')
          const fileName = name.substring(0, lastDot)
          const ext = name.substring(lastDot + 1)
          const reducedNewFile = new File(
            [blobNewFile],
            fileName + moment().format('YYYYMMDDhhmmssSS') + '.' + ext,
            { type: blobNewFile.type }
          )

          let arrayPositions = this.state.arrayPositions
          let lentgArrayPositions = this.state.arrayPositions.length + 1
          const newImage = {
            id: null,
            image: image,
            file: reducedNewFile,
            position: lentgArrayPositions,
            uploadPercent: 0
          }
          arrayPositions.push(lentgArrayPositions)
          this.setState({
            ...this.state,
            image_url: [...this.state.image_url, newImage],
            arrayPositions: arrayPositions
          })
          return this.sendImagesToSave(newImage)
        }
      })
      const resp = await Promise.all(saveImages)
      resp.forEach(({ picture, message }) => {
        this.setState({
          ...this.state,
          image_url: this.state.image_url.map((i) => {
            if (i.position === parseInt(picture.order)) {
              return {
                ...i,
                id: picture.id,
                image: process.env.NEXT_PUBLIC_AMAZON_IMGS + message
              }
            }
            return i
          })
        })
      })
    }
    event.target.value = ''
  }

  async handleDeleteImage(index_in) {
    const elementToDelete = this.state.image_url.find(
      (item) => item.position === index_in
    )
    let arrayPositions = []
    let arrayImages = this.state.image_url
      .filter((item) => item.position !== index_in)
      .map((ai, index) => {
        const arrIndex = index + 1
        arrayPositions.push(arrIndex)
        return arrIndex !== ai.position
          ? { ...ai, isModified: true, position: arrIndex }
          : ai
      })

    const test = this.setState({
      ...this.state,
      image_url: arrayImages,
      image_url_delete:
        elementToDelete.id !== null
          ? [...this.state.image_url_delete, elementToDelete]
          : this.state.image_url_delete,
      arrayPositions
    })
  }

  async handleStepSix(event) {
    event.preventDefault()
    this.setState({ ...this.state, savingStepButton: 'loading' })
    if (this.state.image_url.length < 3) {
      this.setState({ ...this.state, savingStepButton: 'error' })
      return notify(`Un minimum de 3 images est requis`, 'warning')
    }

    this.props.setClearLoading(true)

    const deleteResponse = await this.handleDeleteImages()
    if (!deleteResponse) {
      this.setState({ ...this.state, savingStepButton: 'error' })
      return notify(`Une erreur s'est produite, réessayez plus tard`, 'warning')
    }

    const imagesToUpdate = this.state.image_url.filter(
      (item) => item.id !== null && item.isModified
    )

    if (this.state.toUpdate) {
      const updatePromises = this.state.image_url.map((image, index) =>
        updateRoomImagePosition(image.id, index + 1)
      )
      const updateResp = await Promise.all(updatePromises)
    }

    this.setState({
      ...this.state,
      image_url_delete: [],
      toUpdate: false
    })
    this.setState({ ...this.state, savingStepButton: 'saved' })
    notify(`Données sauvegardées avec succès`, 'success')
    this.handleModal()
    // Router.push('/onboarding/finish-onboarding')
    this.props.setClearLoading(false)
    // toast.success('Photos mises à jour', {
    //   position: toast.POSITION.BOTTOM_LEFT
    // })
  }

  handleDeleteImages = async () => {
    let succsesRespones = true
    if (this.state.image_url_delete.length === 0) return succsesRespones

    const promises = this.state.image_url_delete.map((image) =>
      deleteRoomImages(image.id)
    )
    const resp = await Promise.all(promises)
    resp.forEach((r) => {
      if (r.code !== 200) {
        succsesRespones = false
      }
    })
    return succsesRespones
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  sendImagesToSave = async (image) => {
    const formData = new FormData()
    formData.append('image', image.file)
    formData.append('room_id', this.state.id)
    formData.append('order', image.position)
    const resp = await saveRoomImage2(formData, this.uploadChargeProgress)
    // this.notifyUploadPhoto()
    return resp
  }

  uploadChargeProgress = async (position, progress) => {
    this.setState({
      ...this.state,
      image_url: this.state.image_url.map((i) => {
        if (i.position === parseInt(position)) {
          return { ...i, uploadPercent: progress }
        }
        return i
      })
    })
  }

  handleNextStep = async (optionSelected) => {
    const cookies = new Cookies()
    // $('#staticBackdrop').modal('hide')
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'rooms/' + this.state.id, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'PUT',
      body: JSON.stringify({
        ...this.state.rooms[0],
        reservation_mode: optionSelected,
        page_url: '/onboarding/finish-onboarding',
        page: 22
      })
    })
      .then(() => {
        this.setState({ ...this.state, savingStepButton: 'saved' })
        // notify(`Données sauvegardées avec succès`, 'success')
        $('#staticBackdrop').modal('hide')
        Router.push('/onboarding/finish-onboarding')
      })
      .catch((error) => {
        this.setState({ ...this.state, savingStepButton: 'error' })
        notify(`Opps! nous avons une erreur`, 'error')
      })
  }

  handleModal = () => {
    $('#staticBackdrop').modal('show')
  }

  onChange(sourceId, sourceIndex, targetIndex, targetId) {
    const result = swap(this.state.image_url, sourceIndex, targetIndex)
    this.setState({ ...this.state, image_url: result, toUpdate: true })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={4} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div className="row background-white container-onboarding">
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="hebergement"
                  title="Hébergements"
                />
              </div>
              <div className="container-fluid container-onboarding">
                <h2>Photos de votre hébergement</h2>
                <p>
                  En photo aussi, le réflexe c’est moins mais mieux :)
                  Privilégiez la qualité à la quantité.
                </p>
                {this.state.isShowContent ? (
                  <>
                    <form
                      className="form mb-3 mt-4"
                      onSubmit={this.handleStepSix}
                    >
                      <div className="row">
                        <div className="col-12">
                          <div className="card-white rooms-form__images-box">
                            <div className="row rooms-form__row-info">
                              <div className="col-6 rooms-form__row-info-title">
                                <h3>Photos</h3>
                              </div>
                              <div className="col-6 rooms-form__row-info-limit">
                                <div>
                                  <button
                                    className="btn btn-primary mr-4"
                                    type="button"
                                    onClick={() => {
                                      this.inputFileRef.current.click()
                                    }}
                                  >
                                    Ajouter
                                  </button>
                                  <button
                                    type="submit"
                                    className="btn btn-secondary"
                                  >
                                    Sauvegarder
                                  </button>
                                </div>
                                <p className="rooms-form__row-info-text">
                                  Au moins 3 images
                                </p>
                              </div>
                            </div>
                            <GridContextProvider onChange={this.onChange}>
                              <GridDropZone
                                id="items"
                                boxesPerRow={4}
                                rowHeight={150}
                                style={{
                                  height: `${
                                    this.state.image_url.length * 30 + 200
                                  }px`
                                }}
                              >
                                {this.state.image_url.map((value, index) => (
                                  <GridItem key={value.position}>
                                    <div className="grid-content grabbable"></div>
                                    <div className="col-12 card-image change-position-image-box grid-item">
                                      {value.uploadPercent !== 100 && (
                                        <div class="progress image-progress">
                                          <div
                                            class="progress-bar bg-success"
                                            role="progressbar"
                                            style={{
                                              width: `${value.uploadPercent}%`
                                            }}
                                            aria-valuenow={value.uploadPercent}
                                            aria-valuemin="0"
                                            aria-valuemax="100"
                                          ></div>
                                        </div>
                                      )}
                                      <img
                                        src={value.image}
                                        onError={this.handleDefaultImage}
                                        alt="Logo"
                                        className={
                                          'align-middle image-upload-hebergement image-upload-hebergement--no-margin'
                                        }
                                      />
                                    </div>
                                    {value.uploadPercent === 100 && (
                                      <button
                                        className="delete-image-stablishment"
                                        key={index}
                                        onClick={(e) => {
                                          e.stopPropagation()
                                          this.handleDeleteImage(value.position)
                                        }}
                                        type="button"
                                      >
                                        x
                                      </button>
                                    )}
                                  </GridItem>
                                ))}
                                <div className="col-12 card-add">
                                  <input
                                    ref={this.inputFileRef}
                                    id="add_image"
                                    name="add_image"
                                    type="file"
                                    onChange={this.handleUploadImage}
                                    multiple
                                    accept="image/jpeg, image/jpg, image/png"
                                  />
                                </div>
                              </GridDropZone>
                            </GridContextProvider>
                          </div>
                        </div>
                      </div>
                    </form>
                    {this.state.showModal && (
                      <ModalManualReservations onSave={this.handleNextStep} />
                    )}
                  </>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}

const mapDispatchToProps = { setClearLoading }

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withGreenScore(StepSix))
