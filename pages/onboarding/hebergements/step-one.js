import React from 'react'
import Image from 'next/image'
import Router from 'next/router'
import Link from 'next/link'

import Cookies from 'universal-cookie'

import ProgressBar from '../../../components/progressbar'
import Container from '../../../components/container'
import Steper from '../../../components/steper'
import { Loader } from 'components/'
import withGreenScore from 'HOC/withGreenScore'
import { connect } from 'react-redux'
import { notify } from 'helpers/notificationClassHelper'
import { formatSlug } from 'helpers/roomHelper'

class StepOne extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      step: 1,
      first_text: 'Informations',
      last_text: 'Lits et chambres',
      id: null,
      room_types: [],
      rooms: [],
      isEnableButton: true,
      isShowSidebar: '',
      showSidebar: true,
      isShowContent: false,
      savingStepButton: 'init'
    }

    this.handleStepOne = this.handleStepOne.bind(this)
    this.handleShowSidebar = this.handleShowSidebar.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.validateRoomType = this.validateRoomType.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    const queryString = window.location.search
    const urlParams = new URLSearchParams(queryString)
    this.setState({ ...this.state, id: urlParams.get('id') })

    //Get room types
    const res_room_type = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'room-types',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_room_types = await res_room_type.json()
    this.setState({ ...this.state, room_types: json_room_types })

    //Get information room
    const res_rooms = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'rooms/' + this.state.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_rooms = await res_rooms.json()
    this.setState({ ...this.state, rooms: json_rooms, isShowContent: true })
  }

  validateRoomType(value) {
    if (
      value === '' ||
      value === null ||
      value === undefined ||
      Object.is(NaN, value) ||
      value === 0 ||
      value === '0'
    ) {
      notify(`Vous devez sélectionner le type d'hébergement`, 'error')
      return true
    } else {
      return false
    }
  }

  async handleStepOne(event) {
    event.preventDefault()
    this.setState({ ...this.state, savingStepButton: 'loading' })

    if (this.validateRoomType(event.target.type_id.value)) return

    const slug = formatSlug(event.target.public_name.value)

    const cookies = new Cookies()
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'rooms/' + this.state.id, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'PUT',
      body: JSON.stringify({
        active: this.state.rooms[0].active,
        internal_name: event.target.internal_name.value,
        public_name: event.target.public_name.value,
        description: event.target.description_room.value,
        type_id: event.target.type_id.value,
        host_quantity: event.target.host_quantity.value,
        total_area: event.target.total_area.value,
        rooms_quantity: this.state.rooms[0].rooms_quantity,
        people_capacity: this.state.rooms[0].people_capacity,
        bathroom_quantity: this.state.rooms[0].bathroom_quantity,
        strengths: this.state.rooms[0].strengths,
        establishment_id: this.state.rooms[0].establishment_id,
        basic_price: this.state.rooms[0].basic_price,
        weekend_price: this.state.rooms[0].weekend_price,
        min_nigth: this.state.rooms[0].min_nigth,
        max_nigth: this.state.rooms[0].max_nigth,
        seven_days_discount: this.state.rooms[0].seven_days_discount,
        month_discount: this.state.rooms[0].month_discount,
        slug: slug + '-' + this.state.id,
        page_url: '/onboarding/hebergements/step-two?id=' + this.state.id,
        page: 17
      })
    })
      .then((room) => {
        this.setState({ ...this.state, savingStepButton: 'saved' })
        notify(`Données sauvegardées avec succès`, 'success')
        Router.push('/onboarding/hebergements/step-two?id=' + this.state.id)
      })
      .catch((error) => {
        this.setState({ ...this.state, savingStepButton: 'error' })
        notify(`Une erreur s'est produite, réessayez plus tard.`, 'error')
      })
  }

  handleShowSidebar() {
    this.setState({ ...this.state, showSidebar: false }, () => {
      this.setState({
        ...this.state,
        showSidebar: true,
        isShowSidebar: 'display-sidebar-menu'
      })
    })
  }

  handleInputChange(e) {
    this.setState({ ...this.state, savingStepButton: 'init' })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid">
          <div className="row">
            {this.state.showSidebar && (
              <Steper selected={4} isShowSidebar={this.state.isShowSidebar} />
            )}
            <div className="col-12 col-lg-9">
              <div className="row background-white container-onboarding">
                <span
                  style={{ fontSize: 30 + 'px', cursor: 'pointer' }}
                  className="show-menu col-2"
                  onClick={this.handleShowSidebar}
                >
                  &#9776;
                </span>
                <ProgressBar
                  first_text={this.state.first_text}
                  last_text={this.state.last_text}
                  step={this.state.step}
                  page="hebergement"
                  title="Hébergements"
                />
              </div>
              <div className="container-fluid container-onboarding">
                <h2>Informations générales</h2>
                <p>
                  Vous pourrez modifier vos réponses par la suite dans votre
                  tableau de bord.
                </p>
                {this.state.isShowContent ? (
                  <form
                    className="form"
                    onSubmit={this.handleStepOne}
                    method="POST"
                    autoComplete="off"
                  >
                    <div className="card-white__responsive">
                      <div className="form-group">
                        <h3
                          style={{ marginBottom: 20 + 'px' }}
                          htmlFor="internal_name"
                          className="card-white__responsive-title"
                        >
                          Nom interne *
                        </h3>
                        <input
                          onChange={this.handleInputChange}
                          onKeyPress={(e) => {
                            e.key === 'Enter' && e.preventDefault()
                          }}
                          type="text"
                          className="form-control"
                          id="internal_name"
                          name="internal_name"
                          defaultValue={
                            this.state.rooms.length > 0 &&
                            this.state.rooms[0].internal_name
                              ? this.state.rooms[0].internal_name
                              : ''
                          }
                          placeholder="Exemple : chambre de qualité et spacieuse dans la forêt"
                          required
                        />
                      </div>
                      <hr />
                      <div className="card-advice">
                        <h4>Conseil :</h4>
                        <p className="card-white__text--no-margin">
                          C’est votre nom “interne”. Pour s’y retrouver lors des
                          réservations.{' '}
                        </p>
                      </div>
                    </div>
                    <div className="card-white__responsive">
                      <div className="form-group">
                        <h3
                          style={{ marginBottom: 20 + 'px' }}
                          htmlFor="public_name"
                          className="card-white__responsive-title"
                        >
                          Nom personnalisé *
                        </h3>
                        <input
                          onChange={this.handleInputChange}
                          onKeyPress={(e) => {
                            e.key === 'Enter' && e.preventDefault()
                          }}
                          type="text"
                          className="form-control"
                          id="public_name"
                          name="public_name"
                          defaultValue={
                            this.state.rooms.length > 0 &&
                            this.state.rooms[0].public_name
                              ? this.state.rooms[0].public_name
                              : ''
                          }
                          placeholder="Exemple : chambre de qualité et spacieuse dans la forêt"
                          required
                        />
                      </div>
                      <hr />
                      <div className="card-advice">
                        <h4>Conseil :</h4>
                        <p className="card-white__text--no-margin">
                          Un joli nom évocateur, relativement court (maxi 10
                          mots) qui raconte une histoire : on ferme les yeux et
                          on s’y voit déjà ! Si vous n’avez pas d’inspiration,
                          laissez-le vide et envoyez-nous un petit mot
                          (hebergeurs@wegogreenr.com), on s’en occupera ;){' '}
                        </p>
                      </div>
                    </div>
                    <div className="card-white__responsive">
                      <div className="form-group ">
                        <h3
                          style={{ marginBottom: 20 + 'px' }}
                          htmlFor="description_room"
                          className="card-white__responsive-title"
                        >
                          Décrivez votre hébergement à vos futurs voyageurs *
                        </h3>
                        <textarea
                          onChange={this.handleInputChange}
                          className="form-control"
                          name="description_room"
                          id="description_room"
                          required
                          defaultValue={
                            this.state.rooms.length > 0 &&
                            this.state.rooms[0].description
                              ? this.state.rooms[0].description
                              : ''
                          }
                        ></textarea>
                      </div>
                      <hr />
                      <div className="card-advice">
                        <h4>Conseil :</h4>
                        <p className="card-white__text--no-margin">
                          Décrivez le plus précisément le logement et
                          l’expérience que vos voyageurs pourront vivre chez
                          vous (on vous conseille un texte original d'environ
                          650 caractères ou 100 mots){' '}
                        </p>
                      </div>
                    </div>
                    <div className="card-white__responsive">
                      <h3
                        style={{ marginBottom: 20 + 'px' }}
                        className="card-white__responsive-title"
                      >
                        Informations générales
                      </h3>
                      <div className="row">
                        <div className="col-12">
                          <div className="form-group">
                            <label
                              className="card-white__responsive-label"
                              htmlFor="type_id"
                            >
                              Type d'hébergement *
                            </label>
                            <select
                              onChange={this.handleInputChange}
                              className="form-control"
                              id="type_id"
                              name="type_id"
                              required
                            >
                              <option value=""></option>
                              {this.state.room_types.map((value, index) => {
                                if (
                                  this.state.rooms.length > 0 &&
                                  this.state.rooms[0].type_id
                                ) {
                                  if (this.state.rooms[0].type_id == value.id) {
                                    return (
                                      <option
                                        value={value.id}
                                        key={index}
                                        selected
                                      >
                                        {value.name}
                                      </option>
                                    )
                                  } else {
                                    return (
                                      <option value={value.id} key={index}>
                                        {value.name}
                                      </option>
                                    )
                                  }
                                } else {
                                  return (
                                    <option value={value.id} key={index}>
                                      {value.name}
                                    </option>
                                  )
                                }
                              })}
                            </select>
                          </div>
                        </div>
                        <div className="col-12">
                          <div className="form-group">
                            <label
                              className="card-white__responsive-label"
                              htmlFor="host_quantity"
                            >
                              Nombre d'hébergements (de ce type) *
                            </label>
                            <input
                              onChange={this.handleInputChange}
                              onKeyPress={(e) => {
                                e.key === 'Enter' && e.preventDefault()
                              }}
                              type="number"
                              onWheel={(e) => e.target.blur()}
                              className="form-control"
                              id="host_quantity"
                              name="host_quantity"
                              defaultValue={
                                this.state.rooms.length > 0 &&
                                this.state.rooms[0].host_quantity
                                  ? this.state.rooms[0].host_quantity
                                  : ''
                              }
                              min="0"
                              placeholder=""
                              required
                            />
                          </div>
                        </div>
                        <div className="col-12">
                          <div className="form-group card-white__form-group--no-margin">
                            <label
                              className="card-white__responsive-label"
                              htmlFor="total_area"
                            >
                              Superficie de l'hébérgement (m2) *
                            </label>
                            <input
                              onChange={this.handleInputChange}
                              onKeyPress={(e) => {
                                e.key === 'Enter' && e.preventDefault()
                              }}
                              type="number"
                              onWheel={(e) => e.target.blur()}
                              className="form-control card-white__form-control--no-margin"
                              id="total_area"
                              name="total_area"
                              defaultValue={
                                this.state.rooms.length > 0 &&
                                this.state.rooms[0].total_area
                                  ? this.state.rooms[0].total_area
                                  : ''
                              }
                              min="0"
                              placeholder=""
                              required
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row mt-5 buttons-onboarding">
                      <div className="col-12 col-xl-6 button-back-onboarding">
                        <a
                          className="button button-outline-black button-long"
                          type="button"
                          href="/onboarding/hebergements/add"
                        >
                          Retour
                        </a>
                      </div>
                      <div className="col-12 col-xl-6 justify-content-end d-flex">
                        <button
                          type="submit"
                          className="button button-long button--save-onboarding"
                          disabled={
                            this.state.savingStepButton !== 'loading' &&
                            this.state.savingStepButton !== 'saved'
                              ? false
                              : true
                          }
                        >
                          {this.state.savingStepButton === 'loading'
                            ? 'En cours...'
                            : this.state.savingStepButton === 'saved'
                            ? 'Enregistré'
                            : 'Sauvegarder et continuer'}
                        </button>
                      </div>
                    </div>
                  </form>
                ) : (
                  <Loader />
                )}
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}

// const mapDispatchToProps = {
// }

export default connect(mapStateToProps, null)(withGreenScore(StepOne))
