import React from 'react'
import { consoleLog } from '/utils/logConsole'
import Image from 'next/image'
import Container from '../../components/container'
import Router from 'next/router'
import Link from 'next/link'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import Cookies from 'universal-cookie'

class Presentation extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      page: [{ page_url: '' }],
      isShowLink: false
    }

    library.add(faCheckCircle)

    this.handleNextStep = this.handleNextStep.bind(this)
  }

  async componentDidMount() {
    let _this = this
    const cookies = new Cookies()
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'last-page', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    }).then(async function (response) {
      const response_json = await response.json()
      if (response.status !== 200) {
        _this.setState({
          ..._this.state,
          page: response_json || [{ page_url: '' }],
          isShowLink: false
        })
      } else {
        _this.setState({
          ..._this.state,
          page: response_json || [{ page_url: '' }],
          isShowLink: response_json.length > 0 ? true : false
        })
      }
    })
  }

  handleNextStep() {
    Router.push('/onboarding/hotes')
  }

  render() {
    return (
      <Container>
        <div className="row">
          <div className="col-12 col-md-6 col-xl-6 container-onboarding-pictures">
            <div className="row">
              {/* First row */}
              <div className="col-12 col-xl-4 card-image">
                <img
                  src="/images/onboarding/1_new.jpg"
                  alt="Logo 1"
                  width="100%"
                  height="200px"
                />
              </div>
              <div className="col-12 col-xl-7 card-image">
                <img
                  src="/images/onboarding/2_new.jpg"
                  alt="Logo 2"
                  width="100%"
                  height="200px"
                />
              </div>

              {/* Second row */}
              <div className="col-12 col-xl-6 card-image">
                <img
                  src="/images/onboarding/3.jpg"
                  alt="Logo"
                  width="100%"
                  height="200px"
                />
              </div>
              <div className="col-12 col-xl-5 card-image">
                <img
                  src="/images/onboarding/4.jpg"
                  alt="Logo"
                  width="100%"
                  height="200px"
                />
              </div>

              {/* Third row */}
              <div className="col-12 col-xl-3 card-image">
                <img
                  src="/images/onboarding/5.edit.jpg"
                  alt="Logo"
                  width="100%"
                  height="200px"
                />
              </div>
              <div className="col-12 col-xl-8 card-image">
                <img
                  src="/images/onboarding/6_new.jpg"
                  alt="Logo"
                  width="100%"
                  height="200px"
                />
              </div>

              {/* Fourth row */}
              <div className="col-12 col-xl-6 card-image">
                <img
                  src="/images/onboarding/7.jpg"
                  alt="Logo"
                  width="100%"
                  height="200px"
                />
              </div>
              <div className="col-12 col-xl-5 card-image-border">
                <img
                  src="/images/onboarding/8.jpg"
                  alt="Logo"
                  width="100%"
                  height="200px"
                />
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6 col-xl-6 container-onboarding-presentation">
            <div className="card-white">
              <div className="col-6 img-logo">
                <img src="/images/logo.svg" alt="Logo list" />
              </div>
              <h1 className="col-12">
                Pourquoi devenir hôte de <br />
                We Go GreenR
              </h1>
              <div className="col-6 img-line">
                <img src="/images/line_yellow.png" alt="Line" layout="fill" />
              </div>
              <ul className="col-12">
                <li>
                  <FontAwesomeIcon
                    icon="check-circle"
                    size="1.5x"
                    className="icon-check"
                  />
                  Sans droit d’entrée
                </li>
                <li>
                  <FontAwesomeIcon
                    icon="check-circle"
                    size="1.5x"
                    className="icon-check"
                  />
                  Sans engagement
                </li>
                <li>
                  <FontAwesomeIcon
                    icon="check-circle"
                    size="1.5x"
                    className="icon-check"
                  />
                  Commission des nuitées à{' '}
                  <strong>
                    <span style={{ color: 'black', fontWeight: 'bold' }}>
                      seulement 10% HT
                    </span>
                  </strong>
                </li>
                <li>
                  <FontAwesomeIcon
                    icon="check-circle"
                    size="1.5x"
                    className="icon-check"
                  />
                  Commission des activités à{' '}
                  <strong>
                    <span style={{ color: 'black', fontWeight: 'bold' }}>
                      seulement 15% HT
                    </span>
                  </strong>
                </li>
                <li>
                  <FontAwesomeIcon
                    icon="check-circle"
                    size="1.5x"
                    className="icon-check"
                  />
                  Une équipe à votre écoute et basée à Bordeaux
                </li>
              </ul>
              <button
                type="button"
                onClick={this.handleNextStep}
                className="button button-long"
              >
                Créez votre premier hébergement écoresponsable
              </button>
              {this.state.isShowLink && (
                <Link href={this.state.page[0].page_url}>
                  <a className="d-flex justify-content-center mt-2">
                    Reprendre votre inscription
                  </a>
                </Link>
              )}
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

export default Presentation
