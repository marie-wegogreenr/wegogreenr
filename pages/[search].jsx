import * as React from 'react'
import { useWindowSize } from 'hooks'
import Head from 'next/head'
/* Components */
import parse from 'html-react-parser'
import { Layout, Loader, Pagination, Footer } from 'components/'
import { CardList, Form, Filter } from 'components/Search'
import { ResponsiveSearch } from 'components/SearchResponsive'
import { ChevronUp } from 'components/Icons'
import { useRouter } from 'next/router'
/* Constants */
import { statuses, SCROLLED_STEP } from 'constants/index'
/* Redux */
import { useSelector, useDispatch } from 'react-redux'
/*next*/
import dynamic from 'next/dynamic'
/* Browser detect */
const { detect } = require('detect-browser')
/* Utils */
import { useClickOutside } from 'hooks'
import handleSEO from 'constants/seo'
import {
  handleSearchProps,
  locationTextResponse,
  searchPaths
} from 'helpers/searchHelper'
import {
  cleanFilters,
  cleanOnlyFilters,
  cleanSelection,
  filterRooms,
  initSearching,
  searchRoomsByMapBounds,
  setSearchLocation
} from '@ducks/hebergements/search/actions'
import { SeoComponent } from '@components/Seo/SeoComponent'
import handleSearchTexts from 'constants/searchContent'
import { QuestionsAndLinks } from '@components/Search/QuestionsAndLinks'
import { handleRegions } from 'constants/regions'

const DynamicComponentWithNoSSR = dynamic(
  () => import('@components/Maps/SearchMapLeaflet'),
  { ssr: false }
)
function Search({ seo, searchProps, searchTexts }) {
  /*  Global states  */
  const {
    data: {
      rooms,
      pagedRooms,
      searchParams,
      searchParams: { types = [], locationName },
      isOpenModal,
      establishments
    },
    status
  } = useSelector((state) => state.searchedRooms)
  const dispatch = useDispatch()
  const router = useRouter()
  const search = router.query.search

  const [isLoaded, setIsLoaded] = React.useState(false)

  /*  Effects  */
  React.useEffect(() => {
    const timer = setTimeout(() => {
      setIsLoaded(true)
    }, 3000)

    return () => clearTimeout(timer)
  }, [])

  React.useEffect(() => {
    if (search === 'search') return
    if (!isLoaded) return
    if (status !== 'INIT') return

    const goToSearch = () => {
      router.push('/search')
    }

    if (types?.length === 1) {
      const path = router.asPath.split('/')[1]
      if (types[0] !== searchPaths[path]) {
        goToSearch()
      }
    } else {
      goToSearch()
    }
  }, [types, status])

  React.useEffect(() => {
    const detectChange = () => {
      const path = router.asPath.split('/')[1]
      if (
        locationName &&
        handleRegions(path).name !== 'Other' &&
        locationName !== 'France' &&
        locationName !== 'Autre site'
      ) {
        if (handleRegions(path).name !== locationName) {
          router.push('/search')
        }
      }
    }

    const timer = setTimeout(() => {
      detectChange()
    }, 600)

    return () => clearTimeout(timer)
  }, [locationName])

  React.useEffect(() => {
    if (search === 'search') return
    dispatch(setSearchLocation({ ...searchParams, ...searchProps }))
    dispatch(searchRoomsByMapBounds({ ...searchParams, ...searchProps }))
  }, [])
  React.useEffect(() => {
    const reset = () => {
      const path = router.asPath
      const _region = handleRegions(path.slice(1))
      const conditions = {
        1: {
          condition: () => path.includes('search'),
          action: () => {
            dispatch(filterRooms())
            dispatch(initSearching())
          }
        },
        2: {
          condition: () =>
            path.includes('selection') || path.includes('la-selection'),
          action: () => {
            dispatch(cleanSelection())
          }
        },
        3: {
          condition: () => handleRegions(path.slice(1)).name !== 'Other',
          action: () => {
            dispatch(
              setSearchLocation({ ...searchParams, locationName: _region.name })
            )
          }
        },
        4: {
          condition: () =>
            Object.keys(searchPaths).some((item) => item === path.slice(1)),
          action: () => {
            dispatch(
              setSearchLocation({ ...searchParams, locationName: 'France' })
            )
          }
        }
      }

      if (searchParams) {
        const conditionsLength = Object.values(conditions).length
        for (let i = 1; i <= conditionsLength; i++) {
          if (conditions[i].condition()) {
            conditions[i].action()
            break
          }
          if (i === conditionsLength) dispatch(cleanOnlyFilters())
        }
      }
    }

    const timer = setTimeout(() => {
      reset()
    }, 500)
    return () => clearTimeout(timer)
  }, [router])

  /*  Local states  */
  const { width } = useWindowSize()
  const [isMobile, handleResizeScreenState] = React.useState(false)
  const [isScrollTop, handleTopScrollState] = React.useState(false)
  const [scrollStep, handleScrollStepState] = React.useState(
    SCROLLED_STEP.INITIAL
  )
  const [isIOS, handleIOSDeviceState] = React.useState(false)
  const [showMore, setShowMore] = React.useState(false)
  /*  Refs  */
  const desktopRef = React.useRef(null)
  const executeScroll = () => desktopRef.current.scrollTo(0, 0)

  const browser = detect()
  /*  Effects */
  React.useEffect(() => {
    handleIOSDeviceState(browser.os === 'iOS')
  }, [browser])

  React.useEffect(() => {
    handleResizeScreenState(width < 1200)
  }, [width])

  React.useEffect(() => {
    window.onscroll = (e) => {
      const { pageYOffset, innerHeight } = window
      detectIsScrollTop(pageYOffset, innerHeight)
      stepScroll(pageYOffset, innerHeight)
    }
  }, [])

  /* Step scrolled is initial, midle, top*/
  const stepScroll = (pageYOffset, innerHeight) => {
    let torelanceInitial = 40
    let toleranceMidle = innerHeight * 0.4
    if (pageYOffset < torelanceInitial) return notScrolled()
    if (pageYOffset < toleranceMidle) return inMiddle()
    if (pageYOffset > toleranceMidle) return aboveMiddle()
  }

  /*  Functions  */
  const notScrolled = () => {
    handleScrollStepState(SCROLLED_STEP.INITIAL)
  }

  const inMiddle = () => {
    handleScrollStepState(SCROLLED_STEP.MIDDLE)
  }

  const aboveMiddle = () => {
    handleScrollStepState(SCROLLED_STEP.ABOVE)
  }

  const detectIsScrollTop = (pageYOffset, innerHeight) => {
    const sumPaddings = 150
    const boundTop = innerHeight - sumPaddings
    var isTop = pageYOffset > innerHeight - sumPaddings
    handleTopScrollState(isTop)
  }

  const scrollTo = (position = 0) => {
    window?.scrollTo({ top: position, behavior: 'smooth' })
  }

  const scrolledCardList = () => {
    const { innerHeight } = window
    var middle = innerHeight * 0.5
    var top = innerHeight * 0.8
    if (scrollStep === SCROLLED_STEP.INITIAL) return scrollTo(middle)
    return scrollTo(top)
  }

  const marginByDevice = '80px'

  return (
    <>
      <SeoComponent seo={seo} />
      {isMobile && (
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-start',
            backgroundColor: '#e5e5e5'
          }}
        >
          <ResponsiveSearch onScrollTop={isScrollTop} />
          {!isOpenModal && (
            <div style={{ visibility: 'hidden', position: 'absolute' }}>
              <DynamicComponentWithNoSSR />
            </div>
          )}
          {!isOpenModal && (
            <div
              className="responsive-column-cards__container"
              // ref={domNodeCardList}
            >
              {/* <div
                className="responsive-column-cards__header"
                onClick={() => scrolledCardList()}
              > */}
              {/* <span className="responsive-column-cards__location-text">
                  Séjours à {searchParams.locationName}
                </span> */}
              {searchTexts.text !== null && (
                <>
                  <p style={{ whiteSpace: 'pre-line', padding: '0 12px' }}>
                    {parse(
                      showMore
                        ? searchTexts.text
                        : `${searchTexts.text.substring(0, 250)}...`
                    )}
                    <a
                      className="color-secondary"
                      onClick={() => setShowMore(!showMore)}
                    >
                      {showMore ? 'voir moins' : 'voir plus'}
                    </a>
                  </p>
                </>
              )}

              <div className="responsive-column-cards__cardlist-container">
                <div className="responsive-column-cards__card">
                  {status === 'SUCCESS' && (
                    <CardList rooms={rooms} establishments={establishments} />
                  )}
                </div>
                <div className="responsive-column-cards__pagination">
                  {status === 'SUCCESS' && pagedRooms?.last_page > 1 && (
                    <Pagination executeScroll={executeScroll} />
                  )}
                </div>
                {scrollStep !== SCROLLED_STEP.INITIAL && (
                  <div
                    className="search__arrow-up search__arrow-up--responsive"
                    onClick={scrollTo}
                  >
                    <ChevronUp width="35" height="35" />
                  </div>
                )}
                <div style={{ padding: '0 12px' }}>
                  <QuestionsAndLinks
                    questions={searchTexts.questions}
                    links={searchTexts.redirections}
                  />
                </div>

                <Footer type="green" />
              </div>
            </div>
          )}
        </div>
      )}
      {!isMobile && (
        <Layout from="search">
          <div className="search__container">
            <div className="row m-0">
              <div
                className="col-lg-12 col-xl-6 column--rooms px-lg-5 px-3 pb-4"
                ref={desktopRef}
              >
                <h1 className="size-h1 color-primary pt-4">
                  {searchTexts.searchTitle}
                </h1>
                <Form />
                <Filter />
                {searchTexts.text !== null && (
                  <>
                    <p style={{ whiteSpace: 'pre-line' }}>
                      {parse(
                        showMore
                          ? searchTexts.text
                          : `${searchTexts.text.substring(0, 250)}...`
                      )}
                      <a
                        className="color-secondary"
                        onClick={() => setShowMore(!showMore)}
                      >
                        {showMore ? 'voir moins' : 'voir plus'}
                      </a>
                    </p>
                  </>
                )}
                <p className="fz-14">
                  {locationTextResponse(
                    pagedRooms?.total,
                    searchParams?.locationName
                  )}
                </p>
                {status === statuses.LOADING && <Loader />}

                {status === 'SUCCESS' && (
                  <CardList rooms={rooms} establishments={establishments} />
                )}
                {status === 'SUCCESS' && pagedRooms?.last_page > 1 && (
                  <Pagination executeScroll={executeScroll} />
                )}

                <QuestionsAndLinks
                  questions={searchTexts.questions}
                  links={searchTexts.redirections}
                />
              </div>

              <div className="col-lg-12 col-xl-6 px-0" id="google-map">
                <DynamicComponentWithNoSSR />
              </div>
            </div>
          </div>
          {scrollStep !== SCROLLED_STEP.INITIAL && (
            <div className="search__arrow-up" onClick={scrollTo}>
              <ChevronUp width="35" height="35" />
            </div>
          )}
        </Layout>
      )}
    </>
  )
}

export async function getServerSideProps({ query }) {
  try {
    const { search } = query
    const seo = handleSEO(search)
    const searchProps = handleSearchProps(search)
    const searchTexts = handleSearchTexts(search)
    return {
      props: {
        seo,
        searchProps,
        searchTexts
      }
    }
  } catch (error) {
    let mockObject = {
      seo: {},
      searchProps: {},
      searchTexts: {}
    }
    return {
      props: mockObject
    }
  }
}

export default Search
