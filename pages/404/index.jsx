import Container from '@components/container'
import React from 'react'
//ASSETS
import { UserExplorer } from '@components/Icons'
import Link from 'next/link'

const index = () => {
  return (
    <Container>
      <div
        className="d-flex flex-column align-items-center justify-content-center"
        style={{ height: '100vh' }}
      >
        <UserExplorer width="200" height="200" />
        <h1 className="mt-4 weight-bold" style={{ fontSize: '5rem' }}>
          404
        </h1>
        <p className="text weight-bold mt-3 text-center">{`La page que vous recherchez semble introuvable`}</p>
        <div
          className="border-bottom-divisor my-3"
          style={{ height: '1px', width: '85px' }}
        ></div>
        <Link href="/">
          <button className="btn btn-secondary weight-bold text py-3 px-5 br-10">
            {`Revenir sur notre page d'accueil`}
          </button>
        </Link>
      </div>
    </Container>
  )
}

export default index
