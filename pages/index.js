import React, { useState } from 'react'
/* Components */
import { CardList, Layout } from 'components'
import {
  Footer,
  GiftCard,
  GoFurther,
  Hero,
  OurSpirit,
  Regions,
  Reviews,
  Surprised,
  Thematic
} from 'components/Home'
/* Utils */
import { getData } from 'utils'
/* Constants */
import { endpoints } from 'constants/index'
/* Hooks */
import { useLocation } from 'hooks'
/* Redux */
import { useDispatch, useSelector } from 'react-redux'
import { getRoomsNearMe } from '@ducks/hebergements/nearMe'
import { getPostsFromBlogAction } from '@ducks/blog/actions'
import { consoleLog } from 'helpers/utilsHelper'
import { roomNearMe } from 'services/homeServices'
import { cleanInputs } from '@ducks/hebergements/search/actions'
import { cleanReservationAction } from '@ducks/checkoutDuck'
import CartCadeauHome from '@components/CartCadeau/CartCadeauHome'
import NewsletterSection from '@components/Newsletter/NewsletterSection'
import FullSlide from '@components/Home/FullSlideSection'

/******************************** MAIN COMPONENT *******************************/
function Home({ rooms = [], roomTypes = [], randomRooms = [] }) {
  // hooks
  // const { lon, lat, locationAccess } = useLocation()
  const [closeRooms, setCloseRooms] = useState([])
  // const [randomCloseRooms, setRandomCloseRooms] = useState(false)
  // dispatch
  const dispatch = useDispatch()
  // redux state
  // const { data: roomsNearMe } = useSelector((state) => state.roomsNearMe)
  // React.useEffect(async () => {
  //   let data = {}
  //   if (lon && lat) {
  //     data = {
  //       distance: '50',
  //       lng: lon.toString(),
  //       lat: lat.toString(),
  //       locationAccess
  //     }
  //   } else {
  //     data = {
  //       distance: '50',
  //       lng: lon,
  //       lat: lat,
  //       locationAccess
  //     }
  //   }
  //   const resp = await roomNearMe(data)
  //   if (resp.code === 200) {
  //     const { random, ...rooms } = resp.data
  //     setCloseRooms(Object.values(rooms))
  //     setRandomCloseRooms(random)
  //   }
  //   // dispatch(getRoomsNearMe(data))
  // }, [lon, lat])

  React.useEffect(() => {
    dispatch(getPostsFromBlogAction())
    dispatch(cleanInputs())
    // dispatch(cleanReservationAction())
  }, [])
  return (
    <Layout>
      <Hero />
      <Footer />
      <div className="bg-yellow-light custom-slick">
        <div className="pt-0 pt-lg-5  bg-yellow-light"></div>
        <section className="py-5 mt-5 mt-sm-0 mt-1 bg-yellow-light">
          <div className="container-xxl home-room__slider">
            <CardList
              title="Nos coups de coeur"
              rooms={rooms}
              className="hero__first-room-slider"
              titleClass="hero__first-room-slider-title"
            />
          </div>
        </section>
        <section className="py-5 py-sm-5">
          <Thematic thematics={roomTypes} />
        </section>
        {/* <section className="py-5 py-sm-5 bg-yellow-light">
          <div className="container-xxl">
            {!randomCloseRooms && (
              <CardList
                title={`${
                  randomCloseRooms
                    ? 'des salles recommandées pour vous'
                    : "S'évader à deux pas de chez soi"
                }`}
                rooms={closeRooms}
              />
            )}
          </div>
        </section> */}

        <NewsletterSection />

        <OurSpirit />
        <div className="pb-5 pt-2 pt-md-5 bg-yellow-light">
          <Regions />
        </div>

        <CartCadeauHome />

        <FullSlide />

        <div className="mt-5 mb-0 bg-yellow-light">
          <Reviews />
        </div>
        <div className="py-5 py-sm-5 bg-yellow-light">
          <GoFurther />
        </div>
      </div>
    </Layout>
  )
}

export async function getServerSideProps() {
  try {
    const options = {
      method: 'GET'
    }

    const rooms = await getData({
      ...options,
      url: endpoints.ROOMS_OUTSANDING
    })

    const randomRooms = await getData({
      ...options,
      url: endpoints.ROOMS_RANDOM
    })

    const roomTypes = await getData({
      ...options,
      url: endpoints.ROOM_TYPES
    })

    return {
      props: {
        rooms,
        roomTypes,
        randomRooms
      }
    }
  } catch (error) {
    console.error(error)

    return {
      props: {
        rooms: [],
        roomTypes: []
      }
    }
  }
}

export default Home
