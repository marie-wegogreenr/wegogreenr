import Container from '@components/container'

const index = () => {
  return (
    <Container>
      <div className="wrapper-5 mt-5" style={{ paddingTop: '6rem' }}>
        <h1 className="h1 weight-bold">
          CHARTE SUR LE RESPECT DE LA VIE PRIVÉE
        </h1>

        <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
          Date de dernière mise à jour : 20/03/2020
        </h2>

        <p className="text mt-3">
          La présente charte sur le respect de la vie privée (la « Charte ») a
          pour objectif de formaliser notre engagement quant au respect de la
          vie privée des utilisateurs du site internet www.wegogreenr.com (le «
          Plateforme ») exploité par la SAS WE GO GREENR.
          <br />
          <br />
          <b>
            La Charte et les Conditions Générales d’Utilisation de la Plateforme
            forment un ensemble contractuel. Tous les termes en majuscules
            non-définis dans la présente Charte sont définis dans les Conditions
            Générales consultables sur le site www.wegogreenr.com ou via demande
            par email à rgpd@wegogreenr.com ou par courrier à WE GO GREENR,
            Service RGPD, 108 cours du Médoc 33300 Bordeaux.
          </b>
          Dans le cadre de la mise à disposition de notre Plateforme, nous
          traitons vos données à caractère personnel dans le respect du
          Règlement Général sur la Protection des Données 2016/679 du 27 avril
          2016 (« RGPD ») et dans les conditions exposées ci-après.
          <br />
          <br />
          Une donnée à caractère personnel désigne toute information se
          rapportant à une personne physique identifiée ou identifiable. Nous
          collectons et traitons des données à caractère personnel dans le cadre
          de la fourniture de nos Services ou de la communication à propose de
          ces Services exclusivement, en stricte conformité avec le RGPD.
          <br />
          <br />
          Nous collectons uniquement des données à caractère personnel
          adéquates, pertinentes et limitées à ce qui est nécessaire au regard
          des finalités pour lesquelles elles sont traitées. Ainsi, il ne vous
          sera jamais demandé de renseigner des données à caractère personnel
          considérées comme « sensibles », telles que vos origines raciales ou
          ethniques, vos opinions politiques, philosophiques ou religieuses.
          <br />
          <br />
          En vous enregistrant sur la Plateforme, vous nous autorisez à traiter
          vos données à caractère personnel conformément à la Charte.
          <b>
            Si vous refusez les termes de cette Charte, veuillez-vous abstenir
            d’utiliser la Plateforme ainsi que les Services.
            <br />
            <br />
            <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
              1. Dans quels cas collectons-nous vos données à caractère
              personnel et quelles données sont collectées ?
            </h2>
          </b>
          <br />
          <br />
          Nous sommes susceptibles de recueillir et de conserver vos données à
          caractère personnel, notamment lorsque :
        </p>

        <ul>
          <li>vous naviguez sur la Plateforme</li>

          <li>
            vous renseignez des informations personnelles (prénom, nom, email,
            date de naissance) pour faire une demande de réservation
          </li>

          <li>
            vous payez en ligne via notre prestataire pour confirmer votre
            réservation
          </li>

          <li>
            vous nous contactez par email ou via un formulaire ou un chatbot
          </li>
        </ul>

        <p className="text mt-3">
          Nous utilisons vos données à caractère personnel pour permettre la
          mise en œuvre et la gestion des Services de la Plateforme et répondre
          à vos demandes spécifiques. Nous utilisons également vos données à
          caractère personnel dans le but d'exploiter et d'améliorer nos
          Services, notre Plateforme et notre démarche. Ces informations sont
          utilisées uniquement par nos soins et nous permettent de mieux adapter
          nos Services à vos attentes.
        </p>

        <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
          2. Comment protégeons-nous vos données à caractère personnel ?
        </h2>

        <p className="text mt-3">
          Nous avons mis en place des mesures de sécurité techniques et
          organisationnelles en vue de garantir la sécurité, l’intégrité et la
          confidentialité de toutes vos données à caractère personnel, afin
          d’empêcher que celles-ci soient déformées, endommagées ou que des
          tiers non autorisés y aient accès. Nous assurons un niveau de sécurité
          approprié, compte tenu de l'état des connaissances, des coûts de mise
          en œuvre et de la nature, de la portée, du contexte et des finalités
          du traitement ainsi que des risques et de leur probabilité.
          <br />
          <br />
          Toutefois, il est précisé qu’aucune mesure de sécurité n’étant
          infaillible, nous ne sommes pas en mesure de garantir une sécurité
          absolue à vos données à caractère personnel.
        </p>

        <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
          3. Dans quels cas partageons-nous vos données à caractère personnel ?
        </h2>

        <p className="text mt-3">
          Les données que nous collectons sont accessibles à nos prestataires de
          services, agissant en qualité de sous-traitants, qui concourent
          administrativement et techniquement à la réalisation des finalités
          visées ci-dessus (prestataire d’hébergement, prestataire de paiement,
          technologie publicitaire…)
          <br />
          <br />
          Dans ce cadre, les données que nous collectons peuvent être
          transférées hors de l’Union Européenne. Dans ce cas nous nous assurons
          que ce transfert s’effectue à destination des pays reconnus comme
          assurant un niveau adéquat de protection de vos données personnelles
          ou, à tout le moins, sur la base des garanties appropriées prévues par
          la loi.
          <br />
          <br />
          Afin de respecter nos obligations légales, vos données personnelles
          peuvent également être transmises aux autorités administratives et
          judiciaires autorisées, uniquement sur réquisition judiciaire.
          <br />
          <br />
          Certains prestataires auxquels nous faisons appel sont susceptibles de
          collecter, avec votre consentement, des données en qualité de
          responsable de traitement. Il s’agit de notre prestataire de paiement
          Lemonway, qui collecte les informations dites « Know Your Customer »
          visées ci-dessus. Ce dernier les utilise afin de vérifier votre
          identité, conformément à la règlementation visant la lutte contre le
          blanchiment et le financement du terrorisme.
          <br />
          <br />
          Enfin, lorsque vous remplissez un formulaire de devis, vos données de
          contact sont transmises à l’Hôte professionnel, ayant la qualité de
          responsable de traitement, qui est à l’origine de l’Annonce concernée.
          En effet, tout annonceur professionnel déposant une annonce sur
          wegogreenr.com est responsable du traitement de toutes les données
          personnelles qu’il est susceptible de recevoir des utilisateurs de
          wegogreenr.com. A ce titre, il s'engage à respecter les dispositions
          légales et réglementaires en vigueur relatives aux données
          personnelles des utilisateurs qu'il collecterait. Notamment, il
          s'engage à mettre en œuvre tout moyen technique pour maintenir
          l'intégrité, la sécurité, la confidentialité des données personnelles
          ainsi qu'à garantir la sécurité des accès aux systèmes de stockage de
          ces données personnelles. L'annonceur professionnel est informé des
          obligations lui incombant en tant que responsable de traitement, et
          notamment le respect des droits des utilisateurs et en particulier,
          leur droit d'accès, d'opposition et de rectification des informations
          les concernant. Si les utilisateurs souhaitent exercer leurs droits,
          ils doivent contacter l’annonceur concerné directement. L’annonceur
          professionnel engage sa responsabilité en cas de non-respect des
          présentes obligations et garantit et indemnisera WE GO GREENR et ses
          prestataires (y compris les frais de justice et honoraires d’avocat)
          contre toute action, revendication ou demande introduite par une
          autorité ou un tiers suite à tout manquement de l’annonceur aux termes
          du présent article.
          <br />
          <br />
          D’autres transferts de données à caractère personnel vers les
          Etats-Unis sont régis par le E.U. – U.S. PRIVACY SHIELD (Bouclier de
          protection des Données Union Européenne-Etats-Unis) : cliquez ici pour
          plus d’informations.
          <br />
          <br />
          Si vous procédez à votre désinscription de la Plateforme, vos données
          à caractère personnel seront effacées et uniquement conservées sous
          forme d’archive aux fins d’établissement de la preuve d’un droit ou
          d’un contrat. La durée de conservation de vos données personnelles
          varie en fonction de la finalité de leurs collectes :
          <br />
          <br />
          Les données de Compte (identité, coordonnées électroniques, historique
          d’utilisation des services, CV importé) sont conservées 3 ans à
          compter de la fin de la relation commerciale ;
          <br />
          <br />
          Les données relatives aux Annonces sont conservées pour une durée de 5
          ans à compter de leur suppression et ce, exclusivement à des fins de
          preuve ;
          <br />
          <br />
          Les documents et pièces comptables sont conservés 10 ans, à titre de
          preuve comptable ;
          <br />
          <br />
          Les données susceptibles de faire l’objet d’une réquisition judiciaire
          (données de connexion, identité, coordonnées de contact, données
          relatives aux transactions) sont conservées 12 mois à compter de leur
          collecte ; Les données stockées lorsque vous effectuez une « recherche
          autour de vous » ne sont conservées que le temps de cette recherche.
        </p>

        <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
          4. Cookies : comment les utilisons-nous ?
        </h2>

        <p className="mt-3 text">
          Un cookie est un fichier de petite taille, qui ne permet pas
          l’identification de l’utilisateur, mais qui enregistre des
          informations relatives à la navigation d’un ordinateur sur un site.
          Les données ainsi obtenues visent à faciliter la navigation ultérieure
          sur le site, et ont également vocation à permettre diverses mesures de
          fréquentation. Le refus d’installation d’un cookie peut entraîner
          l’impossibilité d’accéder à certains services.
          <br />
          <br />
          Lors de votre première visite sur la Plateforme, un bandeau cookies
          apparaîtra en page d’accueil. Un lien cliquable permet d’en savoir
          plus sur la finalité et le fonctionnement des cookies et renvoie vers
          la présente Charte. Les cookies déposés sur votre terminal de
          navigation avec votre accord sont détruits 13 mois après leur dépôt
          sur votre terminal.
          <br />
          <br />
          Pour obtenir plus d’information sur les cookies, vous pouvez consulter
          le site internet de la CNIL.
          <br />
          <br />
          Les cookies que nous émettons nous permettent :
        </p>

        <ul>
          <li>
            d'établir des statistiques et volumes de fréquentation et
            d'utilisation des divers éléments composant notre Plateforme
            (rubriques et contenus visités, parcours), nous permettant
            d'améliorer l'intérêt et l'ergonomie de la Plateforme et, le cas
            échéant, de nos produits et services ;
          </li>

          <li>
            d'adapter la présentation de notre Plateforme aux préférences
            d'affichage de votre terminal (langue utilisée, résolution
            d'affichage, système d'exploitation utilisé, etc.) lors de vos
            visites sur notre Plateforme, selon les matériels et les logiciels
            de visualisation ou de lecture que votre terminal comporte ;
          </li>

          <li>
            de mémoriser des informations relatives à un formulaire que vous
            avez rempli sur notre Plateforme (inscription ou accès à votre
            compte) ou à des produits, services ou informations que vous avez
            choisis sur notre Plateforme (service souscrit, contenu d'un panier
            de commande, etc.) ;
          </li>
        </ul>

        <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
          5. Quels sont vos droits ?
        </h2>

        <p className="text mt-3">
          Conformément à la réglementation en matière de protection des données
          à caractère personnel, notamment les articles 15 à 22 du RGPD, et
          après avoir justifié de votre identité, vous avez le droit de nous
          demander l'accès aux données à caractère personnel vous concernant, la
          rectification ou l'effacement de celles-ci.
          <br />
          <br />
          Droit d'accès : il s’agit de votre droit d’obtenir la confirmation que
          vos données sont traitées ou non, et si oui, d’accéder à ces données.
          <br />
          <br />
          Droit de rectification : il s’agit de votre droit d’obtenir, dans les
          meilleurs délais, que vos données inexactes soient rectifiées, et que
          vos données incomplètes soient complétées.
          <br />
          <br />
          Droit de suppression/Effacement : il s’agit de votre droit d’obtenir,
          dans les meilleurs délais, l’effacement de vos données, sous réserve
          de nos obligations légales de conservation.
          <br />
          <br />
          Droit de limitation : il s’agit de votre droit d’obtenir la limitation
          du traitement lorsque vous vous y opposez, lorsque vous contestez
          l’exactitude de vos données, lorsque vous pensez que leur traitement
          est illicite, ou lorsque vous en avez besoin pour la constatation,
          l’exercice ou la défense de vos droits en justice.
          <br />
          <br />
          Droit d'opposition : il s’agit de votre droit de vous opposer à tout
          moment au traitement de vos données par WE GO GREENR, sauf en cas de
          motifs légitimes et impérieux de WE GO GREENR. Vous pouvez également
          vous opposer au traitement fait à des fins de prospection.
          <br />
          <br />
          Droit à la portabilité : il s’agit de votre droit de recevoir vos
          données dans un format structuré, couramment utilisé, lisible par
          machine et interopérable, et de les transmettre à un autre responsable
          du traitement sans que nous y fassions obstacle.
          <br />
          <br />
          Droit de ne pas faire l'objet d'une décision fondée sur un traitement
          automatisé : vous avez le droit de ne pas faire l’objet d’une décision
          fondée exclusivement sur un traitement automatisé produisant des
          effets juridiques vous concernant ou vous affectant, sauf lorsque
          cette décision est nécessaire à la conclusion ou à l’exécution d’un
          contrat, ou est autorisée légalement.
          <br />
          <br />
          Réclamations : vous avez le droit d’introduire une réclamation auprès
          d’une autorité de contrôle.
          <br />
          <br />
          Directives en cas de décès : vous avez le droit de donner des
          directives pour le sort de vos données personnelles en cas de décès.
          <br />
          <br />
          Vous pouvez contactez nos Services afin d’exercer vos droits à
          l’adresse électronique suivante : rgpd@wegogreenr.com ou à l’adresse
          postale suivante : WE GO GREENR, Service RGPD, 108 Cours du Médoc
          33300 Bordeaux, en joignant à votre demande une copie d’un titre
          d’identité.
        </p>

        <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
          6. Pouvons-nous modifier la Charte ?
        </h2>

        <p className="mt-3 text">
          Nous nous réservons le droit de modifier la Charte à tout moment. Il
          est vous est donc recommandé de la consulter régulièrement.
          <br />
          <br />
          Votre utilisation de la Plateforme après toute modification signifie
          que vous acceptez ces modifications. Si vous n'acceptez pas certaines
          modifications substantielles apportées à la présente Charte, vous
          devez arrêter d'utiliser la Plateforme.
        </p>
      </div>
    </Container>
  )
}

export default index
