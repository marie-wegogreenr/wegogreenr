import { TicTacTrip } from '@components/Hebergements/Single/script/tictactrip'
import SinglePageLayout from '@components/layouts/SinglePageLayout'

export default function TicTacTripPage() {
  return (
    <SinglePageLayout>
      <TicTacTrip />
    </SinglePageLayout>
  )
}
