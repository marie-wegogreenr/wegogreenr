import React from 'react'
import Container from '../components/container'
import Link from 'next/link'
import Router from 'next/router'
import { toast } from 'react-toastify'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBell, faCommentDots } from '@fortawesome/free-regular-svg-icons'
import {
  faCheckCircle,
  faEye,
  faEyeSlash
} from '@fortawesome/free-solid-svg-icons'

import Cookies from 'universal-cookie'
import GoogleLogin from 'react-google-login'
import moment from 'moment'
import { formatDateRegister } from 'helpers/utilsHelper'
import { eventListObjectTriggered } from 'helpers/dataLayer/lists'
import { LogotypeExtended } from '@components/Icons/LogotypeExtended'
class Register extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      password: '',
      re_password: '',
      password_validation: '',
      password_style: 'black',
      isPasswordValidate: false,
      showPassword: false,
      showRePassword: false,
      maxBirthDate: moment().subtract(18, 'years').format('YYYY-MM-DD')
    }

    library.add(faCheckCircle, faBell, faCommentDots, faEye, faEyeSlash)

    this.handleCheckPassword = this.handleCheckPassword.bind(this)
    this.handleRegisterUser = this.handleRegisterUser.bind(this)
    this.responseGoogle = this.responseGoogle.bind(this)
  }

  async handleRegisterUser(event) {
    event.preventDefault()
    const final_date_birthday = formatDateRegister(
      event.target.date_birthday.value
    )
    if (final_date_birthday.error) return

    if (event.target.password.value !== event.target.re_password.value) {
      toast.error(
        'Le mot de passe et le nouveau mot de passe ne correspondent pas.',
        {
          position: toast.POSITION.BOTTOM_LEFT
        }
      )
    } else if (this.state.isPasswordValidate == false) {
      toast.error(`Le mot de passe n'est pas valide`, {
        position: toast.POSITION.BOTTOM_LEFT
      })
    } else {
      let original_name = event.target.name.value
      let name =
        original_name.charAt(0).toUpperCase() +
        original_name.slice(1).toLowerCase()

      let original_lastname = event.target.lastname.value
      let lastname =
        original_lastname.charAt(0).toUpperCase() +
        original_lastname.slice(1).toLowerCase()
      const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/signup', {
        body: JSON.stringify({
          name: name,
          phone: event.target.phone.value,
          lastname: lastname,
          email: event.target.email.value,
          birthday: final_date_birthday.finalDate,
          password: event.target.password.value
        }),
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'POST'
      })

      const result = await res.json()
      const status = await res.status
      if (status == 201) {
        const cookies = new Cookies()
        cookies.set('email_validate_user', event.target.email.value)
        eventListObjectTriggered.signUp.callback()
        Router.push('/email/validate_email')
      } else if (status == 403) {
        toast.error(`L'utilisateur existe déjà`, {
          position: toast.POSITION.BOTTOM_LEFT
        })
      } else {
        toast.error(`Une erreur s'est produite, réessayez plus tard.`, {
          position: toast.POSITION.BOTTOM_LEFT
        })
      }
    }
  }

  async responseGoogle(response) {
    const res = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'auth/login-with-google',
      {
        body: JSON.stringify({
          google_token: response.tokenId
        }),
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'POST'
      }
    )
  }

  handleCheckPassword(event) {
    const password_value = event.target.value
    this.setState({ ...this.state, password: password_value }, () => {
      let strongRegex = new RegExp(
        '^(?=.{14,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$',
        'g'
      )
      let mediumRegex = new RegExp(
        '^(?=.{10,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$',
        'g'
      )
      let enoughRegex = new RegExp('(?=.{8,}).*', 'g')
      let pwd = this.state.password
      if (pwd.length == 0) {
        this.setState({
          ...this.state,
          password_validation: 'Type Password',
          password_style: 'red'
        })
      } else if (false == enoughRegex.test(pwd)) {
        this.setState({
          ...this.state,
          password_validation: 'Mot de passe trop court!',
          password_style: 'red'
        })
      } else if (strongRegex.test(pwd)) {
        this.setState({
          ...this.state,
          password_validation: 'FORT !',
          password_style: 'green',
          isPasswordValidate: true
        })
      } else if (mediumRegex.test(pwd)) {
        this.setState({
          ...this.state,
          password_validation: 'MOYEN !',
          password_style: 'orange',
          isPasswordValidate: true
        })
      } else {
        this.setState({
          ...this.state,
          password_validation: 'FAIBLE !',
          password_style: 'red'
        })
      }
    })
  }

  handleShowPassword = async () => {
    this.setState({ ...this.state, showPassword: !this.state.showPassword })
  }

  handleShowRePassword = async () => {
    this.setState({ ...this.state, showRePassword: !this.state.showRePassword })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid container-login__wrapper container-login__wrapper--register">
          <div className="row">
            <div className="col-12 col-xl-4 container-login-left">
              <Link href="/">
                <a>
                  <LogotypeExtended
                    fill="white"
                    className="container-login__logo--register"
                  />
                </a>
              </Link>
            </div>
            <div className="col-12 col-xl-8 container-fluid container-login container-login--register">
              <div className="card-white">
                <h1 className="mb-5 text-center">Inscription</h1>
                <form className="form" onSubmit={this.handleRegisterUser}>
                  <div className="row">
                    <div className="col-12 col-md-6 col-lg-6">
                      <h4 htmlFor="name">Prénom *</h4>
                      <input
                        type="text"
                        id="name"
                        name="name"
                        className="form-control"
                        placeholder="Saisissez votre Prénom"
                        required
                      />
                    </div>
                    <div className="col-12 col-md-6 col-lg-6">
                      <h4 htmlFor="lastname">Nom *</h4>
                      <input
                        type="text"
                        id="lastname"
                        name="lastname"
                        className="form-control"
                        placeholder="Saisissez votre Nom"
                        required
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <h4 htmlFor="email">Adresse email *</h4>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                      name="email"
                      aria-describedby="emailHelp"
                      placeholder="Saisissez votre adresse email"
                      required
                    />
                  </div>
                  <div className="form-group">
                    <h4 htmlFor="phone">Téléphone *</h4>
                    <input
                      type="number"
                      className="form-control"
                      id="phone"
                      name="phone"
                      aria-describedby="emailHelp"
                      placeholder="saisissez votre numéro de téléphone"
                      required
                    />
                  </div>
                  <div className="form-group">
                    <h4 htmlFor="date_birthday">Date de naissance *</h4>
                    <input
                      className="form-control container-onboarding__time-input"
                      type="date"
                      id="date_birthday"
                      name="date_birthday"
                      placeholder="YYYY-MM-DD"
                      max={this.state.maxBirthDate}
                      required
                    />
                  </div>
                  <div className="form-group">
                    <h4 htmlFor="password">Mot de passe *</h4>
                    <div className="password-input">
                      {this.state.showPassword ? (
                        <FontAwesomeIcon
                          icon={['fas', 'eye-slash']}
                          size="1x"
                          className="icon-eye"
                          onClick={this.handleShowPassword}
                        />
                      ) : (
                        <FontAwesomeIcon
                          icon={['fas', 'eye']}
                          size="1x"
                          className="icon-eye"
                          onClick={this.handleShowPassword}
                        />
                      )}
                      <input
                        type={this.state.showPassword ? 'text' : 'password'}
                        className="form-control"
                        id="password"
                        name="password"
                        placeholder="Saisissez votre mot de passe"
                        onChange={this.handleCheckPassword}
                        defaultValue={this.state.password}
                        value={this.state.password}
                        required
                      />
                    </div>
                  </div>
                  <div className="form-group">
                    <h4 htmlFor="re_password">
                      Confirmer le nouveau mot de passe *
                    </h4>
                    <div className="password-input">
                      {this.state.showRePassword ? (
                        <FontAwesomeIcon
                          icon={['fas', 'eye-slash']}
                          size="1x"
                          className="icon-eye"
                          onClick={this.handleShowRePassword}
                        />
                      ) : (
                        <FontAwesomeIcon
                          icon={['fas', 'eye']}
                          size="1x"
                          className="icon-eye"
                          onClick={this.handleShowRePassword}
                        />
                      )}
                      <input
                        type={this.state.showRePassword ? 'text' : 'password'}
                        className="form-control"
                        id="re_password"
                        name="re_password"
                        placeholder="Répétez votre mot de passe"
                        defaultValue={this.state.re_password}
                        required
                      />
                    </div>
                  </div>
                  <div className="form-group card-background-gray">
                    <div className="container-card-gray">
                      <p>
                        Fiabilité du mot de passe :{' '}
                        <strong>
                          <span style={{ color: this.state.password_style }}>
                            {this.state.password_validation}
                          </span>
                        </strong>
                      </p>
                      <ul>
                        <li className="d-flex align-items-center">
                          <FontAwesomeIcon
                            icon="check-circle"
                            size="1x"
                            className="icon-check"
                          />{' '}
                          Ne doit pas contenir votre nom ni votre adresse e-mail
                        </li>
                        <li className="d-flex align-items-center">
                          <FontAwesomeIcon
                            icon="check-circle"
                            size="1x"
                            className="icon-check"
                          />{' '}
                          Au moins 8 caractères
                        </li>
                        <li className="d-flex align-items-center">
                          <FontAwesomeIcon
                            icon="check-circle"
                            size="1x"
                            className="icon-check"
                          />{' '}
                          Contient un chiffre ou un symbole
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="form-check">
                    <input
                      type="checkbox"
                      className="form-check-input input-check-blue-dark"
                      id="conditions"
                      required
                    />
                    <label
                      className="ml-2 form-check-label text-justify"
                      htmlFor="conditions"
                    >
                      J'accepte les <a>conditions d'utilisation</a> et la{' '}
                      <a>politique de confidentailité</a> des données et
                      confirme la véracité des informations ci dessus.*
                    </label>
                  </div>
                  <button
                    type="submit"
                    className="button button-long mb-2 register-button"
                  >
                    S'inscrire
                  </button>
                  <div className="form-group">
                    <a className="col-12 card-button-networks text-center button button-long form-control">
                      <GoogleLogin
                        clientId={process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID}
                        buttonText="Connection via Google"
                        render={(renderProps) => (
                          <>
                            <img
                              src="/images/logo-google.png"
                              alt="Google Logo"
                            />
                            <button
                              onClick={renderProps.onClick}
                              disabled={renderProps.disabled}
                              style={{
                                border: 'none',
                                backgroundColor: 'transparent'
                              }}
                            >
                              Login with Google
                            </button>
                          </>
                        )}
                        onSuccess={this.responseGoogle}
                        onFailure={this.responseGoogle}
                        cookiePolicy={'single_host_origin'}
                      />
                    </a>
                  </div>
                </form>
                <hr />
                <div className="form-group d-flex justify-content-center">
                  <p className="text-center">Vous avez deja un compte?</p>
                </div>
                <div className="form-group d-flex justify-content-center">
                  <Link href="/login">
                    <a className="text-center link-black">Se connecter</a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

export default Register
