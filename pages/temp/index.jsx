import FullLoading from '@components/FullLoading/FullLoading'
import { setFullLoading } from '@ducks/Config/actions'
import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

export default function Temp() {
  const dispatch = useDispatch()
  const router = useRouter()
  const { url } = useSelector((store) => store.tempPage)

  useEffect(() => {
    dispatch(setFullLoading(true))
  }, [])

  useEffect(() => {
    const checkUrl = () => {
      if (url) {
        window.open(url, '_self')
      } else {
        if (typeof window !== undefined) {
          window.location.assign(window.location.origin)
        }
      }
    }

    const timer = setTimeout(() => {
      checkUrl()
    }, 100)

    return () => clearTimeout(timer)
  }, [url])

  return <FullLoading />
}
