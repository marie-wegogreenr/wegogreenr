import React, { useEffect } from 'react'
import { useRouter } from 'next/router'
import { getTokenFromCookie } from './../../utils/getTokenFromCookie'
import { useToast } from 'hooks/useToast'
import { Loader } from 'components/'

const index = () => {
  const [notify] = useToast()
  const router = useRouter()
  function handleStripeVerification() {
    const { code, state } = router.query

    const token = getTokenFromCookie()
    const form = {
      code,
      state
    }

    const options = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(form),
      method: 'POST'
    }

    fetch(`${process.env.NEXT_PUBLIC_API_URL}stripe`, options)
      .then((res) => res.json())
      .then((data) => {
        if (!data.state || !data.code) {
          notify('compte validé avec succès', 'success')
          router.push('/host/dashboard')
        }
      })
      .catch((err) => {
        notify(`échec de la création d'un compte`, 'error')
        router.push('/host/dashboard')
      })
  }

  useEffect(() => {
    if (typeof window !== 'undefined' && router.query) {
      handleStripeVerification()
    }
  }, [router.query])

  return (
    <div className="host-stripe">
      <p className="host-stripe__text">Validation de votre compte...</p>
      <Loader />
    </div>
  )
}

export default index
