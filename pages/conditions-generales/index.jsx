import Container from '@components/container'
import React from 'react'
import { consoleLog } from '/utils/logConsole'

const index = () => {
  return (
    <Container>
      <div style={{ paddingTop: '7rem !important' }}>
        <div className="container-xxl">
          <h1 className="h1 weight-bold">Conditions générales d'utilisation</h1>
          <p>
            COMSi vous êtes ici, c’est que vous avez vraiment envie de lire nos
            mentions légales« vous pourrez dire à vos amisque vous l’avez fait
            et que vous rtes donc exceptionnel ;)
            <br />
            En espérant que vous trouverez ce que vous y cherchez et que vous en
            sortirez indemne et satisfait :)
          </p>
          <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
            Date de dernière mise à jour : 29/04/2020
          </h2>
          <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
            1 - PRÉSENTATION DU SITE
          </h2>
          <p className="text mt-3">
            Conformément aux dispositions de la loi n° 2004-575 du 21 juin 2004
            pour la confiance en l'économienumérique, il est précisé aux
            utilisateurs du site www.wegogreenr.com l'identité des différents
            intervenants dansle cadre de sa réalisation et de son suivi.
          </p>

          <ul>
            <li>
              <a className="weight-bold">Propriétaire :</a> WE GO GREENR - SAS
              au capital de 10 000 euros - inscrite au R.C.S. de Bordeaux -SIRET
              881 550 883 - 108 Cours du Médoc - 33300 Bordeaux - France
            </li>
            <li>
              <a className="weight-bold">N° TVA intracommunautaire :</a> FR 41
              881550883
            </li>
            <li>
              <a className="weight-bold">Représenté par :</a> Stéphane VINCENT,
              président
            </li>
            <li>
              <a className="weight-bold">Directeur de la publication :</a>{' '}
              Stéphane VINCENT -
              <a className="color-black" href="mailto:contact@wegogreenr.com">
                contact@wegogreenr.com
              </a>
            </li>
            <li>
              <a className="weight-bold">Hébergeur :</a> Amazon Web Services LLC
              - P.O. Box 81226 - Seattle WA 98108-1226 - USA -{' '}
              <a href="https://aws.amazon.com" target="_blank">
                https://aws.amazon.com
              </a>
            </li>
          </ul>

          <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
            2 - DEFINITIONS
          </h2>

          <ul>
            <li>
              <a className="weight-bold">Annonce</a> : désigne l'ensemble des
              éléments et données (visuelles, textuelles, sonores,
              photographies,dessins), déposé par un Annonceur sous sa
              responsabilité éditoriale, en vue de proposer à la location
              unservice d’hébergement touristique et/ou d’activité
              écoresponsables, diffusé sur le Plateforme.
            </li>
            <li>
              <a className="weight-bold">Annonceur</a> : désigne toute personne
              physique majeure ou personne morale établie en France (inclus
              lesDOM-TOM), ayant déposé et mis en ligne une Annonce via le
              Service
            </li>
            <li>
              <a className="weight-bold">CGU</a> : les présentes Conditions
              Générales d'Utilisation du Service WE GO GREENR
            </li>
            <li>
              <a className="weight-bold">Service</a> : désigne les services WE
              GO GREENR mis à la disposition des Utilisateurs et des
              Annonceurssur le Plateforme
            </li>
            <li>
              <a className="weight-bold">
                Service de Réservation et de Paiement
              </a>{' '}
              : désigne le service de paiement en ligne des
              réservationsd’hébergements écoresponsables mis à disposition des
              Annonceurs et des Utilisateurs sur le Site
            </li>
            <li>
              <a className="weight-bold">Société :</a> désigne la société qui
              édite et exploite le Plateforme : WE GO GREENR - SAS au capital de
              10 000 euros - inscrite au R.C.S. de Bordeaux - SIRET 881 550 883
              - 108 Cours du Médoc - 33300Bordeaux - France
            </li>
            <li>
              <a className="weight-bold">Plateforme </a> : désigne le site
              internet exploité par WE GO GREENR SAS accessible
              principalementdepuis l'URL www.wegogreenr.com et permettant aux
              Utilisateurs et aux Annonceurs d'accéder viainternet au Service.
            </li>
            <li>
              <a className="weight-bold">Utilisateur </a> : désigne tout
              visiteur, ayant accès au Service via le Plateforme et consultant
              le Service,accessible depuis les différents supports.
            </li>
          </ul>

          <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
            3 - OBJET ET ACCEPTATION
          </h2>

          <p className="text mt-3">
            Les CGU sont mises à la disposition des clients et utilisateurs de
            la Plateforme où elles sont directementconsultables et peuvent
            également lui être communiquées sur simple demande par tout
            moyen.Tout Utilisateur et tout Annonceur déclare en accédant et en
            utilisant le Service depuis la Plateforme, avoir prisconnaissance
            des présentes Conditions Générales d’Utilisation et les accepter
            expressément sans réserve et/oumodification de quelque nature que ce
            soit. Les présentes CGU sont donc pleinement opposables
            auxUtilisateurs et aux Annonceurs. Ces conditions d’utilisation sont
            susceptibles d’rtre modifiées ou complétées àtout moment. Les
            utilisateurs du site sont donc invités à les consulter de manière
            régulière. Le site est mis à jourrégulièrement par la direction.
          </p>

          <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
            4 - UTILISATION DU SERVICE WE GO GREENR
          </h2>

          <p className="text mt-3">
            La Société édite et exploite une plateforme de mise en relation par
            voie électronique accessible sur un siteinternet notamment à
            l’adresse www.wegogreenr.com ou sous forme d’application mobile, et
            destinée à mettreen relation :
          </p>

          <p className="text mt-3 ps-3">
            d’une part, des Utilisateurs souhaitant séjourner une ou plusieurs
            nuitées au sein d’un hébergementengagé dans une démarche
            écoresponsable, et/ou souhaitant découvrir des activités
            écoresponsables,
            <br />
            <br />
            d’autre part, des Annonceurs engagés dans une démarche
            écoresponsable qui proposent aux Utilisateursdes hébergements
            écoresponsables et/ou des activités écoresponsables.
          </p>

          <p className="text mt-3">
            Les Utilisateurs peuvent utiliser la plateforme de la Société pour
            réserver des nuitées dans un hébergement écoresponsable sans pour
            autant réserver des activités écoresponsables dans le même temps, et
            inversement.Les Annonceurs proposant des hébergements
            écoresponsables n’ont aucune obligation à proposer des
            activitésécoresponsables en complément, et inversement.
          </p>

          <p className="text mt-3 ps-5">
            <i>
              L’article242 bis du Code Général des Impôts définit les
              plateformes de mise enrelation par voie électronique comme étant «
              les entreprises, quel que soit leur lieud’établissement, qui
              mettent en relation à distance, par voie électronique,
              despersonnes en vue de la vente d’un bien, de la fourniture d’un
              service ou de l’échangeou du partage d’un bien ou d’un service ».
            </i>
          </p>

          <p className="text mt-3">
            L’objectif poursuivi par la Société consiste à permettre à un
            Utilisateur de réserver un hébergement écoresponsable auprès d’un
            Hébergeur lui assurant une prestation de services d’hébergement,
            et/ou de réserverune activité ou expérience écoresponsable auprès
            d’un Porteur lui assurant une prestation de services autourde cette
            activité ou de cette expérience, conformes aux descriptions rédigées
            sur la Plateforme par l’Hébergeuret le Porteur. La Société permet
            ainsi à l’Hébergeur de recevoir et exécuter des demandes de
            servicesd’hébergement provenant d’un Utilisateur utilisant la
            Plateforme exploitée par la Société. Il convient, dès lors, àchaque
            Hébergeur et chaque Porteur de s’assurer qu’il est en conformité
            avec la loi et qu’il bénéficie desassurances couvrant le service
            qu’il propose. L’Hébergeur signe un contrat avec la Société et
            reconnaîtexpressément souhaiter accéder et utiliser les services de
            la Société afin d’rtre mis en relation avec desUtilisateurs à
            travers la Plateforme fournie par la Société.
            <br />
            <br />
            En sa qualité d’intermédiaire, la Société ne fournit aucun service
            d’hébergement, de quelque nature qu’il soit, etn’agit pas en qualité
            d’hébergeur, le rôle de la Société étant de faciliter l’accès à la
            Plateforme et de mettre enrelation Utilisateurs, Hébergeurs et
            Porteurs.
            <br />
            <br />
            WE GO GREENR s’efforce de fournir sur le site des informations aussi
            précises que possible. Toutefois, il nepourra rtre tenu responsable
            des omissions, des inexactitudes ou des carences dans la mise à
            jour, qu’ellessoient de son fait ou du fait des tiers partenaires
            qui lui fournissent ces informations.
            <br />
            <br />
            Si un Annonceur souhaite retirer son annonce du site, nous pouvons
            temporairement la désactiver puis laréactiver à une date ultérieure
            sur simple demande écrite à hebergeurs@wegogreenr.com.
            <br />
            <br />
            WE GO GREENR se réserve le droit de supprimer une annonce du site si
            des plaintes récurrentes et fondéessont reçues d’Utilisateurs ou
            pour tout autre motif à sa discrétion.
            <br />
            <br />
            Les CGU ont pour objet de définir les modalités et conditions dans
            lesquelles les Utilisateurs accèdent et utilisentle Service. Les
            présentes CGU ne s’appliquent pas aux réservations effectuées entre
            Utilisateurs et Annonceursdirectement sans passer par le Service, ni
            celles effectués sur le site www.abracadaroom.com.
            <br />
            <br />
            Les CGU ne régissent pas les relations entre les Annonceurs et les
            Utilisateurs mais seulement entre WE GOGREENR et les Utilisateurs.
            WE GO GREENR n’est pas partie au contrat de location conclu entre
            l’Annonceur etl’Utilisateur et n’a pas d’autre activité que de
            proposer ce Service de mise en relation et de réservation.
          </p>

          <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
            5 - UTILISATION DU SERVICE DE RESERVATION ET DE PAIEMENT
          </h2>

          <p className="text mt-3">
            WE GO GREENR met à disposition des Utilisateurs un Service de
            Réservation et de Paiement accessible sur saPlateforme pour
            permettre à des Utilisateurs de réserver des hébergements
            écoresponsables auprès desAnnonceurs ayant publié une annonce sur le
            site internet wegogreenr.com.WE GO GREENR ne met pas à disposition
            des Utilisateurs le Service de Réservation et de Paiement
            accessiblesur sa Plateforme pour permettre à des Utilisateurs de
            réserver des activités écoresponsables auprès desAnnonceurs ayant
            publié une annonce sur le site internet wegogreenr.com.
          </p>

          <p className="text mt-3 ps-3">
            Pour utiliser le Service de Réservation et de Paiement, l’Annonceur
            devra
            <ul>
              <li> Être âgé de plus de 18 ans</li>
              <li> Communiquer sa véritable identité</li>
              <li> Télécharger ses papiers d’identité en cours de validité</li>
              <li> Renseigner ses coordonnées bancaires (IBAN)</li>
              <li>
                Avoir complété en amont l’entièreté de notre parcours KYC («
                Know Your Customer »)
              </li>
            </ul>
            Pour utiliser le Service de Réservation et de Paiement,
            l’Utilisateur devra :
            <ul>
              <li>Être âgé de plus de 18 ans</li>
              <li> Communiquer sa véritable identité (nom, prénom)</li>
              <li>
                Renseigner ses données de carte bancaire en cours de validité.
              </li>
            </ul>
          </p>

          <p className="text mt-3">
            Les CGU ne régissent pas les relations entre les Annonceurs et les
            Utilisateurs mais seulement entre WE GOGREENR et les Utilisateurs.
            WE GO GREENR n’est pas partie au contrat de location conclu entre
            l’Annonceur etl’Utilisateur et n’a pas d’autre activité que de
            proposer ce Service de mise en relation et de réservation.
            <br />
            <br />
            WE GO GREENR propose un Service de Réservation permettant aux
            Utilisateurs de réserver des hébergementsécoresponsables auprès des
            Annonceurs. La relation contractuelle est uniquement établie entre
            l’Annonceur etl’Utilisateur, sans intervention directe de WE GO
            GREENR.
          </p>

          <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
            6 - CONDITIONS DE PAIEMENT, DE NON-PRESENTATION, D'ANNULATION ET DE
            REMBOURSEMENT
          </h2>

          <p className="text mt-3">
            L’Utilisateur reconnait expressément que toute réservation effectuée
            via la Plateforme est une commande avecobligation de paiement, qui
            nécessite le paiement d’un prix contre la fourniture du Service
            commandé.L’Utilisateur est informé que la mise à disposition du
            Service ne pourra être effectuée avant le parfaitencaissement par la
            Société des sommes dues par l’Utilisateur et la réception sous24h
            d’un email deconfirmation de réservation à la suite du paiement.
            <br />
            <br />
            Le prix des Services en vigueur lors de la commande est indiqué en
            euros toutes taxes comprises (TTC) sur laPlateforme. Le prix est
            payable en euros (€) exclusivement. Le montant total dû par
            l’Utilisateur et son détail sontindiqués sur la page de confirmation
            de commande ou le mail de paiement, le cas échéant.
            <br />
            <br />
            Les conditions d’annulation, de modification ou de remboursement
            sont variables en fonction de la prestationchoisie et de chaque
            Annonceur. Vous pouvez consulter les conditions générales de vente
            de nos Annonceursdirectement sur leur site ou sur simple demande à
            notre service de réservation (reservations@wegogreenr.com)avant de
            procéder au règlement de votre séjour. Elles sont également
            rappelées, dans la mesure du possible, aucours du processus de
            réservation et dans l’e-mail et/ou le billet de confirmation.
            <br />
            <br />
            En cas d’annulation par l’Utilisateur, celui-ci sera remboursé du
            montant de la location selon les conditions del’Annonceur concerné,
            déduit des frais de services de la Plateforme.
            <br />
            <br />
            En cas d’annulation par l’Annonceur, l’Utilisateur sera remboursé
            intégralement du montant de la location parl’Annonceur. En outre,
            l’Annonceur devra s’acquitter des frais de services de la
            Plateforme, sauf cas de forcemajeurs reconnus par sa police
            d’assurance.
            <br />
            <br />
            En cas d'arrivée tardive le jour prévu de l'enregistrement auprès de
            l’Annonceur, vous vous engagez à eninformer l’Annonceur le plus
            rapidement possible afin qu'il soit mis au courant et n'annule pas
            votre Réservation,ni ne vous facture au motif d’une
            non-présentation. Vous reconnaissez et acceptez que la Société n'est
            en aucuncas tenue responsable des conséquences liées à votre arrivée
            tardive, ou de toute facturation d'annulation ou denon-présentation
            réalisée par l’Annonceur.
            <br />
            <br />
            Le droit de rétractation ne s'applique pas dans le cadre de
            prestations de services d'hébergements dont la datede réservation
            est définie, conformément à l'article L221-28 du code de la
            consommation.
            <br />
            <br />
            Concernant les bons et les cartes cadeaux, l’Utilisateur est informé
            qu'il bénéficie d’un délai de 14 jour franc afind'exercer son droit
            de rétractation (article L121-20 du code de la consommation). Passé
            ce délai ou en cas deconsommation partielle de la carte cadeau,
            aucun remboursement ne pourra être requis.
          </p>

          <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
            7 - CONDITIONS DE RESERVATIONS, DE PAIEMENT, D'ANNULATION ET DE
            REMBOURSEMENT CHEZNOTRE PARTENAIRE ABRACADAROOM.COM
          </h2>

          <p className="text mt-3">
            L’Utilisateur reconnait expressément que toute réservation effectuée
            via notre partenaire Abracadaroom.comimplique l’acceptation pleine
            et entière des CGU du site Abracadaroom.com. L’Utilisateur est alors
            un client dusite Abracadaroom et le suivi de sa réservation et
            toutes autres actions seront gérés par les équipesd’Abracadaroom
            (paiement, confirmation, annulations«).
          </p>

          <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
            8 - PROPRIÉTÉ INTELLECTUELLE ET CONTREFAÇONS
          </h2>

          <p className="text mt-3">
            WE GO GREENR est propriétaire des droits de propriété intellectuelle
            ou détient les droits d’usage sur tous leséléments accessibles sur
            la Plateforme, notamment et sans limitation, tous textes, fichiers,
            images animées ounon, photographies, vidéos, logos, dessins,
            modèles, logiciels, marques, identité visuelle, base de
            données,structure de la Plateforme et tous autres éléments de
            propriété intellectuelle et autres données ou informations(ci-après,
            les« Éléments ») qui sont protégés par les lois et règlements
            français et internationaux relatifsnotamment à la propriété
            intellectuelle.
            <br />
            <br />
            En conséquence, aucun des Éléments de la Plateforme ne pourra en
            tout ou partie être modifié, reproduit, copié,dupliqué, vendu,
            revendu, transmis, publié, communiqué, distribué, diffusé,
            représenté, stocké, utilisé, loué ouexploité de toute autre manière,
            à titre gratuit ou onéreux, par un Utilisateur ou par un tiers, quel
            que soient lesmoyens et/ou les supports utilisés, qu’ils soient
            connus ou inconnus à ce jour, sans l’autorisation préalableexprès et
            écrite de la Société au cas par cas, et l’Utilisateur est seul
            responsable de toute utilisation et/ouexploitation non autorisée.
            <br />
            <br />
            Par ailleurs, il est précisé que la Société n’est pas propriétaire
            du contenu mis en ligne par les Utilisateurs, pourlequel ces
            derniers demeurent intégralement responsables et garantissent la
            Société contre tout recours à cetitre. Les Utilisateurs accordent à
            la Société une licence non-exclusive transférable, sous-licenciable,
            à titregratuit et mondiale pour l’utilisation des contenus de
            propriété intellectuelle qu’ils publient sur la Plateforme,
            pourtoute la durée de protection de ces contenus.
            <br />
            <br />
            La Société se réserve la possibilité de saisir toutes voies de droit
            à l’encontre des personnes qui n’auraient pasrespecté les
            interdictions contenues dans le présent article.
            <br />
            <br />
            We Go GreenR® et le logo We Go GreenR® sont des marques déposées de
            WE GO GREENR.
          </p>

          <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
            9 - LIMITATIONS DE RESPONSABILITE
          </h2>

          <p className="text mt-3">
            La Société déclare avoir souscrit une assurance garantissant sa
            responsabilité professionnelle et contractuelle.La Société ne
            saurait être tenu pour responsable de l'inexécution du contrat du
            fait de l’Utilisateur ou en raisond'un événement qualifié de force
            majeure par les tribunaux compétents ou encore du fait imprévisible
            etinsurmontable de tout tiers aux présentes.
            <br />
            <br />
            La Société ne peut être tenu pour responsable des informations
            importées, stockées et/ou publiées sur laPlateforme par les
            Utilisateurs. La Société ne peut être tenu pour responsable au titre
            de toute informationpubliée par un Utilisateur sur la Plateforme et
            des dommages directs ou indirects que cette utilisation
            pourraitcauser à un tiers, l’Utilisateur à l'origine de la
            publication restant seul responsable à ce titre.
            <br />
            <br />
            La Société ne pourra être tenue responsable des dommages directs et
            indirects causés au matériel du visiteur,lors de l’accès au site, et
            résultant soit de l’utilisation d’un matériel ne répondant pas aux
            spécifications indiquéesdans la section « Limitations contractuelles
            sur les données techniques », soit de l’apparition d’un bug ou
            d’uneincompatibilité.
            <br />
            <br />
            L’Utilisateur reconnaît que les caractéristiques et les contraintes
            d'Internet ne permettent pas de garantir lasécurité, la
            disponibilité et l'intégrité des transmissions de données sur
            Internet. Ainsi, la Société ne garantit pasque la Plateforme et ses
            services fonctionneront sans interruption ni erreur de
            fonctionnement. En particulier, leurexploitation pourra être
            momentanément interrompue pour cause de maintenance, de mises à jour
            oud'améliorations techniques, ou pour en faire évoluer le contenu
            et/ou leur présentation.
            <br />
            <br />
            La Société ne peut être tenu pour responsable de l'utilisation qui
            serait faite de la Plateforme et de ses servicespar les Utilisateurs
            en violation des présentes Conditions Générales et des dommages
            directs ou indirects quecette utilisation pourrait causer à un
            Utilisateur ou à un tiers. En particulier, la Société ne peut être
            tenu pourresponsable des fausses déclarations faites par un
            Utilisateur et de son comportement vis-à-vis des tiers. Dansle cas
            où la responsabilité de la Société serait recherchée à raison d'un
            tel comportement d’un de sesUtilisateurs, ce dernier s'engage à
            garantir la Société contre toute condamnation prononcée à son
            encontre ainsiqu’à rembourser la Société de l’ensemble des frais,
            notamment les honoraires d’avocats, engagés pour
            sadéfense.L’Utilisateur est seul responsable de l'intégralité des
            contenus qu'il met en ligne sur la Plateforme, dont il
            déclareexpressément disposer de l'intégralité des droits, et
            garantit à ce titre la Société qu'il ne met pas en ligne decontenus
            violant des droits tiers, notamment de propriété intellectuelle, ou
            constituant une atteinte aux personnes(notamment diffamation,
            insultes, injures, etc.), au respect de la vie privée, une atteinte
            à l'ordre public et auxbonnes mœurs (notamment, apologie des crimes
            contre l'humanité, incitation à la haine raciale,
            pornographieenfantine, etc.). En cas d'atteinte aux lois en vigueur,
            aux bonnes mœurs ou aux présentes ConditionsGénérales, la Société
            peut exclure de plein droit les Utilisateurs qui se seront rendus
            coupables de tellesinfractions et supprimer des informations et
            renvois à ces contenus litigieux. La Société est qualifié
            d’hébergeurs’agissant du contenu mis en ligne par des tiers. A ce
            titre, il est rappelé que La Société n’a aucune obligationgénérale
            de surveillance du contenu transmis ou stocké via la Plateforme.
            Dans le cas où la responsabilité de laSociété serait recherchée à
            raison d'un contenu mis en ligne par l’Utilisateur, ce dernier
            s'engage à garantir laSociété contre toute condamnation prononcée à
            son encontre ainsi qu’à rembourser la Société de l’ensembledes
            frais, notamment les honoraires d’avocats, engagés pour sa défense.
            <br />
            <br />
            L’Hébergeur est seul responsable de l'intégralité des contenus qu'il
            met en ligne sur la Plateforme, dont il déclareexpressément disposer
            de l'intégralité des droits, et garantit à ce titre la Société qu'il
            ne met pas en ligne decontenus violant des droits tiers, notamment
            de propriété intellectuelle, ou constituant une atteinte aux
            personnes(notamment diffamation, insultes, injures, etc.), au
            respect de la vie privée, une atteinte à l'ordre public et auxbonnes
            mœurs (notamment, apologie des crimes contre l'humanité, incitation
            à la haine raciale, pornographieenfantine, etc.). En cas d'atteinte
            aux lois en vigueur, aux bonnes mœurs ou aux présentes
            ConditionsGénérales, la Société peut exclure de plein droit les
            Hébergeurs qui se seront rendus coupables de tellesinfractions et
            supprimer des informations et renvois à ces contenus litigieux. La
            Société est qualifié d’hébergeurs’agissant du contenu mis en ligne
            par des tiers. A ce titre, il est rappelé que La Société n’a aucune
            obligationgénérale de surveillance du contenu transmis ou stocké via
            la Plateforme. Dans le cas où la responsabilité de laSociété serait
            recherchée à raison d'un contenu mis en ligne par l’Hébergeur, ce
            dernier s'engage à garantir laSociété contre toute condamnation
            prononcée à son encontre ainsi qu’à rembourser la Société de
            l’ensembledes frais, notamment les honoraires d’avocats, engagés
            pour sa défense.
          </p>

          <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
            10 - DONNEES PERSONNELLES
          </h2>

          <p className="text mt-3">
            Le traitement de vos données à caractère personnel est régi par
            notre Charte du respect de la vie privée,consultable à tout moment
            sur notre Plateforme, conformément au Règlement Général sur la
            Protection desDonnées 2016/679 du 27 avril 2016 (« RGPD »).
          </p>

          <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
            11 - COOKIES ET LIENS HYPERTEXTE
          </h2>

          <p className="text mt-3">
            La navigation sur le site est susceptible de provoquer
            l’installation de cookie(s) sur l’ordinateur de l’utilisateur.Pour
            plus d’informations, vous pouvez vous reporter à notre Charte du
            respect de la vie privée, consultable àtout moment sur notre
            Plateforme.
            <br />
            <br />
            Les liens hypertextes disponibles sur la Plateforme peuvent renvoyer
            vers des sites tiers non édités par laSociété. Ils sont fournis
            uniquement pour la convenance de l’Utilisateur, afin de faciliter
            l’utilisation desressources disponibles sur l’Internet. Si
            l’Utilisateur utilise ces liens, il quittera la Plateforme et
            acceptera alorsd’utiliser les sites tiers à ses risques et périls ou
            le cas échéant conformément aux conditions qui les
            régissent.L’Utilisateur reconnait que la Société ne contrôle ni ne
            contribue en aucune manière à l’élaboration desconditions
            d’utilisation et/ou du contenu s’appliquant à ou figurant sur ces
            sites tiers. En conséquence, la Sociéténe saurait être tenu
            responsable de quelque façon que ce soit du fait de ces liens
            hypertextes. L’Utilisateur nepourra pas utiliser et/ou insérer de
            lien hypertexte pointant vers le site sans l’accord écrit et
            préalable de laSociété au cas par cas.
          </p>

          <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
            12 - LITIGES ET CONTESTATIONS
          </h2>

          <p className="text mt-3">
            Loi n°78-87 du 6 janvier 1978, notamment modifiée par la loi n°
            2004-801 du 6 août 2004 relative àl’informatique, aux fichiers et
            aux libertés.
            <br />
            <br />
            Loi n° 2004-575 du 21 juin 2004 pour la confiance dans l’économie
            numérique.
            <br />
            <br />
            Ces Conditions Générales sont régies, interprétées et appliquées
            conformément au droit français. Tout litige enrelation avec
            l’utilisation du site sera réglé à l'amiable entre les parties.
            <br />
            <br />
            A défaut d'accord à l’amiable, les différends seront soumis à la
            médiation. Les Parties conviennent dès àprésent et de manière
            irrévocable de confier cette mission de médiation à CMB (Centre de
            médiation deBordeaux, 17 place de la Bourse, CS 61274, 33076
            Bordeaux Cedex). Le centre de médiation sera saisi à lademande de
            l’une des Parties ou conjointement par lettre recommandée avec
            accusé de réception. Copie decette lettre sera adressée le mrme jour
            et dans la mrme forme à l’autre partie. Dans les 8 jours de sa
            saisine,le centre soumettra par tout moyen (courrier, mail, «) à
            l’agrément des parties le nom d’un médiateur figurantsur sa liste.
            Les parties s’engagent à répondre à cette proposition et à coopérer
            activement au processus demédiation. Si au terme d’un délai de 8
            jours, les parties n’arrivaient pas à se mettre d’accord sur le
            choix dumédiateur, le médiateur sera désigné par Monsieur le
            Président du Tribunal de commerce compétent statuanten référé à la
            requête de la partie la plus diligente. Les frais et honoraires du
            médiateur, ainsi que les autresfrais et honoraires occasionnés par
            la mission de médiation seront supportés à part égale par les
            parties, saufmeilleur accord conclu entre elles.
            <br />
            <br />A défaut d’accord trouvé en médiation, les Parties
            retrouveront toute liberté afin d’user des voies de droit quileur
            sont ouvertes. Le différend sera soumis à la juridiction des
            tribunaux de la cour d’Appel compétente.
          </p>

          <h2 className="h2-dashboard weight-bold mt-5 font-montserrat lh-12">
            13 - DISPOSITIONS GENERALES
          </h2>

          <p className="text mt-3 mb-5">
            La Société se réserve le droit de modifier à tout moment les
            présentes Conditions Générales, auquel cas la date« Dernière mise à
            jour » figurant en haut des Conditions sera modifiée par nos soins.
            <br />
            <br />
            La Société se réserve le droit de modifier à tout moment et sans
            préavis le contenu de la Plateforme ou desservices qui y sont
            disponibles, et/ou de cesser de manière temporaire ou définitive
            d’exploiter tout ou partie dela Plateforme.
            <br />
            <br />
            En outre, la Société se réserve le droit de modifier à tout moment
            et sans préavis la localisation de la Plateformesur l’Internet,
            ainsi que les présentes Conditions Générales. L’Utilisateur est donc
            tenu par conséquent de sereporter aux présentes Conditions Générales
            avant toute utilisation de la Plateforme.
            <br />
            <br />
            L’Utilisateur reconnait que la Société ne saurait être tenu
            responsable de quelque manière que ce soit envers luiou tout tiers
            du fait de ces modifications, suspensions ou cessations.
            <br />
            <br />
            La Société conseille à l’Utilisateur de sauvegarder et/ou imprimer
            les présentes Conditions Générales pour uneconservation sûre et
            durable, et pouvoir ainsi les invoquer à tout moment pendant
            l'exécution du contrat sibesoin.
            <br />
            <br />
            En cas de litige, vous devez vous adresser en priorité au service
            client de l'entreprise aux coordonnéessuivantes:
            reservations@wegogreenr.com.
            <br />
            <br />
            L’Utilisateur reconnait avoir lu attentivement les présentes
            Conditions Générales. En s’inscrivant sur laPlateforme,
            l’Utilisateur confirme avoir pris connaissance des Conditions
            Générales et les accepter, le rendantcontractuellement lié par les
            termes des présentes Conditions Générales.
          </p>
        </div>
      </div>
    </Container>
  )
}

export default index
