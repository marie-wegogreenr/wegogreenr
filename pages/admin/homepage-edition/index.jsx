import React from 'react'
import AdminLayout from '@components/layouts/AdminLayout'
import { enabledEditionTools } from '@ducks/customizable/actions'
import { useSelector, useDispatch } from 'react-redux'
import { useRouter } from 'next/router'
import { setAdminTab } from '@ducks/Config/actions'

const HomepageEdition = () => {
  const dispatch = useDispatch()
  const router = useRouter()

  React.useEffect(async () => {
    dispatch(setAdminTab('homepage-edition'))
  }, [])

  const handleInitEdition = () => {
    dispatch(enabledEditionTools())
    router.push('/')
  }
  return (
    <AdminLayout title="Outils d'édition">
      <div
        style={{
          width: '100%',
          height: '100%',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          alignContent: 'center',
          flexDirection: 'column'
        }}
      >
        <h2>Modifier le contenu du site</h2>
        <img
          src={require('public/images/homepage-edition.gif')}
          style={{ width: '50%', marginTop: '20px' }}
        />
        <button
          style={{
            display: 'flex',
            justifyContent: 'center',
            borderRadius: '10px',
            backgroundColor: '#437558',
            borderStyle: 'none',
            marginTop: '20px'
          }}
          onClick={() => handleInitEdition()}
        >
          <span
            style={{
              padding: '10px 20px',
              color: 'white',
              fontWeight: 700
            }}
          >
            Commencer l'édition
          </span>
        </button>
      </div>
    </AdminLayout>
  )
}

export default HomepageEdition
