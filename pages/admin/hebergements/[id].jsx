import AdminLayout from '@components/layouts/AdminLayout'
import React, { useEffect, useState } from 'react'
import { useRouter, withRouter } from 'next/router'
import { getHebergement } from 'services/hebergementsService'
import { tabs } from 'fixtures/hebergementTabs'
import Link from 'next/link'
import { connect } from 'react-redux'
import { setHebergement } from '@ducks/hebergements/actions'
import Summary from '@components/dashboard_host/hebergements/summary'
import { useDispatch } from 'react-redux'
import { setAdminTab } from '@ducks/Config/actions'

function single() {
  const dispatch = useDispatch()
  const router = useRouter()
  const hebergementId = router.query.id
  const [hebergement, sethebergement] = useState(null)
  const [mainImage, setmainImage] = useState(null)
  const [idTab, setIdtab] = useState('')
  useEffect(async () => {
    if (hebergementId !== undefined) {
      const hebergement = await getHebergement(hebergementId)
      sethebergement(hebergement[0])
      dispatch(setHebergement(hebergement[0]))
    }
  }, [hebergementId])

  useEffect(() => {
    if (hebergement) {
      const images = hebergement.room_image
      const mainImage = images.find((i) => i.order === 1)
      if (mainImage !== 'undefined') {
        setmainImage(process.env.NEXT_PUBLIC_API_URL + mainImage?.image?.url)
      }
    }
  }, [hebergement])

  useEffect(() => {
    dispatch(setAdminTab('Hébergements'))
  }, [])

  return (
    <AdminLayout
      title={hebergement !== null && hebergement.internal_name}
      isReturn={true}
    >
      <div className="mb-5 mt-5">
        {/* WRAPPER */}
        <div className="container-xxl">
          <div className="admin-room__container">
            <ul
              className="nav nav-pills mb-3 establishment-tab admin-room__list"
              id="pills-tab"
              role="tablist"
            >
              {hebergement?.slug && (
                <Link
                  href={`/host/mes-hebergements/preview/${hebergement?.slug}`}
                >
                  <a
                    target="_blank"
                    className="room__title-wrapper-btn--move-down admin-room__preview"
                  >
                    <button className="btn btn-outline-secondary btn-home-1 mr-5 room__title-wrapper-btn--move-down-wide">
                      Aperçu de l'annonce
                    </button>
                  </a>
                </Link>
              )}
              {tabs.map(({ text, id }, index) => (
                <li
                  key={id}
                  className="nav-item"
                  role="presentation"
                  onClick={() => {
                    setIdtab(id)
                  }}
                >
                  <button
                    className={`nav-link ${!index ? 'active' : ''}`}
                    id={`${id}-tab`}
                    data-bs-toggle="pill"
                    data-bs-target={`#pills-${id}`}
                    type="button"
                    role="tab"
                    aria-controls={id}
                    aria-selected="true"
                  >
                    {text}
                  </button>
                </li>
              ))}
            </ul>

            <div className="row w-100 mx-0 mt-3 d-flex">
              <div className="col-12 col-md-8 p-0">
                <div className="tab-content" id="pills-tabContent">
                  {tabs.map(({ Component, id }, index) => (
                    <div
                      key={id}
                      className={`tab-pane fade show ${index ? '' : 'active'}`}
                      id={`pills-${id}`}
                      role="tabpanel"
                      aria-labelledby={`${id}-tab`}
                    >
                      <Component id={hebergementId} room={hebergement} />
                    </div>
                  ))}
                </div>
              </div>
              <div className="col-12 col-md-4 mt-4 summary__container">
                <Summary
                  image={mainImage}
                  data_hebergement={hebergement}
                  id_tab={idTab}
                  id_hebergement={hebergementId}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </AdminLayout>
  )
}

const mapDispatchToProps = {
  setHebergement
}

export default connect(null, mapDispatchToProps)(withRouter(single))
