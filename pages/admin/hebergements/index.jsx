import React, { useEffect, useState } from 'react'

import AdminLayout from '@components/layouts/AdminLayout'

import { useDispatch } from 'react-redux'
import { setAdminTab, setFullLoading } from '@ducks/Config/actions'
import HebergementTable from '@components/dashboard_admin/tables/HebergementTable'
import {
  exportHebergementData,
  getAdminRooms
} from 'services/hebergementsService'
import useDataWithPagination from 'hooks/useDataWithPagination'
import Pagination from '@components/dashboard_admin/Pagination'
import MasiveEditor from '@components/dashboard_admin/MasiveEditor'
import Finder from '@components/dashboard_admin/Finder'
import RoomsFilter from '@components/dashboard_admin/filters/RoomsFilter'

export default function Hebergements() {
  const [rooms, setRooms, pag, setPag, handlePagination, query, setQuery] =
    useDataWithPagination('Hébergements', getAdminRooms)
  const [selectedRows, setselectedRows] = useState([])
  return (
    <AdminLayout title="Hébergements">
      <div className="admin-userTable">
        <div className="admin-users-searchBar">
          <div className="d-flex">
            <MasiveEditor
              selectedRows={selectedRows}
              where={'hebergements'}
              setRooms={setRooms}
              rooms={rooms}
            />
          </div>
          <div class="btn-group">
            <button
              type="button"
              class="btn btn-secondary dropdown-toggle d-flex"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              options
              {/* <FontAwesomeIcon icon={faChevronDown} /> */}
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <button
                class="dropdown-item"
                type="button"
                onClick={async () => {
                  const resp = await exportHebergementData()
                }}
              >
                exporter
              </button>
            </div>
          </div>
        </div>
        <div className="admin-userTable__search">
          <RoomsFilter
            setRooms={setRooms}
            pag={pag}
            setPag={setPag}
            rooms={rooms}
            findFunction={getAdminRooms}
            query={query}
            setQuery={setQuery}
          />
        </div>
        <HebergementTable data={rooms} setselectedRows={setselectedRows} />
        <Pagination paginationInfo={pag} handlePagination={handlePagination} />
      </div>
    </AdminLayout>
  )
}
