import React, { useMemo, useEffect, useState } from 'react'

import AdminLayout from '@components/layouts/AdminLayout'
import { getUsers } from 'services/userServices'
import UsersTable from '@components/dashboard_admin/tables/UsersTable'
import { useDispatch } from 'react-redux'
import { setAdminTab, setFullLoading } from '@ducks/Config/actions'
import Router from 'next/router'

export default function users() {
  const dispatch = useDispatch()

  const [users, setUsers] = useState([])
  useEffect(async () => {
    Router.push('/admin/user')
    // dispatch(setAdminTab('usagers'))
    // dispatch(setFullLoading(true))
    // const resp = await getUsers()
    // setUsers(resp)
    // dispatch(setFullLoading(false))
  }, [])

  return (
    <AdminLayout title="utilisateurs enregistrés">
      <div className="admin-userTable">
        <UsersTable data={users} />
      </div>
    </AdminLayout>
  )
}
