import MainInfo from '@components/dashboard_admin/abracadaroom/MainInfo'
import AdminLayout from '@components/layouts/AdminLayout'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { getHebergement } from 'services/hebergementsService'
import { setAdminTab } from '@ducks/Config/actions'
import { useDispatch } from 'react-redux'

export default function single() {
  const router = useRouter()
  const Id = router.query.id
  const dispatch = useDispatch()

  const [room, setroom] = useState({})

  useEffect(async () => {
    if (Id !== undefined) {
      const room = await getHebergement(Id)
      setroom(room[0])
    }
  }, [Id])

  useEffect(() => {
    dispatch(setAdminTab('abracadaroom'))
  }, [])

  return (
    <AdminLayout title={room?.public_name} isReturn={true}>
      <MainInfo roomData={room} />
    </AdminLayout>
  )
}
