import React, { useMemo, useEffect, useState } from 'react'

import AdminLayout from '@components/layouts/AdminLayout'
import AbracadaroomTable from '@components/dashboard_admin/tables/AbracadaroomTable'
import Pagination from '@components/dashboard_admin/Pagination'
import {
  activateRooms,
  exportAbracadaroomData,
  getRoomPag
} from 'services/abracadaroomServices'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import useDataWithPagination from 'hooks/useDataWithPagination'
import { useToast } from 'hooks/useToast'
import Finder from '@components/dashboard_admin/Finder'
import AbracadaroomMasiveEditor from '@components/dashboard_admin/AbracadaroomMasiveEditor'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'
import AbracadaroomFilter from '@components/dashboard_admin/filters/AbracadaroomFilter'
export default function Abracadaroom() {
  const [notify] = useToast()
  const [rooms, setRooms, pag, setPag, handlePagination, query, setQuery] =
    useDataWithPagination('abracadaroom', getRoomPag)

  const [selectedRows, setselectedRows] = useState([])

  return (
    <AdminLayout title="Abracadaroom">
      <div className="admin-userTable">
        <div className="admin-users-searchBar">
          <div className="d-flex">
            <AbracadaroomMasiveEditor
              selectedRows={selectedRows}
              setRooms={setRooms}
              pag={pag}
              setPag={setPag}
              rooms={rooms}
              findFunction={getRoomPag}
              query={query}
              setQuery={setQuery}
            />
          </div>
          <div class="btn-group">
            <button
              type="button"
              class="btn btn-secondary dropdown-toggle d-flex"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              options
              {/* <FontAwesomeIcon icon={faChevronDown} /> */}
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <button
                class="dropdown-item"
                type="button"
                onClick={async () => {
                  const resp = await exportAbracadaroomData()
                }}
              >
                exporter
              </button>
            </div>
          </div>
        </div>
        <div className="admin-userTable__search">
          <AbracadaroomFilter
            setRooms={setRooms}
            pag={pag}
            setPag={setPag}
            rooms={rooms}
            findFunction={getRoomPag}
            query={query}
            setQuery={setQuery}
          />
        </div>
        <AbracadaroomTable data={rooms} setselectedRows={setselectedRows} />
        <Pagination paginationInfo={pag} handlePagination={handlePagination} />
      </div>
    </AdminLayout>
  )
}
