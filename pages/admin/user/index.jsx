import React, { useMemo, useEffect, useState } from 'react'

import AdminLayout from '@components/layouts/AdminLayout'
import { exportUsersData, getUsers } from 'services/userServices'
import UsersTable from '@components/dashboard_admin/tables/UsersTable'
import { useDispatch } from 'react-redux'
import { setAdminTab, setFullLoading } from '@ducks/Config/actions'
import Pagination from '@components/dashboard_admin/Pagination'
import useDataWithPagination from 'hooks/useDataWithPagination'
import Finder from '@components/dashboard_admin/Finder'
import UserFilter from '@components/dashboard_admin/filters/UserFilter'

export default function users() {
  const dispatch = useDispatch()
  const [users, setUsers, pag, setPag, handlePagination, query, setQuery] =
    useDataWithPagination('usagers', getUsers)

  return (
    <AdminLayout title="utilisateurs enregistrés">
      <div className="admin-userTable">
        <div
          className="d-flex justify-content-end"
          style={{ marginBottom: '4rem' }}
        >
          <div class="btn-group">
            <button
              type="button"
              class="btn btn-secondary dropdown-toggle d-flex"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              options
              {/* <FontAwesomeIcon icon={faChevronDown} /> */}
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <button
                class="dropdown-item"
                type="button"
                onClick={async () => {
                  const resp = await exportUsersData()
                }}
              >
                exporter
              </button>
            </div>
          </div>
        </div>
        <div className="admin-userTable__search">
          <UserFilter
            setUsers={setUsers}
            pag={pag}
            setPag={setPag}
            users={users}
            findFunction={getUsers}
            query={query}
            setQuery={setQuery}
          />
        </div>
        <UsersTable data={users} />
        <Pagination paginationInfo={pag} handlePagination={handlePagination} />
      </div>
    </AdminLayout>
  )
}
