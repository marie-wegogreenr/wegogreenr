import React, { useEffect, useState } from 'react'
import AdminLayout from '@components/layouts/AdminLayout'
import PhothoInput from '@components/dashboard_host/compte/inputs/PhothoInput'
import NameInput from '@components/dashboard_host/compte/inputs/NameInput'
import GenreInput from '@components/dashboard_host/compte/inputs/GenreInput'
import BirthdayInput from '@components/dashboard_host/compte/inputs/BirthdayInput'
import TextInput from '@components/dashboard_host/compte/inputs/TextInput'
import AdressInput from '@components/dashboard_host/compte/inputs/AdressInput'
import PassWordInput from '@components/dashboard_host/compte/inputs/PassWordInput'
import { useSelector, useDispatch } from 'react-redux'
import { setUserToEdit } from '@ducks/AdminUser/actions'
import { isEmpty } from 'utils'
import { useRouter } from 'next/router'

export default function User(props) {
  const router = useRouter()
  const dispatch = useDispatch()
  const userData = useSelector((state) => state.adminUser)
  const userId = router.query.id

  const [user, setUser] = useState(userData)
  /*  Effects */
  useEffect(() => {
    setUser(userData)
  }, [userData])

  useEffect(() => {
    return () => dispatch(setUserToEdit({}))
  }, [])
  return (
    <AdminLayout title="modifier l'information" isReturn={true}>
      <div className="card-white p-4 p-lg-5">
        <h3 className="weight-bold font-montserrat subtitle-dashboard">
          Informations personnelles
        </h3>
        <div className="border-bottom mt-4 mb-4"></div>
        <PhothoInput
          profilePicture={user?.photo_url}
          isAdminUpdate={true}
          idUser={user.id}
          userData={user}
        />
        <NameInput
          name={user.name}
          lastname={user.lastname}
          isAdminUpdate={true}
          idUser={user.id}
          userData={user}
        />
        <GenreInput
          genre={user.gender}
          isAdminUpdate={true}
          idUser={user.id}
          userData={user}
        />
        <BirthdayInput
          birthday={user.birthday}
          isAdminUpdate={true}
          idUser={user.id}
          userData={user}
        />
        <TextInput
          label="Email"
          value={user.email}
          formName="email"
          editable={true}
          isAdminUpdate={true}
          idUser={user.id}
          userData={user}
        />
        <TextInput
          label="Téléphone"
          formName="phone"
          value={user.phone}
          type="tel"
          isAdminUpdate={true}
          idUser={user.id}
          userData={user}
        />
        <AdressInput
          address={user.address}
          country={user.country}
          country_id={user.country_id}
          postalCode={user.postal_code}
          city={user.city}
          isAdminUpdate={true}
          idUser={user.id}
          userData={user}
        />
        {/* <PassWordInput /> */}
      </div>
    </AdminLayout>
  )
}
