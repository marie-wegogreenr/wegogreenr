import React from 'react'

import AdminLayout from '@components/layouts/AdminLayout'
import { getAllEtablissements } from '../../../services/etablissementService'
import EtablissementTable from '@components/dashboard_admin/tables/EtablissementTable'
import useDataWithPagination from 'hooks/useDataWithPagination'
import Pagination from '@components/dashboard_admin/Pagination'
import EstablishmentFilter from '@components/dashboard_admin/filters/EstablishmentFilter'
import { exportEstablishmentData } from 'services/hebergementsService'

export default function Etablissements() {
  const [
    etablissements,
    setEtablissements,
    pag,
    setPag,
    handlePagination,
    query,
    setQuery
  ] = useDataWithPagination('Établissements', getAllEtablissements)

  return (
    <AdminLayout title="Établissements">
      <div className="admin-userTable">
        <div className="d-flex justify-content-end">
          <div class="btn-group">
            <button
              type="button"
              class="btn btn-secondary dropdown-toggle d-flex"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              options
              {/* <FontAwesomeIcon icon={faChevronDown} /> */}
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <button
                class="dropdown-item"
                type="button"
                onClick={async () => {
                  const resp = await exportEstablishmentData()
                }}
              >
                exporter
              </button>
            </div>
          </div>
        </div>
        <div className="admin-userTable__search">
          <EstablishmentFilter
            setEtablissements={setEtablissements}
            pag={pag}
            setPag={setPag}
            etablissements={etablissements}
            findFunction={getAllEtablissements}
            query={query}
            setQuery={setQuery}
          />
        </div>
        <EtablissementTable data={etablissements} />
        <Pagination paginationInfo={pag} handlePagination={handlePagination} />
      </div>
    </AdminLayout>
  )
}
