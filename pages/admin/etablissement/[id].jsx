import AdminLayout from '@components/layouts/AdminLayout'
import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { Navbar } from '@components/host/establishment'
import { hostEstablishmentTabs } from 'fixtures'
import { setAdminTab } from '@ducks/Config/actions'
import { useDispatch } from 'react-redux'
import RoomsTab from '@components/dashboard_admin/RoomsTab'
import { MainTitle } from '@components/dashboard_host/etablissement/MainTitle'

export default function EtablissementDetail() {
  const dispatch = useDispatch()
  const [allTabs, setAllTabs] = useState([])

  const addRoomsTab = () => {
    setAllTabs([
      ...hostEstablishmentTabs,
      {
        id: 'rooms-tab',
        tabText: 'Hébergements',
        Body: RoomsTab
      }
    ])
  }

  useEffect(() => {
    addRoomsTab()
    dispatch(setAdminTab('Établissements'))
  }, [])

  const router = useRouter()
  const etablissementId = router.query.id

  return (
    <AdminLayout title="Établissements" isReturn={true}>
      <div className="container-xxl">
        <div className="host_establishment__content">
          <div className="row w-100 mx-0">
            <div className="col-12 px-0 d-flex align-items-center mb-3">
              <Navbar />
              <MainTitle />
            </div>

            <div className="col-12 px-0">
              <div className="tab-content" id="pills-tabContent">
                {allTabs.map((item, index) => {
                  const { Body, id } = item
                  return (
                    <div
                      key={index}
                      className={`tab-pane fade show ${index ? '' : 'active'}`}
                      id={`pills-${id}`}
                      role="tabpanel"
                      aria-labelledby={`pills-${id}-tab`}
                    >
                      <Body from="admin" id_etablisement={etablissementId} />
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </AdminLayout>
  )
}
