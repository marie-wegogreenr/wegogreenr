import React from 'react'
import AdminLayout from '@components/layouts/AdminLayout'
import ReservationTable from '@components/dashboard_admin/tables/ReservationsTable'
import {
  exportReservationsData,
  getAdminReservations
} from 'services/reservationService'
import useDataWithPagination from 'hooks/useDataWithPagination'
import Pagination from '@components/dashboard_admin/Pagination'
import ReservationFilter from '@components/dashboard_admin/ReservationFilter'

export default function Reservations() {
  const [
    reservations,
    setReservations,
    pag,
    setPag,
    handlePagination,
    query,
    setQuery
  ] = useDataWithPagination('Réservations', getAdminReservations)

  return (
    <AdminLayout title="Réservations">
      <div className="admin-userTable">
        <div
          className="d-flex justify-content-end"
          style={{ marginBottom: '4rem' }}
        >
          <div class="btn-group">
            <button
              type="button"
              class="btn btn-secondary dropdown-toggle d-flex"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              options
              {/* <FontAwesomeIcon icon={faChevronDown} /> */}
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <button
                class="dropdown-item"
                type="button"
                onClick={async () => {
                  const resp = await exportReservationsData()
                }}
              >
                exporter
              </button>
            </div>
          </div>
        </div>
        <div className="admin-userTable__search">
          <ReservationFilter
            setData={setReservations}
            setPag={setPag}
            query={query}
            setQuery={setQuery}
          />
        </div>
        <ReservationTable data={reservations} setData={setReservations} />
        <Pagination paginationInfo={pag} handlePagination={handlePagination} />
      </div>
    </AdminLayout>
  )
}
