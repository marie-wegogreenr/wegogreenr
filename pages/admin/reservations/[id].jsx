/*  React  */
import { useEffect, useState } from 'react'
/*  Redux  */
import { useDispatch, useSelector } from 'react-redux'
/*  Dependencies  */
import moment from 'moment'
import { adjustNumberWithComma, getStatusMessage } from 'helpers/utilsHelper'
/*  Components  */
import AdminLayout from '@components/layouts/AdminLayout'
import { close_reservation_action } from '@ducks/adminReservation/actions'
import { useRouter } from 'next/router'

function ReservationDetail() {
  /*  Global states  */
  const { show, reservation } = useSelector((store) => store.adminReservation)
  /*  Local states */
  const [reservationData, setReservationData] = useState(reservation)
  /*  Effects  */
  useEffect(() => {
    if (reservation) {
      setReservationData(reservation)
    }
  }, [reservation])

  return (
    <AdminLayout title="Réservations">
      {reservationData && (
        <div className="reservation-admin-detail">
          <div className="wrapper-8">
            <div className="text-left">
              <h2 className="h2-dashboard font-montserrat">
                Détail de la réservation
              </h2>
              <img
                src={`${process.env.NEXT_PUBLIC_AMAZON_IMGS}${reservationData?.principal_image}`}
                alt=""
                height="250"
                className="w-100 obj-fit-cover reservation-admin-detail__img"
              />
              <p className="subtitle-dashboard weight-bold my-4">
                {reservationData?.public_name}
              </p>
            </div>
            <div className="border-bottom"></div>
            <div className="d-flex justify-content-between mt-4">
              <p className="weight-bold paragraph">ID de la réservation</p>
              <p className="paragraph">{reservationData?.code}</p>
            </div>
            <div className="d-flex justify-content-between">
              <p className="weight-bold paragraph">Réservation faite le</p>
              <p className="paragraph">
                {moment(reservationData?.created_at).format(
                  'dddd, Do MMMM  YYYY'
                )}
              </p>
            </div>
            <div className="d-flex justify-content-between">
              <p className="weight-bold paragraph">Voyageurs</p>
              <p className=" paragraph">
                {`${reservationData?.number_of_adults} ${
                  reservationData?.number_of_adults === 0 ||
                  reservationData?.number_of_adults > 1
                    ? 'adultes'
                    : 'adult'
                } , ${reservationData?.number_of_children} ${
                  reservationData?.number_of_children === 0 ||
                  reservationData?.number_of_children > 1
                    ? 'enfants'
                    : 'enfant'
                }`}
              </p>
            </div>
            <div className="d-flex justify-content-between">
              <p className="weight-bold paragraph">Dates &amp; heures </p>
              <p className="paragraph"></p>
            </div>
            <div className="d-flex justify-content-between">
              <div className="w-50 mr-3">
                <div className="locationPopUp__card text-center d-flex justify-content-center flex-column">
                  <b className="text-block weight-bold">Arrivée</b>
                  <p className="paragraph">
                    {moment(reservationData?.arrival_date).format(
                      'dddd, Do MMMM  YYYY'
                    )}
                  </p>
                </div>
              </div>
              <div className="w-50">
                <div className="locationPopUp__card text-center d-flex justify-content-center flex-column">
                  <b className="text-block weight-bold">Départ</b>
                  <p className="paragraph">
                    {moment(reservationData?.departure_date).format(
                      'dddd, Do MMMM  YYYY'
                    )}
                  </p>
                </div>
              </div>
            </div>
            <div className="border-bottom mt-3"></div>
            <div className="d-flex justify-content-between mt-4">
              <p className="weight-bold paragraph">Statut </p>
              <p className="paragraph">
                {getStatusMessage(reservationData?.status)}
              </p>
            </div>
            <p className="subtitle-dashboard weight-bold mt-5">
              Informations de paiement
            </p>
            <div className="d-flex justify-content-between mt-4 mb-5">
              <p className="weight-bold paragraph">Total </p>
              <p className="paragraph">
                {adjustNumberWithComma(reservationData?.room_price)} &euro;
              </p>
            </div>
          </div>
        </div>
      )}
    </AdminLayout>
  )
}

export default ReservationDetail
