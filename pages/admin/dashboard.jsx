import React, { useEffect, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
//components
import Container from '@components/layouts/container'
import Reservations from '../../components/dashboard_admin/user_data/Reservations'
//redux
import { useSelector, useDispatch } from 'react-redux'
import { getUserReservationsAction } from '../../redux/ducks/userDuck'
//

import Cookies from 'universal-cookie'
import Loader from '@components/Loader'

const dashboard = () => {
  const store = useSelector((store) => store.user.user.id)
  const [user, setUser] = useState()
  const [loading, setLoading] = useState(true)
  const dispatch = useDispatch()

  useEffect(() => {
    getInitialInfo()
  }, [])

  const getInitialInfo = async () => {
    setLoading(true)
    const cookies = new Cookies()
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'users', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })

    const json = await res.json()
    setUser(json)
    setTimeout(() => {
      setLoading(false)
    }, 1000)
  }

  //consoleLog(user)

  return (
    <Container>
      {/* Container */}
      <section className="host_my_rooms__container bg-yellow-light mb-4 mt-4">
        {/* Wrapper */}
        <div className="container-xxl" style={{ marginTop: '6rem' }}>
          {/* Header */}
          <div className="mb-4">
            <h1 className="color-primary ml-4">Users</h1>
          </div>
          {!loading ? (
            <div className="row col-12 justify-content-center">
              <Reservations
                user_data={user}
                onUpdateInfo={() => getInitialInfo()}
              />
            </div>
          ) : (
            <Loader />
          )}
        </div>
      </section>
    </Container>
  )
}

export default dashboard
