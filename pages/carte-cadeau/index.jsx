import CartCadeauHero from '@components/CartCadeau/CartCadeauHero'
import CartCadeauJustification from '@components/CartCadeau/CartCadeauJustification'
import CartCadeauExperience from '@components/CartCadeau/CartCadeauExperience'
import CartCadeauActivities from '@components/CartCadeau/CartCadeauActivities'
import Footer from '@components/footer'
import NavbarOptions from '@components/NavigationComponents/OptionsNav/NavbarOptions'
import SinglePageLayout from '@components/layouts/SinglePageLayout'

export default function CartCadeau() {
  return (
    <SinglePageLayout>
      <NavbarOptions />
      <main>
        <CartCadeauHero />
        <CartCadeauJustification />
        <CartCadeauExperience />
        {/* <CartCadeauActivities /> */}
      </main>
      <Footer />
    </SinglePageLayout>
  )
}
