import React from 'react'
import Container from '../../components/container'
import { withRouter } from 'next/router'
import Image from 'next/image'
import Link from 'next/link'
import Router from 'next/router'
import { toast } from 'react-toastify'
import Cookies from 'universal-cookie'
import { LogotypeExtended } from '@components/Icons/LogotypeExtended'

class Email extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: ''
    }
  }

  handleResendEmailVerify = async (event) => {
    event.preventDefault()
    const cookies = new Cookies()
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'resend-verification', {
      body: JSON.stringify({
        email: cookies.get('email_validate_user')
      }),
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST'
    }).then(() => {
      toast.info('Email invalide', {
        position: toast.POSITION.BOTTOM_LEFT
      })
    })
  }

  render() {
    const { router } = this.props

    return (
      <Container>
        <div className="container-fluid container-login__wrapper container-login__wrapper--short">
          <div className="row">
            <div className="col-12 col-xl-8 container-fluid container-login">
              <Link href="/">
                <a>
                  <LogotypeExtended
                    fill="white"
                    className="container-login__logo"
                  />
                </a>
              </Link>
              {router.query.id == 'forget_password' && (
                <div className="card-white">
                  <div className="justify-content-center d-flex mb-3">
                    <img
                      src="/images/mail_icon.png"
                      alt="Logo"
                      className="image-logo"
                    />
                  </div>
                  <h3 className="mb-0 text-center">
                    Un email vous a été envoyé{' '}
                  </h3>
                  <h3 className="mb-0 text-center">
                    pour récupérer votre mot de passe
                  </h3>
                  {/* <form className="form" onSubmit={this.handleRecoverPassword}>
                    <div className="form-group">
                      <p
                        className="text-justify"
                        style={{
                          marginBottom: 25 + 'px',
                          marginTop: 30 + 'px'
                        }}
                      >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Nulla quam velit, vulputate eu pharetra nec, mattis ac
                        neque. Duis vulputate commodo lectus, ac blandit elit
                        tincidunt id. Sed rhoncus, tortor sed eleifend
                        tristique, tortor mauris molestie elit, et
                      </p>
                    </div>
                  </form> */}
                  <div className="form-group mt-4 d-flex justify-content-center">
                    <Link href="/login">
                      <a className="text-center link-black"> Se connecter</a>
                    </Link>
                  </div>
                </div>
              )}
              {router.query.id == 'validate_email' && (
                <div className="card-white">
                  <div class="logo-responsive">
                    <Link href="/">
                      <a>
                        <img src="/images/logo.svg" alt="Logo" />
                      </a>
                    </Link>
                  </div>
                  <div className="justify-content-center d-flex mb-3">
                    <img
                      src="/images/mail_icon.png"
                      alt="Logo"
                      className="image-logo"
                    />
                  </div>
                  <h3 className="mb-0 text-center">
                    Un email vous a été envoyé
                  </h3>
                  <h3 className="mb-0 text-center">
                    {' '}
                    pour valider votre compte
                  </h3>
                  <form className="form" onSubmit={this.handleRecoverPassword}>
                    <div className="form-group">
                      <p
                        className="text-center"
                        style={{
                          marginBottom: 25 + 'px',
                          marginTop: 30 + 'px'
                        }}
                      >
                        Rendez-vous dans votre boite mail pour terminer votre
                        inscription. Vous n'avez rien reçu ? Vérifiez dans vos
                        spams.
                      </p>
                    </div>
                  </form>
                  <div className="form-group mt-4 d-flex justify-content-center">
                    <Link href="/login">
                      <a className="text-center link-black"> Se connecter</a>
                    </Link>
                  </div>
                  <div className="form-group mt-4 d-flex justify-content-center">
                    <Link href="">
                      <a
                        className="text-center link-black"
                        onClick={this.handleResendEmailVerify}
                      >
                        Vous n'avez pas reçu de mail ? Cliquez ici pour générer
                        un nouvel envoi
                      </a>
                    </Link>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

export default withRouter(Email)
