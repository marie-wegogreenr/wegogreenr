import * as React from 'react'
import Axeptio from '@components/Axeptio'
import { ToastContainer } from 'react-toastify'
/* Next */
import Head from 'next/head'
import { Router, useRouter } from 'next/router'
import { locale } from 'moment'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import 'nprogress/nprogress.css'
import 'react-toastify/dist/ReactToastify.min.css'
import 'react-datepicker/dist/react-datepicker.css'

import 'styles/app.scss'
import 'styles/single.sass'
import 'styles/home.sass'
import 'styles/list.sass'
import 'styles/main.scss'
import 'styles/host_dashboard.scss'
/* NProgress */
import NProgress from 'nprogress'
/* Redux */
import { useStore } from '../redux/store'
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux'
import FullLoading from '@components/FullLoading/FullLoading'
/* Google Analytics */
import { pageview } from 'services/googleAnalytics'

// nprogress
Router.events.on('routeChangeStart', () => {
  NProgress.start()
})
Router.events.on('routeChangeComplete', () => {
  NProgress.done()
})
Router.events.on('routeChangeError', () => {
  NProgress.done()
})

const libraries = ['maps']

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
  const { store, persistor } = useStore(pageProps.initialReduxState)
  locale('fr')

  const router = useRouter()

  React.useEffect(() => {
    const handleRouteChange = (url) => {
      pageview(url)
    }
    //When the component is mounted, subscribe to router changes
    //and log those page views
    router.events.on('routeChangeComplete', handleRouteChange)

    // If the component is unmounted, unsubscribe
    // from the event with the `off` method
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])

  return (
    <>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          {() => (
            <>
              <Head>
                <title key="metaTitle">We Go GreenR</title>
                <meta
                  property="og:title"
                  content="We Go GreenR, des voyages qui ont du sens"
                  key="ogtitle"
                />
                <meta
                  property="og:description"
                  content="﻿Séjournez dans des lieux de qualité pour des vacances et des weekends éco-responsables en France."
                  key="ogdescription"
                />
                <meta
                  property="title"
                  content="We Go GreenR, des voyages qui ont du sens"
                  key="title"
                />
                <meta
                  property="description"
                  content="﻿Séjournez dans des lieux de qualité pour des vacances et des weekends éco-responsables en France."
                  key="description"
                />
                <meta
                  property="og:image"
                  content="https://wegogreenr-laravel.s3.eu-west-2.amazonaws.com/bloomers_eco1128.png"
                />
                <meta
                  name="facebook-domain-verification"
                  content="6kjmz6plkw9w0al9yweeqk8s7pw155"
                />
                <link rel="shortcut icon" href="/images/favicon.png" />
                <link
                  href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css"
                  rel="stylesheet"
                  integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl"
                  crossOrigin="anonymous"
                />
                <script
                  src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
                  integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
                  crossOrigin="anonymous"
                ></script>
                <link
                  rel="stylesheet"
                  href="https://unpkg.com/leaflet.markercluster@1.1.0/dist/MarkerCluster.css"
                />
                <link
                  rel="stylesheet"
                  href="https://unpkg.com/leaflet.markercluster@1.1.0/dist/MarkerCluster.Default.css"
                />

                <script src="https://unpkg.com/leaflet.markercluster@1.1.0/dist/leaflet.markercluster.js"></script>

                {/* <script
                  id="ze-snippet"
                  src="https://static.zdassets.com/ekr/snippet.js?key=1ec2241c-fc21-42dc-badd-3cbf543abccb"
                >
                  {' '}
                </script> */}
              </Head>
              <Component {...pageProps} />
              <FullLoading />
              {/* <Axeptio /> */}
              <ToastContainer />
            </>
          )}
        </PersistGate>
      </Provider>
    </>
  )
}
