import React from 'react'
import Container from '../../components/container'
import UserInfo from '../../components/dashboard_user/account/UserInfo'
import CardWithIcon from './../../components/dashboard_user/account/CardWithIcon'
//ICONS
import { UserExplorer } from '../../components/Icons'
//REDUX
import { useSelector } from 'react-redux'
import NewsletterSubscribe from '@components/Newsletter/NewsletterSubscribe'

const monprofil = () => {
  const userName = useSelector((store) => store.user.user.name)

  return (
    <Container>
      <div className="container-xxl">
        <h1 className="h1 weight-bold mt-4">Mon Profil</h1>
        <div className="row">
          <div className="col-12 col-lg-7 mt-4">
            <UserInfo />
          </div>
          <div className="col-12 col-lg-4 mt-4">
            <CardWithIcon
              icon={UserExplorer()}
              title={`Hello ${userName} !`}
              content="Vos informations personnelles sont utilisées lors de vos réservations afin de gagner du temps et éviter de les ressaisir à chaque fois."
            />
            <NewsletterSubscribe from="dashboard" />
          </div>
        </div>
      </div>
    </Container>
  )
}

export default monprofil
