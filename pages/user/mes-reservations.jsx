import React, { useEffect, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
//components
import Container from '@components/container'
import DateInput from '@components/dashboard_user/reservations/DateInput'
import HeaderMenu from '@components/dashboard_user/reservations/HeaderMenu'
import LocationInput from '@components/dashboard_user/reservations/LocationInput'
import Reservations from '../../components/dashboard_user/reservations/Reservations'
//redux
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import {
  getUserReservationsAction,
  getUserReservationsAditionalAction
} from '../../redux/ducks/userDuck'
//
import { LoadScript } from '@react-google-maps/api'
import Link from 'next/link'

const libraries = ['places']

const mesreservations = () => {
  const store = useSelector((store) => store.user.user.id)
  const reservations = useSelector((store) => store.user.reservations)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getUserReservationsAction())
  }, [store])

  useEffect(() => {
    if (reservations.length > 0) {
      dispatch(getUserReservationsAditionalAction())
    }
  }, [reservations])

  const [filters, setFilters] = useState({
    date: '',
    location: ''
  })

  return (
    <Container>
      {/* <LoadScript
        googleMapsApiKey="AIzaSyCD2m7tKguF7vbh__Cwy3nbTwacV0DJ2aY"
        libraries={libraries}
      > */}
      <div
        className="container-xxl"
        style={{ minHeight: '60vh', marginTop: '10rem' }}
      >
        {reservations.length > 0 && (
          <>
            <div className="row align-items-center justify-content-between mt-5 mb-3 col-12 mx-0">
              <h1 className="col-12 col-lg-6 h1 weight-bold">
                Mes réservations
              </h1>
              <div className="col-12 col-lg-6 justify-content-end d-flex reservation-inputs">
                {/* <DateInput state={filters} onchange={handleChange} /> */}
                {/* <LocationInput state={filters} onchange={handleChange} /> */}
              </div>
            </div>
            <div className="row col-12 justify-content-center mx-0">
              <HeaderMenu />
              <Reservations filters={filters} />
            </div>
          </>
        )}
        {reservations.length == 0 && (
          <div className="w-100 text-left py-5 mt-5">
            <p className="subtitle-dashboard weight-bold font-fraunces">
              Vous n'avez pas encore de réservation
            </p>
            <Link href="/">
              {/* <button className="btn btn-secondary py-3 px-5 my-5 weight-semibold paragraph"> */}
              <button className="btn btn-secondary btn-home-1 my-5 weight-semibold paragraph">
                Je réserve mon prochain séjour
              </button>
            </Link>
          </div>
        )}
      </div>
      {/* </LoadScript> */}
    </Container>
  )
}

export default mesreservations
