import Container from '@components/container'
import ChatContainer from '@components/dashboard_user/conversations/ChatContainer'
import InputMessage from '@components/dashboard_user/conversations/InputMessage'
import UserCard from '@components/dashboard_user/conversations/UserCard'
import ChatHeader from 'components/dashboard_user/conversations/ChatHeader'

const lorem = 'lorem ipsum dolor sir amet, conscetiei apsoase lot'
const mockPlace = {
  location_name: 'la revolution',
  image: 'http://dummyimage.com/499x315.jpg/ff4444/ffffff',
  date: 'jun 21 - jun 28',
  users: 2
}

const mesconversations = () => {
  return (
    <Container>
      <div className="row">
        <div className="col-3">
          Messeges
          <UserCard name="ricardo" message={lorem} />
          <UserCard name="angelo" message={lorem} />
          <UserCard name="juan" message={lorem} />
        </div>
        <div className="col-9 h-50 border">
          <div className="row"></div>
          <ChatHeader name="juan" place={mockPlace} />
          <ChatContainer />
          <InputMessage />
        </div>
      </div>
    </Container>
  )
}

export default mesconversations
