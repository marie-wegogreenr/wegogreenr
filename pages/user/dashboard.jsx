import Container from '../../components/container'
import React from 'react'
import { consoleLog } from '/utils/logConsole'
import ProfileCard from '@components/dashboard_user/dashboard/ProfileCard'
import Reservations from '@components/dashboard_user/dashboard/Reservations'
import Conversations from '@components/dashboard_user/dashboard/Conversations'
import InfoCard from '@components/dashboard_user/dashboard/InfoCard'

function Dashboard() {
  return (
    <Container>
      <div className="container-xxl" style={{ marginTop: '6rem' }}>
        <div className="row mt-5">
          <div className="col-12 col-lg-8">
            <ProfileCard />
            <Reservations />
          </div>
          <div className="col-12 col-lg-4 px-0 px-lg-4 mb-4">
            {/* <Conversations /> */}
            <InfoCard
              title="Devenez partenaire ?"
              content="Vous avez une expérience ou un hébergement à proposer et vous vous reconnaissez dans le ton WGG ?
            "
              link="/onboarding/presentation"
            />
          </div>
        </div>
      </div>
    </Container>
  )
}

export default Dashboard
