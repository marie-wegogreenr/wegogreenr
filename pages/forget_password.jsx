import React from 'react'
import { toast } from 'react-toastify'
import Container from '../components/container'
import Image from 'next/image'
import Link from 'next/link'
import Router from 'next/router'
import { notify } from 'helpers/notificationClassHelper'
import { LogotypeExtended } from '@components/Icons/LogotypeExtended'

class ForgetPassword extends React.Component {
  constructor(props) {
    super(props)

    this.handleRecoverPassword = this.handleRecoverPassword.bind(this)
  }

  async handleRecoverPassword(event) {
    event.preventDefault()
    const res = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'auth/password/forgot-password',
      {
        body: JSON.stringify({
          email: event.target.email.value
        }),
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'POST'
      }
    )

    const result = await res.json()
    const status = await res.status

    if (status === 200) {
      Router.push('/email/forget_password')
    } else if (status === 403) {
      notify(`L'email n'existe pas`, 'error')
    } else if (status === 400) {
      notify(`Cette adresse mail n'existe pas`, 'error')
    } else {
      notify(`Une erreur s'est produite, réessayez plus tard`, 'error')
    }
  }

  render() {
    return (
      <Container>
        <div className="container-fluid container-login__wrapper container-login__wrapper--short">
          <div className="row">
            <div className="col-12 col-xl-8 container-fluid container-login">
              <Link href="/">
                <a>
                  <LogotypeExtended
                    fill="white"
                    className="container-login__logo"
                  />
                </a>
              </Link>
              <div className="card-white">
                <h1 className="mb-3 text-center">Mot de passe oublié ?</h1>
                <form className="form" onSubmit={this.handleRecoverPassword}>
                  <div className="form-group">
                    {/* <p
                      className="text-justify"
                      style={{ marginBottom: 25 + 'px', marginTop: 30 + 'px' }}
                    >
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Nulla quam velit, vulputate eu pharetra nec, mattis ac
                      neque. Duis vulputate commodo lectus, ac blandit elit
                      tincidunt id. Sed rhoncus, tortor sed eleifend tristique,
                      tortor mauris molestie elit, et
                    </p> */}
                    <h4 htmlFor="email">Adresse email</h4>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                      name="email"
                      aria-describedby="emailHelp"
                      placeholder="Saisissez votre adresse email"
                    />
                  </div>
                  <button type="submit" className="button button-long">
                    Récupérez votre mot de passe
                  </button>
                </form>
                <div className="form-group mt-4 d-flex justify-content-center">
                  <Link href="/login">
                    <a className="text-center link-black">
                      {' '}
                      J'ai retrouvé mon Mot de passe?
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

export default ForgetPassword
