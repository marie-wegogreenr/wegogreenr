import Container from '@components/container'
import React, { useState } from 'react'
import Input from '@components/Contact/Input'
import { useToast } from 'hooks/useToast'

const index = () => {
  const [notify] = useToast()
  const [form, setForm] = useState({
    name: '',
    lastname: '',
    tel: '',
    message: '',
    email: ''
  })

  function handleChange(e) {
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  function handleSubmit(e) {
    e.preventDefault()

    const options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(form)
    }

    fetch(`${process.env.NEXT_PUBLIC_API_URL}contact-form`, options)
      .then((res) => res.json())
      .then((data) => {
        notify(`message envoyé avec succès`, 'success')
        setForm({
          name: '',
          lastname: '',
          tel: '',
          message: '',
          email: ''
        })
      })
      .catch((err) => notify(err, 'error'))
  }

  return (
    <Container>
      <div className="wrapper-6 mb-1 py-3">
        <h1 className="h1 weight-bold mt-5 pt-5 pb-3">Nous contacter</h1>
        <div className="card-white px-3 my-4 py-4 px-lg-5">
          <form onSubmit={handleSubmit}>
            <div className="d-flex flex-column flex-lg-row">
              <Input
                name="name"
                form={form}
                setForm={setForm}
                label="Prenom *"
                required={true}
              />
              <div className="mx-2"></div>
              <Input
                name="lastname"
                form={form}
                setForm={setForm}
                label="Nom *"
                required={true}
              />
            </div>
            <div className="d-flex  flex-column flex-lg-row">
              <Input
                name="email"
                form={form}
                setForm={setForm}
                label="Adresse email *"
                required={true}
              />
              <div className="mx-2"></div>
              <Input
                name="tel"
                form={form}
                setForm={setForm}
                label="Téléphone"
              />
            </div>
            <div className="d-flex">
              <label className="d-flex flex-column w-100 text weight-bold">
                Message *
                <textarea
                  name="message"
                  id=""
                  cols="200"
                  rows="10"
                  className="mt-2 p-3 form form-control font-montserrat"
                  onChange={handleChange}
                  value={form.message}
                  required
                ></textarea>
              </label>
            </div>
            <div>
              <label className="d-flex align-items-center pb-2">
                <input
                  type="checkbox"
                  name="conditions"
                  id=""
                  required
                  value=""
                  className="m-0 form-check-input input-check-blue-dark"
                />
                <p className="ms-4 my-0 contacter__conditions">
                  J'accepte les conditions d'utilisation et la politique de
                  confidentialité des données et confirme la véracité des
                  informations ci dessus.
                </p>
              </label>
              <input
                type="submit"
                value="Envoyer"
                className="btn btn-secondary w-100 text weight-bold py-3"
              />
            </div>
          </form>
        </div>
      </div>
    </Container>
  )
}

export default index
