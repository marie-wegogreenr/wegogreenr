import * as React from 'react'
/* Components */
import { Layout } from 'components'
/* Hooks */
import { useIsShow } from 'hooks'
import dynamic from 'next/dynamic'
import { getARSingleEtablissement } from 'services/ARService'
import MainSection from '@components/establishment/MainSection'
import Description from '@components/establishment/Description'
import Navbar from '@components/establishment/Navbar'
import { getEtablissementServicesById } from 'services/roomServicesService'
import {
  getDescriptionConditions,
  getDescriptionConditionsStablishment
} from 'services/hebergementsService'
import LayoutSection from '@components/Hebergements/Single/Sections/LayoutSection'
import PracticalInfo from '@components/Hebergements/Single/Sections/PracticalInfo'
import Commitments from '@components/Hebergements/Single/Sections/Commitments'
import Equipments from '@components/Hebergements/Single/Sections/Equipments'
import ARwidget from '@components/establishment/ARwidget'
import { getEstablishmentEquipments } from 'services/etablissementService'
import { findBreakfastCategory } from 'helpers/roomServicesHelper'
import TicTacTripBanner from '@components/Hebergements/Single/TicTacTripBanner'
import { eventListObjectTriggered } from 'helpers/dataLayer/lists'
import { useDispatch, useSelector } from 'react-redux'
import { removeUrl } from '@ducks/tempPage/actions'
import { setFullLoading } from '@ducks/Config/actions'
import { setPageViewHold } from '@ducks/dataLayerEvents/actions'

function Single({
  establishment,
  establishmentServices,
  descriptionConditions,
  descriptionConditionsStablishment,
  establishmentEquipments
}) {
  const dispatch = useDispatch()

  const { pageView } = useSelector((store) => store.dataLayerEvent)

  const widgetReference = React.useRef(null)
  const { ref, isShow } = useIsShow()
  const BreakFastCategory = findBreakfastCategory(establishmentServices)

  const [eventSent, setEventSent] = React.useState(false)

  React.useEffect(() => {
    dispatch(setFullLoading(false))

    if (!eventSent && pageView) {
      eventListObjectTriggered.viewItem.callback(establishment)
      setEventSent(true)
      dispatch(setPageViewHold())
    }

    return () => {
      dispatch(removeUrl())
    }
  }, [pageView])

  return (
    <Layout>
      <div className="container-xxl my-5">
        <Navbar sticky hidden={!isShow} />
        <MainSection
          establishment={establishment}
          breakfast={BreakFastCategory}
        />
        <div className="bg-yellow-light py-4">
          <Navbar navRef={ref} sticky={true} />

          <Description
            establishment={establishment}
            services={establishmentServices}
            conditions={descriptionConditions}
            conditionsStablishment={descriptionConditionsStablishment}
            widgetReference={widgetReference}
          />
          <div id="equipement"></div>
        </div>

        <LayoutSection id="">
          <Equipments equipments={establishmentEquipments} />
          <div id="infos_pratiques"></div>
        </LayoutSection>
        <LayoutSection>
          <PracticalInfo services={establishmentServices} />
          <div id="engagement_eco_responsable"></div>
        </LayoutSection>
        <LayoutSection>
          <Commitments
            userId={establishment?.user.id}
            greenScore={establishment?.green_score?.score}
          />
        </LayoutSection>

        <ARwidget
          code={establishment?.ar_widget?.widget_code}
          reference={widgetReference}
        />

        <TicTacTripBanner />
      </div>
    </Layout>
  )
}

export async function getServerSideProps({ query }) {
  try {
    const { id } = query

    let descriptionConditions = []
    let descriptionConditionsStablishment = []

    const { code, data } = await getARSingleEtablissement(id)

    const establishmentServices = await getEtablissementServicesById(data.id)

    const establishmentEquipments = await getEstablishmentEquipments(data.id)

    try {
      descriptionConditions = await getDescriptionConditions()
      descriptionConditionsStablishment =
        await getDescriptionConditionsStablishment(id)
    } catch (e) {
      throw new Error(e)
    }

    return {
      props: {
        establishment: data,
        establishmentServices,
        descriptionConditions,
        descriptionConditionsStablishment,
        establishmentEquipments
      }
    }
  } catch (error) {
    console.warn(error)
    let mockObject = {
      establishment: {}
    }
    return {
      props: mockObject
    }
  }
}

export default Single
