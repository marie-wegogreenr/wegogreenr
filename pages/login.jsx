import React, { useState } from 'react'

import Link from 'next/link'
import Router from 'next/router'

import Container from '../components/container'
import GoogleLogin from 'react-google-login'
import Cookies from 'universal-cookie'
import { Loader } from 'components/'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBell, faCommentDots } from '@fortawesome/free-regular-svg-icons'
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons'

import { useToast } from 'hooks/useToast'
import { getUserData, login } from 'services/userServices'
import { useDispatch } from 'react-redux'
import { setRole, setUserData } from '@ducks/userDuck'
import { getEtablissement } from 'services/etablissementService'
import { setEstablishmentId } from '@ducks/host/establishment/actions'
import { eventListObjectTriggered } from 'helpers/dataLayer/lists'
import { LogotypeExtended } from '@components/Icons/LogotypeExtended'

const Login = (props) => {
  const dispatch = useDispatch()
  const [notify] = useToast()
  const [isShowContent, setShowContent] = useState(true)
  const [showPassword, setShowPassword] = useState(false)
  const [eventSent, setEventSent] = useState(false)

  library.add(faBell, faCommentDots, faEye, faEyeSlash)

  const loginUser = async (event) => {
    event.preventDefault()
    setShowContent(false)
    const body = {
      email: event.target.email.value,
      password: event.target.password.value
    }
    const resp = await login(body)

    if (resp.code == 200) {
      handleCookiesUser(resp.data.access_token)
    } else if (resp.code == 403) {
      notify(`L'utilisateur n'existe pas`, 'error')
      setShowContent(true)
    } else {
      notify(`L'e-mail ou le mot de passe n'est pas valide`, 'error')
      setShowContent(true)
    }
  }

  const handleCookiesUser = async (token) => {
    const cookies = new Cookies()
    try {
      const user = await getUserData(token)

      if (user.email_verified_at == null) {
        notify(
          `Vous devez valider votre compte. Veuillez vérifier votre e-mail.`,
          'error'
        )
        setShowContent(true)
        return
      }
      dispatch(setUserData(user))
      cookies.set('tk_user', token, { path: '/' })
      if (user.establishment !== null) {
        dispatch(setEstablishmentId(user?.establishment?.id))
      }

      if (cookies.get('slug_room') !== undefined) {
        let slug = cookies.get('slug_room')
        cookies.remove('slug_room')
        // Router.push('/hebergements/' + slug)
        Router.push('/checkout')
        return
      }
      const isAdmin = user.roles.filter((rol) => rol.name == 'admin')
      const ishost = user.roles.find((rol) => rol.name == 'host')

      if (!eventSent) {
        eventListObjectTriggered.login.callback()
        setEventSent(true)
      }

      if (isAdmin.length > 0) {
        dispatch(setRole('admin'))
        return Router.push('/admin')
      } else if (ishost) {
        dispatch(setRole('host'))
        Router.push('/host/dashboard')
      } else {
        dispatch(setRole('traveler'))
        Router.push('/user/dashboard')
      }
    } catch (error) {
      Router.push('/login')
    }
  }

  const responseGoogle = async (response) => {
    if (response?.error == 'idpiframe_initialization_failed') {
      notify(
        'Si vous êtes en mode incognito, vous devez autoriser les cookies pour le login google.',
        'error'
      )
      setShowContent(true)
    } else {
      setShowContent(false)
      const res = await fetch(
        process.env.NEXT_PUBLIC_API_URL + 'auth/login-with-google',
        {
          body: JSON.stringify({
            google_token: response.tokenId
          }),
          headers: {
            'Content-Type': 'application/json'
          },
          method: 'POST'
        }
      )

      const result = await res.json()
      const status = await res.status
      //consoleLog(result);
      //consoleLog(status);
      if (status == 200) {
        handleCookiesUser(result.access_token)

        if (!eventSent) {
          eventListObjectTriggered.login.callback()
          setEventSent(true)
        }
      } else if (status == 403) {
        notify(`L'utilisateur n'existe pas `, 'error')
        setShowContent(true)
      } else {
        notify(`Une erreur s'est produite, réessayez plus tard.`, 'error')
        setShowContent(true)
      }
    }
  }

  const handleShowPassword = async () => {
    setShowPassword(!showPassword)
  }

  return (
    <Container>
      <div className="container-fluid container-login__wrapper">
        <div className="row">
          <div className="col-12 col-xl-8 container-fluid container-login">
            <Link href="/">
              <a>
                <LogotypeExtended
                  fill="white"
                  className="container-login__logo"
                />
              </a>
            </Link>
            {isShowContent ? (
              <div className="card-white container-login__card">
                <h1 className="container-login__title">Connexion</h1>
                <form className="form" onSubmit={loginUser}>
                  <div className="form-group">
                    <label htmlFor="email" className="container-login__label">
                      Adresse email *
                    </label>
                    <input
                      type="email"
                      className="form-control container-login__input"
                      id="email"
                      name="email"
                      aria-describedby="emailHelp"
                      placeholder="Saisissez votre adresse email"
                      required
                    />
                  </div>
                  <div className="form-group">
                    <label
                      htmlFor="password"
                      className="container-login__label"
                    >
                      Mot de passe *
                    </label>
                    <div className="password-input">
                      {showPassword ? (
                        <FontAwesomeIcon
                          icon={['fas', 'eye-slash']}
                          size="1x"
                          className="icon-eye container-login__input-icon"
                          onClick={handleShowPassword}
                        />
                      ) : (
                        <FontAwesomeIcon
                          icon={['fas', 'eye']}
                          size="1x"
                          className="icon-eye container-login__input-icon"
                          onClick={handleShowPassword}
                        />
                      )}
                      <input
                        type={showPassword ? 'text' : 'password'}
                        className="form-control container-login__input"
                        id="password"
                        name="password"
                        placeholder="Saisissez votre mot de passe"
                        required
                      />
                    </div>
                  </div>
                  <button
                    type="submit"
                    className="button button-long container-login__button"
                  >
                    Se connecter
                  </button>
                  <div className="d-flex flex-column justify-content-center container-login__links">
                    <Link href="/forget_password">
                      <a className="text-center link-black">
                        Mot de passe oublié ?
                      </a>
                    </Link>
                    <Link href="/resend_email_verification">
                      <a className="text-center link-black">
                        Renvoi de l'e-mail de vérification
                      </a>
                    </Link>
                  </div>
                  <div className="form-group">
                    <a className="col-12 card-button-networks text-center button button-long form-control">
                      <GoogleLogin
                        clientId={process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID}
                        buttonText="Connection via Google"
                        render={(renderProps) => (
                          <>
                            <img
                              src="/images/logo-google.png"
                              alt="Google Logo"
                            />
                            <button
                              onClick={renderProps.onClick}
                              disabled={renderProps.disabled}
                              style={{
                                border: 'none',
                                backgroundColor: 'transparent'
                              }}
                            >
                              Login with Google
                            </button>
                          </>
                        )}
                        onSuccess={responseGoogle}
                        onFailure={responseGoogle}
                        cookiePolicy={'single_host_origin'}
                      />
                    </a>
                  </div>
                </form>

                <hr />
                <div className="form-group d-flex justify-content-center">
                  <p className="text-center container-login__final-question">
                    Vous n'avez pas encore de compte?
                  </p>
                </div>
                <div className="d-flex justify-content-center">
                  <Link href="/register">
                    <a className="text-center link-black">S'inscrire</a>
                  </Link>
                </div>
              </div>
            ) : (
              <Loader />
            )}
          </div>
        </div>
      </div>
    </Container>
  )
}

export default Login
