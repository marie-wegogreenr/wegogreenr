import RegionsAbout from '@components/Regions/RegionsAbout'
import RegionsCardActivities from '@components/Regions/RegionsCardActivities'
import RegionsCardPlaces from '@components/Regions/RegionsCardPlaces'
import RegionsDescription from '@components/Regions/RegionsDescription'
import RegionsHero from '@components/Regions/RegionsHero'
import RegionsOthers from '@components/Regions/RegionsOthers'
import RegionsOutstanding from '@components/Regions/RegionsOutstanding'
import RegionsReviews from '@components/Regions/RegionsReviews'
import RegionsStyle from '@components/Regions/RegionsStyle'
import { Layout } from 'components'

export default function Regions() {
  return (
    <Layout from="regions">
      <RegionsHero />
      <RegionsDescription />
      <RegionsOutstanding />
      <RegionsCardPlaces />
      <RegionsAbout />
      <RegionsStyle />
      {/* <RegionsCardActivities /> */}
      <RegionsReviews />
      {/* <RegionsOthers /> */}
    </Layout>
  )
}
