/*  React  */
import React, { useEffect, useState } from 'react'
/*  Redux  */
import { useSelector } from 'react-redux'
/*  Next  */
import Link from 'next/link'
import { useRouter } from 'next/router'
import Image from 'next/image'
/*  Components  */
import Container from '@components/container'
import LeftBanner from '@components/Payment_Confirmation/LeftBanner'
import RoomCard from '@components/Payment_Confirmation/RoomCard'
import FullLoading from '@components/FullLoading/FullLoading'
/*  Services  */
import { confirmPayment } from 'services/reservationService'
/*  Helpers  */
import { eventListObjectTriggered } from 'helpers/dataLayer/lists'

const index = () => {
  /*  Router  */
  const router = useRouter()
  /*  Global states  */
  const user = useSelector((store) => store.user.user)
  const reservationData = useSelector((store) => store.checkoutReducer)
  const { reservation, paymentData } = reservationData
  /*  Local states  */
  const [delay, setDelay] = useState(false)
  const [eventSent, setEventSent] = useState(false)

  /*  Effects  */
  useEffect(() => {
    const timer = setTimeout(() => {
      setDelay(true)
    }, 1800)
    return () => clearTimeout(timer)
  }, [])

  useEffect(() => {
    if (!eventSent && Object.keys(reservation.additionals).length > 0) {
      const eventBody = {
        event: 'purchase',
        ...reservation,
        ...paymentData
      }
      const userInfo = {
        email: user.email,
        phone: user.phone
      }

      eventListObjectTriggered.purchase.callback(eventBody)
      eventListObjectTriggered.purchase.userCallback(userInfo)
      setEventSent(true)
    }
  }, [reservation])

  /*  Message data  */
  const data = {
    mainText: `Merci de votre réservation sur We Go GreenR !`,
    btnText: `Réserver mon prochain voyage!`
  }

  /*  Component validation  */
  if (typeof window !== 'undefined' && !reservation.room && delay) {
    window.location.replace(window.location.origin)
    return <>Loading</>
  } else {
    return (
      <div className="d-flex">
        <LeftBanner />
        <div className="d-flex flex-lg-column align-items-center justify-content-center w-100">
          <div className="card-white">
            <div className="h2-dashboard weight-bold mb-4">{data.mainText}</div>
            <RoomCard />
            <Link href="/">
              <button className="btn-secondary btn btn-block mt-3 py-3 w-100 text weight-bold">
                {data.btnText}
              </button>
            </Link>
          </div>
        </div>
      </div>
    )
  }
}

export default index
