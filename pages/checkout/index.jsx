/*  React  */
import React, { useEffect, useState } from 'react'
/*  Next  */
import Link from 'next/link'
import { useRouter } from 'next/router'
/*  Redux  */
import { useSelector, useDispatch } from 'react-redux'
/*  HOC  */
import withAuth from 'HOC/withAuthm'
/*  Components  */
import ReservationInfo from '@components/checkout/reservation_form/ReservationInfo'
import Container from '@components/container'
import Resume from '@components/checkout/resume'
import Conditions from './../../components/checkout/aditional_info/Conditions'
/*  Hooks  */
import { useToast } from 'hooks/useToast'
/*  Dependencies  */
import 'react-day-picker/lib/style.css'
import moment from 'moment'
/*  Services  */
import { getDaysAndWeekends } from 'helpers/timeHelper'
import {
  createReservation,
  generatePaymentLink
} from 'services/reservationService'
import { savePaymentData } from '@ducks/checkoutDuck'
import { eventListObjectTriggered } from 'helpers/dataLayer/lists'
import { ChevronLeft } from '@components/Icons'
import { cleanGiftCardData } from '@ducks/giftCard/actions'
import PracticalInfo from '@components/Hebergements/Single/Sections/PracticalInfo'
import { getData } from 'utils'
import { endpoints } from 'constants/index'

const index = () => {
  /*  Next & custom hooks  */
  const router = useRouter()
  const dispatch = useDispatch()
  const [notify] = useToast()
  /*  Global states  */
  const validRes = useSelector((store) => store.checkoutReducer.valid)
  const resData = useSelector((store) => store.checkoutReducer.reservation)
  const userId = useSelector((store) => store.user.user.id)
  const giftCardObject = useSelector((store) => store.giftCardReducer)
  /*  Local states  */
  const [servicesData, setServicesData] = useState([])
  /*  Effects  */
  useEffect(() => {
    const fetchData = async () => {
      if (resData) {
        const options = {
          method: 'GET'
        }
        const establishmentServices = await getData({
          ...options,
          url: `${endpoints.ESTABLISHMENT_SERVICES}${resData?.establishment?.id}`
        }).catch(() => [])

        setServicesData([...establishmentServices])
      }
    }

    fetchData()
  }, [resData])

  /*  Functions  */
  const handlePayment = async () => {
    const {
      room: { id },
      checkIn,
      checkOut,
      total_price,
      children,
      adults,
      discount
    } = resData

    const [weekdays, weekends] = getDaysAndWeekends(checkIn, checkOut)

    const reservationBody = {
      user_id: userId,
      room_id: id,
      status: 0,
      number_of_adults: adults,
      number_of_children: children,
      room_price: total_price - discount,
      arrival_date: moment(checkIn).format('YYYY-MM-DD'),
      departure_date: moment(checkOut).format('YYYY-MM-DD'),
      calendar_id: ''
    }

    const _data = {
      ...reservationBody,
      ...giftCardObject
    }

    const { code, data: reservationData } = await createReservation(_data)
    if (code !== 200) {
      return notify(
        'il y a eu un problème lors de la création de la réservation, veuillez réessayer plus tard.',
        'error'
      )
    }

    const { reservation_id } = reservationData

    const eventBody = {
      event: 'begin_checkout',
      reservation_id,
      ...resData
    }

    /*  Event for Google Tag Manager */
    eventListObjectTriggered.beginCheckout.callback(eventBody)
    const purchaseBody = {
      reservation_id,
      total_nigths: weekdays + weekends
    }
    const { code: paymentCode, data: paymentData } = await generatePaymentLink(
      purchaseBody
    )
    initPayment(
      paymentCode,
      paymentData,
      reservation_id,
      purchaseBody.total_nigths
    )
  }

  const initPayment = (code, paymentData, reservation_id, total_nigths) => {
    if (code !== 200)
      return notify(
        'il y a eu un problème pour générer le paiement, veuillez réessayer plus tard.',
        'error'
      )
    const { checkout_url, payment_intent } = paymentData
    dispatch(
      savePaymentData({
        reservation_id,
        total_nigths,
        payment_intent
      })
    )
    dispatch(cleanGiftCardData())
    window.location.replace(checkout_url)
  }

  const handleRedirectClick = () => {
    router.push(`/hebergements/${resData?.room?.slug}`)
  }

  if (resData) {
    return (
      <Container>
        <div className="container-xxl">
          <div className="my-5">
            <div onClick={handleRedirectClick} className="link-go-back">
              <ChevronLeft className="link-go-back__icon" />
              <span className="link-go-back__text">{'Retour'}</span>
            </div>
            <h1 className="h1--alternative">Confirmation de votre séjour</h1>
          </div>
          <div className="row">
            <div className="col-12 col-lg-7">
              <div className="card-white">
                <ReservationInfo />
              </div>
              <div className="card-white">
                <PracticalInfo
                  resData={resData}
                  services={servicesData}
                  stablishment={resData?.establishment}
                  from="checkout"
                />
              </div>
              {/* <Conditions /> */}
            </div>
            <div className="col-12 col-lg-4">
              <div className="card-white card-white__checkout p-4 shdow-1">
                <Resume />
                <button
                  onClick={handlePayment}
                  className="btn btn-secondary btn-block btn-cal br-10 py-4 mt-3 weight-bold text"
                  disabled={!validRes}
                >
                  Valider et payer
                </button>
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }

  return <>Loading</>
}

export default withAuth(index)
