import React from 'react'
import { consoleLog } from '/utils/logConsole'

import Container from '../components/container'

import Image from 'next/image'
import Link from 'next/link'
import Router from 'next/router'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { toast } from 'react-toastify'
import { LogotypeExtended } from '@components/Icons/LogotypeExtended'

class ForgetPasswordForm extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      password: '',
      re_password: '',
      token: '',
      email: '',
      isPasswordValidate: false
    }

    library.add(faCheckCircle)

    this.handleRecoverPassword = this.handleRecoverPassword.bind(this)
    this.handleCheckPassword = this.handleCheckPassword.bind(this)
  }

  componentDidMount() {
    const queryString = window.location.search
    const urlParams = new URLSearchParams(queryString)
    this.setState(
      {
        ...this.state,
        token: urlParams.get('token'),
        email: urlParams.get('email')
      },
      () => {
        //consoleLog(this.state)
      }
    )
  }

  async handleRecoverPassword(event) {
    event.preventDefault()
    if (this.state.isPasswordValidate == false) {
      toast.error(`Le mot de passe n'est pas valide.`, {
        position: toast.POSITION.BOTTOM_LEFT
      })
    } else {
      const res = await fetch(
        process.env.NEXT_PUBLIC_API_URL + 'auth/password/reset',
        {
          body: JSON.stringify({
            token: this.state.token,
            email: this.state.email,
            password: event.target.password.value,
            password_confirmation: event.target.re_password.value
          }),
          headers: {
            'Content-Type': 'application/json'
          },
          method: 'POST'
        }
      )

      const result = await res.json()
      const status = await res.status
      //consoleLog(result);
      //consoleLog(status);
      if (status == 200) {
        toast.success('Changement de mot de passe réussi', {
          position: toast.POSITION.BOTTOM_LEFT
        })
        Router.push('/login')
      } else if (status == 403) {
        toast.error(`L'email n'existe pas`, {
          position: toast.POSITION.BOTTOM_LEFT
        })
      } else {
        toast.error(`Une erreur s'est produite, réessayez plus tard`, {
          position: toast.POSITION.BOTTOM_LEFT
        })
      }
    }
  }

  handleCheckPassword(event) {
    const password_value = event.target.value
    this.setState({ ...this.state, password: password_value }, () => {
      let strongRegex = new RegExp(
        '^(?=.{14,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$',
        'g'
      )
      let mediumRegex = new RegExp(
        '^(?=.{10,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$',
        'g'
      )
      let enoughRegex = new RegExp('(?=.{8,}).*', 'g')
      let pwd = this.state.password
      if (pwd.length == 0) {
        this.setState({
          ...this.state,
          password_validation: 'Type Password',
          password_style: 'red'
        })
      } else if (false == enoughRegex.test(pwd)) {
        this.setState({
          ...this.state,
          password_validation: 'More Characters',
          password_style: 'red'
        })
      } else if (strongRegex.test(pwd)) {
        this.setState({
          ...this.state,
          password_validation: 'Strong!',
          password_style: 'green',
          isPasswordValidate: true
        })
      } else if (mediumRegex.test(pwd)) {
        this.setState({
          ...this.state,
          password_validation: 'Medium!',
          password_style: 'orange',
          isPasswordValidate: true
        })
      } else {
        this.setState({
          ...this.state,
          password_validation: 'Weak!',
          password_style: 'red'
        })
      }
    })
  }

  render() {
    return (
      <Container>
        <div className="container-fluid container-login__wrapper">
          <div className="row">
            <div className="col-12 col-xl-8 container-fluid container-login">
              <Link href="/">
                <a>
                  <LogotypeExtended
                    fill="white"
                    className="container-login__logo"
                  />
                </a>
              </Link>
              <div className="card-white container-login__card">
                <h1
                  className="container-login__title"
                  style={{ fontSize: 30 + 'px' }}
                >
                  <span className="container-login__title-span">
                    Renseignez votre
                  </span>
                  <span className="container-login__title-span">
                    nouveau mot de passe
                  </span>
                </h1>
                <form className="form" onSubmit={this.handleRecoverPassword}>
                  <div className="form-group">
                    <label
                      htmlFor="password"
                      className="container-login__label container-login__label--bold"
                    >
                      Nouveau mot de passe
                    </label>
                    <input
                      type="password"
                      className="form-control container-login__input"
                      id="password"
                      name="password"
                      placeholder="Saisissez votre mot de passe"
                      onChange={this.handleCheckPassword}
                      defaultValue={this.state.password}
                      value={this.state.password}
                      required
                    />
                  </div>
                  <div className="form-group">
                    <label
                      htmlFor="re_password"
                      className="container-login__label container-login__label--bold"
                    >
                      Confirmer le nouveau mot de passe
                    </label>
                    <input
                      type="password"
                      className="form-control container-login__input"
                      id="re_password"
                      name="re_password"
                      placeholder="Répetez votre mot de passe"
                      defaultValue={this.state.re_password}
                      required
                    />
                  </div>
                  <div className="form-group card-background-gray">
                    <div className="container-card-gray">
                      <p>
                        Fiabilité du mot de passe :{' '}
                        <strong>
                          <span style={{ color: this.state.password_style }}>
                            {this.state.password_validation}
                          </span>
                        </strong>
                      </p>
                      <ul>
                        <li>
                          <FontAwesomeIcon
                            icon="check-circle"
                            size="1.5x"
                            className="icon-check"
                          />{' '}
                          Ne doit pas contenir votre nom ni votre adresse e-mail
                        </li>
                        <li>
                          <FontAwesomeIcon
                            icon="check-circle"
                            size="1.5x"
                            className="icon-check"
                          />{' '}
                          Au moins 8 caractères
                        </li>
                        <li>
                          <FontAwesomeIcon
                            icon="check-circle"
                            size="1.5x"
                            className="icon-check"
                          />{' '}
                          Contient un chiffre ou un symbole
                        </li>
                      </ul>
                    </div>
                  </div>
                  <button type="submit" className="button button-long">
                    Récupérez votre mot de passe
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

export default ForgetPasswordForm
