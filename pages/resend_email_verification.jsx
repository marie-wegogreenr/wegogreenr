import { useState } from 'react'
import Link from 'next/link'
import Container from '../components/container'
import { resendVerification } from 'services/userServices'
import { useToast } from 'hooks/useToast'
import { LogotypeExtended } from '@components/Icons/LogotypeExtended'

function Resend() {
  /*  Local states  */
  const [email, setEmail] = useState({ email: '' })
  /*  Custom hooks  */
  const [notify] = useToast()
  /*  Handle functions  */
  const handleChange = (e) => {
    setEmail({ [e.target.name]: e.target.value })
  }
  const validateResponse = (res) => {
    if (res.code === 200) {
      if (res.data.Message === 'Email dont registered') {
        notify(`Cet email n'est pas enregistré`, 'error')
      } else if (res.data.Message === 'Email sended') {
        notify(`L'e-mail a été envoyé avec succès`, 'success')
      } else {
        notify(`Il y a eu une erreur. Essayez à nouveau`, 'error')
      }
    } else {
      notify(`Il y a eu une erreur avec l'email`, 'error')
    }
  }
  const handleSubmit = async (e) => {
    e.preventDefault()
    const body = {
      email: email.email
    }
    const res = await resendVerification(body)
    validateResponse(res)
  }

  return (
    <Container>
      <div className="container-fluid container-login__wrapper container-login__wrapper--short">
        <div className="row">
          <div className="col-12 col-xl-8 container-fluid container-login">
            <Link href="/">
              <a>
                <LogotypeExtended
                  fill="white"
                  className="container-login__logo"
                />
              </a>
            </Link>
            <div className="card-white container-resend__card-white">
              <form className="form" onSubmit={handleSubmit}>
                <div className="form-group">
                  <label htmlFor="email">Adresse email</label>
                  <input
                    type="email"
                    className="form-control container-resend__input"
                    id="email"
                    name="email"
                    aria-describedby="emailHelp"
                    placeholder="Saisissez votre adresse email"
                    onChange={handleChange}
                    required
                  />
                  <button
                    type="submit"
                    className="button button-long container-resend__button"
                  >
                    Renvoi de l'e-mail de vérification
                  </button>
                  <Link href="/login">
                    <a className="text-center link-black container-resend__link">
                      Retourner à la connexion
                    </a>
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Container>
  )
}

export default Resend
