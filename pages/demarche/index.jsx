import Container from '@components/container'
import CardGrid from '@components/Demarche/CardGrid'
import Concept from '@components/Demarche/Concept'
import Greenscore from '@components/Demarche/Greenscore'
import Hero from '@components/Demarche/Hero'
import MainGrid from '@components/Demarche/MainGrid'
import Presse from '@components/Demarche/Presse'
import Quotes from '@components/Demarche/Quotes'
import React from 'react'
import Axeptio from '@components/Axeptio'
import SinglePageLayout from '@components/layouts/SinglePageLayout'
import Footer from '@components/footer'
import NavbarOptions from '@components/NavigationComponents/OptionsNav/NavbarOptions'
import { Manifesto } from '@components/Demarche/Manifesto'
import { RepenserBanner } from '@components/Demarche/RepenserBanner'
import { ConceptWGG } from '@components/Demarche/ConceptWGG'
import { Layout } from 'components'

const index = () => {
  return (
    <Layout>
      <SinglePageLayout>
        {/* <NavbarOptions /> */}
        <Hero />
        <Manifesto />
        <RepenserBanner />
        <div className="bg-crema-light">
          <Concept />
          <div className=""></div>
          <CardGrid />
        </div>
        <Greenscore />
        <div className="bg-crema-soft py-5">
          <Presse margin={false} />
        </div>
        <Axeptio />
        {/* <Footer /> */}
      </SinglePageLayout>
    </Layout>
  )
}

export default index
