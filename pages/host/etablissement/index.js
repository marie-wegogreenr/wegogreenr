import * as React from 'react'
import { consoleLog } from '/utils/logConsole'
import { Layout } from 'components/'
import { Navbar } from 'components/host/establishment'
import { hostEstablishmentTabs } from 'fixtures'

import { connect } from 'react-redux'
import { MainTitle } from '@components/dashboard_host/etablissement/MainTitle'

class Etablissement extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user_data: ''
    }
  }

  static async getInitialProps(ctx) {
    return { id: ctx.query.id }
  }
  render() {
    const { establishment } = this.props.user

    return (
      <Layout>
        {/* CONTAINER */}
        <section
          className="host_establishment__container mb-5"
          style={{ marginTop: '8.1rem' }}
        >
          {/* WRAPPER */}
          <div className="container-xxl">
            {/* CONTENT */}
            <div className="host_establishment__content">
              <div
                className="d-flex justify-content-between align-items-center"
                style={{ width: '75%' }}
              >
                <h1 className="color-primary">Établissement</h1>
              </div>

              <div className="row w-100 mx-0">
                {/* TABS */}
                <div className="col-12 px-0 d-flex align-items-center mb-3">
                  <Navbar />
                  <MainTitle />
                </div>

                {/* CONTENTS */}
                <div className="col-12 col-lg-9 px-0">
                  <div className="tab-content" id="pills-tabContent">
                    {hostEstablishmentTabs.map((item, index) => {
                      const { Body, id } = item
                      return (
                        <div
                          key={index}
                          className={`tab-pane fade show ${
                            index ? '' : 'active'
                          }`}
                          id={`pills-${id}`}
                          role="tabpanel"
                          aria-labelledby={`pills-${id}-tab`}
                        >
                          <Body
                            from="host"
                            id_etablisement={this.props.user?.establishment?.id}
                          />
                        </div>
                      )
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </Layout>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}

export default connect(mapStateToProps, null)(Etablissement)
