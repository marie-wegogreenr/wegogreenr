import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

import Cookies from 'universal-cookie'

/* Next */
import Router from 'next/router'
import Link from 'next/link'

/* Redux */
import { connect } from 'react-redux'
import { getReservationsByHostId } from '@ducks/host/reservationsByHostId'

/* Components */
import { Layout, Loader } from 'components/'
import {
  Hebergement,
  Message,
  ReservationToValidate,
  SectionLayout
} from 'components/host/dashboard'
import { Star } from '@components/Icons'

/* Constants */
import { routes } from 'constants/index'
import { getRooms } from 'services/etablissementService'
import { getUserData } from 'services/userServices'
import { setUserData, setRole } from '@ducks/userDuck'

class Dashboard extends React.Component {
  constructor(props) {
    super(props)
    const cookies = new Cookies()
    this.props.reservationsByHostId.data = []
    this.state = {
      token_user: cookies.get('tk_user'),
      user_data: '',
      rooms: []
    }
  }

  async componentDidMount() {
    this.props.setRole('host')
    let _this = this
    const cookies = new Cookies()
    if (this.props.reservationsByHostId.data.length === 0) {
      this.props.getReservationsByHostId(this.props.hostId)
    }
    const [user, rooms] = await Promise.all([
      getUserData(),
      getRooms(this.props.establishmentId)
    ])

    this.props.setUserData(user)
    this.setState({ ...this.state, rooms, user_data: this.props.user })
  }

  flatReservationsArray(acc, cur) {
    return [...acc, ...cur]
  }

  // statuses request
  reservationsRequestLoading() {
    return (
      this.props.reservationsByHostId.status === 'LOADING' || !this.props.hostId
    )
  }

  reservationsRequestSuccess() {
    return this.props.reservationsByHostId.status === 'SUCCESS'
  }

  async handleStripeConect() {
    const cookies = new Cookies()
    const options = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      }
    }

    fetch(`${process.env.NEXT_PUBLIC_API_URL}becomeToHost`, options)
      .then((res) => res.json())
      .then((data) => {
        if (data.login_url) {
          window.open(`${data.login_url}`, '_blank')
        } else {
          window.open(`${data.url}`, '_blank')
        }
        //consoleLog(data)
      })
      .catch((err) => consoleLog(err))
  }

  render() {
    const origin = this.props?.user?.establishment?.origin || null
    return (
      <Layout>
        {/* CONTAINER */}
        <section className="host-dashboard__container">
          {/* WRAPPER */}
          <div className="container-xxl">
            {/* CONTENT */}
            <div className="host-dashboard__content">
              <h1 className="color-primary mb-5" id="bonjour">
                Bonjour <span>{this.state.user_data.name}</span>
              </h1>

              {/* GRID */}
              <div
                className={`${
                  origin === 4
                    ? 'host-dashboard__grid--ar'
                    : 'host-dashboard__grid'
                }`}
              >
                <SectionLayout
                  title="Mes réservations"
                  route={routes.HOST_MY_RESERVATIONS}
                >
                  {this.reservationsRequestLoading() && <Loader />}
                  {this.reservationsRequestSuccess() &&
                  this.props.reservationsByHostId.data.length > 0
                    ? this.props.reservationsByHostId.data
                        ?.reduce(this.flatReservationsArray, [])
                        .slice(0, 5)
                        .map((reservation) => {
                          return (
                            <ReservationToValidate
                              key={reservation.id}
                              finalDate={reservation.departure_date}
                              initialDate={reservation.arrival_date}
                              name={reservation.room.public_name}
                              peopleCapacity={reservation.number_of_adults}
                              bedType={reservation.type_bed}
                              imageRoom={
                                reservation.principal_image == null
                                  ? 'https://picsum.photos/200'
                                  : 'https://wegogreenr-laravel.s3.eu-west-2.amazonaws.com/' +
                                    reservation.principal_image
                              }
                              reservation={reservation}
                            />
                          )
                        })
                    : !this.reservationsRequestLoading() && (
                        <div className="w-100 text-left py-5 mt-5">
                          <p className="subtitle-dashboard weight-bold font-fraunces">
                            Vous n'avez pas encore de réservation
                          </p>
                          <Link href="/">
                            <button className="btn btn-secondary btn-home-1 my-5 weight-semibold paragraph">
                              Je réserve mon prochain séjour
                            </button>
                          </Link>
                        </div>
                      )}
                </SectionLayout>

                {origin !== 4 && (
                  <>
                    <SectionLayout
                      title="Hébergements"
                      route={routes.HOST_MY_HEBERGEMENTS}
                    >
                      {this.state.rooms?.map((room) => {
                        return (
                          <Hebergement
                            name={room.internal_name}
                            price={room.basic_price}
                            id={room.id}
                            principal_image={room.principal_image}
                            active={room.active}
                          />
                        )
                      })}
                    </SectionLayout>
                    <button
                      className="button text-center host-dashboard__main-btn"
                      type="button"
                      onClick={this.handleStripeConect}
                    >
                      Saisissez vos informations bancaires
                    </button>
                  </>
                )}
              </div>
            </div>
          </div>
        </section>
      </Layout>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getReservationsByHostId: (hostId) => {
      dispatch(getReservationsByHostId(hostId))
    },
    setUserData,
    setRole
  }
}

const mapStateToProps = (state) => {
  const {
    user: { user },
    reservationsByHostId
  } = state

  return {
    hostId: user?.id || null,
    user: state.user.user,
    establishmentId: state.establishment.id,
    reservationsByHostId
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
