import * as React from 'react'
/* Components */
import { Layout, Loader } from 'components/'
import { Header, Pagination, Table } from 'components/host/myReservations'
/* Redux */
import { useDispatch, useSelector } from 'react-redux'
import { getReservationsByHostId } from '@ducks/host/reservationsByHostId'
import { FilterReservations } from 'helpers/reservationsHelper'

function MyReservations() {
  /*  Global states */
  const {
    reservationsByHostId = [],
    user: { user }
  } = useSelector((state) => state)

  const hostId = user?.id || null

  const dispatch = useDispatch()

  /*  Local states  */
  const [pagination, setPagination] = React.useState({
    perPage: 5,
    page: 0
  })
  const [reservationsData, setReservationsData] =
    React.useState(reservationsByHostId)

  const [filterBy, setFilterBy] = React.useState('toutes')

  /*  Effects  */
  React.useEffect(() => {
    setPagination({
      ...pagination,
      page: 0
    })
  }, [filterBy])

  React.useEffect(() => {
    if (reservationsByHostId.status === 'IDLE') {
      hostId && dispatch(getReservationsByHostId(hostId))
    }
  }, [hostId])

  React.useEffect(() => {
    if (reservationsData) setReservationsData(reservationsByHostId)
  }, [reservationsByHostId])

  /*  Computed  */
  const flatReservation = React.useMemo(() => {
    if (reservationsData.data !== null) {
      return reservationsData.data
        ?.reduce((acc, cur) => {
          return [...acc, ...cur]
        }, [])
        .filter((r) => {
          return FilterReservations(r, filterBy)
        })
        .sort(function (a, b) {
          return (
            Math.abs(Date.now() - new Date(a.arrival_date)) -
            Math.abs(Date.now() - new Date(b.arrival_date))
          )
        })
    } else {
      return []
    }
  }, [reservationsData.data, filterBy, reservationsData.changes])

  /*  Helper functions  */
  const nextPage = () => {
    setPagination({
      ...pagination,
      page: (pagination.page += 1)
    })
  }

  const prevPage = () => {
    setPagination({
      ...pagination,
      page: (pagination.page -= 1)
    })
  }

  const requestsLoading = () => {
    return reservationsData.status === 'LOADING' || !hostId
  }

  return (
    <Layout>
      {/* CONTAINER */}
      <section
        className="host_establishment__container mb-5"
        style={{ marginTop: '7.5rem' }}
      >
        {/* WRAPPER */}
        <div className="container-xxl">
          {/* HEADER */}
          <Header filterBy={filterBy} setFilterBy={setFilterBy} />
          {/* CONTENT */}
          <div>
            {requestsLoading() && <Loader />}

            {reservationsData.status === 'SUCCESS' && (
              <Table
                reservations={flatReservation.slice(
                  pagination.page * pagination.perPage,
                  pagination.page * pagination.perPage + pagination.perPage
                )}
              />
            )}
          </div>

          {flatReservation?.length > 5 ? (
            <Pagination
              prevPage={prevPage}
              nextPage={nextPage}
              currentPage={pagination.page}
              max={Math.ceil(flatReservation?.length / 5)}
            />
          ) : null}
        </div>
      </section>
    </Layout>
  )
}

export default MyReservations
