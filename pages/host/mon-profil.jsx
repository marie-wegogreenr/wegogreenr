import React, { useEffect } from 'react'
import { consoleLog } from '/utils/logConsole'
import Container from '@components/container'
import HostStatistics from '@components/dashboard_host/compte/HostStatistics'
import UserInfo from '@components/dashboard_host/compte/UserInfo'
import PresentationInput from './../../components/dashboard_host/compte/PresentationInput'
import NewsletterCard from './../../components/dashboard_host/compte/NewsletterCard'
//REDUX
import { useDispatch, useSelector } from 'react-redux'
import { getHostEstablishmentAction, getHostRoomsAction } from '@ducks/userDuck'
import { getReservationsByHostId } from '@ducks/host/reservationsByHostId'

const monProfil = () => {
  const dispatch = useDispatch()
  const store = useSelector((store) => store.user)

  useEffect(() => {
    if (store.user) {
      dispatch(getHostEstablishmentAction())
    }
  }, [store.user.id])

  useEffect(() => {
    if (
      store.user?.establishment !== undefined &&
      store.user.establishment.length
    ) {
      dispatch(getHostRoomsAction())
      dispatch(getReservationsByHostId(store?.user?.id))
    }
  }, [store.user.establishment])

  return (
    <Container>
      <div className="container-xxl">
        <h1 className="h1 weight-bold mt-4">Mon Profil</h1>
        <div className="row">
          <div className="col-12 col-lg-12 container__user-presentation">
            <UserInfo />
            <PresentationInput />
          </div>
          {/* <div className="col-12 col-lg-4 mt-4"> */}
          {/* <HostStatistics /> */}
          {/* <NewsletterCard /> */}
          {/* <CardWithIcon
              icon={UserExplorer()}
              title={`Hello ${userName} !`}
              content="Vos informations personnelles sont utilisées lors de vos réservations afin de gagner du temps et éviter de les ressaisir à chaque fois."
            />
            <CardWithIcon
              icon={Email()}
              title="Besoin de votre facture ?"
              content="Retouvez ces documents sur votre compte "
              link="Acceder a mon compte"
              redirect="/user/mes-reservations"
            /> */}
          {/* </div> */}
        </div>
      </div>
    </Container>
  )
}

export default monProfil
