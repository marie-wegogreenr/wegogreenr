import React from 'react'
import { consoleLog } from '/utils/logConsole'

import Container from '../../components/container'

import Cookies from 'universal-cookie'

import Router from 'next/router'
import Link from 'next/link'

class MesMessager extends React.Component {
  constructor(props) {
    super(props)
    const cookies = new Cookies()
    this.state = {
      token_user: cookies.get('tk_user'),
      user_data: ''
    }
  }

  async componentDidMount() {
    let _this = this
    const cookies = new Cookies()
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        //consoleLog(response.status);
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })
  }

  render() {
    return (
      <Container>
        <h3>Mes messager</h3>
      </Container>
    )
  }
}

export default MesMessager
