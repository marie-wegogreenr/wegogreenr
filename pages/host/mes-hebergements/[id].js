import * as React from 'react'
import { connect } from 'react-redux'
/* Components */
import Container from '../../../components/container'
import Info from 'components/dashboard_host/hebergements/info'
import Lits from 'components/dashboard_host/hebergements/lits'
import CalendarHebergement from '@components/dashboard_host/hebergements/calendar'
import Equipments from 'components/dashboard_host/hebergements/equipments'
import Prix from '@components/dashboard_host/hebergements/prix'
import Photos from 'components/dashboard_host/hebergements/photos'
import Reservation from 'components/dashboard_host/hebergements/reservation'
import Summary from 'components/dashboard_host/hebergements/summary'
import Cookies from 'universal-cookie'
/* Next */
import { withRouter } from 'next/router'
import Link from 'next/link'
import { setHebergement } from '@ducks/hebergements/actions'
import Router from 'next/router'
import Title from '@components/dashboard_host/hebergements/Title'
/* Fixture */
const tabs = [
  {
    id: 'info',
    Component: Info,
    text: 'Infos générales'
  },
  {
    id: 'beds',
    Component: Lits,
    text: 'Lits et chambres'
  },
  {
    id: 'calendar',
    Component: CalendarHebergement,
    text: 'Calendrier et disponibilités'
  },
  {
    id: 'equipments',
    Component: Equipments,
    text: 'Équipements'
  },
  {
    id: 'price',
    Component: Prix,
    text: 'Prix'
  },
  {
    id: 'photos',
    Component: Photos,
    text: 'Photos'
  }
  /* {
    id: 'reservation',
    Component: Reservation,
    text: 'Reservation'
  } */
]

class Hebergement extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: '',
      room: [],
      image_summary: null,
      id_tab: 'info',
      showSummary: false,
      updateCalendar: true
    }
  }

  static async getInitialProps(ctx) {
    return { id: ctx.query.id }
  }

  handleChangeTab = async (id) => {
    this.setState({ ...this.state, showSummary: false }, () => {
      this.setState({ ...this.state, id_tab: id, showSummary: true })
    })
  }

  handleChangeAvailability = async () => {
    await this.handleInitialInfo()
    this.setState({ ...this.state, showPopup: !this.state.showPopup })
  }

  async componentDidMount() {
    const cookies = new Cookies()

    //Get information room
    fetch(process.env.NEXT_PUBLIC_API_URL + 'rooms/' + this.props.id, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then((res) => {
        return res.json()
      })
      .then((room) => {
        this.setState({ ...this.state, room: room })
        this.props.setHebergement(room[0])
      })

    //Get images room
    fetch(process.env.NEXT_PUBLIC_API_URL + 'room-images/' + this.props.id, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then((res) => {
        return res.json()
      })
      .then((images) => {
        let image_summary = ''
        images.forEach((image) => {
          if (image.order == 1) {
            image_summary =
              'https://wegogreenr-laravel.s3.eu-west-2.amazonaws.com/' +
              image.image.url
          }
        })
        this.setState({
          ...this.state,
          image_summary: image_summary,
          showSummary: true
        })
      })
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevState.room !== this.state.room) {
      if (
        this.state.room[0].establishment_id !== this.props.idStablishmentSession
      ) {
        Router.push('/host/mes-hebergements')
      }
    }
  }

  render() {
    return (
      <Container>
        {/* CONTAINER */}
        <div className="mb-5 mt-5">
          {/* WRAPPER */}
          <div className="container-xxl">
            <div className="room__title">
              <div className="room__title-wrapper">
                <Link href="/host/mes-hebergements">
                  <a>&lsaquo; Retour a la liste</a>
                </Link>
                <h1 className="main-room__title mb-0 font-fraunces color-primary h2 weight-bold">
                  {this.state.room.length > 0 &&
                    this.state.room[0].internal_name}
                </h1>
              </div>
              {this.state.room.length > 0 && (
                <Link
                  href={`/host/mes-hebergements/preview/${this.state.room[0].slug}`}
                >
                  <a
                    target="_blank"
                    className="room__title-wrapper-btn--move-down"
                  >
                    <button className="btn btn-outline-secondary btn-home-1 mr-5 room__title-wrapper-btn--move-down-wide">
                      Aperçu de l'annonce
                    </button>
                  </a>
                </Link>
              )}
            </div>
            <div className="">
              <ul
                className="nav nav-pills mb-3 establishment-tab"
                id="pills-tab"
                role="tablist"
              >
                {tabs.map(({ text, id }, index) => (
                  <li
                    key={id}
                    className="nav-item"
                    role="presentation"
                    onClick={() => {
                      this.handleChangeTab(id)
                    }}
                  >
                    <button
                      className={`nav-link ${!index ? 'active' : ''}`}
                      id={`${id}-tab`}
                      data-bs-toggle="pill"
                      data-bs-target={`#pills-${id}`}
                      type="button"
                      role="tab"
                      aria-controls={id}
                      aria-selected="true"
                    >
                      {text}
                    </button>
                  </li>
                ))}
              </ul>

              <div
                className={`row w-100 mx-0 mt-3 d-flex ${
                  this.state.id_tab !== 'calendar' ? 'flex-wrap-reverse' : ''
                } `}
              >
                <div
                  className={
                    this.state.id_tab === 'calendar'
                      ? 'col-12 col-xl-8 p-0'
                      : 'col-12 col-lg-8 p-0'
                  }
                >
                  <div className="tab-content" id="pills-tabContent">
                    {tabs.map(({ Component, id }, index) => (
                      <div
                        key={id}
                        className={`tab-pane fade show ${
                          index ? '' : 'active'
                        }`}
                        id={`pills-${id}`}
                        role="tabpanel"
                        aria-labelledby={`${id}-tab`}
                      >
                        <Component id={this.props.id} room={this.state.room} />
                      </div>
                    ))}
                  </div>
                </div>
                <div
                  className={
                    this.state.id_tab === 'calendar'
                      ? 'col-12 col-xl-4 mt-4'
                      : 'col-12 col-lg-4 mt-4'
                  }
                >
                  {this.state.room.length > 0 && this.state.showSummary && (
                    <Summary
                      image={this.state.image_summary}
                      data_hebergement={this.state.room}
                      id_tab={this.state.id_tab}
                      id_hebergement={this.props.id}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Container>
    )
  }
}

const mapStateToProps = (state) => ({
  idStablishmentSession: state.user.user.establishment?.id
})

const mapDispatchToProps = {
  setHebergement
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Hebergement))
