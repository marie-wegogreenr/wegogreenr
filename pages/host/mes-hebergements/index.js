import React from 'react'
import Container from '../../../components/container'
import Cookies from 'universal-cookie'
import Router from 'next/router'
import { RoomCard } from '@components/host/myRooms'
import { Loader } from 'components/'
import { toast } from 'react-toastify'
class Index extends React.Component {
  constructor(props) {
    super(props)
    const cookies = new Cookies()
    this.state = {
      token_user: cookies.get('tk_user'),
      user_data: '',
      stablishments: [],
      rooms: [],
      isShowContent: false
    }

    this.handleNewHebergement = this.handleNewHebergement.bind(this)
    this.handleGetRooms = this.handleGetRooms.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    let _this = this
    //Get user data
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get establishment
    const res_stablishments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments = await res_stablishments.json()
    this.setState({ ...this.state, stablishments: json_stablishments })

    this.handleGetRooms()
  }

  async handleGetRooms() {
    this.setState({ ...this.state, isShowContent: false }, async () => {
      const cookies = new Cookies()
      //Get rooms
      const res_rooms = await fetch(
        process.env.NEXT_PUBLIC_API_URL +
          'establishment-rooms/' +
          this.state.stablishments[0].id,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + cookies.get('tk_user')
          },
          method: 'GET'
        }
      )
      const json_rooms = await res_rooms.json()
      this.setState({ ...this.state, rooms: json_rooms, isShowContent: true })
    })
  }

  async handleNewHebergement() {
    event.preventDefault()
    const cookies = new Cookies()

    var result = []
    var characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    var charactersLength = characters.length
    for (var i = 0; i < 32; i++) {
      result.push(
        characters.charAt(Math.floor(Math.random() * charactersLength))
      )
    }
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'rooms', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      body: JSON.stringify({
        slug: result.join('') + '-' + Math.floor(Math.random() * 101),
        min_nigth: 1,
        max_nigth: 180
      }),
      method: 'POST'
    })
      .then((response) => {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(async (room) => {
        toast.success('Hebergement créé', {
          position: toast.POSITION.BOTTOM_LEFT
        })
        const res_rooms = await fetch(
          process.env.NEXT_PUBLIC_API_URL +
            'establishment-rooms/' +
            this.state.stablishments[0].id,
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'GET'
          }
        )
        const json_rooms = await res_rooms.json()
        this.setState({ ...this.state, rooms: json_rooms })
      })
  }

  render() {
    return (
      <Container>
        <section className="host_my_rooms__container host_my_rooms__container-rooms bg-yellow-light mb-5">
          <div className="container-xxl">
            <div className="mb-5">
              <h1 className="weight-bold">Mes Hébergements</h1>
            </div>
            <div className="host_my_rooms__wrapper">
              {this.state.isShowContent ? (
                <div
                  className={
                    this.state.rooms?.length > 2
                      ? 'host_my_rooms__grid'
                      : 'host_my_rooms__grid--simple'
                  }
                >
                  {this.state.rooms.map((room) => {
                    const {
                      internal_name,
                      people_capacity,
                      basic_price,
                      id,
                      active,
                      outstanding,
                      principal_image,
                      slug,
                      type_id
                    } = room
                    return (
                      <RoomCard
                        key={id}
                        active={active}
                        id={id}
                        className="w-100"
                        internalName={internal_name}
                        peopleCapacity={people_capacity}
                        basicPrice={basic_price}
                        outstanding={outstanding}
                        principal_image={principal_image}
                        from={'host'}
                        slug={slug}
                        type_id={type_id}
                      />
                    )
                  })}
                  <div className="host_my_rooms__card--add-room">
                    <div className="text-center">
                      <button
                        type="button"
                        className="btn btn-secondary rounded-circle shadow"
                        onClick={this.handleNewHebergement}
                      >
                        +
                      </button>

                      <p className="mb-0 mt-3">
                        <strong>Ajouter un hébergement</strong>
                      </p>
                    </div>
                  </div>
                </div>
              ) : (
                <Loader />
              )}
            </div>
          </div>
        </section>
      </Container>
    )
  }
}

export default Index
