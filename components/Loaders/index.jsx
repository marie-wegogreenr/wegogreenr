import React, { useEffect, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
import { createPortal } from 'react-dom'

import wggLogo from 'public/images/logo.svg'
import Image from 'next/image'

const index = () => {
  const [mounted, setMounted] = useState(false)
  useEffect(() => {
    setMounted(true)

    return () => setMounted(false)
  }, [])

  return mounted
    ? createPortal(
        <div className="loader-main">
          <Image
            src={wggLogo}
            width={250}
            height={250}
            className="spinWithAcceleration"
          />
        </div>,
        document.querySelector('#portals')
      )
    : null
}

export default index
