import { useEffect, useState } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import iconBlack from './../../public/images/main_icon_black.png'
import { useDispatch, useSelector } from 'react-redux'
import {
  setContainerDown,
  setContainerUp
} from '@ducks/responsiveSearch/actions'
import { MenuBars } from '@components/Icons/MenuBars'
import { useWindowSize } from 'hooks/useWindowSize'
import MobileSideMenu from './MobileSideMenu'
import { LogotypeExtended } from '@components/Icons/LogotypeExtended'

export function LandingNav({ type, user, role, from }) {
  /*  Custom hooks  */
  const { width } = useWindowSize()
  /*  Global states  */
  const { top } = useSelector((store) => store.responsiveSearch)
  /*  Local states  */
  const router = useRouter()
  const [isActive, setisActive] = useState(false)
  const [menuClicked, setMenuClicked] = useState(false)
  const [isMobile, setIsMobile] = useState(false)

  /*  Match validations  */
  const isMapView = type?.match(/search*/)
  const isHebergements = router.pathname.match(/hebergements*/)
  const isEstablishment = router.pathname.match(/establishment*/)
  const isContacter = router.pathname.match(/contacter*/)
  const isCheckout = router.pathname.match(/checkout*/)
  /*  Effects  */
  useEffect(() => {
    if (width <= 991) {
      setIsMobile(true)
    } else {
      setIsMobile(false)
    }
  }, [width])
  useEffect(() => {
    if (
      isMapView ||
      isHebergements ||
      isContacter ||
      isEstablishment ||
      isCheckout
    ) {
      setisActive(true)
    } else {
      window.addEventListener('scroll', handleScroll)
    }
    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [])
  useEffect(() => {
    const scrollY = window.scrollY
    if (from !== 'regions') {
      if (scrollY === 0 && menuClicked) {
        setisActive(true)
      }
    }
  }, [menuClicked])
  /*  Handle functions  */
  const handleScroll = () => {
    const scrollY = window.scrollY
    if (from !== 'regions') {
      if (scrollY > 0 || menuClicked) {
        setisActive(true)
      } else {
        setisActive(false)
      }
    }
  }

  const dispatch = useDispatch()

  const handleMenuClick = () => {
    setMenuClicked(!menuClicked)

    if (top === 'min') {
      dispatch(setContainerDown())
    } else {
      dispatch(setContainerUp())
    }
  }

  const handleRedirectHome = (e) => {
    e.preventDefault()
    window.location.replace(window.location.origin)
  }
  return (
    <>
      <nav
        className={`search__navbar navbar navbar-expand-lg navbar-light navbar-home ${
          isActive || isHebergements || isContacter || isEstablishment
            ? 'navbar-home-active'
            : ''
        } ${isCheckout ? 'bg-green' : ''}`}
      >
        <div className="container-xxl p-0 search__navbar-container">
          <a className="logo-navbar" href="#" onClick={handleRedirectHome}>
            <LogotypeExtended
              fill={
                !isActive &&
                (router.pathname === '/demarche' ||
                  router.pathname === '/region-auvergne-rhone-alpes')
                  ? '#fff'
                  : '#445136'
              }
              height="2.38rem"
              className={`navbar-logotype
              ${
                isActive
                  ? ' navbar-logotype--margin-selected navbar-logotype--opacity'
                  : router.pathname === '/'
                  ? ' navbar-logotype--margin-no-selected navbar-logotype--no-opacity'
                  : ' navbar-logotype--margin-no-selected navbar-logotype--opacity'
              }
            `}
            />
          </a>
          {!isCheckout && (
            <button
              className="navbar-toggler-button"
              type="button"
              data-toggle="collapse"
              // data-target="#navbarSupportedContent"
              // aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
              onClick={handleMenuClick}
            >
              {isMobile && <MenuBars fill={'#445136'} />}
            </button>
          )}
          {!isMobile && (
            <div
              className={`collapse navbar-collapse ${
                isMapView ? 'responsive-search__navbar--justified' : ''
              }`}
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav ml-auto form-inline my-2 my-lg-0 navbar-without-user">
                <li className="nav-item">
                  <div className="dropdown">
                    {Object.keys(user).length > 0 ? (
                      <Link
                        href={
                          role !== 'traveler'
                            ? '/host/dashboard'
                            : '/onboarding/presentation'
                        }
                      >
                        <a
                          type="button"
                          className={`button button-outline-${
                            isActive
                              ? 'black button-outline-black--alternative'
                              : 'white'
                          } button-navbar`}
                        >
                          {role !== 'traveler' ? 'Mode hôte' : 'Devenez hôte'}
                        </a>
                      </Link>
                    ) : (
                      <Link href="/login">
                        <a
                          type="button"
                          className={`button ${
                            isActive
                              ? 'button-outline-black button-navbar button-outline-black--alternative'
                              : 'button-outline-white button-navbar'
                          }`}
                        >
                          Devenez hôte
                        </a>
                      </Link>
                    )}
                  </div>
                </li>
                <li className="nav-item">
                  <div className="dropdown">
                    {Object.keys(user).length > 0 ? (
                      <Link
                        href={`${
                          role === 'traveler'
                            ? '/user/dashboard'
                            : role === 'host'
                            ? '/host/dashboard'
                            : '/admin'
                        }`}
                      >
                        <a data-toggle="dropdown" href="#">
                          {user.photo_url !== null &&
                            user.photo_url !== undefined && (
                              <img
                                src={
                                  'https://wegogreenr-laravel.s3.eu-west-2.amazonaws.com/' +
                                  user.photo_url
                                }
                                alt="Logo"
                                width={40}
                                height={40}
                                className="rounded-circle navbar-nav__profile-image"
                              />
                            )}
                          {(user.photo_url === null ||
                            user.photo_url === undefined) && (
                            <img
                              src={
                                isActive
                                  ? '/images/user_icon_black.png'
                                  : '/images/user_icon.png'
                              }
                              className="rounded-circle"
                              alt="Rounded Image"
                              width={50}
                              height={50}
                            />
                          )}
                        </a>
                      </Link>
                    ) : (
                      <Link href="/login">
                        <a data-toggle="dropdown" href="#">
                          <img
                            src={isActive ? iconBlack : '/images/user_icon.png'}
                            alt="Logo"
                            width={40}
                            height={40}
                            className="rounded-circle"
                          />
                        </a>
                      </Link>
                    )}
                  </div>
                </li>
              </ul>
            </div>
          )}
        </div>
      </nav>
      {menuClicked && (
        <MobileSideMenu
          className={menuClicked ? '' : 'mobile-side-menu--clicked'}
          setMenuClicked={setMenuClicked}
        />
      )}
    </>
  )
}
