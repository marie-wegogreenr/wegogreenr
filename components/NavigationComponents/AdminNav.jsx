import React from 'react'
import { useSelector } from 'react-redux'
import Link from 'next/link'
import DropdownDahboardMenu from '@components/dropdown/DropdownDahboardMenu'
import { LogotypeExtended } from '@components/Icons/LogotypeExtended'

export default function AdminNav() {
  const user = useSelector((state) => state.user.user)
  return (
    <nav
      className="navbar navbar-expand-lg navbar-light bg-light navbarDashboard fixed-top"
      style={{ margin: 0, padding: 0 }}
    >
      <div className="container-xxl">
        <Link href="/">
          <a className="navbar-brand">
            <LogotypeExtended fill="#445136" width="140px" />
          </a>
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div
          className="collapse navbar-collapse justify-content-end"
          id="navbarSupportedContent"
        >
          {/* <ul className="navbar-nav mr-2 form-inline my-2 my-lg-0">
            <li className="nav-item">
              <div className="dropdown">
                <a className="dropdown-toggle" data-toggle="dropdown" href="#">
                  <img
                    src={
                      user.photo_url !== null && user.photo_url !== undefined
                        ? process.env.NEXT_PUBLIC_AMAZON_IMGS + user.photo_url
                        : '/images/user_default.png'
                    }
                    className="rounded-circle"
                    alt="Rounded Image"
                    width={50}
                    height={50}
                  />
                </a>
              </div>
            </li>
          </ul> */}
          <ul className="navbar-nav mr-2 form-inline my-2 my-lg-0">
            <li className="nav-item">
              <div className="dropdown">
                <a className="dropdown-toggle" data-toggle="dropdown" href="#">
                  <img
                    src={
                      'https://wegogreenr-laravel.s3.eu-west-2.amazonaws.com/11355/user_photo_11355_Parc.png'
                    }
                    className="rounded-circle"
                    alt="Rounded Image"
                    width={50}
                    height={50}
                  />
                </a>
                <DropdownDahboardMenu />
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}
