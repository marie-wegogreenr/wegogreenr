/*  React  */
import { useEffect, useState } from 'react'
/*  Next  */
import Link from 'next/link'
/*  Icons  */
import { CloseSolid } from '@components/Icons'
import { LogotypeExtended } from '@components/Icons/LogotypeExtended'
import { useSelector } from 'react-redux'

function MobileSideMenu({ setMenuClicked }) {
  /*  Data  */
  const dataLinks = [
    { id: 2, path: '/search', name: 'Hébergements' },
    { id: 3, path: '/carte-cadeau', name: 'Carte cadeau' },
    { id: 4, path: '/demarche', name: 'Concepts' },
    { id: 5, path: 'https://blog.wegogreenr.com/', name: 'Blog' },
    { id: 6, path: '/press', name: 'Presse' },
    { id: 7, path: '/contacter', name: 'Contact' }
  ]
  /*  Global states  */
  const _user = useSelector((store) => store.user.user)
  /*  Local states  */
  const [showClassName, setShowClassName] = useState(false)
  const [linksData, setLinksData] = useState([...dataLinks])
  /*  Effects  */
  useEffect(() => {
    const loginObject = { id: 1 }
    if (_user && _user.roles) {
      const isHost = _user.roles.find((rol) => rol.name === 'host')
      const isTraveller = _user.roles.find((rol) => rol.name === 'traveller')

      if (isHost || isTraveller) {
        loginObject.name = 'Tableau de bord'
        if (isHost) {
          loginObject.path = '/host/dashboard'
        } else {
          loginObject.path = '/user/dashboard'
        }
      }

      if (!loginObject.name) {
        loginObject.name = 'Se connecter'
      }
      if (!loginObject.path) {
        loginObject.path = '/login'
      }
      setLinksData([{ ...loginObject }, ...linksData])
    } else {
      loginObject.name = 'Se connecter'
      loginObject.path = '/login'
      setLinksData([{ ...loginObject }, ...linksData])
    }
  }, [_user])
  useEffect(() => {
    setShowClassName(true)
  }, [])
  useEffect(() => {
    let timer
    if (!showClassName) {
      timer = setTimeout(() => {
        setMenuClicked(false)
      }, 300)
    }

    return () => clearTimeout(timer)
  }, [showClassName])
  /*  Functions  */
  const handleCloseClick = () => {
    setShowClassName(false)
  }

  return (
    <section
      className={`mobile-side-menu ${
        showClassName ? 'mobile-side-menu--clicked' : ''
      }`}
    >
      <div className="mobile-side-menu__icons">
        <CloseSolid
          fill="white"
          className="mobile-side-menu__close"
          onClick={() => handleCloseClick()}
        />
        <LogotypeExtended fill="white" className="mobile-side-menu__logo" />
      </div>
      <ul className="mobile-side-menu__list">
        {linksData.map((item) => (
          <Link href={item.path}>
            <a className="mobile-side-menu__link">{item.name}</a>
          </Link>
        ))}
      </ul>
    </section>
  )
}

export default MobileSideMenu
