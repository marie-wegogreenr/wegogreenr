import Link from 'next/link'
import { useRouter } from 'next/router'
import { useDispatch } from 'react-redux'
import { cleanFilters } from '@ducks/hebergements/search/actions'

const ButtonOptions = ({ from }) => {
  const router = useRouter()
  const dispatch = useDispatch()
  const data = {
    btnText: `Recherche d'hébergements`,
    url: '/search'
  }
  return (
    <Link href={data.url}>
      <a
        className={`navbar-options__link ${
          from ? 'navbar-options__link--colored' : ''
        }`}
        onClick={(e) => {
          e.preventDefault()
          dispatch(cleanFilters())
          router.push('/search')
        }}
      >
        {data.btnText}
      </a>
    </Link>
  )
}

export default ButtonOptions
