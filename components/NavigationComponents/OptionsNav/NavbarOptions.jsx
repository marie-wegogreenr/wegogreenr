/*  React  */
import { useEffect, useState } from 'react'
/*  Hooks  */
import { useWindowSize } from 'hooks/useWindowSize'
/*  Components  */
import Logo from './Logo'
import LandingOptions from './LandingOptions'
import ButtonOptions from './ButtonOptions'
import MobileOptions from './MobileOptions'
import useClickOutside from 'hooks/useClickOutside'

const NavbarOptions = () => {
  const [isMobile, setIsMobile] = useState()
  const [menuIsClicked, setMenuIsClicked] = useState(false)
  const [pages, setPages] = useState({
    press: false,
    cartCadeau: false,
    options: false
  })
  const { width } = useWindowSize()

  useEffect(() => {
    if (typeof window !== 'undefined') {
      setPages({
        ...pages,
        press: window.location.href.includes('press'),
        cartCadeau: window.location.href.includes('carte-cadeau'),
        options: window.location.href.includes('green-score')
      })
    }
  }, [])
  useEffect(() => {
    setIsMobile(width < 992)
  }, [width])

  const navbarNode = useClickOutside(() => {
    setMenuIsClicked(false)
  })

  const { press, cartCadeau, options } = pages

  return (
    <>
      <header
        className={`navbar-options__container${
          press || cartCadeau ? '--light' : ''
        }`}
      >
        <Logo from={press || cartCadeau ? 'press' : options ? 'options' : ''} />
        {/* {!isMobile ? (
          <>
            {!cartCadeau && <LandingOptions />}
            <ButtonOptions from={cartCadeau} />
          </>
        ) : (
          <>
            <MobileOptions
              setMenuIsClicked={setMenuIsClicked}
              menuIsClicked={menuIsClicked}
              pages={pages}
            />
            {menuIsClicked && (
              <LandingOptions
                show={menuIsClicked}
                isMobile={isMobile}
                setMenuIsClicked={setMenuIsClicked}
                reference={navbarNode}
                from={cartCadeau}
              />
            )}
          </>
        )} */}
      </header>
    </>
  )
}

export default NavbarOptions
