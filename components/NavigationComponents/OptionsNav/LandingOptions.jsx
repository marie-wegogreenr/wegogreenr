import Link from 'next/link'
import ButtonOptions from './ButtonOptions'
import { optionsNavbar } from 'constants/navbar'
import { CloseSolid } from '@components/Icons'

const LandingOptions = ({
  show,
  isMobile,
  reference,
  setMenuIsClicked,
  from
}) => {
  const handleCloseClick = () => {
    setTimeout(() => {
      setMenuIsClicked(false)
    }, 300)
  }

  return (
    <nav
      className={`navbar-options__nav${
        isMobile && show ? '--mobile-show' : isMobile ? '--mobile' : ''
      }`}
      ref={isMobile ? reference : null}
    >
      {isMobile && (
        <span
          className="navbar-options__close"
          onClick={() => handleCloseClick()}
        >
          <CloseSolid fill="white" className="navbar-options__close-btn" />
        </span>
      )}
      <ul className="navbar-options__list">
        {!from &&
          optionsNavbar.map((option, index) => (
            <li
              className={`navbar-options__list-item${
                index === 1 ? '--selected' : ''
              }`}
            >
              <Link href={option.url}>
                <a
                  type="button"
                  className={`navbar-options__list-link`}
                  onClick={() => handleCloseClick()}
                >
                  {option.btnText}
                </a>
              </Link>
            </li>
          ))}
        {isMobile && (
          <li
            className="navbar-options__list-item--btn"
            onClick={() => handleCloseClick()}
          >
            <ButtonOptions />
          </li>
        )}
      </ul>
    </nav>
  )
}

export default LandingOptions
