import { MenuBars } from '@components/Icons/MenuBars'

const MobileOptions = ({ setMenuIsClicked, menuIsClicked, pages }) => {
  const handleClick = () => {
    setTimeout(() => {
      setMenuIsClicked(!menuIsClicked)
    }, 300)
  }

  return (
    <>
      <div
        className="navbar-light navbar-options__bars-menu"
        onClick={() => handleClick()}
      >
        <MenuBars
          fill={pages.press || pages.cartCadeau ? '#445136' : 'white'}
        />
      </div>
    </>
  )
}

export default MobileOptions
