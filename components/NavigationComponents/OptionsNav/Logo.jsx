import { LogotypeExtended } from '@components/Icons/LogotypeExtended'
import Link from 'next/link'

const Logo = ({ from }) => {
  return (
    <Link href="/">
      <a className="navbar-brand">
        <LogotypeExtended
          fill={from === 'press' ? '#445136' : 'white'}
          width="148px"
          className="navbar-options__container-logotype"
        />
        {/* <img
          src="/images/logo.svg"
          alt="Picture of the author"
          width={from === 'options' ? 90 : from === 'press' ? 76 : 65}
          height={from === 'options' ? 105 : from === 'press' ? 87 : 70}
        /> */}
      </a>
    </Link>
  )
}

export default Logo
