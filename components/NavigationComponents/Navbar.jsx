import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import Router from 'next/router'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBell, faCommentDots } from '@fortawesome/free-regular-svg-icons'
import { faExchangeAlt } from '@fortawesome/free-solid-svg-icons'
/* Constants */
import { routes } from 'constants/index'
import { LandingNav } from './LandingNav'
import { connect, useSelector } from 'react-redux'

import { setEstablishmentId } from '../../redux/ducks/host/establishment/actions'
import DropdownDahboardMenu from '@components/dropdown/DropdownDahboardMenu'
import navRoutes from 'fixtures/navRoutes'
import { MenuBars } from '@components/Icons/MenuBars'
import { useWindowSize } from 'hooks/useWindowSize'
import { LogotypeExtended } from '@components/Icons/LogotypeExtended'

function Navbar({ props }) {
  /*  Global states  */
  const [user, role] = useSelector((state) => [
    state.user.user,
    state.user.role
  ])
  /*  Local states  */
  const [profileImage, setProfileImage] = useState('')
  const [menuClicked, setMenuClicked] = useState(false)
  const [origin, setOrigin] = useState(null)
  const [type, setType] = useState('')
  const [isMobile, setIsMobile] = React.useState(false)
  const { width } = useWindowSize()
  /*  Effects  */
  useEffect(() => {
    if (typeof window !== undefined) {
      const url = window.location.href
      const splittedUrl = url.split('/')
      setType(splittedUrl[3])
    }
  }, [])
  useEffect(() => {
    if (width <= 991) {
      setIsMobile(true)
    } else {
      setIsMobile(false)
    }
  }, [width])
  useEffect(() => {
    if (!isMobile) {
      setMenuClicked(false)
    }
  }, [isMobile])
  useEffect(() => {
    if (user.photo_url) {
      updateProfileImg()
    }

    if (user?.establishment?.origin) {
      setOrigin(user?.establishment?.origin)
    }
  }, [user])
  /*  Functions  */
  const updateProfileImg = () => {
    const imgUrl =
      user?.photo_url === null || user?.photo_url === undefined
        ? '/images/user_icon_black.png'
        : process.env.NEXT_PUBLIC_AMAZON_IMGS + user?.photo_url
    setProfileImage(imgUrl)
  }

  const handleLogout = () => {
    props?.onHandleLogout()
  }

  return (
    <>
      {type !== '' &&
        type !== 'hebergements' &&
        type !== 'regions' &&
        type !== 'establishment' &&
        type !== 'contacter' &&
        type !== '404' &&
        type !== 'demarche' &&
        type !== 'region-auvergne-rhone-alpes' &&
        type !== 'conditions-generales' &&
        type !== 'ccv' && (
          <nav
            className={`navbar navbar-expand-lg navbar-light bg-white navbarDashboard fixed-top ${
              type === 'checkout'
                ? 'bg-green position-static'
                : type === 'demarche'
                ? 'bg-transparent position-relative'
                : ''
            }`}
          >
            <div className="container-xxl p-0">
              <Link href="/">
                <a className="navbar-brand">
                  <LogotypeExtended
                    height="2.38rem"
                    className={`navbar-logotype navbar-logotype--margin-no-selected navbar-logotype--opacity`}
                    fill={
                      type === 'checkout' || type === 'demarche'
                        ? 'white'
                        : '#445136'
                    }
                  />
                </a>
              </Link>
              <button
                className="navbar-toggler-button"
                type="button"
                data-toggle="collapse"
                // data-target="#navbarSupportedContent"
                // aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
                onClick={() => setMenuClicked(!menuClicked)}
              >
                {/* <span className="navbar-toggler-icon" /> */}
                {isMobile && <MenuBars fill={'#445136'} />}
              </button>

              {!menuClicked && isMobile ? null : (
                <div
                  className="navbar-collapse"
                  // id="navbarSupportedContent"
                >
                  <ul className="navbar-nav mr-auto">
                    {React.Children.toArray(
                      navRoutes
                        .filter(({ text }) => {
                          if (
                            (text === 'Mes Hébergements' ||
                              text === 'Mes Réservations') &&
                            origin === 4
                          ) {
                            return false
                          }
                          return true
                        })
                        .map(
                          (
                            {
                              route,
                              text,
                              iconSource,
                              iconSourceAlt,
                              type: _type
                            },
                            index
                          ) =>
                            type === _type ? (
                              <li
                                className={`nav-item active ${
                                  !index ? 'active' : ''
                                }`}
                              >
                                <div>
                                  <img
                                    className={
                                      index === 1 || index === 2 || index === 3
                                        ? 'navbar__icon--align-center'
                                        : ''
                                    }
                                    src={
                                      Router.pathname === route ||
                                      Router.pathname === `${route}/[id]`
                                        ? iconSourceAlt
                                        : iconSource
                                    }
                                    style={{
                                      display: 'inline-block',
                                      marginLeft: 10 + 'px',
                                      marginRight: 10 + 'px'
                                    }}
                                    width={20}
                                    height={20}
                                  />
                                  <Link href={route}>
                                    <a
                                      className="nav-link weight-bold"
                                      style={{
                                        display: 'inline-block',
                                        marginRight: 25 + 'px',
                                        color:
                                          Router.pathname === route ||
                                          Router.pathname === `${route}/[id]`
                                            ? '#915324'
                                            : 'gray'
                                      }}
                                    >
                                      {text}
                                    </a>
                                  </Link>
                                </div>
                              </li>
                            ) : null
                        )
                    )}
                  </ul>
                  {type !== 'checkout' && type !== 'demarche' && (
                    <ul className="navbar-nav mr-2 form-inline my-2 my-lg-0">
                      {user?.roles && user?.roles?.length <= 1 ? (
                        <a
                          type="button"
                          class="btn btn-outline-secondary mr-4"
                          href="/onboarding/presentation"
                        >
                          Devenez hôte
                        </a>
                      ) : user?.roles &&
                        window.location.href.includes('/host/') ? (
                        <a
                          type="button"
                          class="btn btn-outline-secondary mr-4"
                          href="/user/dashboard"
                        >
                          Mode voyageur
                        </a>
                      ) : (
                        <a
                          type="button"
                          class="btn btn-outline-secondary mr-4"
                          href="/host/dashboard"
                        >
                          Mode hôte
                        </a>
                      )}
                      <li className="nav-item">
                        <div className="dropdown">
                          <a
                            className="dropdown-toggle"
                            data-toggle="dropdown"
                            href="#"
                          >
                            <img
                              src={
                                profileImage ||
                                (type === 'checkout' ||
                                type === 'conditions-generales-ventes' ||
                                type === 'conditions-generales-utilisation' ||
                                type === 'donnees-personnelles' ||
                                type === 'demarche'
                                  ? '/images/user_icon.png'
                                  : '/images/user_icon_black.png')
                              }
                              className="rounded-circle navbar-nav__profile-image"
                              alt="profile-picture"
                              width={50}
                              height={50}
                            />
                          </a>

                          <DropdownDahboardMenu
                            userRoles={user?.roles}
                            hasRol={props?.hasRol}
                            whereIs={type}
                            handleLogout={handleLogout}
                          />
                        </div>
                      </li>
                    </ul>
                  )}
                </div>
              )}
            </div>
          </nav>
        )}

      {type !== 'user' &&
        type !== 'host' &&
        type !== 'admin' &&
        type !== 'checkout' &&
        !Router.pathname.match(/search*/) && (
          <LandingNav
            type={props?.type}
            user={user}
            hasRol={props?.hasRol}
            role={role}
          />
        )}
    </>
  )
}

export default Navbar
