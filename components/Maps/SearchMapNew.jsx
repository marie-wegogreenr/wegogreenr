import React, { useEffect } from 'react'
import { consoleLog } from '/utils/logConsole'

const markers = [
  { lat: -1, lng: 1 },
  { lat: -1.5, lng: 1.2 },
  { lat: -1.7, lng: 1 }
]
const markersRef = []

const SearchMapNew = () => {
  let map

  function initMap() {
    map = new google.maps.Map(document.getElementById('hello'), {
      center: { lat: -34.397, lng: 150.644 },
      zoom: 2
    })

    markers.forEach((mark) =>
      markersRef.push(
        new google.maps.Marker({
          position: { lat: mark.lat, lng: mark.lng },
          map,
          title: 'Hello World!'
        })
      )
    )

    google.maps.event.addListener(map, 'bounds_changed', function () {
      var bounds = map.getBounds()
      var ne = bounds.getNorthEast()
      var sw = bounds.getSouthWest()

      const mapBounds = {
        NE: { lat: ne.lat(), long: ne.lng() },
        SW: { lat: sw.lat(), long: sw.lng() }
      }
      //consoleLog(mapBounds)
      //do whatever you want with those bounds
      markersRef.forEach((marker) => {
        const singleMarker = marker.getPosition()
        const point = { lat: singleMarker.lat(), long: singleMarker.lng() }
        consoleLog(inBounds(point, mapBounds))
      })
    })
  }

  function inBounds(point, bounds) {
    var eastBound = point.long < bounds.NE.long
    var westBound = point.long > bounds.SW.long
    var inLong

    if (bounds.NE.long < bounds.SW.long) {
      inLong = eastBound || westBound
    } else {
      inLong = eastBound && westBound
    }

    var inLat = point.lat > bounds.SW.lat && point.lat < bounds.NE.lat
    return inLat && inLong
  }

  useEffect(() => {
    initMap()
    //consoleLog('hola')
  }, [])
  return (
    <div id="hello" style={{ height: '100vh', width: '100vw' }}>
      sadasdasdasd
    </div>
  )
}

export default React.memo(SearchMapNew)
