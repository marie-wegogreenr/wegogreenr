import React, { memo } from 'react'
import { consoleLog } from '/utils/logConsole'
import { LoadScript } from '@react-google-maps/api'

const LoadScriptCustom = () => {
  return (
    <div>
      <LoadScript
        googleMapsApiKey="AIzaSyCD2m7tKguF7vbh__Cwy3nbTwacV0DJ2aY"
        libraries={['places']}
      ></LoadScript>
    </div>
  )
}

export default memo(LoadScriptCustom)
