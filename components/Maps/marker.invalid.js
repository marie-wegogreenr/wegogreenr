var markerInvalid = {
  id: 307,
  establishment_id: 109,
  type_id: null,
  internal_name: null,
  public_name: null,
  description: null,
  total_area: null,
  people_capacity: null,
  strengths: null,
  bathroom_quantity: null,
  rooms_quantity: null,
  host_quantity: null,
  basic_price: null,
  weekend_price: null,
  seven_days_discount: null,
  month_discount: null,
  min_nigth: null,
  max_nigth: null,
  slug: 'UNoTNBBZEUgR285B1IcDzOm2q9LDCkFm-91',
  reservation_mode: 0,
  complete: null,
  outstanding: 0,
  active: 1,
  created_at: '2021-05-14T00:22:18.000000Z',
  updated_at: '2021-05-21T13:45:28.000000Z',
  room_equipment: [],
  establishment: {
    id: 109,
    user_id: 11344,
    type_id: 1,
    clasification_id: 2,
    country_id: 1,
    region_id: 19,
    city_id: 33442,
    name: 'establecimiento test 1',
    stars: null,
    description: 'test edit',
    additional_information: null,
    address: '150 avenue du général de gaulle',
    street_number: null,
    zip_code: '69001',
    min_entry_time: '13:26:00',
    max_entry_time: '18:40:00',
    max_exit_time: '15:26:00',
    tva: '123456789',
    decl_inc_pro: 0,
    amount_taxe: null,
    cancel_max_days: null,
    contact_phone: '3187940735',
    contact_name: 'Gradiweb',
    contact_lastname: 'Test',
    contact_email: 'carlos@gradiweb.com',
    slug: 'establishment-109',
    lng: 2.255847,
    lat: 48.905672,
    active: 1,
    created_at: '2021-04-29T20:22:47.000000Z',
    updated_at: '2021-07-16T21:24:37.000000Z',
    city: {
      id: 33442,
      name: 'Paris',
      region_id: 8,
      lng: 2.34901,
      lat: 48.86472,
      created_at: null,
      updated_at: null
    },
    region: {
      id: 19,
      name: 'Other',
      country_id: 1,
      position: null,
      created_at: null,
      updated_at: null
    },
    green_score: {
      id: 108,
      establishment_id: 109,
      score: 196,
      level: 5,
      created_at: '2021-04-29T20:22:48.000000Z',
      updated_at: '2021-04-29T20:22:48.000000Z'
    }
  },
  room_image: [
    {
      id: 78,
      room_id: 307,
      image_id: 103,
      order: 1,
      created_at: '2021-05-25T20:13:40.000000Z',
      updated_at: '2021-05-25T20:13:40.000000Z',
      image: {
        id: 103,
        url: '11344/room_307_637967.jpg',
        created_at: '2021-05-25T20:13:40.000000Z',
        updated_at: '2021-05-25T20:13:40.000000Z'
      }
    },
    {
      id: 118,
      room_id: 307,
      image_id: 25,
      order: 2,
      created_at: null,
      updated_at: null,
      image: {
        id: 25,
        url: '11357/room_286_home-page-insolite.png',
        created_at: '2021-05-07T16:34:03.000000Z',
        updated_at: '2021-05-07T16:34:03.000000Z'
      }
    }
  ]
}
