import React, { useEffect } from 'react'
import { consoleLog } from '/utils/logConsole'
import { MapContainer, TileLayer, Marker } from 'react-leaflet'
import { useMap } from 'react-leaflet'
import 'leaflet/dist/leaflet.css'
import 'leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css'
import 'leaflet-defaulticon-compatibility'

function Map({
  center = {
    lat: 46.2245,
    lng: 2.2123
  }
}) {
  const { lat, lng } = center

  if (!lat || !lng) return 'Latitude ou longitude introuvable.'
  return (
    <MapContainer
      center={[lat, lng]}
      zoom={8}
      scrollWheelZoom={false}
      style={{ height: '100%', width: '100%' }}
    >
      <MapControls lat={lat} lng={lng} />
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker
        position={[lat, lng]}
        animate={true}
        key={Date.now() + Math.random()}
      />
    </MapContainer>
  )
}

const MapControls = ({ lat, lng }) => {
  const map = useMap()
  useEffect(() => {
    map.flyTo([lat, lng], 10)
  }, [lat, lat])
  return <></>
}

export default React.memo(Map)
