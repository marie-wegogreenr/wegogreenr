var markerValid = {
  id: 286,
  establishment_id: 113,
  type_id: 20,
  internal_name: 'Apartament Coeur',
  public_name: 'Apartament Coeur',
  description:
    ". La salle de bain est composé des toilettes et d'une grande douche. Le lit double se trouve sur la mezzanine.\nPour toute question, n'hésitez pas à me contacter ! A bientôt ;)",
  total_area: 45,
  people_capacity: 2,
  strengths: 'Le lit double se trouve sur la mezzanine.',
  bathroom_quantity: 1,
  rooms_quantity: 2,
  host_quantity: 3,
  basic_price: 5,
  weekend_price: 25,
  seven_days_discount: 10,
  month_discount: 20,
  min_nigth: 2,
  max_nigth: 4,
  slug: 'Apartament-Coeur--286',
  reservation_mode: 0,
  complete: null,
  outstanding: 0,
  active: 1,
  created_at: '2021-05-07T16:28:24.000000Z',
  updated_at: '2021-06-17T16:17:25.000000Z',
  room_equipment: [
    {
      id: 1555,
      room_id: 286,
      type_id: 1,
      created_at: '2021-05-07T16:32:28.000000Z',
      updated_at: '2021-05-07T16:32:28.000000Z'
    },
    {
      id: 1556,
      room_id: 286,
      type_id: 10,
      created_at: '2021-05-07T16:32:29.000000Z',
      updated_at: '2021-05-07T16:32:29.000000Z'
    },
    {
      id: 1557,
      room_id: 286,
      type_id: 47,
      created_at: '2021-05-07T16:32:29.000000Z',
      updated_at: '2021-05-07T16:32:29.000000Z'
    },
    {
      id: 1558,
      room_id: 286,
      type_id: 4,
      created_at: '2021-05-07T16:32:29.000000Z',
      updated_at: '2021-05-07T16:32:29.000000Z'
    },
    {
      id: 1559,
      room_id: 286,
      type_id: 67,
      created_at: '2021-05-07T16:32:29.000000Z',
      updated_at: '2021-05-07T16:32:29.000000Z'
    },
    {
      id: 1560,
      room_id: 286,
      type_id: 7,
      created_at: '2021-05-07T16:32:29.000000Z',
      updated_at: '2021-05-07T16:32:29.000000Z'
    },
    {
      id: 1561,
      room_id: 286,
      type_id: 76,
      created_at: '2021-05-07T16:32:29.000000Z',
      updated_at: '2021-05-07T16:32:29.000000Z'
    },
    {
      id: 1577,
      room_id: 286,
      type_id: 14,
      created_at: '2021-05-14T11:36:11.000000Z',
      updated_at: '2021-05-14T11:36:11.000000Z'
    }
  ],
  establishment: {
    id: 113,
    user_id: 11357,
    type_id: 1,
    clasification_id: 2,
    country_id: 1,
    region_id: 9,
    city_id: 33442,
    name: 'Appartement à Villeurbanne près de la Doua',
    stars: null,
    description: 'Ceci est une description générale de votre établissement.',
    additional_information: null,
    address: '10 rue de la paix',
    street_number: '108',
    zip_code: '75005',
    min_entry_time: '11:27:00',
    max_entry_time: '17:33:00',
    max_exit_time: '12:27:00',
    tva: '43254345364364',
    decl_inc_pro: 0,
    amount_taxe: 20,
    cancel_max_days: null,
    contact_phone: '321606781',
    contact_name: 'Gonzalez',
    contact_lastname: 'Maria',
    contact_email: 'gamemo54@hotmail.com',
    slug: 'appartement-a-villeurbanne-pres-de-la-doua',
    lng: 2.331324,
    lat: 48.868961,
    active: 1,
    created_at: '2021-05-07T16:24:52.000000Z',
    updated_at: '2021-05-25T22:35:22.000000Z',
    city: {
      id: 33442,
      name: 'Paris',
      region_id: 8,
      lng: 2.34901,
      lat: 48.86472,
      created_at: null,
      updated_at: null
    },
    region: {
      id: 9,
      name: 'Normandie',
      country_id: 1,
      position: 11,
      created_at: null,
      updated_at: null
    },
    green_score: {
      id: 112,
      establishment_id: 113,
      score: 238,
      level: 5,
      created_at: '2021-05-07T16:24:53.000000Z',
      updated_at: '2021-05-07T16:24:53.000000Z'
    }
  },
  room_image: [
    {
      id: 19,
      room_id: 286,
      image_id: 26,
      order: 2,
      created_at: '2021-05-07T16:34:03.000000Z',
      updated_at: '2021-05-21T21:11:16.000000Z',
      image: {
        id: 26,
        url: '11357/room_286_home-page-chambredhote.png',
        created_at: '2021-05-07T16:34:03.000000Z',
        updated_at: '2021-05-07T16:34:03.000000Z'
      }
    },
    {
      id: 20,
      room_id: 286,
      image_id: 27,
      order: 1,
      created_at: '2021-05-07T16:34:04.000000Z',
      updated_at: '2021-05-11T21:46:35.000000Z',
      image: {
        id: 27,
        url: '11357/room_286_home-page-hotel.png',
        created_at: '2021-05-07T16:34:04.000000Z',
        updated_at: '2021-05-07T16:34:04.000000Z'
      }
    },
    {
      id: 33,
      room_id: 286,
      image_id: 55,
      order: 5,
      created_at: '2021-05-24T14:47:34.000000Z',
      updated_at: '2021-05-24T14:47:34.000000Z',
      image: {
        id: 55,
        url: '11357/room_286_téléchargement.jpg',
        created_at: '2021-05-24T14:47:34.000000Z',
        updated_at: '2021-05-24T14:47:34.000000Z'
      }
    },
    {
      id: 34,
      room_id: 286,
      image_id: 56,
      order: 4,
      created_at: '2021-05-24T14:47:35.000000Z',
      updated_at: '2021-05-24T14:47:35.000000Z',
      image: {
        id: 56,
        url: '11357/room_286_Définitions des slow tourisme.png',
        created_at: '2021-05-24T14:47:35.000000Z',
        updated_at: '2021-05-24T14:47:35.000000Z'
      }
    },
    {
      id: 46,
      room_id: 286,
      image_id: 68,
      order: 6,
      created_at: '2021-05-25T16:59:38.000000Z',
      updated_at: '2021-05-25T16:59:38.000000Z',
      image: {
        id: 68,
        url: '11357/room_286_chambre-écoresponsable-camping-issac-plante-ta-tente-chez-hervé-1.jpg',
        created_at: '2021-05-25T16:59:38.000000Z',
        updated_at: '2021-05-25T16:59:38.000000Z'
      }
    },
    {
      id: 47,
      room_id: 286,
      image_id: 69,
      order: 6,
      created_at: '2021-05-25T17:00:50.000000Z',
      updated_at: '2021-05-25T17:00:50.000000Z',
      image: {
        id: 69,
        url: '11357/room_286_chambre-écoresponsable-camping-issac-plante-ta-tente-chez-hervé-1.jpg',
        created_at: '2021-05-25T17:00:50.000000Z',
        updated_at: '2021-05-25T17:00:50.000000Z'
      }
    },
    {
      id: 83,
      room_id: 286,
      image_id: 113,
      order: 8,
      created_at: '2021-05-25T22:43:53.000000Z',
      updated_at: '2021-05-25T22:43:53.000000Z',
      image: {
        id: 113,
        url: '11357/room_286_maria 2.jfif',
        created_at: '2021-05-25T22:43:53.000000Z',
        updated_at: '2021-05-25T22:43:53.000000Z'
      }
    },
    {
      id: 84,
      room_id: 286,
      image_id: 114,
      order: 9,
      created_at: '2021-05-25T22:45:00.000000Z',
      updated_at: '2021-05-25T22:45:00.000000Z',
      image: {
        id: 114,
        url: '11357/room_286_ECO-LODGE et familles - DUFOUR Gérard - Pilar.jpg',
        created_at: '2021-05-25T22:45:00.000000Z',
        updated_at: '2021-05-25T22:45:00.000000Z'
      }
    },
    {
      id: 85,
      room_id: 286,
      image_id: 115,
      order: 8,
      created_at: '2021-05-25T22:45:00.000000Z',
      updated_at: '2021-05-25T22:45:00.000000Z',
      image: {
        id: 115,
        url: '11357/room_286_maria 2.jfif',
        created_at: '2021-05-25T22:45:00.000000Z',
        updated_at: '2021-05-25T22:45:00.000000Z'
      }
    }
  ]
}
