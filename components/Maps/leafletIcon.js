import icons from './icons'
import { adjustNumberWithComma } from 'helpers/utilsHelper'

function createIcon(room) {
  const { basic_price, type_id, slug } = room
  if (L)
    return L.divIcon({
      className: 'custom-div-icon',
      html: `<div m-6>
              <div class='d-flex flex-column align-items-center px-3 maps__infoCard br-10 font-montserrat'>
              <img src=${
                icons[type_id]?.icon || icons[5].icon
              } alt="" width="40" />
                <p class="m-0 text-nowrap">
                  <span class="weight-bold ">${
                    basic_price
                      ? adjustNumberWithComma(basic_price) + '&euro;'
                      : 'Caché'
                  }</span>
                  /nuit
                </p>      
                </div>
              <div class='arrow-down'>
              </div>
          </div>
  `,
      iconSize: [100, 40],
      iconAnchor: [0, 0],
      popupAnchor: [15, 40] // first moves the x axis
    })
}

export default createIcon
