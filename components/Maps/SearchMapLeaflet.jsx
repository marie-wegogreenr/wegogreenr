import React, { useRef, useEffect } from 'react'
import { useWindowSize } from 'hooks'
import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
  useMapEvent
} from 'react-leaflet'
import 'leaflet/dist/leaflet.css'
import 'leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css'
import 'leaflet-defaulticon-compatibility'
import LeafletControllerSearch from './LeafletControllerSearch'
import { isEmpty } from 'utils'
//REDUX
import { useSelector } from 'react-redux'
//UTILS
import MarkerClusterGroup from 'react-leaflet-markercluster'
import PopUpCustom from './PopUpCustom'
import PopUpCustomEstablishment from './PopUpCustomEstablishment'
import createIcon from './leafletIcon'
import { Loader } from '..'
import 'react-leaflet-markercluster/dist/styles.min.css'
import { mapRooms } from 'helpers/roomHelper'

const MarkerComponent = ({ room }) => {
  const { lat = 0, lng = 0, type_id } = room.establishment

  return (
    <Marker position={[lat, lng]} animate={true} icon={createIcon(room)}>
      <Popup>
        <PopUpCustom room={room} />
      </Popup>
    </Marker>
  )
}

const MarkerEstablishmentComponent = ({ establishment }) => {
  const { lat = 0, lng = 0 } = establishment

  const price = establishment?.external_information?.price || null
  return (
    <Marker
      position={[lat, lng]}
      animate={true}
      icon={createIcon({ ...establishment, basic_price: price })}
    >
      <Popup>
        <PopUpCustomEstablishment
          establishment={{ ...establishment, basic_price: price }}
        />
      </Popup>
    </Marker>
  )
}

const MarkerClusterComponent = ({ rooms, spiderOptions, establishments }) => {
  const mappedRooms = mapRooms(rooms)
  return (
    <MarkerClusterGroup
      key={Date.now() + Math.random()}
      spiderOptions={spiderOptions}
      spiderfyDistanceMultiplier={4}
      showCoverageOnHover={false}
      animateAddingMarkers={true}
    >
      {mappedRooms?.map((room) => (
        <MarkerComponent room={room} key={Date.now() + Math.random()} />
      ))}
      {establishments?.map((establishment) => (
        <MarkerEstablishmentComponent
          establishment={establishment}
          key={Date.now() + Math.random()}
        />
      ))}
    </MarkerClusterGroup>
  )
}

const SearchMapLeaflet = () => {
  if (window) {
    const {
      data: { rooms, searchParams, establishments },
      status
    } = useSelector((store) => store.searchedRooms)
    const [isMobile, handleResizeScreenState] = React.useState(false)
    const { width } = useWindowSize()

    React.useEffect(() => {
      handleResizeScreenState(width < 680)
    }, [width])

    let spiderOptions = { weight: 2.5, color: '#fff', opacity: 1 }
    if (searchParams && MapContainer) {
      return (
        <>
          <MapContainer
            zoom={10}
            center={[searchParams?.lat, searchParams?.lng]}
            maxZoom={15}
            minZoom={5}
            scrollWheelZoom={true}
            className="searchMap search-map-leaflet__container"
          >
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <LeafletControllerSearch />
            <MarkerClusterComponent
              key={Date.now() + Math.random()}
              rooms={rooms}
              establishments={establishments}
              spiderOptions={spiderOptions}
            />
          </MapContainer>
        </>
      )
    }
    return <Loader />
  } else {
    return <Loader />
  }
}

export default SearchMapLeaflet
