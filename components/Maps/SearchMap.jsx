import React, { useState, useRef } from 'react'
import { consoleLog } from '/utils/logConsole'
import { GoogleMap, Marker, InfoWindow, InfoBox } from '@react-google-maps/api'
//REDUX
import { useSelector } from 'react-redux'
//ASSETS
import { PolygonDown } from '@components/Icons'

import icons from './icons'
import Image from 'next/image'

const containerStyle = {
  width: '100%',
  height: '100%'
}

function SearchMap({
  center = {
    lat: 46.2245,
    lng: 2.2123
  }
}) {
  const {
    data: { rooms },
    status
  } = useSelector((store) => store.searchedRooms)
  const [activeState, setActiveState] = useState({})
  const options = { closeBoxURL: '', enableEventPropagation: true }

  let closeCenter = center

  /* FIXME: Juan, estoy teniendo un error en este código en el Single Page. Lo comento mientras agrego las imágenes en el Single Page. */
  function handleClick(e) {
    closeCenter = { lat: e.latLng.lat() || 0, lng: e.latLng.lng() || 0 }
    //consoleLog(e)
    //consoleLog(e.latLng.lat())
    //consoleLog(e.latLng.lng())
  }
  //juan key AIzaSyBDgmjXDEEZaPVIt00oxXGL6GNo7lGi8OM
  //client's key AIzaSyCD2m7tKguF7vbh__Cwy3nbTwacV0DJ2aY

  return (
    <GoogleMap
      mapContainerStyle={containerStyle}
      center={closeCenter}
      zoom={8}
      onClick={handleClick}
      onCenterChanged={(e) => {}}
    >
      {status === 'SUCCESS' &&
        rooms?.map((room, index) => {
          const {
            establishment: { lat, lng },
            type_id,
            basic_price,
            public_name
          } = room
          const location = { lat: lat, lng: lng }

          return (
            <InfoBox options={options} position={location}>
              <>
                <div className="rounded-3 maps__infoCard">
                  <div
                    className={
                      activeState[index]
                        ? `d-none`
                        : 'd-flex flex-column align-items-center px-3'
                    }
                    onClick={() => setActiveState({ [index]: true })}
                  >
                    <img src={icons[type_id]?.icon} alt="" width="40" />
                    <p className="m-0">
                      <span className="weight-bold ">{basic_price}&euro;</span>
                      /nuit
                    </p>
                  </div>
                  <div
                    className={activeState[index] ? `d-flex p-3` : 'd-none'}
                    onClick={() => setActiveState({ [index]: false })}
                  >
                    <Image
                      src="https://picsum.photos/300"
                      width={139}
                      height={89}
                    />
                    <div className="ml-3">
                      <p className="weight-bold text font-montserrat">
                        {public_name}
                      </p>
                      <div className="d-flex justify-content-between">
                        <div className="d-flex align-items-center mr-3">
                          <img
                            src={icons[type_id]?.icon}
                            alt=""
                            width="40"
                            className="rounded-3"
                          />
                          <p className="m-0">{icons[type_id]?.name}</p>
                        </div>
                        <div>
                          <p className="m-0">A partir de</p>
                          <p className="m-0">
                            <span className="weight-bold ">
                              {basic_price}&euro;
                            </span>
                            /nuit
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="text-center">
                  <PolygonDown />
                </div>
              </>
            </InfoBox>
          )
        })}
    </GoogleMap>
  )
}

export default React.memo(SearchMap)
