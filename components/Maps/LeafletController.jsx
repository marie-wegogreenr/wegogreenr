import React, { useEffect } from 'react'
import { consoleLog } from '/utils/logConsole'
import { useMap, useMapEvents } from 'react-leaflet'
import L from 'leaflet'
//REDUX
import { useSelector, useDispatch } from 'react-redux'
import { filterMapRooms } from '@ducks/hebergements/search/actions'
//UTILS

import { inBounds } from 'utils'

const LeafletController = () => {
  const dispatch = useDispatch()
  const map = useMap()

  const {
    data: { rooms },
    status
  } = useSelector((store) => store.searchedRooms)
  const filterRefresh = useSelector((store) => store.searchedRooms.filterCount)

  const roomsCopy = [...rooms]

  useEffect(() => {
    mapBoundsFilter()
  }, [filterRefresh])

  useEffect(() => {
    flyToResults()
  }, [status])

  const events = useMapEvents({
    dragend: () => {
      mapBoundsFilter()
    },
    resize: () => {
      mapBoundsFilter()
    },
    zoomend: () => {
      mapBoundsFilter()
    }
  })

  function mapBoundsFilter() {
    const mapBounds = map.getBounds()
    const bounds = {
      NE: { lat: mapBounds._northEast.lat, long: mapBounds._northEast.lng },
      SW: { lat: mapBounds._southWest.lat, long: mapBounds._southWest.lng }
    }
    let filter = roomsCopy.map((room) => {
      const {
        establishment: { lat, lng }
      } = room
      const point = { lat, long: lng }
      return { ...room, isVisible: inBounds(point, bounds) }
    })

    dispatch(filterMapRooms(filter))
  }

  function flyToResults() {
    const { lat: lat1, lng: lng1 } = rooms[0]?.establishment
    const { lat: lat2, lng: lng2 } = rooms[rooms.length - 1]?.establishment

    status == 'SUCCESS'
      ? map.flyToBounds(
          [
            [lat1 || 48, lng1 || 2.3],
            [lat2 || 48.5, lng2 || 2]
          ],
          { padding: [0, 0], maxZoom: 10 }
        )
      : ''
  }
  return <></>
}

export default React.memo(LeafletController)
