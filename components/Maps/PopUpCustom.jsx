import React from 'react'
import Image from 'next/image'
import icons from './icons'
//UTILS
import { getImageURL } from 'utils'
import { adjustNumberWithComma } from 'helpers/utilsHelper'

const PopUpCustom = ({ room }) => {
  const {
    type_id = 20,
    public_name = 'Nom caché',
    basic_price = 'Prix ​​caché',
    room_image = [],
    slug = 'SLUG_NO_FOUND',
    origin,
    origin_url
  } = room
  return (
    <div className="PopUpCustom">
      <a
        href={origin === 2 ? origin_url : `/hebergements/${slug}`}
        target="_blank"
        className="text-decoration-none"
      >
        <div className="d-flex p-3 align-items-center flex-column flex-md-row mx-4 my-2">
          <div className="PopUpCustom-image">
            <img
              src={
                origin === 2
                  ? room_image[0]
                  : getImageURL(room_image[0]?.image?.url ?? '')
              }
              width={139}
              height={89}
              className="br-10"
            />
          </div>
          <div className="PopUpCustom-content">
            <div className="ml-3">
              <p className="weight-bold text font-montserrat mt-0 mb-3 text-center text-md-left">
                {public_name}
              </p>
              <div className="d-flex justify-content-md-between flex-column flex-md-row justify-content-center align-items-center">
                <div className="d-flex align-items-center mr-3 flex-column flex-md-row">
                  <img
                    src={icons[type_id]?.icon || icons[1].icon}
                    alt=""
                    width="40"
                    className="rounded-3"
                  />
                  <p className="m-0">{icons[type_id]?.name || 'default'}</p>
                </div>
                <div className="d-flex align-items-center flex-column">
                  <p className="m-0 text-nowrap">A partir de</p>
                  <p className="m-0">
                    <span className="weight-bold ">
                      {adjustNumberWithComma(basic_price)}&euro;
                    </span>
                    /nuit
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
  )
}

export default PopUpCustom
