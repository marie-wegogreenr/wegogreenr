import React, { useEffect } from 'react'
import { useMap, useMapEvents } from 'react-leaflet'
import L from 'leaflet'
//REDUX
import { useSelector, useDispatch } from 'react-redux'
import {
  checkFields,
  setSearchLocation,
  searchRoomsByMapBounds,
  filterMapRooms
} from '@ducks/hebergements/search/actions'
//UTILS
import { inBounds } from 'utils'
/* Constants */
import { routes } from 'constants/index'
/* Next */
import { useRouter } from 'next/router'
import { handleSearchProps } from 'helpers/searchHelper'

const LOC_ALTERNATE_NAME = 'Autre site'
const LeafletControllerSearch = () => {
  const [popUpState, handlePopupState] = React.useState(true)

  const {
    data: {
      searchParams,
      searchParams: { lat, lng, zoom }
    },
    status
  } = useSelector((state) => state.searchedRooms)

  // Router
  const router = useRouter()

  const dispatch = useDispatch()

  const map = useMap()

  useEffect(() => {
    if (!map) return
    if (status !== 'INIT') return
    setTimeout(() => {
      let _path = ''
      if (window !== undefined) {
        _path = window.location.pathname.split('/')[1]
      }
      searchRooms(undefined, _path)
    }, 300)
  }, [status])

  useEffect(() => {
    if (!map) return
    flyToCoords(lat, lng, zoom)
  }, [lat, lng])

  const events = useMapEvents({
    dragend: () => {
      let locationName = LOC_ALTERNATE_NAME
      const path = router.asPath.split('/')[1] || ''
      searchRooms(locationName, path)
    },

    zoomend: () => {
      const path = router.asPath.split('/')[1] || ''
      searchRooms(undefined, path)
    }
  })

  function searchRooms(locationName = undefined, path = '') {
    const { NE, SW } = mapBounds()
    let searchProps = {}

    if (path !== 'search') searchProps = handleSearchProps(path)

    const queries = {
      ...searchParams,
      ...searchProps,
      nelat: NE.lat,
      nelng: NE.long,
      swlat: SW.lat,
      swlng: SW.long,
      page: 1,
      locationName: locationName || searchParams.locationName
    }
    dispatch(setSearchLocation(queries))
    dispatch(searchRoomsByMapBounds(queries))
  }

  function mapBounds() {
    const mapBounds = map.getBounds()
    const bounds = {
      NE: { lat: mapBounds._northEast.lat, long: mapBounds._northEast.lng },
      SW: { lat: mapBounds._southWest.lat, long: mapBounds._southWest.lng }
    }
    return bounds
  }

  function flyToCoords(lat, lng, zoom = 10) {
    map.flyTo([lat, lng], zoom)
  }

  return <></>
}

export default React.memo(LeafletControllerSearch)
