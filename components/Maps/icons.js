import Icon1 from 'public/images/icons/rooms/1.svg'
import Icon2 from 'public/images/icons/rooms/2.svg'
import Icon3 from 'public/images/icons/rooms/3.svg'
import Icon4 from 'public/images/icons/rooms/4.svg'
import Icon5 from 'public/images/icons/rooms/5.svg'
import Icon6 from 'public/images/icons/rooms/6.svg'
import Icon7 from 'public/images/icons/rooms/7.svg'
import Icon8 from 'public/images/icons/rooms/8.svg'
import Icon9 from 'public/images/icons/rooms/9.svg'
import Icon10 from 'public/images/icons/rooms/10.svg'
import Icon11 from 'public/images/icons/rooms/11.svg'
import Icon12 from 'public/images/icons/rooms/12.svg'
import Icon13 from 'public/images/icons/rooms/13.svg'
import Icon14 from 'public/images/icons/rooms/14.svg'
import Icon15 from 'public/images/icons/rooms/15.svg'
import Icon16 from 'public/images/icons/rooms/16.svg'
import Icon17 from 'public/images/icons/rooms/17.svg'
import Icon18 from 'public/images/icons/rooms/18.svg'
import Icon19 from 'public/images/icons/rooms/19.svg'
import Icon20 from 'public/images/icons/rooms/20.svg'
import Icon21 from 'public/images/icons/rooms/21.svg'
import Icon22 from 'public/images/icons/rooms/22.svg'
import Icon23 from 'public/images/icons/rooms/23.svg'
import Icon24 from 'public/images/icons/rooms/24.svg'
import Icon25 from 'public/images/icons/rooms/25.svg'
import Icon26 from 'public/images/icons/rooms/26.svg'
import Icon27 from 'public/images/icons/rooms/27.svg'
import Icon28 from 'public/images/icons/rooms/28.svg'
import Icon29 from 'public/images/icons/rooms/29.svg'
import Icon30 from 'public/images/icons/rooms/30.svg'
import Icon31 from 'public/images/icons/rooms/31.svg'
import Icon32 from 'public/images/icons/rooms/32.svg'
import Icon33 from 'public/images/icons/rooms/33.svg'
import Icon34 from 'public/images/icons/rooms/34.svg'

const icons = {
  1: {
    icon: Icon1,
    name: 'Appartement'
  },
  2: {
    icon: Icon2,
    name: 'Appart-Hôtel'
  },
  3: {
    icon: Icon3,
    name: 'Insolite'
  },
  4: {
    icon: Icon4,
    name: 'Autres'
  },
  5: {
    icon: Icon5,
    name: 'Bateau'
  },
  6: {
    icon: Icon6,
    name: 'Bulle'
  },
  7: {
    icon: Icon7,
    name: 'Bungalow'
  },
  8: {
    icon: Icon8,
    name: 'Cabane'
  },
  9: {
    icon: Icon9,
    name: 'Cabane dans les arbres'
  },
  10: {
    icon: Icon10,
    name: 'Cabane de trappeur'
  },
  11: {
    icon: Icon11,
    name: 'Cabane sur pilotis'
  },
  12: {
    icon: Icon12,
    name: "Cabanes sur l'eau"
  },
  13: {
    icon: Icon13,
    name: 'Chalet'
  },
  14: {
    icon: Icon14,
    name: "Chambre chez l'habitant"
  },
  15: {
    icon: Icon15,
    name: "Chambres d'hôtes"
  },
  16: {
    icon: Icon16,
    name: 'Chambre privée'
  },
  17: {
    icon: Icon17,
    name: 'Dôme'
  },
  18: {
    icon: Icon18,
    name: 'Ecolodges'
  },
  19: {
    icon: Icon19,
    name: 'Emplacement de tente'
  },
  20: {
    icon: Icon20,
    name: 'Gîtes'
  },
  21: {
    icon: Icon21,
    name: 'Hôtels'
  },
  22: {
    icon: Icon22,
    name: 'Lit en dortoir'
  },
  23: {
    icon: Icon23,
    name: "Lov'nid"
  },
  24: {
    icon: Icon24,
    name: 'Maison'
  },
  25: {
    icon: Icon25,
    name: "Maison d'hôtes"
  },
  26: {
    icon: Icon26,
    name: 'Mobile-home'
  },
  27: {
    icon: Icon27,
    name: 'Pod'
  },
  28: {
    icon: Icon28,
    name: 'Roulotte'
  },
  29: {
    icon: Icon29,
    name: 'Tente lodge'
  },
  30: {
    icon: Icon30,
    name: 'Tiny House'
  },
  31: {
    icon: Icon31,
    name: 'Tipi'
  },
  32: {
    icon: Icon32,
    name: 'Tonneau'
  },
  33: {
    icon: Icon33,
    name: 'Villa'
  },
  34: {
    icon: Icon34,
    name: 'Yourte'
  },
  35: {
    icon: Icon1,
    name: 'Établissement'
  }
}

export default icons
