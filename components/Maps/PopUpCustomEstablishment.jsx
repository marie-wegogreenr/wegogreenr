import React, { useEffect, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
import Image from 'next/image'
import icons from './icons'
//UTILS
import { getImageURL } from 'utils'
import types from 'constants/establishmentTypes'

const PopUpCustomEstablishment = ({ establishment }) => {
  const {
    id,
    type_id = 20,
    name = 'Nom caché',
    basic_price = 'Prix ​​caché',
    room_image = [],
    external_images = [],
    slug = 'SLUG_NO_FOUND'
  } = establishment
  return (
    <div className="PopUpCustom">
      <a
        href={`/establishment/${id}`}
        target="_blank"
        className="text-decoration-none"
      >
        <div className="d-flex p-3 align-items-center flex-column flex-md-row my-2">
          <div className="PopUpCustom-image">
            <img
              src={external_images[0]}
              width={139}
              height={89}
              className="br-10"
            />
          </div>
          <div className="PopUpCustom-content--wide">
            <div className="ml-3">
              <p className="weight-bold text font-montserrat mt-0 mb-3 text-center text-md-left">
                {name}
              </p>
              <div className="d-flex justify-content-md-between flex-column flex-md-row justify-content-center align-items-center">
                <div className="d-flex align-items-center mr-3 flex-column flex-md-row">
                  <img
                    src={icons[35]?.icon || icons[1].icon}
                    alt=""
                    width="40"
                    className="rounded-3"
                  />
                  <p className="m-0">{types[type_id - 1]}</p>
                </div>
                <div className="d-flex align-items-center flex-column">
                  <p className="m-0 text-nowrap">A partir de</p>
                  <p className="m-0">
                    <span className="weight-bold ">{basic_price}&euro;</span>
                    /nuit
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
  )
}

export default PopUpCustomEstablishment
