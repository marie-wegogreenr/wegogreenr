import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons'

export const CustomDatePicker = React.forwardRef((props, ref) => {
  return (
    <div className="CustomDatePicker">
      <label onClick={props.onClick} ref={ref}>
        {props.value || props.placeholder}
      </label>
      <FontAwesomeIcon
        icon={faCalendarAlt}
        onClick={props.onClick}
        className={'CustomDatePicker_icon icon-width'}
      />
    </div>
  )
})
