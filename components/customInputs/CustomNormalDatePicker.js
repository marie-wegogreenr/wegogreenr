import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons'

export const CustomNormalDatePicker = React.forwardRef((props, ref) => {
  return (
    <div className="CustomDatePicker CustomDatePicker-form-control">
      <label onClick={props.onClick} ref={ref}>
        {props.value || props.placeholder}
      </label>
      <FontAwesomeIcon
        icon={faCalendarAlt}
        onClick={props.onClick}
        className={'CustomDatePicker_icon-nomove  icon-width'}
      />
    </div>
  )
})
