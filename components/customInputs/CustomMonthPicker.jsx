import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons'

export const CustomMonthPicker = React.forwardRef((props, ref) => {
  return (
    <div className="CustomMonthPicker" onClick={props.onClick}>
      <label ref={ref}>{props.value || props.placeholder}</label>
      <FontAwesomeIcon
        icon={faCalendarAlt}
        onClick={props.onClick}
        className={'CustomMonthPicker_icon'}
        width={16}
        height={16}
      />
    </div>
  )
})
