import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons'

export const CustomDatePickerSearch = React.forwardRef((props, ref) => {
  return (
    <div
      className="custom-date-picker-search"
      onClick={props.onClick}
      ref={ref}
    >
      <div className="custom-date-picker-search__wrapper">
        <span className="custom-date-picker-search__title">{props.title}</span>
        <label className="custom-date-picker-search__label">
          {props.value || props.placeholder}
        </label>
      </div>
      <FontAwesomeIcon
        icon={faCalendarAlt}
        onClick={props.onClick}
        className={'custom-date-picker-search__icon icon-width'}
      />
    </div>
  )
})
