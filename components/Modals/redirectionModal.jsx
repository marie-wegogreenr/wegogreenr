import * as React from 'react'
import Popup from 'public/images/search/abra-popup.png'
import PopupMobile from 'public/images/search/abra-popup-mob_.png'
import { useWindowSize } from 'hooks/useWindowSize'

function RedirectionModal(props) {
  const { width } = useWindowSize()
  const [fields, setFields] = React.useState({
    isSaveButtonDisabled: true,
    optionSelected: ''
  })
  const [photo, setPhoto] = React.useState(Popup)

  React.useEffect(() => {
    if (width) {
      if (width < 768) {
        setPhoto(PopupMobile)
      } else {
        setPhoto(Popup)
      }
    }
  }, [width])

  const handleOnSave = async () => {
    props.onSave(fields.optionSelected)
  }

  const handleOptionSelected = (e) => {
    setFields({
      ...fields,
      isSaveButtonDisabled: false,
      optionSelected: e.target.value
    })
  }
  return (
    <>
      <div
        className="modal fade redirection-modal__wrapper"
        id="redirectionModal"
        data-bs-backdrop="static"
        data-bs-keyboard="false"
        tabindex="-1"
        aria-labelledby="staticBackdropLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content redirection-modal__content">
            {/* <div className="modal-header">
              <img
                src={`/images/logo.svg`}
                alt="Logo"
                className="rounded-circle"
              />
            </div>
            <div className="modal-body">
              <div className="card card-body">
                <h4>
                  Vous allez être redirigé vers le site de notre partenaire
                  Abracadaroom, spécialiste de l’hébergement insolite en France.
                </h4>
              </div>
            </div> */}
            <img
              src={photo}
              alt="abracadaroom-redirection-popup"
              className="redirection-modal__img"
            />
          </div>
        </div>
      </div>
    </>
  )
}

export default RedirectionModal
