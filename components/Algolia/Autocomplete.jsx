import React, { Component } from 'react'
import { consoleLog } from '/utils/logConsole'
import PropTypes from 'prop-types'
import { Highlight, connectAutoComplete } from 'react-instantsearch-dom'
import AutoSuggest from 'react-autosuggest'

class AutoComplete extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: '',
      showInput: false
    }
  }

  async componentDidMount() {
    if (this.props.defaultValue !== '') {
      this.setState({ ...this.state, value: this.props.defaultValue }, () => {
        this.setState({ ...this.state, showInput: true })
      })
    } else {
      this.setState(
        { ...this.state, value: this.props.currentRefinement },
        () => {
          this.setState({ ...this.state, showInput: true })
        }
      )
    }
  }

  static propTypes = {
    hits: PropTypes.arrayOf(PropTypes.object).isRequired,
    currentRefinement: PropTypes.string.isRequired,
    refine: PropTypes.func.isRequired,
    onSuggestionSelected: PropTypes.func.isRequired,
    onSuggestionCleared: PropTypes.func.isRequired
  }

  /* state = {
    value: this.props.currentRefinement
  } */

  onChange = (_, { newValue }) => {
    if (!newValue) {
      this.props.onSuggestionCleared()
    }

    this.setState({
      value: this.props.value || ''
    })
  }

  onSuggestionsFetchRequested = ({ value }) => {
    this.props.refine(value)
  }

  onSuggestionsClearRequested = () => {
    this.props.refine()
  }

  onSuggestionSelected = () => {}

  getSuggestionValue = (hit) => {
    this.setState({
      value: hit
    })
    this.props.onSelectOption(hit)
    return hit.name
  }

  renderSuggestion(hit) {
    return <Highlight attribute="name" hit={hit} tagName="mark" />
  }

  render() {
    const {
      hits,
      placeholder,
      onBlur,
      onFocus,
      inputRef,
      onChange,
      inputValue,
      inputDisabled,
      inputClassName,
      id
    } = this.props

    const inputProps = {
      placeholder,
      onChange: onChange ?? this.onChange,
      onBlur: onBlur,
      onFocus: onFocus,
      ref: inputRef,
      value: inputValue ?? this.state.value,
      disabled: inputDisabled,
      className: inputClassName,
      id,
      required: true
    }

    return (
      <>
        {this.state.showInput && (
          <AutoSuggest
            suggestions={hits}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
            onSuggestionSelected={this.onSuggestionSelected}
            getSuggestionValue={this.getSuggestionValue}
            renderSuggestion={this.renderSuggestion}
            inputProps={inputProps}
          />
        )}
      </>
    )
  }
}

export default connectAutoComplete(AutoComplete)
