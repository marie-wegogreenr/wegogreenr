import React, { Component } from 'react'
import algoliasearch from 'algoliasearch/lite'
import {
  InstantSearch,
  Configure,
  Hits,
  Highlight,
  connectSearchBox
} from 'react-instantsearch-dom'
import Autocomplete from './Autocomplete'

const VirtalSearchBox = connectSearchBox(() => null)

class SearchAlgolia extends Component {
  state = {
    query: '',
    searchClient: algoliasearch(
      process.env.NEXT_PUBLIC_ALGOLIA_APP_ID,
      process.env.NEXT_PUBLIC_ALGOLIA_SECRET
      /* 'latency',
        '6be0576ff61c053d5f9a3225e2a90f76' */
    )
  }

  onSuggestionSelected = (_, { suggestion }) => {
    this.setState({
      query: suggestion.name
    })
  }

  onSuggestionCleared = () => {
    this.setState({
      query: ''
    })
  }

  onSelectOption = (selected_value) => {
    this.props.onSelectOption(selected_value)
  }

  render() {
    const {
      index,
      placeholder,
      attribute,
      defaultValue,
      onBlur,
      onFocus,
      inputRef,
      inputValue,
      onChange,
      inputDisabled = false,
      inputClassName = 'form-control p-4',
      id
    } = this.props

    return (
      <InstantSearch
        indexName={index}
        searchClient={this.state.searchClient}
        isRequired
      >
        <Configure hitsPerPage={80} />
        <Autocomplete
          onSuggestionSelected={this.onSuggestionSelected}
          onSuggestionCleared={this.onSuggestionCleared}
          onSelectOption={this.onSelectOption}
          placeholder={placeholder}
          attribute={attribute}
          defaultValue={defaultValue}
          onBlur={onBlur}
          onFocus={onFocus}
          inputRef={inputRef}
          inputValue={inputValue}
          onChange={onChange}
          inputDisabled={inputDisabled}
          inputClassName={inputClassName}
          id={id}
          autocomplete="nope"
        />
      </InstantSearch>
    )
  }
}

export default SearchAlgolia
