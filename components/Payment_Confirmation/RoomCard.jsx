import Image from 'next/image'
import React from 'react'
import { useSelector } from 'react-redux'
import moment from 'moment'
//UTILS
import { getImageURL } from 'utils'
import { adjustNumberWithComma } from 'helpers/utilsHelper'

const RoomCard = () => {
  const store = useSelector((store) => store.checkoutReducer.reservation)
  //consoleLog(store)

  if (!store.room || !store || !store.total_price) {
    return <></>
  }

  const {
    room: { public_name, room_image },
    checkIn,
    checkOut,
    total_price
  } = store

  return (
    <div className="d-flex flex-column flex-lg-row">
      <Image
        src={getImageURL(room_image[0]?.image?.url)}
        width={250}
        height={130}
        className="obj-fit-cover"
      />
      <div className="ms-2 d-flex flex-column justify-items-center mt-4 mt-lg-2 mx-4 ">
        <p className="subtitle-dashboard weight-bold">{public_name}</p>
        <div>
          <div className="d-flex justify-content-between">
            <b className="text m-0 weight-bold color-black">Prix total :</b>
            <p className="text m-0">
              {adjustNumberWithComma(total_price)}&euro;
            </p>
          </div>
          <div className="d-flex justify-content-between mt-1">
            <b className="text m-0 weight-bold color-black">Du :</b>
            <p className="text m-0">{moment(checkIn).format('DD/MM/YYYY')}</p>
          </div>
          <div className="d-flex justify-content-between mt-1">
            <b className="text m-0 weight-bold color-black">Au:</b>
            <p className="text m-0">{moment(checkOut).format('DD/MM/YYYY')}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default RoomCard
