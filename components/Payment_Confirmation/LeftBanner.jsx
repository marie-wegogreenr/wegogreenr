import Image from 'next/image'
import React from 'react'
import { consoleLog } from '/utils/logConsole'

const LeftBanner = () => {
  return (
    <div className="col-4 container-login-left d-none d-lg-block">
      <img src="/images/logo_white.svg" alt="Logo" className="image-logo" />
      <div className="text-login">
        {/* <h2>Phrase d'accroche ici pour</h2>
        <h2>Motiver un peu</h2> */}
      </div>
      <div>
        <img
          src="/images/background-login.png"
          alt="Image"
          className="image-background"
        />
      </div>
    </div>
  )
}

export default LeftBanner
