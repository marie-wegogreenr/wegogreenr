import React from 'react'
import { consoleLog } from '/utils/logConsole'

import dynamic from 'next/dynamic'

const DynamicComponentWithNoSSR = dynamic(
  () => import('@components/Maps/Map'),
  { ssr: false }
)

const Map = (props) => {
  return <DynamicComponentWithNoSSR center={props.center} />
}

export default Map
