import { useEffect, useState } from 'react'

function RadioButtonSection({ title, description, updateState, acceptCoupon }) {
  return (
    <div className="card-white__responsive">
      <h3 className="card-white__responsive-title">{title}</h3>
      <p>{description}</p>
      <div className="custom-control custom-switch">
        <input
          type="checkbox"
          className="custom-control-input"
          id="accept-coupon"
          checked={acceptCoupon === '1' || acceptCoupon === 1}
        />
        <label
          className="custom-control-label ml-2"
          htmlFor="accept-coupon"
          style={{ cursor: 'pointer' }}
          onClick={() => updateState()}
        ></label>
      </div>
    </div>
  )
}

export default RadioButtonSection
