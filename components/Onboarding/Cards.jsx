import React from 'react'

const Cards = ({ text }) => {
  return (
    <div class="card shadow rounded">
      <div class="card-body">
        <p class="card-text weight-bold text-justify">{text}</p>
        <a href="#!" class="card-link">
          Savoir plus
        </a>
      </div>
    </div>
  )
}

export default Cards
