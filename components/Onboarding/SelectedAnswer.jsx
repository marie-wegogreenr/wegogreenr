function SelectedAnswer({
  id,
  question,
  questionNumber,
  answerOptions,
  answer
}) {
  let finalAnswer = []

  finalAnswer = answerOptions.filter((item) => item.key === answer)

  const answers = {
    firstOption: `Annulation gratuite jusqu’à J-7 (avec retenue de 2% pour les frais bancaires, donc remboursement de 98%)`,
    otherOption: `Non remboursable à moins de 7 jours avant le début du séjour ou non-présentation`
  }

  return (
    <>
      <p>
        <strong>{question}</strong>
      </p>

      {finalAnswer?.length > 0 ? (
        finalAnswer?.map((item) => <p key={item.key}>{item.option}</p>)
      ) : questionNumber === 1 ? (
        <p>{answers.firstOption}</p>
      ) : (
        <p>{answers.otherOption}</p>
      )}
    </>
  )
}

export default SelectedAnswer
