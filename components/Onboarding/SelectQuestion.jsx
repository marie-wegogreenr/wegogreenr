function SelectQuestion({
  id,
  question,
  options,
  selected,
  handleChange,
  defaultValue
}) {
  return (
    <>
      <p>
        <strong>{question}</strong>
      </p>
      <label
        className="form-check-label ob-question-label"
        htmlFor={`question-${id}`}
      >
        Choisir une option
      </label>
      <select
        id={`question-${id}`}
        className="form-control ob-question-select"
        onChange={handleChange}
        defaultValue={defaultValue}
      >
        <option value="">Sélectionnez...</option>
        {options?.map((item) => (
          <option key={item.key} value={item.key}>
            {item.option}
          </option>
        ))}
      </select>
    </>
  )
}

export default SelectQuestion
