import React from 'react'

import yellowLine from 'public/images/icons/yellow-curved-line-long.svg'
import Cards from './Cards'

const cards = [
  {
    text: 'Que veulent les clients maintenant et que pouvez-vous y faire?'
  },
  {
    text: 'Que veulent les clients maintenant et que pouvez-vous y faire?'
  },
  {
    text: 'Que veulent les clients maintenant et que pouvez-vous y faire?'
  }
]

const FarSection = () => {
  return (
    <div className="row mb-5">
      <div className="col-12 text-center">
        <h2>Aller + loin</h2>
        <img src={yellowLine} alt="" className="mb-2 mt-2" width="250px" />
        <p>Quelques conseils pour commencer</p>
      </div>
      <div className="col-12 mt-5">
        <div className="row">
          {cards.map((card, index) => (
            <div className="col-4 d-flex justify-content-center" key={index}>
              <Cards text={card.text} />
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default FarSection
