export default function GoBackBtn({ link }) {
  return (
    <div className="col-12 col-xl-6 button-back-onboarding p-0 pr-xl-2">
      <a
        className="button button-outline-black button-long"
        type="button"
        href={link}
      >
        Retour
      </a>
    </div>
  )
}
