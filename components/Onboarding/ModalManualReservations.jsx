import * as React from 'react'

import axios from 'axios'

function ModalManualReservations(props) {
  const [fields, setFields] = React.useState({
    isSaveButtonDisabled: true,
    optionSelected: ''
  })

  const handleOnSave = async () => {
    props.onSave(fields.optionSelected)
  }

  const handleOptionSelected = (e) => {
    setFields({
      ...fields,
      isSaveButtonDisabled: false,
      optionSelected: e.target.value
    })
  }
  return (
    <>
      <div
        className="modal fade"
        id="staticBackdrop"
        data-bs-backdrop="static"
        data-bs-keyboard="false"
        tabindex="-1"
        aria-labelledby="staticBackdropLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="staticBackdropLabel">
                Une dernière petite chose
              </h5>
            </div>
            <div className="modal-body">
              <div className="card card-body">
                <div className="row">
                  <div className="col-12">
                    <h3>Type de réservation</h3>
                  </div>
                  <div className="form-check mt-3 col-6">
                    <div className="row">
                      <div className="col-10">
                        <label htmlFor="type_reservation">
                          <b>Réservation instantanée</b>
                        </label>
                        <p>
                          Ce que nos voyageurs recherchent en priorité. Les
                          réservations sont automatiquement acceptées et
                          ajoutées à votre calendrier. Vous recevez un email
                          avec toutes les informations de la réservation.
                        </p>
                      </div>
                      <div className="col-2" style={{ textAlign: 'end' }}>
                        <input
                          className="form-check-input"
                          type="radio"
                          name="type_reservation"
                          id="type_reservation"
                          value="0"
                          onChange={handleOptionSelected}
                          required
                        />
                      </div>
                    </div>
                  </div>
                  <div className="form-check mt-3 col-6">
                    <div className="row">
                      <div className="col-10">
                        <label htmlFor="type_reservation">
                          <b>Demande de réservation</b>
                        </label>
                        <p>
                          Vous êtes informé par email d'une nouvelle demande de
                          réservation : vous avez 24h pour l'accepter ou la
                          refuser. Au-delà, celle-ci est automatiquement
                          annulée.
                        </p>
                      </div>
                      <div className="col-2" style={{ textAlign: 'end' }}>
                        <input
                          className="form-check-input"
                          type="radio"
                          name="type_reservation"
                          id="type_no_reservation"
                          value="1"
                          onChange={handleOptionSelected}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                disabled={fields.isSaveButtonDisabled}
                onClick={handleOnSave}
              >
                Sauvegarder
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default ModalManualReservations
