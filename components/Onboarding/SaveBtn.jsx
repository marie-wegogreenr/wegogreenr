export default function SaveBtn({ statusBtn }) {
  return (
    <div className="col-12 col-xl-6 justify-content-end d-flex p-0 pl-xl-2">
      <button
        type="submit"
        className="button button-long button--save-onboarding"
        disabled={
          statusBtn !== 'loading' && statusBtn !== 'saved' ? false : true
        }
      >
        {statusBtn === 'loading'
          ? 'En cours...'
          : statusBtn === 'saved'
          ? 'Enregistré'
          : 'Sauvegarder et continuer'}
      </button>
    </div>
  )
}
