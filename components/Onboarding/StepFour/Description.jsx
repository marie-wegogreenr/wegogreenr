export default function Description() {
  const data = {
    title: `Calendrier et disponibilités`,
    text1: `Indiquez les dates indisponibles à la réservation.`,
    text2: `Vous pourrez modifier le calendrier à tout moment, même après la mise en ligne de l’annonce.`,
    text3: `Pour connecter d'autres calendries extérieurs, rendez vous sur votre espace client`
  }

  return (
    <>
      <h2>{data.title}</h2>
      <p>{data.text1}</p>
      <p>{data.text2}</p>
      <p>{data.text3}</p>
    </>
  )
}
