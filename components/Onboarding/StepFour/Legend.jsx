export default function Legend() {
  const data = {
    textLegend: `Légende:`,
    available: `Disponible`,
    unavailable: `Non disponible`
  }
  return (
    <div className="col-12 col-md-12 col-lg-12">
      <div className="onBoardingHebergements__legenge">
        <span>{data.textLegend}</span>
        <div>
          <div className="box box-not-availabilitie ml-2 mr-2">
            <div className="line-box ml-1"></div>
          </div>
          <p>{data.unavailable}</p>
        </div>
        <div>
          <div className="box box-availabilitie ml-2 mr-2"></div>
          <p>{data.available}</p>
        </div>
      </div>
    </div>
  )
}
