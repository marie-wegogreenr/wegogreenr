import React from 'react'
import { consoleLog } from '/utils/logConsole'

import SliderInfoGoogle from './SliderInfoGoogle'

class PopupInfoGoogle extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.handleCloseDialog = this.handleCloseDialog.bind(this)
  }

  handleCloseDialog() {
    this.props.closeDialog()
  }

  render() {
    return (
      <div className="modal-google-info">
        <div className="modal-content-google-info">
          <span className="close-google-info" onClick={this.handleCloseDialog}>
            &times;
          </span>
          <h2>How configure google calendar?</h2>
          <SliderInfoGoogle />
        </div>
      </div>
    )
  }
}

export default PopupInfoGoogle
