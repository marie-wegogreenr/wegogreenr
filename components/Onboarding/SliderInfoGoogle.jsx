import React from 'react'
import { consoleLog } from '/utils/logConsole'

class SliderInfoGoogle extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <div
        id="carouselExampleIndicators"
        className="carousel slide"
        data-ride="carousel"
      >
        <ol className="carousel-indicators">
          <li
            data-target="#carouselExampleIndicators"
            data-slide-to="0"
            className="active"
          ></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img
              className="d-block w-100"
              src="/images/onboarding/calendar_info/1_step.png"
              alt="First slide"
            />
            <div className="carousel-caption d-none d-md-block">
              <h5 style={{ color: '#000' }}>
                Comment récuperer l'id de mon agenda
              </h5>
              <p>Choisir le calendar de ma chambre et récupérer l'id.</p>
            </div>
          </div>

          <div className="carousel-item">
            <img
              className="d-block w-100"
              src="/images/onboarding/calendar_info/2_step.png"
              alt="Second slide"
            />
            <div className="carousel-caption d-none d-md-block">
              <h5 style={{ color: '#000' }}>
                Comment récuperer l'id de mon agenda
              </h5>
              <p>Choisir le optien "ID de l'agenda"</p>
            </div>
          </div>

          <div className="carousel-item">
            <img
              className="d-block w-100"
              src="/images/onboarding/calendar_info/3_step.png"
              alt="Third slide"
            />
            <div className="carousel-caption d-none d-md-block">
              <h5 style={{ color: '#000' }}>
                Autoriser à WeGoGreenR d'editer mon agenda (si réservation)
              </h5>
              <p>Je me connecte sur mon agenda à partir de Google Calendar</p>
              <p>Je rentre dans mon agenda et je vais aux paramètres</p>
            </div>
          </div>

          <div className="carousel-item">
            <img
              className="d-block w-100"
              src="/images/onboarding/calendar_info/4_step.png"
              alt="Fourth slide"
            />
            <div className="carousel-caption d-none d-md-block">
              <h5>
                Autoriser à WeGoGreenR d'editer mon agenda (si réservation)
              </h5>
              <p style={{ color: '#fff' }}>
                Je rajoute ce link
                wgg-221@wegogreenr-308121.iam.gserviceaccount.com en "partager
                avec des personnes"
              </p>
              <p style={{ color: '#fff' }}>
                Je choisis l'option "Apporter des modifications et gérer le
                partage"
              </p>
            </div>
          </div>
        </div>
        <a
          className="carousel-control-prev"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="prev"
          style={{ backgroundColor: '#808080' }}
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="sr-only">Previous</span>
        </a>
        <a
          className="carousel-control-next"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="next"
          style={{ backgroundColor: '#808080' }}
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="sr-only">Next</span>
        </a>
      </div>
    )
  }
}

export default SliderInfoGoogle
