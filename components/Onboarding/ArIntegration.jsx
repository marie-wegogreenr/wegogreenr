import { useState, useEffect } from 'react'
import { getAREtablissement, setAREtablissement } from 'services/ARService'
import { useDispatch, useSelector } from 'react-redux'
import { setClearLoading } from '@ducks/Config/actions'
import { useToast } from 'hooks/useToast'
import { setGreenScore } from 'services/onBoardingService'
import { getAddressFromCoordinates } from 'services/etablissementService'
import { findRegionAndCity } from 'helpers/localizationHelper'

export const ArIntegration = ({
  email,
  from = 'onboarding',
  updateData,
  level,
  greenscore
}) => {
  const dispatch = useDispatch()
  const [isChecked, setisChecked] = useState(from === 'dashboard')
  const [fields, setfields] = useState({ type: 'hotel', email })
  const [arEtablishment, setarEtablishment] = useState(null)
  const [selected, setselected] = useState(null)
  const [notify] = useToast()

  const searchARetablissement = async () => {
    setselected(null)
    dispatch(setClearLoading(true))

    const { code, data } = await getAREtablissement(fields)
    if (code === 200) {
      setarEtablishment(data)
      dispatch(setClearLoading(false))
    } else if (code === 203) {
      if (data?.Message === 'You dont have establishments to link') {
        dispatch(setClearLoading(false))
        return setarEtablishment([])
      }
      if (data?.Message === 'This email was previously linked') {
        notify(
          "Je ne retrouve plus l'établissement dans la base open pro",
          'warning'
        )
        dispatch(setClearLoading(false))
      }
    } else {
      notify('Il y a eu un problème, veuillez réessayer plus tard', 'error')
      dispatch(setClearLoading(false))
    }
  }

  const saveSelectedestablishment = async (index) => {
    setselected(index)
    dispatch(setClearLoading(true))
    let { code, data } = await setAREtablissement(arEtablishment[index])
    dispatch(setClearLoading(false))
    if (code !== 200) {
      return notify(
        'il y a eu un problème, veuillez réessayer plus tard',
        'error'
      )
    }
    data = Array.isArray(data) ? data[0] : data
    await updateEstablishmentData(data)
    return
    const greenScoreBody = {
      greenscore,
      level,
      establishment_id: data.id
    }
    await setGreenScore(greenScoreBody)
    return notify('Intégration Réussie', 'success')
  }

  const updateEstablishmentData = async (data) => {
    const googleRegionData = await getAddressFromCoordinates(data.lat, data.lng)
    const { address_components } = googleRegionData.results[0]

    const { region_name, city_name } = findRegionAndCity(address_components)
    updateData((state) => ({
      ...state,
      status_stablishment: 'update',
      lat: data.lat,
      lng: data.lng,
      region_name,
      city_name,
      address_stablishment: data.address,
      cp: data.zip_code,
      stablishments: [{ ...state.stablishments[0], ...data }],
      canSearh: false
    }))
  }

  return (
    <div className="card-white">
      <div>
        <h4
          htmlFor="how_know_us"
          className={from === 'dashboard' ? 'ar-title' : ''}
        >
          Utilisez-vous Openpro pour la gestion de votre établissement ?
        </h4>
        <div className="custom-control custom-switch custom-switch-lg mt-4">
          <input
            type="checkbox"
            className="custom-control-input"
            id="ARCheck"
            checked={isChecked}
            onClick={() => {
              setisChecked(!isChecked)
            }}
          />
          <label
            className="custom-control-label ml-2"
            style={{ cursor: 'pointer' }}
            htmlFor="ARCheck"
          >
            {isChecked ? 'Oui' : 'Je n’utilise pas Openpro'}
          </label>
        </div>

        {isChecked && (
          <>
            <h4 className="py-4">
              Renseignez l’email utilisé dans Openpro et le type d’établissement
            </h4>
            <div className="row align-items-center">
              <div className="col-12 col-md-5 col-xl-5 mb-0">
                <div className="form-group mb-0">
                  <h4 htmlFor="email">Email</h4>
                  <input
                    type="email"
                    className="form-control"
                    id="email"
                    name="email"
                    aria-describedby="emailHelp"
                    placeholder=""
                    onChange={(e) => {
                      setfields({ ...fields, email: e.target.value })
                    }}
                  />
                </div>
              </div>
              <div className="col-12 col-md-5 col-xl-5 mb-0">
                <div className="form-group mb-0">
                  <h4 htmlFor="telephone">Type d'établissement</h4>
                  <select
                    name="type"
                    onChange={(e) => {
                      e.preventDefault()
                      setfields({ ...fields, type: e.target.value })
                    }}
                  >
                    <option value="hotel">hôtel</option>
                    <option value="meuble">meublé de tourisme</option>
                    <option value="camping">camping</option>
                    <option value="chhote">chambre-hôte</option>
                  </select>
                </div>
              </div>
              <button
                style={{ height: 'fit-content' }}
                className="col-12 col-md-2 col-xl-2 btn btn-secondary py-2"
                onClick={searchARetablissement}
                type="button"
              >
                Rechercher
              </button>
            </div>
            {arEtablishment !== null && arEtablishment?.length !== 0 && (
              <>
                <h4 className="py-4">2. Sélectionner l'établissement</h4>
                <div className="d-flex justify-content-center row">
                  {arEtablishment.map((ar, index) => (
                    <div
                      class={`ar-establishment-card col-4 mt-4 ${
                        index === selected ? 'is-selected' : ''
                      }`}
                      onClick={() => {
                        saveSelectedestablishment(index)
                      }}
                      key={index}
                    >
                      <div className="ar-establishment-card-content">
                        <img src={ar?.CollectionImage?.Image[0]} />
                        <p>{ar?.Commune?.NomCommune}</p>
                      </div>
                    </div>
                  ))}
                </div>
              </>
            )}
            {arEtablishment?.length === 0 && (
              <p>Il n’y a aucun établissement associé</p>
            )}
          </>
        )}
      </div>
    </div>
  )
}
