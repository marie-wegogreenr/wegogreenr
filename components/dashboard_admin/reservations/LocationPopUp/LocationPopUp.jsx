import { Arrive } from '@components/Icons'
import Image from 'next/image'
import React from 'react'
import Map from './Map'
import SliderPhotos from '../SliderPhotos'
import AditionalInfo from './AditionalInfo'
import moment from 'moment'
//STYLES
import styles from './styles.module.scss'

const LocationPopUp = ({ data, setState, state }) => {
  const {
    id,
    user_id,
    activity_id,
    room_id,
    code,
    status,
    number_of_adults,
    number_of_children,
    room_price,
    arrival_date,
    departure_date,
    created_at,
    updated_at,
    images
  } = data

  const totalUsers = number_of_adults + number_of_children

  return (
    <div className={`col-5 ${styles.locationPopUp}`}>
      <div onClick={() => setState(!state)}>X</div>
      <h3>Votre reservation</h3>
      <SliderPhotos
        images={images}
        id={`${code}-slider`}
        heigth={250}
        width={500}
      />
      <p>{}</p>
      <hr />
      <div className="d-flex justify-content-between">
        <p>ID de la reservation</p>
        <p>{code}</p>
      </div>
      <div className="d-flex justify-content-between">
        <p>Voyageurs</p>
        <p>{totalUsers}</p>
      </div>
      <div className="d-flex justify-content-between">
        <p>Dates &amp; heure </p>
        <p>{code}</p>
      </div>
      <div className="d-flex justify-content-between">
        <div>
          <b>Arrivée</b>
          <br />
          {moment(arrival_date).format('dddd, Do MMMM  YYYY')}
        </div>
        <div>
          <b>Départ</b>
          <br />
          {moment(departure_date).format('dddd, Do MMMM  YYYY')}
        </div>
      </div>
      <p>Votre Logement</p>
      <hr />
      <div className="d-flex justify-content-between">
        <b>Responsable</b>
        <div className="d-flex align-items-center justify-content">
          <Image
            width={25}
            height={25}
            className="rounded-circle"
            src="http://dummyimage.com/499x315.jpg/ff4444/ffffff"
          />
          <p>{user_id}</p>
        </div>
      </div>
      <div className="d-flex justify-content-between">
        <b>Type de logement</b>
        <p>{}</p>
      </div>
      <div className="d-flex justify-content-between">
        <b>Type de logement</b>
        <p>{}</p>
      </div>
      <div className="d-flex justify-content-between">
        <b>Adresse</b>
        <p>`{}` </p>
      </div>
      <div>
        <Map />
        <button className="btn btn-primary">
          Envoyer un message a l'etablissement
        </button>
      </div>
      <p>Informations de paiement</p>
      <hr />
      <div className="d-flex justify-content-between">
        <b>1* chambre parental * 5 jours</b>
        <p>700€</p>
      </div>
      <div className="d-flex justify-content-between">
        <b>Taxe de séjour * 5 jours</b>
        <p>50€</p>
      </div>
      <div className="d-flex justify-content-between">
        <b>Total</b>
        <p>750€</p>
      </div>
      <hr />
      <div className="d-flex justify-content-between mb-5">
        <span>
          <Arrive />
          <b>Annulation de la reservation ?</b>
        </span>
        <a href="#">Annuler la reservation </a>
      </div>
      {/* <AditionalInfo /> */}
    </div>
  )
}

export default LocationPopUp
