import React from 'react'
import { consoleLog } from '/utils/logConsole'
import {
  GoogleMap,
  LoadScript,
  Marker,
  Autocomplete
} from '@react-google-maps/api'

const containerStyle = {
  width: '400px',
  height: '400px'
}

function Map({
  center = {
    lat: 46.2245,
    lng: 2.2123
  }
}) {
  //juan key AIzaSyBDgmjXDEEZaPVIt00oxXGL6GNo7lGi8OM
  //client's key AIzaSyCD2m7tKguF7vbh__Cwy3nbTwacV0DJ2aY
  return (
    <GoogleMap mapContainerStyle={containerStyle} center={center} zoom={10}>
      <Marker position={center} />
      <Autocomplete>
        <input
          type="text"
          style={{
            boxSizing: `border-box`,
            border: `1px solid transparent`,
            width: `240px`,
            height: `32px`,
            padding: `0 12px`,
            borderRadius: `3px`,
            boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
            fontSize: `14px`,
            outline: `none`,
            textOverflow: `ellipses`,
            position: 'absolute',
            left: '50%',
            marginLeft: '-120px'
          }}
        />
      </Autocomplete>
      {/* Child components, such as markers, info windows, etc. */}
      <Marker></Marker>
    </GoogleMap>
  )
}

export default React.memo(Map)
