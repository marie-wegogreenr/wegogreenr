import React from 'react'
import { consoleLog } from '/utils/logConsole'

const InfoCard = ({ text, link = '#', linkText }) => {
  return (
    <div className="card col-3">
      <p>{text}</p>
      <a href={link}>{linkText}</a>
    </div>
  )
}

export default InfoCard
