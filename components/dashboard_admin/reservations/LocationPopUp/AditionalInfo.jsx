import React from 'react'
import { consoleLog } from '/utils/logConsole'
import InfoCard from './InfoCard'

const lorem =
  'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Impedit, tenetur. '

const AditionalInfo = () => {
  return (
    <>
      <div className="border-bottom">
        <b>Informations additionelle</b>
      </div>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus
        autem minima impedit maiores corrupti accusantium officiis, at eum vel
        iusto. Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi,
        distinctio?
      </p>
      <div className="d-flex justify-content-between">
        <InfoCard text={lorem} linkText="Afficher le detail" />
        <InfoCard text={lorem} linkText="Afficher le detail" />
        <InfoCard text={lorem} linkText="Afficher le detail" />
      </div>
    </>
  )
}

export default AditionalInfo
