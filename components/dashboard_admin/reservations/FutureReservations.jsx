import React from 'react'
import LocationCard from './LocationCard'
import { dateFromDb } from 'helpers/timeHelper'

const FutureReservations = ({ reservations, filters }) => {
  let allReservations = reservations

  if (filters.date) {
    const filterDate = dateFromDb(filters.date).getTime()
    allReservations = reservations.filter((item) => {
      if (dateFromDb(item.departure_date).getTime() < filterDate) return item
    })
  }

  return (
    <>
      {reservations &&
        allReservations.map((item, index) => (
          <LocationCard data={item} key={index} />
        ))}
    </>
  )
}

export default FutureReservations
