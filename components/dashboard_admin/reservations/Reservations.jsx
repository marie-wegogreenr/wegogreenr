import React from 'react'
import { consoleLog } from '/utils/logConsole'
import FutureReservations from './FutureReservations'
import PreviousReservations from './PreviousReservations'
//redux
import { useSelector } from 'react-redux'

const Reservations = ({ filters }) => {
  const data = useSelector((store) => store.user.all_reservations)

  return (
    <div className="tab-content" id="myTabContent">
      <div
        className="tab-pane fade show active"
        id="home"
        role="tabpanel"
        aria-labelledby="home-tab"
      >
        <FutureReservations reservations={data} filters={filters} />
      </div>
      <div
        className="tab-pane fade"
        id="profile"
        role="tabpanel"
        aria-labelledby="profile-tab"
      >
        <PreviousReservations reservations={data} filters={filters} />
      </div>
    </div>
  )
}

export default Reservations
