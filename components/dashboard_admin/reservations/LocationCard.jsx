import React, { useState } from 'react'
import LocationPopUp from './LocationPopUp/LocationPopUp'
import SliderPhotos from './SliderPhotos'
import { adjustNumberWithComma } from 'helpers/utilsHelper'

const LocationCard = ({ data }) => {
  const {
    id,
    user_id,
    activity_id,
    room_id,
    code,
    status,
    number_of_adults,
    number_of_children,
    room_price,
    arrival_date,
    departure_date,
    created_at,
    updated_at,
    images,
    room: { internal_name },
    principal_image
  } = data

  const [popUp, setPopUp] = useState(false)

  const totalUsers = number_of_adults + number_of_children
  return (
    <>
      <div className="row location-card background-white">
        <div className="col-3">
          <SliderPhotos
            images={[process.env.NEXT_PUBLIC_AMAZON_IMGS + principal_image]}
            id={code}
          />
        </div>
        <div className="col-9">
          <div className="row justify-content-between">
            <div className="col-5">
              <h2 className="weight-bold font-montserrat">{internal_name}</h2>
              <p>
                Voyageurs: {totalUsers} personne{totalUsers > 1 && 's'}
              </p>
              <p>
                Début: {arrival_date} - Fin :{departure_date}
              </p>
            </div>
            <div className="col-4 text-center">
              Cout total <br />
              {adjustNumberWithComma(room_price)}€
            </div>
            <div className="col-3 text-center">
              <a onClick={() => setPopUp(!popUp)} href="#">
                Afficher le détail
              </a>
            </div>
          </div>
        </div>
      </div>

      {popUp && <LocationPopUp data={data} setState={setPopUp} state={popUp} />}
    </>
  )
}

export default LocationCard
