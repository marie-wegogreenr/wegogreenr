import Image from 'next/image'
import React from 'react'
import { consoleLog } from '/utils/logConsole'

const SliderPhotos = ({ images = [], id, heigth = 150, width = 291 }) => {
  return (
    // <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
    <div id={id} className="carousel slide" data-ride="carousel">
      <div className="carousel-inner">
        {images.map((item, index) => (
          <div
            className={`carousel-item ${index == 0 ? 'active' : ''}`}
            key={index}
          >
            <Image
              width={width}
              height={heigth}
              className="d-block w-100"
              src={item}
              alt="First slide"
            />
          </div>
        ))}
      </div>
      <a
        className="carousel-control-prev"
        href={`#${id}`}
        role="button"
        data-slide="prev"
      >
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="sr-only">Previous</span>
      </a>
      <a
        className="carousel-control-next"
        href={`#${id}`}
        role="button"
        data-slide="next"
      >
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="sr-only">Next</span>
      </a>
    </div>
  )
}

export default SliderPhotos
