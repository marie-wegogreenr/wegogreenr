import React from 'react'
import { useAsyncDebounce } from 'react-table'

export default function GlobalFilter({
  preGlobalFilteredRows,
  globalFilter,
  setGlobalFilter,
  placeholder
}) {
  const count = preGlobalFilteredRows.length
  const [value, setValue] = React.useState(globalFilter)
  const onChange = useAsyncDebounce((value) => {
    setGlobalFilter(value || undefined)
  }, 200)

  return (
    <span>
      recherche:
      <input
        value={value || ''}
        className="form-control"
        onChange={(e) => {
          setValue(e.target.value)
          onChange(e.target.value)
        }}
        placeholder={placeholder}
        style={{
          fontSize: '1.1rem',
          border: '0'
        }}
      />
    </span>
  )
}
