import { getStatusType } from 'helpers/utilsHelper'
import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import {
  useTable,
  useFilters,
  useGlobalFilter,
  useRowSelect
} from 'react-table'
import GlobalFilter from '../GlobalFilter'

export default function AbracadaroomTable({ data, setselectedRows }) {
  const router = useRouter()
  const columns = React.useMemo(
    () => [
      {
        Header: 'id',
        accessor: 'id'
      },
      {
        Header: 'Nom Public',
        accessor: 'public_name',
        Cell: (cell) => (
          <a href={cell.row.original.origin_url} target="_blank">
            {cell.value}
          </a>
        )
      },
      {
        Header: 'Activé',
        accessor: 'active',
        Cell: (cell) => (
          <span>{cell.row.original.active === 1 ? 'Oui' : 'Non'}</span>
        )
      },
      {
        Header: 'Statut',
        accessor: 'status',
        Cell: (cell) => <span>{getStatusType(cell.value)}</span>
      },
      {
        Header: 'se distinguer',
        accessor: 'outstanding',
        Cell: (cell) => <span>{cell.value === 1 ? 'oui' : 'non'}</span>
      },
      {
        Header: 'Publié Abracadaroom',
        accessor: 'origin_published',
        Cell: (cell) => <span>{cell.value === 1 ? 'oui' : 'non'}</span>
      },
      {
        Header: 'Prix de base €',
        accessor: 'basic_price',
        Cell: (cell) => <span>{cell.value.toFixed(2)}</span>
      },
      {
        Header: 'actions',
        Cell: (cell) => (
          <span
            className="admin-action-button"
            onClick={() => {
              router.push(`abracadaroom/${cell.row.original.id}`)
            }}
          >
            voir plus
          </span>
        )
      }
    ],
    []
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    selectedFlatRows,
    state: { selectedRowIds }
  } = useTable(
    {
      columns,
      data
    },
    useRowSelect,
    (hooks) => {
      hooks.visibleColumns.push((columns) => [
        {
          id: 'selection',
          Header: ({ getToggleAllRowsSelectedProps }) => (
            <div>
              <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
            </div>
          ),
          Cell: ({ row }) => (
            <div>
              <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
            </div>
          )
        },
        ...columns
      ])
    }
  )

  useEffect(() => {
    setselectedRows(selectedFlatRows.map((sr) => sr.original))
  }, [selectedFlatRows, selectedRowIds])

  // const firstPageRows = rows.slice(0, 10)
  return (
    <>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>{column.render('Header')}</th>
              ))}
            </tr>
          ))}
        </thead>
        {data.length ? (
          <tbody {...getTableBodyProps()}>
            {rows.map((row, i) => {
              prepareRow(row)
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                    )
                  })}
                </tr>
              )
            })}
          </tbody>
        ) : (
          <tbody>
            <tr>
              <td className="admin-userTable__empty-result" colSpan="7">
                Il n'y a pas de résultats
              </td>
            </tr>
          </tbody>
        )}
      </table>
    </>
  )
}

const IndeterminateCheckbox = React.forwardRef(
  ({ indeterminate, ...rest }, ref) => {
    const defaultRef = React.useRef()
    const resolvedRef = ref || defaultRef

    React.useEffect(() => {
      resolvedRef.current.indeterminate = indeterminate
    }, [resolvedRef, indeterminate])

    return (
      <>
        <input type="checkbox" ref={resolvedRef} {...rest} />
      </>
    )
  }
)
