import React from 'react'
import { useTable, useFilters, useGlobalFilter, useSortBy } from 'react-table'
import { useRouter } from 'next/router'
import GlobalFilter from '../GlobalFilter'
import { useDispatch } from 'react-redux'
import { setUserToEdit } from '@ducks/AdminUser/actions'
import { format } from 'date-fns'

export default function UsersTable({ data }) {
  const dispatch = useDispatch()
  const router = useRouter()

  /**
   * @function : To open new tab for specific room
   */
  const openNewTabRoom = (userData) => {
    dispatch(setUserToEdit(userData))

    if (typeof window !== undefined) {
      window.open(`${window.location.origin}/admin/user/${userData.id}`)
    }
  }

  const columns = React.useMemo(
    () => [
      {
        Header: 'id',
        accessor: 'id'
      },
      {
        Header: 'Prénom',
        accessor: 'name'
      },
      {
        Header: 'Surnom',
        accessor: 'lastname'
      },
      {
        Header: 'Email',
        accessor: 'email'
      },
      {
        Header: 'Role(s)',
        accessor: 'roles',
        Cell: (cell) =>
          cell.value.map((role) => (
            <span style={{ display: 'block' }}>{role.name}</span>
          ))
      },
      {
        Header: 'actions',
        // accessor: 'id',
        Cell: (cell) => (
          <span
            className="admin-action-button"
            onClick={() => {
              openNewTabRoom(cell.row.original)
            }}
          >
            Voir plus
          </span>
        )
      },
      {
        Header: `Date d'inscription`,
        accessor: 'created_at',
        sortType: 'datetime',
        Cell: ({ value }) => {
          if (value) {
            return format(new Date(value), 'dd/MM/yyyy')
          } else {
            return '-'
          }
        }
      }
    ],
    []
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state,
    preGlobalFilteredRows,
    setGlobalFilter
  } = useTable(
    {
      columns,
      data
    },
    useFilters, // useFilters!
    useGlobalFilter, // useGlobalFilter!
    useSortBy
  )

  const firstPageRows = rows
  return (
    <>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                  {column.render('Header')}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        {data.length ? (
          <tbody {...getTableBodyProps()}>
            {firstPageRows.map((row, i) => {
              prepareRow(row)
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                    )
                  })}
                </tr>
              )
            })}
          </tbody>
        ) : (
          <tbody>
            <tr>
              <td className="admin-userTable__empty-result" colSpan="9">
                Il n'y a pas de résultats
              </td>
            </tr>
          </tbody>
        )}
      </table>
    </>
  )
}
