import React from 'react'
import Link from 'next/link'
import {
  useTable,
  useFilters,
  useGlobalFilter,
  useAsyncDebounce
} from 'react-table'
import GlobalFilter from '../GlobalFilter'
import { useRouter } from 'next/router'

export default function EtablissementTable({ data }) {
  const router = useRouter()
  const columns = React.useMemo(
    () => [
      {
        Header: 'id',
        accessor: 'id'
      },
      {
        Header: 'Établissement',
        accessor: 'name'
      },
      {
        Header: 'Prénom',
        accessor: 'contact_name'
      },
      {
        Header: 'Nom',
        accessor: 'contact_lastname'
      },
      {
        Header: 'Adresse',
        accessor: 'address'
      },
      {
        Header: 'Téléphone',
        accessor: 'contact_phone'
      },
      {
        Header: 'Email',
        accessor: 'contact_email'
      },
      {
        Header: 'Origin',
        accessor: 'origin'
      },
      {
        Header: 'Publié',
        accessor: 'active'
      },
      {
        Header: 'actions',
        Cell: (cell) => (
          <Link href={`etablissement/${cell.row.original.id}`}>
            <a className="admin-action-button" target="_blank">
              voir plus
            </a>
          </Link>
        )
      }
    ],
    []
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state,
    preGlobalFilteredRows,
    setGlobalFilter
  } = useTable(
    {
      columns,
      data
    },
    useFilters, // useFilters!
    useGlobalFilter // useGlobalFilter!
  )

  const optionsOrigin = [
    { text: 'Test1', value: 0 },
    { text: 'WGG', value: 1 },
    { text: 'Abracadaroom', value: 2 },
    { text: 'Test2', value: 3 },
    { text: 'Alliance Reseaux', value: 4 }
  ]

  const selectOption = (value) => {
    let _value

    switch (value) {
      case 0:
        _value = 'Test1'
        break
      case 1:
        _value = 'WGG'
        break
      case 2:
        _value = 'Abracadaroom'
        break
      case 3:
        _value = 'Test2'
        break
      case 4:
        _value = 'Alliance Reseaux'
        break
      default:
        break
    }

    return _value
  }

  return (
    <>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>{column.render('Header')}</th>
              ))}
            </tr>
          ))}
        </thead>
        {data.length ? (
          <tbody {...getTableBodyProps()}>
            {rows.map((row, i) => {
              prepareRow(row)
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    if (cell.column.id === 'active') {
                      return <td>{cell.value ? 'Oui' : 'Non'}</td>
                    } else if (cell.column.id === 'origin') {
                      return <td>{selectOption(cell.value)}</td>
                    } else {
                      return (
                        <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                      )
                    }
                  })}
                </tr>
              )
            })}
          </tbody>
        ) : (
          <tbody>
            <tr>
              <td className="admin-userTable__empty-result" colSpan="9">
                Il n'y a pas de résultats
              </td>
            </tr>
          </tbody>
        )}
      </table>
    </>
  )
}
