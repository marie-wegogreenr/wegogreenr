import React, { useState, useEffect } from 'react'
import { useTable, useFilters, useGlobalFilter } from 'react-table'
import moment from 'moment'
import GlobalFilter from '../GlobalFilter'
import noImage from 'public/images/not-image.svg'
import { getStatusMessage } from 'helpers/utilsHelper'
import { setReservationStatus } from 'services/hebergementsService'
import { useToast } from 'hooks/useToast'
import { useDispatch } from 'react-redux'
import {
  close_reservation_action,
  select_reservation_action
} from '@ducks/adminReservation/actions'

export default function ReservationTable({ data, setData }) {
  const [notify] = useToast()
  const dispatch = useDispatch()

  const cancelReservations = async (id, reservations) => {
    const resp = await setReservationStatus({
      accept: '0',
      reservation_id: id
    })

    const newStatus = reservations.map((d) => {
      return d.id !== id ? d : { ...d, status: 5 }
    })

    setData(newStatus)
    notify('réservation annulée', 'success')
  }

  const handleReservationClick = (cell) => {
    dispatch(close_reservation_action())
    dispatch(select_reservation_action(cell.row.original))
    if (typeof window !== undefined) {
      window.open(
        `${window.location.origin}/admin/reservations/${cell.row.original.id}`
      )
    }
  }

  const columns = React.useMemo(
    () => [
      {
        Header: `Id`,
        accessor: 'code'
      },
      {
        Header: `Nom Public`,
        accessor: 'room',
        Cell: (cell) => <span>{cell.cell.value.public_name}</span>
      },
      {
        Header: `Email du Client`,
        accessor: 'user',
        Cell: (cell) => <span>{cell.value.email}</span>
      },
      {
        Header: `Date d'Arrivée`,
        accessor: 'arrival_date',
        Cell: (cell) => <span>{moment(cell.cell.value).format('L')}</span>
      },
      {
        Header: 'Date de Départ',
        accessor: 'departure_date',
        Cell: (cell) => <span>{moment(cell.cell.value).format('L')}</span>
      },
      {
        Header: 'Price €',
        accessor: 'room_price',
        Cell: (cell) => {
          return <span>{cell.value?.toFixed(2)}</span>
        }
      },
      {
        Header: 'Statut',
        accessor: 'status',
        Cell: (cell) => <span>{getStatusMessage(cell.value)}</span>
      },
      {
        Header: 'Créé',
        accessor: 'created_at',
        Cell: (cell) => <span>{moment(cell.cell.value).format('L')}</span>
      },
      {
        Header: 'actions',
        // accessor: 'id',
        Cell: (cell) => (
          <span
            className="admin-action-button"
            onClick={() => {
              handleReservationClick(cell)
            }}
          >
            Voir plus
          </span>
        )
      }
      /*{
        Header: 'Actions',
        Cell: (cell) => {
          const status = cell.row.original.status
          if (status === 0 || status === 1 || status === 3) {
            return (
              <span
                className="admin-action-button"
                onClick={() => {
                  cancelReservations(cell.row.original.id, cell.data)
                }}
              >
                annuler
              </span>
            )
          } else {
            return <span></span>
          }
        }
      }*/
    ],
    []
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state,
    preGlobalFilteredRows,
    setGlobalFilter
  } = useTable(
    {
      columns,
      data
    },
    useFilters, // useFilters!
    useGlobalFilter // useGlobalFilter!
  )

  const firstPageRows = rows
  return (
    <>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>{column.render('Header')}</th>
              ))}
            </tr>
          ))}
        </thead>
        {data.length ? (
          <tbody {...getTableBodyProps()}>
            {firstPageRows.map((row, i) => {
              prepareRow(row)
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                    )
                  })}
                </tr>
              )
            })}
          </tbody>
        ) : (
          <tbody>
            <tr>
              <td className="admin-userTable__empty-result" colSpan="8">
                Il n'y a pas de résultats
              </td>
            </tr>
          </tbody>
        )}
      </table>
    </>
  )
}
