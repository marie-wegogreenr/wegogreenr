import React from 'react'
import Link from 'next/link'
import {
  useTable,
  useFilters,
  useGlobalFilter,
  useRowSelect
} from 'react-table'
import { useRouter } from 'next/router'
import moment from 'moment'

export default function HebergementTable({ data, setselectedRows }) {
  const router = useRouter()
  const columns = React.useMemo(
    () => [
      {
        Header: 'Id',
        accessor: 'id'
      },
      {
        Header: 'Nom public',
        accessor: 'public_name'
      },
      {
        Header: 'Établissement',
        Cell: (cell) => {
          return (
            <span>
              <a href={`etablissement/${cell.row.original.establishment.id}`}>
                {cell.row.original.establishment.name}
              </a>
            </span>
          )
        }
      },
      {
        Header: 'GreenScore',
        accessor: 'establishment',
        Cell: (cell) => {
          return (
            <span style={{ display: 'block', textAlign: 'center' }}>
              {cell.value.green_score
                ? `${cell.value.green_score.level}/5`
                : ''}
            </span>
          )
        }
      },

      {
        Header: 'Activé',
        accessor: 'active',
        Cell: (cell) => (
          <span>
            {cell.row.original.active === 1 ? 'publiee' : 'non publiee'}
          </span>
        )
      },
      {
        Header: 'Se distinguer',
        accessor: 'outstanding',
        Cell: (cell) => <span>{cell.value === 1 ? 'oui' : 'non'}</span>
      },
      {
        Header: 'Prix €',
        accessor: 'basic_price',
        Cell: (cell) => {
          return <span>{cell.value ? cell.value.toFixed(2) : ''}</span>
        }
      },
      {
        Header: `Date d'inscription`,
        accessor: 'created_at',
        Cell: (cell) => {
          return <span>{moment(cell.value).format('L')}</span>
        }
      },
      {
        Header: 'Actions',
        Cell: (cell) => (
          <Link href={`hebergements/${cell.row.original.id}`}>
            <a className="admin-action-button" target="_blank">
              voir plus
            </a>
          </Link>
        )
      }
    ],
    []
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    selectedFlatRows,
    state: { selectedRowIds }
  } = useTable(
    {
      columns,
      data
    },
    useRowSelect,
    (hooks) => {
      hooks.visibleColumns.push((columns) => [
        {
          id: 'selection',
          Header: ({ getToggleAllRowsSelectedProps }) => (
            <div>
              <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
            </div>
          ),
          Cell: ({ row }) => (
            <div>
              <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
            </div>
          )
        },
        ...columns
      ])
    }
  )

  React.useEffect(() => {
    setselectedRows(selectedFlatRows.map((sr) => sr.original))
  }, [selectedFlatRows, selectedRowIds])

  return (
    <>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>{column.render('Header')}</th>
              ))}
            </tr>
          ))}
        </thead>
        {data.length ? (
          <tbody {...getTableBodyProps()}>
            {rows.map((row, i) => {
              prepareRow(row)
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                    )
                  })}
                </tr>
              )
            })}
          </tbody>
        ) : (
          <tbody>
            <tr>
              <td className="admin-userTable__empty-result" colSpan="10">
                Il n'y a pas de résultats
              </td>
            </tr>
          </tbody>
        )}
      </table>
    </>
  )
}

const IndeterminateCheckbox = React.forwardRef(
  ({ indeterminate, ...rest }, ref) => {
    const defaultRef = React.useRef()
    const resolvedRef = ref || defaultRef

    React.useEffect(() => {
      resolvedRef.current.indeterminate = indeterminate
    }, [resolvedRef, indeterminate])

    return (
      <>
        <input type="checkbox" ref={resolvedRef} {...rest} />
      </>
    )
  }
)
