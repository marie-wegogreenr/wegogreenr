import React from 'react'
import { ChevronLeft, ChevronRight } from 'components/Icons'

export default function Pagination({ paginationInfo, handlePagination }) {
  const handleArrows = (direnction, pag) => {
    if (direnction === 'prev' && pag >= 1) {
      handlePagination(pag)
    } else if (direnction === 'next' && pag <= paginationInfo.last_page) {
      handlePagination(pag)
    }
  }
  return (
    <div className="pagination">
      <button
        className="pagination-item"
        onClick={() => {
          handleArrows('prev', paginationInfo.current_page - 1)
        }}
      >
        <ChevronLeft width="20" height="20" strokeWidth={1} />
      </button>
      {paginationInfo.current_page > 2 && paginationInfo.last_page > 2 && (
        <>
          <button
            className={`pagination-item ${
              paginationInfo.current_page === 1 ? 'is-active' : ''
            } `}
            onClick={() => {
              handlePagination(1)
            }}
          >
            1
          </button>
          <button className="pagination-item">...</button>
        </>
      )}

      <RenderPags
        paginationInfo={paginationInfo}
        handlePagination={handlePagination}
      />
      {paginationInfo.last_page - paginationInfo.current_page > 1 && (
        <>
          <button className="pagination-item">...</button>
          <button
            className={`pagination-item ${
              paginationInfo.current_page === paginationInfo.last_page
                ? 'is-active'
                : ''
            } `}
            onClick={() => {
              handlePagination(paginationInfo.last_page)
            }}
          >
            {paginationInfo.last_page}
          </button>
        </>
      )}
      <button
        className="pagination-item"
        onClick={() => {
          handleArrows('next', paginationInfo.current_page + 1)
        }}
      >
        <ChevronRight width="20" height="20" strokeWidth={1} />
      </button>
    </div>
  )
}

const RenderPags = ({ paginationInfo, handlePagination }) => {
  const { current_page, last_page } = paginationInfo
  let paginationArray = [
    <button className="pagination-item is-active">{current_page}</button>
  ]

  if (current_page > 1) {
    paginationArray = [
      <button
        className="pagination-item"
        onClick={() => {
          handlePagination(current_page - 1)
        }}
      >
        {current_page - 1}
      </button>,
      ...paginationArray
    ]
  }
  if (current_page !== last_page) {
    paginationArray = [
      ...paginationArray,
      <button
        className="pagination-item"
        onClick={() => {
          handlePagination(current_page + 1)
        }}
      >
        {current_page + 1}
      </button>
    ]
  }

  return paginationArray
}
