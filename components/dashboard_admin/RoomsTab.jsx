import { SectionLayout } from '@components/host/establishment'
import { useEffect, useState } from 'react'
import { getRooms } from 'services/etablissementService'

export default function RoomsTab({ id_etablisement }) {
  const [roomsData, setRoomsData] = useState([])

  useEffect(async () => {
    const rooms = await getRooms(id_etablisement)
    setRoomsData(rooms)
  }, [])

  return (
    <SectionLayout title="Hébergements" hideModifyButton={true}>
      <ul>
        {roomsData.map((room, index) => (
          <li key={`room-list-${index}`}>
            <a href={`../hebergements/${room.id}`}>
              {room.public_name
                ? room.public_name
                : room.internal_name
                ? room.internal_name
                : `Hébergement - ${index + 1}`}
            </a>
          </li>
        ))}
      </ul>
    </SectionLayout>
  )
}
