/*  React  */
import { useState } from 'react'
/*  Redux  */
import { setClearLoading } from '@ducks/Config/actions'
import { useDispatch } from 'react-redux'
/*  Components  */
import SelectFilter from './SelectFilter'
/*  Constants */
import { optionsStablishments } from 'constants/filterTypes'
import InputFilter from './InputFilter'

export default function RoomsFilter({
  setRooms,
  pag,
  setPag,
  rooms,
  findFunction,
  query,
  setQuery
}) {
  /*  Constants  */
  const initialFilterValues = {
    active: '',
    greenscore: '',
    q: ''
  }
  /*  Local states  */
  const [filterValues, setFilterValues] = useState({ ...initialFilterValues })
  /*  Global scope */
  const dispatch = useDispatch()
  /*  Destructure constant for select */
  const { active, greenscore, origin } = optionsStablishments
  /*  Handle functions  */
  const handleSearchClick = async () => {
    dispatch(setClearLoading(true))
    setQuery({ ...query, ...filterValues })
    const { data, ...pagination } = await findFunction(pag, {
      ...query,
      ...filterValues
    })
    setRooms(data)
    setPag(pagination)
    dispatch(setClearLoading(false))
  }

  const resetSearch = async () => {
    dispatch(setClearLoading(true))
    const { data, ...pagination } = await findFunction(1, {
      ...initialFilterValues
    })
    setRooms(data)
    setPag(pagination)
    dispatch(setClearLoading(false))
  }

  const handleResetClick = () => {
    setFilterValues(initialFilterValues)
    setQuery({ ...initialFilterValues })
    resetSearch()
  }

  return (
    <div className="admin-userTable__search-bar">
      <div className="admin-userTable__search-inputs">
        <SelectFilter
          options={active}
          setFilterValues={setFilterValues}
          filterValues={filterValues}
        />
        <SelectFilter
          options={greenscore}
          setFilterValues={setFilterValues}
          filterValues={filterValues}
        />
        <InputFilter
          id="q"
          label="Recherche"
          setFilterValues={setFilterValues}
          filterValues={filterValues}
        />
      </div>
      <div className="admin-userTable__search-buttons">
        <button
          className="btn btn-secondary admin-userTable__search-btn"
          onClick={handleSearchClick}
        >
          Appliquer
        </button>
        <button
          className="btn btn-secondary admin-userTable__search-btn"
          onClick={handleResetClick}
        >
          Réinitialiser
        </button>
      </div>
    </div>
  )
}
