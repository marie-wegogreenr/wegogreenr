export default function InputFilter({
  filterValues,
  setFilterValues,
  id,
  label
}) {
  const handleChangeValue = (e) => {
    setFilterValues({
      ...filterValues,
      [id]: e.target.value
    })
  }

  return (
    <div className="admin-userTable__search-txtinput">
      <label htmlFor={`input-${id}`}>{label}</label>
      <input
        id={`input-${id}`}
        className="form-control border rounded-2 px-4 select-height"
        type="text"
        onChange={handleChangeValue}
        value={filterValues[id]}
      />
    </div>
  )
}
