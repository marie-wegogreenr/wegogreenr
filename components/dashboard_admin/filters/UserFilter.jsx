/*  React  */
import { useState } from 'react'
/*  Redux  */
import { setClearLoading } from '@ducks/Config/actions'
import { useDispatch } from 'react-redux'
/*  Components  */
import SelectFilter from './SelectFilter'
/*  Constants */
import { optionsRols } from 'constants/filterTypes'
import InputFilter from './InputFilter'

export default function UserFilter({
  setUsers,
  pag,
  setPag,
  users,
  findFunction,
  query,
  setQuery
}) {
  /*  Constants  */
  const initialFilterValues = {
    role: '',
    q: ''
  }
  /*  Local states  */
  const [filterValues, setFilterValues] = useState({ ...initialFilterValues })
  /*  Global scope */
  const dispatch = useDispatch()
  /*  Handle functions  */
  const handleSearchClick = async () => {
    dispatch(setClearLoading(true))
    setQuery({ ...query, ...filterValues })
    const { data, ...pagination } = await findFunction(pag, {
      ...query,
      ...filterValues
    })
    setUsers(data)
    setPag(pagination)
    dispatch(setClearLoading(false))
  }

  const resetSearch = async () => {
    dispatch(setClearLoading(true))
    const { data, ...pagination } = await findFunction(1, {
      ...initialFilterValues
    })
    setUsers(data)
    setPag(pagination)
    dispatch(setClearLoading(false))
  }

  const handleResetClick = () => {
    setFilterValues(initialFilterValues)
    setQuery({
      ...initialFilterValues
    })
    resetSearch()
  }

  return (
    <div className="admin-userTable__search-bar">
      <div className="admin-userTable__search-inputs">
        <SelectFilter
          options={optionsRols}
          setFilterValues={setFilterValues}
          filterValues={filterValues}
        />
        <InputFilter
          id="q"
          label="Recherche"
          setFilterValues={setFilterValues}
          filterValues={filterValues}
        />
      </div>
      <div className="admin-userTable__search-buttons">
        <button
          className="btn btn-secondary admin-userTable__search-btn"
          onClick={handleSearchClick}
        >
          Appliquer
        </button>
        <button
          className="btn btn-secondary admin-userTable__search-btn"
          onClick={handleResetClick}
        >
          Réinitialiser
        </button>
      </div>
    </div>
  )
}
