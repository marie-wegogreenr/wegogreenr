export default function SelectFilter({
  options,
  setFilterValues,
  filterValues
}) {
  const { id, label, data } = options

  const handleChange = (e) => {
    setFilterValues({
      ...filterValues,
      [id]: e.target.value
    })
  }

  return (
    <div className="admin-userTable__search-select">
      <label htmlFor={`${id}-select`}>{label}</label>
      <select
        id={`${id}-select`}
        type="text"
        className="form-control border rounded-2 px-4 select-height"
        onChange={handleChange}
      >
        {data.map((option) => (
          <option
            value={option.value}
            selected={option.value === filterValues[id]}
          >
            {option.text}
          </option>
        ))}
      </select>
    </div>
  )
}
