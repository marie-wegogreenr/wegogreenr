import React, { useState, useEffect } from 'react'
import checkIcon from 'public/images/check.png'
import Image from 'next/image'
//redux
import { useDispatch, useSelector } from 'react-redux'
import {
  logUserAction,
  updateUserInfoAction,
  unAuthUserAction
} from '@ducks/userDuck'
import { useToast } from 'hooks/useToast'

const PassWordInput = ({ onUpdateInfo, idUser, userData }) => {
  const [notify] = useToast()
  const dispatch = useDispatch()
  const user = useSelector((store) => store.user.user)
  const isAuth = useSelector((store) => store.user.auth)

  const { email, name } = user

  const [isEditing, setIsEditing] = useState(false)
  const [form, setForm] = useState({
    password: '',
    passwordConfirm: '',
    old_password: ''
  })

  let passwordStength = 'Weak'

  let passwordDisabled = true

  useEffect(() => {
    if (isAuth) {
      dispatch(updateUserInfoAction('password', form.password))
      notify(`Modifications sauvegardées`, 'success')
      dispatch(unAuthUserAction())
    }
  }, [isAuth])

  function PassworLength() {
    return form.password && form.password.length > 8
  }

  function emailNameCheck() {
    const regex = `(${email})|(${name})`
    return RegExp(regex).test(form.password)
  }

  function PasswordEqual() {
    return form.password === form.passwordConfirm
  }

  function PasswordNumberSymbol() {
    const regex = /[$-/:-?{-~!"^_@`\[\]]/
    return RegExp(regex).test(form.password)
  }

  function checkPasswordStength() {
    let length = PassworLength()
    let symbol = PasswordNumberSymbol()
    let emailAndName = emailNameCheck()

    length || symbol || !emailAndName ? (passwordStength = 'Weak') : ''
    ;(length || symbol) && (symbol || !emailAndName)
      ? (passwordStength = 'Medium')
      : ''
    length && symbol && !emailAndName
      ? ((passwordStength = 'Strong'), (passwordDisabled = false))
      : ''
  }
  checkPasswordStength()

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  const handleSubmit = () => {
    if (PasswordEqual()) {
      dispatch(
        logUserAction({
          newPassword: form.password,
          password: form.old_password
        })
      )
      dispatch(
        updateUserInfoAction('password', form.password, idUser, true, userData)
      )
      setIsEditing(false)
      onUpdateInfo()
    } else notify('les mots de passe ne sont pas égaux', 'error')
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'}`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <h3>Mot de passe</h3>
            <p>*******</p>
          </div>
          <a onClick={() => setIsEditing(true)} href="#">
            Modifier
          </a>
        </div>
      </div>

      {isEditing && (
        <>
          <div className="d-flex justify-content-between">
            <h2>Mot de passe</h2>
            <a href="#" onClick={() => setIsEditing(false)}>
              Annuler
            </a>
          </div>
          <div className="d-flex">
            <div className="">
              <h3>Actual Mot de passe</h3>
              <input
                type="text"
                name="old_password"
                value={form.old_password}
                placeholder="votre mot de passe"
                onChange={handleChange}
              />
            </div>
          </div>
          <div className="d-flex">
            <div>
              <h3>Mot de passe</h3>
              <input
                type="text"
                name="password"
                value={form.name}
                placeholder="Saisissez votre mot de passe"
                onChange={handleChange}
              />
            </div>
            <div>
              <h3>Confirmez le mot de passe</h3>
              <input
                type="text"
                name="passwordConfirm"
                value={form.lastname}
                placeholder="Répétez votre mot de passe"
                onChange={handleChange}
              />
            </div>
          </div>
          <div>
            <h3>Fiabilité du mot de passe : {passwordStength}</h3>
            <div className="d-flex">
              {!emailNameCheck() && (
                <span>
                  <Image src={checkIcon} width={20} height={20} />
                </span>
              )}
              <p>Ne doit pas contenir votre nom ni votre adresse e-mail</p>
            </div>
            <div className="d-flex">
              {PassworLength() && (
                <span>
                  <Image src={checkIcon} width={20} height={20} />
                </span>
              )}
              <p>Au moins 8 caractères</p>
            </div>
            <div className="d-flex">
              {PasswordNumberSymbol() && (
                <span>
                  <Image src={checkIcon} width={20} height={20} />
                </span>
              )}
              <p>Contient un chiffre ou un symbole</p>
            </div>
          </div>
          <button
            onClick={handleSubmit}
            className="btn btn-primary"
            disabled={passwordDisabled}
          >
            Sauvegarder
          </button>
        </>
      )}
    </>
  )
}

export default PassWordInput
