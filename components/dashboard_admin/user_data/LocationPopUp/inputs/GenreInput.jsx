import React, { useState, useEffect } from 'react'
import { consoleLog } from '/utils/logConsole'
import genres from './../../../../../constants/genres'
//REDUX
import { updateUserInfoAction, getUserDataAction } from '@ducks/userDuck'
import { useDispatch } from 'react-redux'
import { useToast } from 'hooks/useToast'

const GenreInput = ({ genre, onUpdateInfo, idUser, email }) => {
  const dispatch = useDispatch()
  const [notify] = useToast()
  const [isEditing, setIsEditing] = useState(false)

  const [form, setForm] = useState({})

  useEffect(() => {
    setForm({
      email,
      genre
    })
  }, [])

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  const handleSubmit = () => {
    dispatch(updateUserInfoAction('gender', form.genre, idUser))
    dispatch(updateUserInfoAction('email', `${form.email}`, idUser))
    notify(`Modifications sauvegardées`, 'success')
    dispatch(getUserDataAction())
    setTimeout(() => {
      onUpdateInfo()
    }, 1000)
    setIsEditing(false)
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'}`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <h3>Sexe</h3>
            <p>{genre}</p>
          </div>
          <a onClick={() => setIsEditing(true)} href="#!">
            Modifier
          </a>
        </div>
      </div>

      {isEditing && (
        <>
          <div className="d-flex justify-content-between">
            <h3>Sexe</h3>
            <a href="#!" onClick={() => setIsEditing(false)}>
              Annuler
            </a>
          </div>
          <div className="d-flex">
            <div>
              <select name="genre" onChange={handleChange}>
                <option value={form.genre}>{form.genre}</option>
                {genres.map((item, index) => (
                  <option onChange={handleChange} value={item} key={index}>
                    {item}
                  </option>
                ))}
              </select>
            </div>
          </div>
          <button onClick={handleSubmit} className="btn btn-primary">
            Sauvegarder
          </button>
        </>
      )}
    </>
  )
}

export default GenreInput
