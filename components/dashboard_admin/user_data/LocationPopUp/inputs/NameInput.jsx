import React, { useEffect, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
//REDUX
import { useDispatch } from 'react-redux'
import { updateUserInfoAction } from '@ducks/userDuck'
import { useToast } from 'hooks/useToast'

const NameInput = ({ name, lastname, email, onUpdateInfo, idUser }) => {
  const dispatch = useDispatch()
  const [notify] = useToast()
  const [isEditing, setIsEditing] = useState(false)

  const [form, setForm] = useState({})

  useEffect(() => {
    setForm({
      name,
      lastname,
      email
    })
  }, [name])

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  const handleSubmit = () => {
    dispatch(updateUserInfoAction('name', `${form.name}`, idUser))
    dispatch(updateUserInfoAction('lastname', `${form.lastname}`, idUser))
    dispatch(updateUserInfoAction('email', `${form.email}`, idUser))
    notify(`Modifications sauvegardées`, 'success')
    setTimeout(() => {
      onUpdateInfo()
    }, 1000)
    setIsEditing(false)
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'}`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <h2>Nom</h2>
            <p>
              {name} {lastname}
            </p>
          </div>
          <a onClick={() => setIsEditing(true)} href="#!">
            Modifier
          </a>
        </div>
      </div>
      {isEditing && name != undefined && (
        <>
          <div className="d-flex justify-content-between">
            <h2>Nom</h2>
            <a href="#!" onClick={() => setIsEditing(false)}>
              Annuler
            </a>
          </div>
          <div className="d-flex">
            <div>
              <h2>Nom</h2>
              <input
                type="text"
                name="name"
                value={form.name}
                onChange={handleChange}
              />
            </div>
            <div>
              <h2>Prénom</h2>
              <input
                type="text"
                name="lastname"
                value={form.lastname}
                onChange={handleChange}
              />
            </div>
          </div>
          <button onClick={handleSubmit} className="btn btn-primary">
            Sauvegarder
          </button>
        </>
      )}
    </>
  )
}

export default NameInput
