import React, { useEffect, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
//REDUX
import { useDispatch } from 'react-redux'
import { updateUserInfoAction } from '@ducks/userDuck'
import { useToast } from 'hooks/useToast'

const TextInput = ({
  label,
  value = '',
  formName,
  idUser,
  onUpdateInfo,
  email,
  type = 'text'
}) => {
  const dispatch = useDispatch()
  const [notify] = useToast()
  const [isEditing, setIsEditing] = useState(false)

  const [form, setForm] = useState({})

  useEffect(() => {
    setForm({
      [formName]: value,
      email
    })
  }, [value])

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }
  const handleSubmit = () => {
    dispatch(updateUserInfoAction(formName, form[formName], idUser))
    dispatch(updateUserInfoAction('email', `${form.email}`, idUser))
    notify(`Modifications sauvegardées`, 'success')
    setTimeout(() => {
      onUpdateInfo()
    }, 1000)
    setIsEditing(false)
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'}`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <h3>{label}</h3>
            <p>{value}</p>
          </div>
          <a onClick={() => setIsEditing(true)} href="#!">
            Modifier
          </a>
        </div>
      </div>

      {isEditing && (
        <>
          <div className="d-flex justify-content-between">
            <h2>{label}</h2>
            <a href="#!" onClick={() => setIsEditing(false)}>
              Annuler
            </a>
          </div>
          <div className="d-flex">
            <div>
              <input
                type={type}
                name={formName}
                value={form[formName]}
                onChange={handleChange}
              />
            </div>
          </div>
          <button onClick={handleSubmit} className="btn btn-primary">
            Sauvegarder
          </button>
        </>
      )}
    </>
  )
}

export default TextInput
