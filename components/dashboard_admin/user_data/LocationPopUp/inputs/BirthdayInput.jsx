import React, { useEffect, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
//REDUX
import { updateUserInfoAction } from '@ducks/userDuck'
import { useDispatch } from 'react-redux'
import moment from 'moment'
import { useToast } from 'hooks/useToast'
const BirthdayInput = ({
  birthday = '2020-02-01',
  onUpdateInfo,
  idUser,
  email
}) => {
  const dispatch = useDispatch()
  const [notify] = useToast()
  const [isEditing, setIsEditing] = useState(false)
  const [form, setForm] = useState({})

  useEffect(() => {
    setForm({
      birthday,
      email
    })
  }, [birthday])

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }
  const handleSubmit = () => {
    dispatch(updateUserInfoAction('birthday', form.birthday, idUser))
    dispatch(updateUserInfoAction('email', `${form.email}`, idUser))
    notify(`Modifications sauvegardées`, 'success')
    setTimeout(() => {
      onUpdateInfo()
    }, 1000)
    setIsEditing(false)
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'}`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <h3>Date de naissance</h3>
            <p> {moment(form.birthday).format('dddd, Do MMMM  YYYY')} </p>
          </div>
          <a onClick={() => setIsEditing(true)} href="#!">
            Modifier
          </a>
        </div>
      </div>

      {isEditing && birthday != undefined && (
        <>
          <div className="d-flex justify-content-between">
            <h3>Date de naissance</h3>
            <a href="#!" onClick={() => setIsEditing(false)}>
              Annuler
            </a>
          </div>
          <div className="d-flex">
            <div>
              <input
                type="date"
                name="birthday"
                value={form.birthday}
                onChange={handleChange}
              />
            </div>
          </div>
          <button onClick={handleSubmit} className="btn btn-primary">
            Sauvegarder
          </button>
        </>
      )}
    </>
  )
}

export default BirthdayInput
