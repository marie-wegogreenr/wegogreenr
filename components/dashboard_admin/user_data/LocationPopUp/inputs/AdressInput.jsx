import React, { useEffect, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
import countries from 'constants/countries'
//redux
import { useDispatch } from 'react-redux'
import { updateUserInfoAction } from '@ducks/userDuck'

import Cookies from 'universal-cookie'

import SearchAlgoia from '@components/Algolia/SearchAlgolia'
import { useToast } from 'hooks/useToast'

const AdressInput = ({
  name,
  email,
  country,
  address = '',
  address2 = '',
  extraAdress,
  postalCode = '',
  city,
  country_id,
  idUser,
  onUpdateInfo
}) => {
  const dispatch = useDispatch()
  const [notify] = useToast()
  const [isEditing, setIsEditing] = useState(false)

  const [form, setForm] = useState({})

  useEffect(async () => {
    const cookies = new Cookies()
    //Get countries
    const res_countrie = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'countries',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_countries = await res_countrie.json()
    setForm({
      name,
      email,
      country,
      address,
      address2,
      extraAdress,
      postalCode,
      city,
      country_id,
      countries: json_countries,
      idUser
    })
  }, [country, address, extraAdress, postalCode, city])

  //consoleLog(form)

  const handleChange = (e) => {
    //consoleLog('hanlde')
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  const onSelectOption = (selected_value) => {
    //consoleLog(selected_value)
    setForm({
      ...form,
      city: selected_value.name
    })
  }

  const handleChangeVille = (event) => {
    const { value } = event.target

    if (typeof value === 'string') {
      setForm({ ...form, city: value })
    }
  }

  const handleSubmit = () => {
    dispatch(updateUserInfoAction('address', `${form.address}`, idUser))
    dispatch(updateUserInfoAction('postal_code', `${form.postalCode}`, idUser))
    dispatch(updateUserInfoAction('country_id', `${form.country_id}`, idUser))
    dispatch(updateUserInfoAction('city', `${form.city}`, idUser))
    dispatch(updateUserInfoAction('name', `${form.name}`, idUser))
    dispatch(updateUserInfoAction('email', `${form.email}`, idUser))
    notify(`Modifications sauvegardées`, 'success')
    setIsEditing(false)
    setTimeout(() => {
      onUpdateInfo()
    }, 1000)
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'}`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <h2>Adresse postale</h2>
            <p>
              {form.address} {form.postalCode} {form.city} {form.country?.name}
              {/* {countries && form.country && countries[form.country - 1].name} */}
            </p>
          </div>
          <a onClick={() => setIsEditing(true)} href="#!">
            Modifier
          </a>
        </div>
      </div>

      {isEditing && (
        <div className="row">
          <div className="d-flex justify-content-between">
            <h2>Adresse postale</h2>
            <a href="#!" onClick={() => setIsEditing(false)}>
              Annuler
            </a>
          </div>
          <div className="col-12 mb-2">
            <div className="row">
              <div className="col-12 col-md-6">
                <h3>Adresse</h3>
                <input
                  type="text"
                  name="address"
                  value={form.address}
                  onChange={handleChange}
                  className="form-control p-4"
                />
              </div>
              <div className="col-12 col-md-6">
                <h3>Pays</h3>
                <select
                  defaultValue=""
                  className="form-control"
                  name="country_id"
                  onChange={handleChange}
                  value={form.country_id}
                >
                  <option value=""></option>
                  {form.country_id
                    ? form?.countries.map((value, index) => {
                        if (form.country_id == value.id) {
                          return (
                            <option key={index} value={value.id} selected>
                              {value.name}
                            </option>
                          )
                        } else {
                          return (
                            <option key={index} value={value.id}>
                              {value.name}
                            </option>
                          )
                        }
                      })
                    : form.countries.map((value, index) => {
                        return (
                          <option key={index} value={value.id}>
                            {value.name}
                          </option>
                        )
                      })}
                </select>
              </div>
            </div>
          </div>
          <div className="col-12">
            <div className="row">
              <div className="col-12 col-md-6 form-group">
                <h3>Code postal</h3>
                <input
                  type="text"
                  name="postalCode"
                  value={form.postalCode}
                  onChange={handleChange}
                  className="form-control p-4"
                />
              </div>
              <div className="col-12 col-md-6 form-group">
                <h3>Ville</h3>
                <SearchAlgoia
                  onChange={handleChangeVille}
                  inputValue={form.city || ''}
                  index="cities"
                  placeholder="Ville"
                  attribute="name"
                  onSelectOption={onSelectOption}
                  defaultValue={form.city}
                />
              </div>
            </div>
          </div>
          <button onClick={handleSubmit} className="btn btn-primary">
            Sauvegarder
          </button>
        </div>
      )}
    </>
  )
}

export default AdressInput
