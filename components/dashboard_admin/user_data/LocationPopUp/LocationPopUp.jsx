import React from 'react'

//STYLES
import styles from './styles.module.scss'

import PhothoInput from './inputs/PhothoInput'
import NameInput from './inputs/NameInput'
import GenreInput from './inputs/GenreInput'

import BirthdayInput from './inputs/BirthdayInput'
import TextInput from './inputs/TextInput'
import AdressInput from './inputs/AdressInput'
import PassWordInput from './inputs/PassWordInput'

const LocationPopUp = ({ data, setState, state, onUpdateInfo }) => {
  const {
    id,
    name,
    lastname,
    birthday,
    email,
    phone,
    address,
    country,
    country_id,
    city,
    postal_code,
    photo_url,
    gender
  } = data

  return (
    <div className={`col-5 ${styles.locationPopUp}`}>
      <div onClick={() => setState(!state)}>X</div>
      <h3>Informations personelles</h3>
      <PhothoInput
        email={email}
        profilePicture={photo_url}
        onUpdateInfo={() => onUpdateInfo()}
        idUser={id}
      />
      <NameInput
        email={email}
        name={name}
        lastname={lastname}
        onUpdateInfo={() => onUpdateInfo()}
        idUser={id}
      />
      <GenreInput
        email={email}
        genre={gender}
        onUpdateInfo={() => onUpdateInfo()}
        idUser={id}
      />
      <BirthdayInput
        email={email}
        birthday={birthday}
        onUpdateInfo={() => onUpdateInfo()}
        idUser={id}
      />
      <TextInput
        email={email}
        label="Email"
        value={email}
        formName="email"
        onUpdateInfo={() => onUpdateInfo()}
        idUser={id}
      />
      <TextInput
        email={email}
        label="Téléphone"
        formName="phone"
        type="tel"
        value={phone}
        onUpdateInfo={() => onUpdateInfo()}
        idUser={id}
      />
      <AdressInput
        name={name}
        email={email}
        address={address}
        country={country}
        country_id={country_id}
        postalCode={postal_code}
        city={city}
        idUser={id}
        onUpdateInfo={() => onUpdateInfo()}
      />
      <PassWordInput
        email={email}
        onUpdateInfo={() => onUpdateInfo()}
        idUser={id}
        userData={data}
      />
    </div>
  )
}

export default LocationPopUp
