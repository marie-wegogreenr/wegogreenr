import React from 'react'
import LocationCard from './LocationCard'

const PreviousReservations = ({ user_data, onUpdateInfo }) => {
  return (
    <>
      {user_data &&
        user_data.map((item, index) => {
          //consoleLog(item)
          return (
            <LocationCard
              data={item}
              key={index}
              onUpdateInfo={() => onUpdateInfo()}
            />
          )
        })}
    </>
  )
}

export default PreviousReservations
