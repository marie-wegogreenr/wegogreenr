import React, { useEffect, useState } from 'react'
import LocationPopUp from './LocationPopUp/LocationPopUp'

const LocationCard = ({ data, onUpdateInfo }) => {
  /*  Destructuring data prop  */
  const { id, name, lastname, email, roles, photo_url } = data
  /*  Local states  */
  const [popUp, setPopUp] = useState(false)

  return (
    <>
      <div className="row text-align-center">
        <div className="col-2 d-flex justify-content-center text-align-center">
          <img
            src="/images/logo.svg"
            className="rounded-circle"
            alt="Rounded Image"
            width={50}
            height={50}
          />
        </div>
        <div className="col-10">
          <div className="row justify-content-between">
            <div className="col-3">
              <p>{}</p>
              <p>Nom: {name}</p>
              <p>Prenom: {lastname}</p>
            </div>
            <div className="col-3 text-center">
              <p>{}</p>
              <p>Email: {email}</p>
            </div>
            <div className="col-3 text-center">
              <p>Rol(s): </p>
              <ul>
                {roles.map((value, index) => {
                  return <li style={{ listStyle: 'none' }}>{value.name}</li>
                })}
              </ul>
            </div>
            <div className="col-3 text-center">
              <a onClick={() => setPopUp(!popUp)} href="#!">
                Afficher le détail
              </a>
            </div>
          </div>
        </div>
      </div>

      {popUp && (
        <LocationPopUp
          data={data}
          setState={setPopUp}
          state={popUp}
          onUpdateInfo={() => onUpdateInfo()}
        />
      )}
    </>
  )
}

export default LocationCard
