export default function SelectGreenScore({ setGreenScoreSelect }) {
  const options = [
    { text: `Baby GreenR`, value: 1 },
    { text: `GreenR en Herbe`, value: 2 },
    { text: `GreenR en ascension`, value: 3 },
    { text: `Captain GreenR`, value: 4 },
    { text: `GreenR au sommet`, value: 5 }
  ]

  return (
    <select
      id="green-score-select"
      type="text"
      className="form-control border rounded-2 px-4  select-height"
      onChange={(e) => setGreenScoreSelect(parseInt(e.target.value))}
    >
      {options.map((item) => (
        <option value={item.value}>{`Greenscore: ${item.text}`}</option>
      ))}
    </select>
  )
}
