import React from 'react'
import { useSelector } from 'react-redux'
import Link from 'next/link'
import Router from 'next/router'

export default function SideMenu() {
  const { aTab } = useSelector((state) => state.configuration)

  return (
    <div className="admin-sidemenu-content">
      <div
        className={`admin-sidemenu-item ${
          aTab === 'abracadaroom' ? 'is-active' : ''
        }`}
        onClick={() => {
          Router.push('/admin/abracadaroom')
        }}
      >
        <a>Abracadaroom</a>
      </div>
      <hr />
      <div
        className={`admin-sidemenu-item ${
          aTab === 'Établissements' ? 'is-active' : ''
        }`}
        onClick={() => {
          Router.push('/admin/etablissement')
        }}
      >
        <a>Établissements</a>
      </div>
      <hr />
      <div
        className={`admin-sidemenu-item ${
          aTab === 'Hébergements' ? 'is-active' : ''
        }`}
        onClick={() => {
          Router.push('/admin/hebergements')
        }}
      >
        <a>Hébergements</a>
      </div>
      <hr />
      <div
        className={`admin-sidemenu-item ${
          aTab === 'homepage-edition' ? 'is-active' : ''
        }`}
        onClick={() => {
          Router.push('/admin/homepage-edition')
        }}
      >
        <a>Outils d'édition</a>
      </div>
      <hr />
      <div
        className={`admin-sidemenu-item ${
          aTab === 'Réservations' ? 'is-active' : ''
        }`}
        onClick={() => {
          Router.push('/admin/reservations')
        }}
      >
        <a>Réservations</a>
      </div>
      <hr />
      <div
        className={`admin-sidemenu-item ${
          aTab === 'usagers' ? 'is-active' : ''
        }`}
        onClick={() => {
          Router.push('/admin/user')
        }}
      >
        <a>Usagers</a>
      </div>
      <hr />
    </div>
  )
}
