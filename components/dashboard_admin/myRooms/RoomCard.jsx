import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Next */
import Image from 'next/image'
import Link from 'next/link'

/* Components */
import { User } from '@components/Icons'
import { routes } from 'constants/index'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faStar as farFaStar } from '@fortawesome/free-regular-svg-icons'
import {
  faStar as fasFaStar,
  faChevronRight
} from '@fortawesome/free-solid-svg-icons'

import Cookies from 'universal-cookie'

function RoomCard({
  internalName,
  peopleCapacity = '1',
  basicPrice = '0',
  id,
  active,
  outstanding,
  principal_image,
  onPublish,
  onPublishWithOutRefresh,
  from,
  updateRooms,
  key
}) {
  library.add(farFaStar, fasFaStar, faChevronRight)

  const [fields, setFields] = React.useState({
    statusActive: active
  })

  const outstandingHebergement = async () => {
    if (from == 'admin') {
      const cookies = new Cookies()
      //Publish or unpublish room
      const res_publish = await fetch(
        process.env.NEXT_PUBLIC_API_URL + 'add-outsanding/' + id,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + cookies.get('tk_user')
          },
          method: 'GET'
        }
      )

      const status = await res_publish.status
      if (status == 200) {
        updateRooms()
      }
    }
  }

  const changeStatus = () => {
    setFields({ ...fields, statusActive: !fields.statusActive })
    onPublishWithOutRefresh()
  }

  return (
    <div id={key}>
      <div className="bg-white rounded-3 overflow-hidden shadow">
        <div className="card-image">
          <figure className="mb-0">
            <img
              width={100 + '%'}
              height={300}
              src={
                principal_image
                  ? 'https://wegogreenr-laravel.s3.eu-west-2.amazonaws.com/' +
                    principal_image
                  : 'https://picsum.photos/200'
              }
              className="image-hebergement"
            />
            {from == 'admin' && (
              <FontAwesomeIcon
                icon={outstanding == 0 ? ['far', 'star'] : ['fas', 'star']}
                className="icon-star"
                data-toggle="tooltip"
                data-placement="right"
                title={outstanding == 0 ? 'Sin destacar' : 'Destacado'}
                onClick={outstandingHebergement}
              />
            )}
          </figure>
        </div>

        <div
          className="p-3 d-flex flex-column justify-content-between card-info"
          style={{ minHeight: '250px' }}
        >
          <h2 className="font-montserrat weight-bold h4 m-0">{internalName}</h2>

          <div className="d-flex justify-content-between align-items-center">
            <div className="card-modifier">
              <Link
                href={{
                  pathname: routes.ADMIN_ETABLISHMENT_BY_ID,
                  query: { id }
                }}
              >
                <a className="color-secondary">
                  Modifier
                  <FontAwesomeIcon
                    icon="chevron-right"
                    size="5x"
                    className="icon-progressbar"
                  />
                </a>
              </Link>
            </div>
          </div>

          <div className="border-top border-bottom py-2">
            <p className="text-center mb-0 d-flex align-items-center justify-content-center">
              Estado del anuncio:
              <div className="custom-control custom-switch">
                <input
                  type="checkbox"
                  className="custom-control-input"
                  id={id}
                  checked={fields.statusActive ? true : false}
                  onClick={from == 'admin' ? '' : changeStatus}
                />
                <label
                  className="custom-control-label ml-2"
                  htmlFor={id}
                  style={{ cursor: 'pointer' }}
                >
                  {active ? 'Publié' : 'Non Publié'}
                </label>
              </div>
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default RoomCard
