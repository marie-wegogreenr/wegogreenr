import { setClearLoading } from '@ducks/Config/actions'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { getRoomPag } from 'services/abracadaroomServices'
import { getAdminRooms } from 'services/hebergementsService'

export default function Finder({
  setRooms,
  pag,
  setPag,
  findFunction,
  query,
  setQuery
}) {
  const dispatch = useDispatch()
  const [Input, setInput] = useState('')
  const find = async () => {
    dispatch(setClearLoading(true))
    setQuery({ ...query, q: Input })
    const { data, ...pagination } = await findFunction(pag, {
      ...query,
      q: Input
    })
    setRooms(data)
    setPag(pagination)
    dispatch(setClearLoading(false))
  }
  return (
    <div>
      {/* <h5 className="mb-3">search</h5> */}
      <div className="d-flex">
        <input
          className="form-control"
          type="text"
          onChange={(e) => {
            setInput(e.target.value)
          }}
        />
        <button className="btn btn-secondary" onClick={find}>
          recherche
        </button>
      </div>
    </div>
  )
}
