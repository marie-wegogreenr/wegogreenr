import {
  greenScoreAbracadaroom,
  optionsFilterAbracadaroom,
  optionsSelectAbracadaroom
} from 'constants/filterTypes'
import { useToast } from 'hooks/useToast'
import React, { useState } from 'react'
import {
  activateRooms,
  changeAbracadaroomStatus,
  filterByStatus,
  updateGreenScore
} from 'services/abracadaroomServices'
import { highlightHebergement } from 'services/hebergementsService'
import MainSelect from './abracadaroom/MainSelect'

export default function AbracadaroomMasiveEditor({
  selectedRows,
  setRooms,
  rooms,
  where
}) {
  const [mainSelect, setmainSelect] = useState('void')
  const [statusSelect, setstatusSelect] = useState(1)
  const [greenScoreSelect, setGreenScoreSelect] = useState(1)
  const [notify] = useToast()

  const ChangeStatus = (newStatus, property) => {
    const oldData = rooms.map((r) => {
      return selectedRows.some((f) => {
        return f.id === r.id
      })
        ? { ...r, [property]: newStatus }
        : r
    })

    setRooms(oldData)
  }

  const handleChange = async () => {
    if (mainSelect === 'void')
      return notify('vous devez choisir une option', 'warning')

    const ids = selectedRows.map((sw) => sw.id).join(',')
    let update = null
    let body = { ids }

    if (mainSelect === 'publish' && isSomeItemChecked()) {
      body['activate'] = 1
      update = await activateRooms(body)
      if (update.code === 200) ChangeStatus(1, 'active')
      showNotify(update.code)
    } else if (mainSelect === 'unpublish' && isSomeItemChecked()) {
      body['activate'] = 0
      update = await activateRooms(body)
      if (update.code === 200) ChangeStatus(0, 'active')
      showNotify(update.code)
    } else if (mainSelect === 'change Status' && isSomeItemChecked()) {
      body['status'] = statusSelect
      update = await changeAbracadaroomStatus(body)
      if (update.code === 200) {
        ChangeStatus(statusSelect, 'status')
      }
      showNotify(update.code)
    } else if (mainSelect === 'change greenscore' && isSomeItemChecked()) {
      body['level'] = greenScoreSelect ? parseInt(greenScoreSelect) : ''
      update = await updateGreenScore(body)
      if (update.code === 200) {
        ChangeStatus(greenScoreSelect, 'level')
      }
      showNotify(update.code)
    } else if (mainSelect === 'mark') {
      const isNotActive = selectedRows.some((item) => !item.active)
      if (isNotActive) {
        notify('Les éléments doivent être actifs pour surligner', 'warning')
        return
      }
      body['outstanding'] = 1
      update = await highlightHebergement(body)
      if (update.code === 200) ChangeStatus(1, 'outstanding')
      showNotify(update.code)
    } else if (mainSelect === 'unmark') {
      body['outstanding'] = 0
      update = await highlightHebergement(body)
      if (update.code === 200) ChangeStatus(0, 'outstanding')
      showNotify(update.code)
    }
  }

  const isSomeItemChecked = () => {
    if (selectedRows.length === 0) {
      notify(
        'Vous devez sélectionner au moins un élément dans le tableau',
        'warning'
      )
      return false
    } else {
      return true
    }
  }

  const showNotify = (code) => {
    if (code === 200) {
      notify('Bonne mise à jour', 'success')
    } else {
      notify('Il y a eu un problème, veuillez réessayer plus tard', 'error')
    }
  }

  return (
    <div className="admin-userTable__search-bar">
      <MainSelect
        id="actions"
        label="Actions"
        callback={setmainSelect}
        options={optionsSelectAbracadaroom}
      />
      {mainSelect === 'change Status' && (
        <>
          <MainSelect
            id="status"
            label="Statut"
            callback={setstatusSelect}
            options={optionsFilterAbracadaroom.data}
          />
        </>
      )}
      {mainSelect === 'change greenscore' && (
        <>
          <MainSelect
            id="status"
            label="Greenscore"
            callback={setGreenScoreSelect}
            options={greenScoreAbracadaroom.data}
          />
        </>
      )}
      <button onClick={handleChange} className="btn btn-secondary ml-2">
        appliquer
      </button>
    </div>
  )
}
