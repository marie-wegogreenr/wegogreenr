import { useToast } from 'hooks/useToast'
import React, { useState } from 'react'
import {
  activateRooms,
  changeAbracadaroomStatus,
  filterByStatus
} from 'services/abracadaroomServices'
import { highlightHebergement } from 'services/hebergementsService'

export default function MasiveEditor({ selectedRows, setRooms, rooms }) {
  const [mainSelect, setmainSelect] = useState('void')
  const [notify] = useToast()

  const ChangeStatus = (newStatus, property) => {
    const oldData = rooms.map((r) => {
      return selectedRows.some((f) => {
        return f.id === r.id
      })
        ? { ...r, [property]: newStatus }
        : r
    })

    setRooms(oldData)
  }

  const handleChange = async () => {
    if (mainSelect === 'void')
      return notify('vous devez choisir une option', 'warning')
    if (selectedRows.length === 0)
      return notify(
        'vous devez sélectionner au moins un élément dans le tableau',
        'warning'
      )

    const ids = selectedRows.map((sw) => sw.id).join(',')
    let update = null
    let body = { ids }
    if (mainSelect === 'publish') {
      body['activate'] = 1
      update = await activateRooms(body)
      if (update.code === 200) ChangeStatus(1, 'active')
      showNotify(update.code)
    } else if (mainSelect === 'unpublish') {
      body['activate'] = 0
      update = await activateRooms(body)
      if (update.code === 200) ChangeStatus(0, 'active')
      showNotify(update.code)
    } else if (mainSelect === 'mark') {
      body['outstanding'] = 1
      update = await highlightHebergement(body)
      if (update.code === 200) ChangeStatus(1, 'outstanding')
      showNotify(update.code)
    } else if (mainSelect === 'unmark') {
      body['outstanding'] = 0
      update = await highlightHebergement(body)
      if (update.code === 200) ChangeStatus(0, 'outstanding')
      showNotify(update.code)
    }
  }

  const showNotify = (code) => {
    if (code === 200) {
      notify('bonne mise à jour', 'success')
    } else {
      notify('il y a eu un problème, veuillez réessayer plus tard', 'danger')
    }
  }

  return (
    <div>
      <h5 className="mb-3">options</h5>
      <div className="d-flex">
        <select
          type="text"
          className="form-control border rounded-2 select-height"
          onChange={(e) => {
            setmainSelect(e.target.value)
          }}
        >
          <option value="void">sélectionnez</option>
          <option value="publish">publier</option>
          <option value="unpublish">dépublier</option>
          <option value="mark">surligner</option>
          <option value="unmark">ne pas souligner</option>
        </select>
        <button onClick={handleChange} className="btn btn-secondary ml-2">
          appliquer
        </button>
      </div>
    </div>
  )
}
