import { CustomNormalDatePicker } from '@components/customInputs/CustomNormalDatePicker'
import { optionsReservations } from 'constants/filterTypes'
import moment from 'moment'
import React, { useState } from 'react'
import DatePicker from 'react-datepicker'
import { useDispatch } from 'react-redux'
import { getAdminReservations } from 'services/reservationService'
import InputFilter from './filters/InputFilter'
import SelectFilter from './filters/SelectFilter'
import { setClearLoading } from '@ducks/Config/actions'
import { isAfter } from 'date-fns'
import { useToast } from 'hooks/useToast'

export default function ReservationFilter({
  setData,
  setPag,
  pag,
  query,
  setQuery
}) {
  /*  Constants  */
  const initialFilterValues = {
    status: '',
    code: '',
    initial_date: '',
    final_date: ''
  }
  /*  Global scope  */
  const dispatch = useDispatch()
  /*  Local states  */
  const [notify] = useToast()
  const [filterValues, setFilterValues] = useState({ ...initialFilterValues })
  const [dates, setDates] = useState({ start: '', end: '' })
  const handleSearchClick = async () => {
    if (isAfter(new Date(dates.start), new Date(dates.end))) {
      notify(
        `La date de départ doit être postérieure à la date d'arrivée`,
        'error'
      )
      return
    }
    dispatch(setClearLoading(true))
    const initial_date = dates.start
      ? moment(dates.start).format('YYYY-MM-DD')
      : ''
    const final_date = dates.end ? moment(dates.end).format('YYYY-MM-DD') : ''

    const { data, ...pagination } = await getAdminReservations(pag, {
      ...filterValues,
      initial_date,
      final_date
    })
    setData(data)
    setPag(pagination)
    dispatch(setClearLoading(false))
  }

  const resetSearch = async () => {
    dispatch(setClearLoading(true))
    const { data, ...pagination } = await getAdminReservations(1, {
      ...initialFilterValues
    })
    setData(data)
    setPag(pagination)
    dispatch(setClearLoading(false))
  }

  const handleResetClick = () => {
    setFilterValues({ ...initialFilterValues })
    setDates({ start: '', end: '' })
    setQuery({ ...initialFilterValues })
    resetSearch()
  }

  return (
    <div className="admin-userTable__search-bar">
      <div className="admin-userTable__search-inputs">
        <SelectFilter
          options={optionsReservations}
          setFilterValues={setFilterValues}
          filterValues={filterValues}
        />
        <InputFilter
          setFilterValues={setFilterValues}
          filterValues={filterValues}
          id="code"
          label="Code"
        />
        <div className="d-flex">
          <div className="">
            <label>Arrivée</label>
            <DatePicker
              placeholderText="Arrivée"
              selected={dates.start}
              name="initial_date"
              onChange={(date) => setDates({ ...dates, start: date })}
              dateFormat="dd/MM/yyyy"
              className={'map-time-input'}
              customInput={<CustomNormalDatePicker />}
            />
          </div>
          <div className="">
            <label>Départ</label>
            <DatePicker
              selected={dates.end}
              placeholderText="Départ"
              onChange={(date) => setDates({ ...dates, end: date })}
              dateFormat="dd/MM/yyyy"
              className={'map-time-input'}
              customInput={<CustomNormalDatePicker />}
            />
          </div>
        </div>
      </div>
      <div className="admin-userTable__search-buttons">
        <button
          className="btn btn-secondary admin-userTable__search-btn"
          onClick={handleSearchClick}
        >
          Appliquer
        </button>
        <button
          className="btn btn-secondary admin-userTable__search-btn"
          onClick={handleResetClick}
        >
          Réinitialiser
        </button>
      </div>
    </div>
  )
}
