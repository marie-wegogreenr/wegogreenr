import { isEmpty } from 'lodash'
import { SectionLayout } from '@components/host/establishment'
import { Loader } from 'components/'
import React from 'react'
import { updateRoom } from 'services/abracadaroomServices'
import { useToast } from 'hooks/useToast'

export default function MainInfo({ roomData }) {
  const [isLoading, setIsLoading] = React.useState(false)
  const [isModifyMode, setIsModifyMode] = React.useState(false)
  const [notify] = useToast()
  const [fields, setFields] = React.useState({
    name: '',
    description: '',
    greenscore: 1,
    points: 1,
    isActive: 0,
    status: 1
  })

  React.useEffect(() => {
    if (!isEmpty(roomData)) {
      const greenscore = roomData.establishment.green_score
      setIsLoading(false)
      setFields({
        ...fields,
        description: roomData.description,
        name: roomData.public_name,
        greenscore: greenscore.level,
        points: greenscore.points,
        isActive: roomData.active,
        status: roomData.status,
        id: roomData.id
      })
    }
  }, [roomData])

  const changePoints = (action) => {
    let input = fields.greenscore
    if (action === '+' && input < 5) {
      input++
    } else if (action === '-' && input > 0) {
      input--
    }
    setFields({
      ...fields,
      greenscore: input
    })
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    const resp = await updateRoom(fields)
    if (resp.code === 200) {
      notify('chambre éditée avec succès', 'success')
    } else {
      notify('il y a eu un problème. veuillez réessayer plus tard', 'error')
    }

    setIsModifyMode(false)
  }

  const handleChange = (event) => {
    const { value, name } = event.target
    setFields({
      ...fields,
      [name]: value
    })
  }
  return (
    <SectionLayout
      title="Infos basiques"
      onModify={() => {
        setIsModifyMode(!isModifyMode)
      }}
      isModifyMode={isModifyMode}
    >
      {isEmpty(roomData) && <Loader />}

      {!isEmpty(roomData) && (
        <form onSubmit={handleSubmit}>
          <div className="row mb-2">
            <div className="col-6">
              <label
                htmlFor="nom_etablissement"
                className="form-label weight-bold"
              >
                Nom
              </label>
              <input
                type="text"
                className="form-control p-2"
                id="nom_etablissement"
                name="name"
                onChange={handleChange}
                value={fields.name}
                disabled={!isModifyMode || isLoading}
                // placeholder="La Rêvolution: roulotte pour  2 (voire 3)"
              />
            </div>
            <div className="col-6">
              <label htmlFor="isActive" className="form-label weight-bold">
                Estatut
              </label>
              <select
                className="form-control p-2"
                disabled={!isModifyMode || isLoading}
                name="status"
                onChange={handleChange}
                value={fields.status}
              >
                <option key={1} value={1}>
                  nouveau
                </option>
                <option key={2} value={2}>
                  en ligne
                </option>
                <option key={3} value={3}>
                  hors ligne
                </option>
                <option key={4} value={4}>
                  archivé
                </option>
              </select>
            </div>
          </div>

          <div className="row mb-2">
            <div className="col-6">
              <label
                htmlFor="nom_etablissement"
                className="form-label weight-bold"
              >
                Green Points
              </label>
              <input
                type="number"
                className="form-control p-2"
                id="nom_etablissement"
                name="points"
                onChange={handleChange}
                value={fields.points}
                disabled={!isModifyMode || isLoading}
                // placeholder="La Rêvolution: roulotte pour  2 (voire 3)"
              />
            </div>
            <div className="col-6 d-flex flex-column">
              <label htmlFor="greenscore" className="form-label weight-bold">
                Greenscore
              </label>
              <div className="card-button-nights abracadaroom-greenscore">
                <div className="col-8 input-night">
                  <p>{fields.greenscore} Points</p>
                </div>
                <div className="col-4 buttons-night">
                  <button
                    className="button-left"
                    type="button"
                    onClick={() => changePoints('-')}
                    disabled={!isModifyMode}
                  >
                    -
                  </button>
                  <button
                    className="button-right"
                    type="button"
                    onClick={() => changePoints('+')}
                    disabled={!isModifyMode}
                  >
                    +
                  </button>
                </div>
              </div>
            </div>
          </div>

          <div className="mb-3">
            <label htmlFor="description" className="form-label weight-bold">
              Description
            </label>
            <textarea
              disabled={!isModifyMode || isLoading}
              name="description"
              id="description"
              rows="5"
              defaultValue={fields.description}
              onChange={handleChange}
              className="form-control p-4"
            ></textarea>
          </div>

          {isModifyMode && (
            <button
              disabled={isLoading}
              type="submit"
              className="btn btn-secondary"
            >
              {isLoading ? 'Loading...' : 'Enregistrer'}
            </button>
          )}
        </form>
      )}
    </SectionLayout>
  )
}
