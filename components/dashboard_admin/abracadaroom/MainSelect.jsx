export default function MainSelect({ id, label, callback, options }) {
  return (
    <div className="admin-userTable__search-select">
      <label htmlFor={`${id}-main-select`}>{label}</label>
      <select
        id={`${id}-main-select`}
        type="text"
        className="form-control border rounded-2 select-height--long"
        onChange={(e) => {
          callback(e.target.value)
        }}
      >
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.text}
          </option>
        ))}
      </select>
    </div>
  )
}
