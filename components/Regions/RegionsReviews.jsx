import Slider from 'react-slick'
import RegionsReviewCard from './RegionsReviews/RegionsReviewCard'
import imageProfile from 'public/images/regions/Janette.jpg'

export default function RegionsReviews() {
  const settingsSlider = {
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    arrows: true
  }

  const data = [
    {
      name1: `Janette,`,
      name2: ` hôte du Vallon d'Armandine nous parle de sa région. `,
      text1: `L’Auvergne-Rhône-Alpes est une région qui a su préserver son identité, je suis très fière de voir que sa population y est donc très attachée. Ses espaces naturels et le volcanisme sont deux aspects très présents dans la région et selon moi, c’est ce qui la différencie des autres. Les paysages que l’on trouve ici sont uniques et l’on ne pourra pas les trouver ailleurs en France et en Europe !`,
      text2: `Quand on me demande quel est le plus joli village de l’Auvergne-Rhône-Alpes, j’ai souvent du mal à y répondre car la région compte de nombreux villages authentiques à découvrir. Mais si je devais en choisir un seul, ce serait le village de Montpeyroux qui n’est pas très loin de chez moi. Il se trouve à la limite entre le Puy-de-Dôme et la Haute-Loire. On ne peut pas le louper, il se trouve sur un petit mont, ce qui nous permet de le voir de loin et heureusement car il est classé comme l’un des plus beaux villages de France !`,
      text3: `Si l’on veut sortir des sentiers battus, l’Auvergne-Rhône-Alpes abrite aussi des jolis spots secrets à découvrir. Je pense notamment au Lac Bleu à 35 minutes de route du Puy-en-Velay, son eau claire nous permet de voir les poissons nager et découvrir un environnement fabuleux.`,
      text4: `Concernant la gastronomie, je vous conseille de goûter le plat emblématique, selon moi, la potée auvergnate. C’est sans hésitation, le meilleur plat typique de la région.`,
      text5: `Étant formée dans le secteur du tourisme, j’ai toujours cherché à être dans le partage et les rencontres. Je ressentais le besoin d’avoir un échange plus direct avec les voyageurs. Aujourd’hui je suis hôte et guide et c’est ce qui me permet de m’épanouir en faisant connaître ma région mais aussi en apprenant sur les autres régions de France et du monde grâce aux expériences livrées par les visiteurs.`,
      imgProfile: imageProfile
    }
  ]

  return (
    <section className="regions-page-reviews regions-page-section">
      <h2 className="regions-page-reviews__title cart-cadeau-secondary-subtitle">
        Nos hôtes parlent de leur région
      </h2>
      <Slider {...settingsSlider}>
        {data.map((item, index) => (
          <RegionsReviewCard data={item} key={index} alternative={index} />
        ))}
      </Slider>
    </section>
  )
}
