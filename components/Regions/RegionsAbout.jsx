import React, { useEffect, useState } from 'react'
import Content from './RegionsAbout/Content'
import Tabs from './RegionsAbout/Tabs'
import Coin1 from 'public/images/regions-page/les-coins1.png'
import Coin2 from 'public/images/regions-page/les-coins2.png'
import Coin3 from 'public/images/regions-page/les-coins3.png'
import Turisme1 from 'public/images/regions-page/turisme1.png'
import Turisme2 from 'public/images/regions-page/turisme2.png'
import Turisme3 from 'public/images/regions-page/turisme3.png'
import Gastro1 from 'public/images/regions-page/gastro1.png'
import Gastro2 from 'public/images/regions-page/gastro2.png'
import Gastro3 from 'public/images/regions-page/gastro3.png'
import Gastro4 from 'public/images/regions-page/gastro4.png'
import Tradition1 from 'public/images/regions-page/tradition1.png'
import Tradition2 from 'public/images/regions-page/tradition2.png'
import Ville1 from 'public/images/regions-page/ville1.png'
import Ville2 from 'public/images/regions-page/ville2.png'

export default function RegionsAbout() {
  const data = [
    {
      title: 'Les coins nature à ne pas manquer',
      subtitle: null,
      cards: [
        {
          title: `Les gorges de L’Ardèche - Ardèche (07)`,
          imgUrl: Coin1,
          text: `Prenez du temps pour admirer les paysages grandioses entre Vallon-Pont-d'Arc et Saint-Martin-d'Ardèche, 32 kilomètres de gorges spectaculaires au cœur de l'Ardèche vous attendent ! Une nature sauvage et préservée vous offre un époustouflant défilé de falaises calcaires pouvant atteindre jusqu'à 300 mètres de haut.`
        },
        {
          title: 'Volcan de Lemptegy - Puy-de Dôme (63)',
          imgUrl: Coin2,
          text: `À 15 kilomètres de Clermont-Ferrand, au centre de la France, vous trouverez un site unique en Europe, le Volcan de Lemptégy ! Il est possible de parcourir ce Volcan à pied ou en petit train. Vous pourrez toucher les bombes volcaniques, marcher le long de la coulée de lave, entre les cheminées volcaniques. Vous pourrez comprendre le fonctionnement d’un volcan, vu de l’intérieur !`
        },
        {
          title: 'Parc naturel regional du Vervors - Isère (38)',
          imgUrl: Coin3,
          text: `Un lieu magnifique où l’on retrouve des randonnées pour tous les niveaux. Ici toutes les indications vous guideront lors de votre balade, impossible de se perdre ! Le Vercors est un véritable écrin de verdure en toutes saisons. Sensation de liberté garantie.`
        }
      ]
    },
    {
      title: 'Slow tourisme',
      subtitle: null,
      cards: [
        {
          title: 'Sur les pas des Huguenots - Drôme (26) et Isère (38) ',
          imgUrl: Turisme1,
          text: `Découvrez cet itinéraire chargé d’histoire au sein de merveilleux paysages naturels entre Drôme, Isère et massif de la Chartreuse. Vous retracerez la route historique de l’exil des Huguenots après la révocation de l’Edit de Nantes.`
        },
        {
          title: 'Randonnés dans les massif des Bauges',
          imgUrl: Turisme2,
          text: `Des randonnés de tous les niveaux sont proposées dans ce magnifique massif composé de 14 villages qui ne demandent qu’à être explorés. Situé entre Chambéry, Aix-les-Bains et Annecy, le Parc Naturel Régional du Massif des Bauges est classé au patrimoine mondial de l’UNESCO. L’air de la montagne offre un vrai moment de fraîcheur.`
        },
        {
          title: 'Via fluvia',
          imgUrl: Turisme3,
          text: `À l’Est de l’Auvergne, vous pourrez rouler le long de la Haute-Loire grâce à la Via Fluvia qui relie Rhône et Loire. Vous pourrez parcourir les hauts plateaux du Velay, surplomber la Dunière et les gorges du Lignon, pour finir avec une vue incroyable sur les volcans endormis et les ravins de Corboeuf pour arriver à la vallée de la Loire. Vous pourrez profiter de cet itinéraire dépaysant et ressourçant pour un moment en pleine nature.`
        }
      ]
    },
    {
      title: 'Gastronomie',
      subtitle: `Les meilleurs plats de la région => Manger comme des locaux ?`,
      cards: [
        {
          title: 'Le Saint-Nectaire',
          imgUrl: Gastro1,
          text: `Le meilleur fromage de la région à déguster lors d’une escapade dans les plus beaux villages d’Auvergne`
        },
        {
          title: 'Le poulet de Bresse',
          imgUrl: Gastro2,
          text: `C’est le 4ème meilleur produit gastronomique au monde après le foie gras, le homard et la truffe. A déguster d’urgence !`
        },
        {
          title: 'Les cuisses de grenouille',
          imgUrl: Gastro3,
          text: `Souvent cuisinée en persillade, cette spécialité se déguste parfaitement en été au bord de la Saône`
        },
        {
          title: 'La fondue savoyarde',
          imgUrl: Gastro4,
          text: `Trois fromages fondus, un soupçon d'ail, du vin blanc et le tour est joué ! A déguster lors d’un séjour dans les stations des Alpes`
        }
      ]
    },
    {
      title: 'Traditions & fêtes locales',
      subtitle: null,
      cards: [
        {
          title: 'Fete des lumières à Lyon - Rhône (69)',
          imgUrl: Tradition1,
          text: `L’incontournable fête des lumières vous attend pour une vraie parenthèse de bonheur qui fait briller toute la ville de Lyon. Cette fête est née 8 décembre 1852 à l'inauguration de la statue de Marie au sommet de la colline de Fourvière. Depuis 30 ans, la ville a mis ses murs à la disposition d'artistes de la lumière.`
        },
        {
          title: 'Fète des vignerons ardéchois - Ardèche (07)',
          imgUrl: Tradition2,
          text: `Depuis plus de 40 ans, cette fête estivale rassemble les riverains et les touristes. Vous pourrez profiter de différentes activités : randonnée cyclo et VTT, défilé rue commerçante, loterie géante, jeu des anneaux, roulette à vin, baptêmes de poneys, trampoline, manège pour enfants. Et un défilé dans les rues de Ruoms sur le thème de la vigne !`
        }
      ]
    },
    {
      title: 'Les villes incontournables',
      subtitle: null,
      cards: [
        {
          title: 'Lyon',
          imgUrl: Ville1,
          text: `Son centre historique inscrit au patrimoine mondial de l’UNESCO vous enchantera. Vous y découvrirez le vieux Lyon, le parc de la Tête d’Or, ou les berges du Rhône pour un city tour à couper le souffle. Vous pourrez aussi monter au sommet de la colline de Fourvière (à pied ou en funiculaire) pour profiter d’une incroyable vue sur toute la ville.`
        },
        {
          title: 'Annecy',
          imgUrl: Ville2,
          text: `Ressourcez-vous dans celle que l’on surnomme la Venise des Alpes grâce à ses charmants canaux fleuris ou sa promenade au bord du lac. Une architecture d’époque, avec ses belles façades colorées et ses vieilles pierres vous fera voyager dans le temps. Pour mieux découvrir le lac et ses montagnes, détendez-vous grâce à la location de pédalos proposées aux visiteurs.`
        }
      ]
    }
  ]

  const [tabs, setTabs] = useState([])
  const [selectedTab, setSelectedTab] = useState(0)

  useEffect(() => {
    let tabsList = []
    data.map((item) => tabsList.push(item.title))
    setTabs(tabsList)
  }, [])

  return (
    <section className="regions-page-about regions-page-section">
      <h2 className="regions-page-about__title cart-cadeau-secondary-subtitle">
        À propos de la région
      </h2>
      <Tabs
        tabs={tabs}
        setSelectedTab={setSelectedTab}
        selectedTab={selectedTab}
      />
      <Content
        data={data}
        tabs={tabs}
        setSelectedTab={setSelectedTab}
        selectedTab={selectedTab}
      />
    </section>
  )
}
