import { ChevronRight } from '@components/Icons'
import Link from 'next/link'

export default function LinksBlock({ data, divider }) {
  return (
    <>
      <div className="regions-page-other__links-block">
        <ul className="regions-page-other__links-list">
          {data.map((item) => (
            <li className="regions-page-other__links-item">
              <Link href={item.href}>
                <a className="regions-page-other__links-anchor">{item.text}</a>
              </Link>
              {/* <span >&gt;</span> */}
              <ChevronRight className="regions-page-other__links-arrow" />
            </li>
          ))}
        </ul>
      </div>
      {divider && <div className="regions-page-other__links-divider"></div>}
    </>
  )
}
