import LinksBlock from './LinksBlock'

export default function RegionsLinks() {
  const dataFirstBlock = [
    { text: `Nouvelle Aquitaine`, href: `#` },
    { text: `Bretagne`, href: `#` },
    { text: `Provence Alpes Côte d'azur`, href: `#` }
  ]

  return (
    <nav className="regions-page-other__links">
      <LinksBlock data={dataFirstBlock} divider={true} />
      <LinksBlock data={dataFirstBlock} divider={true} />
      <LinksBlock data={dataFirstBlock} divider={false} />
    </nav>
  )
}
