import React, { useEffect, useState } from 'react'
import Slider from 'react-slick'
import { Loader } from '..'
import TitleBlock from './RegionsCard/TitleBlock'
import { Card } from 'components'
import { exampleActivitiesData } from 'constants/exampleRoomRegions'

export default function RegionsCardActivities() {
  const [randomActivities, setRandomActivities] = useState([])

  const data = {
    title: `Activités proche de chez vous`,
    textButton: `Voir tous les hébergements`
  }

  const settingsSlider = {
    dots: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: true,
    arrows: true,
    responsive: [
      {
        breakpoint: 1300,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 1099,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  }

  useEffect(async () => {
    setRandomActivities(exampleActivitiesData)
  }, [])

  return (
    <section className="regions-page-card-activities regions-page-section">
      <TitleBlock title={data.title} textButton={data.textButton} />

      {randomActivities.length === 0 ? (
        <Loader />
      ) : (
        <Slider {...settingsSlider}>
          {randomActivities?.map((item, index) => (
            <Card {...item} from="regions-page-activities" />
          ))}
        </Slider>
      )}
    </section>
  )
}
