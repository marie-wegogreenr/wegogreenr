import { useEffect, useState } from 'react'
import { useWindowSize } from 'hooks/useWindowSize'

export default function RegionsReviewCard({ data, alternative }) {
  const { width } = useWindowSize()
  const [plusBtn, setPlusBtn] = useState(false)

  const { name1, name2, text1, text2, text3, text4, text5, imgProfile } = data

  return (
    <div className="regions-page-reviews__card">
      <p className="regions-page-reviews__card-text">{text1}</p>

      {plusBtn && (
        <>
          <p className="regions-page-reviews__card-text">{text2}</p>
          <p className="regions-page-reviews__card-text">{text3}</p>
          <p className="regions-page-reviews__card-text">{text4}</p>
          <p className="regions-page-reviews__card-text">{text5}</p>
        </>
      )}

      <span
        className={`regions-page-reviews__card-text regions-page-reviews__card-text-span ${
          !plusBtn ? 'regions-page-reviews__card-text--moved' : ''
        }`}
        onClick={() => setPlusBtn(!plusBtn)}
      >
        <strong>{plusBtn ? 'Voir moins' : 'Voir plus...'}</strong>
      </span>
      <div className="regions-page-reviews__card-block">
        <img
          src={imgProfile}
          alt={`regions-page-reviews__card-img-${alternative}`}
          className="regions-page-reviews__card-img"
        />
        <h3 className="regions-page-reviews__card-title">
          <span className="regions-page-reviews__card-title-span">{name1}</span>
          {name2}
        </h3>
      </div>
    </div>
  )
}
