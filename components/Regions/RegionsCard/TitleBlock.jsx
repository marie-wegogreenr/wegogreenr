export default function TitleBlock({ title, textButton }) {
  return (
    <div className="regions-page-card-places__block">
      <h2 className="regions-page-card-places__title cart-cadeau-secondary-subtitle">
        {title}
      </h2>
      <a
        href="#"
        type="button"
        className="btn btn-outline-secondary regions-page-card-places__button regions-btn"
      >
        {textButton}
      </a>
    </div>
  )
}
