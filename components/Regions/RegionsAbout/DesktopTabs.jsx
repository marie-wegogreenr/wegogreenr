export default function DesktopTabs({ tabs, setSelectedTab, selectedTab }) {
  return (
    <nav className="regions-page-about-tabs">
      <ul className="regions-page-about-tabs__list">
        {tabs?.map((item, index) => (
          <li
            onClick={() => setSelectedTab(index)}
            key={`title-tab-${index}`}
            className={`regions-page-about-tabs__list-item${
              selectedTab === index ? '--selected' : ''
            }`}
          >
            {item}
          </li>
        ))}
      </ul>
      <div className="regions-page-about-tabs__line"></div>
    </nav>
  )
}
