export default function ContentCard({ imgUrl, title, text, generalTitle }) {
  return (
    <div className={`regions-page-about-content__card`}>
      <img
        src={imgUrl}
        alt={`regions-page-about-content__card-img-${imgUrl}`}
        className={`regions-page-about-content__card-img ${
          generalTitle === 'Gastronomie'
            ? 'regions-page-about-content__card-img--small'
            : ''
        }`}
      />
      <div className="regions-page-about-content__card-block">
        <h4
          className={`regions-page-about-content__card-title ${
            generalTitle === 'Gastronomie'
              ? 'regions-page-about-content__card-title--centered'
              : ''
          }`}
        >
          {title}
        </h4>
        <p
          className={`regions-page-about-content__card-text ${
            generalTitle === 'Gastronomie'
              ? 'regions-page-about-content__card-title--centered'
              : ''
          }`}
        >
          {text}
        </p>
      </div>
    </div>
  )
}
