import { useState, useEffect } from 'react'
import { useWindowSize } from 'hooks/useWindowSize'
import DesktopTabs from './DesktopTabs'
import MobileTabs from './MobileTabs'

export default function Tabs({ tabs, setSelectedTab, selectedTab }) {
  /*  Local states  */
  const [isMobile, setIsMobile] = useState(false)
  /*  Window width  */
  const { width } = useWindowSize()
  /*  Effects  */
  useEffect(() => {
    if (width > 1115) {
      setIsMobile(false)
    } else {
      setIsMobile(true)
    }
  }, [width])

  return (
    <>
      {isMobile ? null : (
        <DesktopTabs
          tabs={tabs}
          setSelectedTab={setSelectedTab}
          selectedTab={selectedTab}
        />
      )}
    </>
  )
}
