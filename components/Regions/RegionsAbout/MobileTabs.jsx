import { useEffect, useState } from 'react'

export default function MobileTabs({
  showMenu,
  setShowMenu,
  tabs,
  setSelectedTab,
  selectedTab
}) {
  /*  Local states  */
  const [tabsToShow, setTabsToShow] = useState([])
  /*  Effects  */
  useEffect(() => {
    let arrayTabs = []
    if (selectedTab === 0 || (selectedTab && tabs)) {
      tabs.forEach((element, index) => {
        if (index !== selectedTab) {
          arrayTabs.push(element)
        } else {
          arrayTabs.push(null)
        }
      })
      setTabsToShow(arrayTabs)
    }
  }, [selectedTab])
  /*  Functions  */
  const handleClick = (index) => {
    setSelectedTab(index)
    setShowMenu(false)
  }

  return (
    <nav className="regions-page-about-mobile-tabs">
      <ul className="regions-page-about-mobile-tabs__list">
        {tabsToShow.map((item, index) => (
          <>
            <li
              onClick={() => handleClick(index)}
              key={`title-tab-${index}`}
              className={`regions-page-about-mobile-tabs__list-item${
                selectedTab === index ? '--selected' : ''
              }`}
            >
              {item}
            </li>
            {item && (
              <div className="regions-page-about-mobile-tabs__list-line"></div>
            )}
          </>
        ))}
      </ul>
      <div className="regions-page-about-mobile-tabs__line"></div>
    </nav>
  )
}
