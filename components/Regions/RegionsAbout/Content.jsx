import { useEffect, useState } from 'react'
import { useWindowSize } from 'hooks/useWindowSize'
import { ChevronRight } from '@components/Icons'
import ContentCard from './ContentCard'
import MobileTabs from './MobileTabs'

export default function Content({ data, tabs, setSelectedTab, selectedTab }) {
  /*  Local states  */
  const [content, setContent] = useState({})
  const [isMobile, setIsMobile] = useState(false)
  const [showMenu, setShowMenu] = useState(false)
  const [smallArrow, setSmallArrow] = useState(false)
  const [isLongText, setIsLongText] = useState(false)
  /*  Window width  */
  const { width } = useWindowSize()
  /*  Effects  */
  useEffect(() => {
    if (width > 1115) {
      setIsMobile(false)
    } else {
      setIsMobile(true)
    }

    if (width < 500) {
      setSmallArrow(true)
    } else {
      setSmallArrow(false)
    }
  }, [width])

  useEffect(() => {
    const dataFiltered = data.filter((item, index) => index === selectedTab)
    setContent(dataFiltered[0])

    if (tabs[selectedTab]?.length > 25 && smallArrow) {
      setIsLongText(true)
    } else {
      setIsLongText(false)
    }
  }, [selectedTab])

  return (
    <div className="regions-page-about-content">
      <div className="regions-page-about-content__container-title">
        <h3
          className={`regions-page-about-content__title${
            isMobile && isLongText
              ? '--mobile-long'
              : isMobile
              ? '--mobile'
              : ''
          }`}
          onClick={() => setShowMenu(!showMenu)}
        >
          {content?.title}
        </h3>
        {isMobile ? (
          <ChevronRight
            width={smallArrow ? 12 : 15}
            height={smallArrow ? 12 : 15}
            className={`regions-page-about-content__title-icon${
              showMenu ? '--rotate' : ''
            }`}
          />
        ) : null}
      </div>
      {isMobile && showMenu ? (
        <MobileTabs
          showMenu={showMenu}
          setShowMenu={setShowMenu}
          tabs={tabs}
          setSelectedTab={setSelectedTab}
          selectedTab={selectedTab}
        />
      ) : null}
      {content?.subtitle && (
        <span className="regions-page-about-content__subtitle">
          {content?.subtitle}
        </span>
      )}
      <div className="regions-page-about-content__cards">
        {content?.cards?.map((item, index) => {
          const { imgUrl, title, text } = item
          return (
            <ContentCard
              key={index}
              imgUrl={imgUrl}
              title={title}
              text={text}
              generalTitle={content?.title}
            />
          )
        })}
      </div>
    </div>
  )
}
