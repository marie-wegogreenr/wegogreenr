export default function RegionsDescription() {
  const data = {
    subtitle1: `Parenthèse Eco-friendly en`,
    subtitle2: `Auvergne-Rhône-Alpes`,
    textList: [
      {
        text: `« Réveiller les sens, retrouver le sens » telle est la promesse de la région Auvergne-Rhône-Alpes !`
      },
      {
        text: `Entre lacs et montagnes, cette région offre une large palette de destinations, avec la ville de Lyon, capitale de la gastronomie, les Alpes et le Mont Blanc, le toit de l'Europe, la Drôme et l'Ardèche, évocatrices de la Provence, l'Auvergne et ses volcans.`
      },
      {
        text: `La destination est idéale pour les amoureux de randonnée et de loisirs en plein air. Depuis 2018, la région s’engage pour un tourisme bienveillant en valorisant un tourisme durable, incitant aux mobilités douces en créant du lien entre les habitants de la région et les visiteurs.`
      },
      { text: `(Re)découvrons la région sous un nouveau jour !` }
    ]
  }

  return (
    <section className="regions-page-description regions-page-section">
      <h2 className="regions-page-description__title cart-cadeau-secondary-subtitle">
        <span className="regions-page-description__title-span">
          {data.subtitle1}
        </span>
        <span className="regions-page-description__title-span">
          {` ${data.subtitle2}`}
        </span>
      </h2>
      <div>
        {data.textList.map((item, index) => (
          <p
            className="regions-page-description__text regions-text"
            key={`region-desc-text-${index}`}
          >
            {item.text}
          </p>
        ))}
      </div>
    </section>
  )
}
