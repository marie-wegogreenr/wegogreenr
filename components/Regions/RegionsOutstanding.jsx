import React from 'react'
import TextImage from './RegionsOutstanding/TextImage'
import TitleBlock from './RegionsOutstanding/TitleBlock'
import Balazuc from 'public/images/regions-page/balazuc.png'
import Tournemire from 'public/images/regions-page/tournemire.png'
import Bonneval from 'public/images/regions-page/bonneval.png'

export default function RegionsOutstanding() {
  const dataTitle = {
    title: `L’Auvergne-Rhône-Alpes hors des sentiers battus`,
    titleDetail: `Zoom sur les plus beaux villages `
  }

  const data = [
    {
      title: 'BALAZUC',
      titleDetail: 'Ardèche (07)',
      text: 'Le village de Balazuc fait partie des plus beaux villages de France. Plongez dans le passé médiéval de ce village à l’allure d’une vieille ville provençale. Admirez ses petites maisons accrochées à la falaise au-dessus de la rivière Ardèche, des ruelles fraîches et ombragées et des passages secrets comme celui de la « Fachinière ».',
      imgUrl: Balazuc
    },
    {
      title: 'TOURNEMIRE',
      titleDetail: 'Cantal (15)',
      text: 'Découvrez ce bourg du Parc régional des volcans d’Auvergne. Ce village, très vert, est situé en plein cœur du Cantal, à 2 h 30 de route au nord de Clermont-Ferrand. Établi le long d’une rue principale, il survole la vallée de la Doire. Vous serez enchantés par les maisons en pierres entourées de jardinets et potagers.',
      imgUrl: Tournemire
    },
    {
      title: 'BONNEVAL-SUR-ARC',
      titleDetail: ' Savoie (47)',
      text: 'Le plus haut village de la vallée de Maurienne est parfait pour une visite inoubliable. Lové dans le parc de la Vanoise, il vous emmène dans la quiétude des montagnes. Le magnifique panorama offert par le village est un endroit authentique qui permet d’admirer de nombreuses maisons en pierre et leurs cheminées traditionnelles.',
      imgUrl: Bonneval
    }
  ]

  return (
    <section className="regions-page-outstanding regions-page-section">
      <TitleBlock data={dataTitle} from="top-level" />
      <div className="regions-page-outstanding__wrapper">
        {data.map((item, index) => (
          <TextImage
            key={`item-${index}`}
            alternative={`item-${index}`}
            data={item}
          />
        ))}
      </div>
    </section>
  )
}
