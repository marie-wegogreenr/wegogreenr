import TextBlock from './TextBlock'

export default function TextImage({ data, alternative }) {
  return (
    <div className="regions-page-outstanding__text-image">
      <TextBlock data={data} />
      <img
        src={data.imgUrl}
        className="regions-page-outstanding__image"
        alt={`regions-page-outstanding_image_${alternative}`}
      />
    </div>
  )
}
