import TitleBlock from './TitleBlock'

export default function TextBlock({ data }) {
  const { title, titleDetail, text } = data

  return (
    <div className="regions-page-outstanding__text-block">
      <TitleBlock data={data} />
      <p className="regions-page-outstanding__text-block-text regions-text">
        {text}
      </p>
    </div>
  )
}
