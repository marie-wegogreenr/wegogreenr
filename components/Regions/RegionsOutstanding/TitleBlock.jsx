export default function TitleBlock({ data, from }) {
  const { title, titleDetail } = data

  return (
    <>
      {from === 'top-level' ? (
        <h2 className="regions-page-outstanding__title cart-cadeau-secondary-subtitle">
          <span className="regions-page-outstanding__title-span">{title}</span>
        </h2>
      ) : (
        <h3 className="regions-page-outstanding__text-block-title">
          <span className="regions-page-outstanding__text-block-span">
            {title}
          </span>
        </h3>
      )}
      <span
        className={`regions-page-outstanding__${
          from === 'top-level' ? '' : 'text-block-'
        }subtitle`}
      >
        {titleDetail}
      </span>
    </>
  )
}
