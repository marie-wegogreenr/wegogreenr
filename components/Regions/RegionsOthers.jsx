import React from 'react'
import RegionsLinks from './RegionsOther/RegionsLinks'

export default function RegionsOthers() {
  return (
    <section className="regions-page-other regions-page-section">
      <h2 className="regions-page-other__title cart-cadeau-secondary-subtitle">
        Autres régions
      </h2>
      <RegionsLinks />
    </section>
  )
}
