/*  React */
import { useEffect, useState } from 'react'
/*  Dependencies */
import Slider from 'react-slick'
import { exampleData } from 'constants/exampleRoomRegions'
/*  Components  */
import Loader from '@components/Loader'
import TitleBlock from './RegionsCard/TitleBlock'
import { Card } from 'components'
import { roomsByRegion } from 'services/homeServices'

export default function RegionsCardPlaces({ slidesToShow = 4 }) {
  /*  Local states  */
  const [randomRooms, setRandomRooms] = useState([])
  /*  Effects  */
  useEffect(async () => {
    try {
      const searchBody = {
        activities: '',
        equipments: '',
        final_date: '',
        initial_date: '',
        lat: 45.5126545,
        lng: 4.4904519,
        locationName: 'Autre site',
        max_price: null,
        min_price: 0,
        nelat: 45.89383147810292,
        nelng: 6.50946863851695,
        origin: '',
        page: 1,
        swlat: 45.01530198999212,
        swlng: 2.801582896329449,
        travellers_quantity: 0,
        types: '',
        zoom: 9
      }

      const result = await roomsByRegion(searchBody)
      if (result.code === 200) {
        const filtered = result.data.data.filter((room) => room.user_id)

        setRandomRooms(result.data.data)
        // const pages = Math.ceil(result.data.total)
        // for (let i = 0; i < pages; i++) {
        //   setRandomRooms([...randomRooms, ...result.data.data])
        // }
      }
    } catch (error) {
      setRandomRooms([])
    }
  }, [])

  const _data = {
    title: `Hébergements en Auvergne-Rhône-Alpes`,
    textButton: `Voir tous les hébergements`
  }

  const arraySize = randomRooms?.length || 0
  const slides = arraySize > slidesToShow ? slidesToShow : arraySize
  const settingsSlider = {
    dots: false,
    slidesToShow: slides,
    slidesToScroll: 1,
    infinite: true,
    responsive: [
      {
        breakpoint: 1300,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1050,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 705,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  }

  return (
    <section className="regions-page-card-places regions-page-section">
      <TitleBlock
        title={_data.title}
        textButton={_data.textButton}
        classBtn="homepage-regions__btn"
      />

      {randomRooms.length === 0 ? (
        <Loader />
      ) : (
        <Slider {...settingsSlider}>
          {randomRooms?.map((item, index) => (
            <Card
              {...item}
              key={`region-card-room-${index}`}
              from="regions-page"
            />
          ))}
        </Slider>
      )}
    </section>
  )
}
