import { Divider } from '@components/Icons'

export default function RegionsStyle() {
  const data = [
    {
      title: 'Pour les sportifs',
      text: `Pour les amateurs de randonnée ou de VTT, l’Auvergne-Rhône-Alpes est une excellente destination. Ses paysages montagneux offrent une vraie façon de se dépasser.`
    },
    {
      title: 'Séjour en tribu',
      text: `Venir en Auvergne-Rhône-Alpes en famille ou entre amis c’est découvrir une région féerique et des activités en plein air qui plairont aux petits comme aux grands. Pour créer des souvenirs, l’Auvergne-Rhône-Alpes propose différents cadres. Des activités à sensations fortes ou des randonnées pour les sportifs du dimanche, vous aurez le choix pour votre séjour ! De plus, les différentes spécialités de la région sauront mettre les papilles de tout le monde d’accord.`
    },
    {
      title: 'En mode slowlife',
      text: `Entre montagnes et lacs, les paysages de la région permettent de passer un incroyable séjour nature. Pour ceux qui veulent prendre le temps de profiter de leur voyage et apprécier les grands espaces verts, l’Auvergne-Rhône-Alpes est un vrai coin de paradis !`
    }
  ]

  return (
    <section className="regions-page-style regions-page-section">
      <h2 className="regions-page-style__title cart-cadeau-secondary-subtitle">
        Voyage pour tous en Auvergne-Rhône-Alpes
      </h2>
      <div className="regions-page-style__cards">
        {data.map((item, index) => (
          <div className="regions-page-style__card" key={index}>
            <h3 className="regions-page-style__card-title">{item.title}</h3>
            <p className="regions-page-style__card-text regions-text">
              {item.text}
            </p>
          </div>
        ))}
      </div>
    </section>
  )
}
