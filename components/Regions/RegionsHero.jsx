import Img from 'public/images/regions-page/hero-bg-1.png'

export default function RegionsHero() {
  const data = {
    title: `Auvergne-Rhône-Alpes`,
    imgUrl: Img
  }

  return (
    <section className="regions-page-hero">
      <div
        className="regions-page-hero__image"
        style={{
          backgroundImage: `url(${data.imgUrl})`
        }}
      ></div>
      <h1 className="regions-page-hero__title regions-main-title">
        {data.title}
      </h1>
    </section>
  )
}
