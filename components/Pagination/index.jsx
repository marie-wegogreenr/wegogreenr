import * as React from 'react'
import { colors, sizes } from 'styles/theme'
import { isEmpty } from 'utils'
import { useSelector, useDispatch } from 'react-redux'
import { useRouter } from 'next/router'

/* Components */
import { ChevronLeft, ChevronRight } from 'components/Icons'
import {
  setSearchLocation,
  searchRoomsByMapBounds
} from 'redux/ducks/hebergements/search/actions'
/* Styles */
import { ItemContainer, Item, Container } from './styles'
import { routes } from 'constants/index'

const evalIfIsValidPaged = (string) => {
  const patterEvalPage = 'page=\\d' // example: http://example/page=5 -> is valid
  const regexEvalPage = new RegExp(patterEvalPage).test(string) // boolean
  return regexEvalPage
}
const isValidUrlPagination = () => {
  let url = ''
  if (window) url = window.location.href
  evalIfIsValidPaged(url)
  return regexEvalPage // boolean
}

const getPageNumberByString = (string) => {
  if (!evalIfIsValidPaged(string)) return
  const expresion = /\d/i
  const arrayResult = string.match(expresion)
  if (isEmpty(arrayResult)) return
  return Number(arrayResult[0]) // first element containt number
}

// Main component
export function Pagination({ executeScroll }) {
  const [infoPagination, handleStateInfoPaged] = React.useState({})

  const [currentPage, handleCurrentPageState] = React.useState(1)

  const { pagedRooms, searchParams } = useSelector(
    (state) => state.searchedRooms.data
  )

  React.useEffect(() => {
    const infoPaged = {
      prevPage: getPageNumberByString(pagedRooms.prev_page_url),
      firstPage: getPageNumberByString(pagedRooms.first_page_url),
      nextPage: getPageNumberByString(pagedRooms.next_page_url),
      totalPages: pagedRooms.last_page,
      lastPage: pagedRooms.last_page,
      currentPage: pagedRooms.current_page
    }
    handleStateInfoPaged(infoPaged)
    handleCurrentPageState(pagedRooms.current_page)
  }, [pagedRooms])

  const dispatch = useDispatch()

  const router = useRouter()

  const contextColor = (index) => ({
    color: index === currentPage ? colors.white : colors.black,
    bgColor: index === currentPage ? colors.black : 'transparent'
  })

  const handleChangePag = (page) => {
    if (typeof page === 'string') page = Number(page)
    if (isEmpty(page)) return

    const params = {
      ...searchParams,
      page
    }
    dispatch(setSearchLocation(params))
    dispatch(searchRoomsByMapBounds(params))
    handleCurrentPageState(page)
    // router.replace(routes.SEARCHED_ROOMS)
  }

  const gotoFirstPage = () => {
    const firstPage = infoPagination.firstPage
    handleChangePag(firstPage)
  }

  const gotoLastPage = () => {
    const lastPage = infoPagination.lastPage
    handleChangePag(lastPage)
  }

  const gotoNextPage = () => {
    const nextPage = infoPagination.nextPage
    handleChangePag(nextPage)
  }

  const gotoPrevPage = () => {
    const prevPage = infoPagination.prevPage
    handleChangePag(prevPage)
  }

  const PrevPag = () => {
    const disabled = !infoPagination.prevPage
    const handlePrevClick = () => {
      gotoPrevPage()
      window.scrollTo(0, 0)
      executeScroll()
    }

    return (
      <ItemContainer
        onClick={handlePrevClick}
        disabled={disabled}
        bgHover={disabled && 'transparent'}
      >
        <Item color={colors['black']}>
          <ChevronLeft
            width="20"
            height="20"
            stroke={colors['darkBlue']}
            strokeWidth={1}
          />
        </Item>
      </ItemContainer>
    )
  }

  const NextPag = () => {
    const disabled = !infoPagination.nextPage
    const handleNextClick = () => {
      gotoNextPage()
      window.scrollTo(0, 0)
      executeScroll()
    }

    return (
      <ItemContainer
        onClick={handleNextClick}
        disabled={disabled}
        bgHover={disabled && 'transparent'}
      >
        <Item color={colors['black']}>
          <ChevronRight
            width="20"
            height="20"
            stroke={colors['darkBlue']}
            strokeWidth={1}
          />
        </Item>
      </ItemContainer>
    )
  }

  const RenderPags = () =>
    [...Array(infoPagination.totalPages).keys()].map((item, index) => {
      let safeIndex = index + 1
      if (currentPage < 3 && safeIndex < 3) {
        return (
          <ItemContainer
            bgColor={
              contextColor(safeIndex, infoPagination.currentPage).bgColor
            }
            onClick={() => handleChangePag(safeIndex)}
          >
            <Item
              color={contextColor(safeIndex, infoPagination.currentPage).color}
            >
              {safeIndex}
            </Item>
          </ItemContainer>
        )
      }
      if (
        currentPage >= 2 &&
        safeIndex > currentPage - 2 &&
        safeIndex < currentPage + 2
      ) {
        return (
          <ItemContainer
            bgColor={
              contextColor(safeIndex, infoPagination.currentPage).bgColor
            }
            onClick={() => handleChangePag(safeIndex)}
          >
            <Item
              color={contextColor(safeIndex, infoPagination.currentPage).color}
            >
              {safeIndex}
            </Item>
          </ItemContainer>
        )
      }
    })

  return (
    <Container>
      <PrevPag />
      {currentPage > 2 && infoPagination.totalPages > 2 && (
        <ItemContainer
          onClick={() => {
            gotoFirstPage()
            executeScroll()
          }}
        >
          <Item>1</Item>
        </ItemContainer>
      )}
      {currentPage > 2 && infoPagination.totalPages > 2 && (
        <ItemContainer
          bgHover="transparent"
          bgColor="transparent"
          onClick={() => executeScroll()}
        >
          <Item>...</Item>
        </ItemContainer>
      )}
      <RenderPags />
      {infoPagination.totalPages - currentPage >= 2 &&
        infoPagination.totalPages >= 2 && (
          <ItemContainer
            bgHover="transparent"
            bgColor="transparent"
            onClick={() => executeScroll()}
          >
            <Item>...</Item>
          </ItemContainer>
        )}
      {infoPagination.totalPages - currentPage >= 2 &&
        infoPagination.totalPages >= 2 && (
          <ItemContainer
            onClick={() => {
              gotoLastPage()
              executeScroll()
            }}
          >
            <Item>{infoPagination.totalPages}</Item>
          </ItemContainer>
        )}

      <NextPag />
    </Container>
  )
}
