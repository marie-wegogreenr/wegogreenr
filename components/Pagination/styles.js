import styled from '@emotion/styled'
import { colors, sizes } from 'styles/theme'

export const ItemContainer = styled.button`
  border-style: none;
  background-color: ${(props) => props.bgColor || 'transparent'};
  border-radius: 10rem;
  height: 3rem;
  width: 3rem;
  padding-right: 6px;
  padding-left: 6px;
  &:hover {
    background-color: ${(props) => props.bgHover || colors['graySecondary']};
  }
`
export const Item = styled.span`
  color: ${(props) => props.color || colors['black']};
  font-size: ${(props) => props.size || sizes['text-base']};
  font-weight: 600;
`
export const ButtonTransparent = styled.button`
  border-style: none;
  background-color: transparent;
`
export const Container = styled.div`
  display: flex;
  max-width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`
