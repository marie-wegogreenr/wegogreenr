import React from 'react'
import { consoleLog } from '/utils/logConsole'

const CreditCard = (props) => {
  return (
    <svg
      width="34"
      height="21"
      viewBox="0 0 34 21"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M3.32238 13.4732H8.52107M10.8313 13.4732H16.03M18.3399 13.4732H23.5375M25.8475 13.4732H31.0462M1 20.2425H33V1H1V20.2425ZM3.32238 6.35235H9.65453V9.76724H3.32238V6.35235Z"
        stroke="#9D9D9D"
        strokeWidth="0.851062"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default CreditCard
