import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

function DisabledPerson(props) {
  return (
    <svg
      width={30}
      height={30}
      viewBox="0 0 30 30"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M12.1952 7.70513C14.0468 7.70513 15.5478 6.20414 15.5478 4.35257C15.5478 2.50099 14.0468 1 12.1952 1C10.3437 1 8.84266 2.50099 8.84266 4.35257C8.84266 6.20414 10.3437 7.70513 12.1952 7.70513ZM12.1952 7.70513L12.1952 18.9897L21.0379 18.9897L24.035 26.3839H28M20.2591 18.9896C20.2738 19.1866 20.2797 19.3854 20.2797 19.5863C20.2797 24.3577 16.4112 28.2252 11.6389 28.2252C6.86653 28.2252 3 24.3577 3 19.5863C3 14.8149 6.86751 10.9454 11.6389 10.9454C11.826 10.9454 12.0111 10.9514 12.1952 10.9642M20.7563 13.1529H12.2641"
        stroke="#363636"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
        {...props}
      />
    </svg>
  )
}

export default DisabledPerson
