import React from 'react'
import { consoleLog } from '/utils/logConsole'

const PolygonDown = (props) => {
  return (
    <svg
      width="16"
      height="14"
      viewBox="0 0 16 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M7.00275 13.3204C7.52509 14.2251 8.83093 14.2251 9.35326 13.3204L15.8172 2.12461C16.3395 1.21989 15.6866 0.0890045 14.6419 0.0890045H1.7141C0.669433 0.0890045 0.0165145 1.21989 0.53885 2.12461L7.00275 13.3204Z"
        fill="white"
      />
    </svg>
  )
}

export default PolygonDown
