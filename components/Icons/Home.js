import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

function Home(props) {
  return (
    <svg
      width={30}
      height={30}
      viewBox="0 0 30 30"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M26.4889 11.9226V28L18.8206 27.9928V20.3782H10.6683V27.9875V27.9848L3 27.9776V11.9226L14.7444 3L26.4889 11.9226Z"
        stroke="#363636"
        strokeMiterlimit={10}
        strokeLinecap="round"
        strokeLinejoin="round"
        {...props}
      />
    </svg>
  )
}

export default Home
