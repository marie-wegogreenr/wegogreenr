import React from 'react'
import { consoleLog } from '/utils/logConsole'

const Coin = (props) => {
  return (
    <svg
      width="18"
      height="17"
      viewBox="0 0 18 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M9 16.5C13.4183 16.5 17 12.9183 17 8.49999C17 4.08172 13.4183 0.5 9 0.5C4.58172 0.5 1 4.08172 1 8.49999C1 12.9183 4.58172 16.5 9 16.5Z"
        stroke="#363636"
        strokeWidth="0.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6.72626 10.8807C6.939 11.1432 7.24381 11.3605 7.64069 11.5321C8.03757 11.7048 8.4642 11.7963 8.91998 11.8089H9.09841C9.57993 11.8026 9.98482 11.7243 10.3125 11.5744C10.6408 11.424 10.8827 11.2273 11.0388 10.9837C11.1955 10.7395 11.2733 10.4678 11.2733 10.167C11.2733 9.78617 11.1732 9.48478 10.973 9.2629C10.7729 9.04101 10.531 8.87517 10.2467 8.76536C9.96194 8.65614 9.57936 8.54232 9.09784 8.42337L8.91941 8.3862C8.53168 8.28612 8.21314 8.18319 7.96323 8.07625C7.71332 7.97045 7.50687 7.81833 7.34446 7.62161C7.18204 7.42488 7.10084 7.16696 7.10084 6.84843C7.10084 6.43553 7.25582 6.09297 7.5652 5.82132C7.87459 5.54968 8.3258 5.40101 8.91998 5.37584L9.09841 5.36668H9.11728C9.42381 5.36668 9.73434 5.4153 10.0506 5.51137C10.3663 5.60916 10.6516 5.75099 10.9084 5.93856"
        stroke="#363636"
        strokeWidth="0.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M9.00012 4.09311V12.9069"
        stroke="#363636"
        strokeWidth="0.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default Coin
