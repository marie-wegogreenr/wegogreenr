import React from 'react'
import { consoleLog } from '/utils/logConsole'

const HouseWithLeaf = (props) => {
  return (
    <svg
      width="63"
      height="79"
      viewBox="0 0 63 79"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      style={{
        filter: 'brightness(0) invert(1)'
      }}
      {...props}
    >
      <path
        d="M49.9409 26.4231L61.9297 35.5308V77.4531L41.9344 77.432V57.5791H20.677V77.4134L0.681641 77.3947V35.5308L31.3057 12.2647L48.3732 25.231"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
        {...props}
      />
      <path
        d="M43.124 10.2654C43.124 6.82205 44.3255 3.55362 46.4951 0.961761C48.6647 3.55595 49.8708 6.82205 49.8708 10.2654C49.8708 13.7111 48.6647 16.9818 46.4951 19.569C44.3255 16.9795 43.124 13.7088 43.124 10.2654Z"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
        {...props}
      />
      <path
        d="M61.323 13.5362C61.0244 16.8979 59.564 20.0613 57.1308 22.4992C55.1291 24.4985 52.6469 25.8399 49.9408 26.4208L48.373 25.2286C48.9143 22.4175 50.2814 19.807 52.36 17.7284C54.7885 15.2952 57.9356 13.8324 61.323 13.5362Z"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default HouseWithLeaf
