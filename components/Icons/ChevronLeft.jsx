import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

function ChevronLeft(props) {
  return (
    <svg
      width={55}
      height={55}
      viewBox="0 0 18 31"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M16.1299 1.65247L2.47769 
        15.3046L16.1299 28.9568"
        stroke="#363636"
        stroke-width="3"
        stroke-linecap="round"
        stroke-linejoin="round"
        {...props}
      />
    </svg>
  )
}

export default ChevronLeft
