import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

function Depart(props) {
  return (
    <svg
      width={30}
      height={30}
      viewBox="0 0 30 30"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      style={{ transform: 'rotate(90deg)' }}
      {...props}
    >
      <path
        d="M21.25 21.25L8.75 8.75M8.75 8.75V21.25M8.75 8.75H21.25"
        stroke="#363636"
        strokeLinecap="round"
        strokeLinejoin="round"
        {...props}
      />
    </svg>
  )
}

export default Depart
