import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

function ChevronRight(props) {
  return (
    <svg
      width={55}
      height={55}
      viewBox="0 0 18 31"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M2.47852 1.65234L16.1307 15.3045L2.47852 28.9567"
        stroke="#363636"
        stroke-width="3"
        stroke-linecap="round"
        stroke-linejoin="round"
        {...props}
      />
    </svg>
  )
}

export default ChevronRight
