import React from 'react'
import { consoleLog } from '/utils/logConsole'

const Warning = (props) => {
  return (
    <svg
      width="36"
      height="36"
      viewBox="0 0 36 36"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M18.0002 11.3334V18M18.0002 24.6667H18.0168M34.6668 18C34.6668 27.2048 27.2049 34.6667 18.0002 34.6667C8.79542 34.6667 1.3335 27.2048 1.3335 18C1.3335 8.79529 8.79542 1.33337 18.0002 1.33337C27.2049 1.33337 34.6668 8.79529 34.6668 18Z"
        stroke="#363636"
        strokeWidth="1.2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default Warning
