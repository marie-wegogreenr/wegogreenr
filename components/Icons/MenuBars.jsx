import React from 'react'

export const MenuBars = (props) => (
  <svg viewBox="0 0 220 104" width="37" height="37" {...props}>
    <rect y="0" width="220" height="20"></rect>
    <rect y="52" width="220" height="20"></rect>
    <rect y="104" width="220" height="20"></rect>
  </svg>
)
