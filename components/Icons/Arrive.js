import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

function Arrive(props) {
  return (
    <svg
      width={30}
      height={30}
      viewBox="0 0 30 30"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M8.75 8.75L21.25 21.25M21.25 21.25V8.75M21.25 21.25H8.75"
        stroke="#363636"
        strokeLinecap="round"
        strokeLinejoin="round"
        {...props}
      />
    </svg>
  )
}

export default Arrive
