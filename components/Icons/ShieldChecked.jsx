import React from 'react'
import { consoleLog } from '/utils/logConsole'

const ShieldChecked = (props) => {
  return (
    <svg
      width="49"
      height="56"
      viewBox="0 0 49 56"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      style={{
        filter: 'brightness(0) invert(1)'
      }}
      {...props}
    >
      <path
        d="M44.2546 33.1831C44.2546 40.5901 32.5201 48.3774 24.2149 51.0625C15.9098 48.3774 4.17529 40.5901 4.17529 33.1831V10.7639L24.2149 4.75201L44.2546 10.7639V33.1831Z"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M47.5065 34.0393C47.5065 42.6477 33.8683 51.6971 24.2171 54.8185C14.5659 51.6971 0.927734 42.6453 0.927734 34.0393V7.98307L24.2171 0.996094L47.5065 7.98307V34.0393Z"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.6157 26.4784L22.0103 35.7936L38.4176 19.6896"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default ShieldChecked
