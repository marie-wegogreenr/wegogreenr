import React from 'react'
import { consoleLog } from '/utils/logConsole'

const UserExplorer = (props) => {
  return (
    <svg
      width="63"
      height="67"
      viewBox="0 0 63 67"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M44.9941 24.9262V9.29572H18.2007V24.9262C18.2007 32.3261 24.1962 38.3241 31.5962 38.3241C38.9962 38.3241 44.9941 32.3238 44.9941 24.9262Z"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
      />
      <path
        d="M20.0908 2.41136C20.856 1.46187 21.8125 1 23.0116 1H40.1165C41.3156 1 42.2721 1.46187 43.0373 2.41136C44.4137 4.11905 44.9666 7.12149 45.1579 9.29343H17.9702C18.1638 7.12149 18.7144 4.11905 20.0908 2.41136Z"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M17.9697 9.2934H54.9649"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M25.7402 21.8491V23.0785"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M37.4097 21.8491V23.0785"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M35.9702 30.2662C35.359 31.61 33.523 32.5898 31.3697 32.5898C30.0447 32.5898 28.8409 32.2189 27.966 31.6194C27.4248 31.2485 27.0119 30.7888 26.7739 30.2756"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M35.9703 37.9555V43.5217C35.9703 46.0506 33.9011 48.1176 31.3722 48.1176C28.8457 48.1176 26.7764 46.0506 26.7764 43.5217V37.4283"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M18.2007 19.5909C21.5484 19.5909 25.7406 17.3116 25.7406 12.5735C27.7003 14.5331 38.2263 21.8491 44.9941 12.5735"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M46.0994 23.3538C45.7005 23.3538 45.3343 23.2255 45.0356 23.0086V20.0691C45.3343 19.8521 45.7029 19.7238 46.0994 19.7238C47.1026 19.7238 47.9144 20.5379 47.9144 21.5388C47.9144 22.5396 47.1026 23.3538 46.0994 23.3538Z"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M17.1275 23.3538C17.5264 23.3538 17.8927 23.2255 18.1913 23.0086V20.0691C17.8927 19.8521 17.5241 19.7238 17.1275 19.7238C16.1244 19.7238 15.3125 20.5379 15.3125 21.5388C15.3125 22.5396 16.1244 23.3538 17.1275 23.3538Z"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M1 66.186V57.0293C1 49.2164 7.38982 42.8265 15.2027 42.8265H26.7739"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M35.9702 42.8242H47.9427C55.7532 42.8242 62.1454 49.214 62.1454 57.0269V66.1836"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M43.062 31.8572C50.4037 32.2584 56.3059 38.303 56.4855 45.6983"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M20.1367 31.869C12.9001 32.3285 7.09115 38.2751 6.84619 45.5654V45.5677"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M45.5865 42.9759C45.5865 49.9186 44.9939 54.6801 50.6746 55.3193C50.6746 58.5084 50.6746 64.679 50.6746 65.5048"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M50.6724 62.2457H62.1479"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M62.1477 66.186H1"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M40.7503 42.9759C40.7503 49.9186 39.5489 60.2418 50.6722 60.0132"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M17.3259 42.9758C17.3259 49.9186 17.9184 54.68 12.2378 55.3193C12.2378 58.5083 12.2378 64.6789 12.2378 65.5048"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.2375 62.2456H1.17725"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M22.1616 42.9758C22.1616 49.9186 23.363 60.2417 12.2397 60.0131"
        stroke="#363636"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default UserExplorer
