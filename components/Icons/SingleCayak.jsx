import React from 'react'
import { consoleLog } from '/utils/logConsole'

const SingleCayak = (props) => {
  return (
    <svg
      width="64"
      height="68"
      viewBox="0 0 64 68"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      style={{
        filter: 'brightness(0) invert(1)'
      }}
      {...props}
    >
      <path
        d="M45.2998 46.8775C56.6681 34.7954 62.6659 18.8359 62.582 1.60742C45.3815 2.57091 29.7953 9.51134 18.4271 21.5911C7.05653 33.6732 1.06563 49.6676 1.16128 66.8797C18.3501 65.9326 33.9293 58.9619 45.2998 46.8775Z"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M56.6868 20.6603C58.0002 16.0131 58.6604 11.1094 58.6347 6.06335C53.6283 6.3433 48.7992 7.28813 44.2617 8.86284"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11.3818 17.5575L49.3125 55.4881"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M37.6902 43.0523C43.2568 37.0074 44.4872 29.0845 40.4383 25.356C36.3894 21.6275 28.5945 23.5052 23.0279 29.5501C17.4614 35.5949 16.231 43.5178 20.2799 47.2464C24.3287 50.9749 32.1236 49.0971 37.6902 43.0523Z"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M20.9048 32.2711L34.5149 45.879"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M17.9629 39.9185L27.0729 49.0284"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M2.03369 55.3622L11.9952 65.3237"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M2.87158 51.0208L15.8705 64.022"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M16.0546 20.0211L14.046 22.0297C12.6299 23.4435 10.3157 23.4435 8.89961 22.0297L4.17782 17.308C3.27732 16.4075 3.27732 14.9331 4.17782 14.0326L8.05977 10.1506C8.96027 9.25009 10.4347 9.25009 11.3352 10.1506L16.0523 14.8678C17.4707 16.2862 17.4707 18.6051 16.0546 20.0211Z"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M45.8807 50.09L43.8721 52.0986C42.456 53.5147 42.456 55.8289 43.8721 57.245L48.5939 61.9667C49.4944 62.8672 50.9688 62.8672 51.8693 61.9667L55.7513 58.0849C56.6518 57.1844 56.6518 55.7099 55.7513 54.8094L51.0341 50.0923C49.6157 48.6739 47.2968 48.6739 45.8807 50.09Z"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default SingleCayak
