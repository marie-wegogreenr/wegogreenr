import React from 'react'

export const Filter = (props) => (
  <svg
    width="29"
    height="25"
    viewBox="0 0 29 25"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <line
      x1="12"
      y1="19.25"
      x2="29"
      y2="19.25"
      stroke="black"
      stroke-width="1.5"
    />
    <circle cx="6" cy="19" r="5.25" stroke="black" stroke-width="1.5" />
    <line
      y1="-0.75"
      x2="17"
      y2="-0.75"
      transform="matrix(-1 -8.74228e-08 -8.74228e-08 1 17 7)"
      stroke="black"
      stroke-width="1.5"
    />
    <circle
      r="5.25"
      transform="matrix(-1 0 0 1 22 6)"
      stroke="black"
      stroke-width="1.5"
    />
  </svg>
)
