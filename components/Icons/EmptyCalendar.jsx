import React from 'react'
import { consoleLog } from '/utils/logConsole'

const EmptyCalendar = (props) => {
  return (
    <svg
      width="22"
      height="21"
      viewBox="0 0 22 21"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M21 3.91513H1V20.2481H21V3.91513Z"
        stroke="#363636"
        strokeWidth="0.665029"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M1 6.65909H21"
        stroke="#363636"
        strokeWidth="0.665029"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M10.9994 3.17614C11.6688 3.17614 12.2115 2.63349 12.2115 1.96408C12.2115 1.29466 11.6688 0.751976 10.9994 0.751976C10.33 0.751976 9.78735 1.29466 9.78735 1.96408C9.78735 2.63349 10.33 3.17614 10.9994 3.17614Z"
        stroke="#363636"
        strokeWidth="0.665029"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M9.78819 1.63861L5.75586 3.91513"
        stroke="#363636"
        strokeWidth="0.665029"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.2104 1.63861L16.2428 3.91513"
        stroke="#363636"
        strokeWidth="0.665029"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default EmptyCalendar
