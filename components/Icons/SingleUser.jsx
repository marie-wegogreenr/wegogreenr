import React from 'react'
import { consoleLog } from '/utils/logConsole'

const SingleUser = (props) => {
  return (
    <svg
      width="44"
      height="45"
      viewBox="0 0 44 45"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M42.8005 43.7029V38.5565C42.8005 32.8712 38.193 28.2637 32.5077 28.2637H11.9222C6.23688 28.2637 1.62939 32.8712 1.62939 38.5565V43.7029"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M22.2152 21.9369C27.8997 21.9369 32.5079 17.3287 32.5079 11.6441C32.5079 5.95958 27.8997 1.35132 22.2152 1.35132C16.5306 1.35132 11.9224 5.95958 11.9224 11.6441C11.9224 17.3287 16.5306 21.9369 22.2152 21.9369Z"
        stroke="#1D1D1B"
        strokeWidth="1.2831"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}

export default SingleUser
