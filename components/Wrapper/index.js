import * as React from 'react'

/* Styles */
import { screens } from 'styles/theme'
import { Container } from './styles'

function Wrapper({ width = screens.lg, children }) {
  return <Container theme={{ width }}>{children}</Container>
}

export default Wrapper
