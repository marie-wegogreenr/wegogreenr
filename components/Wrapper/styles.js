import styled from '@emotion/styled'

export const Container = styled.div`
  width: 100%;
  max-width: ${({ theme }) => theme.width};
  margin: 0 auto;
  padding: 0 1rem;
`
