import * as React from 'react'
import AutoComplete from 'react-google-autocomplete'
/* Next */
import { useRouter } from 'next/router'
/* Components */
import ModalNumberTravelers from '@components/Home/Hero/Form/Inputs/ModalNumberTravelers'
/* Calendar */
import Calendar from 'react-calendar'
import 'react-calendar/dist/Calendar.css'
/* Constants */
import { statuses } from 'constants/index'
/* Redux */
import {
  setSearchLocation,
  setFinalDate,
  setInitialDate,
  setTraverllersQuantity,
  initSearching
} from '@ducks/hebergements/search/actions'
import { useDispatch, useSelector } from 'react-redux'
/* Icons */
import {
  People,
  Calendar as CalendarIcon,
  CloseSolid,
  ChevronLeft,
  Filter as FilterIcon,
  Search as SearchIcon
} from 'components/Icons'
/* moment js*/
import moment from 'moment'
/* Constants */
import { routes, DEFAULT_CITY_ZOOM } from 'constants/index'
/* Utils */
import { useClickOutside } from 'hooks'

export function ResponsiveSearch() {
  // state - redux
  const {
    data: {
      searchParams: {
        initial_date,
        final_date,
        locationName,
        travellers_quantity
      }
    },
    status
  } = useSelector((state) => state.searchedRooms)

  const [isSearchActive, handleSearchActiveState] = React.useState(false)

  const Search = () => (
    <div
      className="search__container"
      onClick={() => handleSearchActiveState(true)}
    >
      <div className="search__input-container">
        <ChevronLeft width="15" height="15" />
        <span className="search__location-name">{locationName}</span>
        <span className="search__datetime">
          {intervalDateFormat(initial_date, final_date)}
        </span>
        <div className="search__filter-icon">
          <FilterIcon width="17" height="17" />
        </div>
      </div>
    </div>
  )

  return isSearchActive ? (
    <ChangeSearchParams handleSearchActiveState={handleSearchActiveState} />
  ) : (
    <Search />
  )
}

const intervalDateFormat = (_initial_date, _final_date) => {
  if (!_initial_date && !_final_date) return 'Jours flexibles'
  var start = _initial_date ? moment(_initial_date).format('DD MMM') : 'souple'
  var final = _final_date ? moment(_final_date).format('DD MMM') : 'souple'
  return `${start} - ${final}`
}

const ChangeSearchParams = ({ handleSearchActiveState }) => {
  // state - redux
  const {
    data: {
      searchParams: {
        initial_date,
        final_date,
        locationName,
        travellers_quantity
      }
    },
    status
  } = useSelector((state) => state.searchedRooms)

  const [isPlaceSelected, setIsPlacedSelected] = React.useState(true)
  const [locationValue, handleLocationState] = React.useState(locationName)
  const [showTravelers, setShowTravelers] = React.useState(false)
  const [travelers, handleTravelersState] = React.useState(travellers_quantity)
  const [openCalendar, handleCalendarState] = React.useState(false)

  const domNode = useClickOutside(() => setShowTravelers(false))

  const onChangeLocation = (e) => {
    handleLocationState(e.target.value)
    setIsPlacedSelected(false)
  }
  // dispatch
  const dispatch = useDispatch()
  // Router
  const router = useRouter()

  const handleChangeTravelers = (id, value) => {
    handleTravelersState({
      ...travelers,
      [id]: value
    })
  }

  return (
    <div className="change-search__container">
      <div className="change-search__containeraction-icons">
        <div
          className="change-search__close-icon"
          onClick={() => handleSearchActiveState(false)}
        >
          <CloseSolid width="17" height="17" />
        </div>
        <div className="change-search__filter-icon">
          <FilterIcon width="17" height="17" />
        </div>
      </div>
      <div className="change-search__container-inputs">
        <div className="change-search__location-container">
          <div className="change-search__condition-icon">
            <SearchIcon width="17" height="17" />
          </div>
          <AutoComplete
            apiKey={process.env.NEXT_PUBLIC_GOOGLE_MAPS_KEY}
            options={{
              componentRestrictions: { country: 'fr' },
              types: ['(regions)']
            }}
            onPlaceSelected={(value) => {
              setIsPlacedSelected(true)
              handleLocationState(value.formatted_address)
              const coords = {
                lat: value.geometry.location.lat(),
                lng: value.geometry.location.lng(),
                locationName: value.formatted_address,
                zoom: DEFAULT_CITY_ZOOM
              }
              dispatch(setSearchLocation(coords))
            }}
            onChange={onChangeLocation}
            defaultValue={locationName}
            value={locationValue}
            className="change-search__location-input"
            placeholder="Où allez-vous?"
          />
        </div>
        <div className="search__horizontal-divider" />

        <div className="change-search__conditions-container">
          <div
            className="change-search__condition-input"
            onClick={() => handleCalendarState(true)}
          >
            <div className="change-search__condition-icon">
              <CalendarIcon width="17" height="17" />
            </div>
            <span className="change-search__condition-text">
              {intervalDateFormat(initial_date, final_date)}
            </span>
            <div style={{ width: '1px' }}></div>
          </div>

          <div className="search__vertical-divider" />

          <div
            className="change-search__condition-input"
            onClick={() => setShowTravelers(true)}
          >
            <div className="change-search__condition-icon">
              <People width="17" height="17" />
            </div>
            <span className="change-search__condition-text">
              {travellers_quantity.total} les hôtes
            </span>
            <div style={{ width: '1px' }}></div>
          </div>
        </div>
      </div>
      <ModalCalendar openCalendar={openCalendar} />
      <div className={`${showTravelers ? 'd-flex' : 'd-none'}`} ref={domNode}>
        <ModalNumberTravelers
          show={showTravelers}
          fields={travelers}
          handleChange={handleChangeTravelers}
        />
      </div>
    </div>
  )
}

const ModalCalendar = ({ openCalendar = false }) => {
  var countDisplayMonths = [...Array(3).keys()]
  const [isCalendarOpen, handleCalendarState] = React.useState(openCalendar)
  const [monthsDisplay, handleMonthsDisplayState] =
    React.useState(countDisplayMonths)

  const onChangeDay = (day) => {}

  React.useEffect(() => {
    handleCalendarState(openCalendar)
  }, [openCalendar])

  const addMoreMonthsDisplay = () => {
    var lastItem = monthsDisplay.length
    handleMonthsDisplayState([...monthsDisplay, lastItem])
  }

  const getMonthByItem = (item) => {
    if (item === undefined || item === null) return
    var date = new Date()
    date.setMonth(date.getMonth() + item)
    return date
  }

  return (
    <div
      className={`calendar__container ${
        isCalendarOpen ? '' : 'calendar__hidden'
      }`}
    >
      <div className="calendar__closeicon_container">
        <div
          className="calendar__closeicon"
          onClick={() => handleCalendarState(false)}
        >
          <CloseSolid width="20" height="20" />
        </div>
      </div>
      <div className="calendar__title-container">
        {/* <span className="calendar__title">Quand seras-tu là?</span> */}
      </div>
      {monthsDisplay.map((item) => (
        <div className="calendar__calendar-container">
          <div className="calendar__month_container">
            <span>{moment(getMonthByItem(item)).format('MMM YYYY')}</span>
          </div>
          <Calendar
            onClickDay={onChangeDay}
            value={getMonthByItem(item)}
            minDate={new Date()}
            showNavigation={false}
            showNeighboringMonth={false}
            className={`calendar__calendar-style`}
          />
        </div>
      ))}

      <div className="calendar__buttons-container">
        <button className="calendar__button-del-dates">
          <span className="calendar__button-del-dates-text">Effacer</span>
        </button>

        <button
          className="calendar__button-more-months"
          onClick={() => addMoreMonthsDisplay()}
        >
          <span className="calendar__button-more-months-text">Montre plus</span>
        </button>
      </div>
    </div>
  )
}
