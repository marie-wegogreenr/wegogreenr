import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Redux */
import { useDispatch, useSelector } from 'react-redux'
import { addEquipmentsFilter } from '@ducks/hebergements/search/actions'
import { CheckInput } from '.'

/* Constanst */
import { statuses } from 'constants/index'
import { Loader } from '..'

function EquipmentFilter() {
  // state - redux
  const {
    categories: {
      data: { equipments = [] },
      status
    },
    searchedRooms: {
      data: {
        searchParams: { equipments: equipmentsFilter }
      }
    }
  } = useSelector((state) => state)

  // dispatch
  const distpatch = useDispatch()

  // helper methods
  const handleChange = (e) => {
    const { checked, value } = e.target

    if (checked) {
      distpatch(addEquipmentsFilter([...equipmentsFilter, value]))
    } else {
      distpatch(
        addEquipmentsFilter(equipmentsFilter.filter((e) => e !== value))
      )
    }
  }
  return (
    <div className="row">
      {status === statuses.LOADING && <Loader />}

      {status === statuses.SUCCESS &&
        equipments.map((item) => {
          const { id, name } = item
          return (
            <CheckInput
              key={id}
              id={id}
              name={name}
              handleChange={handleChange}
              categoryFilter={equipmentsFilter}
            />
          )
        })}
    </div>
  )
}

export default EquipmentFilter
