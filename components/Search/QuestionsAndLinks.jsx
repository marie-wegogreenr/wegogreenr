import Faq from 'react-faq-component'
import { useRouter } from 'next/router'
export const QuestionsAndLinks = ({ questions, links }) => {
  const router = useRouter()
  return (
    <div className="mb-4">
      {questions !== null && (
        <Faq
          data={questions}
          styles={{
            bgColor: '#faf6f2',
            titleTextColor: '#48482a',
            rowTitleColor: '#363636',
            rowTitleTextSize: 'medium',
            rowContentColor: '#48484a',
            rowContentTextSize: '16px',
            rowContentPaddingTop: '10px',
            rowContentPaddingBottom: '10px',
            rowContentPaddingLeft: '20px',
            rowContentPaddingRight: '50px',
            arrowColor: '#af7154'
          }}
        />
      )}
      {links !== null && (
        <div class="mt-4">
          <h2>Découvrez les autres types d’hébergements</h2>
          <div className="d-flex  justify-content-around mx-4 mt-4">
            {links.map((l) => (
              <button
                className="btn btn-outline-secondary  text weight-semibold discover-Button"
                onClick={() =>
                  window.open(process.env.NEXT_PUBLIC_URL + l.link, '_self')
                }
              >
                {l.title}
              </button>
            ))}
          </div>
        </div>
      )}
    </div>
  )
}
