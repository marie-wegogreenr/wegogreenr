import * as React from 'react'
import { consoleLog } from '/utils/logConsole'
import { isEmpty } from 'utils'
/* Redux */
import { addPriceFilter } from '@ducks/hebergements/search/actions'
import { useDispatch, useSelector } from 'react-redux'

function PriceFilter() {
  // dispatch
  const dispatch = useDispatch()

  // redux state
  const {
    searchParams: { min_price = 0, max_price = null },
    rooms
  } = useSelector((state) => state.searchedRooms.data)

  const [price, handlePriceLocalState] = React.useState({
    min_price,
    max_price
  })

  React.useEffect(() => {
    if (min_price || max_price) return
    var listPrices = rooms?.map((item) => item.basic_price)
    var roomMinPrice = isEmpty(listPrices) ? 0 : Math.min(...listPrices)
    var roomMaxPrice = isEmpty(listPrices) ? null : Math.max(...listPrices)
    if (!Number.isFinite(roomMinPrice) || !Number.isInteger(roomMinPrice))
      roomMinPrice = 0
    if (!Number.isFinite(roomMaxPrice) || !Number.isInteger(roomMaxPrice))
      roomMaxPrice = null
    handlePriceLocalState({
      min_price: roomMinPrice,
      max_price: roomMaxPrice
    })
  }, rooms)

  React.useEffect(() => {
    handlePriceLocalState({
      min_price: min_price,
      max_price: max_price || 0
    })
  }, [min_price, max_price])

  const validateMaxPrice = () => {
    let minPrice = Number(min_price)
    let maxPrice = Number(max_price)
    if (minPrice > maxPrice) {
      var safeMaxPrice = minPrice + 1
      handleChangePrices({
        ...price,
        max_price: safeMaxPrice
      })

      dispatch(
        addPriceFilter({
          max_price: safeMaxPrice
        })
      )
    }
  }

  // helper methods
  const handleChangePrices = (event) => {
    const { name, value } = event.target
    var safeValue = Number(value)

    handlePriceLocalState({
      ...price,
      [name]: safeValue
    })
    // if (
    //   Number(price.min_price) > Number(value) &&
    //   name === 'max_price'
    // )
    //   safeValue = null
    dispatch(
      addPriceFilter({
        [name]: safeValue
      })
    )
  }

  return (
    <div className="d-flex flex-column flex-lg-row">
      <div className="border br-10 p-3 me-2">
        <label
          htmlFor="price__minimal"
          className="form-label d-inline-block text-truncate"
        >
          <b>Prix minium par nuitée : </b>
          {price.min_price}&euro;
        </label>
        <input
          min="0"
          step="1"
          type="number"
          onWheel={(e) => e.target.blur()}
          name="min_price"
          className="border-0"
          id="price__minimal"
          onChange={handleChangePrices}
          value={price.min_price}
          style={{ outline: 0 }}
        />
      </div>
      <div className="border-1 br-10 border p-3 ms-0 ms-lg-2 mt-4 mt-lg-0">
        <label
          htmlFor="price__maximum"
          className="form-label d-inline-block text-truncate"
        >
          <b>Prix maximum par nuitée : </b>
          {price.max_price || 0}&euro;
        </label>
        <input
          min={Number(price.max_price) + 1}
          step="1"
          type="number"
          onWheel={(e) => e.target.blur()}
          name="max_price"
          className="border-0"
          id="price__maximum"
          onChange={handleChangePrices}
          value={price.max_price}
          style={{ outline: 0 }}
        />
      </div>
    </div>
  )
}

export default PriceFilter
