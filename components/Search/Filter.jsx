import * as React from 'react'
/* Redux */
import { useDispatch, useSelector } from 'react-redux'
import {
  cleanFilters,
  filterRooms,
  initSearching,
  setFinalDate,
  setInitialDate,
  setSearchLocation,
  setTraverllersQuantity
} from '@ducks/hebergements/search/actions'
import { getAllCategories } from '@ducks/hebergements/categories/actions'

/* Components */
import {
  ActivitiesFilter,
  EquipmentFilter,
  Modal,
  PriceFilter,
  TypesFilter
} from '.'
import { useRouter } from 'next/router'
import GiftCardFilter from './GiftCardFilter'

function Filter() {
  // dispatch
  const dispatch = useDispatch()
  const router = useRouter()
  // redux state
  const { types, equipments, activities, min_price, max_price, accept_coupon } =
    useSelector((state) => state.searchedRooms.data.searchParams)

  // effects
  React.useEffect(() => {
    dispatch(getAllCategories())
  }, [])

  // helper methods
  const handleSave = () => {
    dispatch(initSearching())
  }

  const handleCleanFilters = async () => {
    const coords = {
      lat: 46.543749602738565,
      lng: 2.142333984375,
      locationName: 'France',
      zoom: 6
    }

    router.push('/search')

    setTimeout(() => {
      /*  Erase form bar data  */
      dispatch(setSearchLocation(coords))
      dispatch(setInitialDate(''))
      dispatch(setFinalDate(''))
      dispatch(setTraverllersQuantity({ adults: 0, children: 0 }))
      /*  Erase filters  */
      dispatch(cleanFilters())
      dispatch(filterRooms())
      dispatch(initSearching())
    }, 500)
  }

  const [showFilters, setShowFilters] = React.useState(false)

  return (
    <div className="my-4">
      <p className="font-monserrat weight-semibold size-xs">Affiner par:</p>
      <div>
        <Modal
          active={min_price || max_price || false}
          onSave={handleSave}
          id="prices"
          title="Prix"
        >
          <PriceFilter />
        </Modal>
        <Modal
          id="equipments"
          active={equipments.length}
          title="Équipements"
          onSave={handleSave}
        >
          <EquipmentFilter />
        </Modal>
        <Modal
          active={types.length}
          onSave={handleSave}
          id="types"
          title="Type de logement"
        >
          <TypesFilter />
        </Modal>

        {showFilters && (
          <>
            <Modal
              active={activities.length}
              onSave={handleSave}
              id="activities"
              title="A faire sur place"
            >
              <ActivitiesFilter />
            </Modal>
            <button
              onClick={handleCleanFilters}
              className="btn btn-primary rounded-pill py-1 px-4 mr-2 mb-2"
            >
              Effacer filtres
            </button>
          </>
        )}

        <button
          onClick={() => {
            setShowFilters(!showFilters)
          }}
          className="btn btn-primary rounded-pill py-1 mb-2"
        >
          {!showFilters ? '+' : '-'}
        </button>
      </div>
      <GiftCardFilter accept_coupon={accept_coupon} onSave={handleSave} />
    </div>
  )
}

export default Filter
