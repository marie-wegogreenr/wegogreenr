import * as React from 'react'

import { getGreenScoreSourceIcon, getImageURL } from 'utils'
import SliderPhotos from '@components/dashboard_user/reservations/SliderPhotos'
import types from '../../constants/establishmentTypes'
import { eventListObjectTriggered } from 'helpers/dataLayer/lists'
import { useDispatch } from 'react-redux'
import { setUrl } from '@ducks/tempPage/actions'

function EstablishmentCard({ establishment }) {
  const dispatch = useDispatch()
  const cityName = establishment?.city_name || ''
  const regionName = establishment?.region_name || ''
  const price = establishment?.external_information?.price || ''
  const sliderExtraSettings = { autoplay: false }

  const handleClick = () => {
    eventListObjectTriggered.selectItem.callback({
      ...establishment,
      price,
      from: 'establishment-card'
    })
    if (typeof window !== undefined) {
      dispatch(
        setUrl(`${window.location.origin}/establishment/${establishment.id}`)
      )
      window.open(`${window.location.origin}/temp`, '_blank').focus()
    }
  }

  return (
    <a
      href={`/establishment/${establishment.id}`}
      className="text-decoration-none"
      target="_blank"
      onClick={(e) => {
        e.preventDefault()
        handleClick()
      }}
    >
      <div className="text-reset w-100" style={{ cursor: 'pointer' }}>
        <div className="list-card__container card border-0">
          <div className="list-card__content">
            <div className="list-card__image-main">
              <SliderPhotos
                className="h-100"
                images={establishment?.external_images || []}
                extraSettings={sliderExtraSettings}
              />
            </div>
            <div className="pl-sm-4 list-card__details">
              <div className="card-body w-100 h-100 d-flex flex-column justify-content-between">
                <div>
                  <div className="d-flex justify-content-between mb-2">
                    <p className="card-text color-gray mb-0 fz-12">
                      {cityName}, {regionName}
                    </p>
                  </div>
                  <h5 className="card-title font-montserrat weight-bold fz-18">
                    {establishment.name}
                  </h5>

                  <div className="border-bottom border-gray my-0-75 w-25" />

                  <p className="card-text color-black fz-15">
                    <span>{types[establishment.type_id - 1]}</span>
                  </p>
                </div>

                <div className="d-flex justify-content-between align-items-center mt-4">
                  <div className="d-flex flex-column">
                    {/* <img
                      width="100px"
                      style={{ objectFit: 'cover' }}
                      src={getGreenScoreSourceIcon(
                        establishment.greenScore.score
                      )}
                    /> */}
                    <figure className="card__image-greenscore-figure">
                      <img
                        width="100px"
                        style={{ objectFit: 'cover' }}
                        src={getGreenScoreSourceIcon(
                          establishment.greenScore.score
                        )}
                        className="card__image-greenscore-img"
                      />
                      <label className="card__image-greenscore-label">
                        Green Score
                      </label>
                    </figure>
                  </div>

                  {price && (
                    <div className="d-flex flex-column">
                      <p className="color-black mb-0 fz-14 text-right">
                        A partir de
                      </p>
                      <p className="color-black mb-0 fz-13">
                        <span className="weight-bold fz-18">{price}&euro;</span>
                        /nuit
                      </p>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </a>
  )
}

export default EstablishmentCard
