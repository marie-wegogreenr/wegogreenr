import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Redux */
import { addActivitiesFilter } from '@ducks/hebergements/search/actions'
import { useDispatch, useSelector } from 'react-redux'
import { CheckInput } from '.'

/* Constants */
import { statuses } from 'constants/index'
import { Loader } from '..'

function ActivitiesFilter() {
  // state - redux
  const {
    categories: {
      data: { activities = [] },
      status
    },
    searchedRooms: {
      data: {
        searchParams: { activities: activitiesFilters }
      }
    }
  } = useSelector((state) => state)
  // dispatch
  const distpatch = useDispatch()

  // helper methods
  const handleChange = (e) => {
    const { checked, value } = e.target

    if (checked) {
      distpatch(addActivitiesFilter([...activitiesFilters, value]))
    } else {
      distpatch(
        addActivitiesFilter(activitiesFilters.filter((e) => e !== value))
      )
    }
  }

  return (
    <div className="row">
      {status === statuses.LOADING && <Loader />}

      {status === statuses.SUCCESS &&
        activities.map((item) => {
          const { id, name } = item
          return (
            <CheckInput
              key={id}
              id={id}
              name={name}
              handleChange={handleChange}
              categoryFilter={activitiesFilters}
            />
          )
        })}
    </div>
  )
}

export default ActivitiesFilter
