/* React */
import { useEffect, useState } from 'react'
/* Components */
import { CheckInput } from '.'
/* Redux */
import { addTypeFilter } from '@ducks/hebergements/search/actions'
import { getHebergementTypes } from '@ducks/hebergements/types/actions'
import { useDispatch, useSelector } from 'react-redux'
/* Constants */
import { statuses } from 'constants/index'
import { Loader } from '..'

function TypesFilter() {
  /*  Global states  */
  const {
    hebergementTypes: {
      data: { types = [] },
      status
    },
    searchedRooms: {
      data: {
        searchParams: { types: typesFilter }
      }
    }
  } = useSelector((state) => state)

  const dispatch = useDispatch()

  /*  Local states  */
  const [filteredTypes, setFilteredTypes] = useState([])
  const [includesInsolite, setIncludesInsolite] = useState(false)
  const [typesToShow, setTypesToShow] = useState([])

  /*  Effects  */
  useEffect(() => {
    dispatch(getHebergementTypes())
  }, [])

  useEffect(() => {
    if (Array.isArray(types) && types.length) {
      const insolitesGroup = [
        5, 9, 11, 13, 17, 23, 27, 29, 31, 4, 6, 8, 10, 12, 26, 28, 32, 34
      ]
      const arrayFiltered = types.filter(
        (item) => !insolitesGroup.some((element) => item.id === element)
      )
      setFilteredTypes(arrayFiltered)
    }
  }, [types])

  useEffect(() => {
    setTypesToShow([...filteredTypes])
  }, [typesFilter, filteredTypes])

  /*  Helper methods  */
  const handleChange = (event) => {
    const { checked, value } = event.target

    if (checked) {
      dispatch(addTypeFilter([...typesFilter, value]))
    } else {
      dispatch(addTypeFilter(typesFilter.filter((e) => e !== value)))
    }
  }

  return (
    <div className="row">
      {status === statuses.LOADING && <Loader />}

      {status === statuses.SUCCESS &&
        typesToShow.map((item) => {
          const { id, name } = item
          return (
            <CheckInput
              key={id}
              id={id}
              name={name}
              handleChange={handleChange}
              categoryFilter={typesFilter}
            />
          )
        })}
    </div>
  )
}

export default TypesFilter
