import React from 'react'
/* Components */
import { Card } from '.'
import EstablishmentCard from './EstablishmentCard'
import RedirectionModal from '@components/Modals/redirectionModal'
import { mapRooms } from 'helpers/roomHelper'

function CardList({ rooms = [], establishments = [] }) {
  if (!rooms.length && !establishments.length) {
    return <p>Nous n’avons pas d’hébergement pour le moment</p>
  }

  const mappedRooms = mapRooms(rooms)
  return (
    <div className="mb-4">
      {establishments?.map((establishment) => {
        return <EstablishmentCard establishment={establishment} />
      })}
      {mappedRooms?.map((room) => {
        const {
          id,
          public_name,
          people_capacity,
          basic_price,
          slug,
          type_id,
          establishment = {},
          isVisible,
          room_image = [],
          origin,
          origin_url
        } = room
        const {
          id: establishment_id,
          name,
          city_name,
          region_name,
          green_score = {}
        } = establishment
        return (
          <Card
            images={room_image}
            key={id}
            id={id}
            establishment_id={establishment_id}
            publicName={public_name}
            peopleCapacity={people_capacity}
            basicPrice={basic_price}
            slug={slug}
            establishmentName={name}
            typeId={type_id}
            cityName={city_name}
            regionName={region_name}
            greenScore={green_score?.score}
            origin={origin}
            urlRoom={origin_url}
          />
        )
      })}
      <RedirectionModal />
    </div>
  )
}

export default React.memo(CardList)
