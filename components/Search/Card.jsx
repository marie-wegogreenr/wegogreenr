import * as React from 'react'
import noImage from '/public/images/image-not-found.png'
import people from '/public/images/logo-line-person.png'
/* Next */
import Link from 'next/link'
/* Components */
import { Star } from '@components/Icons'
/* Constants */
import { routes } from 'constants/index'
import { useDispatch, useSelector } from 'react-redux'
import { getGreenScoreSourceIcon, getImageURL } from 'utils'
import SliderPhotos from '@components/dashboard_user/reservations/SliderPhotos'
import Resizer from 'react-image-file-resizer'
import Loader from '@components/Loader'
import { useState } from 'react'
import { adjustNumberWithComma } from 'helpers/utilsHelper'
import { eventListObjectTriggered } from 'helpers/dataLayer/lists'
import { setUrl } from '@ducks/tempPage/actions'
import { setFullLoading } from '@ducks/Config/actions'

function Card(props) {
  const {
    id,
    basicPrice,
    cityName,
    greenScore,
    images,
    peopleCapacity,
    publicName,
    regionName,
    slug,
    typeId,
    origin,
    urlRoom
  } = props

  const dispatch = useDispatch()
  const { types } = useSelector((state) => state.hebergementTypes.data)

  const sliderExtraSettings = { autoplay: false }

  const typeName = types.find((t) => t.id === typeId)?.name || ''
  const arrayImages = images.map((i) => {
    if (origin === null || origin === 1) {
      return process.env.NEXT_PUBLIC_AMAZON_IMGS + i.image.url
    } else if (origin === 2) {
      return i
    }
  })

  const redirect = (url, isExternal, event) => {
    if (event.target.type === 'button') return
    eventListObjectTriggered.selectItem.callback({
      ...props,
      typeName
    })
    if (isExternal) {
      $('#redirectionModal').modal('show')
      // notify('rediriger', 'success')
      return setTimeout(() => {
        $('#redirectionModal').modal('hide')
        window.open(url, '_blank').focus()
      }, 3500)
    } else {
      dispatch(setUrl(`${window.location.origin}${url}`))
      window.open(`${window.location.origin}/temp`, '_blank').focus()
    }
  }

  return (
    <div
      onClick={(e) => {
        urlRoom !== null
          ? redirect(urlRoom, true, e)
          : redirect(`/hebergements/${slug}`, false, e)
      }}
      className="text-decoration-none"
      target="_blank"
    >
      <div className="text-reset w-100" style={{ cursor: 'pointer' }}>
        <div className="list-card__container card border-0">
          <div className="list-card__content">
            <div className="list-card__image-main">
              <SliderPhotos
                className="h-100"
                images={arrayImages}
                extraSettings={sliderExtraSettings}
              />
            </div>
            <div className="pl-sm-4 list-card__details custom-card__content">
              <div className="card-body w-100 h-100 d-flex flex-column justify-content-between">
                <div>
                  <div className="d-flex justify-content-between mb-2">
                    <p className="card-text color-gray mb-0 card-region">
                      {cityName}, {regionName}
                    </p>
                  </div>
                  <h5 className="card-title font-montserrat weight-bold fz-22 card-title">
                    {publicName}
                  </h5>

                  <div className="border-bottom border-gray my-0-75 w-25" />

                  <p className="card-text color-black fz-15 number-persons">
                    <span>{typeName}</span>
                    {peopleCapacity !== 0 && (
                      <>
                        {' '}
                        pour <span>{peopleCapacity}</span>{' '}
                        <span>
                          {peopleCapacity > 1 ? 'personnes' : 'personne'}
                        </span>
                      </>
                    )}
                  </p>
                </div>

                <div className="d-flex justify-content-between align-items-center mt-4">
                  <div className="d-flex flex-column">
                    <figure className="card__image-greenscore-figure">
                      <img
                        width="100px"
                        style={{ objectFit: 'cover' }}
                        src={getGreenScoreSourceIcon(greenScore)}
                        className="card__image-greenscore-img"
                      />
                      <label className="card__image-greenscore-label">
                        Green Score
                      </label>
                    </figure>
                  </div>

                  <div className="person-logo">
                    <img src={people} style={{ marginBottom: '-10px' }} />
                    <p className="card-text color-black fz-15">
                      {`${peopleCapacity} ${
                        peopleCapacity > 1 ? 'pers' : 'per'
                      }`}
                    </p>
                  </div>

                  <div className="d-flex flex-column">
                    <p className="color-black mb-0 fz-14 text-right">
                      A partir de
                    </p>
                    <p className="color-black mb-0 fz-13 card-price">
                      <span className="weight-bold card-price-number">
                        {adjustNumberWithComma(basicPrice)}&euro;
                      </span>
                      /nuit
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Card
