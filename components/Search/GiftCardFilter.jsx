import { addGiftCardFilter } from '@ducks/hebergements/search/actions'
import { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import cadeau from 'public/images/search/cadeau.png'

function GiftCardFilter({ accept_coupon, onSave = () => {} }) {
  const dispatch = useDispatch()
  const [checkValue, setCheckValue] = useState(false)

  useEffect(() => {
    if (accept_coupon === 1 || accept_coupon === '1') {
      setCheckValue(true)
    } else {
      setCheckValue(false)
    }
  }, [accept_coupon])

  const handleChange = () => {
    dispatch(addGiftCardFilter(checkValue ? null : '1'))
    setCheckValue(!checkValue)
    onSave()
  }

  return (
    <div>
      <input
        id="gift-card-search"
        type="checkbox"
        className="form-check-input form-check-input--search"
        checked={checkValue}
        onChange={handleChange}
      />
      <label for="gift-card-search" className="onboarding__input-label">
        <img
          src={cadeau}
          width="18px"
          height="18px"
          className="form-check-input__span"
        />
        <span className="form-check-input__span">
          {' Utiliser ma carte cadeau We Go GreenR'}
        </span>
      </label>
    </div>
  )
}

export default GiftCardFilter
