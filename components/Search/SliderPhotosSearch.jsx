import Image from 'next/image'
import React, { useRef, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
import Slider from 'react-slick'
import { useEffect } from 'react'

const SliderPhotosSearch = ({
  images = [],
  id,
  heigth = 150,
  width = 291,
  className = '',
  extraSettings,
  indexSelected = 0
}) => {
  const slider1 = useRef()

  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    arrows: true,
    slickGoTo: indexSelected,
    ...extraSettings
  }

  useEffect(() => {
    slider1.current.slickGoTo(indexSelected)
  }, [])
  return (
    <>
      <div className="modal-content px-2 py-2">
        <div
          className="search-modal-body modal-body"
          style={{ height: '220px' }}
        >
          <Slider ref={(slider) => (slider1.current = slider)} {...settings}>
            {images.map((item, index) => (
              <div key={index}>
                {/* <Image
            width={width}
            height={heigth}
            className={`d-block w-100 rounded-3 obj-fit-cover ${className}`}
            src={item}
            alt="First slide"
          /> */}
                <Image
                  width={width}
                  height={heigth}
                  className="d-block w-100"
                  src={
                    typeof item === 'string'
                      ? item
                      : process.env.NEXT_PUBLIC_AMAZON_IMGS + item.image.url
                  }
                  alt="First slide"
                />
              </div>
            ))}
          </Slider>
        </div>
      </div>
    </>
  )
}

export default SliderPhotosSearch
