import * as React from 'react'
import DatePicker from 'react-datepicker'
/* Next */
import { useRouter } from 'next/router'

/* Components */
import { Search as SearchIcon } from 'components/Icons'

/* Constants */
import { routes, DEFAULT_CITY_ZOOM } from 'constants/index'

/* Redux */
import {
  checkFields,
  setSearchLocation,
  setFinalDate,
  setInitialDate,
  setTraverllersQuantity,
  initSearching
} from '@ducks/hebergements/search/actions'
import { useDispatch, useSelector } from 'react-redux'
/* Utils */
import { formatDate, getADayLater } from 'utils'
import ModalNumberTravelers from '@components/Home/Hero/Form/Inputs/ModalNumberTravelers'
import { useClickOutside } from 'hooks'
/* Libraries */
import AutoComplete from 'react-google-autocomplete'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons'
import { CustomDatePicker } from '@components/customInputs/CustomDatePicker'
import { zoomByPlaceTypes } from 'constants/index'
import { hasTimeToShow } from 'helpers/timeHelper'
/* Dependencies */
import { registerLocale, setDefaultLocale } from 'react-datepicker'
import fr from 'date-fns/locale/fr'
registerLocale('fr', fr)
setDefaultLocale('fr')

/*****************  MAIN COMPONENT ******************/
function Form() {
  // States
  const {
    searchParams,
    searchParams: {
      locationName,
      initial_date,
      final_date,
      travellers_quantity
    }
  } = useSelector((state) => state.searchedRooms.data)

  const [value, locationValueState] = React.useState(locationName)
  const [isPlaceSelected, setIsPlacedSelected] = React.useState(true)
  const [showTravelers, setShowTravelers] = React.useState(false)
  // const [travelers, handleTravelersState] = React.useState(travellers_quantity)

  // dispatch
  const dispatch = useDispatch()

  // Router
  const router = useRouter()

  const domNode = useClickOutside(() => setShowTravelers(false))

  // helper methods
  const handleSubmit = (e) => {
    e.preventDefault()
    // dispatch(checkFields())
    dispatch(initSearching())
    // router.replace(routes.SEARCHED_ROOMS)
  }

  const onChange = (e) => {
    locationValueState(e.target.value)
    setIsPlacedSelected(false)
  }

  React.useEffect(() => {
    locationValueState(locationName)
  }, [locationName])

  const setDate = (date, type) => {
    const dateFormatted = formatDate(date)
    dispatch(type === 'start' ? setInitialDate(date) : setFinalDate(date))
  }

  const handlePlaceSelected = (value) => {
    setIsPlacedSelected(true)
    locationValueState(value.formatted_address)
    let placeType = value.address_components[0].types[0]
    const coords = {
      lat: value.geometry.location.lat(),
      lng: value.geometry.location.lng(),
      locationName: value.formatted_address,
      zoom: zoomByPlaceTypes[placeType]
    }
    dispatch(setSearchLocation(coords))
  }

  return (
    <form
      onSubmit={handleSubmit}
      className="list-search-form d-flex justify-content-between align-items-center border rounded-pill py-2 pr-0 px-2 my-4 bg-white w-100 form-search-map"
    >
      <div className="form-search-map_input-content">
        <div className="mr-2 algolia-input__container">
          <AutoComplete
            apiKey={process.env.NEXT_PUBLIC_GOOGLE_MAPS_KEY}
            options={{
              componentRestrictions: { country: 'fr' },
              types: ['(regions)']
            }}
            onPlaceSelected={handlePlaceSelected}
            onChange={onChange}
            defaultValue={locationName}
            value={value}
            className="outline-0 border-0 bg-transparent p-0 text-sm-start map-country"
            placeholder="Où allez-vous?"
          />
        </div>
        <div className="mr-2 mt-lg-0 relative">
          <DatePicker
            placeholderText="Arrivée"
            selected={hasTimeToShow(initial_date)}
            name="initial_date"
            onChange={(date) => setDate(date, 'start')}
            selectsStart
            startDate={hasTimeToShow(initial_date)}
            endDate={hasTimeToShow(final_date)}
            minDate={new Date()}
            dateFormat="dd/MM/yyyy"
            className={'map-time-input'}
            customInput={<CustomDatePicker />}
          />
        </div>
        <div className="mr-2 mt-lg-0">
          <DatePicker
            selected={hasTimeToShow(
              initial_date > final_date ? initial_date : final_date
            )}
            placeholderText="Départ"
            name="final_date"
            onChange={(date) => setDate(date, 'end')}
            selectsEnd
            startDate={hasTimeToShow(initial_date)}
            endDate={hasTimeToShow(final_date)}
            dateFormat="dd/MM/yyyy"
            minDate={hasTimeToShow(initial_date)}
            className={'map-time-input'}
            customInput={<CustomDatePicker />}
          />
        </div>
        <div className="mr-2 mt-3 mt-lg-0 position-relative">
          <input
            value={`${travellers_quantity.total} Voyageurs`}
            type="button"
            name="travellers_quantity"
            className="form-control bg-transparent border-0 px-1 w-100"
            placeholder="Qui?"
            min="0"
            onClick={() => setShowTravelers(!showTravelers)}
            id="travellers_quantity"
          />
          <div
            className={`${showTravelers ? 'd-flex' : 'd-none'}`}
            ref={domNode}
          >
            <ModalNumberTravelers show={showTravelers} />
          </div>
        </div>
      </div>
      <div className=" mt-lg-0">
        <button
          style={{ width: '2.5rem', height: '2.5rem' }}
          type="submit"
          disabled={!isPlaceSelected}
          className="btn btn-secondary rounded-circle p-0 map-button"
        >
          <SearchIcon width="1rem" height="1rem" />
        </button>
      </div>
    </form>
  )
}

export default Form
