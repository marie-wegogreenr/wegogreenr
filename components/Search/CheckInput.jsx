import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

function CheckInput({ name, handleChange = () => {}, id, categoryFilter }) {
  const inputRef = React.useRef()
  const [isChecked, setIsChecked] = React.useState(false)

  React.useEffect(() => {
    setIsChecked(categoryFilter.some((a) => a === inputRef.current?.value))
  }, [categoryFilter])

  return (
    <div className="col-12 col-md-6 col-lg-6 py-1">
      <div className="form-check d-flex align-items-center">
        <input
          ref={inputRef}
          name={name}
          className="form-check-input"
          defaultValue={id}
          type="checkbox"
          id={`${name}-${id}`}
          onChange={handleChange}
          checked={isChecked}
          style={{ width: '20px', height: '20px' }}
        />
        <label
          className="form-check-label fz-13 ms-1"
          htmlFor={`${name}-${id}`}
        >
          {name}
        </label>
      </div>
    </div>
  )
}

export default CheckInput
