import React, { useEffect } from 'react'
import { consoleLog } from '/utils/logConsole'

const index = () => {
  useEffect(() => {
    window.axeptioSettings = {
      clientId: '5c11ff5ce95cd64112feab79'
    }
    ;(function (d, s) {
      var t = d.getElementsByTagName(s)[0],
        e = d.createElement(s)
      e.async = true
      e.src = '//static.axept.io/sdk-slim.js'
      t.parentNode.insertBefore(e, t)
    })(document, 'script')
  }, [])

  return <></>
}

export default index
