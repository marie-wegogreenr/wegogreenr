import { formatDate } from 'utils'
import { useDispatch, useSelector } from 'react-redux'
import { isWithinInterval } from 'date-fns'
import moment from 'moment'
import ReactTooltip from 'react-tooltip'

const CustomHeader = ({ calendar, hebergement, customDays }) => {
  const { specialDates: cd } = useSelector((store) => store.hostCalendar)
  const isWeekendDay = calendar.getDay() === 5 || calendar.getDay() === 6
  const CustomDay = customDays.filter(
    (cd) => formatDate(cd.start) === formatDate(calendar)
  )
  let SpecialDate = null
  let overlapedDates = []

  cd.forEach((cd, index) => {
    if (
      isWithinInterval(calendar, {
        start: new Date(moment(cd.start_date).format('YYYY/MM/DD 00:00')),
        end: new Date(moment(cd.end_date).format('YYYY/MM/DD 00:00'))
      })
    ) {
      overlapedDates.push(cd)
      // SpecialDate = cd
    }
  })

  if (overlapedDates.length > 1) {
    const biggestDate = overlapedDates.reduce((first, second) =>
      first.created_at > second.created_at ? first : second
    )
    SpecialDate = biggestDate
  } else if (overlapedDates.length === 1) {
    SpecialDate = overlapedDates[0]
  }

  if (SpecialDate !== null) {
    const weekend_price =
      SpecialDate?.room_special_availability_price?.weekend_price ||
      SpecialDate?.room_special_availability_price?.price ||
      hebergement.weekend_price ||
      hebergement.basic_price

    const dayPrice =
      SpecialDate?.room_special_availability_price?.price ||
      hebergement.basic_price

    return (
      <>
        <p className="special-price-day" data-tip data-for={SpecialDate.title}>
          {isWeekendDay
            ? weekend_price.toString().replace('.', ',')
            : dayPrice.toString().replace('.', ',')}
          €
        </p>
        <ReactTooltip
          id={SpecialDate.title}
          type="dark"
          effect="solid"
          textColor="#FFF"
        >
          <span>{`Tarif personnalisé : ${SpecialDate.title}`}</span>
        </ReactTooltip>
      </>
    )
  } else {
    return (
      <p className="price-day">
        {isWeekendDay && hebergement.weekend_price
          ? hebergement.weekend_price.toString().replace('.', ',')
          : hebergement.basic_price?.toString().replace('.', ',')}
        €
      </p>
    )
  }
}

export default CustomHeader
