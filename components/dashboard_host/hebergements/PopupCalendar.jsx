const PopupCalendar = (props) => {
  const handleCloseDialog = () => {
    props.onCloseDialog()
  }

  return (
    <div className={`locationPopUp`}>
      <div onClick={handleCloseDialog} className="locationPopUp__close">
        X
      </div>
      <form className="wrapper-8 row form">
        <div className="form-group">
          {props.events.map((event) => {
            return (
              <ul>
                <ul>
                  <li>Nombre Evento:</li>
                  <li>Fecha Inicial:</li>
                  <li>Fecha Final:</li>
                </ul>
              </ul>
            )
          })}
        </div>
      </form>
    </div>
  )
}

export default PopupCalendar
