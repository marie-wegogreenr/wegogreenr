import React from 'react'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import Cookies from 'universal-cookie'
import SummaryCalendar from './SummaryCalendar'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStart } from '@fortawesome/free-solid-svg-icons'
import HebergementsCard from './HebergementsCard'
import { toast } from 'react-toastify'
class Summary extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showPopup: false,
      showPopupIcal: false,
      showPopupPrice: false,
      showPopupTarifs: false,
      availabilities: [],
      dates_not_availabities: []
    }

    library.add(faInfoCircle)
  }

  async componentDidMount() {
    this.handleInitialInfo()
    this.handleSortDates()
  }

  handleInitialInfo = async () => {
    this.setState(
      { ...this.state, availabilities: [], dates_not_availabities: [] },
      async () => {
        const cookies = new Cookies()
        //Get availabilities
        const res_availabilities = await fetch(
          process.env.NEXT_PUBLIC_API_URL +
            'availabilities/' +
            this.props.id_hebergement,
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'GET'
          }
        )
        const json_availabilities = await res_availabilities.json()
        this.setState({ ...this.state, availabilities: json_availabilities })
      }
    )
  }

  async handleSortDates() {
    this.setState({ ...this.state, dates_not_availabities: [] }, () => {
      let array_dates = []

      this.state.availabilities?.forEach((availability) => {
        let name
        if (availability.event_name) {
          name = availability.event_name
        } else if (availability.title) {
          name = availability.title
        } else {
          name = 'Reservation'
        }
        array_dates.push({
          date: availability.date,
          isChecked: true,
          type: availability.type,
          id: availability.id,
          name
        })
      })
      this.setState({
        ...this.state,
        dates_not_availabities: array_dates
      })
    })
  }

  handleChangeAvailability = () => {
    this.setState({ ...this.state, showPopup: !this.state.showPopup })
  }

  handleChangeIcal = () => {
    this.setState({ ...this.state, showPopupIcal: !this.state.showPopupIcal })
  }

  handleChangePrice = () => {
    this.setState({ ...this.state, showPopupPrice: !this.state.showPopupPrice })
  }

  handleChangeTarifs = () => {
    this.setState({
      ...this.state,
      showPopupTarifs: !this.state.showPopupTarifs
    })
  }

  render() {
    return (
      <>
        {this.props.id_tab !== 'calendar' && (
          <HebergementsCard
            hebergementData={this.props.data_hebergement}
            image={this.props.image}
          />
        )}
        {this.props.id_tab == 'calendar' && (
          <SummaryCalendar
            handleChangeAvailability={this.handleChangeAvailability}
            handleChangeIcal={this.handleChangeIcal}
            handleChangePrice={this.handleChangePrice}
            handleChangeTarifs={this.handleChangeTarifs}
            showPopup={this.state.showPopup}
            showPopupIcal={this.state.showPopupIcal}
            showPopupPrice={this.state.showPopupPrice}
            showPopupTarifs={this.state.showPopupTarifs}
            id_hebergement={this.props.id_hebergement}
            slug_hebergement={
              window !== undefined && window.location.href.includes('admin')
                ? this.props.data_hebergement.slug
                : this.props.data_hebergement[0].slug
            }
            dates_not_availabities={this.state.dates_not_availabities}
          />
        )}
      </>
    )
  }
}

export default Summary
