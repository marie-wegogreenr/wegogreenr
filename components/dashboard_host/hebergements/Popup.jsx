import {
  formatDate,
  formatMoment,
  formatSafari,
  getADayLater
} from 'helpers/timeHelper'
import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getArrayofDates } from 'helpers/calendarHelper'
import { setToAvailable, setToUnavailable } from 'services/hebergementsService'
import {
  changeToAvailable,
  changeToUnavailable
} from '@ducks/hostCalendar/actions'
import { useToast } from 'hooks/useToast'
import moment from 'moment'
/* Dependencies */
import { eachDayOfInterval } from 'date-fns'
import DatePicker from 'react-datepicker'
import { CustomNormalDatePicker } from '@components/customInputs/CustomNormalDatePicker'
import { registerLocale, setDefaultLocale } from 'react-datepicker'
import fr from 'date-fns/locale/fr'
registerLocale('fr', fr)
setDefaultLocale('fr')

const Popup = (props) => {
  const [notify] = useToast()
  const unavailableDays = useSelector(
    (store) => store.hostCalendar.unavailableDays
  )
  const dispatch = useDispatch()

  const [inputs, setInputs] = useState({
    initalDate: formatSafari(new Date()),
    endDate: formatSafari(new Date()),
    isDisponible: null
  })

  const updateAvailability = () => {
    const start = new Date(inputs.initalDate)
    const end = new Date(inputs.endDate)

    if (start > end) {
      return notify('La date min ne peut pas être une date max à max.', 'error')
    }
    if (inputs.isDisponible === null) {
      return notify('Choisissez le type de disponibilités', 'warning')
    }

    const arrayDates = eachDayOfInterval({ start, end })

    if (arrayDates.length >= 60) {
      return notify(
        'Le nombre de jours sélectionnés dépasse la limite par demande, veuillez sélectionner moins de 60 jours',
        'warning'
      )
    }

    const unavailableDaysFiltered = unavailableDays.filter(
      (day) => day.type === 1
    )

    arrayDates.forEach((date) => {
      const dayTochange = unavailableDaysFiltered.find(
        (udf) =>
          moment(udf.start).format('YYYY-MM-DD') ===
          moment(date).format('YYYY-MM-DD')
      )
      let classification = 0

      if (formatMoment(date) === formatMoment(start)) {
        classification = 1
      } else if (formatMoment(date) === formatMoment(end)) {
        classification = 3
      } else {
        classification = 2
      }

      const body = {
        room_id: props.room_id,
        date: formatMoment(date),
        classification
      }

      if (inputs.isDisponible && typeof dayTochange !== 'undefined') {
        //here change from unavailable to available
        dispatch(changeToAvailable(body, dayTochange.id))
      } else if (!inputs.isDisponible && typeof dayTochange === 'undefined') {
        //here change from available to unavailable
        dispatch(changeToUnavailable(body))
      }
    })
    notify('Les disponibilités sont modifiées avec succès', 'success')
    props.onCloseDialog()
  }

  return (
    <div className={`locationPopUp`}>
      <div onClick={props.onCloseDialog} className="locationPopUp__close">
        X
      </div>
      <form className="wrapper-8 row form">
        <div className="form-group modalPriceContent___dates">
          <h3
            className="font-montserrat col-12 weight-bold mb-3 text-center"
            style={{ color: 'black', fontSize: '22px' }}
          >
            Mettre à jour le calendrier manuellement
          </h3>
          <div className="input-group mb-3 col-12">
            <div className="input-group-prepend">
              <div
                className="input-group-text bg-white color-black"
                style={{ border: '1px solid rgb(206, 212, 218)' }}
              >
                Début
              </div>
            </div>
            <DatePicker
              className="form-control date-inputs rounded-right modalPriceContent__date-picker"
              selected={new Date(inputs.initalDate)}
              minDate={formatSafari(new Date())}
              onChange={(date) =>
                setInputs({ ...inputs, initalDate: formatSafari(date) })
              }
              dateFormat="dd/MM/yyyy"
              customInput={<CustomNormalDatePicker />}
            />
          </div>
          <div className="input-group mb-3 col-12">
            <div className="input-group-prepend">
              <div
                className="input-group-text bg-white color-black"
                style={{ border: '1px solid rgb(206, 212, 218)' }}
              >
                Fin
              </div>
            </div>
            <DatePicker
              className="form-control date-inputs rounded-right modalPriceContent__date-picker"
              selected={new Date(inputs.endDate)}
              minDate={formatSafari(new Date())}
              onChange={(date) =>
                setInputs({ ...inputs, endDate: formatSafari(date) })
              }
              dateFormat="dd/MM/yyyy"
              customInput={<CustomNormalDatePicker />}
            />
          </div>
          <div className="col-12">
            <div className="row">
              <div className="form-check mt-3 col-12">
                <input
                  className="form-check-input"
                  type="radio"
                  name="type_availability"
                  id="type_availability_1"
                  value="1"
                  style={{ marginLeft: 0 }}
                  onChange={(e) => setInputs({ ...inputs, isDisponible: true })}
                />
                <label
                  htmlFor="type_availability_1"
                  style={{ marginLeft: '24px' }}
                >
                  Disponible
                </label>
              </div>
              <div className="form-check mt-3 col-12">
                <input
                  className="form-check-input"
                  type="radio"
                  name="type_availability"
                  id="type_no_availability_2"
                  value="2"
                  style={{ marginLeft: 0 }}
                  onChange={(e) =>
                    setInputs({ ...inputs, isDisponible: false })
                  }
                />
                <label
                  htmlFor="type_no_availability_2"
                  style={{ marginLeft: '24px' }}
                >
                  Non disponible
                </label>
              </div>
            </div>
          </div>

          <div className="row mt-5">
            <div className=" col-md-6 col-xl-6">
              <button
                className="button button-outline-black"
                onClick={props.onCloseDialog}
              >
                Annuler
              </button>
            </div>
            <div className=" col-md-6 col-xl-6 justify-content-end d-flex">
              <button
                type="button"
                onClick={updateAvailability}
                className="button button-long"
              >
                Sauvegarder
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  )
}

export default Popup
