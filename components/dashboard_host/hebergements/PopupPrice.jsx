import SummaryPrice from './SummaryPrice'

const PopupPrice = (props) => {
  const handleCloseDialog = () => {
    props.onCloseDialog()
  }

  return (
    <div className={`locationPopUp`}>
      <div onClick={handleCloseDialog} className="locationPopUp__close">
        X
      </div>
      <div className="wrapper-8 row form">
        <div className="form-group">{props.component}</div>
      </div>
    </div>
  )
}

export default PopupPrice
