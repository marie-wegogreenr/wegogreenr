import SummaryIcal from './SummaryIcal'

const PopupIcal = (props) => {
  const handleCloseDialog = () => {
    props.onCloseDialog()
  }

  return (
    <div className={`locationPopUp`}>
      <div onClick={handleCloseDialog} className="locationPopUp__close">
        X
      </div>
      <div className="wrapper-8 row form">
        <div className="form-group">
          <SummaryIcal
            room_id={props.room_id}
            slug_id={props.slug_id}
            onClosePopup={handleCloseDialog}
          />
        </div>
      </div>
    </div>
  )
}

export default PopupIcal
