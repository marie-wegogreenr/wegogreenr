import React from 'react'
import { connect } from 'react-redux'
import 'react-calendar/dist/Calendar.css'
import PopupCalendar from './PopupCalendar'
import { CustomDatePicker } from '@components/customInputs/CustomDatePicker'
import DatePicker from 'react-datepicker'
import { Calendar as BigCalendar, momentLocalizer } from 'react-big-calendar'

import moment from 'moment'
import ApiCalendar from 'react-google-calendar-api'
import { Loader } from 'components/'
import CustomHeader from './CustomHeader'
import { setInitialCalendar } from '@ducks/hostCalendar/actions'
import { CustomMonthPicker } from '@components/customInputs/CustomMonthPicker'
/* Dependencies */
import { registerLocale, setDefaultLocale } from 'react-datepicker'
import fr from 'date-fns/locale/fr'
registerLocale('fr', fr)
setDefaultLocale('fr')

class CalendarHebergement extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      availabilities: [],
      calendar_value: moment(),
      editMode: true,
      showPopup: false,
      showIframeCalendar: true,
      calendar_id: null,
      actual_date: new Date(),
      localizer: momentLocalizer(moment),
      showModal: false,
      events: []
    }

    this.handleChangeMonth = this.handleChangeMonth.bind(this)
    this.handleGetEvents = this.handleGetEvents.bind(this)
    this.handleCreateEvent = this.handleCreateEvent.bind(this)
  }

  async componentDidMount() {
    moment.locale('fr')
    this.props.setInitialCalendar(this.props.id)
  }

  async handleChangeMonth(event) {
    this.setState({ ...this.state, actual_date: event })
  }

  async handleCreateEvent(event) {
    event.preventDefault()
    this.setState({ ...this.state, showIframeCalendar: false })
    let timeZone = 'Europe/Paris'

    const event_google_calendar = {
      summary: event.target.event_name.value,
      description: event.target.event_description.value,
      start: {
        dateTime: new Date(event.target.start_date.value).toISOString(),
        timeZone: timeZone
      },
      end: {
        dateTime: new Date(event.target.end_date.value),
        timeZone: timeZone
      }
    }

    const response = await ApiCalendar.createEvent(event_google_calendar)
    if (response !== false) {
      //consoleLog(response)
      toast.success('Event Événement créé', {
        position: toast.POSITION.BOTTOM_LEFT
      })

      event.target.value = null
      this.setState({ ...this.state, showIframeCalendar: true })
    }
  }

  async handleGetEvents() {
    ApiCalendar.listUpcomingEvents(10).then((response) => {
      //consoleLog(response.result.items)
    })
  }

  eventStyleGetter = (event, start, end, isSelected) => {
    var backgroundColor = event.color
    var style = {
      backgroundColor: backgroundColor,
      borderRadius: '10px',
      opacity: 0.8,
      //color: 'black',
      border: '0px',
      display: 'block'
    }
    return {
      style: style
    }
  }

  rendenCalendarItem(calendarIten) {
    const { unavailableDays } = this.props.hostCalendar
    const current_date = moment().toDate()
    if (
      unavailableDays.some(
        (date) =>
          moment(date.start).format('YYYY-MM-DD 00:00:00') ===
            moment(calendarIten).format('YYYY-MM-DD 00:00:00') && date.type == 1
      )
    ) {
      return 'date-disabled'
    } else if (
      unavailableDays.some(
        (date) =>
          moment(date.start).format('YYYY-MM-DD 00:00:00') ===
            moment(calendarIten).format('YYYY-MM-DD 00:00:00') &&
          date.type === 2
      )
    ) {
      return 'date-price-custom'
    } else if (moment(calendarIten) < current_date) {
      return 'date-disabled'
    }
  }

  render() {
    const {
      hebergement,
      hostCalendar: { customDays, isDataLoaded }
    } = this.props

    return (
      <div className="row mt-4">
        <div className="col-12">
          <form className="form mb-3">
            <div className="row">
              <div className="col-12">
                <div className="card-white calendar-room__calendar-box">
                  <div className="row calendar-room__calendar-box-wrapper">
                    <div className="col-12 col-lg-6 calendar-room__dates">
                      <DatePicker
                        selected={this.state.actual_date}
                        onChange={this.handleChangeMonth}
                        dateFormat="MM/yyyy"
                        closeOnScroll={true}
                        showMonthYearPicker
                        className={'calendar-room__dates-input'}
                        customInput={<CustomMonthPicker />}
                      />
                    </div>
                    <div className="col-12 col-lg-6 calendar-room__conventions">
                      <div className="calendar-room__conventions-box">
                        <div className="col-12">
                          <div className="row">
                            <h3
                              className="font-montserrat col-12 weight-bold mb-3  calendar-room__conventions-title"
                              style={{ color: 'black', fontSize: '22px' }}
                            >
                              Légende
                            </h3>
                            <div className="col-12 mb-2 calendar-room__conventions-option">
                              <div className="box box-not-availabilitie calendar-room__conventions-option-icon">
                                <div className="line-box ml-1"></div>
                              </div>
                              Indisponible{' '}
                            </div>
                            <div className="col-12 mb-2 calendar-room__conventions-option">
                              <div className="box box-availabilitie calendar-room__conventions-option-icon"></div>
                              Disponible
                            </div>
                            <div className="col-12 mb-2 calendar-room__conventions-option">
                              <div className="box box-reservation calendar-room__conventions-option-icon"></div>
                              Réservé via We Go GreenR
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    {isDataLoaded ? (
                      <>
                        <p className="col-12" style={{ margin: 0 }}>
                          {moment(this.state.actual_date).format('MMM YYYY')}
                        </p>
                        <div
                          className="col-12"
                          style={{ height: '700px', padding: '15px' }}
                        >
                          <BigCalendar
                            defaultView="month"
                            toolbar={false}
                            date={this.state.actual_date}
                            localizer={this.state.localizer}
                            events={customDays}
                            startAccessor="start"
                            endAccessor="end"
                            popup={true}
                            onShowMore={(events, date) =>
                              this.setState({ showModal: true, events })
                            }
                            eventPropGetter={this.eventStyleGetter}
                            components={{
                              month: {
                                dateHeader: (props) => {
                                  return <p>{props.label}</p>
                                }
                              },
                              dateCellWrapper: ({ children, value }) => {
                                const styles = this.rendenCalendarItem(value)

                                return (
                                  <div className={`rbc-day-bg ${styles}`}>
                                    <CustomHeader
                                      hebergement={hebergement}
                                      calendar={value}
                                      customDays={customDays}
                                    />
                                  </div>
                                )
                              }
                            }}
                          />
                        </div>
                        {this.state.showModal && (
                          <PopupCalendar
                            onCloseDialog={this.setState({ showModal: false })}
                            events={this.state.events}
                          />
                        )}
                      </>
                    ) : (
                      <Loader />
                    )}
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { hebergement: state.hebergement, hostCalendar: state.hostCalendar }
}

const mapDispatchToProps = {
  setInitialCalendar
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarHebergement)
