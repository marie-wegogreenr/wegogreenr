import * as React from 'react'
import { connect } from 'react-redux'

import { Loader } from 'components/'
import { cleanHebergement } from '../../../redux/ducks/hebergements/actions'
import EditPriceInput from '@components/host/dashboard/EditPriceInput'
import EditNuitsInput from '@components/host/dashboard/EditNuitsInput'
import EditDiscountInput from '@components/host/dashboard/EditDiscountInput'
import EditManualReservation from '@components/host/dashboard/EditManualReservation'
import EditPricePerson from '@components/host/dashboard/EditPricePerson'
import EditGiftCardInput from '@components/host/dashboard/EditGiftCardInput'

class Prix extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      editMode: false
    }
  }

  componentWillUnmount() {
    this.props.cleanHebergement()
  }

  render() {
    const { isLoading } = this.props.hebergement
    if (isLoading) {
      return <Loader />
    }
    return (
      <>
        <EditPriceInput
          title={'Prix base'}
          name={'basic_price'}
          subtitle={'Le prix par défaut d’une nuitée dans votre hébergement.'}
          help={
            <>
              <p className="form-room__prix-text">
                Tarif payé par les voyageurs (taxes, commission et frais
                compris).
              </p>
              <p className="form-room__prix-text">
                Indiquez votre prix “bas” (souvent un jour de semaine en basse
                saison).
              </p>
            </>
          }
        />
        <EditPriceInput
          title={'Prix week-end (optionnel)'}
          name={'weekend_price'}
          subtitle={
            'Prix par nuit, du vendredi au samedi, et du samedi au dimanche.'
          }
        />
        <EditGiftCardInput
          title="Carte cadeau"
          description="Vous pouvez activer la carte cadeau pour cette hebergement"
        />
        <EditPricePerson
          title={'Autres frais'}
          name={'extra_people_price'}
          subtitle={
            'Facturez ce montant par voyageur supplémentaire et par nuit'
          }
        />
        <EditNuitsInput />
        <EditDiscountInput />
        <EditManualReservation />
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return { hebergement: state.hebergement }
}

const mapDispatchToProps = {
  cleanHebergement
}

export default connect(mapStateToProps, mapDispatchToProps)(Prix)
