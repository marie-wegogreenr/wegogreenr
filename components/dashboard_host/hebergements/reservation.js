import React, { Component } from 'react'
import { toast } from 'react-toastify'
/* Components */
import { SectionLayout } from 'components/host/establishment'

/*Libraries */
import Cookies from 'universal-cookie'

class Reservation extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: null,
      editMode: false,
      optionSelected: '',
      rooms: []
    }

    this.handleEditMode = this.handleEditMode.bind(this)
    this.handleReservation = this.handleReservation.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    this.setState({ ...this.state, id: this.props.id })
    //Get rooms
    const res_room = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'rooms/' + this.props.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_rooms = await res_room.json()
    this.setState({
      ...this.state,
      rooms: json_rooms,
      optionSelected: json_rooms[0].reservation_mode
    })
  }

  handleEditMode() {
    this.setState({ ...this.state, editMode: !this.state.editMode })
  }

  async handleReservation(e) {
    e.preventDefault()
    const cookies = new Cookies()
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'rooms/' + this.state.id, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'PUT',
      body: JSON.stringify({
        active: this.state.rooms[0].active,
        internal_name: this.state.rooms[0].internal_name,
        public_name: this.state.rooms[0].public_name,
        description: this.state.rooms[0].description,
        type_id: this.state.rooms[0].type_id,
        host_quantity: this.state.rooms[0].host_quantity,
        total_area: this.state.rooms[0].total_area,
        rooms_quantity: this.state.quantityRooms,
        people_capacity: this.state.quantityTravelers,
        bathroom_quantity: this.state.quantityBathroom,
        strengths: this.state.rooms[0].strengths,

        establishment_id: this.state.rooms[0].establishment_id,
        basic_price: this.state.rooms[0].basic_price,
        weekend_price: this.state.rooms[0].weekend_price,
        min_nigth: this.state.rooms[0].min_nigth,
        max_nigth: this.state.rooms[0].max_nigth,
        seven_days_discount: this.state.rooms[0].seven_days_discount,
        month_discount: this.state.rooms[0].month_discount,
        slug: this.state.rooms[0].slug,
        reservation_mode: this.state.optionSelected
      })
    }).then(() => {
      toast.success('Réservation mise à jour', {
        position: toast.POSITION.BOTTOM_LEFT
      })
      this.handleEditMode()
    })
  }

  handleOptionSelected = (e) => {
    this.setState({
      ...this.state,
      optionSelected: e.target.value
    })
  }

  render() {
    return (
      <form className="form mb-3" onSubmit={this.handleReservation}>
        <SectionLayout
          isModifyMode={this.state.editMode}
          onModify={this.handleEditMode}
          title="Type reservation"
        >
          <div className="card card-body">
            <div className="row">
              <div className="col-12">
                <h3>Types de reservations</h3>
              </div>
              <div className="form-check mt-3 col-6">
                <div className="row">
                  <div className="col-6">
                    <label htmlFor="type_reservation">
                      Reservation Automatique
                    </label>
                  </div>
                  <div className="col-6" style={{ textAlign: 'end' }}>
                    <input
                      className="form-check-input"
                      type="radio"
                      name="type_reservation"
                      id="type_reservation"
                      value="0"
                      onChange={this.handleOptionSelected}
                      disabled={!this.state.editMode}
                      checked={this.state.optionSelected == 0}
                      required
                    />
                  </div>
                </div>
              </div>
              <div className="form-check mt-3 col-6">
                <div className="row">
                  <div className="col-6">
                    <label htmlFor="type_reservation">
                      Reservation Manuelle
                    </label>
                  </div>
                  <div className="col-6" style={{ textAlign: 'end' }}>
                    <input
                      className="form-check-input"
                      type="radio"
                      name="type_reservation"
                      id="type_no_reservation"
                      value="1"
                      onChange={this.handleOptionSelected}
                      disabled={!this.state.editMode}
                      checked={this.state.optionSelected == 1}
                    />
                  </div>
                </div>
              </div>
            </div>
            {this.state.editMode && (
              <button type="submit" className="btn btn-secondary">
                Sauvegarder
              </button>
            )}
          </div>
        </SectionLayout>
      </form>
    )
  }
}

export default Reservation
