import { useState, useEffect } from 'react'
import Cookies from 'universal-cookie'
import axios from 'axios'
import moment from 'moment'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons'
import Loader from '@components/Loader'
import { useDispatch, useSelector } from 'react-redux'
import { ifDateInArray } from 'helpers/calendarHelper'
import { formatDate, formatSafari } from '../../../helpers/timeHelper'
import { eachDayOfInterval } from 'date-fns'
import {
  addRangeSpecialPrice,
  deleteSpecialPrice,
  updateRangeSpecialPrice
} from '@ducks/hostCalendar/actions'
import { useToast } from 'hooks/useToast'
import {
  adjustNumberWithComma,
  convertPrices,
  inputOnlyNumberComma,
  validatePrices
} from 'helpers/utilsHelper'
/* Dependencies */
import DatePicker from 'react-datepicker'
import { CustomNormalDatePicker } from '@components/customInputs/CustomNormalDatePicker'
import { registerLocale, setDefaultLocale } from 'react-datepicker'
import fr from 'date-fns/locale/fr'
registerLocale('fr', fr)
setDefaultLocale('fr')

const PopupTarifs = (props) => {
  const dispatch = useDispatch()
  const { customDays, specialDates } = useSelector(
    (store) => store.hostCalendar
  )

  const [fields, setFields] = useState({
    min_night: 0,
    max_night: 0,
    title: '',
    price: null,
    weekend_price: null,
    seven_days_discount: null,
    startDate: formatSafari(new Date()),
    endDate: formatSafari(new Date()),
    isWeekEndPriceEnable: false,
    isNuitsEnable: false,
    isMonthDiscountEnable: false,
    isWeekDiscountEnable: false,
    specialPrices: [],
    showSpecialPrices: true,
    idItemToEdit: null
  })
  const [pricesValues, setPricesValues] = useState({
    price: '',
    weekend_price: ''
  })
  const [notify] = useToast()
  useEffect(() => {
    getSpecialPrices()
  }, [])

  const getSpecialPrices = async () => {
    const specialPrices = customDays.filter((day) => day.type === 4)
    const specialPricesNames = [
      ...new Map(specialPrices.map((item) => [item['title'], item])).values()
    ]

    let map = specialPricesNames
      .map((spn) => {
        return {
          name: spn.title,
          price: spn.price,
          dates: specialPrices
            .filter((sp) => sp.title === spn.title)
            .sort((a, b) => new Date(b.start) - new Date(a.start))
        }
      })
      .map((spn) => {
        return {
          name: spn.name,
          price: spn.price,
          final_date: formatDate(spn.dates[0].start),
          initial_date: formatDate(spn.dates[spn.dates.length - 1].start)
        }
      })
    setFields({ ...fields, specialPrices: map })
  }
  const handleUpdatepecialPrice = (id) => {
    document.getElementsByClassName('locationPopUp')[0].scrollTo(0, 0)
    const specialDate = specialDates.find((sd) => sd.id === id)
    const { room_special_availability_price } = specialDate
    const dataToEdit = {
      min_night: room_special_availability_price.min_nigth,
      max_night: room_special_availability_price.max_nigth,
      title: specialDate.title,
      price: room_special_availability_price.price,
      startDate: specialDate.start_date,
      endDate: specialDate.end_date,
      isWeekEndPriceEnable:
        typeof room_special_availability_price.weekend_price === null,
      isWeekDiscountEnable:
        typeof room_special_availability_price.seven_days_discount !== null,
      seven_days_discount: room_special_availability_price.seven_days_discount,
      weekend_price: room_special_availability_price.weekend_price,
      idItemToEdit: room_special_availability_price.id
    }

    setFields({ ...fields, ...dataToEdit })

    setPricesValues({
      price: room_special_availability_price.price,
      weekend_price: room_special_availability_price.weekend_price || 0
    })
    $('#collapseExample').collapse('show')
  }

  const closeCollapse = (e) => {
    e.preventDefault()
    $('#collapseExample').collapse('toggle')
  }

  const handlePopupPrice = (e) => {
    e.preventDefault()
    const valid = validatePrices(
      pricesValues.price.toString(),
      pricesValues.weekend_price.toString()
    )

    if (!valid) {
      notify(
        `Corrigez les prix. Vous ne pouvez pas écrire plus d'une virgule.`,
        'error'
      )
      return
    }

    let start = formatSafari(fields.startDate)

    let end = formatSafari(fields.endDate)

    if (start > end) {
      return notify('erreur dans la date sélectionnée', 'error')
    }

    const dates = eachDayOfInterval({ start, end })

    const reservationDates = customDays.filter(
      (cd) => cd.type === 2 || cd.type === 3
    )

    const specialPricesDates = customDays.filter((cd) => cd.type === 4)

    // if (ifDateInArray(reservationDates, dates)) {
    //   notify(`Il y a des réservations dans cette fourchette de dates`, 'error')
    //   props.onClosePopup()
    //   return
    // }

    if (ifDateInArray(specialPricesDates, dates)) {
      notify(
        `Un tarif personnalisé est déjà rentré pour l'une de ces dates`,
        'error'
      )
      props.onClosePopup()
      return
    }
    if (parseInt(fields.min_night) > parseInt(fields.max_night)) {
      notify(`erreur dans le nombre de nuits minimum et maximum`, 'error')
      return
    }

    let _basePrice, _weekendPrice

    if (pricesValues.weekend_price) {
      const { priceBaseFormatted, priceWeekendFormatted } = convertPrices(
        pricesValues.price.toString(),
        pricesValues.weekend_price.toString()
      )

      _basePrice = priceBaseFormatted
      _weekendPrice = priceWeekendFormatted
    } else {
      _basePrice = convertPrices(pricesValues.price)
    }

    const body = {
      id: fields.idItemToEdit,
      title: e.target.name_custom.value,
      start_date: moment(fields.startDate).format('YYYY-MM-DD'),
      end_date: moment(fields.endDate).format('YYYY-MM-DD'),
      price: _basePrice || '',
      weekend_price: _weekendPrice || '',
      min_nigth: fields.min_night,
      max_nigth: fields.max_night
    }
    dispatch(updateRangeSpecialPrice(body))

    // notify('Prix personnalisé created', 'success')
    props.onClosePopup()
  }

  const handleDeleteSpecialPrice = async (id) => {
    dispatch(deleteSpecialPrice(id))
    props.onClosePopup()
    notify('prix personnalisé éliminé', 'success')
  }

  const handlePriceChange = (e) => {
    const isNumberWithComma = inputOnlyNumberComma(e.target.value)
    if (isNumberWithComma) {
      setPricesValues({
        ...pricesValues,
        [e.target.name]: e.target.value
      })
    } else {
      setPricesValues({
        ...pricesValues,
        [e.target.name]: e.target.value.slice(0, -1)
      })
    }
  }

  return (
    <div className="modalPriceContent">
      <div
        className="modalPriceContent___addNewForm collapse"
        id="collapseExample"
      >
        <form className="" onSubmit={handlePopupPrice}>
          <div className="">
            <div className="mb-3">
              <b className="text-block weight-bold">Dates</b>
            </div>
            <div className="modalPriceContent___dates">
              <div className="input-group mb-3 ">
                <div className="input-group-prepend">
                  <div
                    className="input-group-text bg-white color-black"
                    style={{ border: '1px solid rgb(206, 212, 218)' }}
                  >
                    Du:
                  </div>
                </div>
                <DatePicker
                  className="form-control date-inputs rounded-right modalPriceContent__date-picker"
                  selected={new Date(fields.startDate)}
                  onChange={(date) =>
                    setFields({ ...fields, startDate: formatSafari(date) })
                  }
                  dateFormat="dd/MM/yyyy"
                  customInput={<CustomNormalDatePicker />}
                  required
                />
              </div>
              <div className="input-group mb-4 ">
                <div className="input-group-prepend">
                  <div
                    className="input-group-text bg-white color-black"
                    style={{ border: '1px solid rgb(206, 212, 218)' }}
                  >
                    Au:
                  </div>
                </div>
                <DatePicker
                  className="form-control date-inputs rounded-right modalPriceContent__date-picker"
                  selected={new Date(fields.endDate)}
                  onChange={(date) =>
                    setFields({ ...fields, endDate: formatSafari(date) })
                  }
                  dateFormat="dd/MM/yyyy"
                  customInput={<CustomNormalDatePicker />}
                  required
                />
              </div>
            </div>

            <div className=" mb-2">
              <hr style={{ height: '0px' }} />
            </div>
            <div className=" mb-3">
              <b className="text-block weight-bold">Nom personnalisé</b>
            </div>
            <div className=" col-md-12 mb-4">
              <div className="input-group input-group-lg rounded">
                <input
                  type="text"
                  defaultValue={fields.title}
                  className="form-control border text-center"
                  id="name_custom"
                  name="name_custom"
                  placeholder=""
                  required
                />
              </div>
            </div>

            <div className=" mb-2">
              <hr style={{ height: '0px' }} />
            </div>
            <div className=" mb-3">
              <b className="text-block weight-bold">Prix de base</b>
            </div>
            <div className=" col-md-12 mb-4">
              <div className="input-group input-group-lg mb-2 rounded">
                <input
                  type="text"
                  onWheel={(e) => e.target.blur()}
                  className="form-control border text-center"
                  defaultValue={fields.price}
                  id="price"
                  name="price"
                  placeholder=""
                  min="0"
                  step=".01"
                  required
                  onChange={handlePriceChange}
                  value={pricesValues.price}
                />
                <div className="input-group-prepend">
                  <label htmlFor="price" className="input-group-text border-0">
                    / nuit
                  </label>
                </div>
              </div>
              <p className="text-justify">
                Ce sera le prix à la nuitée de votre hébergement du{' '}
                <b className="weight-bold">
                  {moment(fields.startDate).format('DD MMM')} au{' '}
                  {moment(fields.endDate).format('DD MMM')}
                </b>
              </p>
            </div>

            <div className=" mb-2">
              <hr style={{ height: '0px' }} />
            </div>
            <div className=" mb-3">
              <div className="row">
                <div className="col-6">
                  <b className="text-block weight-bold">Prix week-end</b>
                </div>
                <div className="col-6" style={{ textAlign: 'end' }}>
                  <div className="custom-control custom-switch">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="prix-weekend-switch"
                      checked={fields.isWeekEndPriceEnable}
                    />
                    <label
                      className="custom-control-label ml-2"
                      htmlFor="prix-switch"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        setFields({
                          ...fields,
                          isWeekEndPriceEnable: !fields.isWeekEndPriceEnable
                        })
                      }}
                    ></label>
                  </div>
                </div>
                <div className="">
                  <p>
                    Du vendredi au samedi, et du samedi au dimanche (prix par
                    nuit)
                  </p>
                </div>
              </div>
            </div>
            <div className=" col-md-12 mb-4">
              <div className="input-group input-group-lg mb-2 rounded">
                <input
                  type="text"
                  onWheel={(e) => e.target.blur()}
                  defaultValue={fields.weekend_price}
                  className="form-control border text-center"
                  id="weekend_price"
                  name="weekend_price"
                  placeholder=""
                  min="0"
                  disabled={!fields.isWeekEndPriceEnable}
                  required={fields.isWeekEndPriceEnable}
                  onChange={handlePriceChange}
                  value={pricesValues.weekend_price}
                />
                <div className="input-group-prepend">
                  <label
                    htmlFor="weekend_price"
                    className="input-group-text border-0"
                  >
                    / nuit
                  </label>
                </div>
              </div>
            </div>

            <div className=" mb-2">
              <hr style={{ height: '0px' }} />
            </div>
            <div className=" mb-3">
              <div className="row">
                <div className="col-6">
                  <b className="text-block weight-bold">
                    Nuits minimum et maximum
                  </b>
                </div>
                <div className="col-6" style={{ textAlign: 'end' }}>
                  <div className="custom-control custom-switch">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="nuits-switch"
                      checked={fields.isNuitsEnable}
                    />
                    <label
                      className="custom-control-label ml-2"
                      htmlFor="prix-switch"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        setFields({
                          ...fields,
                          isNuitsEnable: !fields.isNuitsEnable
                        })
                      }}
                    ></label>
                  </div>
                </div>
              </div>
            </div>
            <div className="">
              <div className=" col-xl-12 card-button-nights mt-3">
                <div className="row">
                  <div className="col-8 mt-3 input-night">
                    <input
                      type="number"
                      onWheel={(e) => e.target.blur()}
                      value={fields.min_night}
                      className="number-styled-input"
                      disabled={!fields.isNuitsEnable}
                      onChange={(e) => {
                        setFields({
                          ...fields,
                          min_night: e.target.value
                        })
                      }}
                    />
                    <p className="ml-2">{`${
                      fields.min_night >= 2 ? 'Nuits' : 'Nuit'
                    } minimum`}</p>
                  </div>
                  <div className="col-4 buttons-night">
                    <button
                      className="button-left"
                      type="button"
                      name="nights_min"
                      onClick={(e) => {
                        let input = fields.min_night
                        if (input > 0) input = input -= 1
                        setFields({
                          ...fields,
                          min_night: input
                        })
                      }}
                      disabled={!fields.isNuitsEnable}
                    >
                      -
                    </button>
                    <button
                      className="button-right"
                      type="button"
                      name="nights_min"
                      onClick={(e) => {
                        let input = fields.min_night
                        input = input += 1
                        setFields({
                          ...fields,
                          min_night: input
                        })
                      }}
                      disabled={!fields.isNuitsEnable}
                    >
                      +
                    </button>
                  </div>
                </div>
              </div>
              <div className=" col-xl-12 card-button-nights mt-3">
                <div className="row">
                  <div className="col-8 mt-3 input-night">
                    <input
                      type="number"
                      onWheel={(e) => e.target.blur()}
                      value={fields.max_night}
                      className="number-styled-input"
                      disabled={!fields.isNuitsEnable}
                      onChange={(e) => {
                        setFields({
                          ...fields,
                          max_night: e.target.value
                        })
                      }}
                    />
                    <p className="ml-2">
                      {`${fields.max_night >= 2 ? 'Nuits' : 'Nuit'} maximum`}
                    </p>
                  </div>
                  <div className="col-4 buttons-night">
                    <button
                      className="button-left"
                      type="button"
                      name="nights_max"
                      onClick={(e) => {
                        let input = fields.max_night
                        if (input > 0) input = input -= 1
                        setFields({
                          ...fields,
                          max_night: input
                        })
                      }}
                      disabled={!fields.isNuitsEnable}
                    >
                      -
                    </button>
                    <button
                      className="button-right"
                      type="button"
                      name="nights_max"
                      onClick={(e) => {
                        let input = fields.max_night
                        input = input += 1
                        setFields({
                          ...fields,
                          max_night: input
                        })
                      }}
                      disabled={!fields.isNuitsEnable}
                    >
                      +
                    </button>
                  </div>
                </div>
              </div>
            </div>

            <div className="row mt-5">
              <div className=" col-md-6 col-xl-6">
                <button
                  className="button button-outline-black"
                  onClick={(e) => {
                    closeCollapse(e)
                  }}
                >
                  Annuler
                </button>
              </div>
              <div className=" col-md-6 col-xl-6 justify-content-end d-flex">
                <button type="submit" className="button button-long">
                  Sauvegarder
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <h3
        className="font-montserrat col-12 weight-bold mb-3 text-center mt-5"
        style={{ color: 'black', fontSize: '22px' }}
      >
        Mes tarifs spéciaux
      </h3>
      <div class="card card-body">
        {specialDates.map((price) => {
          return (
            <div className="row mb-3">
              <div className="col-10">
                <p>
                  <b>Nom :</b> {price.title}
                </p>
                <p>
                  <b>Prix :</b>{' '}
                  {adjustNumberWithComma(
                    price.room_special_availability_price.price
                  )}{' '}
                  €
                </p>
                {fields.showSpecialPrices ? (
                  <p>
                    <b> Date : </b>
                    {moment(price.start_date).format('DD/MM/YYYY')} -{' '}
                    {moment(price.end_date).format('DD/MM/YYYY')}
                  </p>
                ) : (
                  <Loader />
                )}
              </div>
              <div className="col-2 tarifs__actions">
                <FontAwesomeIcon
                  icon={faEdit}
                  onClick={() => handleUpdatepecialPrice(price.id)}
                />
                <FontAwesomeIcon
                  icon={faTrashAlt}
                  onClick={() => handleDeleteSpecialPrice(price.id)}
                />
              </div>
              <div className="col-12">
                <hr />
              </div>
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default PopupTarifs
