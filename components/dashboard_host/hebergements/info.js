import * as React from 'react'
import { toast } from 'react-toastify'
import { SectionLayout } from 'components/host/establishment'
import { Loader } from 'components/'
import { getRoomTypes, updateHebergement } from 'services/hebergementsService'
import EditInput from '@components/host/dashboard/EditInput'
import { connect } from 'react-redux'
import { updateHebergementProp } from '@ducks/hebergements/actions'

const Info = ({ hebergement, updateHebergementProp }) => {
  const [roomTypes, setRoomTypes] = React.useState([])
  const [editMode, setEditMode] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(false)
  const [generalInformation, setGeneralInformation] = React.useState({})
  React.useEffect(async () => {
    const resp = await getRoomTypes()
    setRoomTypes(resp)
  }, [])
  React.useEffect(() => {
    setGeneralInformation({
      type_id: hebergement.type_id,
      total_area: hebergement.total_area,
      host_quantity: hebergement.host_quantity
    })
  }, [hebergement])

  const updateGeneralInformation = async () => {
    if (
      generalInformation.type_id === '' ||
      generalInformation.type_id === null ||
      generalInformation.type_id === undefined ||
      Object.is(NaN, generalInformation.type_id) ||
      generalInformation.type_id === 0 ||
      generalInformation.type_id === '0'
    ) {
      toast.error(`Vous devez sélectionner le type d'hébergement`, {
        position: toast.POSITION.BOTTOM_LEFT
      })
      return
    }

    setIsLoading(true)
    const body = {
      ...hebergement,
      ...generalInformation
    }
    const { message } = await updateHebergement(body, hebergement.id)
    if (message === 'Room updated successfuly!') {
      updateHebergementProp(generalInformation)
      toast.success('Informations mises à jour', {
        position: toast.POSITION.BOTTOM_LEFT
      })
    }
    setEditMode(!editMode)
    setIsLoading(false)
  }

  const updateFields = (name, value) => {
    setGeneralInformation({ ...generalInformation, [name]: value })
  }

  if (hebergement.isLoading) {
    return <Loader />
  } else {
    return (
      <>
        <EditInput
          title={'Nom interne'}
          type={'text'}
          name={'internal_name'}
          placeholder={'Exemple: chambre de qualité et spacieuse dans la forêt'}
          help={`C’est votre nom “interne”. Pour s’y retrouver lors des
              réservations.`}
        />
        <EditInput
          title={'Nom personnalisé'}
          type={'test'}
          name={'public_name'}
          placeholder={'Exemple: chambre de qualité et spacieuse dans la forêt'}
          help={`Un joli nom évocateur, relativement court (maxi 10 mots) qui
                    raconte une histoire : on ferme les yeux et on s’y voit déjà
                    ! Si vous n’avez pas d’inspiration, laissez-le vide et
                    envoyez-nous un petit mot (hebergeurs@wegogreenr.com), on
                    s’en occupera ;)`}
        />

        <EditInput
          title={'Comment décririez-vous votre hébergement à vos invités ?'}
          type={'textarea'}
          name={'description'}
          help={`Décrivez le plus précisément le logement et l’expérience que
                    vos voyageurs pourront vivre chez vous (on vous conseille un
                    texte original d'environ 650 caractères ou 100 mots)`}
          from="description"
        />

        <SectionLayout
          onModify={() => {
            setEditMode(!editMode)
          }}
          isModifyMode={editMode}
          titleAlternative="Informations générales"
        >
          <div className="row">
            <div className="col-12">
              <div className="form-group">
                <label htmlFor="type_id">
                  <p className="mb-0">
                    <strong>Type d'hébergement</strong>
                  </p>
                </label>
                <select
                  className="form-control border rounded-3"
                  onChange={(e) => {
                    updateFields('type_id', e.target.value)
                  }}
                  name="type_id"
                  disabled={!editMode || isLoading}
                >
                  <option value=""></option>
                  {roomTypes.map((value, index) => (
                    <option
                      value={value.id}
                      key={index}
                      selected={hebergement.type_id == value.id}
                    >
                      {value.name}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="col-12 col-xl-6">
              <div className="form-group">
                <label htmlFor="host_quantity">
                  <p className="mb-0">
                    <strong>Nombre d'hébergements (de ce type)</strong>
                  </p>
                </label>
                <input
                  style={{ height: '3.5rem' }}
                  onChange={(e) => {
                    updateFields('host_quantity', e.target.value)
                  }}
                  type="number"
                  min="0"
                  className="form-control"
                  name="host_quantity"
                  defaultValue={hebergement.host_quantity}
                  aria-describedby="emailHelp"
                  placeholder=""
                  disabled={!editMode || isLoading}
                />
              </div>
            </div>
            <div className="col-12 col-xl-6">
              <div className="form-group">
                <label htmlFor="total_area">
                  <p className="mb-0">
                    <strong>Superficie de l'hébérgement (m2)</strong>
                  </p>
                </label>
                <input
                  style={{ height: '3.5rem' }}
                  onChange={(e) => {
                    updateFields('total_area', e.target.value)
                  }}
                  type="number"
                  onWheel={(e) => e.target.blur()}
                  min="0"
                  className="form-control"
                  name="total_area"
                  defaultValue={hebergement.total_area}
                  aria-describedby="emailHelp"
                  placeholder=""
                  disabled={!editMode || isLoading}
                />
              </div>
            </div>
            {editMode && (
              <div className="mt-4">
                <button
                  disabled={isLoading}
                  type="button"
                  onClick={updateGeneralInformation}
                  className="btn btn-secondary"
                >
                  {isLoading ? 'Loading...' : 'Sauvegarder'}
                </button>
              </div>
            )}
          </div>
        </SectionLayout>
      </>
    )
  }
}

const mapDispatchToProps = { updateHebergementProp }

const mapStateToProps = (state) => {
  return { hebergement: state.hebergement }
}

export default connect(mapStateToProps, mapDispatchToProps)(Info)
