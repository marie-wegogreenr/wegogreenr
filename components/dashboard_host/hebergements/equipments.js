import * as React from 'react'
import { consoleLog } from '/utils/logConsole'
import { toast } from 'react-toastify'
import Cookies from 'universal-cookie'

/* Components */
import { SectionLayout } from 'components/host/establishment'
import EquipmentInput from 'components/host/dashboard/EquipmentInput'

import { Loader } from 'components/'
import {
  addEquipment,
  deleleEquipment,
  getEquipments,
  getRoomEquipmentsCategories,
  getRoomEquipmentsTypes
} from 'services/hebergementsService'

class Equipments extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: null,
      editMode: false,
      room_equipments_type_categories: [],
      rooms_equipments_types: [],
      rooms_equipments: [],
      mappedEquiments: [],
      isLoading: true
    }
  }

  async componentDidMount() {
    this.handleInitialInfo()
  }

  handleInitialInfo() {
    this.setState({ ...this.state, isLoading: true }, async () => {
      const [
        json_rooms_equipments_types,
        json_room_equipments_type_categories,
        equipments
      ] = await Promise.all([
        getRoomEquipmentsTypes(),
        getRoomEquipmentsCategories(),
        getEquipments(this.props.id)
      ])

      this.setState({
        ...this.state,
        rooms_equipments_types: json_rooms_equipments_types,
        room_equipments_type_categories: json_room_equipments_type_categories,
        room_equipments: equipments
      })

      const mappedEquiments = this.state.rooms_equipments_types.map(
        (allEquipments) => {
          const savedEquipment = this.state.room_equipments.filter(
            (re) => allEquipments.id === re.type_id
          )
          const isChecked = savedEquipment.length > 0
          return {
            ...allEquipments,
            checked: isChecked,
            intialState: isChecked,
            room_equipment_id: isChecked ? savedEquipment[0].id : ''
          }
        }
      )

      let orderByName = mappedEquiments.reduce((r, a) => {
        const { name } = json_room_equipments_type_categories.find(
          (categoriesName) => categoriesName.id === a.category_id
        )
        r[name] = r[name] || []
        r[name].push(a)
        return r
      }, Object.create(null))

      orderByName = Object.keys(orderByName).map((key) => [
        key,
        orderByName[key]
      ])

      this.setState({
        ...this.state,
        rooms_equipments_types: mappedEquiments,
        mappedEquiments: orderByName,
        isLoading: false
      })
    })
  }

  render() {
    if (this.state.isLoading) {
      return <Loader />
    }
    return (
      <>
        {this.state.mappedEquiments.map(([name, equipments]) => {
          return (
            <EquipmentInput
              title={name}
              equipments={equipments}
              key={name}
              id={this.props.id}
            />
          )
        })}
      </>
    )
  }
}

export default Equipments
