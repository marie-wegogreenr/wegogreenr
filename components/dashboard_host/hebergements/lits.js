import * as React from 'react'
import { consoleLog } from '/utils/logConsole'
import { toast } from 'react-toastify'
import EditInput from '@components/host/dashboard/EditInput'
import Cookies from 'universal-cookie'

/* Components */
import { SectionLayout } from 'components/host/establishment'

import { Loader } from 'components/'
import QuantityRoomInput from '@components/host/dashboard/QuantityRoomInput'

class Lits extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id: null,
      editMode: false,
      bed_types: [],
      room_beds: [],
      status_room_beds: 'create',
      isLoading: true,
      isUpdating: false
    }

    this.handleChangeQuantity = this.handleChangeQuantity.bind(this)
    this.handleLits = this.handleLits.bind(this)
    this.handleEditMode = this.handleEditMode.bind(this)
  }

  async componentDidMount() {
    this.handleInitialInfo()
  }

  handleInitialInfo = async () => {
    this.setState(
      {
        ...this.state,
        isLoading: true,
        room_beds: []
      },
      async () => {
        const cookies = new Cookies()
        this.setState({ ...this.state, id: this.props.id })

        //Get bed types
        const res_bed_types = await fetch(
          process.env.NEXT_PUBLIC_API_URL + 'bed_types',
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'GET'
          }
        )
        const json_bed_types = await res_bed_types.json()
        this.setState({ ...this.state, bed_types: json_bed_types })

        //Get room beds
        const res_room_bed = await fetch(
          process.env.NEXT_PUBLIC_API_URL + 'room-beds/' + this.state.id,
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'GET'
          }
        )
        const json_room_beds = await res_room_bed.json()
        this.setState({ ...this.state, room_beds: json_room_beds })

        let bed_types = this.state.bed_types

        bed_types.forEach((bed) => {
          bed.quantity = 0
        })

        this.setState({ ...this.state, bed_types: bed_types })

        let bed_types_2 = this.state.bed_types

        this.state.room_beds.forEach((room_bed) => {
          bed_types_2.forEach((bed_type) => {
            if (bed_type.id == room_bed.bed_type_id) {
              bed_type.quantity = room_bed.quantity
              bed_type.room_bed_id = room_bed.id
            }
          })
        })

        this.setState({ ...this.state, bed_types: bed_types, isLoading: false })

        if (this.state.room_beds.length > 0) {
          this.state.status_room_beds = 'update'
        }
      }
    )
  }

  async handleChangeQuantity(event) {
    event.preventDefault()
    let bed_types = this.state.bed_types
    if (event.target.id.includes('plus')) {
      bed_types.forEach((bed) => {
        if (bed.id == parseInt(event.target.name)) {
          bed.quantity += 1
        }
      })
    } else {
      bed_types.forEach((bed) => {
        if (bed.id == parseInt(event.target.name)) {
          if (bed.quantity > 0) {
            bed.quantity -= 1
          }
        }
      })
    }

    this.setState({ ...this.state, bed_types: bed_types })
  }

  handleEditMode() {
    this.setState({ ...this.state, editMode: !this.state.editMode })
  }

  async handleLits(event) {
    event.preventDefault()
    const cookies = new Cookies()
    this.setState({ ...this.state, isUpdating: true })
    this.state.bed_types.forEach((bed) => {
      if (this.state.status_room_beds == 'create') {
        fetch(process.env.NEXT_PUBLIC_API_URL + 'room-beds', {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + cookies.get('tk_user')
          },
          method: 'POST',
          body: JSON.stringify({
            room_id: this.state.id,
            bed_type_id: bed.id,
            quantity: bed.quantity
          })
        })
      } else {
        fetch(
          process.env.NEXT_PUBLIC_API_URL + 'room-beds/' + bed.room_bed_id,
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'PUT',
            body: JSON.stringify({
              room_id: this.state.id,
              bed_type_id: bed.id,
              quantity: bed.quantity
            })
          }
        )
      }
    })

    toast.success('Lits mis à jour', {
      position: toast.POSITION.BOTTOM_LEFT
    })
    this.setState({
      ...this.state,
      isUpdating: false,
      editMode: !this.state.editMode
    })
  }

  render() {
    if (this.state.isLoading) {
      return <Loader />
    } else {
      return (
        <>
          <QuantityRoomInput
            title={'Nombre de chambres'}
            name={'rooms_quantity'}
            subtitle={'Chambres'}
          />

          <QuantityRoomInput
            title={'Nombre de voyageurs'}
            name={'people_capacity'}
            subtitle={'Voyageurs'}
          />

          <SectionLayout
            isModifyMode={this.state.editMode}
            onModify={this.handleEditMode}
            title="Lits et invités"
          >
            <div>
              {this.state.bed_types?.map((bed) => {
                if (!this.state.editMode) {
                  return (
                    <div className="d-flex justify-content-between align-items-center py-3 border-bottom">
                      <p className="mb-0">
                        <strong>{bed.name}</strong>
                      </p>

                      <p className="mb-0">{bed.quantity || 0}</p>
                    </div>
                  )
                }

                return (
                  <div className="bg-light p-3 d-flex justify-content-between align-items-center mb-2">
                    <div>
                      <p className="mb-0">
                        <strong>{bed.name}</strong>
                      </p>
                    </div>

                    <div className="d-flex align-items-center">
                      <button
                        className="btn bg-white btn-circle"
                        type="button"
                        name={bed.id}
                        id={'minus' + bed.id}
                        onClick={this.handleChangeQuantity}
                        disabled={!this.state.editMode || this.state.isUpdating}
                      ></button>

                      <input
                        style={{ width: '3rem' }}
                        className="form-control quantity border-0 text-center input-spin-hide bg-transparent mx-2"
                        min="0"
                        id={bed.id}
                        name={'input_bed_types_' + bed.id}
                        type="number"
                        onWheel={(e) => e.target.blur()}
                        readOnly
                        value={bed.quantity}
                      />

                      <button
                        type="button"
                        className="btn bg-white btn-circle"
                        name={bed.id}
                        id={'plus' + bed.id}
                        onClick={this.handleChangeQuantity}
                        disabled={!this.state.editMode || this.state.isUpdating}
                      >
                        +
                      </button>
                    </div>
                  </div>
                )
              })}
            </div>

            {this.state.editMode && (
              <div className="mt-3">
                <button
                  type="submit"
                  onClick={this.handleLits}
                  className="btn btn-secondary"
                  disabled={this.state.isUpdating}
                >
                  {this.state.isUpdating ? 'Loading...' : 'Sauvegarder'}
                </button>
              </div>
            )}
          </SectionLayout>

          <QuantityRoomInput
            title={'Nombre de salles de bain'}
            name={'bathroom_quantity'}
            subtitle={'Salles de bain'}
          />

          <EditInput
            title={'points forts *'}
            type={'textarea'}
            name={'strengths'}
            from="strengths"
          />
        </>
      )
    }
  }
}

export default Lits
