import React, { useState, useEffect } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { updateRoomStatus } from 'services/hebergementsService'
import { useSelector, useDispatch } from 'react-redux'
import noImage from '../../../public/images/not-image.svg'
import { updateHebergementProp } from '@ducks/hebergements/actions'
import { toast } from 'react-toastify'
import { useToast } from 'hooks/useToast'
import PopupStripe from '@components/host/myRooms/PopupStripe'

const HebergementsCard = ({ hebergementData, image, stripeConnect }) => {
  const [notify] = useToast()
  const dispatch = useDispatch()
  const hebergement = useSelector((store) => store.hebergement)
  const {
    establishment: { green_score, city_name, region_name, address },
    mainImage
  } = hebergement

  const [roomStatus, setRoomStatus] = useState(!!Number(hebergement.active))
  const [showPopup, setShowPopup] = useState(false)
  const [switchBtn, setSwitchBtn] = useState(false)

  useEffect(() => {
    if (hebergement.active !== null) {
      setRoomStatus(hebergement.active)
    }
  }, [hebergement.active])

  const changeStatus = async () => {
    if (
      hebergement.type_id === '' ||
      hebergement.type_id === null ||
      hebergement.type_id === undefined ||
      Object.is(NaN, hebergement.type_id) ||
      hebergement.type_id === 0 ||
      hebergement.type_id === '0'
    ) {
      notify(`Vous devez sélectionner le type d'hébergement`, 'error')
      return
    }

    setRoomStatus(!roomStatus)
    const { Message, fails } = await updateRoomStatus(hebergement.id)
    if (Message === 'You must insert minimum fields for activation') {
      setShowPopup(fails.stripe === null || fails.stripe === '' ? true : false)

      toast.warning(
        <div>
          vous devez effectuer les actions suivantes afin d'activer cet espace:
          <br />
          <br />
          {`${fails.name === null ? '-  ajouter un nom public' : ''}`}
          {fails.name === null && <br />}
          {`${fails.price === null ? '- ajouter le prix' : ''}`}
          {fails.price === null && <br />}
          {`${
            fails.stripe === null || fails.stripe === ''
              ? '-  associez vos coordonnées bancaires à wegogreenr'
              : ''
          }`}
          {fails.images < 3 && <br />}
          {`${
            fails.images < 3 ? '-  ajoutez au moins 3 images à cette pièce' : ''
          }`}
        </div>,
        {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 8000
        }
      )
      setRoomStatus(0)
    } else {
      dispatch(updateHebergementProp({ active: !roomStatus }))
      notify('changement réussi', 'success')
    }
  }
  if (hebergement.isLoading) {
    return <></>
  } else {
    return (
      <div className="hebergement hebergement__infoCard">
        <div className="cardTextUp">
          <img src={image ? mainImage : noImage} alt="picture" />
          <div className="textContent">
            <span>
              {city_name && region_name
                ? `${city_name}, ${region_name}`
                : address
                ? `${address}`
                : 'Address'}
            </span>
            <h4>
              {hebergement.public_name ? hebergement.public_name : 'Room name'}
            </h4>
            <div className="priceAndstars">
              <div>
                <FontAwesomeIcon icon={faStar} />
                <span>{green_score?.level ? green_score?.level : 5}/5</span>
              </div>

              <h4>
                {hebergement.basic_price
                  ? hebergement.basic_price.toString().replace('.', ',')
                  : 0}
                €/nuit
              </h4>
            </div>
          </div>
        </div>
        <hr />
        <div className="cardTextDown">
          <div className="status">
            <span>État de l'annonce : </span>
            <div>
              <div
                className={`circle circle--${roomStatus ? 'green' : 'red'}`}
              ></div>
              <span>{`${roomStatus ? '' : 'Non'}Publiée`}</span>
            </div>
          </div>
          {window !== undefined && window.location.href.includes('admin') ? (
            <div className="custom-control custom-switch">
              <input
                type="checkbox"
                className="custom-control-input"
                id="customSwitch3"
                checked={roomStatus}
                onClick={() => {
                  setSwitchBtn(!switchBtn)
                  changeStatus()
                }}
              />
              <label
                className={`custom-control-labels${
                  switchBtn ? '--active' : ''
                }`}
                for="customSwitch3"
              ></label>
            </div>
          ) : (
            <div className="custom-control custom-switch">
              <input
                type="checkbox"
                className="custom-control-input"
                id="customSwitch1"
                checked={roomStatus}
                onClick={changeStatus}
              />
              <label
                className="custom-control-label"
                for="customSwitch1"
              ></label>
            </div>
          )}
        </div>

        {showPopup ? (
          <PopupStripe setShowPopup={setShowPopup} from="room" />
        ) : null}
      </div>
    )
  }
}

export default HebergementsCard
