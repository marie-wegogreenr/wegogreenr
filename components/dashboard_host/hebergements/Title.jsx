import { useSelector } from 'react-redux'

export default function Title() {
  const hebergement = useSelector((store) => store.hebergement)

  return (
    <div className="my-3">
      <h1 className="main-room__title mb-0 font-fraunces color-primary h2 weight-bold">
        {hebergement.internal_name}
      </h1>
    </div>
  )
}
