import React from 'react'
import { consoleLog } from '/utils/logConsole'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'

import Popup from './Popup'
import PopupIcal from './PopupIcal'
import SummaryIcal from './SummaryIcal'
import PopupInfoGoogle from '../../Onboarding/PopupInfoGoogle'
import Loader from '@components/Loader'
import PopupPrice from './PopupPrice'
import PopupTarifs from './PopupTarifs'
import SummaryPrice from './SummaryPrice'

function SummaryCalendar(props) {
  library.add(faInfoCircle)

  return (
    <div>
      <div className="col-12">
        <div className="card-white row">
          <img
            src="/images/icon_calendar_sync.png"
            alt="Empty calendar"
            className="dashboard-icons"
          />
          <h3
            className="font-montserrat col-12 weight-bold mb-3"
            style={{ color: 'black', fontSize: '22px' }}
          >
            Synchronisation iCal
          </h3>
          <p className="col-12 dashboard-links text-justify">
            Vous avez un calendrier iCal ou vous utilisez déjà un Channel
            Manager pour votre hébergement ? Ajoutez-les ici pour que votre
            calendrier We Go GreenR se synchronise autoamtiquement avec eux.
          </p>
          <a
            href="#!"
            className="col-12 dashboard-links"
            onClick={props.handleChangeIcal}
          >
            Ajouter
          </a>
        </div>
      </div>

      {props.showPopupIcal && (
        <PopupIcal
          onCloseDialog={props.handleChangeIcal}
          room_id={props.id_hebergement}
          slug_id={props.slug_hebergement}
        />
      )}

      <div className="col-12">
        <div className="card-white row">
          <img
            src="/images/icon_calendar.png"
            alt="Empty calendar"
            className="dashboard-icons"
          />
          <h3
            className="font-montserrat col-12 weight-bold mb-3"
            style={{ color: 'black', fontSize: '22px' }}
          >
            Ajouter des disponibilités:
          </h3>
          <a
            href="#!"
            className="col-12 dashboard-links"
            onClick={props.handleChangeAvailability}
          >
            Ajouter
          </a>
        </div>
      </div>

      {props.showPopup && (
        <Popup
          onCloseDialog={props.handleChangeAvailability}
          dates={props.dates_not_availabities}
          room_id={props.id_hebergement}
        />
      )}

      <div className="col-12">
        <div className="card-white row">
          <img
            src="/images/icon_calendar_price.png"
            alt="Empty calendar"
            className="dashboard-icons"
          />
          <h3
            className="font-montserrat col-12 weight-bold mb-3"
            style={{ color: 'black', fontSize: '22px' }}
          >
            Tarif personnalisé
          </h3>
          <p className="col-12 dashboard-links text-justify">
            Affinez vos prix en créant un tarif spécial pour une plage de dates
            spécifiques.
          </p>
          <a
            href="#!"
            className="col-6 dashboard-links"
            onClick={props.handleChangePrice}
          >
            Ajouter
          </a>
          <a
            href="#!"
            className="col-6 dashboard-links"
            onClick={props.handleChangeTarifs}
          >
            Voir mes tarifs
          </a>
        </div>
      </div>

      {props.showPopupPrice && (
        <PopupPrice
          onCloseDialog={props.handleChangePrice}
          room_id={props.id_hebergement}
          special_prices={props.dates_not_availabities}
          component={
            <SummaryPrice
              onClosePopup={props.handleChangePrice}
              room_id={props.id_hebergement}
              special_prices={props.dates_not_availabities}
            />
          }
        />
      )}
      {props.showPopupTarifs && (
        <PopupPrice
          onCloseDialog={props.handleChangeTarifs}
          room_id={props.id_hebergement}
          special_prices={props.dates_not_availabities}
          component={
            <PopupTarifs
              onClosePopup={props.handleChangeTarifs}
              room_id={props.id_hebergement}
              special_prices={props.dates_not_availabities}
            />
          }
        />
      )}
    </div>
  )
}

export default SummaryCalendar
