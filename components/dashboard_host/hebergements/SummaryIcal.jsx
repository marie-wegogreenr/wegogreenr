import React, { useEffect } from 'react'
import { consoleLog } from '/utils/logConsole'

import Cookies from 'universal-cookie'
import axios from 'axios'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCopy, faTrashAlt } from '@fortawesome/free-solid-svg-icons'

import Loader from '@components/Loader'
import { useToast } from 'hooks/useToast'
const SummaryIcal = (props) => {
  const [notify] = useToast()
  library.add(faCopy, faTrashAlt)

  const [fields, setFields] = React.useState({
    userIcals: [],
    showIcal: false,
    isChecked: true
  })

  useEffect(() => {
    getIcals()
  }, [])

  const getIcals = () => {
    const cookies = new Cookies()
    axios
      .get(process.env.NEXT_PUBLIC_API_URL + 'ical/' + props.room_id, {
        headers: {
          Authorization: 'Bearer ' + cookies.get('tk_user')
        }
      })
      .then((response) => {
        setFields({
          ...fields,
          userIcals: response.data,
          showIcal: true,
          isChecked: response.data.length > 0 ? true : false
        })
      })
      .catch((error) => {
        console.error(error)
        setFields({
          ...fields,
          showIcal: true
        })
      })
  }

  const createIcal = (e) => {
    e.preventDefault()
    const cookies = new Cookies()
    axios
      .post(
        process.env.NEXT_PUBLIC_API_URL + 'ical',
        {
          room_id: props.room_id,
          ical_link: e.target.input_ical.value
        },
        {
          headers: {
            Authorization: 'Bearer ' + cookies.get('tk_user')
          }
        }
      )
      .then((response) => {
        if (response.data.Message === 'Incorrect Ical link') {
          notify('Ical non valide', 'error')
        } else if (response.data.Message === 'Ical link already exist') {
          notify('Le lien Ical existe déjà', 'warning')
        } else if (
          response.data.Message === 'Room already have an ical configurated'
        ) {
          notify('Un lien iCal est déjà rentré pour cet hébergement', 'warning')
        } else {
          notify('Ical créé', 'success')
          getIcals()
          props.onClosePopup()
        }
      })
      .catch((error) => {
        notify('Ical non valide', 'error')
      })
  }

  const deleteIcal = (id_ical) => {
    const cookies = new Cookies()
    axios
      .delete(process.env.NEXT_PUBLIC_API_URL + 'ical/' + id_ical, {
        headers: {
          Authorization: 'Bearer ' + cookies.get('tk_user')
        }
      })
      .then((response) => {
        notify('Ical supprimé', 'success')
        getIcals()
      })
      .catch((error) => {
        console.error(error)
      })
  }

  return (
    <div className="card-white row">
      <h3
        className="font-montserrat col-12 weight-bold mb-3 text-center"
        style={{ color: 'black', fontSize: '22px' }}
      >
        Synchroniser votre calendrier
      </h3>

      {fields.showIcal ? (
        <div className="form-group">
          <p className="text-justify">
            Collez le lien iCal de votre calendrier principal pour importer
            automatiquement les disponibilités de votre logement.
          </p>
          <p className="text-justify">
            Si vous utilisez un Channel Manager, veuillez importer uniquement le
            calendrier correspondant. N'importez pas les calendriers des autres
            plateformes de réservation
          </p>
          <form onSubmit={createIcal} method="POST">
            <input
              type="text"
              className="form-control"
              placeholder="Collez ici le lien du calendrier"
              name="input_ical"
              required
            />
            {fields.userIcals.length > 0 &&
              fields.userIcals.map((ical, index) => {
                return (
                  <div className="row d-flex align-items-center" key={index}>
                    <span className="ical-paragraph-link">
                      {ical.ical_link}
                    </span>

                    <div className="col-2 mb-3" style={{ textAlign: 'center' }}>
                      <FontAwesomeIcon
                        icon="trash-alt"
                        size="2x"
                        data-toggle="tooltip"
                        data-placement="right"
                        title="Delete"
                        className="icon-check"
                        style={{ cursor: 'pointer' }}
                        onClick={() => deleteIcal(ical.id)}
                      />
                    </div>
                  </div>
                )
              })}

            <p>
              Besoin d'aide?{' '}
              <a
                href="https://wegogreenr-laravel.s3.eu-west-2.amazonaws.com/Documents/Tutoriel+-+synchroniser+ses+calendriers+(1)_compressed+(1).pdf"
                target="_blank"
              >
                Voir notre guide de synchronisation des calendriers.{' '}
              </a>
            </p>

            <button type="submit" className="button button-long mb-3">
              Importer votre calendrier iCal
            </button>

            <p>
              Pour que les réservations sur We Go GreenR remontent dans votre
              calendrier principal, veuillez copier/coller ce lien iCal et le
              reporter dans votre calendrier principal ou votre Channel Manager
            </p>
            <div className="row mt-3">
              <p className="col-10" style={{ wordWrap: 'break-word' }}>
                {' '}
                <strong>Exporter votre calendrier:</strong>{' '}
                {process.env.NEXT_PUBLIC_API_URL +
                  'get-ical-dates/' +
                  props.slug_id}
              </p>
              <CopyToClipboard
                text={
                  process.env.NEXT_PUBLIC_API_URL +
                  'get-ical-dates/' +
                  props.slug_id
                }
                onCopy={() => notify('Copied!')}
                style={{ cursor: 'pointer', textAlign: 'center' }}
                className="col-2"
              >
                <div className="col-6">
                  <FontAwesomeIcon
                    icon="copy"
                    size="2x"
                    data-toggle="tooltip"
                    data-placement="right"
                    title="Copy"
                    className="icon-check"
                  />
                </div>
              </CopyToClipboard>
            </div>
          </form>
        </div>
      ) : (
        <Loader />
      )}
    </div>
  )
}

export default SummaryIcal
