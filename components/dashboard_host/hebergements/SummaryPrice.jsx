import { useState, useEffect } from 'react'
import Cookies from 'universal-cookie'
import axios from 'axios'
import moment from 'moment'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons'
import Loader from '@components/Loader'
import { useDispatch, useSelector } from 'react-redux'
import { ifDateInArray } from 'helpers/calendarHelper'
import { formatDate, formatSafari } from '../../../helpers/timeHelper'
import { eachDayOfInterval } from 'date-fns'
import {
  addRangeSpecialPrice,
  deleteSpecialPrice
} from '@ducks/hostCalendar/actions'
import { useToast } from 'hooks/useToast'
import {
  convertPrices,
  inputOnlyNumberComma,
  validatePrices
} from 'helpers/utilsHelper'
/* Dependencies */
import DatePicker from 'react-datepicker'
import { CustomNormalDatePicker } from '@components/customInputs/CustomNormalDatePicker'
import { registerLocale, setDefaultLocale } from 'react-datepicker'
import fr from 'date-fns/locale/fr'
registerLocale('fr', fr)
setDefaultLocale('fr')

const SummaryPrice = (props) => {
  const dispatch = useDispatch()
  const { customDays, specialDates } = useSelector(
    (store) => store.hostCalendar
  )

  const [fields, setFields] = useState({
    min_night: 0,
    max_night: 0,
    startDate: formatSafari(new Date()),
    endDate: formatSafari(new Date()),
    isWeekEndPriceEnable: false,
    isNuitsEnable: false,
    isMonthDiscountEnable: false,
    isWeekDiscountEnable: false,
    specialPrices: [],
    showSpecialPrices: true
  })

  const [pricesValues, setPricesValues] = useState({
    price: '',
    weekend_price: ''
  })

  const [notify] = useToast()
  useEffect(() => {}, [])
  const handlePopupPrice = (e) => {
    e.preventDefault()

    const valid = validatePrices(pricesValues.price, pricesValues.weekend_price)

    if (!valid) {
      notify(
        `Corrigez les prix. Vous ne pouvez pas écrire plus d'une virgule.`,
        'error'
      )
      return
    }

    let start = formatSafari(fields.startDate)

    let end = formatSafari(fields.endDate)

    if (start > end) {
      return notify('erreur dans la date sélectionnée', 'error')
    }

    const dates = eachDayOfInterval({ start, end })

    const reservationDates = customDays.filter(
      (cd) => cd.type === 2 || cd.type === 3
    )

    const specialPricesDates = customDays.filter((cd) => cd.type === 4)

    // if (ifDateInArray(reservationDates, dates)) {
    //   notify(`Il y a des réservations dans cette fourchette de dates`, 'error')
    //   props.onClosePopup()
    //   return
    // }

    if (ifDateInArray(specialPricesDates, dates)) {
      notify(
        `Un tarif personnalisé est déjà rentré pour l'une de ces dates`,
        'error'
      )
      props.onClosePopup()
      return
    }

    if (parseInt(fields.min_night) > parseInt(fields.max_night)) {
      notify(`erreur dans le nombre de nuits minimum et maximum`, 'error')
      return
    }

    let _basePrice, _weekendPrice

    if (pricesValues.weekend_price) {
      const { priceBaseFormatted, priceWeekendFormatted } = convertPrices(
        pricesValues.price,
        pricesValues.weekend_price
      )
      _basePrice = priceBaseFormatted
      _weekendPrice = priceWeekendFormatted
    } else {
      _basePrice = convertPrices(pricesValues.price)
    }

    const body = {
      room_id: props.room_id,
      title: e.target.name_custom.value,
      start_date: moment(fields.startDate).format('YYYY-MM-DD'),
      end_date: moment(fields.endDate).format('YYYY-MM-DD'),
      price: _basePrice || '',
      weekend_price: _weekendPrice || '',
      min_nigth: fields.min_night,
      max_nigth: fields.max_night
    }
    dispatch(addRangeSpecialPrice(body))

    // dates.forEach((date) => {

    // })

    // notify('Prix personnalisé created', 'success')
    props.onClosePopup()
  }

  const handlePriceChange = (e) => {
    const isNumberWithComma = inputOnlyNumberComma(e.target.value)
    if (isNumberWithComma) {
      setPricesValues({
        ...pricesValues,
        [e.target.name]: e.target.value
      })
    } else {
      setPricesValues({
        ...pricesValues,
        [e.target.name]: e.target.value.slice(0, -1)
      })
    }
  }
  return (
    <>
      <h3
        className="font-montserrat col-12 weight-bold mb-5 text-center mt-5"
        style={{ color: 'black', fontSize: '22px' }}
      >
        Ajouter un tarif personnalisé
      </h3>
      <form className="" onSubmit={handlePopupPrice}>
        <div className="">
          <div className="mb-3">
            <b className="text-block weight-bold">Dates</b>
          </div>
          <div className="modalPriceContent___dates">
            <div className="input-group mb-3 ">
              <div className="input-group-prepend">
                <div
                  className={`input-group-text bg-white color-black ${
                    window !== undefined &&
                    window.location.href.includes('admin')
                      ? 'height-50'
                      : ''
                  }`}
                  style={{ border: '1px solid rgb(206, 212, 218)' }}
                >
                  Du:
                </div>
              </div>
              <DatePicker
                className="form-control date-inputs rounded-right modalPriceContent__date-picker"
                selected={new Date(fields.startDate)}
                onChange={(date) =>
                  setFields({ ...fields, startDate: formatSafari(date) })
                }
                dateFormat="dd/MM/yyyy"
                customInput={<CustomNormalDatePicker />}
              />
            </div>
            <div className="input-group mb-4 ">
              <div className="input-group-prepend">
                <div
                  className={`input-group-text bg-white color-black ${
                    window !== undefined &&
                    window.location.href.includes('admin')
                      ? 'height-50'
                      : ''
                  }`}
                  style={{ border: '1px solid rgb(206, 212, 218)' }}
                >
                  Au:
                </div>
              </div>
              <DatePicker
                className="form-control date-inputs rounded-right modalPriceContent__date-picker"
                selected={new Date(fields.endDate)}
                minDate={new Date(fields.startDate)}
                onChange={(date) =>
                  setFields({ ...fields, endDate: formatSafari(date) })
                }
                dateFormat="dd/MM/yyyy"
                customInput={<CustomNormalDatePicker />}
              />
            </div>
          </div>

          <div className=" mb-2">
            <hr style={{ height: '0px' }} />
          </div>
          <div className=" mb-3">
            <b className="text-block weight-bold">Nom personnalisé</b>
          </div>
          <div className=" col-md-12 mb-4">
            <div className="input-group input-group-lg rounded">
              <input
                type="text"
                className="form-control border text-center"
                id="name_custom"
                name="name_custom"
                placeholder=""
                required
              />
            </div>
          </div>

          <div className=" mb-2">
            <hr style={{ height: '0px' }} />
          </div>
          <div className=" mb-3">
            <b className="text-block weight-bold">Prix de base</b>
          </div>
          <div className=" col-md-12 mb-4">
            <div className="input-group input-group-lg mb-2 rounded">
              <input
                type="text"
                onWheel={(e) => e.target.blur()}
                className="form-control border text-center"
                id="price"
                name="price"
                placeholder=""
                min="0"
                step=".01"
                onChange={handlePriceChange}
                value={pricesValues.price}
                required
              />
              <div className="input-group-prepend">
                <label
                  htmlFor="price"
                  className={`input-group-text border-0 ${
                    window !== undefined &&
                    window.location.href.includes('admin')
                      ? 'height-50'
                      : ''
                  }`}
                >
                  / nuit
                </label>
              </div>
            </div>
            <p className="text-justify">
              Ce sera le prix à la nuitée de votre hébergement du{' '}
              <b className="weight-bold">
                {moment(fields.startDate).format('DD MMM')} au{' '}
                {moment(fields.endDate).format('DD MMM')}
              </b>
            </p>
          </div>

          <div className=" mb-2">
            <hr style={{ height: '0px' }} />
          </div>
          <div className=" mb-3">
            <div className="row">
              <div className="col-6">
                <b className="text-block weight-bold">Prix week-end</b>
              </div>
              <div className="col-6" style={{ textAlign: 'end' }}>
                {window !== undefined &&
                window.location.href.includes('admin') ? (
                  <div className="custom-control custom-switch">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="prix-weekend-switch"
                      checked={fields.isWeekEndPriceEnable}
                    />
                    <label
                      className={`ml-2 custom-control-labels${
                        fields.isWeekEndPriceEnable ? '--active' : ''
                      }`}
                      htmlFor="prix-switch"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        setFields({
                          ...fields,
                          isWeekEndPriceEnable: !fields.isWeekEndPriceEnable
                        })
                      }}
                    ></label>
                  </div>
                ) : (
                  <div className="custom-control custom-switch">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="nuits-switch"
                      checked={fields.isWeekEndPriceEnable}
                    />
                    <label
                      className="custom-control-label ml-2"
                      htmlFor="prix-switch"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        setFields({
                          ...fields,
                          isWeekEndPriceEnable: !fields.isWeekEndPriceEnable
                        })
                      }}
                    ></label>
                  </div>
                )}
              </div>
              <div className="">
                <p>
                  Du vendredi au samedi, et du samedi au dimanche (prix par
                  nuit)
                </p>
              </div>
            </div>
          </div>
          <div className=" col-md-12 mb-4">
            <div className="input-group input-group-lg mb-2 rounded">
              <input
                type="text"
                onWheel={(e) => e.target.blur()}
                className="form-control border text-center"
                id="weekend_price"
                name="weekend_price"
                placeholder=""
                min="0"
                disabled={!fields.isWeekEndPriceEnable}
                required={fields.isWeekEndPriceEnable}
                onChange={handlePriceChange}
                value={pricesValues.weekend_price}
              />
              <div className="input-group-prepend">
                <label
                  htmlFor="weekend_price"
                  className={`input-group-text border-0 ${
                    window !== undefined &&
                    window.location.href.includes('admin')
                      ? 'height-50'
                      : ''
                  }`}
                >
                  / nuit
                </label>
              </div>
            </div>
          </div>

          <div className=" mb-2">
            <hr style={{ height: '0px' }} />
          </div>
          <div className=" mb-3">
            <div className="row">
              <div className="col-6">
                <b className="text-block weight-bold">
                  Nuits minimum et maximum
                </b>
              </div>
              <div className="col-6" style={{ textAlign: 'end' }}>
                {window !== undefined &&
                window.location.href.includes('admin') ? (
                  <div className="custom-control custom-switch">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="nuits-switch"
                      checked={fields.isNuitsEnable}
                    />
                    <label
                      className={`ml-2 custom-control-labels${
                        fields.isNuitsEnable ? '--active' : ''
                      }`}
                      htmlFor="prix-switch"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        setFields({
                          ...fields,
                          isNuitsEnable: !fields.isNuitsEnable
                        })
                      }}
                    ></label>
                  </div>
                ) : (
                  <div className="custom-control custom-switch">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="nuits-switch"
                      checked={fields.isNuitsEnable}
                    />
                    <label
                      className="custom-control-label ml-2"
                      htmlFor="prix-switch"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        setFields({
                          ...fields,
                          isNuitsEnable: !fields.isNuitsEnable
                        })
                      }}
                    ></label>
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="">
            <div className=" col-xl-12 card-button-nights mt-3">
              <div
                className={`row ${
                  window !== undefined && window.location.href.includes('admin')
                    ? 'w-100'
                    : ''
                }`}
              >
                <div
                  className={`col-8 mt-3 input-night ${
                    window !== undefined &&
                    window.location.href.includes('admin')
                      ? 'ps-4'
                      : ''
                  }`}
                >
                  <input
                    type="number"
                    onWheel={(e) => e.target.blur()}
                    value={fields.min_night}
                    className="number-styled-input"
                    disabled={!fields.isNuitsEnable}
                    onChange={(e) => {
                      setFields({
                        ...fields,
                        min_night: e.target.value
                      })
                    }}
                  />
                  <p
                    className={`ml-2 ${
                      window !== undefined &&
                      window.location.href.includes('admin')
                        ? 'ps-2'
                        : ''
                    }`}
                  >{`${fields.min_night >= 2 ? 'Nuits' : 'Nuit'} minimum`}</p>
                </div>
                <div className="col-4 buttons-night">
                  <button
                    className="button-left"
                    type="button"
                    name="nights_min"
                    onClick={(e) => {
                      let input = fields.min_night
                      if (input > 0) input = input -= 1
                      setFields({
                        ...fields,
                        min_night: input
                      })
                    }}
                    disabled={!fields.isNuitsEnable}
                  >
                    -
                  </button>
                  <button
                    className="button-right"
                    type="button"
                    name="nights_min"
                    onClick={(e) => {
                      let input = fields.min_night
                      input = input += 1
                      setFields({
                        ...fields,
                        min_night: input
                      })
                    }}
                    disabled={!fields.isNuitsEnable}
                  >
                    +
                  </button>
                </div>
              </div>
            </div>
            <div className=" col-xl-12 card-button-nights mt-3">
              <div
                className={`row ${
                  window !== undefined && window.location.href.includes('admin')
                    ? 'w-100'
                    : ''
                }`}
              >
                <div
                  className={`col-8 mt-3 input-night ${
                    window !== undefined &&
                    window.location.href.includes('admin')
                      ? 'ps-4'
                      : ''
                  }`}
                >
                  <input
                    type="number"
                    onWheel={(e) => e.target.blur()}
                    value={fields.max_night}
                    className="number-styled-input"
                    disabled={!fields.isNuitsEnable}
                    onChange={(e) => {
                      setFields({
                        ...fields,
                        max_night: e.target.value
                      })
                    }}
                  />
                  <p
                    className={`ml-2 ${
                      window !== undefined &&
                      window.location.href.includes('admin')
                        ? 'ps-2'
                        : ''
                    }`}
                  >
                    {`${fields.max_night >= 2 ? 'Nuits' : 'Nuit'} maximum`}
                  </p>
                </div>
                <div className="col-4 buttons-night">
                  <button
                    className="button-left"
                    type="button"
                    name="nights_max"
                    onClick={(e) => {
                      let input = fields.max_night
                      if (input > 0) input = input -= 1
                      setFields({
                        ...fields,
                        max_night: input
                      })
                    }}
                    disabled={!fields.isNuitsEnable}
                  >
                    -
                  </button>
                  <button
                    className="button-right"
                    type="button"
                    name="nights_max"
                    onClick={(e) => {
                      let input = fields.max_night
                      input = input += 1
                      setFields({
                        ...fields,
                        max_night: input
                      })
                    }}
                    disabled={!fields.isNuitsEnable}
                  >
                    +
                  </button>
                </div>
              </div>
            </div>
          </div>

          <div className="row mt-5">
            <div className=" col-md-6 col-xl-6">
              <button
                className="button button-outline-black"
                onClick={props.onClosePopup}
              >
                Annuler
              </button>
            </div>
            <div className=" col-md-6 col-xl-6 justify-content-end d-flex">
              <button type="submit" className="button button-long">
                Sauvegarder
              </button>
            </div>
          </div>
        </div>
      </form>
    </>
  )
}

export default SummaryPrice
