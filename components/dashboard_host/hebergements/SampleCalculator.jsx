import * as React from 'react'
import 'react-day-picker/lib/style.css'

//redux
import { useDispatch, useSelector } from 'react-redux'
import { postReservationAction } from '@ducks/checkoutDuck'
//utils
import { useRouter } from 'next/router'
import Cookies from 'universal-cookie'
import { hasTimeToShow } from 'helpers/timeHelper'
import { useToast } from 'hooks/useToast'
import {
  mapCustomServices,
  mapNormalServices
} from 'helpers/roomServicesHelper'
import PricesCalc from '@components/Hebergements/Single/Form/PricesCalc'
import ArrivePopUp from '@components/Hebergements/Single/Form/ArrivePopUp'
import DeparturePopUp from '@components/Hebergements/Single/Form/DeparturePopUp'
import UsersQuantity from '@components/Hebergements/Single/Form/UsersQuantity'

const initialFields = {
  checkIn: '',
  checkOut: '',
  breakfast: '',
  adults: 0,
  children: 0
}

export function SampleCalculator({
  establishment,
  room,
  unavailableDates,
  specialDates,
  googleUnavailable,
  establishmentServices,
  customServices
}) {
  const [notify] = useToast()
  const { initial_date, final_date, travellers_quantity } = useSelector(
    (store) => store.searchedRooms.data.searchParams
  )
  const router = useRouter()
  const dispatch = useDispatch()

  const [checkedServices, setCheckedServices] = React.useState([
    // ...mapNormalServices(establishmentServices),
    // ...mapCustomServices(customServices)
  ])

  const [fields, setFields] = React.useState({
    checkIn: hasTimeToShow(initial_date),
    checkOut: hasTimeToShow(final_date),
    breakfast: '',
    adults: travellers_quantity.adults,
    children: travellers_quantity.children
  })

  const [disableBtn, setDisableBtn] = React.useState('')

  const changeFields = (e) => {
    if (fields[e.target.name] > e.target.max) {
      setFields({ ...fields, [e.target.name]: e.target.max })
    } else {
      setFields({ ...fields, [e.target.name]: e.target.value })
    }
  }

  const totalUsers = Number(fields.adults) + Number(fields.children)
  let overload = totalUsers > room.people_capacity
  let validRes = false
  let errorMsg = ''
  let disabled = false

  if (fields.checkIn && fields.checkOut) {
    const isUnavailable = matchUnavailableDates(
      fields.checkIn,
      fields.checkOut,
      unavailableDates
    )
    const isUnavailableGoogle = matchUnavailableDates(
      fields.checkIn,
      fields.checkOut,
      googleUnavailable
    )

    if (isUnavailable) (validRes = false), (errorMsg = 'Dates non disponibles')
    if (isUnavailableGoogle)
      (validRes = false), (errorMsg = 'Dates non disponibles')
    else validRes = true
  }

  if (fields.adults == 0 || overload || !validRes || disableBtn) {
    disabled = true
  }
  return (
    <div className="card sticky-top shdow-1 br-10 calculator-container">
      <div className="p-2 bg-primary br-10">
        <div className="card-body bg-primary text-white p-4 br-10">
          <h3 className="card-title h2 color-white">
            {room.basic_price}&euro;/<small className="h3">nuit</small>
          </h3>
          <div className="border-bottom color-white background-white mt-4" />
          <form className="row g-3 mt-3" onSubmit={(e) => e.preventDefault()}>
            <div className="calc__dates d-flex">
              <div className="w-50">
                <ArrivePopUp
                  fields={fields}
                  setFields={setFields}
                  unavailableDates={unavailableDates}
                  googleUnavailable={googleUnavailable}
                  from="preview"
                />
              </div>
              <div className="w-50">
                <DeparturePopUp
                  fields={fields}
                  setFields={setFields}
                  unavailableDates={unavailableDates}
                  googleUnavailable={googleUnavailable}
                  from="preview"
                />
              </div>
            </div>

            <div className="col-12">
              <div className="bg-white br-8 calc__dates-users">
                <label
                  htmlFor="travelers"
                  className="form-label paragraph weight-bold m-0"
                >
                  Voyageurs
                </label>
                <UsersQuantity
                  state={fields}
                  setUpperState={setFields}
                  changeFields={changeFields}
                  room={room}
                />
              </div>
            </div>

            {/* <AditionalServices
              services={checkedServices}
              setServices={setCheckedServices}
            /> */}

            <div className="col-12">
              <PricesCalc
                establishment={establishment}
                room={room}
                fields={fields}
                specialDates={specialDates}
                unavailableDates={unavailableDates}
                setDisableBtn={setDisableBtn}
                // services={checkedServices}
              />
            </div>
            <div className="col-12">
              <p
                className="color-red"
                style={{ textShadow: '1px 1px 1px #404040' }}
              >
                {errorMsg}
              </p>
              <p
                className="color-red "
                style={{ textShadow: '1px 1px 1px #404040' }}
              >
                {disableBtn}
              </p>
              <button
                className="btn btn-secondary btn-block btn-calc py-3 text-block weight-bold"
                disabled={disabled}
              >
                Réserver
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default SampleCalculator
