import { useEffect } from 'react'
import { Email } from '@components/Icons'
import { useSelector } from 'react-redux'
import { useToast } from 'hooks/useToast'
import { eventListObjectTriggered } from 'helpers/dataLayer/lists'

const NewsletterCard = ({ status, message, onValidated }) => {
  /*  Hooks  */
  const [notify] = useToast()
  /*  Global states  */
  const store = useSelector((store) => store.user.user)
  /*  Effects  */
  useEffect(() => {
    if (status === 'success') {
      eventListObjectTriggered.subscribeNewsletter.callback()
      notify('Vous vous êtes inscrit avec succès à la newsletter', 'success')
    } else if (status === 'error') {
      if (message.includes('already subscribed')) {
        notify('Cet e-mail est déjà inscrit', 'error')
      } else if (message.includes('too many recent signup requests')) {
        notify('Vous avez essayé trop de fois avec le même e-mail', 'error')
      } else {
        notify('Il y a eu une erreur. Essayez à nouveau', 'error')
      }
    }
  }, [status])
  /*  Handle Functions  */
  const handleFormSubmit = (e) => {
    e.preventDefault()

    if (!store.email) {
      setError('Please enter a valid email address')
      return null
    }

    const isFormValidated = onValidated({ EMAIL: store.email })
    /* On success return true */
    return store.email && store.email.indexOf('@') > -1 && isFormValidated
  }

  return (
    <div className="card-white pl-4 pl-lg-5 user-card-white">
      <Email width="30" />
      <h2 className="subtitle-dashboard font-montserrat">Newsletter</h2>
      <form onSubmit={handleFormSubmit}>
        <input type="checkbox" required className="user-card-white__checkbox" />
        <label className="user-card-white__label">
          J'accepte les conditions d'utilisation et la politique de
          confidentialité des données et confirme la véracité des informations
          ci dessus.
        </label>
        <button className="btn btn-secondary btn-home-1 weight-semibold paragraph user-card-white__button">
          Inscrivez-moi
        </button>
      </form>
    </div>
  )
}

export default NewsletterCard
