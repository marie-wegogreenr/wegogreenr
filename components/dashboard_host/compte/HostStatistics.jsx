import React from 'react'
import { consoleLog } from '/utils/logConsole'
import { useSelector } from 'react-redux'

const HostStatistics = () => {
  const store = useSelector((store) => store.user.user.establishmentRooms)
  const reservations = useSelector((store) => store.reservationsByHostId.data)
  //consoleLog(store, 'store host')
  //consoleLog(reservations, 'store host')

  let totalEarnings = 0
  if (reservations) {
    for (let reservation of reservations) {
      for (let singleRes of reservation) {
        totalEarnings += singleRes?.payment?.value
      }
    }
  }

  return (
    <div className="card-white pl-4 pl-lg-5">
      <h2 className="subtitle-dashboard font-montserrat">Mes numéros</h2>
      <div className="border-bottom my-4"></div>
      <div className="d-flex justify-content-between">
        <p>Hebergmenst a publié</p>
        <p>{store && store.length}</p>
      </div>
      <div className="border-bottom my-4"></div>
      <div className="d-flex justify-content-between">
        <p className="subtitle weight-bold">Total des gains</p>
        <p className="subtitle weight-bold">{totalEarnings}&euro;</p>
      </div>
    </div>
  )
}

export default HostStatistics
