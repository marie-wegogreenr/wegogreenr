import React, { useEffect, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
import PhothoInput from './inputs/PhothoInput'
import NameInput from './inputs/NameInput'
import GenreInput from './inputs/GenreInput'

import { useSelector } from 'react-redux'
import BirthdayInput from './inputs/BirthdayInput'
import TextInput from './inputs/TextInput'
import AdressInput from './inputs/AdressInput'
import PassWordInput from './inputs/PassWordInput'
import { useToast } from 'hooks/useToast'

const UserInfo = () => {
  const [notify] = useToast()
  const user = useSelector((store) => store.user.user)
  const updated = useSelector((store) => store.user.status)

  // useEffect(() => {
  //   if (typeof updated == 'string' && updated.includes('Error')) {
  //     notify('Mise à jour des erreurs :(', 'error')
  //   } else if (updated !== 200 && updated !== 404 && updated !== 'UPDATED') {
  //     notify(updated, 'success')
  //   }
  // }, [updated])

  const {
    name,
    lastname,
    birthday,
    email,
    phone,
    country,
    address,
    country_id,
    postal_code,
    photo_url,
    gender,
    city
  } = user

  return (
    <div className="card-white p-4 p-lg-5">
      <h2 className="weight-bold font-montserrat subtitle-dashboard">
        Informations personnelles
      </h2>
      <div className="border-bottom mt-4 mb-4"></div>
      <PhothoInput profilePicture={photo_url} />
      <NameInput name={name} lastname={lastname} />
      <TextInput label="Email" value={email} formName="email" />
      <TextInput label="Téléphone" formName="phone" value={phone} type="tel" />
      <PassWordInput />
      <AdressInput
        address={address}
        country={country}
        country_id={country_id}
        postalCode={postal_code}
        city={city}
      />
      <GenreInput genre={gender} />
      <BirthdayInput birthday={birthday} />
    </div>
  )
}

export default UserInfo
