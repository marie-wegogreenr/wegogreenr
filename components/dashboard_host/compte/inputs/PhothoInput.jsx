import React, { useEffect, useRef, useState } from 'react'
import Image from 'next/image'
import { getTokenFromCookie } from 'utils'
import { useSelector, useDispatch } from 'react-redux'
import moment from 'moment'
//redux
import { getUserDataAction } from '@ducks/userDuck'
import { useToast } from 'hooks/useToast'
import { useRouter } from 'next/router'
import { getUsers } from 'services/userServices'
import { setUserToEdit } from '@ducks/AdminUser/actions'

const PhothoInput = ({ profilePicture, isAdminUpdate, idUser, userData }) => {
  const router = useRouter()
  const [notify] = useToast()
  const dispatch = useDispatch()
  const [photo, setPhoto] = useState({
    photo: null,
    url: null
  })
  const [showInput, setShowInput] = useState(false)

  const inputFileRef = useRef()

  const _user = useSelector((store) => store.user.user)
  const [user, setUser] = useState({})
  const [isSaved, setIsSaved] = useState(false)

  useEffect(() => {
    if (isAdminUpdate) {
      setUser({ ...userData })
    } else {
      setUser({ ..._user })
    }
  }, [])

  const {
    phone,
    name,
    email,
    birthday,
    lastname,
    address,
    country_id,
    gender,
    postal_code
  } = user

  const handleSubmit = (e) => {
    e.preventDefault()
    const formData = new FormData()
    const defaultDate = moment().subtract(18, 'years').format('YYYY-MM-DD')
    formData.append('photo', photo.photo)
    formData.append('establishment_id', 9)
    formData.append('name', name)
    formData.append('birthday', birthday !== null ? birthday : defaultDate)
    formData.append('email', email)
    formData.append('lastname', lastname)
    address ? formData.append('address', address) : ''
    country_id ? formData.append('country_id', country_id) : ''
    phone ? formData.append('phone', phone) : ''
    gender ? formData.append('gender', gender) : ''
    postal_code ? formData.append('postal_code', postal_code) : ''

    if (idUser) {
      formData.append('user_id', idUser)
    }

    const token = getTokenFromCookie()

    const options = {
      headers: {
        Authorization: `Bearer ${token}`
      },
      method: 'POST',
      body: formData
    }

    fetch(`${process.env.NEXT_PUBLIC_API_URL}update-user`, options)
      .then((res) => res.json())
      .then((data) => {
        notify(data.message, 'success')
        dispatch(getUserDataAction())
        setShowInput(!showInput)
      })
      .catch((err) => {
        notify('Erreur de mise à jour de la photo de profil :(', 'error')
        setPhoto({})
        setShowInput(!showInput)
      })

    if (isAdminUpdate) {
      const getData = async () => {
        let response = await getUsers(1, { q: user.name })
        let dataFiltered = response.data.find((item) => item.id === user.id)
        if (!dataFiltered) {
          response = await getUsers(2, { q: user.name })
          dataFiltered = response.data.find((item) => item.id === user.id)
        }
        dispatch(setUserToEdit(dataFiltered))
      }
      setTimeout(() => {
        getData()
      }, 500)
    }
  }

  const handleChange = (e) => {
    if (e.target.files[0].size > 2000000) {
      notify('la taille du fichier ne doit pas dépasser 2 Mo', 'warning')

      setPhoto({
        photo: null
      })
      return false
    }
    setPhoto({
      photo: e.target.files[0],
      url: URL.createObjectURL(e.target.files[0])
    })

    setShowInput(!showInput)
  }

  return (
    <form encType="multipart/form-data" onSubmit={handleSubmit}>
      <div className="profile__photo-input">
        {showInput && (
          <div style={{ height: '92px' }}>
            <img
              width="92"
              height="92"
              src={photo.url}
              className="rounded-circle img-fluid h-100 obj-fit-cover"
            />
          </div>
        )}
        {!showInput && (
          <Image
            width={92}
            height={92}
            src={`${process.env.NEXT_PUBLIC_AMAZON_IMGS}${
              isSaved && isAdminUpdate ? user.photo_url : profilePicture
            }`}
            className="rounded-circle img-fluid obj-fit-cover"
          />
        )}

        {!showInput && (
          <a
            className="text weight-semibold"
            href="#"
            onClick={() => inputFileRef.current.click()}
          >
            <input
              ref={inputFileRef}
              type="file"
              onChange={handleChange}
              accept="image/jpeg, image/jpg, image/png"
              className="d-none"
            />
            Modifier / Ajouter une photo
          </a>
        )}
        {showInput && (
          <input
            type="submit"
            value="Sauvegarder la photo"
            className="btn btn-secondary py-2 mb-0"
          />
        )}
      </div>
    </form>
  )
}

export default PhothoInput
