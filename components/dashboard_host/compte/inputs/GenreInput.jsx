import React, { useState, useEffect } from 'react'
import genres from './../../../../constants/genres'
import { CircleClose } from '@components/Icons'
//REDUX
import { updateUserInfoAction, getUserDataAction } from '@ducks/userDuck'
import { useDispatch } from 'react-redux'
import { useToast } from 'hooks/useToast'

const GenreInput = ({ genre, isAdminUpdate, idUser, userData }) => {
  const dispatch = useDispatch()

  const [isEditing, setIsEditing] = useState(false)
  const [notify] = useToast()
  const [form, setForm] = useState({})

  useEffect(() => {
    setForm({
      genre
    })
  }, [])

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  const handleSubmit = () => {
    dispatch(
      updateUserInfoAction(
        'gender',
        form.genre,
        idUser,
        isAdminUpdate,
        userData
      )
    )
    dispatch(getUserDataAction())
    notify(`Modifications sauvegardées`, 'success')
    setIsEditing(false)
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'} mt-3`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <p className="text-block m-0 weight-bold">Genre</p>
            <p className="paragraph mt-2">{form.genre}</p>
          </div>
          <a
            className="weight-semibold text"
            onClick={() => setIsEditing(true)}
            href="#"
          >
            Modifier
          </a>
        </div>
      </div>

      {isEditing && (
        <>
          <div className="d-flex justify-content-between mt-3">
            <p className="subtitle-dashboard weight-bold">Genre</p>
            <a
              className="weight-bold text color-black"
              href="#"
              onClick={() => setIsEditing(false)}
            >
              <span className="mr-2">
                <CircleClose />
              </span>
              Annuler
            </a>
          </div>
          <div className="d-flex">
            <div className="profile__gender-input-select">
              <select
                name="genre"
                className="w-100 mt-2 form-control"
                onChange={handleChange}
              >
                {genres.map((item, index) => (
                  <option
                    onChange={handleChange}
                    value={item}
                    key={index}
                    selected={item === form.genre}
                  >
                    {item}
                  </option>
                ))}
              </select>
            </div>
          </div>
          <button
            onClick={handleSubmit}
            className="btn btn-secondary weight-bold paragraph py-3 profile__save-btn"
          >
            Sauvegarder
          </button>
          <div className="border-bottom my-5"></div>
        </>
      )}
    </>
  )
}

export default GenreInput
