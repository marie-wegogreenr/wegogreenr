import React, { useEffect, useState } from 'react'
//REDUX
import { useDispatch } from 'react-redux'
import { updateUserInfoAction } from '@ducks/userDuck'
//ICONS
import { CircleClose } from '@components/Icons'
import { useToast } from 'hooks/useToast'

const TextInput = ({
  label,
  value = '',
  formName,
  editable = true,
  idUser,
  isAdminUpdate,
  userData
}) => {
  const dispatch = useDispatch()

  const [isEditing, setIsEditing] = useState(false)
  const [notify] = useToast()
  const [form, setForm] = useState({})

  useEffect(() => {
    setForm({
      [formName]: value
    })
  }, [value])

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }
  const handleSubmit = () => {
    if (form[formName] !== value)
      dispatch(
        updateUserInfoAction(
          formName,
          form[formName],
          idUser,
          isAdminUpdate,
          userData
        )
      )
    notify(`Modifications sauvegardées`, 'success')
    setIsEditing(false)
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'} mt-3`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <p className="text-block weight-bold m-0">{label}</p>
            <p className="paragraph mt-2">{form[formName]}</p>
          </div>
          <a
            className="text weight-semibold"
            onClick={() => setIsEditing(true)}
            href="#"
          >
            {editable && 'Modifier'}
          </a>
        </div>
      </div>

      {isEditing && (
        <>
          <div className="d-flex justify-content-between mt-3">
            <p className="subtitle-dashboard weight-bold">{label}</p>
            <a
              className="color-black text weight-bold"
              href="#"
              onClick={() => setIsEditing(false)}
            >
              <span className="mr-2">
                <CircleClose />
              </span>
              Annuler
            </a>
          </div>
          <div className="d-flex">
            <div className="w-50">
              <input
                type="text"
                name={formName}
                value={form[formName]}
                onChange={handleChange}
                className="mt-2 w-100 form-control"
              />
            </div>
          </div>
          <button
            onClick={handleSubmit}
            className="btn btn-secondary py-3 w-25 weight-bold paragraph"
          >
            Sauvegarder
          </button>
          <div className="border-bottom my-5"></div>
        </>
      )}
    </>
  )
}

export default TextInput
