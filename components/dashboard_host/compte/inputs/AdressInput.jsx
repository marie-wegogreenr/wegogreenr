import React, { useEffect, useState } from 'react'
import countries from 'constants/countries'
//redux
import { useDispatch } from 'react-redux'
import { updateUserInfoAction } from '@ducks/userDuck'
//ASSETS
import { CircleClose } from '@components/Icons'
import Cookies from 'universal-cookie'

import SearchAlgoia from '@components/Algolia/SearchAlgolia'
import { useToast } from 'hooks/useToast'
import { Loader } from 'components/'

const AdressInput = ({
  country,
  address = '',
  address2 = '',
  extraAdress,
  postalCode = '',
  city,
  country_id,
  isAdminUpdate,
  idUser,
  userData
}) => {
  const dispatch = useDispatch()

  const [isEditing, setIsEditing] = useState(false)

  const [form, setForm] = useState({})
  const [notify] = useToast()
  useEffect(async () => {
    const cookies = new Cookies()
    //Get countries
    const res_countrie = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'countries',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_countries = await res_countrie.json()
    setForm({
      address,
      address2,
      extraAdress,
      postalCode,
      city,
      country,
      country_id,
      countries: json_countries
    })
  }, [address, extraAdress, postalCode, city])

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  const onSelectOption = (selected_value) => {
    //consoleLog(selected_value)
    setForm({
      ...form,
      city: selected_value.name
    })
  }

  const handleChangeVille = (event) => {
    const { value } = event.target

    if (typeof value === 'string') {
      setForm({ ...form, city: value })
    }
  }

  const showAddress = () => {
    let _address = form?.address || ''
    let _postalCode = form?.postalCode || ''
    let _city = form?.city || ''
    let _country = form?.country?.name || ''

    if (!_address && !_postalCode && !_city) {
      return `${_country}`
    } else {
      return `${_address} ${_postalCode} ${_city}, ${_country}`
    }
  }

  const handleSubmit = () => {
    if (form.address !== address)
      dispatch(
        updateUserInfoAction(
          'address',
          `${form.address}`,
          idUser,
          isAdminUpdate,
          userData
        )
      )
    if (form.postalCode !== postalCode)
      dispatch(
        updateUserInfoAction(
          'postal_code',
          `${form.postalCode}`,
          idUser,
          isAdminUpdate,
          userData
        )
      )
    if (form.country_id !== country_id)
      dispatch(
        updateUserInfoAction(
          'country_id',
          `${form.country_id}`,
          idUser,
          isAdminUpdate,
          userData
        )
      )
    if (form.city !== city)
      dispatch(
        updateUserInfoAction(
          'city',
          `${form.city}`,
          idUser,
          isAdminUpdate,
          userData
        )
      )

    const dispatchCountry = (countryToShow) => {
      dispatch(
        updateUserInfoAction(
          'country',
          countryToShow,
          idUser,
          isAdminUpdate,
          userData
        )
      )
    }
    const updateCountry = () => {
      const countryToShow = form.countries?.filter(
        (item) => item.id === parseInt(form.country_id)
      )
      setForm({ ...form, country: countryToShow[0] })
      dispatchCountry(countryToShow[0])
    }

    updateCountry()

    notify(`Modifications sauvegardées`, 'success')
    setIsEditing(false)
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'} mt-3`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <p className="text-block m-0 weight-bold">Adresse postale</p>
            {!form.country && !isAdminUpdate ? (
              <Loader />
            ) : (
              <p className="paragraph mt-2">{showAddress()}</p>
            )}
          </div>
          <a
            className="text weight-bold"
            onClick={() => setIsEditing(true)}
            href="#"
          >
            Modifier
          </a>
        </div>
      </div>

      {isEditing && (
        <>
          <div className="d-flex justify-content-between mt-3">
            <p className="weight-bold subtitle-dashboard">Adresse postale</p>
            <a
              className="weight-bold text color-black"
              href="#"
              onClick={() => setIsEditing(false)}
            >
              <span className="mr-2">
                <CircleClose />
              </span>
              Annuler
            </a>
          </div>
          <div className="row">
            <div className="col-12 col-md-6">
              <p className="text-block weight-bold">Pays</p>
              <select
                defaultValue=""
                className="form-control"
                name="country_id"
                onChange={handleChange}
                value={form.country_id}
              >
                <option value=""></option>
                {form.country_id
                  ? form?.countries.map((value, index) => {
                      if (form.country_id == value.id) {
                        return (
                          <option key={index} value={value.id} selected>
                            {value.name}
                          </option>
                        )
                      } else {
                        return (
                          <option key={index} value={value.id}>
                            {value.name}
                          </option>
                        )
                      }
                    })
                  : form.countries.map((value, index) => {
                      return (
                        <option key={index} value={value.id}>
                          {value.name}
                        </option>
                      )
                    })}
              </select>
            </div>
            <div className="col-12 col-md-6 form-group">
              <p className="text-block weight-bold">Ville</p>
              <SearchAlgoia
                onChange={handleChangeVille}
                inputValue={form.city || ''}
                index="cities"
                placeholder="Ville"
                attribute="name"
                onSelectOption={onSelectOption}
                defaultValue={form.city}
              />
            </div>
          </div>
          <div className="d-flex">
            <div className="w-50">
              <p className="text-block weight-bold">Adresse</p>
              <input
                type="text"
                name="address"
                value={form.address}
                onChange={handleChange}
                className="w-100"
              />
            </div>
            <div className="ml-0 ml-lg-4 w-50">
              <p className="text-block weight-bold">Code postal</p>
              <input
                type="text"
                name="postalCode"
                value={form.postalCode}
                onChange={handleChange}
                className="w-100"
              />
            </div>
          </div>
          <button
            onClick={handleSubmit}
            className="btn btn-secondary weight-bold paragraph w-25 py-3 mb-4"
          >
            Sauvegarder
          </button>
          <div className="border-bottom my-5"></div>
        </>
      )}
    </>
  )
}

export default AdressInput
