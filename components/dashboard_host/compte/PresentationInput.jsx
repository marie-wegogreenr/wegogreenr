import React, { useEffect, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
import { useSelector } from 'react-redux'
import { getTokenFromCookie } from 'utils'

const PresentationInput = () => {
  const presentation = useSelector(
    (store) => store?.user?.user?.host_information?.presentation
  )
  const hostInfo = useSelector((store) => store?.user?.user?.host_information)

  const [data, setData] = useState({
    presentation: ''
  })
  const [isEditing, setIsEditing] = useState(false)
  useEffect(() => {
    setData({ presentation })
  }, [presentation])

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value })
  }

  const handleSubmit = () => {
    const token = getTokenFromCookie()
    const options = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        presentation: data.presentation,
        additional_information: hostInfo.additional_information,
        how_heard_about_id: hostInfo.how_heard_about_id
      }),
      method: 'PUT'
    }
    fetch(
      `${process.env.NEXT_PUBLIC_API_URL}host-informations/${hostInfo.id}`,
      options
    )
      .then((res) =>
        res.json().then((data) => {
          /* consoleLog(data) */
        })
      )
      .catch((err) => console.error(err))
    setIsEditing(false)
  }

  return (
    <div className="card-white p-4 p-lg-5">
      <div className="d-flex justify-content-between align-items-center">
        <h2 className="weight-bold font-montserrat">Votre présentation</h2>
        <a
          onClick={() => setIsEditing(!isEditing)}
          href="#"
          className="text weight-semibold"
        >
          Modifier
        </a>
      </div>
      <textarea
        onChange={handleChange}
        disabled={!isEditing}
        name="presentation"
        id=""
        rows="10"
        value={data['presentation']}
        className="w-100 p-3"
      ></textarea>
      {isEditing && (
        <div>
          <p className="color-s">Conseil :</p>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit.
            Consequuntur nostrum aliquam quas, quo facilis minus tenetur odio
            porro assumenda esse.
          </p>

          <button
            onClick={handleSubmit}
            className="btn btn-secondary paragraph weight-semibold py-3 br-10 px-5"
          >
            Sauvegarder
          </button>
        </div>
      )}
    </div>
  )
}

export default PresentationInput
