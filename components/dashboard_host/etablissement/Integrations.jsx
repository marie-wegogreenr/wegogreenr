import { ArIntegration } from '@components/Onboarding/ArIntegration'

export const Integrations = () => {
  return (
    <div>
      <ArIntegration from={'dashboard'} />
    </div>
  )
}
