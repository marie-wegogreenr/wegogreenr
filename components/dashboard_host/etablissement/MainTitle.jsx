import { updateEstablishment } from '@ducks/host/establishment/services'
import { useToast } from 'hooks/useToast'
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

export const MainTitle = () => {
  const [notify] = useToast()
  const dispatch = useDispatch()
  const {
    establishment: { data, status }
  } = useSelector((state) => state)

  const [active, setActive] = useState(data?.active || false)
  useEffect(() => {
    if (active !== data?.active) {
      setActive(data?.active)
    }
  }, [data?.active])

  const changeStatus = () => {
    const { id: establishmentId, ...rest } = data
    const dataToUpdate = {
      ...rest,
      id: establishmentId,
      is_active: !active
    }
    setActive(!active)
    dispatch(updateEstablishment(establishmentId, dataToUpdate)).then(() => {
      notify(`Modifications sauvegardées`, 'success')
    })
  }
  if (data?.origin === 4 && status === 'SUCCESS') {
    return (
      <div className="custom-control custom-switch custom-switch-lg">
        <input
          type="checkbox"
          className="custom-control-input"
          id="id_switch_1"
          checked={active}
          onClick={changeStatus}
        />
        <label
          className="custom-control-label ml-2"
          htmlFor="id_switch_1"
          style={{ cursor: 'pointer' }}
        >
          {active ? 'Publiée' : 'Non Publiée'}
        </label>
      </div>
    )
  } else {
    return <></>
  }
}
