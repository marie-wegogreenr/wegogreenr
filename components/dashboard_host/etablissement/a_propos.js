import React from 'react'
import { consoleLog } from '/utils/logConsole'

import Cookies from 'universal-cookie'
import Router from 'next/router'
import Link from 'next/link'
import { toast } from 'react-toastify'
class APropos extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user_data: '',
      stablishments: [],
      editMode: true,
      establishment_equipments: [],
      equipments: []
    }

    this.handleChekedPositionAPropos =
      this.handleChekedPositionAPropos.bind(this)
    this.handleAPropos = this.handleAPropos.bind(this)
    this.handleEditMode = this.handleEditMode.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    let _this = this

    //Get user data
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get establishment
    const res_stablishments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments = await res_stablishments.json()
    this.setState({ ...this.state, stablishments: json_stablishments })

    //Get establishment equipments
    const res_establishment_equipments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishment-equipments/' +
        this.state.stablishments[0].id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_establishment_equipments =
      await res_establishment_equipments.json()
    this.setState({
      ...this.state,
      establishment_equipments: json_establishment_equipments
    })

    //Get equipments
    const res_equipments = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'equipments',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_equipments = await res_equipments.json()
    this.setState({ ...this.state, equipments: json_equipments })

    let equipments = this.state.equipments

    equipments.forEach((equipment) => {
      equipment.isChecked = false
    })

    this.setState({ ...this.state, equipments: equipments })
    //consoleLog(this.state.equipments);
    let equipments_2 = this.state.equipments

    this.state.establishment_equipments.forEach((establishment_equipment) => {
      equipments_2.forEach((equipment) => {
        if (equipment.id == establishment_equipment.equipment_id) {
          equipment.isChecked = true
        }
      })
    })

    this.setState({ ...this.state, equipments: equipments })
  }

  //Change Checked position
  async handleChekedPositionAPropos(event) {
    let equipments = this.state.equipments
    equipments.forEach((equipment) => {
      //consoleLog(equipment.id, event.target.value);
      if (equipment.id === parseInt(event.target.value))
        equipment.isChecked = event.target.checked

      //consoleLog(equipment);
    })
    this.setState({ equipments: equipments })
    //consoleLog(this.state.equipments);
  }

  //POST save information establishment

  async handleAPropos(event) {
    event.preventDefault()

    const cookies = new Cookies()

    const res = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.stablishments[0].id,
      {
        body: JSON.stringify({
          name: this.state.stablishments[0].name,
          type_id: this.state.stablishments[0].type_id,
          clasification_id: this.state.stablishments[0].clasification_id,
          address: this.state.stablishments[0].address,
          country_id: this.state.stablishments[0].country_id,
          contact_name: this.state.stablishments[0].contact_name,
          contact_lastname: this.state.stablishments[0].contact_lastname,
          contact_email: this.state.stablishments[0].contact_email,
          contact_phone: this.state.stablishments[0].contact_phone,
          description: event.target.place_description.value,
          min_entry_time: this.state.stablishments[0].min_entry_time,
          max_entry_time: this.state.stablishments[0].max_entry_time,
          max_exit_time: this.state.stablishments[0].max_exit_time,
          tva: this.state.stablishments[0].tva,
          decl_inc_pro: this.state.stablishments[0].decl_inc_pro,
          amount_taxe: this.state.stablishments[0].amount_taxe,
          slug: this.state.stablishments[0].slug
        }),
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'PUT'
      }
    )

    const result = await res.json()
    const status = await res.status
    //consoleLog(result);
    //consoleLog(status);
    if (status == 200) {
      this.state.equipments.forEach((equipment) => {
        if (equipment.isChecked == true) {
          fetch(process.env.NEXT_PUBLIC_API_URL + 'establishment-equipments', {
            body: JSON.stringify({
              establishment_id: this.state.stablishments[0].id,
              equipment_id: equipment.id
            }),
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'POST'
          })
        } else {
          fetch(
            process.env.NEXT_PUBLIC_API_URL + 'delete-establishment-equipment',
            {
              body: JSON.stringify({
                establishment_id: this.state.stablishments[0].id,
                equipment_id: equipment.id
              }),
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'POST'
            }
          )
        }
      })
      toast.success('À propos mis à jour', {
        position: toast.POSITION.BOTTOM_LEFT
      })
      this.handleEditMode()
    } else {
      toast.error(`Une erreur s'est produite, réessayez plus tard`, {
        position: toast.POSITION.BOTTOM_LEFT
      })
    }
  }

  handleEditMode() {
    this.setState({ ...this.state, editMode: !this.state.editMode })
  }

  render() {
    return (
      <form className="form" onSubmit={this.handleAPropos}>
        <div className="card rounded-0">
          <div className="card-body">
            <div className="row">
              <p className="col-6">À propos de l'établissement</p>
              <a
                className="col-6 text-right"
                href="#"
                onClick={this.handleEditMode}
              >
                {!this.state.editMode == false ? 'Modifier' : 'Annuler'}
              </a>
            </div>
            <div className="form-group">
              <label htmlFor="place_description">
                Description générale de votre établissement *
              </label>
              <br />
              <textarea
                className="form-control"
                name="place_description"
                id="place_description"
                required
                defaultValue={
                  this.state.stablishments.length > 0
                    ? this.state.stablishments[0].description
                    : ''
                }
                disabled={this.state.editMode}
              ></textarea>
            </div>
            {!this.state.editMode && (
              <div className="col-4">
                <button
                  type="submit"
                  className="btn btn-success btn-block btn-lg"
                >
                  Sauvegarder
                </button>
              </div>
            )}
          </div>
        </div>
        <br />
        <div className="card rounded-0">
          <div className="card-body">
            <div className="row">
              <p className="col-6">Equipments de l'établissement</p>
              <a
                className="col-6 text-right"
                href="#"
                onClick={this.handleEditMode}
              >
                {!this.state.editMode == false ? 'Modifier' : 'Annuler'}
              </a>
            </div>
            <div className="row">
              {this.state.equipments.map((value, index) => {
                return (
                  <div className="col-6" key={index}>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value={value.id}
                        id={value.id}
                        onClick={this.handleChekedPositionAPropos}
                        checked={value.isChecked}
                        disabled={this.state.editMode}
                      />
                      <label className="form-check-label" htmlFor={value.id}>
                        {value.name}
                      </label>
                    </div>
                  </div>
                )
              })}
            </div>
            {!this.state.editMode && (
              <div className="col-4">
                <button
                  type="submit"
                  className="btn btn-success btn-block btn-lg"
                >
                  Sauvegarder
                </button>
              </div>
            )}
          </div>
        </div>
        <br />
      </form>
    )
  }
}

export default APropos
