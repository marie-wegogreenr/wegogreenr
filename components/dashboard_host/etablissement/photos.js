/*  React  */
import React from 'react'
/*  Redux  */
import { connect } from 'react-redux'
import { setClearLoading } from '@ducks/Config/actions'
/*  Dependencies  */
import Cookies from 'universal-cookie'
import moment from 'moment'
import { toast } from 'react-toastify'
import {
  GridContextProvider,
  GridDropZone,
  GridItem,
  swap
} from 'react-grid-dnd'
/*  Components  */
import { Loader } from 'components/'
/*  Helpers  */
import { ChangePositionImage, resizeFile } from 'helpers/imagesHelper'
/*  Services */
import {
  deleteEtablissementImages,
  getEtablissementImages,
  saveEtablissementImage,
  saveEtablissementImage2,
  updateEtablissementImagePosition
} from 'services/imagesService'

class StepSix extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      image_url: [],
      image_url_delete: [],
      arrayPositions: [],
      showPhotos: false,
      isLoading: false,
      toUpdate: false
    }
    this.inputFileRef = React.createRef()
    this.handleUploadImage = this.handleUploadImage.bind(this)
    this.handleDeleteImage = this.handleDeleteImage.bind(this)
    this.handlePhotos = this.handlePhotos.bind(this)
    this.onChange = this.onChange.bind(this)
    this.getImages = this.getImages.bind(this)
  }

  async componentDidMount() {
    this.getImages()
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.establishment !== this.props.establishment) {
      this.getImages()
    }
  }

  /**
   *  Get establishment images
   */
  async getImages() {
    if (typeof window === undefined) return
    let _id
    /*  Validation to distinguish between admin and host dashboard */
    if (window.location.href.includes('admin')) {
      _id = window.location.href.split('/')[5]
    } else {
      if (this.props.establishment) {
        _id = this.props.id_etablisement
      }
    }
    const json_images = await getEtablissementImages(_id)

    let images_url = []
    let arrayPositions = []
    json_images.forEach(async (images, index) => {
      images_url.push({
        id: images.id,
        image: process.env.NEXT_PUBLIC_AMAZON_IMGS + images.image.url,
        position: images.order,
        uploadPercent: 100
      })
      arrayPositions.push(index + 1)
    })
    this.setState({
      ...this.state,
      image_url: images_url,
      arrayPositions: arrayPositions,
      showPhotos: true
    })
  }

  async handleUploadImage(event) {
    if (event.target.files.length > 0) {
      const saveImages = Array.from(event.target.files).map(async (file) => {
        // for (let i = 0; i < event.target.files.length; i++) {
        if (file.size >= 5000000) {
          toast.warn(
            'Photo ' +
              file.name +
              ' en grand, veuillez charger des photos de moins de 5Mo',
            {
              position: toast.POSITION.BOTTOM_LEFT
            }
          )
          return {}
        } else {
          const resp = await resizeFile(file)
          const image = URL.createObjectURL(resp)
          const blobNewFile = resp.slice(0, resp.size, resp.type)

          const name = file.name
          const lastDot = name.lastIndexOf('.')
          const fileName = name.substring(0, lastDot)
          const ext = name.substring(lastDot + 1)
          const reducedNewFile = new File(
            [blobNewFile],
            fileName + moment().format('YYYYMMDDhhmmssSS') + '.' + ext,
            { type: blobNewFile.type }
          )

          let arrayPositions = this.state.arrayPositions
          let lentgArrayPositions = this.state.arrayPositions.length + 1
          const newImage = {
            id: null,
            image: image,
            file: reducedNewFile,
            position: lentgArrayPositions,
            uploadPercent: 0
          }
          arrayPositions.push(lentgArrayPositions)
          this.setState({
            ...this.state,
            image_url: [...this.state.image_url, newImage],
            arrayPositions: arrayPositions
          })
          return this.sendImagesToSave(newImage)
        }
      })
      const resp = await Promise.all(saveImages)
      resp.forEach(({ picture, message }) => {
        this.setState({
          ...this.state,
          image_url: this.state.image_url.map((i) => {
            if (i.position === parseInt(picture.order)) {
              return {
                ...i,
                id: picture.id,
                image: process.env.NEXT_PUBLIC_AMAZON_IMGS + message
              }
            }
            return i
          })
        })
      })
    }
    event.target.value = ''
  }

  async handleDeleteImage(index_in) {
    const elementToDelete = this.state.image_url.find(
      (item) => item.position === index_in
    )
    let arrayPositions = []
    let arrayImages = this.state.image_url
      .filter((item) => item.position !== index_in)
      .map((ai, index) => {
        const arrIndex = index + 1
        arrayPositions.push(arrIndex)
        return arrIndex !== ai.position
          ? { ...ai, isModified: true, position: arrIndex }
          : ai
      })

    this.setState({
      ...this.state,
      image_url: arrayImages,
      image_url_delete:
        elementToDelete.id !== null
          ? [...this.state.image_url_delete, elementToDelete]
          : this.state.image_url_delete,
      arrayPositions
    })
  }

  async handlePhotos(event) {
    event.preventDefault()

    if (this.state.image_url.length < 1) {
      return toast.warn('Un minimum de 1 images est requis', {
        position: toast.POSITION.BOTTOM_LEFT
      })
    }

    this.props.setClearLoading(true)

    const deleteResponse = await this.handleDeleteImages()
    if (!deleteResponse) {
      return toast.warn("Une erreur s'est produite, réessayez plus tard", {
        position: toast.POSITION.BOTTOM_LEFT
      })
    }

    if (this.state.toUpdate) {
      const updatePromises = this.state.image_url.map((image, index) =>
        updateEtablissementImagePosition(image.id, index + 1)
      )
      const updateResp = await Promise.all(updatePromises)
    }

    this.setState({
      ...this.state,
      image_url_delete: [],
      toUpdate: false
    })

    this.props.setClearLoading(false)
    toast.success('Photos mises à jour', {
      position: toast.POSITION.BOTTOM_LEFT
    })
  }

  sendImagesToSave = async (image) => {
    let _id
    if (window.location.href.includes('admin')) {
      _id = window.location.href.split('/')[5]
    } else {
      _id = this.props.establishment.id
    }

    const formData = new FormData()
    formData.append('image', image.file)
    formData.append('establishment_id', _id)
    formData.append('order', image.position)
    const resp = await saveEtablissementImage2(
      formData,
      this.uploadChargeProgress
    )
    // this.notifyUploadPhoto()
    return resp
  }

  uploadChargeProgress = async (position, progress) => {
    this.setState({
      ...this.state,
      image_url: this.state.image_url.map((i) => {
        if (i.position === parseInt(position)) {
          return { ...i, uploadPercent: progress }
        }
        return i
      })
    })
  }

  handleDeleteImages = async () => {
    let succsesRespones = true
    if (this.state.image_url_delete.length === 0) return succsesRespones

    const promises = this.state.image_url_delete.map((image) =>
      deleteEtablissementImages(image.id)
    )
    const resp = await Promise.all(promises)
    resp.forEach((r) => {
      if (r.code !== 200) {
        succsesRespones = false
      }
    })
    return succsesRespones
  }

  handleDefaultImage = (e) => {
    e.target.src = '/images/not-image.svg'
  }

  onChange(sourceId, sourceIndex, targetIndex, targetId) {
    const result = swap(this.state.image_url, sourceIndex, targetIndex)
    this.setState({ ...this.state, image_url: result, toUpdate: true })
  }

  render() {
    return (
      <form className="form mb-3 mt-4" onSubmit={this.handlePhotos}>
        <div className="row">
          <div className="col-12">
            <div className="card-white rooms-form__images-box">
              <div className="row rooms-form__row-info">
                <div className="col-6 rooms-form__row-info-title">
                  <h3>Photos</h3>
                </div>
                <div className="col-6 rooms-form__row-info-limit">
                  <div>
                    <button
                      className="btn btn-primary mr-4"
                      type="button"
                      onClick={() => {
                        this.inputFileRef.current.click()
                      }}
                    >
                      Ajouter
                    </button>
                    <button type="submit" className="btn btn-secondary">
                      Sauvegarder
                    </button>
                  </div>
                  <p className="rooms-form__row-info-text">Au moins 1 images</p>
                </div>
              </div>
              <GridContextProvider onChange={this.onChange}>
                <GridDropZone
                  id="items"
                  boxesPerRow={4}
                  rowHeight={150}
                  style={{
                    height: `${this.state.image_url.length * 30 + 200}px`
                  }}
                >
                  {this.state.image_url.map((value, index) => (
                    <GridItem key={value.position}>
                      <div className="grid-content grabbable"></div>
                      <div className="col-12 card-image change-position-image-box grid-item">
                        {value.uploadPercent !== 100 && (
                          <div class="progress image-progress">
                            <div
                              class="progress-bar bg-success"
                              role="progressbar"
                              style={{
                                width: `${value.uploadPercent}%`
                              }}
                              aria-valuenow={value.uploadPercent}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                        )}
                        <img
                          src={value.image}
                          onError={this.handleDefaultImage}
                          alt="Logo"
                          className={
                            'align-middle image-upload-hebergement image-upload-hebergement--no-margin'
                          }
                        />
                      </div>
                      {value.uploadPercent === 100 && (
                        <button
                          className="delete-image-stablishment"
                          key={index}
                          onClick={(e) => {
                            e.stopPropagation()
                            this.handleDeleteImage(value.position)
                          }}
                          type="button"
                        >
                          x
                        </button>
                      )}
                    </GridItem>
                  ))}
                  <div className="col-12 card-add">
                    <input
                      ref={this.inputFileRef}
                      id="add_image"
                      name="add_image"
                      type="file"
                      onChange={this.handleUploadImage}
                      multiple
                      accept="image/jpeg, image/jpg, image/png"
                    />
                  </div>
                </GridDropZone>
              </GridContextProvider>
            </div>
          </div>
        </div>
      </form>
    )
  }
}

const mapDispatchToProps = { setClearLoading }

const mapStateToProps = (state) => {
  return { establishment: state.user.user.establishment }
}

export default connect(mapStateToProps, mapDispatchToProps)(StepSix)
