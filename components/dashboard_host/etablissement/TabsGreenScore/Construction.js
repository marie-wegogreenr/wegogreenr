import { Component } from 'react'
import { toast } from 'react-toastify'
/* Libraries */
import Cookies from 'universal-cookie'

/**Components */
import { SectionLayout } from 'components/host/establishment'
import { Loader } from 'components/'

class Construction extends Component {
  constructor(props) {
    super(props)
    this.state = {
      //10
      materials: [],
      //9
      inside: [],
      //7
      outside: [],
      //6
      workout: [],
      //8
      installations: [],
      editMode: false,
      isShowContent: false
    }
  }

  async componentDidMount() {
    const cookies = new Cookies()
    //Get information materials
    const res_materials = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/10',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_materials = await res_materials.json()
    json_materials.forEach((materials) => {
      materials.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (materials.id == response.response_id) {
            materials.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, materials: json_materials })

    //Get information inside
    const res_inside = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/9',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_inside = await res_inside.json()
    json_inside.forEach((inside) => {
      inside.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (inside.id == response.response_id) {
            inside.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, inside: json_inside })

    //Get information outside
    const res_outside = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/7',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_outside = await res_outside.json()
    json_outside.forEach((outside) => {
      outside.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (outside.id == response.response_id) {
            outside.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, outside: json_outside })

    //Get information workout
    const res_workout = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/6',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_workout = await res_workout.json()
    json_workout.forEach((workout) => {
      workout.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (workout.id == response.response_id) {
            workout.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, workout: json_workout })

    //Get information installations
    const res_installations = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/8',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_installations = await res_installations.json()
    json_installations.forEach((installations) => {
      installations.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (installations.id == response.response_id) {
            installations.isChecked = true
          }
        })
      }
    })
    this.setState({
      ...this.state,
      installations: json_installations,
      isShowContent: true
    })
  }

  handleEditMode = async () => {
    this.setState({ ...this.state, editMode: !this.state.editMode })
  }

  handleChekedPositionMaterials = (event) => {
    let materials = this.state.materials
    materials.forEach((material) => {
      //consoleLog(material.id, event.target.value);
      if (material.id === parseInt(event.target.value))
        material.isChecked = event.target.checked

      //consoleLog(material);
    })
    this.setState({ materials: materials })
    //consoleLog(this.state.materials)
  }

  handleChekedPositionInside = async (event) => {
    let inside = this.state.inside
    inside.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(material);
    })
    this.setState({ inside: inside })
    //consoleLog(this.state.inside)
  }

  handleChekedPositionOutside = async (event) => {
    let outside = this.state.outside
    outside.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(material);
    })
    this.setState({ outside: outside })
    //consoleLog(this.state.outside)
  }

  handleChekedPositionWorkout = async (event) => {
    let workout = this.state.workout
    workout.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(material);
    })
    this.setState({ workout: workout })
    //consoleLog(this.state.workout)
  }

  handleChekedPositionInstallations = async (event) => {
    let installations = this.state.installations
    installations.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(material);
    })
    this.setState({ installations: installations })
    //consoleLog(this.state.installations)
  }

  handleGreenScore = async (event) => {
    event.preventDefault()
    const cookies = new Cookies()
    let array_of_arrays = [
      { name: 'materials', question: 10 },
      { name: 'inside', question: 9 },
      { name: 'outside', question: 7 },
      { name: 'workout', question: 6 },
      { name: 'installations', question: 8 }
    ]
    let _this = this
    let acum = 0
    array_of_arrays.forEach((value) => {
      //consoleLog(value)
      //consoleLog(_this.state)
      _this.state[value.name].forEach((condition) => {
        if (condition.isChecked == true) {
          fetch(process.env.NEXT_PUBLIC_API_URL + 'establishment-response', {
            body: JSON.stringify({
              user_id: _this.props.user_data.id,
              question_id: value.question,
              response_id: condition.id
            }),
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'POST'
          })
        } else {
          fetch(
            process.env.NEXT_PUBLIC_API_URL + 'delete-establishment-response',
            {
              body: JSON.stringify({
                user_id: _this.props.user_data.id,
                question_id: value.question,
                response_id: condition.id
              }),
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'POST'
            }
          )
        }
      })
      if (acum == array_of_arrays.length - 1) {
        toast.success('Construction éditée', {
          position: toast.POSITION.BOTTOM_LEFT
        })
        this.handleEditMode()
        this.props.onUpdateData('construction')
      }
      acum++
    })
  }

  render() {
    return (
      <form className="form" onSubmit={this.handleGreenScore}>
        <SectionLayout
          isModifyMode={this.state.editMode}
          onModify={this.handleEditMode}
          title="Construction"
          fromGreenscore={true}
        >
          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              {/* <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img src="/images/onboarding/greenscore/step_one/house_greenscore.png" className="green-score__card-head-img"></img>
              </div> */}
              <div className="col-12 col-xs-9 col-md-10 ms-0 ps-0 d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-12 text-justify">
                  <img
                    src="/images/onboarding/greenscore/step_one/house_greenscore.png"
                    className="me-2 me-lg-5 me-xl-2 green-score__card-head-img"
                  ></img>
                  Matériaux utilisés à la construction / rénovation naturels
                </h3>
                {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
              </div>
            </div>
            <hr className="mt-1" />
            {this.state.isShowContent ? (
              <div className="row">
                {this.state.materials.map((value, index) => {
                  return (
                    <div className="col-12 col-md-6 col-xl-6" key={index}>
                      <div className="form-check">
                        <input
                          className="form-check-input input-check-blue-dark"
                          type="checkbox"
                          value={value.id}
                          id={value.id}
                          onClick={this.handleChekedPositionMaterials}
                          checked={value.isChecked}
                          disabled={!this.state.editMode}
                        />
                        <div>
                          <label
                            className="form-check-label"
                            htmlFor={value.id}
                          >
                            {value.response}
                          </label>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            ) : (
              <Loader />
            )}
          </div>
          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img
                  src="/images/onboarding/greenscore/step_one/bed_greenscore.png"
                  className="green-score__card-head-img"
                ></img>
              </div>
              <div className="col-12 col-xs-9 col-md-10 row d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-12 text-justify">
                  Intérieur
                </h3>
                {/* <p className="col-12 text-justify">
                          De la construction à l'entretien en passant par les installations de l'établissement, We Go GreenR s'intéresse aux alternatives écoresponsables mises en place par vos soins
                          </p> */}
              </div>
            </div>
            <hr className="mt-1" />
            {this.state.isShowContent ? (
              <div className="row">
                {this.state.inside.map((value, index) => {
                  return (
                    <div className="col-12 col-md-6 col-xl-6" key={index}>
                      <div className="form-check">
                        <input
                          className="form-check-input input-check-blue-dark"
                          type="checkbox"
                          value={value.id}
                          id={value.id}
                          onClick={this.handleChekedPositionInside}
                          checked={value.isChecked}
                          disabled={!this.state.editMode}
                        />
                        <div>
                          <label
                            className="form-check-label"
                            htmlFor={value.id}
                          >
                            {value.response}
                          </label>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            ) : (
              <Loader />
            )}
          </div>
          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img
                  src="/images/onboarding/greenscore/step_one/out_greenscore.png"
                  className="green-score__card-head-img"
                ></img>
              </div>
              <div className="col-12 col-xs-9 col-md-10 row d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-12 text-justify">
                  Extérieur
                </h3>
                {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
              </div>
            </div>
            <hr className="mt-1" />
            {this.state.isShowContent ? (
              <div className="row">
                {this.state.outside.map((value, index) => {
                  return (
                    <div className="col-12 col-md-6 col-xl-6" key={index}>
                      <div className="form-check">
                        <input
                          className="form-check-input input-check-blue-dark"
                          type="checkbox"
                          value={value.id}
                          id={value.id}
                          onClick={this.handleChekedPositionOutside}
                          checked={value.isChecked}
                          disabled={!this.state.editMode}
                        />
                        <div>
                          <label
                            className="form-check-label"
                            htmlFor={value.id}
                          >
                            {value.response}
                          </label>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            ) : (
              <Loader />
            )}
          </div>
          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img
                  src="/images/onboarding/greenscore/step_one/bottle_greenscore.png"
                  className="green-score__card-head-img"
                ></img>
              </div>
              <div className="col-12 col-xs-9 col-md-10 row d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-12 text-justify">
                  Entretien
                </h3>
                {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
              </div>
            </div>
            <hr className="mt-1" />
            {this.state.isShowContent ? (
              <div className="row">
                {this.state.workout.map((value, index) => {
                  return (
                    <div className="col-12 col-md-6 col-xl-6" key={index}>
                      <div className="form-check">
                        <input
                          className="form-check-input input-check-blue-dark"
                          type="checkbox"
                          value={value.id}
                          id={value.id}
                          onClick={this.handleChekedPositionWorkout}
                          checked={value.isChecked}
                          disabled={!this.state.editMode}
                        />
                        <div>
                          <label
                            className="form-check-label"
                            htmlFor={value.id}
                          >
                            {value.response}
                          </label>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            ) : (
              <Loader />
            )}
          </div>
          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img
                  src="/images/onboarding/greenscore/step_one/instalations_greenscore.png"
                  className="green-score__card-head-img"
                ></img>
              </div>
              <div className="col-12 col-xs-9 col-md-10 row d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-12 text-justify">
                  Installations écologiques
                </h3>
                {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
              </div>
            </div>
            <hr className="mt-1" />
            {this.state.isShowContent ? (
              <div className="row">
                {this.state.installations.map((value, index) => {
                  return (
                    <div className="col-12 col-md-6 col-xl-6" key={index}>
                      <div className="form-check">
                        <input
                          className="form-check-input input-check-blue-dark"
                          type="checkbox"
                          value={value.id}
                          id={value.id}
                          onClick={this.handleChekedPositionInstallations}
                          checked={value.isChecked}
                          disabled={!this.state.editMode}
                        />
                        <div>
                          <label
                            className="form-check-label"
                            htmlFor={value.id}
                          >
                            {value.response}
                          </label>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            ) : (
              <Loader />
            )}
          </div>
          {this.state.editMode && (
            <div className="mt-4">
              <button type="submit" className="btn btn-secondary">
                Sauvegarder
              </button>
            </div>
          )}
        </SectionLayout>
      </form>
    )
  }
}

export default Construction
