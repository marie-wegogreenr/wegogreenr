import { Component } from 'react'
import { toast } from 'react-toastify'
/* Libraries */
import Cookies from 'universal-cookie'

/**Components */
import { SectionLayout } from 'components/host/establishment'
import { Loader } from 'components/'

class Food extends Component {
  constructor(props) {
    super(props)
    this.state = {
      //4
      food: [],
      editMode: false,
      isShowContent: false
    }
  }

  async componentDidMount() {
    const cookies = new Cookies()
    //Get information food
    const res_food = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/4',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_food = await res_food.json()
    json_food.forEach((value) => {
      value.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, food: json_food, isShowContent: true })
  }

  handleEditMode = async () => {
    this.setState({ ...this.state, editMode: !this.state.editMode })
  }

  handleChekedPositionFood = async (event) => {
    let food = this.state.food
    food.forEach((material) => {
      //consoleLog(material.id, event.target.value);
      if (material.id === parseInt(event.target.value))
        material.isChecked = event.target.checked

      //consoleLog(material);
    })
    this.setState({ food: food })
    //consoleLog(this.state.food)
  }

  handleGreenScore = async (event) => {
    event.preventDefault()
    const cookies = new Cookies()
    let array_of_arrays = [{ name: 'food', question: 4 }]
    let _this = this
    let acum = 0
    array_of_arrays.forEach((value) => {
      _this.state[value.name].forEach((condition) => {
        if (condition.isChecked == true) {
          fetch(process.env.NEXT_PUBLIC_API_URL + 'establishment-response', {
            body: JSON.stringify({
              user_id: _this.props.user_data.id,
              question_id: value.question,
              response_id: condition.id
            }),
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'POST'
          })
        } else {
          fetch(
            process.env.NEXT_PUBLIC_API_URL + 'delete-establishment-response',
            {
              body: JSON.stringify({
                user_id: _this.props.user_data.id,
                question_id: value.question,
                response_id: condition.id
              }),
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'POST'
            }
          )
        }
      })
      if (acum == array_of_arrays.length - 1) {
        toast.success('Nourriture éditée', {
          position: toast.POSITION.BOTTOM_LEFT
        })
        this.handleEditMode()
        this.props.onUpdateData('food')
      }
      acum++
    })
  }

  render() {
    return (
      <form className="form" onSubmit={this.handleGreenScore}>
        <SectionLayout
          isModifyMode={this.state.editMode}
          onModify={this.handleEditMode}
          title="Alimentation"
          fromGreenscore={true}
        >
          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img
                  src="/images/onboarding/greenscore/step_three/food_greenscore.png"
                  className="green-score__card-head-img"
                ></img>
              </div>
              <div className="col-12 col-xs-9 col-md-10 row d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-12 text-justify">
                  Alimentation sur place
                </h3>
                {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
              </div>
            </div>
            <hr className="mt-1" />
            {this.state.isShowContent ? (
              <div className="row">
                {this.state.food.map((value, index) => {
                  return (
                    <div className="col-12 col-md-6 col-xl-6" key={index}>
                      <div className="form-check">
                        <input
                          className="form-check-input input-check-blue-dark"
                          type="checkbox"
                          value={value.id}
                          id={value.id}
                          onClick={this.handleChekedPositionFood}
                          checked={value.isChecked}
                          disabled={!this.state.editMode}
                        />
                        <div>
                          <label
                            className="form-check-label"
                            htmlFor={value.id}
                          >
                            {value.response}
                          </label>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            ) : (
              <Loader />
            )}
          </div>
          {this.state.editMode && (
            <div className="mt-4">
              <button type="submit" className="btn btn-secondary">
                Sauvegarder
              </button>
            </div>
          )}
        </SectionLayout>
      </form>
    )
  }
}

export default Food
