import { Component } from 'react'
import { toast } from 'react-toastify'
/* Libraries */
import Cookies from 'universal-cookie'

/**Components */
import { SectionLayout } from 'components/host/establishment'
import { Loader } from 'components/'

class Activities extends Component {
  constructor(props) {
    super(props)
    this.state = {
      //1
      activity: [],
      //2
      another_actions: [],
      //3
      transports: [],
      editMode: false,
      isShowContent: false
    }
  }

  async componentDidMount() {
    const cookies = new Cookies()
    //Get information activity
    const res_activity = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/1',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_activity = await res_activity.json()
    json_activity.forEach((value) => {
      value.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, activity: json_activity })

    //Get information another_actions
    const res_another_actions = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/2',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_another_actions = await res_another_actions.json()
    json_another_actions.forEach((value) => {
      value.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, another_actions: json_another_actions })

    //Get information transports
    const res_transports = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/3',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_transports = await res_transports.json()
    json_transports.forEach((value) => {
      value.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({
      ...this.state,
      transports: json_transports,
      isShowContent: true
    })
  }

  handleEditMode = async () => {
    this.setState({ ...this.state, editMode: !this.state.editMode })
  }

  handleChekedPositionActivity = async (event) => {
    let activity = this.state.activity
    activity.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(item);
    })
    this.setState({ activity: activity })
    //consoleLog(this.state.activity)
  }

  handleChekedPositionAnotherActions = async (event) => {
    let another_actions = this.state.another_actions
    another_actions.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(item);
    })
    this.setState({ another_actions: another_actions })
    //consoleLog(this.state.activity)
  }

  handleChekedPositionTransports = async (event) => {
    let transports = this.state.transports
    transports.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(item);
    })
    this.setState({ transports: transports })
    //consoleLog(this.state.activity)
  }

  handleGreenScore = async (event) => {
    event.preventDefault()
    const cookies = new Cookies()
    let array_of_arrays = [
      { name: 'activity', question: 1 },
      { name: 'another_actions', question: 2 },
      { name: 'transports', question: 3 }
    ]
    let _this = this
    let acum = 0
    array_of_arrays.forEach((value) => {
      //consoleLog(value)
      //consoleLog(_this.state)
      _this.state[value.name].forEach((condition) => {
        if (condition.isChecked == true) {
          fetch(process.env.NEXT_PUBLIC_API_URL + 'establishment-response', {
            body: JSON.stringify({
              user_id: _this.props.user_data.id,
              question_id: value.question,
              response_id: condition.id
            }),
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'POST'
          })
        } else {
          fetch(
            process.env.NEXT_PUBLIC_API_URL + 'delete-establishment-response',
            {
              body: JSON.stringify({
                user_id: _this.props.user_data.id,
                question_id: value.question,
                response_id: condition.id
              }),
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'POST'
            }
          )
        }
      })
      if (acum == array_of_arrays.length - 1) {
        toast.success('Activités éditées', {
          position: toast.POSITION.BOTTOM_LEFT
        })
        this.handleEditMode()
        this.props.onUpdateData('activities')
      }
      acum++
    })
  }

  render() {
    return (
      <form className="form" onSubmit={this.handleGreenScore}>
        <SectionLayout
          isModifyMode={this.state.editMode}
          onModify={this.handleEditMode}
          title="Activités"
          fromGreenscore={true}
        >
          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img
                  src="/images/onboarding/greenscore/step_five/activity_greenscore.png"
                  className="green-score__card-head-img"
                ></img>
              </div>
              <div className="col-12 col-xs-9 col-md-10 row d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-12 text-justify">
                  Activités eco positives
                </h3>
                {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
              </div>
            </div>
            <hr className="mt-1" />
            {this.state.isShowContent ? (
              <div className="row">
                {this.state.activity.map((value, index) => {
                  return (
                    <div className="col-12 col-md-6 col-xl-6" key={index}>
                      <div className="form-check">
                        <input
                          className="form-check-input input-check-blue-dark"
                          type="checkbox"
                          value={value.id}
                          id={value.id}
                          onClick={this.handleChekedPositionActivity}
                          checked={value.isChecked}
                          disabled={!this.state.editMode}
                        />
                        <div>
                          <label
                            className="form-check-label"
                            htmlFor={value.id}
                          >
                            {value.response}
                          </label>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            ) : (
              <Loader />
            )}
          </div>
          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img
                  src="/images/onboarding/greenscore/step_five/another_actions_greenscore.png"
                  className="green-score__card-head-img"
                ></img>
              </div>
              <div className="col-12 col-xs-9 col-md-10 row d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-11 text-start">
                  Autres actions écoresponsables
                </h3>
                {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
              </div>
            </div>
            <hr className="mt-1" />
            {this.state.isShowContent ? (
              <div className="row">
                {this.state.another_actions.map((value, index) => {
                  return (
                    <div className="col-12 col-md-6 col-xl-6" key={index}>
                      <div className="form-check">
                        <input
                          className="form-check-input input-check-blue-dark"
                          type="checkbox"
                          value={value.id}
                          id={value.id}
                          onClick={this.handleChekedPositionAnotherActions}
                          checked={value.isChecked}
                          disabled={!this.state.editMode}
                        />
                        <div>
                          <label
                            className="form-check-label"
                            htmlFor={value.id}
                          >
                            {value.response}
                          </label>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            ) : (
              <Loader />
            )}
          </div>
          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img
                  src="/images/onboarding/greenscore/step_five/transport_greenscore.png"
                  className="green-score__card-head-img"
                ></img>
              </div>
              <div className="col-12 col-xs-9 col-md-10 row d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-12 text-justify">
                  Transports
                </h3>
                {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
              </div>
            </div>
            <hr className="mt-1" />
            {this.state.isShowContent ? (
              <div className="row">
                {this.state.transports.map((value, index) => {
                  return (
                    <div className="col-12 col-md-6 col-xl-6" key={index}>
                      <div className="form-check">
                        <input
                          className="form-check-input input-check-blue-dark"
                          type="checkbox"
                          value={value.id}
                          id={value.id}
                          onClick={this.handleChekedPositionTransports}
                          checked={value.isChecked}
                          disabled={!this.state.editMode}
                        />
                        <div>
                          <label
                            className="form-check-label"
                            htmlFor={value.id}
                          >
                            {value.response}
                          </label>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            ) : (
              <Loader />
            )}
          </div>
          {this.state.editMode && (
            <div className="mt-4">
              <button type="submit" className="btn btn-secondary">
                Sauvegarder
              </button>
            </div>
          )}
        </SectionLayout>
      </form>
    )
  }
}

export default Activities
