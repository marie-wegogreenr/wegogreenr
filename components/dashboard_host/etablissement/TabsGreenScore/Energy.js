import { Component } from 'react'
import { toast } from 'react-toastify'
/* Libraries */
import Cookies from 'universal-cookie'

/**Components */
import { SectionLayout } from 'components/host/establishment'
import { Loader } from 'components/'

class Energy extends Component {
  constructor(props) {
    super(props)
    this.state = {
      //16
      performance: [],
      //15
      electricity: [],
      //14
      energy: [],
      //17
      air: [],
      //13
      energy_economy: [],
      //12
      water_economy: [],
      editMode: false,
      isShowContent: false
    }
  }

  async componentDidMount() {
    const cookies = new Cookies()
    //Get information performance
    const res_performance = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/16',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_performance = await res_performance.json()
    json_performance.forEach((value) => {
      value.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, performance: json_performance })

    //Get information electricity
    const res_electricity = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/15',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_electricity = await res_electricity.json()
    json_electricity.forEach((value) => {
      value.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, electricity: json_electricity })

    //Get information energy
    const res_energy = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/14',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_energy = await res_energy.json()
    json_energy.forEach((value) => {
      value.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, energy: json_energy })

    //Get information air
    const res_air = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/17',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_air = await res_air.json()
    json_air.forEach((value) => {
      value.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, air: json_air })

    //Get information energy_economy
    const res_energy_economy = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/13',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_energy_economy = await res_energy_economy.json()
    json_energy_economy.forEach((value) => {
      value.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({ ...this.state, energy_economy: json_energy_economy })

    //Get information water_economy
    const res_water_economy = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'index-for-type/12',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_water_economy = await res_water_economy.json()
    json_water_economy.forEach((value) => {
      value.isChecked = false
      if (this.props.establishment_response.length > 0) {
        this.props.establishment_response.forEach((response) => {
          if (value.id == response.response_id) {
            value.isChecked = true
          }
        })
      }
    })
    this.setState({
      ...this.state,
      water_economy: json_water_economy,
      isShowContent: true
    })
  }

  handleEditMode = async () => {
    this.setState({ ...this.state, editMode: !this.state.editMode })
  }

  handleChekedPositionPerformance = async (event) => {
    let performance = this.state.performance

    performance.forEach((performance) => {
      if (performance.id === parseInt(event.target.value)) {
        performance.isChecked = !performance.isChecked
      }
    })

    this.setState({ performance: performance })
    //consoleLog(this.state.performance)
  }

  handleChekedPositionElectricity = async (event) => {
    let electricity = this.state.electricity

    electricity.forEach((electricity) => {
      //consoleLog(electricity.id, event.target.value);
      if (electricity.isChecked == true) {
        electricity.isChecked = false
      }
      if (electricity.id === parseInt(event.target.value)) {
        electricity.isChecked = true
      }
    })
    this.setState({ electricity: electricity })
    //consoleLog(this.state.electricity)
  }

  handleChekedPositionMaterials = async (event) => {
    let materials = this.state.materials
    materials.forEach((material) => {
      //consoleLog(material.id, event.target.value);
      if (material.id === parseInt(event.target.value))
        material.isChecked = event.target.checked

      //consoleLog(material);
    })
    this.setState({ materials: materials })
    //consoleLog(this.state.materials)
  }

  handleChekedPositionEnergy = async (event) => {
    let energy = this.state.energy
    energy.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(material);
    })
    this.setState({ energy: energy })
    //consoleLog(this.state.energy)
  }

  handleChekedPositionAir = async (event) => {
    let air = this.state.air
    air.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(material);
    })
    this.setState({ air: air })
    //consoleLog(this.state.air)
  }

  handleChekedPositionEnergyEconomy = async (event) => {
    let energy_economy = this.state.energy_economy
    energy_economy.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(material);
    })
    this.setState({ energy_economy: energy_economy })
    //consoleLog(this.state.energy_economy)
  }

  handleChekedPositionWaterEconomy = async (event) => {
    let water_economy = this.state.water_economy
    water_economy.forEach((item) => {
      //consoleLog(item.id, event.target.value);
      if (item.id === parseInt(event.target.value))
        item.isChecked = event.target.checked

      //consoleLog(material);
    })
    this.setState({ water_economy: water_economy })
    //consoleLog(this.state.water_economy)
  }

  handleGreenScore = async (event) => {
    event.preventDefault()
    const cookies = new Cookies()
    let array_of_arrays = [
      { name: 'performance', question: 16 },
      { name: 'electricity', question: 15 },
      { name: 'energy', question: 14 },
      { name: 'air', question: 17 },
      { name: 'energy_economy', question: 13 },
      { name: 'water_economy', question: 12 }
    ]
    let _this = this
    let acum = 0
    array_of_arrays.forEach((value) => {
      //consoleLog(value)
      //consoleLog(_this.state)
      _this.state[value.name].forEach((condition) => {
        if (condition.isChecked == true) {
          fetch(process.env.NEXT_PUBLIC_API_URL + 'establishment-response', {
            body: JSON.stringify({
              user_id: _this.props.user_data.id,
              question_id: value.question,
              response_id: condition.id
            }),
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'POST'
          })
        } else {
          fetch(
            process.env.NEXT_PUBLIC_API_URL + 'delete-establishment-response',
            {
              body: JSON.stringify({
                user_id: _this.props.user_data.id,
                question_id: value.question,
                response_id: condition.id
              }),
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'POST'
            }
          )
        }
      })
      if (acum == array_of_arrays.length - 1) {
        toast.success('Énergie éditée', {
          position: toast.POSITION.BOTTOM_LEFT
        })

        this.handleEditMode()
        this.props.onUpdateData('energy')
      }
      acum++
    })
  }

  render() {
    return (
      <form className="form" onSubmit={this.handleGreenScore}>
        <SectionLayout
          isModifyMode={this.state.editMode}
          onModify={this.handleEditMode}
          title="Énergie"
          fromGreenscore={true}
        >
          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img
                  src="/images/onboarding/greenscore/step_two/performance_greenscore.png"
                  className="green-score__card-head-img"
                ></img>
              </div>
              <div className="col-12 col-xs-9 col-md-10 row d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-12 text-justify">
                  Performances énergétiques
                </h3>
                {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
              </div>
            </div>
            <div className="col-12" style={{ padding: 0 }}>
              <hr className="mt-1" />
            </div>
            {this.state.isShowContent ? (
              <div className="row">
                {this.state.performance.map((value, index) => {
                  return (
                    <div className="col-12 col-md-6 col-xl-6" key={index}>
                      <div className="form-check">
                        <input
                          className="form-check-input input-check-blue-dark"
                          type="checkbox"
                          value={value.id}
                          id={value.id}
                          onClick={this.handleChekedPositionPerformance}
                          checked={value.isChecked}
                          disabled={!this.state.editMode}
                        />
                        <div>
                          <label
                            className="form-check-label"
                            htmlFor={value.id}
                          >
                            {value.response}
                          </label>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            ) : (
              <Loader />
            )}
          </div>

          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img
                  src="/images/onboarding/greenscore/step_two/electricity_greenscore.png"
                  className="green-score__card-head-img"
                ></img>
              </div>
              <div className="col-12 col-xs-9 col-md-10 row d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-12 text-justify">
                  Fournisseur d'électricité
                </h3>
                {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
              </div>
            </div>
            <div className="col-12" style={{ padding: 0 }}>
              <hr className="mt-1" />
            </div>
            {this.state.isShowContent ? (
              <div>
                <select
                  className="form-control mb-0"
                  id="how_know_us"
                  onChange={this.handleChekedPositionElectricity}
                  defaultValue={
                    this.state.electricity?.find(
                      (value) => value.isChecked == true
                    )?.id || 90
                  }
                  disabled={!this.state.editMode}
                >
                  <option value=""></option>
                  {this.state.electricity.map((value, index) => {
                    if (value.isChecked == true) {
                      return (
                        <option value={value.id} key={index} selected>
                          {value.response}
                        </option>
                      )
                    } else {
                      return (
                        <option value={value.id} key={index}>
                          {value.response}
                        </option>
                      )
                    }
                  })}
                </select>
              </div>
            ) : (
              <Loader />
            )}
          </div>
          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img
                  src="/images/onboarding/greenscore/step_two/energy_greenscore.png"
                  className="green-score__card-head-img"
                ></img>
              </div>
              <div className="col-12 col-xs-9 col-md-10 row d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-12 text-justify">
                  Energie
                </h3>
                {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
              </div>
            </div>
            <hr className="mt-1" />
            {this.state.isShowContent ? (
              <div className="row">
                {this.state.energy.map((value, index) => {
                  return (
                    <div className="col-12 col-md-6 col-xl-6" key={index}>
                      <div className="form-check">
                        <input
                          className="form-check-input input-check-blue-dark"
                          type="checkbox"
                          value={value.id}
                          id={value.id}
                          onClick={this.handleChekedPositionEnergy}
                          checked={value.isChecked}
                          disabled={!this.state.editMode}
                        />
                        <div>
                          <label
                            className="form-check-label"
                            htmlFor={value.id}
                          >
                            {value.response}
                          </label>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            ) : (
              <Loader />
            )}
          </div>
          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img
                  src="/images/onboarding/greenscore/step_two/air_greenscore.png"
                  className="green-score__card-head-img"
                ></img>
              </div>
              <div className="col-12 col-xs-9 col-md-10 row d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-12 text-justify">
                  Refroidissement de l'air
                </h3>
                {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
              </div>
            </div>
            <hr className="mt-1" />
            {this.state.isShowContent ? (
              <div className="row">
                {this.state.air.map((value, index) => {
                  return (
                    <div className="col-12 col-md-6 col-xl-6" key={index}>
                      <div className="form-check">
                        <input
                          className="form-check-input input-check-blue-dark"
                          type="checkbox"
                          value={value.id}
                          id={value.id}
                          onClick={this.handleChekedPositionAir}
                          checked={value.isChecked}
                          disabled={!this.state.editMode}
                        />
                        <div>
                          <label
                            className="form-check-label"
                            htmlFor={value.id}
                          >
                            {value.response}
                          </label>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            ) : (
              <Loader />
            )}
          </div>
          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img
                  src="/images/onboarding/greenscore/step_two/energy_economy_greenscore.png"
                  className="green-score__card-head-img"
                ></img>
              </div>
              <div className="col-12 col-xs-9 col-md-10 row d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-12 text-justify">
                  Economies d'énergie
                </h3>
                {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
              </div>
            </div>
            <hr className="mt-1" />
            {this.state.isShowContent ? (
              <div className="row">
                {this.state.energy_economy.map((value, index) => {
                  return (
                    <div className="col-12 col-md-6 col-xl-6" key={index}>
                      <div className="form-check">
                        <input
                          className="form-check-input input-check-blue-dark"
                          type="checkbox"
                          value={value.id}
                          id={value.id}
                          onClick={this.handleChekedPositionEnergyEconomy}
                          checked={value.isChecked}
                          disabled={!this.state.editMode}
                        />
                        <div>
                          <label
                            className="form-check-label"
                            htmlFor={value.id}
                          >
                            {value.response}
                          </label>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            ) : (
              <Loader />
            )}
          </div>
          <div className="card-white card-white__green-score px-0">
            <div className="card-head green-score__card-head">
              <div className="d-flex justify-content-start mr-2 image-greenscore">
                <img
                  src="/images/onboarding/greenscore/step_two/water_economy_greenscore.png"
                  className="green-score__card-head-img"
                ></img>
              </div>
              <div className="col-12 col-xs-9 col-md-10 row d-flex align-items-center green-score__card-head-title">
                <h3 style={{ marginBottom: 0 }} className="col-12 text-justify">
                  Economies d'eau
                </h3>
                {/* <p className="col-12 text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Nulla quam velit, vulputate eu pharetra nec,
                            mattis ac
                          </p> */}
              </div>
            </div>
            <hr className="mt-1" />
            {this.state.isShowContent ? (
              <div className="row">
                {this.state.water_economy.map((value, index) => {
                  return (
                    <div className="col-12 col-md-6 col-xl-6" key={index}>
                      <div className="form-check">
                        <input
                          className="form-check-input input-check-blue-dark"
                          type="checkbox"
                          value={value.id}
                          id={value.id}
                          onClick={this.handleChekedPositionWaterEconomy}
                          checked={value.isChecked}
                          disabled={!this.state.editMode}
                        />
                        <div>
                          <label
                            className="form-check-label"
                            htmlFor={value.id}
                          >
                            {value.response}
                          </label>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            ) : (
              <Loader />
            )}
          </div>
          {this.state.editMode && (
            <div className="mt-4">
              <button type="submit" className="btn btn-secondary">
                Sauvegarder
              </button>
            </div>
          )}
        </SectionLayout>
      </form>
    )
  }
}

export default Energy
