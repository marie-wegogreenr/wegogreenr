import React from 'react'
import { consoleLog } from '/utils/logConsole'

import Cookies from 'universal-cookie'

/* Next */
import Router from 'next/router'

/* Components */
import Construction from './TabsGreenScore/Construction'
import Energy from './TabsGreenScore/Energy'
import Food from './TabsGreenScore/Food'
import Reduction from './TabsGreenScore/Reduction'
import Activities from './TabsGreenScore/Activities'
import Labels from './TabsGreenScore/Labels'
import { Loader } from 'components/'

const tabs = [
  {
    id: 'construction',
    Component: Construction,
    text: 'Construction'
  },
  {
    id: 'energy',
    Component: Energy,
    text: 'Energies'
  },
  {
    id: 'food',
    Component: Food,
    text: 'Aliments'
  },
  {
    id: 'reduction',
    Component: Reduction,
    text: 'Réduction'
  },
  {
    id: 'activities',
    Component: Activities,
    text: 'Activités'
  },
  {
    id: 'labels',
    Component: Labels,
    text: 'Labels'
  }
]

class GreenScore extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user_data: '',
      establishment_response: [],
      showComponents: false
    }
  }

  async componentDidMount() {
    const cookies = new Cookies()
    let _this = this

    //Get user data
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data }, () => {
          _this.handleGetResponses()
        })
      })
  }

  handleGetResponses = async () => {
    const cookies = new Cookies()
    //Get establishment response
    const res_establishment_response = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishment-response/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_establishment_response = await res_establishment_response.json()
    this.setState({
      ...this.state,
      establishment_response: json_establishment_response,
      showComponents: true
    })
  }

  handleUpdateData = (id) => {
    this.setState({ ...this.state, id_tab: id }, () => {
      this.handleGetResponses()
    })
  }

  handleChangeTab = async (id) => {
    this.setState({ ...this.state, id_tab: id })
  }

  render() {
    return (
      <div className="card-white green-score__host">
        <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
          {tabs.map(({ text, id }, index) => (
            <li
              key={id}
              className="nav-item"
              role="presentation"
              onClick={() => {
                this.handleChangeTab(id)
              }}
            >
              <button
                className={`nav-link ${!index ? 'active' : ''}`}
                id={`${id}-tab`}
                data-bs-toggle="pill"
                data-bs-target={`#pills-${id}`}
                type="button"
                role="tab"
                aria-controls={id}
                aria-selected="true"
              >
                {text}
              </button>
            </li>
          ))}
        </ul>

        {this.state.showComponents ? (
          <div className="row w-100 mx-0 mt-3">
            <div className="col-12 col-md-12 p-0">
              <div className="tab-content" id="pills-tabContent">
                {tabs.map(({ Component, id }, index) => (
                  <div
                    key={id}
                    className={`tab-pane fade show ${index ? '' : 'active'}`}
                    id={`pills-${id}`}
                    role="tabpanel"
                    aria-labelledby={`${id}-tab`}
                  >
                    <Component
                      establishment_response={this.state.establishment_response}
                      user_data={this.state.user_data}
                      onUpdateData={this.handleUpdateData}
                    />
                  </div>
                ))}
              </div>
            </div>
          </div>
        ) : (
          <Loader />
        )}
      </div>
    )
  }
}

export default GreenScore
