import React from 'react'
import { consoleLog } from '/utils/logConsole'

import Cookies from 'universal-cookie'

import Router from 'next/router'
import Link from 'next/link'
import { toast } from 'react-toastify'

import SearchAlgoia from '@components/Algolia/SearchAlgolia'

class Info extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      clasification: [],
      countries: [],
      regions: [],
      stablishments_types: [],
      stablishments: [],
      editMode: true,
      showAlgolia: false,
      citySelected: '',
      lat: '',
      lng: ''
    }

    this.handleEstablissement = this.handleEstablissement.bind(this)
    this.handleEditMode = this.handleEditMode.bind(this)
    this.handleSearchAddress = this.handleSearchAddress.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    let _this = this

    //Get user data
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get establishment
    const res_stablishments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments = await res_stablishments.json()
    this.setState({ ...this.state, stablishments: json_stablishments })

    let stablishments = this.state.stablishments

    if (stablishments.length > 0) {
      this.setState({
        ...this.state,
        status_stablishment: 'update'
      })

      const res_city = await fetch(
        process.env.NEXT_PUBLIC_API_URL + 'city/' + stablishments[0].city.id,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + cookies.get('tk_user')
          },
          method: 'GET'
        }
      )
      const json_city = await res_city.json()
      //consoleLog(json_city)
      stablishments[0].name_city = json_city.name
      this.setState({ ...this.state, stablishments: stablishments }, () => {
        this.setState({ ...this.state, showAlgolia: true })
      })
    } else {
      this.setState({ ...this.state, showAlgolia: true })
    }

    //Get type stablishment
    const res_stablishments_types = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'establishment-types',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments_types = await res_stablishments_types.json()
    this.setState({
      ...this.state,
      stablishments_types: json_stablishments_types
    })

    //Get countries
    const res_countrie = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'countries',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_countries = await res_countrie.json()
    this.setState({ ...this.state, countries: json_countries })

    if (this.state.stablishments.length > 0) {
      this.setState({ ...this.state, status_stablishment: 'update' })
    }

    //Get clasification
    const res_clasification = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'establishment-clasifications',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_clasification = await res_clasification.json()
    this.setState({ ...this.state, clasification: json_clasification })
  }

  handleEditMode() {
    this.setState({ ...this.state, editMode: !this.state.editMode })
  }

  onSelectOption = (selected_value) => {
    //consoleLog(selected_value)
    this.setState({ ...this.state, citySelected: selected_value })
  }

  async handleEstablissement(event) {
    event.preventDefault()
    const cookies = new Cookies()
    const res = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.stablishments[0].id,
      {
        body: JSON.stringify({
          name: event.target.name_stablishment.value,
          type_id: event.target.type_stablishment.value,
          clasification_id: event.target.stars_stablishment.value,
          address: event.target.address_stablishment.value,
          lat: this.state.lat,
          lng: this.state.lng,
          street_number: event.target.number_rue.value,
          city: this.state.citySelected.id,
          country_id: event.target.pays.value,
          region_id: this.state.citySelected.region_id,
          zip_code: event.target.cp.value,
          contact_name: event.target.name_host.value,
          contact_lastname: event.target.last_name.value,
          contact_email: event.target.email_host.value,
          contact_phone: event.target.phone_host.value,
          description: this.state.stablishments[0].description,
          min_entry_time: this.state.stablishments[0].min_entry_time,
          max_entry_time: this.state.stablishments[0].max_entry_time,
          max_exit_time: this.state.stablishments[0].max_exit_time,
          tva: this.state.stablishments[0].tva,
          decl_inc_pro: this.state.stablishments[0].decl_inc_pro,
          amount_taxe: this.state.stablishments[0].amount_taxe,
          slug: this.state.stablishments[0].slug
        }),
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'PUT'
      }
    )

    const result = await res.json()
    const status = await res.status
    //consoleLog(result);
    //consoleLog(status);
    if (status == 200) {
      toast.success('Établissement mis à jour', {
        position: toast.POSITION.BOTTOM_LEFT
      })
      this.handleEditMode()
    } else {
      toast.error(`Une erreur s'est produite, réessayez plus tard `, {
        position: toast.POSITION.BOTTOM_LEFT
      })
    }
  }

  handleChangeVille = (event) => {
    const { value } = event.target

    if (typeof value === 'string') {
      this.setState({ ...this.state, ville: value })
    }
  }

  async handleSearchAddress(event) {
    /* let tranform_address = event.target.value.split(' ').join('+') */
    //consoleLog(tranform_address)
    let _this = this
    axios
      .get('https://maps.googleapis.com/maps/api/geocode/json', {
        params: {
          address: event.target.value,
          key: process.env.NEXT_PUBLIC_GOOGLE_MAPS_KEY
        }
      })
      .then(function (response) {
        //consoleLog(response.data.results[0].geometry.location)
        const location = response.data.results[0].geometry.location
        _this.setState({ ..._this.state, lat: location.lat, lng: location.lng })
      })
      .catch(function (error) {
        //consoleLog(error)
      })
      .then(function () {
        // always executed
      })
  }

  render() {
    return (
      <form className="form" onSubmit={this.handleEstablissement} method="POST">
        <div className="card rounded-0">
          <div className="card-body">
            <div className="row">
              <p className="col-6">Infos basiques</p>
              <a
                className="col-6 text-right"
                href="#"
                onClick={this.handleEditMode}
              >
                {!this.state.editMode == false ? 'Modifier' : 'Annuler'}
              </a>
            </div>
            <div className="form-group">
              <label htmlFor="name_stablishment">Nom Etablissement *</label>
              <input
                type="text"
                className="form-control"
                id="name_stablishment"
                name="name_stablishment"
                aria-describedby="emailHelp"
                placeholder=""
                defaultValue={
                  this.state.stablishments.length > 0
                    ? this.state.stablishments[0].name
                    : ''
                }
                required
                disabled={this.state.editMode}
              />
            </div>
            <div className="form-group">
              <label htmlFor="type_stablishment">Type d'établissement *</label>
              <br />
              <select
                defaultValue=""
                className="form-control"
                id="type_stablishment"
                required
                disabled={this.state.editMode}
              >
                <option value=""></option>
                {this.state.stablishments.length > 0
                  ? this.state.stablishments_types.map((value, index) => {
                      if (this.state.stablishments[0].type_id == value.id) {
                        return (
                          <option key={index} value={value.id} selected>
                            {value.name}
                          </option>
                        )
                      } else {
                        return (
                          <option key={index} value={value.id}>
                            {value.name}
                          </option>
                        )
                      }
                    })
                  : this.state.stablishments_types.map((value, index) => {
                      return (
                        <option key={index} value={value.id}>
                          {value.name}
                        </option>
                      )
                    })}
              </select>
            </div>
            <div className="form-group">
              <label htmlFor="stars_stablishment">Classification *</label>
              <br />
              <select
                defaultValue=""
                className="form-control"
                id="stars_stablishment"
                required
                disabled={this.state.editMode}
              >
                <option defaultValue=""></option>
                {this.state.stablishments.length > 0
                  ? this.state.clasification.map((value, index) => {
                      if (
                        this.state.stablishments[0].clasification_id == value.id
                      ) {
                        return (
                          <option value={value.id} key={index} selected>
                            {value.name}
                          </option>
                        )
                      } else {
                        return (
                          <option value={value.id} key={index}>
                            {value.name}
                          </option>
                        )
                      }
                    })
                  : this.state.clasification.map((value, index) => {
                      return (
                        <option value={value.id} key={index}>
                          {value.name}
                        </option>
                      )
                    })}
              </select>
            </div>
            {!this.state.editMode && (
              <div className="col-4">
                <button
                  type="submit"
                  className="btn btn-success btn-block btn-lg"
                >
                  Sauvegarder
                </button>
              </div>
            )}
          </div>
        </div>
        <br />
        <div className="card rounded-0">
          <div className="card-body">
            <div className="row">
              <p className="col-6">Localisation</p>
              <a
                className="col-6 text-right"
                href="#"
                onClick={this.handleEditMode}
              >
                {!this.state.editMode == false ? 'Modifier' : 'Annuler'}
              </a>
            </div>
            <div className="form-group">
              <label htmlFor="address_stablishment">Adresse *</label>
              <input
                type="text"
                className="form-control"
                id="address_stablishment"
                name="address_stablishment"
                aria-describedby="emailHelp"
                placeholder=""
                required
                defaultValue={
                  this.state.stablishments.length > 0
                    ? this.state.stablishments[0].address
                    : ''
                }
                onChange={this.handleSearchAddress}
                disabled={this.state.editMode}
              />
            </div>
            <div className="row form-group">
              <div className="col-12 col-xl-6">
                <h4 htmlFor="lat_stablishment">Latitude *</h4>
                <input
                  type="text"
                  className="form-control"
                  id="lat_stablishment"
                  name="lat_stablishment"
                  aria-describedby="emailHelp"
                  placeholder=""
                  defaultValue={this.state.lat}
                  value={this.state.lat}
                  required
                  readOnly
                />
              </div>
              <div className="col-12 col-xl-6">
                <h4 htmlFor="lng_stablishment">Longitude *</h4>
                <input
                  type="text"
                  className="form-control"
                  id="lng_stablishment"
                  name="lng_stablishment"
                  aria-describedby="emailHelp"
                  placeholder=""
                  defaultValue={this.state.lng}
                  value={this.state.lng}
                  required
                  readOnly
                />
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <h4 htmlFor="pays">Pays *</h4>
                <select
                  defaultValue=""
                  className="form-control"
                  id="pays"
                  name="pays"
                  /* onChange={this.handleChangeRegion} */
                  required
                  disabled={this.state.editMode}
                >
                  <option value=""></option>
                  {this.state.stablishments.length > 0
                    ? this.state.countries.map((value, index) => {
                        if (
                          this.state.stablishments[0].country_id == value.id
                        ) {
                          return (
                            <option key={index} value={value.id} selected>
                              {value.name}
                            </option>
                          )
                        } else {
                          return (
                            <option key={index} value={value.id}>
                              {value.name}
                            </option>
                          )
                        }
                      })
                    : this.state.countries.map((value, index) => {
                        return (
                          <option key={index} value={value.id}>
                            {value.name}
                          </option>
                        )
                      })}
                </select>
              </div>
              <div className="col-6">
                <h4 htmlFor="ville">Ville</h4>
                {this.state.showAlgolia && (
                  <SearchAlgoia
                    onChange={this.handleChangeVille}
                    inputValue={this.state.ville || ''}
                    index="cities"
                    placeholder="Ville"
                    attribute="name"
                    onSelectOption={this.onSelectOption}
                    defaultValue={
                      this.state.stablishments.length > 0
                        ? this.state.stablishments[0].city_name
                        : ''
                    }
                  />
                )}
              </div>
            </div>
            <div className="row form-group">
              <div className="col-12 col-xl-6">
                <h4 htmlFor="number_rue">N° rue *</h4>
                <input
                  type="number"
                  onWheel={(e) => e.target.blur()}
                  className="form-control"
                  id="number_rue"
                  name="number_rue"
                  aria-describedby="emailHelp"
                  placeholder=""
                  min="0"
                  required
                  defaultValue={
                    this.state.stablishments.length > 0
                      ? this.state.stablishments[0].street_number
                      : ''
                  }
                  disabled={this.state.editMode}
                />
              </div>
              <div className="col-12 col-xl-6">
                <h4 htmlFor="cp">CP *</h4>
                <input
                  type="number"
                  className="form-control"
                  id="cp"
                  name="cp"
                  aria-describedby="emailHelp"
                  placeholder=""
                  required
                  min="0"
                  defaultValue={
                    this.state.stablishments.length > 0
                      ? this.state.stablishments[0].zip_code
                      : ''
                  }
                  disabled={this.state.editMode}
                />
              </div>
            </div>
            {!this.state.editMode && (
              <div className="col-4">
                <button
                  type="submit"
                  className="btn btn-success btn-block btn-lg"
                >
                  Sauvegarder
                </button>
              </div>
            )}
          </div>
        </div>
        <br />
        <div className="card rounded-0">
          <div className="card-body">
            <div className="row">
              <p className="col-6">Contact Gestionnaire</p>
              <a
                className="col-6 text-right"
                href="#"
                onClick={this.handleEditMode}
              >
                {!this.state.editMode == false ? 'Modifier' : 'Annuler'}
              </a>
            </div>
            <div className="row">
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="name_host">Prénom gestionnaire *</label>
                  <input
                    type="text"
                    className="form-control"
                    id="name_host"
                    name="name_host"
                    aria-describedby="emailHelp"
                    placeholder=""
                    required
                    defaultValue={
                      this.state.stablishments.length > 0
                        ? this.state.stablishments[0].contact_name
                        : ''
                    }
                    disabled={this.state.editMode}
                  />
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="last_name">Nom gestionnaire *</label>
                  <input
                    type="text"
                    className="form-control"
                    id="last_name"
                    name="last_name"
                    aria-describedby="emailHelp"
                    placeholder=""
                    required
                    defaultValue={
                      this.state.stablishments.length > 0
                        ? this.state.stablishments[0].contact_lastname
                        : ''
                    }
                    disabled={this.state.editMode}
                  />
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="email_host">Email *</label>
                  <input
                    type="text"
                    className="form-control"
                    id="email_host"
                    name="email_host"
                    aria-describedby="emailHelp"
                    placeholder=""
                    required
                    defaultValue={
                      this.state.stablishments.length > 0
                        ? this.state.stablishments[0].contact_email
                        : ''
                    }
                    disabled={this.state.editMode}
                  />
                </div>
              </div>
              <div className="col-6">
                <div className="form-group">
                  <label htmlFor="phone_host">Teléphone *</label>
                  <input
                    type="text"
                    className="form-control"
                    id="phone_host"
                    name="phone_host"
                    aria-describedby="emailHelp"
                    placeholder=""
                    required
                    defaultValue={
                      this.state.stablishments.length > 0
                        ? this.state.stablishments[0].contact_phone
                        : ''
                    }
                    disabled={this.state.editMode}
                  />
                </div>
              </div>
              {!this.state.editMode && (
                <div className="col-4">
                  <button
                    type="submit"
                    className="btn btn-success btn-block btn-lg"
                  >
                    Sauvegarder
                  </button>
                </div>
              )}
            </div>
            <br />
          </div>
        </div>
        <br />
      </form>
    )
  }
}

export default Info
