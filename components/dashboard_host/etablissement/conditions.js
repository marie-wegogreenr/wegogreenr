import React from 'react'

/* Components */
import { SectionLayout } from 'components/host/establishment'
import { Loader } from 'components/'
import SelectQuestion from '@components/Onboarding/SelectQuestion'
import SelectedAnswer from '@components/Onboarding/SelectedAnswer'
import { notify } from 'helpers/notificationClassHelper'
import {
  addCancellationConditions,
  deleteCancellationConditions,
  getCancellationConditions
} from 'services/etablissementService'
import {
  annulationQuestions,
  fixedAnswer,
  optionsQuestion1,
  optionsQuestion2,
  textCovid
} from 'constants/cancellationConditions'

class Conditions extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      text_anulation: '',
      isShowContent: false,
      isModifyMode: false,
      conditionId: null,
      valueQuestion1: 0,
      valueQuestion2: 0,
      userData: [],
      cancellationConditions: [],
      stateSaveBtn: 'init'
    }

    this.handleChangeQuestion1 = this.handleChangeQuestion1.bind(this)
    this.handleChangeQuestion2 = this.handleChangeQuestion2.bind(this)
    this.handleModify = this.handleModify.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.getCancellationConditions = this.getCancellationConditions.bind(this)
    this.addCancellationConditions = this.addCancellationConditions.bind(this)
    this.deleteCancellationConditions =
      this.deleteCancellationConditions.bind(this)
    this.validateConditions = this.validateConditions.bind(this)
    this.actionSuccess = this.actionSuccess.bind(this)
    this.actionError = this.actionError.bind(this)

    //  Annulation questions
    this.annulationQuestions = {
      question1: annulationQuestions.question1,
      question2: annulationQuestions.question2
    }
    this.fixedAnswer = fixedAnswer
    this.optionsQuestion1 = optionsQuestion1
    this.optionsQuestion2 = optionsQuestion2
    this.textCovid = textCovid
  }

  async componentDidMount() {
    /*  Get cancellation conditions data  */
    await this.getCancellationConditions()
    this.setState({ ...this.state, isShowContent: true })
  }

  componentDidUpdate(prevProps) {
    if (prevProps.id_etablisement !== this.props.id_etablisement) {
      this.getCancellationConditions()
    }
  }

  handleChangeQuestion1(event) {
    this.setState({
      ...this.state,
      valueQuestion1: parseInt(event.target.value)
    })
  }

  handleChangeQuestion2(event) {
    this.setState({
      ...this.state,
      valueQuestion2: parseInt(event.target.value)
    })
  }

  handleModify() {
    this.setState({ ...this.state, isModifyMode: !this.state.isModifyMode })

    if (this.state.isModifyMode) {
      notify(`Les modifications n'ont pas été enregistrées`, 'warning')
    }
  }

  actionSuccess() {
    this.setState({ ...this.state, stateSaveBtn: 'saved' })
    notify(`Les modifications ont été enregistrées avec succès`, 'success')
    this.setState({ ...this.state, stateSaveBtn: 'init' })
  }

  actionError() {
    this.setState({ ...this.state, stateSaveBtn: 'error' })
    notify(`Il y a eu une erreur dans le processus. Réessayer`, 'error')
  }

  async getCancellationConditions() {
    let _this = this

    try {
      const cancellationConditions = await getCancellationConditions(
        this.props.id_etablisement
      )

      _this.setState({
        ..._this.state,
        cancellationConditions: cancellationConditions
      })

      if (_this.state.cancellationConditions?.length > 0) {
        _this.setState({
          ..._this.state,
          conditionId: _this.state.cancellationConditions[0].id,
          valueQuestion1: _this.state.cancellationConditions[0].days_quantity,
          valueQuestion2:
            _this.state.cancellationConditions[0].penality_percentage
        })
      }
    } catch (error) {
      notify(
        `Il y a eu une erreur dans le processus. Essayez de recharger la page.`,
        'error'
      )
    }
  }

  async addCancellationConditions() {
    let status

    try {
      const body = {
        establishment_id: this.props.id_etablisement,
        days_quantity: this.state.valueQuestion1,
        penality_percentage: this.state.valueQuestion2
      }
      const add_cancellation_conditions = await addCancellationConditions(body)
      status = add_cancellation_conditions.code
    } catch (error) {
      status = 500
    }
    return status
  }

  async deleteCancellationConditions() {
    let status

    try {
      const delete_cancellation_conditions = await deleteCancellationConditions(
        this.state.conditionId
      )
      status = delete_cancellation_conditions.code
    } catch (error) {
      status = 500
    }
    return status
  }

  validateConditions() {
    let _this = this
    if (_this.state.cancellationConditions.length > 0) {
      ;(async () => {
        const deleteStatus = await _this.deleteCancellationConditions()

        if (deleteStatus === 200) {
          const addStatus = await _this.addCancellationConditions()
          if (addStatus === 200) {
            _this.actionSuccess()
          } else {
            _this.actionError()
          }
        } else {
          _this.actionError()
        }
        _this.setState({ ..._this.state, isModifyMode: false })
        await _this.getCancellationConditions()
      })()
    } else if (_this.state.cancellationConditions.length === 0) {
      ;(async () => {
        const addStatus = await _this.addCancellationConditions()
        if (addStatus === 200) {
          _this.actionSuccess()
        } else {
          _this.actionError()
        }
        _this.setState({ ..._this.state, isModifyMode: false })
        await _this.getCancellationConditions()
      })()
    } else {
      _this.actionError()
    }
  }

  async handleSubmit(event) {
    event.preventDefault()
    let _this = this
    _this.setState({ ..._this.state, stateSaveBtn: 'loading' })

    if (
      this.state.valueQuestion1 === '' ||
      this.state.valueQuestion2 === '' ||
      this.state.valueQuestion1 === 0 ||
      this.state.valueQuestion2 === 0 ||
      Object.is(NaN, this.state.valueQuestion1) ||
      Object.is(NaN, this.state.valueQuestion2)
    ) {
      notify(`Vous devez sélectionner les questions`, 'error')
      _this.setState({ ..._this.state, stateSaveBtn: 'init' })
    } else {
      _this.validateConditions()
    }
  }

  render() {
    return (
      <>
        {this.state.isShowContent ? (
          <SectionLayout
            title="Conditions d'annulation"
            hideModifyButton={false}
            isModifyMode={this.state.isModifyMode}
            onModify={this.handleModify}
          >
            {this.state.isModifyMode ? (
              <div className="mb-5">
                <form onSubmit={this.handleSubmit}>
                  <SelectQuestion
                    id="days"
                    question={this.annulationQuestions.question1}
                    options={this.optionsQuestion1}
                    handleChange={this.handleChangeQuestion1}
                    defaultValue={
                      this.state.cancellationConditions.length > 0
                        ? this.state.valueQuestion1
                        : ''
                    }
                  />
                  <SelectQuestion
                    id="percentage"
                    question={this.annulationQuestions.question2}
                    options={this.optionsQuestion2}
                    handleChange={this.handleChangeQuestion2}
                    defaultValue={
                      this.state.cancellationConditions.length > 0
                        ? this.state.valueQuestion2
                        : ''
                    }
                  />
                  <button
                    type="submit"
                    className="btn btn-secondary mt-4 button--save-onboarding"
                    disabled={
                      this.state.stateSaveBtn !== 'loading' ? false : true
                    }
                  >
                    {this.state.stateSaveBtn === 'saved'
                      ? 'Enregistré'
                      : this.state.stateSaveBtn === 'loading'
                      ? 'En cours...'
                      : 'Enregistrer'}
                  </button>
                </form>
              </div>
            ) : (
              <div className="mb-5">
                <SelectedAnswer
                  question={this.annulationQuestions.question1}
                  questionNumber={1}
                  answerOptions={this.optionsQuestion1}
                  answer={
                    this.state.cancellationConditions.length > 0
                      ? this.state.cancellationConditions[0].days_quantity
                      : ''
                  }
                />
                <p>{this.fixedAnswer}</p>
                <SelectedAnswer
                  question={this.annulationQuestions.question2}
                  questionNumber={2}
                  answerOptions={this.optionsQuestion2}
                  answer={
                    this.state.cancellationConditions.length > 0
                      ? this.state.cancellationConditions[0].penality_percentage
                      : ''
                  }
                />
              </div>
            )}

            <div>
              <h4>Politiques et COVID-19</h4>
              <p className="text-wrap text-justify">{this.textCovid}</p>
            </div>
          </SectionLayout>
        ) : (
          <Loader />
        )}
      </>
    )
  }
}
export default Conditions
