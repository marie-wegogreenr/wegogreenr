import React from 'react'
import { consoleLog } from '/utils/logConsole'
import { toast } from 'react-toastify'
import Cookies from 'universal-cookie'

import Router from 'next/router'
import Link from 'next/link'
import { SectionLayout } from '@components/host/establishment'

class Services extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user_data: '',
      stablishments: [],
      editMode: false,
      services: [],
      type_breakfast: [],
      languages: [],
      establishment_languages: [],
      establishment_services: []
    }

    this.handleEditMode = this.handleEditMode.bind(this)
    this.handleServices = this.handleServices.bind(this)
    this.handleChekedPositionServices =
      this.handleChekedPositionServices.bind(this)
    this.handleChekedPositionServicesBreakfast =
      this.handleChekedPositionServicesBreakfast.bind(this)
    this.handleChangePrixBreakfast = this.handleChangePrixBreakfast.bind(this)
    this.handleChangePrixServices = this.handleChangePrixServices.bind(this)
    this.handleChekedPositionLanguage =
      this.handleChekedPositionLanguage.bind(this)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    let _this = this

    //Get user data
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get establishment
    const res_stablishments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments = await res_stablishments.json()
    this.setState({ ...this.state, stablishments: json_stablishments })

    //Get services
    const res_services = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'services',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_services = await res_services.json()
    this.setState({ ...this.state, services: json_services })

    //Get type breakfast
    const res_type_breakfast = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'breakfast-types',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_type_breakfast = await res_type_breakfast.json()
    this.setState({ ...this.state, type_breakfast: json_type_breakfast })

    //Get establishment services
    const res_establishment_services = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishment-services/' +
        this.state.stablishments[0].id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_establishment_services = await res_establishment_services.json()
    this.setState({
      ...this.state,
      establishment_services: json_establishment_services
    })

    let services = this.state.services

    services.forEach((service) => {
      service.isCheckedNo = true
      service.isCheckedYes = false
      service.price = 0
    })

    this.setState({ ...this.state, services: services })

    let services_2 = this.state.services

    this.state.establishment_services.forEach((establishment_service) => {
      services_2.forEach((service) => {
        if (service.id == establishment_service.service_id) {
          service.isCheckedYes = true
          service.isCheckedNo = false
          service.price = establishment_service.price
          service.establishment_service_id = establishment_service.id
        }
      })
    })

    this.setState({ ...this.state, services: services_2 })

    let type_breakfast = this.state.type_breakfast

    type_breakfast.forEach((service) => {
      service.isCheckedNo = true
      service.isCheckedYes = false
      service.price = 0
    })

    this.setState({ ...this.state, type_breakfast: type_breakfast })

    let type_breakfast_2 = this.state.type_breakfast

    this.state.establishment_services.forEach((establishment_service) => {
      type_breakfast_2.forEach((service) => {
        if (service.id == establishment_service.breakfast_type_id) {
          service.isCheckedYes = true
          service.isCheckedNo = false
          service.price = establishment_service.price
          service.establishment_service_id = establishment_service.id
        }
      })
    })

    this.setState({ ...this.state, type_breakfast: type_breakfast })

    //Get languages
    const res_language = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'languages',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_languages = await res_language.json()
    this.setState({ ...this.state, languages: json_languages })

    //Get establishment language
    const res_stablishments_language = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishment-languages/' +
        this.state.stablishments[0].id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments_language = await res_stablishments_language.json()
    this.setState({
      ...this.state,
      establishment_languages: json_stablishments_language
    })

    let languages = this.state.languages

    languages.forEach((language) => {
      language.isChecked = false
    })

    this.setState({ ...this.state, languages: languages })

    let languages_2 = this.state.languages

    this.state.establishment_languages.forEach((establishment_language) => {
      languages_2.forEach((language) => {
        if (language.id == establishment_language.language_id) {
          language.isChecked = true
        }
      })
    })

    this.setState({ ...this.state, languages: languages })
  }

  async handleChekedPositionServices(event) {
    let services = this.state.services
    services.forEach((service) => {
      if (service.id === parseInt(event.target.value)) {
        service.isCheckedYes = !service.isCheckedYes
        service.isCheckedNo = !service.isCheckedNo
      }
    })
    this.setState({ ...this.state, services: services })
  }

  async handleChekedPositionServicesBreakfast(event) {
    let type_breakfast = this.state.type_breakfast
    type_breakfast.forEach((service) => {
      if (service.id === parseInt(event.target.value)) {
        service.isCheckedYes = !service.isCheckedYes
        service.isCheckedNo = !service.isCheckedNo
      }
    })
    this.setState({ ...this.state, type_breakfast: type_breakfast })
  }

  handleChangePrixBreakfast(event) {
    let type_breakfast = this.state.type_breakfast
    type_breakfast.forEach((service) => {
      if (service.id === parseInt(event.target.id)) {
        service.price = event.target.value
      }
    })
    this.setState({ ...this.state, type_breakfast: type_breakfast })
  }

  handleChangePrixServices(event) {
    let services = this.state.services
    services.forEach((service) => {
      if (service.id === parseInt(event.target.id)) {
        service.price = parseInt(event.target.value)
      }
    })
    this.setState({ ...this.state, services: services })
  }

  async handleChekedPositionLanguage(event) {
    let languages = this.state.languages
    languages.forEach((language) => {
      if (language.id === parseInt(event.target.value))
        language.isChecked = event.target.checked
    })
    this.setState({ ...this.state, languages: languages })
    //consoleLog(this.state.languages)
  }

  //POST save information establishment
  async handleServices(event) {
    event.preventDefault()
    const cookies = new Cookies()
    this.state.services.forEach((service) => {
      if (service.id == 1) {
        this.state.type_breakfast.forEach((breakfast) => {
          if (breakfast.isCheckedYes == true) {
            //consoleLog(service.establishment_service_id !== undefined, service.establishment_service_id !== 'undefined');
            if (breakfast.establishment_service_id !== undefined) {
              fetch(
                process.env.NEXT_PUBLIC_API_URL +
                  'update-service/' +
                  breakfast.establishment_service_id,
                {
                  body: JSON.stringify({
                    establishment_id: this.state.stablishments[0].id,
                    service_id: service.id,
                    price: breakfast.price,
                    breakfast_type_id: breakfast.id
                  }),
                  headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + cookies.get('tk_user')
                  },
                  method: 'POST'
                }
              )
            } else {
              fetch(
                process.env.NEXT_PUBLIC_API_URL + 'establishment-services',
                {
                  body: JSON.stringify({
                    establishment_id: this.state.stablishments[0].id,
                    service_id: service.id,
                    price: breakfast.price,
                    breakfast_type_id: breakfast.id
                  }),
                  headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + cookies.get('tk_user')
                  },
                  method: 'POST'
                }
              )
            }
          }
        })
      } else {
        if (service.isCheckedYes == true) {
          if (service.establishment_service_id !== undefined) {
            fetch(
              process.env.NEXT_PUBLIC_API_URL +
                'update-service/' +
                service.establishment_service_id,
              {
                body: JSON.stringify({
                  establishment_id: this.state.stablishments[0].id,
                  service_id: service.id,
                  price: service.price,
                  breakfast_type_id: ''
                }),
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: 'Bearer ' + cookies.get('tk_user')
                },
                method: 'POST'
              }
            )
          } else {
            fetch(process.env.NEXT_PUBLIC_API_URL + 'establishment-services', {
              body: JSON.stringify({
                establishment_id: this.state.stablishments[0].id,
                service_id: service.id,
                price: service.price,
                breakfast_type_id: ''
              }),
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'POST'
            })
          }
        }
      }
    })

    this.state.languages.forEach((language) => {
      if (language.isChecked == true) {
        fetch(process.env.NEXT_PUBLIC_API_URL + 'establishment-languages', {
          body: JSON.stringify({
            establishment_id: this.state.stablishments[0].id,
            language_id: language.id
          }),
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + cookies.get('tk_user')
          },
          method: 'POST'
        })
      } else {
        fetch(
          process.env.NEXT_PUBLIC_API_URL + 'delete-establishment-language',
          {
            body: JSON.stringify({
              establishment_id: this.state.stablishments[0].id,
              language_id: language.id
            }),
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'POST'
          }
        )
      }
    })
    toast.success('Services mis à jour', {
      position: toast.POSITION.BOTTOM_LEFT
    })

    this.setState({
      ...this.state,
      editMode: false
    })
  }

  handleEditMode() {
    this.setState({ ...this.state, editMode: !this.state.editMode })
  }

  render() {
    return (
      <form className="form" onSubmit={this.handleServices}>
        {this.state.services.map((value_services, index) => {
          return (
            <div key={index}>
              <SectionLayout
                title={value_services.name}
                onModify={this.handleEditMode}
                isModifyMode={this.state.editMode}
              >
                {value_services.id == 1 &&
                  this.state.type_breakfast.map((value, index) => {
                    return (
                      <div key={index} className="row">
                        <div className="col-12 col-sm-6 mb-4">
                          <p className="mb-0 weight-semibold">{value.name}</p>

                          <div className="d-flex justify-content-between">
                            <div className="py-3 px-5 bg-light w-100 rounded-3 mr-2">
                              <div className="form-check form-check-inline ml-0">
                                <input
                                  className="form-check-input"
                                  type="radio"
                                  name={'breakfast_' + value.id}
                                  id={'breakfast-input-' + value.id}
                                  value={value.id}
                                  onClick={
                                    this.handleChekedPositionServicesBreakfast
                                  }
                                  checked={value.isCheckedYes}
                                  disabled={!this.state.editMode}
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor={'breackfast-input-' + value.id}
                                >
                                  Oui
                                </label>
                              </div>
                            </div>

                            <div className="py-3 px-5 bg-light w-100 rounded-3 ml-2">
                              <div className="form-check form-check-inline ml-0">
                                <input
                                  className="form-check-input"
                                  type="radio"
                                  name={'breakfast_' + value.id}
                                  id={'breackfast-input-' + value.id}
                                  value={value.id}
                                  onClick={
                                    this.handleChekedPositionServicesBreakfast
                                  }
                                  checked={value.isCheckedNo}
                                  disabled={!this.state.editMode}
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor={'breackfast-input-' + value.id}
                                >
                                  Non
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-12 col-sm-6 mb-4">
                          <div className="">
                            <label
                              className="weight-semibold"
                              htmlFor={'prix_breakfast_' + value.id}
                            >
                              Prix
                            </label>
                            <input
                              type="number"
                              onWheel={(e) => e.target.blur()}
                              className="form-control py-4"
                              id={'prix_breakfast_' + value.id}
                              name={'prix_breakfast_' + value.id}
                              aria-describedby="emailHelp"
                              placeholder=""
                              onChange={this.handleChangePrixBreakfast}
                              defaultValue={value.price}
                              readOnly={value.isCheckedNo === true}
                              disabled={!this.state.editMode}
                            />
                          </div>
                        </div>
                      </div>
                    )
                  })}
                {value_services.id !== 1 && (
                  <div className="row">
                    <div className="col-6">
                      <div className="row">
                        <div className="col-6">
                          <div className="">
                            <div className="form-check">
                              <input
                                className="form-check-input"
                                type="radio"
                                name={'service_' + value_services.id}
                                id={value_services.id}
                                value={value_services.id}
                                onClick={this.handleChekedPositionServices}
                                checked={value_services.isCheckedYes}
                                disabled={!this.state.editMode}
                              />
                              <label
                                className="form-check-label"
                                htmlFor={value_services.id}
                              >
                                Oui
                              </label>
                            </div>
                          </div>
                        </div>
                        <div className="col-6">
                          <div className="">
                            <div className="form-check">
                              <input
                                className="form-check-input"
                                type="radio"
                                name={'service_' + value_services.id}
                                id={value_services.id}
                                value={value_services.id}
                                onClick={this.handleChekedPositionServices}
                                checked={value_services.isCheckedNo}
                                disabled={!this.state.editMode}
                              />
                              <label
                                className="form-check-label"
                                htmlFor={value_services.id}
                              >
                                Non
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-6">
                      <label htmlFor={'prix_services_' + value_services.id}>
                        Prix
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id={value_services.id}
                        name={'prix_services_' + value_services.id}
                        aria-describedby="emailHelp"
                        placeholder=""
                        onChange={this.handleChangePrixServices}
                        defaultValue={value_services.price}
                        readOnly={value_services.isCheckedNo == true}
                        disabled={!this.state.editMode}
                      />
                    </div>
                  </div>
                )}
                {this.state.editMode && (
                  <div className="mt-3">
                    <button type="submit" className="btn btn-secondary">
                      Sauvegarder
                    </button>
                  </div>
                )}
              </SectionLayout>
              <br />
            </div>
          )
        })}
        <br />
        <div className="card rounded-0">
          <div className="card-body">
            <div className="row">
              <p className="col-6">Langues parlées</p>
              <a
                className="col-6 text-right"
                href="#"
                onClick={this.handleEditMode}
              >
                {!this.state.editMode == false ? 'Modifier' : 'Annuler'}
              </a>
              <br />
              {this.state.languages.map((value, index) => {
                return (
                  <div className="col-4" key={index}>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value={value.id}
                        id={value.id}
                        onClick={this.handleChekedPositionLanguage}
                        checked={value.isChecked}
                        disabled={!this.state.editMode}
                      />
                      <label className="form-check-label" htmlFor={value.id}>
                        {value.name}
                      </label>
                    </div>
                  </div>
                )
              })}
            </div>
            {this.state.editMode && (
              <div className="mb-6">
                <button type="submit" className="btn btn-secondary">
                  Sauvegarder
                </button>
              </div>
            )}
          </div>
        </div>
      </form>
    )
  }
}

export default Services
