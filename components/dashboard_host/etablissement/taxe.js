import React from 'react'
import { consoleLog } from '/utils/logConsole'
import { toast } from 'react-toastify'
import Cookies from 'universal-cookie'

/* Next */
import Router from 'next/router'

/* Components */
import { SectionLayout } from 'components/host/establishment'
import { Loader } from 'components/'

class Taxe extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user_data: '',
      stablishments: [],
      editMode: false,
      establishment_equipments: [],
      equipments: [],
      tva: [],
      revenues: [],
      isLoading: true
    }

    this.handleChangeTva = this.handleChangeTva.bind(this)
    this.handleChangehandleChangeDeclIncPro =
      this.handleChangehandleChangeDeclIncPro.bind(this)
    this.handleTaxe = this.handleTaxe.bind(this)
    this.handleEditMode = this.handleEditMode.bind(this)
  }

  async componentDidMount() {
    this.getInitialData()
  }

  async getInitialData() {
    const cookies = new Cookies()
    let _this = this

    //Get user data
    await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookies.get('tk_user')
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        }

        return response.json()
      })
      .then(function (user_data) {
        _this.setState({ user_data: user_data })
      })

    //Get stablishment
    const res_stablishments = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.user_data.id,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )

    const json_stablishments = await res_stablishments.json()
    this.setState({ ...this.state, stablishments: json_stablishments })

    let stablishments = this.state.stablishments

    stablishments.forEach((stablishment) => {
      if (stablishment.tva !== null && stablishment.tva > 0) {
        stablishment.isCheckedTvaYes = true
        stablishment.isCheckedTvaNo = false
      } else {
        stablishment.isCheckedTvaYes = false
        stablishment.isCheckedTvaNo = true
      }
      //consoleLog(stablishment.decl_inc_pro !== null)
      if (stablishment.decl_inc_pro !== null) {
        if (stablishment.decl_inc_pro == 1) {
          stablishment.isCheckedDeclIncProYes = true
          stablishment.isCheckedDeclIncProNo = false
        } else {
          stablishment.isCheckedDeclIncProYes = false
          stablishment.isCheckedDeclIncProNo = true
        }
      } else {
        stablishment.isCheckedDeclIncProYes = false
        stablishment.isCheckedDeclIncProNo = true
      }
    })

    this.setState({
      ...this.state,
      stablishments: stablishments,
      isLoading: false
    })
  }

  handleChangeTva() {
    let stablishments = this.state.stablishments

    stablishments.forEach((stablishment) => {
      stablishment.isCheckedTvaYes = !stablishment.isCheckedTvaYes
      stablishment.isCheckedTvaNo = !stablishment.isCheckedTvaNo
    })

    this.setState({ ...this.state, stablishments: stablishments })
  }

  handleChangehandleChangeDeclIncPro() {
    let stablishments = this.state.stablishments

    stablishments.forEach((stablishment) => {
      stablishment.isCheckedDeclIncProYes = !stablishment.isCheckedDeclIncProYes
      stablishment.isCheckedDeclIncProNo = !stablishment.isCheckedDeclIncProNo
    })

    this.setState({ ...this.state, stablishments: stablishments })
  }

  //POST save information establishment

  async handleTaxe(event) {
    event.preventDefault()
    const cookies = new Cookies()

    const res = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.state.stablishments[0].id,
      {
        body: JSON.stringify({
          name: this.state.stablishments[0].name,
          type_id: this.state.stablishments[0].type_id,
          clasification_id: this.state.stablishments[0].clasification_id,
          address: this.state.stablishments[0].address,
          lat: this.state.stablishments[0].lat,
          lng: this.state.stablishments[0].lng,
          country_id: this.state.stablishments[0].country_id,
          contact_name: this.state.stablishments[0].contact_name,
          contact_lastname: this.state.stablishments[0].contact_lastname,
          contact_email: this.state.stablishments[0].contact_email,
          contact_phone: this.state.stablishments[0].contact_phone,
          description: this.state.stablishments[0].description,
          min_entry_time: this.state.stablishments[0].min_entry_time,
          max_entry_time: this.state.stablishments[0].max_entry_time,
          max_exit_time: this.state.stablishments[0].max_exit_time,
          tva:
            event.target.tva_establissement.value == 1
              ? event.target.tva.value
              : 0,
          decl_inc_pro: event.target.decl_inc_pro.value,
          //amount_taxe: event.target.amount_taxe.value,
          slug: 'establishment-' + this.state.stablishments[0].id
        }),
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'PUT'
      }
    )

    await res.json()
    const status = res.status
    //consoleLog(result);
    //consoleLog(status);
    if (status == 200) {
      toast.success('Taxe mise à jour', {
        position: toast.POSITION.BOTTOM_LEFT
      })
      this.handleEditMode()
    } else {
      toast.error(`Une erreur s'est produite, réessayez plus tard`, {
        position: toast.POSITION.BOTTOM_LEFT
      })
    }
  }

  handleEditMode() {
    this.setState(
      { ...this.state, editMode: !this.state.editMode, isLoading: true },
      () => {
        if (this.state.editMode == false) {
          this.getInitialData()
        } else {
          this.setState({ ...this.state, isLoading: false })
        }
      }
    )
  }

  render() {
    if (this.state.isLoading) {
      return <Loader />
    }
    return (
      <form className="form" onSubmit={this.handleTaxe}>
        {this.state.editMode ? (
          <>
            <SectionLayout
              onModify={this.handleEditMode}
              isModifyMode={this.state.editMode}
              title="Taxe de séjour"
            >
              <div className="row">
                <div className="col-12 col-sm-6">
                  <p>
                    <strong>N° TVA</strong>
                  </p>

                  <div className="d-flex justify-content-between align-items-center">
                    <div className="bg-light w-100 mr-1">
                      <label className="w-100 ml-0 py-3 mb-0 text-center">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="tva_establissement"
                          value={1}
                          onClick={this.handleChangeTva}
                          checked={
                            this.state.stablishments.length > 0 &&
                            this.state.stablishments[0].isCheckedTvaYes
                          }
                          disabled={!this.state.editMode}
                        />
                        Oui
                      </label>
                    </div>

                    <div className="bg-light w-100 ml-1">
                      <label className="w-100 ml-0 py-3 mb-0 text-center">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="tva_establissement"
                          value={2}
                          onClick={this.handleChangeTva}
                          checked={
                            this.state.stablishments.length > 0 &&
                            this.state.stablishments[0].isCheckedTvaNo
                          }
                          disabled={!this.state.editMode}
                        />
                        Non
                      </label>
                    </div>
                  </div>
                </div>

                <div className="col-12 col-sm-6">
                  <label className="w-100">
                    <p>
                      <strong>Le numéro</strong>
                    </p>
                    <input
                      type="text"
                      className="form-control w-100 py-3 h-100"
                      name="tva"
                      aria-describedby="emailHelp"
                      placeholder=""
                      defaultValue={
                        this.state.stablishments.length > 0
                          ? this.state.stablishments[0].tva
                          : ''
                      }
                      min="0"
                      required={
                        this.state.stablishments.length > 0 &&
                        this.state.stablishments[0].isCheckedTvaYes
                      }
                      readOnly={
                        this.state.stablishments.length > 0 &&
                        this.state.stablishments[0].isCheckedTvaNo
                      }
                      disabled={!this.state.editMode}
                    />
                  </label>
                </div>
              </div>

              <div className="mt-4">
                <button type="submit" className="btn btn-secondary">
                  Sauvegarder
                </button>
              </div>
            </SectionLayout>

            <SectionLayout
              onModify={this.handleEditMode}
              isModifyMode={this.state.editMode}
              title="Revenus déclarés en tant que professionnel pour les impôts directs ?"
            >
              <div className="row">
                <div className="col-12 col-sm-6">
                  <div className="d-flex justify-content-between align-items-center">
                    <div className="bg-light w-100 mr-1">
                      <label className="w-100 ml-0 py-3 mb-0 text-center">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="decl_inc_pro"
                          value={1}
                          onClick={this.handleChangehandleChangeDeclIncPro}
                          checked={
                            this.state.stablishments.length > 0 &&
                            this.state.stablishments[0].isCheckedDeclIncProYes
                          }
                          disabled={!this.state.editMode}
                        />
                        Oui
                      </label>
                    </div>

                    <div className="bg-light w-100 ml-1">
                      <label className="w-100 ml-0 py-3 mb-0 text-center">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="decl_inc_pro"
                          value={0}
                          onClick={this.handleChangehandleChangeDeclIncPro}
                          checked={
                            this.state.stablishments.length > 0 &&
                            this.state.stablishments[0].isCheckedDeclIncProNo
                          }
                          disabled={!this.state.editMode}
                        />
                        Non
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div className="mt-4">
                <button type="submit" className="btn btn-secondary">
                  Sauvegarder
                </button>
              </div>
            </SectionLayout>
          </>
        ) : (
          <SectionLayout
            isModifyMode={this.state.editMode}
            onModify={this.handleEditMode}
            title="Informations fiscales"
          >
            <ul className="list-unstyled">
              <li className="border-bottom py-3">
                <p className="m-0">
                  <strong>Nº TVA : </strong>
                  <span>
                    {this.state.stablishments.length > 0 &&
                    this.state.stablishments[0].isCheckedTvaYes
                      ? 'Oui'
                      : 'Non'}
                  </span>
                </p>
              </li>
              <li className="border-bottom py-3">
                <p className="m-0">
                  <strong>Le numéro : </strong>
                  <span>
                    {this.state.stablishments.length > 0
                      ? this.state.stablishments[0].tva
                      : ''}
                  </span>
                </p>
              </li>
              <li className="border-bottom py-3">
                <p className="m-0">
                  <strong>
                    Revenus déclarés en tant que professionnel pour les impôts
                    directs :{' '}
                  </strong>
                  <span>
                    {this.state.stablishments.length > 0 &&
                    this.state.stablishments[0].isCheckedDeclIncProYes
                      ? 'Oui'
                      : 'Non'}
                  </span>
                </p>
              </li>
            </ul>
          </SectionLayout>
        )}
      </form>
    )
  }
}

export default Taxe
