import React from 'react'
import { consoleLog } from '/utils/logConsole'
import { toast } from 'react-toastify'
import { connect } from 'react-redux'
/* Next */
import Router from 'next/router'

import Cookies from 'universal-cookie'

/* Components */
import { SectionLayout } from '../../host/establishment'
import { Loader } from 'components/'
import {
  getConditions,
  getEstablishmentConditions
} from 'services/etablissementService'

class Terms extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user_data: '',
      stablishments: [],
      editMode: false,
      conditions: [],
      establishment_conditions: [],
      timeMax: '',
      isLoading: true
    }

    this.handleTimeMax = this.handleTimeMax.bind(this)

    this.handleTerms = this.handleTerms.bind(this)
    this.handleChekedPositionTerms = this.handleChekedPositionTerms.bind(this)
  }

  static async getInitialProps(ctx) {
    return { id: ctx.query.id }
  }

  async componentDidMount() {
    this.handleInitialInfo()
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.establishment.id !== this.props.establishment.id) {
      this.handleInitialInfo()
    }
  }

  async handleInitialInfo() {
    const establishment = this.props.establishment

    let [conditions, establishmentConditions] = await Promise.all([
      getConditions(),
      getEstablishmentConditions(establishment?.id)
    ])

    conditions.forEach((equipment) => {
      equipment.isCheckedNo = true
      equipment.isCheckedYes = false
    })

    establishmentConditions.forEach((establishment_condition) => {
      conditions.forEach((condition) => {
        if (condition.id == establishment_condition.condition_id) {
          condition.isCheckedYes = true
          condition.isCheckedNo = false
          condition.establishment_id = establishment_condition.id
        }
      })
    })

    this.setState({
      ...this.state,
      conditions,
      establishment_conditions: establishmentConditions,
      timeMax: establishment.max_entry_time,
      isLoading: false
    })
  }

  handleTimeMax(event) {
    //consoleLog(event);
    this.setState({ ...this.state, timeMax: event.target.value })
  }

  async handleChekedPositionTerms(event) {
    //consoleLog(event.target.value);
    let conditions = this.state.conditions
    conditions.forEach((condition) => {
      //consoleLog(condition.id, event.target.value);
      if (condition.id === parseInt(event.target.value)) {
        condition.isCheckedYes = !condition.isCheckedYes
        condition.isCheckedNo = !condition.isCheckedNo
      }

      //consoleLog(condition);
    })
    this.setState({ conditions: conditions })
    //consoleLog(this.state.conditions);
  }
  handleTimeMax(event) {
    this.setState({ ...this.state, timeMax: event.target.value })
  }

  //POST save information establishment
  async handleTerms(event) {
    event.preventDefault()
    //consoleLog(event.target.mini_arrive.value);
    const cookies = new Cookies()
    const res = await fetch(
      process.env.NEXT_PUBLIC_API_URL +
        'establishments/' +
        this.props.establishment.id,
      {
        body: JSON.stringify({
          ...this.props.establishment,
          min_entry_time: event.target.mini_arrive.value,
          max_entry_time: event.target.max_arrive.value,
          max_exit_time: event.target.max_go.value,
          is_active: this.props.establishment.active
        }),
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'PUT'
      }
    )

    const status = res.status
    //consoleLog(result);
    //consoleLog(status);
    if (status == 200) {
      this.state.conditions.forEach((condition) => {
        //consoleLog(condition);
        if (condition.isCheckedYes) {
          fetch(process.env.NEXT_PUBLIC_API_URL + 'establishment-conditions', {
            body: JSON.stringify({
              establishment_id: this.props.establishment.id,
              condition_id: condition.id
            }),
            headers: {
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + cookies.get('tk_user')
            },
            method: 'POST'
          })
        } else {
          fetch(
            process.env.NEXT_PUBLIC_API_URL +
              'establishment-conditions/' +
              condition.establishment_id,
            {
              headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookies.get('tk_user')
              },
              method: 'DELETE'
            }
          )
        }
      })
      toast.success('Modifications sauvegardées', {
        position: toast.POSITION.BOTTOM_LEFT
      })
      this.setState({ ...this.state, editMode: false })
    } else {
      toast.error(`Une erreur s'est produite, réessayez plus tard`, {
        position: toast.POSITION.BOTTOM_LEFT
      })
    }
  }

  getCondition(id) {
    return this.state.establishment_conditions.find(
      (condition) => condition.condition_id === id
    )
  }

  render() {
    if (this.state.isLoading) {
      return <Loader />
    }
    return (
      <form className="form" onSubmit={this.handleTerms}>
        <SectionLayout
          isModifyMode={this.state.editMode}
          onModify={() =>
            this.setState({ ...this.state, editMode: !this.state.editMode })
          }
          title="Heure d'arrivée et de départ"
        >
          <div className="row">
            <div className="col-sm-6 col-12 mb-3">
              <label htmlFor="mini_arrive">
                <p className="m-0 font-montserrat">
                  <strong>Heure d'arrivée (MINI)</strong>
                </p>
              </label>
              <div className="input-group mb-2 rounded-2">
                <div className="input-group-prepend">
                  <div className="input-group-text border-0">MINI *</div>
                </div>
                <input
                  type="time"
                  className="form-control py-4 border-0"
                  id="mini_arrive"
                  name="mini_arrive"
                  required
                  max={this.state.timeMax}
                  defaultValue={this.props.establishment.min_entry_time}
                  disabled={!this.state.editMode}
                />
              </div>
            </div>
            <div className="col-sm-6 col-12 mb-3">
              <label htmlFor="max_arrive">
                <p className="m-0 font-montserrat">
                  <strong>Heure d'arrivée (MAX)</strong>
                </p>
              </label>
              <div className="input-group mb-2 rounded-2">
                <div className="input-group-prepend">
                  <div className="input-group-text border-0">MAX *</div>
                </div>
                <input
                  onChange={(event) => {
                    this.handleTimeMax(event)
                  }}
                  type="time"
                  className="form-control py-4 border-0"
                  id="max_arrive"
                  name="max_arrive"
                  required
                  defaultValue={this.props.establishment.max_entry_time}
                  disabled={!this.state.editMode}
                />
              </div>
            </div>
            <div className="col-sm-6 col-12 mb-3">
              <label htmlFor="max_go">
                <p className="m-0 font-montserrat">
                  <strong>Heure de départ</strong>
                </p>
              </label>
              <div className="input-group mb-2 rounded-2">
                <div className="input-group-prepend">
                  <div className="input-group-text border-0">MAX *</div>
                </div>
                <input
                  type="time"
                  className="form-control py-4 border-0"
                  id="max_go"
                  name="max_go"
                  required
                  defaultValue={this.props.establishment.max_exit_time}
                  disabled={!this.state.editMode}
                />
              </div>
            </div>
          </div>
          {this.state.editMode && (
            <div className="mt-4">
              <button type="submit" className="btn btn-secondary">
                Sauvegarder
              </button>
            </div>
          )}
        </SectionLayout>
        <SectionLayout
          isModifyMode={this.state.editMode}
          onModify={() =>
            this.setState({ ...this.state, editMode: !this.state.editMode })
          }
          title="Conditions particulières"
        >
          {this.state.editMode ? (
            <div className="row">
              {this.state.conditions.map((value, index) => {
                return (
                  <div
                    key={index}
                    className="d-flex justify-content-between align-items-center mb-3"
                  >
                    <div className="">
                      <p className="m-0 host-dashboard__conditions__label">
                        {value.name}
                      </p>
                    </div>

                    <div className="d-flex justify-content-between align-items-center">
                      <div className="form-check bg-light pl-5 pr-4 py-3">
                        <input
                          className="form-check-input"
                          type="radio"
                          name={value.id}
                          id={value.id}
                          value={value.id}
                          onClick={this.handleChekedPositionTerms}
                          checked={value.isCheckedYes}
                          disabled={!this.state.editMode}
                        />
                        <label className="form-check-label" htmlFor={value.id}>
                          Oui
                        </label>
                      </div>

                      <div className="form-check bg-light pl-5 pr-4 py-3 ml-4">
                        <input
                          className="form-check-input"
                          type="radio"
                          name={value.id}
                          id={value.id}
                          value={value.id}
                          onClick={this.handleChekedPositionTerms}
                          checked={value.isCheckedNo}
                          disabled={!this.state.editMode}
                        />
                        <label className="form-check-label" htmlFor={value.id}>
                          Non
                        </label>
                      </div>
                    </div>
                  </div>
                )
              })}
              <div className="mt-4">
                <button type="submit" className="btn btn-secondary">
                  Sauvegarder
                </button>
              </div>
            </div>
          ) : (
            <ul className="list-unstyled">
              {this.state.conditions.map((condition) => (
                <li key={condition.id} className="border-bottom py-3">
                  <div className="">
                    <p className="mb-0">
                      <strong>{condition.name} : </strong>
                      <span>
                        {!!this.getCondition(condition.id) ? 'Oui' : 'Non'}
                      </span>
                    </p>
                  </div>
                </li>
              ))}
            </ul>
          )}
        </SectionLayout>
      </form>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user, establishment: state.establishment.data }
}
export default connect(mapStateToProps, null)(Terms)
