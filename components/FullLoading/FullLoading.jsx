import React from 'react'
import { connect } from 'react-redux'
import { Loader } from '..'

function FullLoading({ isLoading, isOpen, message = 'Loading...', isClear }) {
  return (
    <>
      <div
        className={`pageloader ${isOpen || isLoading ? 'is-active' : ''} ${
          isClear ? 'is-clear' : ''
        }`}
      >
        <Loader />
        {message}
      </div>
      <div
        className={`infraloader ${isOpen || isLoading ? 'is-active' : ''}`}
      ></div>
      <div
        className={`app-overlay ${isOpen || isLoading ? 'is-active' : ''} ${
          isClear ? 'is-clear' : ''
        }`}
      ></div>
    </>
  )
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.configuration.isLoading,
    isClear: state.configuration.isClear
  }
}

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(FullLoading)
