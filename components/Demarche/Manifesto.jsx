import React from 'react'
import Marie from 'public/images/demarche/marie.png'

export const Manifesto = () => {
  return (
    <div className="demarche-manifesto">
      <h3 className="main-title mb-5">Manifesto</h3>
      <p>
        {`Si vous êtes arrivés jusqu'ici, c'est que vous êtes curieux d'en apprendre davantage sur nous, et on s’en réjouit !

        Chez We Go GreenR, nous mettons tout en oeuvre pour vous faire profiter de séjours respectueux de la planète, des Hommes et dont les retombées économiques feront surtout vivre toute une économie locale, sans pour autant sacrifier à la beauté ou au confort.

        Notre mission : vous faire vivre votre séjour de rêve sans compromis. \n`}
      </p>

      <h4 className="title-2">{`Mais en fait, pourquoi voyager autrement ?\n`}</h4>

      <h5 className="title-6"></h5>
      <p>
        <b>Partir moins loin.</b>
        {`\nCa n’est pas un secret, prendre l’avion pour un court séjour c’est tentant mais ça coute à la planète. Quand on sait que 70% des émissions carbone sont associées au transport, on se dit qu’il est temps de réconcilier voyage et environnement... Alors, le temps d’un séjour, nous vous proposons de partir moins loin et de vous laissez vous dépayser en (re)découvrant les nombreuses facettes de nos paysages français.\n`}
      </p>
      {/* <h5 className="title-6 title-6-variant">{``}</h5> */}

      <p>
        <b>Séjourner mieux.</b>
        {`\nLors de vos séjours, en vacances ou même juste pour un weekend, faire le choix d’un hébergement engagé contribue à réduire votre empreinte écologique. Choisir un hôte qui gère son établissement de manière raisonnée et qui fait sa part c’est un peu comme prolonger les actions mises en place au sein de votre foyer même pendant les vacances. 

        Laissez-vous surprendre et apprenez-en plus pendant votre séjour grâce à des hôtes qui ont a coeur de préserver leur écosystème.\n`}
      </p>

      <h4 className="title-5 ">{`Alors on fait comment ? \n`}</h4>
      <p>
        {`Où partir ? Comment trouver facilement un séjour responsable, séduisant et confortable ? Comment s’y retrouver dans la jungle des écolabels ? 

        Après avoir identifié ce problème, et surtout l'avoir vécu, le concept We Go GreenR germait naturellement… 
         We Go GreenR souhaite ainsi redonner du sens à l’expression “bonnes vacances”. 

        Depuis 2020, We Go GreenR met en relation des hôtes engagés et des voyageurs soucieux de voyager mieux, de faire attention à leur impact. Nous avons souhaité créer une plateforme qui vous facilite l'accès à un tourisme plus responsable.

        Gîtes, cabanes, chambres d'hôtes, écolodges, hôtels... ? Rando, surf, vélo, yoga et relaxation… ? 
        Nous proposons une solution pour toutes vos envies et tous vos moments de vie pour sortir de votre quotidien. 
        Et bientôt nos offres s’étofferont pour vous offrir une large palette d’expériences à faire sur place ! \n`}
      </p>
      <h4 className="title-5 ">{`En toute transparence \n`}</h4>
      <p>
        {`Nos hôtes répondent à un questionnaire de 118 critères générant un "GreenScore" qui leur sert de passeport pour être référencés sur notre plateforme et nous rendons lisibles l’ensemble des engagements sur chaque page hébergements. Nos hôtes bénéficient d’un suivi personnalisé avec une personne de notre équipe, et un temps d’échange approfondi est systématiquement effectué afin de mieux comprendre leurs démarches et leurs engagements. Nous rencontrons chaque année plusieurs centaines d’hôtes afin de retranscrire au mieux l’expérience qu’ils proposent. Et bien évidement les retours de nos voyageurs sont aussi pris en compte et nous aident à maintenir un niveau d’écoresponsabilité et de qualité véritables.  \n`}
      </p>

      <h4 className="title-5 ">{`Une équipe de passionnés... \n`}</h4>

      <p>
        {`Un projet de telle ampleur ne peut pas être porté par des personnes dénuées de passion. Tous les membres de l'équipe We Go GreenR sont personnellement déjà engagés dans une transition écologique quotidienne et en quête réelle d'un tourisme plus durable . Nous sommes vanlifers, campeurs, co-voyageurs, baroudeurs adeptes du train, en quête réélle d’un tourisme plus durable. Parce-que nous incarnons d'abord nos convictions avant de les transmettre au plus grand nombre. \n`}
      </p>

      <div className="last-text">
        <h4 className="title-5 ">
          {`Et parce qu’ensemble on va plus loin...\n`}{' '}
        </h4>

        <p>
          {`Pour que ça marche, il faut que les choses changent, bougent, évoluent. Grâce à vous, et à travers vos futurs voyages optez pour des séjours qui ont du sens. `}
        </p>
      </div>

      <div className="mt-4 responsive-content">
        <div>
          <h2 className="firm">Marie & Stéphane</h2>
          <span>Co-fondateurs We Go GreenR</span>
        </div>

        <img src={Marie} alt="Marie & stephane" className="marie-photo" />
      </div>
    </div>
  )
}
