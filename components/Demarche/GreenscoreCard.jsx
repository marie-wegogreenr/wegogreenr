import React from 'react'

const GreenscoreCard = ({ text, icon, reverse = false }) => {
  return (
    <div
      className={
        'd-flex flex-row-reverse align-items-center justify-content-evenly justify-content-lg-center greenscoreCard my-3'
      }
    >
      <p className={`text-left m-0 mx-lg-4 mx-0 greenscore__general-text`}>
        {text}
      </p>
      <img src={icon} alt="" width="40" height="40" />
    </div>
  )
}

export default GreenscoreCard
