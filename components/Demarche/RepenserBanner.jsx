import React from 'react'
import bannerImage from 'public/images/demarche/green-image.png'

export const RepenserBanner = () => {
  return (
    <div className="repenser">
      <div className="repenser-content">
        <div className="text-content">
          <h2 className="text-white">Repenser le sens des bonnes vacances</h2>
          <p className="text-white">
            <b>We Go GreenR</b> écrit pas à pas l'histoire de son engagement
            pour l'environnement et vous propose de voyager moins loin et de
            séjourner mieux à travers une sélection d'établissements et
            d'activités aussi désirables que responsables.
          </p>
        </div>
        <img src={bannerImage} alt="green-image" className="banner-image" />
      </div>
    </div>
  )
}
