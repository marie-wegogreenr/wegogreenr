import ConceptCard from './ConceptCard'
//assets
import { HouseWithLeaf, SingleCayak, ShieldChecked } from '@components/Icons'
import yellowLine from 'public/images/icons/yellow-curved-line-long.svg'
import house from 'public/images/demarche/house.svg'
import heart from 'public/images/demarche/heart.svg'
import kayak from 'public/images/demarche/kayak.svg'
import leaf from 'public/images/demarche/leaf.svg'

const Concept = () => {
  const data = {
    title: `Le concept We Go GreenR`,
    text: `Notre mission : Vous faire offre votre séjour de rêve sans compromis. Profitez d'une expérience éco-positive respectueuse de la planète, des Hommes, tout en faisant vivre l’économie locale.​`,
    cards: [
      {
        id: 1,
        img: house,
        text: `Une sélection d'hébergements de qualité engagés dans une démarche écologique, pour tous les moments de vie.`
      },
      {
        id: 2,
        img: heart,
        text: `Des hôtes animés par leur métier et fiers de leur terroir, à l'écoute des envies des voyageurs et soucieux de transmettre leur passion.​​​`
      },
      {
        id: 3,
        img: leaf,
        text: `Des voyageurs ayant l'envie de réduire leur impact, et de retrouver le sens des bonnes vacances.`
      },
      {
        id: 4,
        img: kayak,
        text: `Des activités green et slow pour des séjours uniques, ressourçants et inoubliables.​`
      }
    ]
  }

  return (
    <div className="text-center py-5 bg-crema-light">
      <h2 className="">{data.title}</h2>
      <p className="demarche__concept-text">{data.text}</p>
      <div className="concept-card-content mt-4">
        {data.cards.map((item) => (
          <ConceptCard key={item.id} icon={item.img} text={item.text} />
        ))}
      </div>
      {/* <button className="btn btn-secondary py-3 br-10 weight-semibold paragraph px-5 mt-5">
        En Savoir plus
      </button> */}
    </div>
  )
}

export default Concept
