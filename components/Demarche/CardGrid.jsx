import React from 'react'
import CardGridCard from './CardGridCard'
//ASSETS
import image1 from 'public/images/demarche/103.png'
import image2 from 'public/images/demarche/129.png'
import image3 from 'public/images/demarche/130.png'

const card1Text =
  'Pour garantir un séjour chaleureux et de qualité aux voyageurs, ils ont le souci du détail et ont à cœur de recevoir dans un environnement authentique et préservé.'

const card2Text =
  'Chaque nouvelle rencontre est un véritable bonheur ! Ils regorgent de conseils pour vous faire vivre le séjour idéal.'

const card3Text =
  'Leur démarche écologique est vérifiée : nous interviewons nos hôtes et partageons avec vous la genèse de leur projet et leurs actions menées. Leur réputation est à la hauteur de leurs engagements écologiques et nous vérifions continuellement que la barre reste haute ;)'

const CardGrid = () => {
  const data = {
    title: `En savoir plus sur nos hôtes`
  }

  return (
    <div className="demarche-card-section">
      <h2 className="demarche-card-section__title">{data.title}</h2>
      <div className="demarche-cardGrid">
        <div>
          <CardGridCard
            title={`Ils prennent soin de leur propriété`}
            text={card1Text}
          />
        </div>
        <div>
          <img
            src={image1}
            alt=""
            className=" h-100 w-100 br-10 obj-fit-cover demarche-card-section__img"
          />
        </div>
        <div>
          <CardGridCard
            title="Ils aiment transmettre et partager"
            text={card2Text}
          />
        </div>
        <div>
          <img
            src={image2}
            alt=""
            className="h-100 w-100 br-10 obj-fit-cover demarche-card-section__img"
          />
        </div>
        <div>
          <img
            src={image3}
            alt=""
            className="h-100 w-100 br-10 obj-fit-cover demarche-card-section__img"
          />
        </div>
        <div>
          <CardGridCard title="Ils sont engagés​" text={card3Text} />
        </div>
      </div>
    </div>
  )
}

export default CardGrid
