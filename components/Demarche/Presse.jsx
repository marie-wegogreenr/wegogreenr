import React from 'react'
import PresseCard from './PresseCard'

const title = 'Que veulent les clients maintenant et que pouvez-vous y faire ?'
const Presse = ({ margin = true }) => {
  const secondClass = margin
    ? 'bg-primary px-5 py-4 br-10 w-100 d-flex justify-content-center flex-column me-4'
    : 'bg-primary px-5 py-4 br-10 w-100 d-flex justify-content-center flex-column'

  const data = {
    title: `Accès presse`,
    text: `Fondée en 2020 par Marie-Pierre et Stéphane deux amoureux de voyage et de nature, We Go GreenR a su rassembler en peu de temps la plus grande communauté d'hôtes responsables en France avec plus de 1000 hébergements partout en France. `
  }

  return (
    <div className="wrapper-4 d-flex flex-column flex-lg-row mt-5 py-4">
      <div className={secondClass}>
        <h2 className="color-white">{data.title}</h2>
        <p className="color-white mt-3">{data.text}</p>
        <p className="color-white">
          Téléchargez notre{' '}
          <a
            href="/documents/Press-Kit-WeGoGreenR2022.pdf"
            className="demarche__link"
            download
          >
            dossier de presse{' '}
          </a>
          et{' '}
          <a href="/documents/Photos.zip" className="demarche__link" download>
            accédez au dossier photos
          </a>
        </p>
      </div>
      {/* <div className="w-100 mt-4 mt-lg-0 ms-lg-4 d-flex flex-column justify-content-center align-items-center">
        <PresseCard title={title} />
        <PresseCard title={title} />
        <PresseCard title={title} margin={false} />
      </div> */}
    </div>
  )
}

export default Presse
