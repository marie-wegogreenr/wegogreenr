import React from 'react'
import { consoleLog } from '/utils/logConsole'
import Image from 'next/image'

import img1 from 'public/images/demarche/demarche_001.png'
import img2 from 'public/images/demarche/demarche_002.png'
import img3 from 'public/images/demarche/demarche_003.png'

const MainGrid = () => {
  return (
    <div className="info-banner py-5">
      <div className="demarche-MainGrid mt-3 mt-md-0">
        <div className="demarche-MainGrid-text">
          <h2 className="color-white lh-12 h2 weight-bold">
            Repenser le sens des <br className="demarche-MainGrid-br" /> bonnes
            vacances
          </h2>
          <div className="w-75 mt-4 demarche-MainGrid-container">
            <p className="paragraph demarche-MainGrid-p color-white">
              <b>We Go GreenR</b> écrit pas-à-pas l'histoire de son engagement
              pour l'environnement et vous propose de voyager moins loin et de
              séjourner mieux à travers une sélection d'établissements et
              d'activités aussi désirables que responsables
            </p>
          </div>
        </div>
        <div className="demarche-MainGrid-pics">
          <div className="demarche-MainGrid-grid">
            <img src={img1} width="300" height="300" alt="" className="br-10" />

            <img src={img2} width="300" height="300" alt="" className="br-10" />

            <img src={img3} width="300" height="300" alt="" className="br-10" />
          </div>
        </div>
      </div>
    </div>
  )
}

export default MainGrid
