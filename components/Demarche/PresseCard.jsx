import Image from 'next/image'
import React from 'react'
import img1 from 'public/images/icons/home/logo-lemonde.png'

const PresseCard = ({ title, margin = true }) => {
  return (
    <div className={`d-flex align-items-center ${margin ? 'mb-3' : ''}`}>
      <div className="shdow-1 br-10 bg-white">
        <img
          src={img1}
          // height={80}
          width={125}
          className="br-10 "
        />
      </div>
      <div className="ms-4">
        <h2
          className="weight-bold text font-montserrat w-100"
          style={{ lineHeight: '1.25' }}
        >
          {title}
        </h2>
        <a href="#">Plus d'inspirations</a>
      </div>
    </div>
  )
}

export default PresseCard
