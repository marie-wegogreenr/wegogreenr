import React from 'react'
import { consoleLog } from '/utils/logConsole'
//ASSETS
import quotes from 'public/images/icons/quotes.svg'
import yellowLine from 'public/images/icons/yellow-curved-line.svg'

const Quotes = () => {
  return (
    <div className="d-flex justify-content-center align-items-center pb-5">
      <div className="mt-5 pt-5 text-center position-relative">
        <img src={quotes} className="quotes-leftQuote position-absolute" />
        <p className="h2-dashboard text-center weight-bold text-decoration-none lh-12">
          Tenter de réduire votre impact
          <br />
          en proposant des séjours de qualité.
          <br />
          Voilà notre ambition
        </p>
        <br />
        <p className="m-0 weight-bold mt-3 paragraph">Marie &amp; Stéphane</p>
        <p className="m-0 paragraph">Co-fondateurs We Go GreenR</p>
        <img src={quotes} className="quotes-rigthQuote position-absolute" />
      </div>
    </div>
  )
}

export default Quotes
