import React from 'react'
import { consoleLog } from '/utils/logConsole'

const CardGridCard = ({ text, title }) => {
  return (
    <div className="bg-primary p-5 p-lg-4 p-xl-5 h-100 br-10">
      <p className="weight-bold m-0 color-white paragraph">{title}</p>
      <p className="my-2 color-white">{text}</p>
    </div>
  )
}

export default CardGridCard
