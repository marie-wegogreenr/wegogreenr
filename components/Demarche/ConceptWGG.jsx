import * as React from 'react'
import Link from 'next/link'
import house from 'public/images/demarche/house.svg'
import heart from 'public/images/demarche/heart.svg'
import kayak from 'public/images/demarche/kayak.svg'
import leaf from 'public/images/demarche/leaf.svg'
import { HouseWithLeaf, SingleCayak, ShieldChecked } from '@components/Icons'
export const ConceptWGG = () => {
  return (
    <div className="concept-content">
      <div className="concept-content-title">
        <h2 className="">Le concept We Go GreenR</h2>
        <p className="">
          {`Notre mission : Vous faire offre votre séjour de rêve sans
              compromis. Profitez d'une expérience éco-positive respectueuse de la planète, des Hommes, tout en faisant vivre 
              l’économie locale. `}
        </p>
      </div>

      <ul className="list-items">
        <li className="list-item">
          <div className="list-icon">
            <img src={house} alt="house" />
          </div>
          <p className="">
            Une sélection d'hébergements de qualité engagés dans une démarche
            écologique, pour tous les moments de vie.
          </p>
        </li>
        <li className="list-item">
          <div className="list-icon">
            <img src={heart} alt="house" />
          </div>
          <p className="">
            Des hôtes animés par leur métier et fiers de leur terroir, à
            l'écoute des envies des voyageurs et soucieux de transmettre leur
            passion.​
          </p>
        </li>
        <li className="list-item">
          <div className="list-icon">
            <img src={leaf} alt="house" />
          </div>
          <p className="">
            Des voyageurs ayant l'envie de réduire leur impact, et de retrouver
            le sens des bonnes vacances. ​
          </p>
        </li>
        <li className="list-item">
          <div className="list-icon">
            <img src={kayak} alt="house" />
          </div>
          <p className="">
            Des activités green et slow pour des séjours uniques, ressourçants
            et inoubliables.​
          </p>
        </li>
      </ul>
      <Link href="/demarche">
        <button className="btn btn-secondary font-montserrat weight-semibold px-4 py-2 btn-home-1 mb-4 our-spirit__content-btn">
          En Savoir plus
        </button>
      </Link>
    </div>
  )
}
