import React from 'react'
import { consoleLog } from '/utils/logConsole'

const Hero = () => {
  return (
    <div className="position-relative demarche-hero d-flex justify-content-center align-items-center">
      <div className="hero-gradient"></div>
      <h1 className="demarche-hero-h1 weight-bold font-fraunces color-white">
        Séjourner autrement
      </h1>
    </div>
  )
}

export default Hero
