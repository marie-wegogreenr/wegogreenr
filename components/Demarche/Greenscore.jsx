import React from 'react'
import GreenscoreCard from './GreenscoreCard'
//assets
import gScore from 'public/images/onboarding/greenscore/final_score/result_greenscore_4.png'
import Image from 'next/image'
import Leaf from 'public/images/icons/action-eco.svg'
import Alimentation from 'public/images/icons/alimentation.svg'
import Entretien from 'public/images/icons/entretien.svg'
import GestionEnergie from 'public/images/icons/Gestion-energie.svg'
import GestionEau from 'public/images/icons/gestion-eau.svg'
import Hebergement from 'public/images/icons/hebergement-engage.svg'
import ReductionDechets from 'public/images/icons/reduction-dechets-compost.svg'
import Transport from 'public/images/icons/transport-velo-location-velo-.svg'

const Greenscore = () => {
  const data = {
    mainText: `Nous avons créé le « GreenScore » pour mesurer et qualifier l’engagement écoresponsable réel des hébergements présents sur notre site. Positionné sur toutes les pages d'hébergement, il est symbolisé par cinq petites feuilles  vous permettant de visualiser très simplement leur niveau d'implication. Il est gratuit et obligatoire pour permettre à l’hébergement d’être référencé. Ainsi, les hébergements n’atteignant pas la note minimale de 1 sur 5 ne sont pas sélectionnés sur notre site, mais nous leur proposons un accompagnement pour les aider à enclencher la transition écoresponsable. `
  }

  return (
    <div className="py-5 text-center GreenScore-demarche-content">
      <h2 className="color-primary greenscore__main-title">
        <span className="greenscore__main-title-span">
          Un GreenScore pour passeport{' '}
        </span>
        <span className="greenscore__main-title-span">
          Notre indice d'engagement écoresponsable
        </span>
      </h2>
      <p className="greenscore__main-text greenscore__general-text">
        {data.mainText}
      </p>
      <p className="weight-bold greenscore__title-text">
        Les critères du GreenScore
      </p>
      <Image
        src={gScore}
        width={210}
        height={26.6}
        className="greenscore__title-img"
      />

      {/* <div className="greenscore__title">
      </div> */}
      <div className="d-flex justify-content-between flex-column flex-lg-row wrapper mt-4 demarche__greenscore-content">
        <div className="demarche__greenscore-info">
          <GreenscoreCard
            icon={Hebergement}
            text="Habitats durables (matériaux construction/rénovation)"
          />
          <GreenscoreCard icon={GestionEnergie} text="Gestion de l’énergie" />
          <GreenscoreCard icon={GestionEau} text="Gestion de l'eau" />
          <GreenscoreCard
            icon={ReductionDechets}
            text="Réduction des déchets"
          />
        </div>

        <div className="demarche__greenscore-info">
          <GreenscoreCard
            icon={Entretien}
            text="Équipements et entretien du quotidien"
            reverse={true}
          />
          <GreenscoreCard
            icon={Transport}
            text="Transports et mobilité"
            reverse={true}
          />
          <GreenscoreCard
            icon={Leaf}
            text="Actions écoresponsables"
            reverse={true}
          />
          <GreenscoreCard
            icon={Alimentation}
            text="Alimentation"
            reverse={true}
          />
        </div>
      </div>
    </div>
  )
}

export default Greenscore
