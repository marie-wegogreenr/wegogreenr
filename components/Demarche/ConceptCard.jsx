import React from 'react'

const ConceptCard = ({ icon, text }) => {
  return (
    <div className="text-center mx-3 px-5 concept-container">
      <div className="icon-concept">
        <img src={icon} alt="house" />
      </div>
      <p className="mt-3">{text}</p>
    </div>
  )
}

export default ConceptCard
