import React, { useState, useEffect } from 'react'
import { consoleLog } from '/utils/logConsole'
//REDUX
import { useDispatch } from 'react-redux'
import {
  updateReservationUsersAction,
  updateReservationStatusAction
} from '@ducks/checkoutDuck'
import { useRouter } from 'next/router'

const UsersForm = ({ reservation }) => {
  const dispatch = useDispatch()
  const router = useRouter()

  const {
    adults,
    children,
    room: { people_capacity }
  } = reservation

  const [isEditing, setIsEditing] = useState(false)

  const [form, setForm] = useState({
    adults,
    children
  })

  let total_people = Number(form.adults) + Number(form.children)
  let overload = total_people > people_capacity ? true : false

  const handleChange = (e) => {
    if (e.target.value > people_capacity) return
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  useEffect(() => {
    dispatch(updateReservationUsersAction(form))
    if (overload) {
      dispatch(updateReservationStatusAction(false))
    } else dispatch(updateReservationStatusAction(true))
  }, [form])

  return (
    <>
      <div className={`${isEditing ? 'd-none' : ''}`}>
        <div className="d-flex justify-content-between mb-2">
          <p className="text subtitle weight-bold m-0">Voyageurs</p>
          {/* <a
            className="weight-bold m-0 underline-0 text-block"
            href="#"
            onClick={() => setIsEditing(true)}
          >
            Modifier
          </a> */}
        </div>
        <div>
          <p>
            {`${form.adults} ${
              parseInt(form.adults) === 1 ? 'Adult' : 'Adultes'
            }`}{' '}
            -{' '}
            {`${form.children} ${
              parseInt(form.children) === 1 ? 'Enfant' : 'Enfants'
            }`}
          </p>
        </div>
      </div>

      {isEditing && adults != undefined && (
        <>
          <div className="d-flex justify-content-between">
            <p className="text weight-bold m-0">Voyageurs</p>
            {!overload && (
              <a
                className="weight-bold m-0"
                href="#"
                onClick={() => setIsEditing(false)}
              >
                Anuler
              </a>
            )}
          </div>
          <div>
            <label className="text d-flex flex-column mt-4">
              Enfants
              <input
                type="number"
                value={form.children}
                onChange={handleChange}
                name="children"
                className="w-50"
                max={people_capacity}
                onWheel={(e) => e.target.blur()}
              />
            </label>
            <label className="text d-flex flex-column">
              Adultes
              <input
                type="number"
                value={form.adults}
                onChange={handleChange}
                name="adults"
                className="w-50"
                max={people_capacity}
                onWheel={(e) => e.target.blur()}
              />
            </label>
          </div>
          <p>
            {overload
              ? `Ce logement peut accueillir au maximum ${people_capacity} voyageurs`
              : ''}
          </p>
          <button
            className="btn btn-primary py-3 mt-3 w-50"
            onClick={() => setIsEditing(false)}
            disabled={overload}
          >
            Sauvegarder
          </button>
        </>
      )}
    </>
  )
}

export default UsersForm
