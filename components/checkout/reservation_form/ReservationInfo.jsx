import React from 'react'
import { consoleLog } from '/utils/logConsole'
import DatesForm from './DatesForm'
import { useSelector } from 'react-redux'
import UsersForm from './UsersForm'
import { useRouter } from 'next/router'

const ReservationInfo = () => {
  const store = useSelector((store) => store.checkoutReducer)
  const router = useRouter()
  const { reservation, loaded } = store

  if (typeof window !== 'undefined') !reservation.room ? router.push('/') : null

  //consoleLog(reservation, 'resss')

  if (reservation.room && loaded && typeof window !== 'undefined') {
    return (
      <div>
        <h2 className="font-montserrat h2-dashboard">Votre séjour</h2>
        <div className="border-bottom-divisor mt-3 mb-4"></div>
        <DatesForm reservation={reservation} />
        <div className="mt-4"></div>
        <UsersForm reservation={reservation} />
      </div>
    )
  } else {
    return <>Please reserve something</>
  }
}

export default ReservationInfo
