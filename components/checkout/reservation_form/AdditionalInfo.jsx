import { useEffect, useState } from 'react'
import Bathroom from 'public/images/icons/checkout/bath-tub.png'
import Bed from 'public/images/icons/checkout/bed.png'
import Door from 'public/images/icons/checkout/door.png'
import User from 'public/images/icons/checkout/user.png'

function AdditionalInfo({ data }) {
  /*  Destructuring  */
  const { rooms_quantity, bathroom_quantity, room_beds } = data?.room
  /*  Local states */
  const [dataToShow, setDataToShow] = useState([])
  const [roomBeds, setRoomBeds] = useState(1)
  /*  Effects  */
  useEffect(() => {
    if (Array.isArray(room_beds) && room_beds.length) {
      const beds = room_beds.reduce((acc, curr) => acc + curr.quantity, 0)
      setRoomBeds(beds)
    }
  }, [data])

  useEffect(() => {
    if (data && roomBeds) {
      const _data = [
        { id: 'travellers', icon: User, quantity: data.adults + data.children },
        { id: 'bedroom', icon: Door, quantity: rooms_quantity },
        { id: 'beds', icon: Bed, quantity: roomBeds },
        { id: 'bathroom', icon: Bathroom, quantity: bathroom_quantity }
      ]

      setDataToShow([..._data])
    }
  }, [data, roomBeds])

  return (
    <ul className="d-flex">
      {dataToShow.map((item, index) => (
        <li key={item.id} className="me-3 d-flex align-items-center">
          <img
            width="24px"
            src={item.icon}
            alt={`${item.id}-icon`}
            className="mx-2"
            style={{ transform: index === 3 ? 'translateY( -0.17rem' : '' }}
          />
          <span className="h3 color-black m-0">{item.quantity}</span>
        </li>
      ))}
    </ul>
  )
}

export default AdditionalInfo
