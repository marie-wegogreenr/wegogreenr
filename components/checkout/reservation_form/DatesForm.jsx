import ArrivePopUp from '@components/Hebergements/Single/Form/ArrivePopUp'
import DeparturePopUp from '@components/Hebergements/Single/Form/DeparturePopUp'
import React, { useEffect, useState } from 'react'

import { matchUnavailableDates } from '../../Hebergements/Single/Form/matchSpecialDates'
import moment from 'moment'
//REDUX

import {
  updateReservationDatesAction,
  updateReservationStatusAction
} from '@ducks/checkoutDuck'
import { useDispatch, useSelector } from 'react-redux'
import { roundTimeToNearest } from 'helpers/timeHelper'

const DatesForm = ({ reservation }) => {
  const dispatch = useDispatch()
  const validReservation = useSelector((store) => store.checkoutReducer.valid)

  const { checkIn, checkOut, unavailableDates } = reservation

  const [isEditing, setIsEditing] = useState(false)
  const [timeLimits, setTimeLimits] = useState({})

  const [form, setForm] = useState({
    checkIn,
    checkOut
  })

  useEffect(() => {
    if (form.checkIn && form.checkOut && unavailableDates) {
      if (
        matchUnavailableDates(form.checkIn, form.checkOut, unavailableDates)
      ) {
        dispatch(updateReservationStatusAction(false))
      } else {
        dispatch(updateReservationStatusAction(true))
      }
    }
    dispatch(updateReservationDatesAction(form))
  }, [form])

  useEffect(() => {
    setTimeLimits({
      arrivalMin: roundTimeToNearest(reservation.establishment.min_entry_time),
      arrivalMax: roundTimeToNearest(reservation.establishment.max_entry_time)
    })
  }, [])

  return (
    <>
      <div className={`${isEditing ? 'd-none' : ''}`}>
        <div className="d-flex justify-content-between my-2">
          <p className="weight-bold subtitle m-0">Dates</p>
          {/* <a
            className="weight-bold underline-0 text-block"
            href="#"
            onClick={() => setIsEditing(true)}
          >
            Modifier
          </a> */}
        </div>
        <div>
          <p>
            {moment(form.checkIn).format('dddd, Do MMMM  YYYY')} -
            {moment(form.checkOut).format('dddd, Do MMMM  YYYY')}
          </p>
          <p>
            Heure d'arrivée :{' '}
            {`${timeLimits.arrivalMin} h - ${timeLimits.arrivalMax} h`}
          </p>
        </div>
      </div>

      {isEditing && checkIn != undefined && (
        <>
          <div className="d-flex justify-content-between">
            <p>Dates</p>
            <a href="#" onClick={() => setIsEditing(false)}>
              Anuler
            </a>
          </div>
          <div className="d-flex">
            <div>
              <ArrivePopUp
                fields={form}
                setFields={setForm}
                unavailableDates={reservation.unavailableDates}
              />
            </div>
            <div>
              <DeparturePopUp
                fields={form}
                setFields={setForm}
                unavailableDates={reservation.unavailableDates}
              />
            </div>
          </div>
          <button
            className="btn btn-primary py-3 mt-4 w-50"
            onClick={() => setIsEditing(false)}
            disabled={!validReservation}
          >
            Sauvegarder
          </button>
        </>
      )}
    </>
  )
}

export default DatesForm
