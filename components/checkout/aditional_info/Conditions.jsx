/*  Dependencies  */
import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
/*  Components  */
import { Warning } from '@components/Icons'
/*  Constants  */
import {
  fixedAnswerByDefault,
  fixedAnswerForUser,
  textsCheckout
} from 'constants/cancellationConditions'
import { getCancellationConditions } from 'services/etablissementService'

const Conditions = () => {
  /*  Local states  */
  const [cancellationConditions, setCancellationConditions] = useState({})

  /*  Global states  */
  const store = useSelector((store) => store.checkoutReducer.reservation)

  useEffect(async () => {
    if (store) {
      if (store.establishment.id) {
        const response = await getCancellationConditions(store.establishment.id)
        setCancellationConditions(response)
      }
    }
  }, [store])

  return (
    <div className="checkout__conditions">
      <Warning width="30" />
      <h2 className="checkout__conditions-title">Conditions d'annulation</h2>

      <p>{textsCheckout.title}</p>

      {cancellationConditions?.length > 0 ? (
        <>
          <p>
            <strong>{`Jusqu’à ${cancellationConditions[0].days_quantity} ${
              cancellationConditions[0].days_quantity === 1 ? 'jour' : 'jours'
            } avant votre séjour`}</strong>
          </p>
          <p>{fixedAnswerForUser}</p>

          <p>
            <strong>{`Moins de ${cancellationConditions[0].days_quantity} ${
              cancellationConditions[0].days_quantity === 1 ? 'jour' : 'jours'
            } avant votre séjour ou non-présentation`}</strong>
          </p>
          {cancellationConditions[0].penality_percentage === 100 ? (
            <p>Non remboursable</p>
          ) : (
            <p>{`Votre réservation est remboursée à hauteur de ${cancellationConditions[0].penality_percentage}%`}</p>
          )}
        </>
      ) : (
        <>
          <p>{fixedAnswerByDefault.option1}</p>
          <p>{fixedAnswerByDefault.option2}</p>
        </>
      )}

      <p>
        <br />
        <strong>Covid-19</strong>
        <br />
        <br />
        {textsCheckout.textCovid}
      </p>
    </div>
  )
}

export default Conditions
