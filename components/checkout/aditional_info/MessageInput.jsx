import React from 'react'
import { consoleLog } from '/utils/logConsole'

const MessageInput = () => {
  return (
    <div>
      <textarea
        name=""
        id=""
        cols="50"
        rows="5"
        className="w-100 p-3 font-montserrat"
      ></textarea>
    </div>
  )
}

export default MessageInput
