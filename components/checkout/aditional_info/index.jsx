import React from 'react'
import { consoleLog } from '/utils/logConsole'
import Image from 'next/image'
//
import MessageInput from './MessageInput'

const index = () => {
  return (
    <div>
      <h2 className="h2-dashboard font-montserrat">
        Information additionnelles pour votre séjour
      </h2>
      <p className="paragraph mt-1">
        Envoyer un message à l'hôte. Dites à l'hôte pourquoi vous voyagez et
        quand vous arrivez.
      </p>
      <div className="d-flex align-items-center mt-2 mb-3">
        <Image
          src="http://dummyimage.com/499x315.jpg/ff4444/ffffff"
          width={50}
          height={50}
          className="rounded-circle"
        />
        <p className="m-0 ml-2 weight-bold">Guy Hawkins</p>
      </div>
      <MessageInput />
    </div>
  )
}

export default index
