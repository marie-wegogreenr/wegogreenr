import { useDispatch, useSelector } from 'react-redux'

function GiftCardBtns({
  handleAddGift,
  handleRemoveGift,
  setValidateGiftCard,
  validateGiftCard
}) {
  const handleClick = () => {
    setValidateGiftCard(true)
  }

  return (
    <div className="col-12 col-xl-4 pe-0 ps-0">
      {/* <button
        className="btn btn-outline-secondary button-gift--checkout"
        type="button"
        name="add_gift_field"
        onClick={handleAddGift}
      >
        +
      </button>
      <button
        className="btn btn-outline-secondary button-gift--checkout"
        type="button"
        name="remove_gift_field"
        onClick={handleRemoveGift}
      >
        -
      </button> */}
      <button
        className={`w-100 btn btn-outline-secondary button-gift--checkout ${
          false ? 'button-gift--checkout-load' : ''
        }`}
        type="button"
        name="remove_gift_field"
        onClick={handleClick}
        disabled={false}
      >
        {false ? '...En cours' : 'Appliquer'}
      </button>
    </div>
  )
}

export default GiftCardBtns
