import { useEffect, useState } from 'react'

function GiftCardInput({
  id,
  objectCode,
  setObjectCode,
  notification,
  setDataFetched
}) {
  const [notificationText, setNotificationText] = useState({})

  useEffect(() => {
    if (notification) {
      setNotificationText(
        notification && notification.code === 200
          ? data.success
          : notification?.data?.message === 'Gift Card was redeemed already'
          ? data.used
          : data.error
      )
    }
  }, [notification])

  const handleChange = (e) => {
    setDataFetched(false)
    setObjectCode({
      ...objectCode,
      [e.target.name]: e.target.value
    })
  }

  const data = {
    success: { text: `Le code est valide`, class: 'color-green-soft' },
    error: { text: `Le code n'est pas valide`, class: 'color-red' },
    used: { text: `Le code a déjà été échangé`, class: 'color-red' }
  }

  return (
    <div className="mb-4 rounded position-relative">
      <input
        type="text"
        className="form-control border text-center form-control--checkout"
        id={`gift-input-${id}`}
        name={`gift-input-${id}`}
        placeholder="Carte cadeau ou code promo"
        value={objectCode[id]}
        onChange={handleChange}
      />
      <span
        className={`${notificationText.class} form-control--checkout-notif`}
      >
        {notificationText.text}
      </span>
    </div>
  )
}

export default GiftCardInput
