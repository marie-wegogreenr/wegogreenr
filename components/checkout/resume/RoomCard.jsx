import { Label } from '@components/Hebergements/Single/Gallery/gallery.styles'
import icons from '@components/Maps/icons'
import Image from 'next/image'
import { getImageURL, getGreenScoreSourceIcon } from 'utils'

const RoomCard = ({ room }) => {
  const {
    public_name,
    room_image,
    establishment: { city_name, region_name, green_score },
    type_id
  } = room

  return (
    <div className="d-flex checkout__card-container">
      <Image
        width={120}
        height={120}
        alt={`${public_name}-image`}
        src={getImageURL(room_image[0]?.image?.url)}
        className="rounded-4 w-100 obj-fit-cover checkout__card-img"
      />
      <div className="ml- checkout__card-wrapper">
        <p className="m-0 p--alternative">
          {city_name}, {region_name}
        </p>
        <h3 className="font-montserrat h3--alternative">{public_name}</h3>
        <div className="mt-2 d-flex justify-content-between align-items-center">
          <Label className="font-montserrat fz-15 color-white">
            {icons[type_id]?.name}
          </Label>

          <figure className="card__image-greenscore-figure">
            <img
              alt="green-score-icon"
              src={getGreenScoreSourceIcon(green_score?.score)}
              height="11"
              className="checkout__card-greenscore"
            />{' '}
            <label className="card__image-greenscore-label">GreenScore</label>
          </figure>
        </div>
      </div>
    </div>
  )
}

export default RoomCard
