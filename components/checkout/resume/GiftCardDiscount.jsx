import { updateReservationPriceAction } from '@ducks/checkoutDuck'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

function GiftCardDiscount({ _reservationData, route }) {
  const giftCardObject = useSelector((store) => store.giftCardReducer)
  const dispatch = useDispatch()
  const [initialTotalPrice, setInitialTotalPrice] = useState(0)
  const [discountGift, setDiscountGift] = useState(0)

  useEffect(() => {
    if (giftCardObject?.giftcard_code && giftCardObject?.giftcard_value) {
      setDiscountGift(giftCardObject?.giftcard_value)
    }
  }, [giftCardObject, _reservationData])

  useEffect(() => {
    if (!initialTotalPrice && _reservationData?.total_price) {
      setInitialTotalPrice(_reservationData.total_price)
    }
    if (
      initialTotalPrice &&
      _reservationData?.total_price !== initialTotalPrice
    ) {
      setDiscountGift(initialTotalPrice - _reservationData?.total_price)
    }
  }, [_reservationData, initialTotalPrice])

  return (
    <>
      {!!discountGift && route === '/checkout' && (
        <div className="d-flex justify-content-between mt-2">
          <p
            className={`color-gray h3 font-fraunces weight-bold single-total-price`}
          >
            Remise
          </p>
          <p
            className={`color-gray h3 font-fraunces weight-bold single-total-price`}
          >
            -{_reservationData?.total_price && discountGift}&euro;
          </p>
        </div>
      )}
    </>
  )
}

export default GiftCardDiscount
