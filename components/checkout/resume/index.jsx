import React, { useEffect, useState } from 'react'
import RoomCard from './RoomCard'
import PricesCalc from '@components/Hebergements/Single/Form/PricesCalc'
//utils
import { matchUnavailableDates } from '../../Hebergements/Single/Form/matchSpecialDates'
//redux
import { useSelector, useDispatch } from 'react-redux'
import { updateReservationStatusAction } from '@ducks/checkoutDuck'
import GiftCardCheckout from './GiftCardCheckout'
import PricesCalcCheckout from './PricesCalcCheckout'

const index = () => {
  const store = useSelector((store) => store.checkoutReducer.reservation)
  const reservationStatus = useSelector(
    (store) => store.checkoutReducer.reservation.checkIn
  )
  const dispatch = useDispatch()

  const [disableBtn, setDisableBtn] = useState('')

  const { room, specialDates, checkIn, checkOut, adults, children } = store

  const [fields, setFields] = useState({})

  useEffect(() => {
    setFields({
      checkIn,
      checkOut,
      adults,
      children
    })
  }, [store, reservationStatus])

  useEffect(() => {
    setFields({
      checkIn,
      checkOut
    })
  }, [reservationStatus])

  useEffect(() => {
    if (disableBtn != '') {
      dispatch(updateReservationStatusAction(false))
    } else {
      dispatch(updateReservationStatusAction(true))
    }
  }, [disableBtn])

  if (store && room) {
    return (
      <div>
        <h2 className="h2--alternative font-montserrat">Résumé</h2>
        <div className="border-bottom-divisor mt-2 mb-4"></div>
        <RoomCard room={room} />
        <div>
          <p className="subtitle mt-4 weight-bold">Détails</p>
        </div>
        <PricesCalcCheckout room={room} fields={fields} color="black" />
        <GiftCardCheckout />
        {disableBtn}
      </div>
    )
  } else return <></>
}

export default index
