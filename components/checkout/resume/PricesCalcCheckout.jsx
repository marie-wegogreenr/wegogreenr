import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { updateReservationPriceAction } from '@ducks/checkoutDuck'
import GiftCardDiscount from './GiftCardDiscount'
import { useRouter } from 'next/router'

const PricesCalcCheckout = ({ room, color = 'white' }) => {
  const { basic_price } = room

  const router = useRouter()
  const _reservationData = useSelector(
    (store) => store.checkoutReducer.reservation
  )

  const {
    total_price,
    totalDays,
    totalPersons,
    priceExtraPeople,
    discountAmmount,
    discountMessage,
    priceWithAditionals
  } = _reservationData.additionals

  return (
    <div>
      {totalDays && (
        <div className="d-flex justify-content-between">
          <p className={`color-${color}`}>
            {!(total_price / totalDays)
              ? basic_price.toString().replace('.', ',')
              : (total_price / totalDays)
                  .toFixed(2)
                  .toString()
                  .replace('.', ',')}
            &euro; x {totalDays} nuits
          </p>
          <p className={`color-${color}`}>
            {total_price.toFixed(2).toString().replace('.', ',')}
            &euro;
          </p>
        </div>
      )}
      {totalPersons > room['extra_people_capacity'] && (
        <div className="d-flex justify-content-between">
          <p className={`color-${color}`}>Frais pour voyageur supplémentaire</p>
          <p className={`color-${color}`}>
            {priceExtraPeople}
            &euro;
          </p>
        </div>
      )}
      {/* {aditionalServicesPrice !== 0 && (
        <div className="d-flex justify-content-between">
          <p className={`color-${color}`}>
            Total pour les services additionnels
          </p>
          <p className={`color-${color}`}>
            {aditionalServicesPrice}
            &euro;
          </p>
        </div>
      )} */}
      {discountAmmount && discountAmmount > 0 && discountMessage && (
        <div className="d-flex justify-content-between">
          <p className={`color-${color}`}>{discountMessage}</p>
          <p className={`color-${color}`}>
            {' '}
            - {discountAmmount.toString().replace('.', ',')}&euro;
          </p>
        </div>
      )}
      <div
        className={`border-bottom-divisor my-2 color-${color} background-white`}
      ></div>
      <GiftCardDiscount
        _reservationData={_reservationData}
        route={router.asPath}
      />
      <div className="d-flex justify-content-between mt-4">
        <p
          className={`color-${color} h3 font-fraunces weight-bold single-total-price`}
        >
          Total
        </p>
        <p
          className={`color-${color} h3 font-fraunces weight-bold single-total-price`}
        >
          {discountAmmount
            ? (_reservationData.total_price - discountAmmount)
                .toFixed(2)
                .toString()
                .replace('.', ',')
            : _reservationData.total_price
                .toFixed(2)
                .toString()
                .replace('.', ',')}
          &euro;
        </p>
      </div>
      <div className="d-flex justify-content-between"></div>
    </div>
  )
}

export default PricesCalcCheckout
