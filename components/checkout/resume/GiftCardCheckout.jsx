import { updateReservationPriceAction } from '@ducks/checkoutDuck'
import { addGiftCardData } from '@ducks/giftCard/actions'
import { useToast } from 'hooks/useToast'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { validateGiftCardCode } from 'services/reservationService'
import GiftCardBtns from './GiftCardBtns'
import GiftCardInput from './GiftCardInput'

function GiftCardCheckout() {
  /*  Global states */
  const [reservationData, giftCardObject] = useSelector((store) => [
    store.checkoutReducer.reservation,
    store.giftCardReducer
  ])
  const dispatch = useDispatch()
  /*  Local states  */
  const [arrayGiftCards, setArrayGiftCards] = useState([])
  const [validateGiftCard, setValidateGiftCard] = useState(false)
  const [dataFetched, setDataFetched] = useState(false)
  const [objectCode, setObjectCode] = useState({})
  const [objectResponse, setObjectResponse] = useState({})
  const [initialTotalPrice, setInitialTotalPrice] = useState(0)
  const [notify] = useToast()
  /*  Effects  */

  useEffect(() => {
    if (giftCardObject?.giftcard_code) {
      setObjectResponse({ ...giftCardObject })
    }
  }, [giftCardObject])
  useEffect(() => {
    if (!initialTotalPrice && reservationData?.total_price) {
      setInitialTotalPrice(reservationData.total_price)
    }
  }, [reservationData])
  useEffect(() => {
    const giftObject = { id: arrayGiftCards.length }
    setArrayGiftCards([...arrayGiftCards, giftObject])
  }, [])

  useEffect(() => {
    const checkData = () => {
      let error
      const element = Object.values(objectCode)
      for (let i = 0; i < element.length; i++) {
        for (let j = i; j < element.length; j++) {
          if (i === j) continue
          if (element[i] === element[j]) {
            notify('Vous ne pouvez pas répéter les codes', 'error')
            error = true
            setValidateGiftCard(false)
            break
          }
        }
      }
      return error
    }
    const fetchData = () => {
      let objectWithResponses = {}
      const mapCodes = () => {
        Object.entries(objectCode).forEach(([key, code], index) => {
          const fetchCode = async () => {
            if (!code) return
            const body = { code }
            const response = await validateGiftCardCode(body)
            objectWithResponses = {
              ...objectWithResponses,
              [key]: response
            }
            if (index + 1 === Object.keys(objectCode).length) {
              setObjectResponse({ ...objectWithResponses })
            }
          }
          fetchCode()
        })
      }
      mapCodes()
      return objectWithResponses
    }

    if (validateGiftCard) {
      setObjectResponse({})
      const isError = checkData()
      if (isError) return
      fetchData()
      setDataFetched(true)
      setValidateGiftCard(false)
    }
  }, [validateGiftCard])

  useEffect(() => {
    if (
      dataFetched &&
      objectResponse &&
      Object.values(objectResponse).length &&
      initialTotalPrice
    ) {
      const discount_price = Object.values(objectResponse).reduce(
        (acc, curr) => {
          if (curr.code === 200) {
            const value = curr.data?.giftcard?.value
            return value + acc
          } else {
            return acc
          }
        },
        0
      )
      const final_price = initialTotalPrice - discount_price
      const element = Object.values(objectResponse)[0]
      if (element?.code === 200) {
        const giftObject = {
          giftcard_code: element?.data?.giftcard?.code,
          giftcard_value: element?.data?.giftcard?.value
        }

        if (
          giftCardObject?.giftcard_code === giftObject?.giftcard_code &&
          giftCardObject?.giftcard_value === giftObject?.giftcard_value
        ) {
          return notify('Ce code a déjà été appliqué', 'error')
        }

        if (final_price >= 0) {
          dispatch(addGiftCardData(giftObject))
          const _reservationData = {
            ...reservationData,
            total_price: final_price,
            giftcard_value: discount_price
          }
          dispatch(updateReservationPriceAction({ ..._reservationData }))
        } else {
          return notify(
            `Provisoirement, pour pouvoir utiliser votre carte cadeau, le montant de votre réservation doit être supérieure ou égale à la valeur de votre carte cadeau.`,
            'warning'
          )
        }
      }
    }
  }, [objectResponse])

  /**
   * @function : It adds one aditional element for gift card
   */
  const handleAddGift = () => {
    if (arrayGiftCards.length < 10) {
      const giftObject = { id: arrayGiftCards.length }
      setArrayGiftCards([...arrayGiftCards, giftObject])
    }
  }

  /**
   * @function : It removes the last element in gift cards
   */
  const handleRemoveGift = () => {
    if (arrayGiftCards.length > 1) {
      setObjectCode({
        ...objectCode,
        [`gift-input-${arrayGiftCards.length - 1}`]: undefined
      })
      setArrayGiftCards(
        arrayGiftCards.filter((item) => item.id !== arrayGiftCards.length - 1)
      )
    }
  }

  const fixedData = {
    btnText: `Ajouter carte cadeau`,
    btnTextAdded: ``,
    btnTextMore: `Ajouter une autre carte cadeau`,
    btnSubmit: `Appliquer`
  }
  return (
    <div>
      <div
        className={`border-bottom-divisor my-2 color-gray background-white`}
      ></div>

      <span className="color-black">{fixedData.btnTextAdded}</span>
      <div className="row align-items-start justify-content-between px-3 mt-2">
        <ul className="col-12 col-xl-7 w-100 m-0">
          {arrayGiftCards.map((item) => (
            <GiftCardInput
              key={item.id}
              id={item.id}
              objectCode={objectCode}
              setObjectCode={setObjectCode}
              notification={
                dataFetched ? objectResponse[`gift-input-${item.id}`] : null
              }
              setDataFetched={setDataFetched}
            />
          ))}
        </ul>
        <GiftCardBtns
          handleAddGift={handleAddGift}
          handleRemoveGift={handleRemoveGift}
          setValidateGiftCard={setValidateGiftCard}
          validateGiftCard={validateGiftCard}
        />
      </div>
      <div
        className={`border-bottom-divisor my-2 color-gray background-white`}
      ></div>
    </div>
  )
}

export default GiftCardCheckout
