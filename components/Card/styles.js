import styled from '@emotion/styled'
import { shadows, sizes } from 'styles/theme'

export const Image = styled.img`
  object-fit: cover;
  width: 100%;
  transition: all ease 300ms;
  height: 300px;
`

export const Container = styled.a`
  display: flex;
  flex-direction: column;
  background-color: var(--color-white);
  border-radius: ${sizes['text-xs']};
  overflow: hidden;
  position: relative;
  box-shadow: 0px 15px 10px -15px #80808078;
  margin: 0.5rem 0;
  height: 100%;
  max-width: 400px;
  padding: 0;
  margin-right: 0.85em;
  text-decoration: none;

  &:hover {
    box-shadow: 0px 15px 10px -12px #808080aa;
  }

  &:hover ${Image} {
    transform: scale(1.02);
  }

  @media (max-width: 640px) {
    margin: 0.5rem auto;
  }

  @media (max-width: 480px) {
    min-height: unset;
    /* max-height: 21.4rem !important; */
    width: 73%;
  }
  @media (max-width: 365px) {
    min-height: unset;
    /* max-height: 21.4rem !important; */
    width: 85%;
  }
`

export const Place = styled.h3`
  color: var(--color-gray);
  font-family: var(--font-montserrat);
  font-size: 12px;
  font-weight: 400;

  @media (min-width: 769px) and (max-width: 835px) {
    font-size: 10px;
    line-height: 15px;
  }
  @media screen and (max-width: 575px) {
    line-height: 1.3em;
  }
`

export const Title = styled.h2`
  font-size: ${sizes['text-xl']};
  font-family: var(--font-montserrat);
  font-weight: 700;
  color: var(--color-black);
  line-height: 1.5rem;

  @media (max-width: 912px) {
    height: auto;
  }

  @media (min-width: 769px) and (max-width: 835px) {
    height: auto;
    font-size: 0.9rem;
    line-height: 1.2rem;
  }
  @media screen and (max-width: 575px) {
    line-height: 1.15em;
  }
`

export const Content = styled.div`
  padding: ${sizes['text-sm']} ${sizes['text-lg']};
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-height: 13rem;
  @media (max-width: 912px) {
    height: 200px;
  }

  @media (min-width: 769px) and (max-width: 870px) {
    padding: 0.75rem;
  }

  @media (max-width: 768px) {
    padding: 1.4rem 1.125rem;
  }
  @media (max-width: 575px) {
    height: 14rem;
    padding: 0.87rem;
  }
`

export const IconContainer = styled.button`
  position: absolute;
  top: 0.7rem;
  right: 0.7rem;
  border: none;
  padding: 0;
  background-color: transparent;
  z-index: 5;
`

export const ImageContainer = styled.figure`
  margin-bottom: 0;
  cursor: pointer;
  overflow: hidden;

  img {
    @media (max-width: 480px) {
      max-height: 12.8rem;
    }
  }
`

export const Details = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  column-gap: 0.5rem;
  align-items: center;
  height: 100%;
  @media (max-width: 786px) {
    margin-top: 1rem;
  }

  @media (min-width: 769px) and (max-width: 835px) {
    grid-template-columns: repeat(2, 1fr);
  }
  @media screen and (max-width: 575px) {
    margin-top: 0.5rem;
  }
`

export const Pane = styled.div`
  ${({ theme }) => {
    if (!theme.center) return ''
    return `
      display: flex;
      flex-direction: column;
      align-items: center;`
  }}
`

export const PanePeople = styled.div`
  ${({ theme }) => {
    if (!theme.center) return ''
    return `
      display: flex;
      flex-direction: column;
      align-items: center;`
  }}

  @media (min-width: 769px) and (max-width: 835px) {
    position: absolute;
    right: 18px;
    bottom: 62px;
    flex-direction: row;
  }
`

export const ProgressBar = styled.progress`
  width: 100%;
`

export const Text = styled.p`
  font-size: ${sizes['text-xs']};
  color: var(--color-black);
  font-family: var(--font-montserrat);
  margin-bottom: 0;
  text-align: center;
  @media (max-width: 786px) {
    line-height: 1rem;
  }
`

export const TextPrice = styled.p`
  font-size: ${sizes['text-xs']};
  color: var(--color-black);
  font-family: var(--font-montserrat);
  margin-bottom: 0;
  text-align: center;
  @media (max-width: 786px) {
    line-height: 1rem;
  }
  @media (min-width: 769px) and (max-width: 835px) {
    text-align: end;
  }
`

export const TextPeople = styled.p`
  font-size: ${sizes['text-xs']};
  color: var(--color-black);
  font-family: var(--font-montserrat);
  margin-bottom: 0;
  text-align: center;
  @media (max-width: 786px) {
    line-height: 1rem;
  }
  @media (min-width: 769px) and (max-width: 835px) {
    padding-left: 0.25rem;
  }
`

export const PriceDisplay = styled.p`
  font-size: 15px;
  margin-bottom: 0;
  color: var(--color-black);
  text-align: right;
`
