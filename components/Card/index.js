import * as React from 'react'
import Image from 'next/image'
/* Components */
import { Heart, User } from 'components/Icons'
import noImage from '/public/images/not-image.svg'
/* Next */
import Link from 'next/link'
/* Styles */
import {
  Container,
  IconContainer,
  Place,
  Title,
  Image as Img,
  Content,
  ImageContainer,
  Details,
  Pane,
  Text,
  PriceDisplay,
  PanePeople,
  TextPeople,
  TextPrice
} from './styles'
import { routes } from 'constants/index'
import { getGreenScoreSourceIcon, getImageURL, cutText } from 'utils'
import { useEffect, useState } from 'react'
import axios from 'axios'
import Resizer from 'react-image-file-resizer'
import Loader from '@components/Loader'
import { adjustNumberWithComma } from 'helpers/utilsHelper'
import { eventListObjectTriggered } from 'helpers/dataLayer/lists'
import { useDispatch } from 'react-redux'
import { setUrl } from '@ducks/tempPage/actions'

function Card({
  basic_price,
  liked,
  people_capacity,
  public_name,
  slug,
  establishment,
  room_image = [],
  origin,
  external_images,
  duration = 1,
  from = '',
  ...room
}) {
  /*  Redux  */
  const dispatch = useDispatch()
  /*  Local states  */
  const [mainImageSource, setMainImageSource] = useState('')
  /*  Destructuring & validate */
  const { green_score = {}, city_name, region_name } = establishment || {}
  const [score, setScore] = useState(50)

  useEffect(() => {
    if (origin === 2 && green_score) {
      let _score = 45

      const greenScoreLevels = [
        { level: 1, score: 50 },
        { level: 2, score: 80 },
        { level: 3, score: 120 },
        { level: 4, score: 150 },
        { level: 5, score: 200 }
      ]

      if (green_score.level) {
        greenScoreLevels.map((item) => {
          if (item.level === green_score.level) {
            _score = item.score
          }
        })
      }

      setScore(_score)
    } else if (origin === 4) {
      if (room.greenScore) {
        setScore(room.greenScore.score)
      }
    } else {
      setScore(green_score ? green_score.score : null)
    }
  }, [])

  useEffect(() => {
    let mainImage = ''
    if (origin === 2 && typeof external_images === 'string') {
      const splittedImages = external_images.split(';')
      const splittedMainImage = splittedImages[1].split('"')
      mainImage = splittedMainImage[1]
      setMainImageSource(mainImage)
    } else if (origin === 2 && external_images.length > 0) {
      setMainImageSource(external_images[0])
    } else if (origin === 4) {
      if (Array.isArray(external_images)) setMainImageSource(external_images[0])
    } else {
      room_image.forEach((element) => {
        if (element.order === 1) {
          mainImage = element.image.url
        }
      })

      if (mainImage === undefined || mainImage === '' || mainImage === null) {
        mainImage = room_image[0]?.image?.url
      }

      // const mainImage = room_image[0]?.image?.url || null
      let mainImageSourceOriginal = getImageURL(mainImage)
      setMainImageSource(mainImageSourceOriginal)
    }
  }, [])

  const handleClick = () => {
    if (origin !== 4) {
      const body = {
        public_name,
        establishment,
        basic_price,
        from: 'card-list',
        ...room
      }
      eventListObjectTriggered.selectItem.callback(body)
    }

    if (typeof window !== undefined) {
      if (origin === 4) {
        dispatch(setUrl(`${window.location.origin}/establishment/${room.id}`))
      } else if (origin === 2) {
        return window.open(room.origin_url, '_blank').focus()
      } else {
        dispatch(setUrl(`${window.location.origin}/hebergements/${slug}`))
      }
      window.open(`${window.location.origin}/temp`, '_blank').focus()
    }
  }

  return (
    <Container
      href={`/hebergements/${slug}`}
      target="_blank"
      onClick={(e) => {
        e.preventDefault()
        handleClick()
      }}
    >
      <IconContainer>
        {/* <Heart
            strokeWidth="1px"
            fill={liked ? 'white' : 'transparent'}
            width="2rem"
            stroke="white"
          /> */}
      </IconContainer>
      <ImageContainer>
        <Image
          className="Card-image"
          src={mainImageSource || noImage}
          layout="responsive"
          width="400px"
          height="300px"
          alt="card image"
          priority={true}
        />
      </ImageContainer>
      <Content>
        <Pane>
          <Place className="">
            {origin === 4 ? room.city_name : city_name}
            {room.city_name || city_name ? ', ' : ''}
            {origin === 4 ? room.region_name : region_name}
          </Place>
          <Title className="">
            {cutText(origin === 4 ? room.name : public_name, 55)}
          </Title>
        </Pane>
        <Pane>
          <Details>
            <Pane>
              {score ? (
                <figure className="card__image-greenscore-figure">
                  <Image
                    width="100px"
                    height="10px"
                    style={{ objectFit: 'cover' }}
                    src={getGreenScoreSourceIcon(score)}
                    className="card__image-greenscore-img"
                  />
                  <label className="card__image-greenscore-label">
                    Green Score
                  </label>
                </figure>
              ) : (
                <p className="text-danger">Green Score not found.</p>
              )}
            </Pane>

            <PanePeople theme={{ center: true }}>
              {(people_capacity || room.peopleCapacity) && (
                <>
                  <User
                    width="2rem"
                    strokeWidth="1px"
                    color="#000"
                    stroke="#000"
                  />
                  <TextPeople style={{ fontSize: '12px' }}>
                    {people_capacity} pers
                  </TextPeople>
                </>
              )}
            </PanePeople>

            <Pane className="text-right">
              <TextPrice style={{ lineHeight: 1.2, fontSize: '12px' }}>
                A partir de
              </TextPrice>
              <PriceDisplay className="">
                <strong className="fz-18">
                  {adjustNumberWithComma(
                    origin === 4
                      ? room.external_information?.price
                      : basic_price
                  )}
                  €
                </strong>
                /nuit
              </PriceDisplay>
            </Pane>
          </Details>
        </Pane>
      </Content>
    </Container>
  )
}

export default Card
