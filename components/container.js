import Navigation from './navigation'
import Footer from './footer'
import Head from 'next/head'
import React from 'react'
import Cookies from 'universal-cookie'
import Router from 'next/router'
import Auth from './Auth'
import { connect } from 'react-redux'
import { evaluateEvent } from 'helpers/dataLayer'
import { paths } from 'constants/alowedNavPaths'
import {
  setPageViewSent,
  setPageViewHold
} from '@ducks/dataLayerEvents/actions'
import { eventListObjectTriggered } from 'helpers/dataLayer/lists'

class Container extends React.Component {
  constructor(props) {
    super(props)
    const cookies = new Cookies()
    this.state = {
      token_user: cookies.get('tk_user'),
      url: [],
      showFooter: false,
      showHeader: false,
      colorFooter: 'green',
      eventSent: false
    }
  }

  async componentDidMount() {
    let _this = this
    const cookies = new Cookies()
    let url = []
    let delay = 250

    if (typeof window !== 'undefined') {
      url = window.location.href.split('/')
    }

    if (_this.props.from === 'search') {
      delay = 80
    }

    setTimeout(() => {
      if (!this.state.eventSent) {
        evaluateEvent(this.props.user)
        if (this.props.user?.id) {
          eventListObjectTriggered.loggedUser.callback(this.props.user?.id)
        }
        this.setState({ ...this.state, eventSent: true })

        if (!url[3].includes('login')) {
          _this.props.setPageViewSent()
        }

        if (
          !url[3] ||
          (url[3] !== 'hebergements' && url[3] !== 'establishment')
        ) {
          _this.props.setPageViewHold()
        }
      }
    }, delay)

    if (url[3] === 'register' || url[3] === 'login') {
    } else if (url[3] === 'user' || url[3] === 'host' || url[3] == 'admin') {
      if (this.state.token_user === undefined) {
        Router.push('/login')
      }
      this.setState({
        ...this.state,
        colorFooter: 'gray',
        showFooter: true,
        showHeader: true,
        url: window.location.href.split('/')
      })
    } else if (paths.some((p) => p === url[3])) {
      this.setState({
        ...this.state,
        colorFooter: 'green',
        showFooter: true,
        showHeader: true,
        url: window.location.href.split('/')
      })
    } else if (url[3] === 'onboarding') {
      if (url[4] == 'finish-onboarding') {
        this.setState({
          ...this.state,
          colorFooter: 'green',
          showFooter: true,
          showHeader: true,
          url: window.location.href.split('/')
        })
      } else {
        this.setState({
          ...this.state,
          showFooter: false,
          showHeader: true,
          colorFooter: 'green',
          url: window.location.href.split('/')
        })
      }
      if (this.state.token_user === undefined) {
        Router.push('/login')
      } else {
        let _this = this
      }
    } else if (url[3] === '') {
      this.setState({
        ...this.state,
        colorFooter: 'green',
        showFooter: true,
        showHeader: true,
        url: window.location.href.split('/')
      })
    }
  }

  render() {
    return (
      <div className="main__layout">
        <Head>
          <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
            crossOrigin="anonymous"
          />
        </Head>
        <Auth actualUrl={this.state.url[3]} />
        {this.state.showHeader &&
          this.state.url[3] !== 'register' &&
          this.state.url[3] !== 'login' &&
          this.state.url[3] !== 'forget_password' &&
          this.state.url[3] !== 'forget_password_form' &&
          this.state.url[3] !== 'email' &&
          this.state.url[3] !== 'onboarding' && (
            <Navigation from={this.props.from} />
          )}
        <>{this.props.children}</>
        {this.state.showFooter && <Footer type={this.state.colorFooter} />}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { user: state.user.user }
}

const mapDispatchToProps = {
  setPageViewSent,
  setPageViewHold
}

export default connect(mapStateToProps, mapDispatchToProps)(Container)
