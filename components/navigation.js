import React from 'react'
import Router from 'next/router'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faCommentDots,
  faBell,
  faExchangeAlt
} from '@fortawesome/free-solid-svg-icons'
import Cookies from 'universal-cookie'
import Navbar from './NavigationComponents/Navbar'
import { deleteUser } from '@ducks/userDuck'
import { connect } from 'react-redux'
import { deleteEstablishment } from '@ducks/host/establishment/actions'
import { cleanReservationAction } from '@ducks/checkoutDuck'

class Navigation extends React.Component {
  constructor(props) {
    super(props)
    const cookies = new Cookies()
    this.state = {
      token_user: cookies.get('tk_user'),
      url: '',
      hasRolHost: false,
      showNavbar: false,
      user_data: ''
    }
    this.handleLogout = this.handleLogout.bind(this)

    library.add(faCommentDots, faBell, faExchangeAlt)
  }

  async componentDidMount() {
    const cookies = new Cookies()
    this.setState(
      { ...this.state, url: window.location.href.split('/') },
      () => {
        this.setState({ ...this.state, showNavbar: true })
      }
    )
    if (this.state.token_user !== undefined) {
      const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/user', {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.state.token_user
        },
        method: 'GET'
      })
        .then(function (response) {
          if (response.status !== 200) {
            if (cookies.get('tk_user')) {
              cookies.remove('tk_user')
            }
            Router.push('/login')
          }
          return response.json()
        })
        .then((user) => {
          let acum = 0
          for (let i = 0; i < user.roles.length; i++) {
            if (user.roles[i].name !== 'traveler') {
              acum++
            }
          }
          if (acum > 0) {
            this.setState({ ...this.state, hasRolHost: true })
          }
        })
        .catch((error) => {
          if (cookies.get('tk_user')) {
            cookies.remove('tk_user')
          }
          Router.push('/login')
        })
    }
  }

  async handleLogout() {
    let _this = this
    const cookies = new Cookies()
    const res = await fetch(process.env.NEXT_PUBLIC_API_URL + 'auth/logout', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.state.token_user
      },
      method: 'GET'
    })
      .then(function (response) {
        if (response.status !== 200) {
          cookies.remove('tk_user', { path: '/', expires: new Date() })
          setTimeout(() => {
            Router.push('/login')
          }, 1000)
        } else {
          cookies.remove('tk_user', { path: '/', expires: new Date() })
          setTimeout(() => {
            Router.push('/login')
          }, 1000)
        }
      })
      .catch((e) => {
        document.cookie = 'tk_user=;path=/;expires=' + new Date()
        Router.push('/login')
      })
      .finally(() => {
        _this.props.deleteUser()
        _this.props.deleteEstablishment()
        _this.props.cleanReservationAction()
      })
  }

  render() {
    return (
      <div>
        {this.state.showNavbar && (
          <Navbar
            type={this.state.url[3]}
            hasRol={this.state.hasRolHost}
            onHandleLogout={this.handleLogout}
            from={this.props.from}
          />
        )}
      </div>
    )
  }
}

const mapDispatchToProps = {
  deleteUser,
  deleteEstablishment,
  cleanReservationAction
}

export default connect(null, mapDispatchToProps)(Navigation)
