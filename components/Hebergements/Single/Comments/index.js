import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import { Star } from 'components/Icons'

function Comments() {
  // JSX.Elements
  const rows = Array(2)
    .fill('')
    .map((_, index) => {
      return (
        <div key={index} className="col-xl-6 mb-3">
          <div className="d-flex align-items-center">
            <img
              width="50px"
              height="50px"
              className="rounded-circle"
              src="https://picsum.photos/300"
              alt=""
            />
            <div className="ml-4">
              <h5 className="card-title color-black fz-18 lh-22 font-montserrat mb-0">
                Guy Hawkins
              </h5>
              <p className="mb-0 font-montserrat fz-14">Décembre 17 - 2020</p>
            </div>
          </div>

          <p className="mt-4 font-montserrat fz-15">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla quam
            velit, vulputate eu pharetra nec, mattis ac neque.
          </p>
        </div>
      )
    })

  return (
    <div style={{ backgroundColor: '#eeedec', padding: '5rem auto' }}>
      <div className="container-xxl py-5rem px-4">
        <h2 className="d-flex align-items-center color-primary size-h2">
          <Star />
          <span className="ml-2">5/5 (24 Commentaires)</span>
        </h2>

        <div className="border-bottom my-3 color-black" />

        <div className="row w-100 mb-4">{rows}</div>

        <button
          className="btn btn-outline-secondary"
          style={{ padding: '10px 20px' }}
        >
          Afficher tous les commentaires
        </button>
      </div>
    </div>
  )
}

export default Comments
