export { default as Gallery } from './Gallery'
export { default as Navbar } from './Navbar'
export { default as Form } from './Form'
export { default as Sections } from './Sections'
