export const TicTacTrip = () => (
  <div className="tictactrip__container">
    <a
      href="https://www.tictactrip.eu"
      id="ttt-widget"
      data-iframe-partnerid="WB-WEGOGREENR"
    >
      .{' '}
    </a>
    <script
      async
      type="text/javascript"
      src="https://widgetbooking.tictactrip.eu/index.js"
    ></script>
  </div>
)
