import { SingleUser } from '@components/Icons'
import React, { useState } from 'react'
import { consoleLog } from '/utils/logConsole'

const UsersQuantity = ({ state, setUpperState, changeFields, room }) => {
  const [isModifying, setIsModifying] = useState(false)

  const { people_capacity } = room

  const totalInputsSum = Number(state.adults) + Number(state.children)
  let overload = totalInputsSum > people_capacity

  const max_people = Array(people_capacity).fill(0)
  //consoleLog(max_people)

  const handleIncrease = (name) => {
    if (state[name] > people_capacity) return
    setUpperState({ ...state, [name]: state[name] + 1 })
  }

  const handleDecrease = (name) => {
    if (state[name] - 1 > people_capacity || state[name] == 0) return
    setUpperState({ ...state, [name]: state[name] - 1 })
  }

  return (
    <div className="">
      <div onClick={() => setIsModifying(true)} className="bg-white d-flex">
        <span className="d-flex align-items-center">
          <SingleUser width="13" height="15" color="black" />
          <p className="fz-15 m-0 ml-2">Adultes : {state.adults}</p>
        </span>
        <span className="d-flex align-items-center ml-3">
          <SingleUser width="13" height="15" color="black" />
          <p className="fz-15 m-0 ml-2">Enfants : {state.children}</p>
        </span>
      </div>

      {isModifying && (
        <div>
          <div className="d-flex justify-content-between align-items-center mt-4 pb-3 border-bottom">
            <div>
              <span className="d-flex align-items-center">
                <SingleUser width="13" height="15" color="black" />
                <div>
                  <p className="p-0 my-0 ms-3">Adultes</p>
                </div>
              </span>
            </div>
            <div>
              <button
                className="btn btn-circle border"
                onClick={() => handleDecrease('adults')}
              >
                {'-'}
              </button>
              <span className="color-black mx-3">{state.adults}</span>
              <button
                className="btn btn-circle border"
                onClick={() => handleIncrease('adults')}
              >
                {'+'}
              </button>
            </div>
          </div>

          <div className="d-flex justify-content-between align-items-center mt-4 pb-3 border-bottom">
            <div>
              <span className="d-flex align-items-center">
                <SingleUser width="13" height="15" color="black" />
                <p className="p-0 my-0 ms-3">Enfants</p>
              </span>
            </div>
            <div>
              <button
                className="btn btn-circle border"
                onClick={() => handleDecrease('children')}
              >
                {'-'}
              </button>
              <span className="color-black mx-3">{state.children}</span>
              <button
                className="btn btn-circle border"
                onClick={() => handleIncrease('children')}
              >
                {'+'}
              </button>
            </div>
          </div>
          {overload && (
            <span className="text-black">
              <p className="tect-black mt-2 text-center">
                {overload && `Nombre maximum de voyageurs : ${people_capacity}`}
              </p>
            </span>
          )}
          <button
            className="btn btn-secondary w-100 my-3 py-2 weight-bold"
            onClick={() => setIsModifying(false)}
            disabled={overload}
          >
            Sauvegarder
          </button>
        </div>
      )}
    </div>
  )
}

export default UsersQuantity
