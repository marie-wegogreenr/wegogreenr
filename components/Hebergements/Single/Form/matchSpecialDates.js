import {
  subDays,
  isWithinInterval,
  eachDayOfInterval,
  areIntervalsOverlapping,
  isAfter
} from 'date-fns'
import { dateFromDb, formatSafari } from 'helpers/timeHelper'
import moment from 'moment'

export function matchUnavailableDates(
  initDate,
  endDate,
  unavailableDates = []
) {
  if (typeof unavailableDates !== 'object') return
  /*  Set initial range  */
  const arrivalMoment = moment(initDate).format('YYYY-MM-DD 00:00:00')
  const departureMoment = moment(endDate).format('YYYY-MM-DD 00:00:00')
  const arrival = new Date(arrivalMoment).getTime()
  const departure = new Date(departureMoment).getTime()

  let isUnavailable = false

  /*  Get dates on range  */
  let datesOnRange = []
  try {
    datesOnRange = unavailableDates
      .filter((item) => {
        return isWithinInterval(new Date(item.date), {
          start: arrival,
          end: departure
        })
      })
      .map((item) => {
        if (item.classification === null) {
          item.classification = 2
        }
        return item
      })
      .reduce(
        (daysAvailable, item) => {
          const days = {
            firstDay: 1,
            middleDay: 2,
            lastDay: 3
          }
          for (const [key, value] of Object.entries(days)) {
            if (item.classification === value) {
              daysAvailable = {
                ...daysAvailable,
                [key]: {
                  type: true,
                  dates: [...daysAvailable[key]['dates'], item.date]
                }
              }
            }
          }

          return daysAvailable
        },
        {
          firstDay: {
            type: false,
            dates: []
          },
          middleDay: {
            type: false,
            dates: []
          },
          lastDay: {
            type: false,
            dates: []
          }
        }
      )

    const { firstDay, middleDay, lastDay } = datesOnRange

    if (middleDay.type) {
      isUnavailable = true
    } else if (firstDay.type && lastDay.type) {
      if (firstDay.dates.length > 1 || lastDay.dates.length > 1) {
        isUnavailable = true
      } else if (
        isAfter(new Date(lastDay.dates[0]), new Date(firstDay.dates[0]))
      ) {
        isUnavailable = true
      }
    }
  } catch (error) {
    console.error(error)
  }

  return isUnavailable
}

export function getSpecialDatesPrices(
  initDate,
  endDate,
  specialDates,
  basic_price,
  _weekend_price
) {
  const arrival = formatSafari(initDate)

  const departure = subDays(formatSafari(endDate), 1)
  let weekends = 0,
    weekdays = 0,
    weekends_price = 0,
    weekdays_price = 0

  const filteredSpecialDates = specialDates.filter((sd) =>
    areIntervalsOverlapping(
      { start: formatSafari(sd.start_date), end: formatSafari(sd.end_date) },
      {
        start: arrival,
        end: departure
      },
      { inclusive: true }
    )
  )

  const Range = eachDayOfInterval({
    start: arrival,
    end: departure
  })
  const datesWithSpecialPrice = Range.map((day) => {
    const conditionsPerDay = filteredSpecialDates.filter((fsd) =>
      isWithinInterval(formatSafari(day), {
        start: formatSafari(fsd.start_date),
        end: formatSafari(fsd.end_date)
      })
    )
    if (conditionsPerDay.length > 1) {
      const biggestDate = conditionsPerDay.reduce((first, second) =>
        first.created_at > second.created_at ? first : second
      )
      return { ...biggestDate, day }
    } else if (conditionsPerDay.length === 1) {
      return { ...conditionsPerDay[0], day }
    }
    return { day, price: basic_price, weekend_price: _weekend_price }
  })

  datesWithSpecialPrice.forEach((priceDay) => {
    const { price, weekend_price } = getPrices(
      priceDay?.room_special_availability_price,
      _weekend_price,
      basic_price
    )
    if (priceDay.day.getDay() === 5 || priceDay.day.getDay() === 6) {
      weekends++
      weekends_price += weekend_price
    } else {
      weekdays++
      weekdays_price += price
    }
  })

  return [weekdays, weekends, weekdays_price, weekends_price]
}

export function getMinMaxNights(
  initDate,
  endDate,
  minBase,
  maxBase,
  specialDates
) {
  if (initDate === '' || endDate === '' || initDate > endDate) return [0, 0]
  const mappedSpecialDates = specialDates
    .map((sd) => ({
      date: sd.date,
      start_date: formatSafari(sd.start_date),
      end_date: formatSafari(sd.end_date),
      minNights: sd.room_special_availability_price?.min_nigth,
      maxNights: sd.room_special_availability_price?.max_nigth
    }))
    .filter((item) => item.minNights !== null && item.maxNights !== null)

  const minAvailableDays = []
  const maxAvailableDays = []

  mappedSpecialDates.forEach((sd) => {
    const isDatesInSpecialRange = areIntervalsOverlapping(
      { start: sd.start_date, end: sd.end_date },
      {
        start: formatSafari(initDate),
        end: formatSafari(endDate)
      },
      { inclusive: true }
    )

    if (isDatesInSpecialRange) {
      minAvailableDays.push(sd.minNights)
      maxAvailableDays.push(sd.maxNights)
    }
  })

  const minInSpecialDates = Math.min.apply(null, minAvailableDays)
  const maxInSpecialDates = Math.max.apply(null, maxAvailableDays)

  const MinToReturn =
    minInSpecialDates === Infinity || minInSpecialDates === 0
      ? minBase
      : minInSpecialDates
  const MaxToReturn =
    maxInSpecialDates === -Infinity || maxInSpecialDates === 0
      ? maxBase
      : maxInSpecialDates

  return [MinToReturn, MaxToReturn]
}

const getPrices = (
  priceArray = {},
  weekendStandardPrice,
  weekStandardPrice
) => {
  if (priceArray) {
    if (Object.keys(priceArray).length) {
      const weekSpeacialPrice = priceArray.price
      const weekendSpeacialPrice =
        priceArray.weekend_price !== 0 ? priceArray.weekend_price : null
      return {
        price: weekSpeacialPrice || weekStandardPrice,
        weekend_price:
          weekendSpeacialPrice ||
          weekSpeacialPrice ||
          weekendStandardPrice ||
          weekStandardPrice
      }
    }
  }
  return {
    price: weekStandardPrice,
    weekend_price: weekendStandardPrice || weekStandardPrice
  }
}
