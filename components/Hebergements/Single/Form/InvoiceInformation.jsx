import React, { useEffect } from 'react'
import { useRouter } from 'next/router'
import { useDispatch } from 'react-redux'
import { updateReservationPriceAction } from '@ducks/checkoutDuck'

function InvoiceInformation({ children, _reservationData }) {
  const router = useRouter()
  const dispatch = useDispatch()

  useEffect(() => {
    setTimeout(() => {
      if (_reservationData?.giftcard_value) {
        const _resData = {
          ..._reservationData,
          giftcard_value: 0
        }
        dispatch(updateReservationPriceAction({ ..._resData }))
      }
    }, 300)
  }, [])

  return (
    <>
      {React.Children.toArray(children).map((child) =>
        React.cloneElement(child, { _reservationData, route: router.asPath })
      )}
    </>
  )
}

export default InvoiceInformation
