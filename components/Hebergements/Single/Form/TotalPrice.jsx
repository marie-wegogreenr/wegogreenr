import React from 'react'

function TotalPrice(props) {
  const {
    _reservationData,
    route,
    color,
    priceWithAditionals,
    discountAmmount
  } = props
  return (
    <div className="d-flex justify-content-between mt-3">
      <p
        className={`color-${color} h3 font-fraunces weight-bold single-total-price`}
      >
        Total
      </p>
      <p
        className={`color-${color} h3 font-fraunces weight-bold single-total-price`}
      >
        {route === '/checkout' && _reservationData?.total_price
          ? _reservationData?.total_price
          : discountAmmount
          ? (priceWithAditionals - discountAmmount)
              .toFixed(2)
              .toString()
              .replace('.', ',')
          : priceWithAditionals.toFixed(2).toString().replace('.', ',')}
        &euro;
      </p>
    </div>
  )
}

export default TotalPrice
