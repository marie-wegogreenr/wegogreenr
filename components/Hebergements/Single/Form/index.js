import * as React from 'react'
import 'react-day-picker/lib/style.css'
import UsersQuantity from './UsersQuantity'
import PricesCalc from './PricesCalc'
import DeparturePopUp from './DeparturePopUp'
import ArrivePopUp from './ArrivePopUp'

//redux
import { useDispatch, useSelector } from 'react-redux'
import { postReservationAction } from '@ducks/checkoutDuck'
//utils
import { useRouter } from 'next/router'
import { matchUnavailableDates } from './matchSpecialDates'
import Cookies from 'universal-cookie'
import { hasTimeToShow } from 'helpers/timeHelper'
import { useToast } from 'hooks/useToast'
import { eventListObjectTriggered } from 'helpers/dataLayer/lists'
import moment from 'moment'

const initialFields = {
  checkIn: '',
  checkOut: '',
  breakfast: '',
  adults: 0,
  children: 0
}

export function Form({
  establishment,
  room,
  unavailableDates,
  specialDates,
  googleUnavailable
}) {
  const [notify] = useToast()

  const [{ initial_date, final_date, travellers_quantity }, reservationData] =
    useSelector((store) => [
      store.searchedRooms.data.searchParams,
      store.checkoutReducer.reservation
    ])

  const { additionals, total_price } = reservationData
  const router = useRouter()
  const dispatch = useDispatch()

  const initialState = {
    checkIn: hasTimeToShow(initial_date),
    checkOut: hasTimeToShow(final_date),
    breakfast: '',
    adults: travellers_quantity.adults,
    children: travellers_quantity.children
  }

  const [fields, setFields] = React.useState({ ...initialState })

  const [disableBtn, setDisableBtn] = React.useState('')

  React.useEffect(() => {
    setFields({ ...initialState })
  }, [initial_date, final_date, travellers_quantity])

  React.useEffect(() => {
    const { checkIn, checkOut, breakfast, adults, children } = reservationData
    const isDataFilled = checkIn && checkOut && adults
    if (reservationData && isDataFilled) {
      setFields({
        ...initialState,
        checkIn: new Date(moment(checkIn).format('YYYY/MM/DD 00:00:00')),
        checkOut: new Date(moment(checkOut).format('YYYY/MM/DD 00:00:00')),
        breakfast,
        adults,
        children
      })
    }
  }, [reservationData])

  const changeFields = (e) => {
    if (fields[e.target.name] > e.target.max) {
      setFields({ ...fields, [e.target.name]: e.target.max })
    } else {
      setFields({ ...fields, [e.target.name]: e.target.value })
    }
  }

  const totalUsers = Number(fields.adults) + Number(fields.children)
  let overload = totalUsers > room.people_capacity
  let validRes = false
  let errorMsg = ''
  let disabled = false

  if (fields.checkIn && fields.checkOut) {
    const isUnavailable = matchUnavailableDates(
      fields.checkIn,
      fields.checkOut,
      unavailableDates
    )
    const isUnavailableGoogle = matchUnavailableDates(
      fields.checkIn,
      fields.checkOut,
      googleUnavailable
    )

    if (isUnavailable || isUnavailableGoogle) {
      validRes = false
      errorMsg = 'Dates non disponibles'
    } else validRes = true
  }

  const validateForm = () => {
    let validated = false
    const cookie = new Cookies()
    const token = cookie.get('tk_user')

    if (fields.checkIn >= fields.checkOut) {
      return notify(
        `La date de départ doit être postérieure à la date d'arrivée`,
        'error'
      )
    }
    if (fields.checkIn && fields.checkOut && (fields.adults || fields.children))
      validated = true

    if (validated) {
      dispatch(
        postReservationAction({
          ...fields,
          additionals,
          total_price,
          room,
          establishment,
          unavailableDates,
          specialDates
        })
      )
    }

    if (validated && !token) {
      cookie.set('slug_room', room)
      document.cookie = `slug_room=${room.slug}`
      router.push('/register')
    }
    validated && token ? router.push('/checkout') : (validated = false)
  }

  if (fields.adults == 0 || overload || !validRes || disableBtn) {
    disabled = true
  }
  return (
    <div
      className="card sticky-top shdow-1 br-10 "
      style={{ top: '110px', zIndex: 0 }}
    >
      <div className="p-0 p-sm-2 bg-primary br-10">
        <div className="card-body bg-primary text-white p-4 br-10">
          <h3
            className="card-title h2 color-white"
            style={{ fontFamily: 'Montserrat', fontWeight: 400 }}
          >
            {room.basic_price?.toString().replace('.', ',')}&euro;/
            <small className="h3">Nuit</small>
          </h3>
          <div className="border-bottom color-white background-white mt-4" />
          <form className="row g-3 mt-3" onSubmit={(e) => e.preventDefault()}>
            <div className="calc__dates d-flex">
              <div className="w-50">
                <ArrivePopUp
                  fields={fields}
                  setFields={setFields}
                  unavailableDates={unavailableDates}
                  googleUnavailable={googleUnavailable}
                />
              </div>
              <div className="w-50">
                <DeparturePopUp
                  fields={fields}
                  setFields={setFields}
                  unavailableDates={unavailableDates}
                  googleUnavailable={googleUnavailable}
                />
              </div>
            </div>
            <div className="col-12">
              <div className="bg-white br-8 calc__dates-users">
                <label
                  htmlFor="travelers"
                  className="form-label paragraph weight-bold m-0"
                >
                  Voyageurs
                </label>
                <UsersQuantity
                  state={fields}
                  setUpperState={setFields}
                  changeFields={changeFields}
                  room={room}
                />
              </div>
            </div>
            <div className="col-12">
              <PricesCalc
                establishment={establishment}
                room={room}
                fields={fields}
                specialDates={specialDates}
                unavailableDates={unavailableDates}
                setDisableBtn={setDisableBtn}
              />
            </div>
            <div className="col-12">
              <p
                className="color-red"
                style={{ textShadow: '1px 1px 1px #404040' }}
              >
                {errorMsg}
              </p>
              <p
                className="color-red "
                style={{ textShadow: '1px 1px 1px #404040' }}
              >
                {disableBtn}
              </p>
              <button
                className="btn btn-secondary btn-block btn-calc btn-calc--mobile py-3 text-block weight-bold"
                onClick={() => {
                  eventListObjectTriggered.beginBooking.callback()
                  validateForm()
                }}
                disabled={disabled}
              >
                Réserver
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default Form
