import React, { useState, useEffect } from 'react'
import DayPicker from 'react-day-picker'
import { MONTHS, WEEKDAYS_SHORT } from 'constants/calendarLocales'
import moment from 'moment'

//HOOKS
import { useClickOutside } from 'hooks'
import { getDuplicateItem } from 'helpers/calendarHelper'

Date.prototype.removeDays = function (days) {
  const date = new Date(this.valueOf())
  date.setDate(date.getDate() - days)
  return date
}

const ArrivePopUp = ({
  fields,
  setFields,
  unavailableDates,
  googleUnavailable = [],
  from
}) => {
  const [isSelecting, setIsSelecting] = useState(false)
  const [isAllDisabled, setIsAllDisabled] = useState(false)

  useEffect(() => {
    if (from === 'preview') {
      setIsAllDisabled(true)
    }
  }, [])

  const domNode = useClickOutside(() => setIsSelecting(false))

  const reservationDays = unavailableDates.filter((ud) => ud.type !== 1)
  const highlighted = {
    center: reservationDays
      .filter((rd) => rd.classification === 2)
      .map((rd) => new Date(moment(rd.date).format('YYYY/MM/DD 00:00:00'))),
    start: reservationDays
      .filter((rd) => rd.classification === 1)
      .map((rd) => new Date(moment(rd.date).format('YYYY/MM/DD 00:00:00'))),
    end: reservationDays
      .filter((rd) => rd.classification === 3)
      .map((rd) => new Date(moment(rd.date).format('YYYY/MM/DD 00:00:00'))),
    unavailableStart: unavailableDates
      .filter((ud) => ud.classification === 1)
      .map((ud) => new Date(moment(ud.date).format('YYYY/MM/DD 00:00:00'))),
    unavailableCenter: unavailableDates
      .filter((ud) => ud.classification === 2 || ud.classification === null)
      .map((ud) => new Date(moment(ud.date).format('YYYY/MM/DD 00:00:00'))),
    unavailableEnd: unavailableDates
      .filter((ud) => ud.classification === 3)
      .map((ud) => new Date(moment(ud.date).format('YYYY/MM/DD 00:00:00')))
  }

  const handleDateArriveChange = (selectedDay, modifiers, dayPicker) => {
    if (modifiers.disabled == true || modifiers.today == true) return

    const copyFields = Object.assign({}, fields)
    copyFields.checkIn = selectedDay
    setFields(copyFields)
    setIsSelecting(false)
  }

  const dup = getDuplicateItem(unavailableDates).map(
    (item) => new Date(moment(item.date).format('YYYY/MM/DD 00:00:00'))
  )

  const disabledDays = [
    ...dup,
    {
      before: new Date(),
      after: new Date(0, 0, 0)
    }
  ]
  unavailableDates
    .filter((ud) => ud.classification !== 1 && ud.classification !== 3)
    .map((date) => {
      disabledDays.push(
        new Date(moment(date.date).format('YYYY/MM/DD 00:00:00'))
      )
    })

  if (googleUnavailable && typeof googleUnavailable == 'object') {
    googleUnavailable.map((date) =>
      disabledDays.push(new Date(date.date + ' 00:00'))
    )
  }

  return (
    <>
      <div
        onClick={() => setIsSelecting(true)}
        className="calc__dates-input-arrive calc__dates-input"
      >
        <label
          htmlFor="checkIn "
          className="form-label paragraph weight-bold m-0"
        >
          Arrivée
        </label>
        <p className="fz-15 m-0">
          {fields.checkIn ? moment(fields.checkIn).format('L') : 'Quand ?'}
        </p>
      </div>

      <div ref={domNode}>
        {isSelecting && (
          <div className="position-relative w-100">
            {isAllDisabled ? (
              <>
                <div
                  className="position-absolute calc__dates-close color-black "
                  onClick={() => setIsSelecting(false)}
                >
                  X
                </div>
                <DayPicker className="br-10 shdow-1" />
              </>
            ) : (
              <>
                <div
                  className="position-absolute calc__dates-close color-black "
                  onClick={() => setIsSelecting(false)}
                >
                  X
                </div>
                <DayPicker
                  modifiers={highlighted}
                  onDayClick={handleDateArriveChange}
                  value={fields.checkIn}
                  disabledDays={disabledDays}
                  months={MONTHS}
                  weekdaysShort={WEEKDAYS_SHORT}
                  firstDayOfWeek={1}
                  className="br-10 shdow-1"
                />
              </>
            )}
          </div>
        )}
      </div>
    </>
  )
}

export default ArrivePopUp
