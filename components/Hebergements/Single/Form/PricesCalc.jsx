import React, { useEffect, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
import { getDaysAndWeekends } from 'utils/index'
import getSpecialDates, {
  getMinMaxNights,
  getSpecialDatesPrices
} from './matchSpecialDates'
//REDUX
import { useDispatch, useSelector } from 'react-redux'
import { updateReservationPriceAction } from '@ducks/checkoutDuck'
import { priceWithExtraPeople } from 'helpers/priceCalculatorHelper'
import { useRouter } from 'next/router'
import GiftCardDiscount from '@components/checkout/resume/GiftCardDiscount'
import InvoiceInformation from './InvoiceInformation'
import TotalPrice from './TotalPrice'
import useCalcMinMaxNight from 'hooks/useCalcMinMaxNight'

const PricesCalc = ({
  room,
  fields,
  specialDates = [],
  setDisableBtn,
  color = 'white'
}) => {
  const {
    basic_price,
    weekend_price,
    month_discount,
    seven_days_discount,
    min_nigth,
    max_nigth
  } = room

  const _reservationData = useSelector(
    (store) => store.checkoutReducer.reservation
  )
  const dispatch = useDispatch()
  let total_price = 0
  let messageErr = 'Les nuits maximum sont'
  let discountMessage
  let discountAmmount = 0
  const totalPersons = fields.adults + fields.children
  let wknd_price = weekend_price ? weekend_price : basic_price

  const [weekdays, weekends, totalDays] = getDaysAndWeekends(
    fields.checkIn,
    fields.checkOut
  )
  const [minNights, maxNights] = useCalcMinMaxNight({
    fields,
    specialDates,
    min_nigth,
    max_nigth,
    totalDays,
    setDisableBtn
  })

  if (fields.checkIn && fields.checkOut && fields.checkIn < fields.checkOut) {
    if (specialDates.length > 0) {
      const [
        specialWeekdays, // special days
        specialWeekends, // special findes
        specialWeekDaysPrice,
        specialWeekendsPrice
      ] = getSpecialDatesPrices(
        fields.checkIn,
        fields.checkOut,
        specialDates,
        basic_price,
        weekend_price
      )

      const normalDays =
        (weekdays - specialWeekdays) * basic_price +
        (weekends - specialWeekends) * wknd_price

      const specialDays = specialWeekDaysPrice + specialWeekendsPrice
      total_price = normalDays + specialDays
    } else {
      total_price = basic_price * weekdays + wknd_price * weekends
    }

    if (totalDays >= 28 && month_discount > 0) {
      discountAmmount = (total_price * (month_discount / 100)).toFixed(2)
      total_price -= (total_price * month_discount) / 100
      discountMessage = 'Réduction +28 jours'
    } else if (totalDays >= 7 && seven_days_discount > 0) {
      discountAmmount = (total_price * (seven_days_discount / 100)).toFixed(2)
      discountMessage = 'Réduction +7 jours'
    } else {
      discountMessage = false
      discountAmmount = false
    }
  }

  const priceExtraPeople = priceWithExtraPeople(
    room['extra_people_capacity'],
    room['extra_people_price'],
    totalPersons,
    totalDays
  )

  const priceWithAditionals = total_price + priceExtraPeople
  useEffect(() => {
    if (_reservationData.total_price !== priceWithAditionals) {
      const additionalData = {
        basic_price,
        total_price,
        totalDays,
        totalPersons,
        priceWithAditionals,
        priceExtraPeople,
        discountAmmount,
        discountMessage
      }

      const { checkIn, checkOut, ...rest } = _reservationData
      const _resData = {
        ...rest,
        total_price: priceWithAditionals,
        giftcard_value: _reservationData.giftcard_value,
        discount: Number(discountAmmount) || 0,
        additionals: { ...additionalData }
      }
      dispatch(updateReservationPriceAction({ ..._resData }))
    }
  }, [fields])

  useEffect(() => {
    if (totalDays < minNights && minNights !== null)
      return setDisableBtn(`Durée minimale de séjour : ${minNights} nuit(s)`)
    if (totalDays > maxNights && maxNights !== null)
      return setDisableBtn(`Durée maximale de séjour : ${maxNights} nuit(s)`)
    if (fields.checkIn > fields.checkOut)
      return setDisableBtn(`dates invalides`)
    return setDisableBtn('')
  }, [totalDays])

  return (
    <div>
      {totalDays !== 0 && (
        <div className="d-flex justify-content-between">
          <p className={`color-${color}`}>
            {!(total_price / totalDays)
              ? basic_price.toString().replace('.', ',')
              : (total_price / totalDays)
                  .toFixed(2)
                  .toString()
                  .replace('.', ',')}
            &euro; x {totalDays} nuits
          </p>
          <p className={`color-${color}`}>
            {total_price.toFixed(2).toString().replace('.', ',')}
            &euro;
          </p>
        </div>
      )}
      {totalPersons > room['extra_people_capacity'] && (
        <div className="d-flex justify-content-between">
          <p className={`color-${color}`}>Frais pour voyageur supplémentaire</p>
          <p className={`color-${color}`}>
            {priceExtraPeople}
            &euro;
          </p>
        </div>
      )}
      {fields.checkIn !== null &&
        fields.checkOut !== null &&
        discountAmmount !== null &&
        discountAmmount > 0 &&
        discountMessage !== null && (
          <div className="d-flex justify-content-between">
            <p className={`color-${color}`}>{discountMessage}</p>
            <p className={`color-${color}`}>
              {' '}
              - {discountAmmount.toString().replace('.', ',')}&euro;
            </p>
          </div>
        )}
      <div
        className={`border-bottom-divisor my-2 color-${color} background-white`}
      ></div>

      <InvoiceInformation _reservationData={_reservationData}>
        <GiftCardDiscount />
        <TotalPrice
          discountAmmount={discountAmmount}
          priceWithAditionals={priceWithAditionals}
          color={color}
        />
      </InvoiceInformation>

      <div className="d-flex justify-content-between"></div>
    </div>
  )
}

export default PricesCalc
