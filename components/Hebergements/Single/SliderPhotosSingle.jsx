import React, { useRef, useState } from 'react'
import Slider from 'react-slick'
import { useEffect } from 'react'

const SliderPhotosSingle = ({
  images = [],
  id,
  heigth = 150,
  width = 291,
  className = '',
  extraSettings,
  indexSelected = 0
}) => {
  const [imagesDimensions, setImagesDimensions] = useState(images)
  const slider1 = useRef()

  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    arrows: true,
    adaptiveHeight: true,
    slickGoTo: indexSelected,
    initialSlide: indexSelected,
    ...extraSettings,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          dots: false
        }
      }
    ]
  }

  useEffect(() => {
    slider1.current.slickGoTo(indexSelected)
  }, [])

  const updatedImage = ({ target: img }) => {
    const imagesDimensionsNew = imagesDimensions
    imagesDimensionsNew.map((item, index) => {
      if (parseInt(img.id) === index) {
        item.width = img.naturalWidth
        item.heigth = img.naturalHeight
      }
    })
    setImagesDimensions(imagesDimensionsNew)
  }

  return (
    <>
      <div className="modal-content modal-content__slider-photos">
        <div className="search-modal-body modal-body">
          <Slider ref={(slider) => (slider1.current = slider)} {...settings}>
            {imagesDimensions.map((item, index) => (
              <div key={index} className="d-flex justify-content-center">
                {/* <Image
            width={width}
            height={heigth}
            className={`d-block w-100 rounded-3 obj-fit-cover ${className}`}
            src={item}
            alt="First slide"
          /> */}
                <img
                  id={index}
                  height={item.heigth}
                  style={{ maxWidth: '100%', height: 'auto', width: 'auto' }}
                  className={`rounded-3 ${className} slick-slide__img-modal`}
                  src={
                    typeof item === 'string'
                      ? item
                      : process.env.NEXT_PUBLIC_AMAZON_IMGS + item.image.url
                  }
                  alt="First slide"
                  onLoad={updatedImage}
                />
              </div>
            ))}
          </Slider>
        </div>
      </div>
      <div className="text-center py-4"></div>
    </>
  )
}

export default SliderPhotosSingle
