import * as React from 'react'
import { consoleLog } from '/utils/logConsole'
import * as $ from 'jquery'
import { findDOMNode } from 'react-dom'
import Slider from 'react-slick'
/* Styles */
import {
  Content,
  Header,
  Pane,
  Labels,
  Label,
  Column,
  Title,
  Like,
  ProgressBar,
  Text,
  Anchor,
  Grid,
  Image,
  Progress,
  TextTravelers
} from './gallery.styles'

/* Components */
import { Cutlery, Heart, Star, Users } from 'components/Icons'

import icons from '../../../Maps/icons'

/* Utils */
import { getGreenScoreSourceIcon, getImageURL } from 'utils'
import SliderPhotosSingle from '../SliderPhotosSingle'
import { useEffect, useRef } from 'react'
import Loader from '@components/Loader'
import Resizer from 'react-image-file-resizer'
import { useWindowSize } from 'hooks/useWindowSize'

function Gallery({
  public_name,
  roomType,
  cityName,
  regionName,
  greenScore,
  images = [],
  roomTypeId,
  breakfast,
  room
}) {
  const [activeImage, setActiveImage] = React.useState(null)
  const [showSlider, setShowSlider] = React.useState(false)
  const [isMobile, setIsMobile] = React.useState(false)

  const settings = {
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
    arrows: false,
    touchMove: true
  }

  const extraSliderSettings = {
    dots: true
  }

  const handleSelectedImage = async (index) => {
    setShowSlider(false)
    setActiveImage(index)
    setTimeout(() => {
      handleShowSelectedImage(index)
    }, 500)
  }

  const handleShowSelectedImage = (index) => {
    setShowSlider(true)
  }

  const { width } = useWindowSize()

  React.useEffect(() => {
    setIsMobile(width < 768)
  }, [width])

  return (
    <Content className="gallery__container m-0">
      <Header>
        {roomType && (
          <Pane>
            <Labels className="">
              <Label className="font-montserrat fz-15">
                {icons[roomTypeId]?.name}
              </Label>
            </Labels>
          </Pane>
        )}

        <Pane className="flex-column flex-lg-row position-relative align-items-start align-content-lg-center">
          <Title className="h1">{public_name}</Title>
          <a
            href="#engagement_eco_responsable"
            style={{ textDecoration: 'none' }}
          >
            <figure className="single__greenscore-header-figure">
              <img
                width="150px"
                style={{ objectFit: 'cover', right: 0 }}
                src={getGreenScoreSourceIcon(greenScore)}
                className="single__greenscore-header-img"
              />
              <label className="single__greenscore-header-label">
                Greenscore
              </label>
            </figure>
          </a>
        </Pane>

        <Pane className="justify-content-start flex-lg-row flex-column align-items-start gallery__container-about">
          <Column>
            <Text>
              <Anchor className="font-montserrat underline fz-15">
                {cityName}, {regionName}
              </Anchor>
            </Text>
          </Column>
          <Column className="d-none d-lg-block">
            <span className="mx-4 size-text">|</span>
          </Column>
          <Column className="gallery__container-about-info">
            <TextTravelers className="font-montserrat ml-2 fz-15">
              <Users height="20px" width="20px" className="mr-10" />
              Les hôtes : {room.people_capacity}
            </TextTravelers>
          </Column>
          <Column className="d-none d-lg-block">
            <span className="mx-4 size-text">|</span>
          </Column>
          <Column className="gallery__container-about-info">
            <Cutlery />
            <Text className="font-montserrat ml-2 fz-15 ms-0">
              Petit déjeuner : {breakfast}
            </Text>
          </Column>
        </Pane>
      </Header>

      {isMobile ? (
        <div className="gallery__slider__container">
          <Slider {...settings}>
            {images.map(
              (image, index) =>
                index < 4 && (
                  <Image
                    onClick={() => handleSelectedImage(index)}
                    key={image.image.id}
                    data-bs-toggle="modal"
                    data-bs-target="#galleryModal"
                    className="gallery__slider__item"
                    src={getImageURL(image.image?.url)}
                  />
                )
            )}
          </Slider>
        </div>
      ) : (
        <Grid className="gallery__grid">
          {images.map(
            (image, index) =>
              index < 4 && (
                <Image
                  onClick={() => handleSelectedImage(index)}
                  key={image.image.id}
                  data-bs-toggle="modal"
                  data-bs-target="#galleryModal"
                  className="gallery__grid__item"
                  src={getImageURL(image.image?.url)}
                />
              )
          )}
        </Grid>
      )}

      <div
        className="modal fade"
        id="galleryModal"
        tabIndex={-1}
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="search-modal-dialog modal-dialog modal-dialog-centered">
          {showSlider ? (
            <>
              <SliderPhotosSingle
                images={images}
                width="1920"
                heigth="1000"
                extraSettings={extraSliderSettings}
                indexSelected={activeImage}
              />
            </>
          ) : (
            <Loader />
          )}
        </div>
      </div>
    </Content>
  )
}

export default Gallery
