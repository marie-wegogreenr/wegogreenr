import styled from '@emotion/styled'
import { colors, sizes } from 'styles/theme'

export const Container = styled.section`
  background-color: var(--color-yellow-light);
  padding: 1rem 0;
`

export const Content = styled.div``

export const Header = styled.div``

export const Pane = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const Labels = styled.div`
  display: flex;
  margin-bottom: 2rem;
`

export const Label = styled.span`
  margin-right: 1rem;
  font-size: ${sizes['text-xs']};
  background-color: #899a7a;
  padding: 0 0.5rem;
  border-radius: 1rem;
  color: var(--color-white);
  font-size: 12px;
  padding: 0.25rem 1rem;
`

export const Column = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 2.5rem;
  margin-top: 0.5rem;

  @media (max-width: 991px) {
    margin-bottom: 1rem;
  }
`

export const Title = styled.h1`
  font-family: var(--font-fraunces);
  font-weight: 700;
  color: var(--color-primary);
  line-height: 1.3;
`
export const Like = styled.button`
  margin-left: 1rem;
  background-color: transparent;
  border: none;
`

export const ProgressBar = styled.progress``

export const Text = styled.p`
  margin-bottom: 0;
`

export const TextTravelers = styled.p`
  margin: 0 !important;
`

export const Anchor = styled.a`
  font-weight: 500;
  text-decoration: underline !important;
  @media screen and (max-width: 991px) {
    display: block;
    margin: 0.82rem 0 0;
  }
`

export const Grid = styled.div`
  display: grid;
  width: 100%;
  grid-template-columns: repeat(5, 1fr);
  grid-template-rows: repeat(2, minmax(100px, 250px));
  gap: 1rem;
  border-radius: 0.5rem;
  overflow: hidden;
`

export const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  cursor: pointer;

  &:nth-of-type(1) {
    grid-row: 1 / span 2;
    grid-column: 1 / span 2;
  }

  &:nth-of-type(2) {
    grid-row: 1 / span 2;
    grid-column: 3 / span 2;
  }
`

export const Progress = styled.div`
  display: flex;
  flex-direction: column;
`
