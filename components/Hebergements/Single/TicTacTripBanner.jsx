import Link from 'next/link'
import Icon from 'public/images/tictactrip/tictactrip_icon.png'
import { useEffect, useState } from 'react'

export default function TicTacTripBanner() {
  const [isEstablishment, setIsEstablishment] = useState(false)
  useEffect(() => {
    const url = window.location.href
    const splittedUrl = url.split('/')
    if (splittedUrl[3] === 'establishment') {
      setIsEstablishment(true)
    }
  }, [])

  const data = {
    title: `Réservez votre trajet responsable avec notre partenaire`,
    icon: '',
    btn: `Go GreenR !`
  }

  return (
    <section className="tictactrip__banner">
      <h2
        className={`tictactrip__banner-title ${
          isEstablishment ? 'tictactrip__banner-title--wide' : ''
        }`}
      >
        <span className="tictactrip__banner-text">{data.title}</span>
        <span
          className={`tictactrip__banner-image ${
            isEstablishment ? 'tictactrip__banner-image--wide' : ''
          }`}
        >
          <img
            src={Icon}
            alt="tictactrip_icon"
            className="tictactrip__banner-img"
          />
        </span>
      </h2>
      <Link href="/mobilite-durable">
        <a
          target="_blank"
          className="button tictactrip__banner-btn button--save-onboarding"
        >
          {data.btn}
        </a>
      </Link>
    </section>
  )
}
