import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import { Checked } from 'components/Icons'
import { Loader } from 'components/'

import { getGreenScoreSourceIcon } from 'utils'

/* Redux */
import { useDispatch, useSelector } from 'react-redux'
import { getGreenScoreItemsById } from '@ducks/hebergements/greenScoreItems'

const MAX_COMMITMENTS = 9
function Commitments({ userId, greenScore }) {
  // states
  const [showAll, setShowAll] = React.useState(false)

  // redux states
  const { greenScoreItems } = useSelector((state) => state)
  // helper methods
  const requestsPending = () => {
    return greenScoreItems.status === 'LOADING' || !userId
  }

  const requestSuccessful = () => {
    return greenScoreItems.status === 'SUCCESS'
  }

  const requestsFailure = () => {
    return greenScoreItems.status === 'FAILURE'
  }

  // dispatch
  const dispatch = useDispatch()

  //consoleLog(userId, 'userid')
  // effects
  React.useEffect(() => {
    if (userId && greenScoreItems.status === 'IDLE') {
      dispatch(getGreenScoreItemsById(userId))
    }
  }, [userId])

  if (requestSuccessful() && greenScoreItems.data?.length === 0) {
    return (
      <>
        <div>
          <h2 className="color-primary size-h2">Engagements écoresponsables</h2>
        </div>

        <div className="border-bottom my-3" />

        <div className="w-100 d-flex flex-wrap">
          <p>aucune information</p>
        </div>
      </>
    )
  }

  return (
    <>
      <div className="d-md-flex justify-content-between align-items-center">
        <h2 className="color-primary single__title-name">
          Engagements écoresponsables
        </h2>
        <div className="d-flex flex-column ">
          <img
            width="90px"
            style={{ objectFit: 'cover' }}
            src={getGreenScoreSourceIcon(greenScore)}
          />
          <label className="single__greenscore-header-label--small">
            Greenscore
          </label>
        </div>
      </div>

      <div className="border-bottom my-3" />

      {requestsPending() && <Loader />}

      {requestsFailure() && <p>Server internal error</p>}

      {requestSuccessful() && (
        <>
          <div className="row w-100">
            {greenScoreItems.data
              ?.slice(0, showAll ? undefined : MAX_COMMITMENTS)
              .map((item) => {
                const { green_score_response } = item
                const { response } = green_score_response
                return (
                  <div key={item.id} className="col-lg-4 col-md-6">
                    <div className="d-flex mb-2 mb-sm-4">
                      <span>
                        <Checked
                          className="mt-1 d-none d-sm-block"
                          width="1.5rem"
                          height="1.5rem"
                        />
                      </span>

                      <p className="ml-0 ml-sm-2 mb-0 font-montserrat fz-15">
                        {response}
                      </p>
                    </div>
                  </div>
                )
              })}
          </div>
          {greenScoreItems?.data.length > MAX_COMMITMENTS && (
            <button
              onClick={() => setShowAll(!showAll)}
              className={`btn btn-outline-secondary single__btn-section`}
            >
              tout voir
            </button>
          )}
        </>
      )}
    </>
  )
}

export default Commitments
