import * as React from 'react'
import { consoleLog } from '/utils/logConsole'
import { Checked } from 'components/Icons'

/* Components */
import { Users } from 'components/Icons'

function Equipments({ equipments = [] }) {
  const MAX_EQUIPMENTS = 9
  const [showAll, setShowAll] = React.useState(false)
  let content

  if (equipments.length) {
    content = equipments
      ?.slice(0, showAll ? undefined : MAX_EQUIPMENTS)
      .map((item) => {
        const { type, id } = item
        const { name } = type === undefined ? item.equipment : type
        return (
          <div key={id} className="col-lg-4 col-md-6 px-0 px-sm-3">
            <div className="d-flex align-items-center mb-2 mb-sm-4">
              {/* <span className="bg-primary p-1 rounded-2">
                <Users stroke="white" width="20" height="20" />
              </span> */}
              <Checked
                className="mt-1 single__check"
                width="1.5rem"
                height="1.5rem"
              />
              <p className="ml-0 ml-sm-2 mb-0 font-montserrat fz-15">
                {name}
                <span></span>
              </p>
            </div>
          </div>
        )
      })
  } else {
    content = <p>Il n'y a pas d'équipement</p>
  }

  return (
    <div>
      <h2 className="color-primary single__title-name">Équipements</h2>

      <div className="border-bottom my-3" />

      <div className="w-100 d-flex flex-wrap">{content}</div>

      {equipments.length > MAX_EQUIPMENTS && (
        <button
          onClick={() => setShowAll(!showAll)}
          className={`btn btn-outline-secondary single__btn-section`}
        >
          Afficher les autres équipements
        </button>
      )}
    </div>
  )
}

export default Equipments
