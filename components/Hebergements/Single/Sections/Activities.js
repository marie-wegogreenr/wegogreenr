import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

import { Checked } from 'components/Icons'

function Activities({ activities = [] }) {
  const MAX_ACTIVITIES = 9
  const [showAll, setShowAll] = React.useState(false)
  let content

  if (activities.length) {
    content = activities
      ?.slice(0, showAll ? undefined : MAX_ACTIVITIES)
      .map((item) => {
        const { type, id } = item
        const { name } = type
        return (
          <div key={id} className="col-lg-4 col-md-6 px-0 px-sm-3">
            <div className="d-flex align-items-center mb-2 mb-sm-4">
              <span>
                <Checked
                  className="mt-1 single__check"
                  width="1.5rem"
                  height="1.5rem"
                />
              </span>
              <p className="ml-0 ml-sm-2 mb-0 font-montserrat">{name}</p>
            </div>
          </div>
        )
      })
  } else {
    content = <p>Il n'y a pas d'activités</p>
  }
  return (
    <React.Fragment>
      <div>
        <h2 className="color-primary single__title-name">
          Activités sur place
        </h2>
      </div>

      <div className="border-bottom my-3" />

      <div className="w-100 d-flex flex-wrap">{content}</div>

      {activities.length > MAX_ACTIVITIES && (
        <button
          onClick={() => setShowAll(!showAll)}
          className={`btn btn-outline-secondary single__btn-section`}
        >
          Afficher les 20 activités
        </button>
      )}
    </React.Fragment>
  )
}

export default Activities
