import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Hooks */
import { useIsInViewport } from 'hooks'
// import { NavbarContext } from 'context/navbar'

function LayoutSection({ children, id, className, from }) {
  const { elementRef, boundingClientRect } = useIsInViewport()

  // React.useEffect(() => {
  //   if (boundingClientRect.y <= 200 && boundingClientRect.y >= -200) {
  //     setIsActive(id)
  //   }
  // }, [boundingClientRect.y])

  return (
    <div id={id} className={`pt-5 card-white ${className}`}>
      <div
        ref={elementRef}
        className={`rounded-3 ${
          from === 'description'
            ? 'card-white__single--description'
            : 'bg-white'
        }`}
      >
        {children}
      </div>
    </div>
  )
}

export default LayoutSection
