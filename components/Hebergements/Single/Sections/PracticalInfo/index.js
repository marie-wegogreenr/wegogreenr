import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import Tabs from './Tabs'
import Content from './Content'
import AdditionalInfo from '@components/checkout/reservation_form/AdditionalInfo'

function PracticalInfo({ resData, services, stablishment, from }) {
  return (
    <React.Fragment>
      <div>
        <h2
          className={`color-primary single__title-name ${
            from === 'checkout' ? 'font-montserrat h2-dashboard' : ''
          }`}
        >
          Informations pratiques
        </h2>
      </div>

      {from === 'checkout' && (
        <>
          <div className="border-bottom-divisor mt-3 mb-4"></div>
          <div>
            <AdditionalInfo data={resData} />
          </div>
        </>
      )}

      {from !== 'checkout' && <div className="border-bottom my-3" />}

      <div>
        {/* Nav tabs */}
        <ul className="nav nav-pills secondary" id="myTab" role="tablist">
          <Tabs />
        </ul>
        {/* Tab panes */}
        <div className="tab-content py-2">
          <Content services={services} stablishment={stablishment} />
        </div>
      </div>
    </React.Fragment>
  )
}

export default PracticalInfo
