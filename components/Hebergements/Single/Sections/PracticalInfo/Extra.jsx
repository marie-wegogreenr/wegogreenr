import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import { Checked } from '@components/Icons'
import { getServiceType } from 'helpers/utilsHelper'

export function Extra({ paymentServices = [] }) {
  return (
    <div className="row w-100">
      {paymentServices.map(({ service, id, ...s }) => (
        <div key={id} className="col-lg-4 col-md-6">
          <div className="d-flex mb-4">
            <span>
              <Checked className="mt-1" width="1.5rem" height="1.5rem" />
            </span>

            <p className="ml-2 mb-0 font-montserrat fz-15">{`${service.name} ${
              s.breakfast_type_name ? ': ' + s.breakfast_type_name : ''
            } ${s.price}€  ${
              s.modality ? `/ ${getServiceType(s.modality)}` : ''
            }`}</p>
          </div>
        </div>
      ))}
    </div>
  )
}

export default Extra
