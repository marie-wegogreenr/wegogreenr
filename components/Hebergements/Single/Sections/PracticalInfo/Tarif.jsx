import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import { Checked } from '@components/Icons'

export function Tarif({ freeServices = [] }) {
  return (
    <div className="row w-100">
      {freeServices.map(({ service, id, ...s }) => (
        <div key={id} className="col-lg-4 col-md-6">
          <div className="d-flex mb-3">
            <span>
              <Checked className="mt-1" width="1.5rem" height="1.5rem" />
            </span>

            <p className="ml-2 mb-0 font-montserrat fz-15">
              {`${service.name} ${
                s.breakfast_type_name ? ': ' + s.breakfast_type_name : ''
              }`}
            </p>
          </div>
        </div>
      ))}
    </div>
  )
}

export default Tarif
