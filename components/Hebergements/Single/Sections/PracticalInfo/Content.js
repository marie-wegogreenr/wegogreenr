import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Fixtures */
import { practicalInfoTabs } from 'fixtures'

function Content({ services, stablishment }) {
  //consoleLog(services)
  const paymentServices = React.useMemo(() => {
    return services.filter(({ price }) => {
      return Number(price) > 0
    })
  }, [services])

  const freeServices = React.useMemo(() => {
    return services.filter(({ price }) => {
      return Number(price) === 0
    })
  }, [services])

  return (
    <React.Fragment>
      {practicalInfoTabs.map((item, index) => {
        const { content: Content, id } = item
        return (
          <div
            key={id}
            className={`tab-pane ${index === 0 ? 'active' : ''}`}
            id={id}
            role="tabpanel"
            aria-labelledby={`${id}-tab`}
          >
            {typeof Content === 'string' ? (
              <p className="font-montserrat fz-15">{Content}</p>
            ) : (
              <Content
                paymentServices={paymentServices}
                freeServices={freeServices}
                stablishment={stablishment}
              />
            )}
          </div>
        )
      })}
    </React.Fragment>
  )
}

export default Content
