import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Fixtures */
import { practicalInfoTabs } from 'fixtures'

function Tabs() {
  return (
    <React.Fragment>
      {practicalInfoTabs.map((item, index) => {
        const { title, id } = item

        return (
          <li key={id} className="nav-item" role="presentation">
            <button
              className={`nav-link rounded-pill py-1 weight-bold font-montserrat fz-15 ${
                index === 0 ? 'active' : ''
              }`}
              id={`${id}-tab`}
              data-bs-toggle="tab"
              data-bs-target={`#${id}`}
              type="button"
              role="tab"
              aria-controls={id}
              aria-selected={index === 0}
              style={{ padding: '1.5rem 1.25rem' }}
            >
              {title}
            </button>
          </li>
        )
      })}
    </React.Fragment>
  )
}

export default Tabs
