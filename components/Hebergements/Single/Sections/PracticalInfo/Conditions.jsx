import {
  fixedAnswerByDefault,
  fixedAnswerForUser
} from 'constants/cancellationConditions'
import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { getCancellationConditionsStatus } from 'services/etablissementService'

export function Conditions({ stablishment }) {
  const store = useSelector((store) => store?.siteTexts?.data)
  let text
  if (store !== null && store[1]) text = store['1']

  const [cancellationConditions, setCancellationConditions] = useState([])

  useEffect(async () => {
    if (stablishment?.id) {
      await getCancellationConditions()
    }
  }, [stablishment])

  const getCancellationConditions = async () => {
    try {
      const cancellationConditionsResponse =
        await getCancellationConditionsStatus(stablishment.id)
      if (cancellationConditionsResponse.code === 200) {
        setCancellationConditions(cancellationConditionsResponse.data)
      }
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <div>
      <h3 className="font-montserrat weight-bold color-black fz-20">
        {text?.title}
      </h3>
      {cancellationConditions?.length > 0 ? (
        <>
          <p>
            <strong>{`Jusqu’à ${cancellationConditions[0].days_quantity} ${
              cancellationConditions[0].days_quantity === 1 ? 'jour' : 'jours'
            } avant votre séjour :`}</strong>
          </p>
          <p>{fixedAnswerForUser}</p>
          <p>
            <strong>{`Moins de ${cancellationConditions[0].days_quantity} ${
              cancellationConditions[0].days_quantity === 1 ? 'jour' : 'jours'
            } avant votre séjour ou non présentation :`}</strong>
          </p>
          {cancellationConditions[0].penality_percentage === 100 ? (
            <p>Non remboursable</p>
          ) : (
            <p>{`Votre réservation est remboursée à hauteur de ${cancellationConditions[0].penality_percentage}%.`}</p>
          )}
        </>
      ) : (
        <>
          <p>{fixedAnswerByDefault.option1}</p>
          <p>{fixedAnswerByDefault.option2}</p>
        </>
      )}
      <h3 className="font-montserrat weight-bold color-black fz-20 mt-4">
        {store && store[2]?.title}
      </h3>
      <p className="fz-15">{store && store[2]?.text}</p>
    </div>
  )
}

export default Conditions
