import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import {
  Arrive,
  Cutlery,
  Depart,
  DisabledPerson,
  Home,
  Pet,
  Users
} from 'components/Icons'

import dynamic from 'next/dynamic'

import { getImageURL } from 'utils'
import { findBreakfastCategory } from 'helpers/roomServicesHelper'
import { roundTimeToNearest } from 'helpers/timeHelper'

const DynamicMap = dynamic(() => import('@components/Maps/Map'), { ssr: false })

function Description({
  description,
  strengths,
  firstName,
  area,
  capacity,
  establishmentLat,
  establishmentLng,
  hostInformation,
  establishment,
  conditions,
  conditionsStablishment,
  services = [],
  user
}) {
  const BreakFastCategory = findBreakfastCategory(services)
  const [animalCondition, setAnimalCondition] = React.useState(false)
  const [timeLimits, setTimeLimits] = React.useState({})

  React.useEffect(() => {
    let conditionArray = []

    if (conditions && conditionsStablishment) {
      if (conditions.length > 0 && conditionsStablishment.length > 0) {
        conditionArray = conditionsStablishment.filter(
          (item) => item.condition.id === conditions[2].id
        )
      }
    }

    const conditionAnimal = conditionArray.length > 0 ? true : false

    setAnimalCondition(conditionAnimal)
  }, [])

  React.useEffect(() => {
    setTimeLimits({
      arrive: roundTimeToNearest(establishment?.min_entry_time ?? '00:00'),
      departure: roundTimeToNearest(establishment?.max_exit_time ?? '00:00')
    })
  }, [])

  return (
    <React.Fragment>
      <div className="d-flex justify-content-between align-items-center">
        <div className="single__title">
          <h2 className="single__title-name font-fraunces">Description</h2>
          <h3 className="font-montserrat weight-semibold lh-20 fz-18">
            {/* {establishmentName} */}
          </h3>
        </div>

        <div className="d-flex flex-column flex-sm-row align-items-center">
          <img
            className="rounded-circle navbar-nav__profile-image me-0 me-sm-0"
            src={getImageURL(user?.photo_url)}
            height="50px"
            width="50px"
          />

          <h5 className="mb-0 font-montserrat ms-0 ms-sm-2 text mt-2 mt-sm-0">
            {establishment.user.name}
          </h5>
        </div>
      </div>

      <div className="border-bottom my-1-5" />

      <div>
        <p className="font-montserrat fz-15">{description}</p>

        {/* <button className="btn color-secondary p-0 weight-semibold font-montserrat">
          Savoir plus
        </button> */}
      </div>

      <div className="border-bottom my-1-5" />

      <div className="row">
        <div className="col-md-6">
          <div className="mb-3 fz-15 d-flex align-items-center">
            <Arrive className="mr-10" height="20px" width="20px" />
            <span className="font-montserrat color-black">
              Arrivée : {timeLimits.arrive} h
            </span>
          </div>
          <div className="mb-3 fz-15 d-flex align-items-center">
            <Users height="20px" width="20px" className="mr-10" />
            <span className="font-montserrat color-black">
              Nombre de personnes : {capacity}
            </span>
          </div>

          <div className="mb-3 fz-15 d-flex align-items-center">
            <Cutlery height="20px" width="20px" className="mr-10" />
            <span className="font-montserrat color-black">
              Petit-déjeuner : {BreakFastCategory}
            </span>
          </div>
        </div>
        <div className="col-md-6">
          <div className="mb-3 fz-15 d-flex align-items-center">
            <Depart height="20px" width="20px" className="mr-10" />
            <span className="font-montserrat color-black">
              Départ : {timeLimits.departure}h
            </span>
          </div>
          <div className="mb-3 fz-15 d-flex align-items-center">
            <Pet className="mr-10" width="20px" height="20px" />
            <span className="font-montserrat color-black">
              Animaux : {animalCondition ? 'autorisés' : 'non autorisés'}
            </span>
          </div>
          <div className="mb-3 fz-15 d-flex align-items-center">
            <DisabledPerson height="20px" width="20px" className="mr-10" />
            <span className="font-montserrat color-black">
              Accessible aux PMR : Non
            </span>
          </div>
          <div className="mb-3 fz-15 d-flex align-items-center">
            <Home height="20px" width="20px" className="mr-10" />
            <span className="font-montserrat color-black">
              Surface du logement : {area}m<sup>2</sup>
            </span>
          </div>
        </div>
      </div>

      <div className="mt-4">
        <h4 className="mb-3 font-montserrat fz-20">Ses points forts</h4>

        <div>
          <p className="font-montserrat">{strengths}</p>
        </div>

        {/* <button className="btn color-secondary p-0 weight-semibold font-montserrat">
          Savoir plus
        </button> */}
      </div>

      <div className=" bg-light my-4">
        <div className="" style={{ height: '385px' }}>
          <DynamicMap
            center={{ lat: establishmentLat, lng: establishmentLng }}
          />
        </div>
      </div>

      {/* CARD */}

      <div className="card bg-primary text-white card-white">
        <div className="">
          <div className="d-flex align-items-center">
            <img
              width="92px"
              height="92px"
              className="rounded-circle navbar-nav__profile-image"
              src={getImageURL(user?.photo_url)}
              alt=""
            />
            <div className="ml-4">
              <h5 className="card-title fz-28 mb-0 weight-bold color-white">
                {firstName}
              </h5>
              {/* <p className="mb-0 font-montserrat text-white paragraph">Totem</p> */}
            </div>
          </div>

          <p className="mt-4 text-white font-montserrat paragraph ">
            {hostInformation?.presentation}
          </p>
        </div>
      </div>
    </React.Fragment>
  )
}

export default Description
