import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import Description from './Description'
import Equipments from './Equipments'
import Commitments from './Commitments'
import Activities from './Activities'
import LayoutSection from './LayoutSection'
import PracticalInfo from './PracticalInfo'

function Sections({
  categories,
  room,
  establishment,
  conditions,
  conditionsStablishment,
  services = []
}) {
  const equipments = React.useMemo(
    () => categories.filter((category) => category.type.category_id <= 5),
    [categories]
  )

  const activities = React.useMemo(
    () => categories.filter((category) => category.type.category_id > 5),
    [categories]
  )

  const { description, strengths, total_area, people_capacity } = room
  const {
    contact_lastname,
    contact_name,
    lat,
    lng,
    name: establishmentName,
    user_id,
    user: { host_information },
    user,
    green_score
  } = establishment

  return (
    <div>
      <div id="description"></div>
      <LayoutSection
        id=""
        className="card-white__single--description"
        from="description"
      >
        <Description
          establishmentName={establishmentName}
          area={total_area}
          firstName={contact_name}
          lastName={contact_lastname}
          about={'additional_information'}
          strengths={strengths}
          description={description}
          conditions={conditions}
          conditionsStablishment={conditionsStablishment}
          capacity={people_capacity || 1}
          establishmentLat={lat}
          establishmentLng={lng}
          hostInformation={host_information}
          establishment={establishment}
          services={services}
          user={user}
        />
        <div id="equipement"></div>
      </LayoutSection>
      <LayoutSection id="" className="card-white__single-section">
        <Equipments equipments={equipments} />
        <div id="activites_proches"></div>
      </LayoutSection>

      <LayoutSection className="card-white__single-section">
        <Activities activities={activities} />
        <div id="engagement_eco_responsable"></div>
      </LayoutSection>

      <LayoutSection className="card-white__single-section">
        <Commitments userId={user_id} greenScore={green_score?.score} />
        <div id="infos_pratiques"></div>
      </LayoutSection>

      <LayoutSection className="card-white__single-section">
        <PracticalInfo services={services} stablishment={establishment} />
      </LayoutSection>
    </div>
  )
}

export default Sections
