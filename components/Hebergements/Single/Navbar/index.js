import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Fixtures */
import { singleTabs } from 'fixtures'

/* Context */
// import { NavbarContext } from 'context/navbar'

/* MAIN COMPONENT */
function Navbar({ sticky, navRef = null, hidden = false }) {
  // const { isActive } = React.useContext(NavbarContext)
  const isActive = false

  return (
    <nav
      style={{
        visibility: hidden ? 'hidden' : 'visible',
        maxHeight: '4.2em',
        top: sticky ? '110px' : ''
      }}
      ref={navRef}
      className={`navbar__container size-small ${sticky ? 'sticky-top' : ''}
       rounded-3 bg-gray-light`}
    >
      <div className="navbar__content">
        <ul className="d-flex list-unstyled px-2 align-items-center justify-content-evenly nav__item-description-list">
          {singleTabs.map((tab) => {
            const { id, title } = tab
            return (
              <li
                key={id}
                className={`nav__item mr-2 w-100 text-center ${
                  isActive === id ? 'active' : ''
                }`}
              >
                <a
                  className={`nav__item nav__item-description-list-tab text-truncate py-1-25 text-decoration-none w-100 font-montserrat weight-semibold ${
                    isActive === id ? 'color-black' : 'color-gray'
                  }`}
                  href={`#${id}`}
                  type="button"
                >
                  {title}
                </a>
              </li>
            )
          })}
        </ul>
      </div>
    </nav>
  )
}

export default Navbar
