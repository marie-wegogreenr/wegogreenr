import React from 'react'
import { consoleLog } from '/utils/logConsole'

const input = ({ form, label, name, setForm, required = false }) => {
  const handleChange = (e) => {
    setForm({ ...form, [name]: e.target.value })
  }

  return (
    <label className="d-flex flex-column w-100 text weight-bold">
      {label}
      <input
        type="text"
        value={form[name]}
        onChange={handleChange}
        name={name}
        className="form form-control mt-2"
        required={required}
      />
    </label>
  )
}

export default input
