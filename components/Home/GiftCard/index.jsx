import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import { Gift } from '@components/Icons'

function GiftCardSection() {
  return (
    <section className="gift-card__container bg-primary py-5 color-white">
      <div className="container-xxl">
        <div className="gift-card__content d-md-flex justify-content-between align-items-center">
          <div className="gift-card__content__column gift-card__content__column--icon">
            <Gift width="17rem" />
          </div>
          <div className="gift-card__content__column gift-card__content__column--body">
            <h3 className="color-white size-h2">Carte cadeau</h3>
            <p className="color-white size-text">
              Offrez des moments, des souvenirs, une bonne dose de déconnexion
              parce que vivre une expérience c’est écrire une histoire que l’on
              garde dans le coin de sa mémoire.
            </p>
          </div>
          <div className="gift-card__content__column">
            <button className="btn btn-secondary no-wrap ml-md-4 mr-4">
              J’offre une carte cadeau
            </button>
          </div>
        </div>
      </div>
    </section>
  )
}

export default GiftCardSection
