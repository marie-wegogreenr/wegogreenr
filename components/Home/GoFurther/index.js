import * as React from 'react'
import { resize } from 'utils'
/* Components */
import { ChevronRight } from '@components/Icons'
import { useSelector } from 'react-redux'

import { cutText } from 'utils'
import { useWindowSize } from 'hooks/useWindowSize'
import Image from 'next/image'
//redux

function GoFurther() {
  const blogs = useSelector((store) => store.blog.data)
  const { width } = useWindowSize()
  const max = width <= 575 ? 2 : 4

  const latestBlogs = blogs ? blogs.slice(1, max) : []

  const gotoUrl = (url) => {
    if (!window || !url) return
    window.open(url, '_blank')
  }

  if (blogs) {
    return (
      <div className="container-xxl go-further py-5">
        <div className="mb-4 tittle-go-further">
          <h1 className="color-primary size-h2">
            {width <= 575
              ? 'Aller + loin'
              : `Plus d'inspirations pour des séjours uniques`}
          </h1>
        </div>
        <div className="go-further__grid">
          <div
            className="go-further__main-image br-10"
            onClick={() => gotoUrl(blogs[0]?.link)}
          >
            <a
              href="#"
              target="_blank"
              className="br-10"
              onClick={(e) => e.preventDefault()}
            >
              <div className="card border-0 br-10 h-100 bg-dark text-white shadow">
                <Image
                  height="500px"
                  width="300px"
                  src={blogs[0].jetpack_featured_media_url}
                  className="card-img shdow-1 br-10 w-100"
                  alt=""
                />
                <div className="card-img-overlay p-3 p-sm-5 d-flex flex-column justify-content-end">
                  <div style={{ zIndex: '2' }} className="position-relative">
                    {/* <p className="card-title color-gray size-text">
                    Cauvignac, nouvelle acquitaine 
                  </p> */}
                    <div className="d-flex  justify-content-between align-items-center">
                      <div className="w-75">
                        <h3 className="card-text color-white weight-bold fz-1-5 lh-175rem go-further__main-article">
                          {blogs[0].title.rendered}
                        </h3>
                      </div>
                      <button
                        className="btn btn-secondary rounded-circle p-2 my-3 mt-lg-0"
                        style={{ height: '3rem', width: '3rem' }}
                      >
                        <ChevronRight
                          stroke="white"
                          width="1.5rem"
                          height="1.5rem"
                        />
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </a>
          </div>

          <div className="go-further__links">
            {React.Children.toArray(
              latestBlogs.map((blog, index) => (
                <a
                  href={blog.link}
                  target="_blank"
                  className={`text-decoration-none`}
                >
                  <div className="card mb-1 border-0 bg-transparent go-further__card-wrapper">
                    <div className="go-further__card-content d-flex align-items-baseline align-items-lg-center h-100">
                      <div className="h-100 go-further__box-img">
                        <Image
                          height="100px"
                          width="300px"
                          className="shdow-1 br-10 image__sizes go-further__card-img"
                          src={blog.jetpack_featured_media_url}
                          alt="..."
                        />
                      </div>
                      <div className="go-further__box-text">
                        <div className="go-further__card-body card-body py-0 p-0 px-lg-3">
                          <p className="card-text color-primary font-fraunces m-0 fz-1-25 weight-bold fs-6">
                            {cutText(blog.title.rendered, 60)}
                          </p>
                          <a
                            href={blog.link}
                            target="_blank"
                            className="card-text font-montserrat text-decoration-none color-secondary border-0 px-0 weight-semibold fz-15"
                          >
                            Plus d’inspirations
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
              ))
            )}
          </div>
        </div>
      </div>
    )
  } else return null
}

export default GoFurther
