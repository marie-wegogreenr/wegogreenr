import styled from '@emotion/styled'
import { sizes } from 'styles/theme'

export const Radios = styled.div``

export const InputText = styled.span`
  padding: 0.5rem 1.5rem;
  border-radius: 2rem;
  font-family: var(--font-montserrat);
  font-weight: 500;
  font-size: 15px;
  transition: all ease 300ms;
`

export const Radio = styled.input`
  &:hover:checked + ${InputText} {
    filter: brightness(0.9);
  }

  &:checked + ${InputText} {
    background-color: var(--color-secondary);
  }

  &:hover + ${InputText} {
    background-color: var(--color-secondary);
  }
`

export const Label = styled.label`
  cursor: pointer;
  margin-right: 1rem;

  &:last-of-type {
    margin-right: 0;
    @media (max-width: 400px) {
      margin-top: 10px;
    }
  }
`
