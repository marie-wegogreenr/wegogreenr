import * as React from 'react'
/* Context */
import { useDispatch } from 'react-redux'
import { setSearchLocation } from 'redux/ducks/hebergements/search/actions'
/* Next */
import { useRouter } from 'next/router'
/* Styles */
import { Radios, Radio, Label, InputText } from './styles'
/* Fixtures */
import { searchFormRadios } from 'fixtures'
/* Constants */
import { routes, DEFAULT_FRANCE } from 'constants/index'
import { useWindowSize } from 'hooks/useWindowSize'

function Buttons() {
  const dispatch = useDispatch()
  const router = useRouter()
  const [searchType, setSearchType] = React.useState(
    searchFormRadios.find((item) => item.checked).text
  )

  const { width } = useWindowSize()
  const [isMobile, setIsMobile] = React.useState(false)

  React.useEffect(() => {
    if (width <= 912) {
      setIsMobile(true)
    } else {
      setIsMobile(false)
    }
  }, [width])

  /* Helpers functions */
  const onChangeType = (e) => {
    const value = e.target.value
    setSearchType(value)
  }

  const handleClick = (e, text) => {
    if (text == 'Proche de chez moi') {
      window.scroll({ top: 2200, behavior: 'smooth' })
    } else {
      const coords = {
        zoom: DEFAULT_FRANCE.zoom,
        locationName: DEFAULT_FRANCE.locationName,
        lat: DEFAULT_FRANCE.lat,
        lng: DEFAULT_FRANCE.lng
      }
      dispatch(setSearchLocation(coords))
      router.push(routes.SEARCHED_ROOMS)
    }
  }

  return (
    <Radios>
      {!isMobile &&
        React.Children.toArray(
          searchFormRadios.map((item) => (
            <Label onClick={(e) => handleClick(e, item.text)}>
              <Radio
                onChange={onChangeType}
                defaultChecked={item.checked}
                hidden
                name="type"
                type="radio"
                defaultValue={item.value}
              />
              <InputText className="text-white">{item.text}</InputText>
            </Label>
          ))
        )}
    </Radios>
  )
}

export default Buttons
