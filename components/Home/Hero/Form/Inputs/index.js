import * as React from 'react'
/* Components */
import AddressInput from './AddressInput'
import CheckInInput from './CheckInInput'
import CheckOutInput from './CheckOutInput'
import TravelsInput from './TravelsInput'
/* Styles */
import { Container } from './styles'
/* Dependencies */
import { registerLocale, setDefaultLocale } from 'react-datepicker'
import fr from 'date-fns/locale/fr'
registerLocale('fr', fr)
setDefaultLocale('fr')

function SearchInput({ addressInputRef }) {
  return (
    <Container className="hero__image-search-container">
      <AddressInput addressInputRef={addressInputRef} />
      <CheckInInput />
      <CheckOutInput />
      <TravelsInput />
    </Container>
  )
}

export default SearchInput
