import styled from '@emotion/styled'
import { colors, shadows, sizes, transitions } from 'styles/theme'

export const DateContainer = styled.div`
  position: relative;
  width: 100%;
  font-size: 1.2rem;
  @media screen and (max-width: 375px) {
    font-size: 1rem;
  }
`
export const Label = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  height: 70px;
  margin-bottom: 0;
  padding: 0 2rem;
  font-family: var(--font-montserrat);
  font-size: 1.07rem;
  cursor: pointer;

  @media screen and (max-width: 912px) {
    padding: 0;
    border-bottom: 0.07rem solid #7c7c7c;
    font-size: 1.2rem;
  }
`

export const InputContainer = styled.div`
  flex-grow: 1;
  width: 100%;
  border-radius: 5rem;
  position: relative;
  transition: all ease 300ms;

  &:hover {
    background-color: #9d9d9d33;
    @media (max-width: 912px) {
      background-color: #ebebeb;
    }
  }

  @media (max-width: 912px) {
    & {
      width: 100%;
      border-radius: 0.8rem;
      margin: 0;
      background: var(--color-white);
    }
  }
  @media (max-width: 575px) {
    & {
      width: 100%;
    }
  }

  /* IS FOCUS */
  box-shadow: ${({ theme }) =>
    theme.isFocus ? `${shadows['horizontal']}` : 'none'};

  .react-datepicker-popper {
    transform: translate3d(-1.85rem, 47.5px, 0px) !important;

    @media screen and (max-width: 912px) {
      transform: translate3d(0px, 47.5px, 0px) !important;

      .react-datepicker__triangle {
        &::before,
        &::after {
          transform: translateX(-6rem);
        }
      }
    }
  }
`

export const Container = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 4.67rem;
  margin-top: 1.6rem;
  padding: 0;
  border-radius: 5rem;
  box-shadow: ${shadows.lg};
  color: var(--color-black);
  background-color: var(--color-white);

  & ${InputContainer}:last-of-type ${Label} {
    width: 22.47rem;
    height: auto;
    margin-top: 0;
    padding-right: 0.3rem;
    border-bottom: none;

    @media screen and (max-width: 1010px) {
      width: 18.5rem;
    }
    @media screen and (max-width: 912px) {
      width: 100%;
      margin-top: 0.3rem;
      padding-right: 0;
    }
  }
  &
    ${InputContainer}:nth-of-type(2)
    ${Label},
    &
    ${InputContainer}:nth-of-type(3)
    ${Label} {
    width: 10.7rem;
    @media screen and (max-width: 912px) {
      width: 100%;
    }
  }
  & ${InputContainer}:first-of-type ${Label} {
    width: 21.47rem;
    @media screen and (max-width: 1010px) {
      width: 18.5rem;
    }
    @media screen and (max-width: 912px) {
      width: 100%;
    }
  }
  & ${InputContainer}:nth-of-type(2n+1) ${Label} {
    @media screen and (min-width: 913px) {
      &::after {
        content: '';
        position: absolute;
        height: 3.13rem;
        width: 1px;
        background-color: ${colors.disable};
        top: 50%;
        right: 0;
        transform: translateY(-50%);
      }
    }
  }

  @media screen and (max-width: 912px) {
    & {
      flex-direction: column;
      width: 100%;
      height: auto;
      margin: 0;
      padding: 1.2rem 2.5rem 2.13rem;
      border-radius: 1.33rem;
      background-color: transparent;
      background-color: var(--color-white);
    }
  }
  @media screen and (max-width: 575px) {
    & {
      padding: 0.55rem 1.43rem 2.13rem;
    }
  }

  // @media (min-width: 0px) and (max-width: 425px) {
  //   & {
  //     // transform-origin: 0 0;
  //     // flex-direction: column;
  //     // background: transparent;
  //     position: sticky !important ;
  //   }
  // }
`

export const Title = styled.span`
  display: block;
  padding: 0;
  font-weight: 600;
  font-family: var(--font-montserrat);
  font-size: 1.2rem;
  ${
    '' /* transform: scale(1.45);
  transform-origin: left; */
  }

  @media screen and (max-width: 575px) {
    font-weight: 700;
  }
`

export const Input = styled.input`
  width: 100%;
  border: none;
  font-size: 1.07rem;
  font-weight: 500;
  text-overflow: ellipsis;
  color: var(--color-gray);
  background-color: transparent;
  outline: none;

  @media screen and (max-width: 912px) {
    font-size: 1.2rem;
    font-weight: 300;
    color: var(--color-gray);
  }

  &::placeholder {
    color: var(--color-gray);
    font-size: ${sizes['text-md']};
  }

  &[type='button'] {
    text-align: left;
    padding: 0;
  }

  /* Exec only in safary -> show placeholder datepicker*/
  @media not all and (min-resolution: 0.001dpcm) {
    @media {
      &[type='date']::before {
        content: attr(placeholder);
      }
    }
  }
`
export const InputWithIcon = styled.input`
  position: absolute;
  background-color: transparent;
  color: transparent;
  right: 0;
  top: 0;
  z-index: 999;
  border: none;
  outline: none;
  background-color: transparent;
  width: 100%;
  text-overflow: ellipsis;
  ::-moz-selection {
    /* Code for Firefox */
    color: transparent;
    background: transparent;
  }
  ::selection {
    color: transparent;
    background: transparent;
  }
  &::placeholder {
    color: transparent;
    font-size: ${sizes['text-md']};
  }

  &[type='button'] {
    text-align: left;
    padding: 0;
  }

  ::-webkit-calendar-picker-indicator {
    background: transparent;
    bottom: 0;
    color: transparent;
    cursor: pointer;
    height: auto;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    width: auto;
  }

  /* Exec only in safary -> show placeholder datepicker*/
  @media not all and (min-resolution: 0.001dpcm) {
    @media {
      &[type='date']::before {
        content: attr(placeholder);
      }
    }
  }
`

export const Pane = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: auto;
  max-height: unset;

  @media screen and (max-width: 912px) {
    flex-direction: column;
  }
`

export const Button = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  width: 4rem;
  height: 4rem;
  margin-left: 2rem;
  border-radius: 50%;
  border: none;
  font-size: 1.2rem;
  font-weight: 700;
  background-color: var(--color-secondary);
  transition: ${transitions.DEFAULT} filter;
  transform: translateX(-0.15rem);

  @media (max-width: 912px) {
    width: 100%;
    height: 3.27rem;
    margin: 1.2rem 0.33rem 0;
    border-radius: 3.87rem;
    transform: translateX(0);
  }

  &:hover {
    filter: brightness(0.9);
  }
`

export const Column = styled.div`
  text-align: start;
  width: 100%;
`

export const ModalContainer = styled.div`
  background-color: var(--color-white);
  position: absolute;
  bottom: -17rem;
  width: 100%;
  padding: 2rem;
  padding-bottom: 1rem;
  border-radius: 1rem;
  text-align: left;
  width: 350px;
  right: 0;

  @media (min-width: 0px) and (max-width: 610px) {
    & {
      // transform-origin: 0 0;
      // flex-direction: column;
      // background: transparent;
      position: sticky !important ;
    }
  }
`
