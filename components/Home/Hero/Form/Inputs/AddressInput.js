import * as React from 'react'
/* Context */
import { useSelector, useDispatch } from 'react-redux'
import { setSearchLocation } from 'redux/ducks/hebergements/search/actions'
/* Hooks */
import { useFocus } from 'hooks'
/* Styles */
import { Title, Label, InputContainer } from './styles'
/* Libraries */
import AutoComplete from 'react-google-autocomplete'
/* Cosntans */
import { zoomByPlaceTypes } from 'constants/index'

function AddressInput({ addressInputRef }) {
  const dispatch = useDispatch()
  const { isFocus, handleBlur, handleFocus } = useFocus()

  // Helpers functions
  const onSelectOption = (value) => {
    let placeType = value.address_components[0].types[0]
    const coords = {
      lat: value.geometry.location.lat(),
      lng: value.geometry.location.lng(),
      locationName: value.formatted_address,
      zoom: zoomByPlaceTypes[placeType]
    }
    dispatch(setSearchLocation(coords))
  }

  const onChange = () => {
    dispatch(
      setSearchLocation({
        locationName: null
      })
    )
  }

  const searchOptions = {
    componentRestrictions: { country: 'fr' },
    types: ['(regions)']
    // types: ['(cities)'],
  }

  return (
    <InputContainer
      className="hero-address-input algolia-input__container hero__image-search-input"
      theme={{ isFocus }}
    >
      <Label>
        <Title className="search-form__title">Adresse</Title>
        <AutoComplete
          apiKey={process.env.NEXT_PUBLIC_GOOGLE_MAPS_KEY}
          onPlaceSelected={onSelectOption}
          onChange={onChange}
          options={searchOptions}
          className="outline-0 border-0 bg-transparent p-0 search-form__autocomplete"
          placeholder="Où allez-vous ?"
        />
      </Label>
    </InputContainer>
  )
}

export default AddressInput
