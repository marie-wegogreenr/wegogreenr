import * as React from 'react'
/* Context */
import { useDispatch, useSelector } from 'react-redux'
import {
  cleanFilters,
  setTraverllersQuantity
} from 'redux/ducks/hebergements/search/actions'
import { useClickOutside } from 'hooks'
/* Styles */
import {
  Title,
  Label,
  Input,
  Button,
  Pane,
  Column,
  DateContainer,
  InputContainer
} from './styles'
import { Search } from 'components/Icons'
import ModalNumberTravelers from './ModalNumberTravelers'
import { useWindowSize } from 'hooks/useWindowSize'

function TravelsInput() {
  const {
    data: {
      searchParams: { locationName, travellers_quantity }
    },
    status
  } = useSelector((state) => state.searchedRooms)
  const [isFocus, setIsFocus] = React.useState(false)
  const [travelers, handleTravelersState] = React.useState(travellers_quantity)

  const { width } = useWindowSize()
  const [isMobile, setIsMobile] = React.useState(false)

  React.useEffect(() => {
    if (width <= 912) {
      setIsMobile(true)
    } else {
      setIsMobile(false)
    }
  }, [width])

  const dispatch = useDispatch()
  const domNode = useClickOutside(() => setIsFocus(false))

  return (
    <InputContainer
      theme={{ isFocus }}
      ref={domNode}
      className="hero__image-search-input"
    >
      <Label>
        <Pane>
          <Column>
            <Title className="search-form__title">Voyageurs</Title>
            <Input
              onClick={() => setIsFocus(!isFocus)}
              type="button"
              placeholder="Combien de voyageurs?"
              name="travellers_quantity"
              value={`${
                travellers_quantity.total == 0
                  ? 'Qui ?'
                  : travellers_quantity.total + ' Voyageurs'
              }`}
              min="0"
            />
          </Column>
          <Column className="d-flex justify-content-end">
            <Button
              disabled={!locationName}
              onClick={() => dispatch(cleanFilters())}
            >
              {isMobile && (
                <span className="hero__image-search-button-text">
                  Rechercher
                </span>
              )}
              <Search
                width="1.5rem"
                height="1.5rem"
                stroke="white"
                className="hero__image-search-button"
              />
            </Button>
          </Column>
        </Pane>
      </Label>
      <ModalNumberTravelers show={isFocus} />
    </InputContainer>
  )
}

export default TravelsInput
