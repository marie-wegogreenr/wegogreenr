import * as React from 'react'

/* Context */
import { useDispatch, useSelector } from 'react-redux'
import { setInitialDate } from 'redux/ducks/hebergements/search/actions'

/* Hooks */
import { useFocus } from 'hooks'

/* Styles */
import {
  Label,
  InputContainer,
  Input,
  InputWithIcon,
  DateContainer
} from './styles'

/* Utils */
import { formatDate } from 'utils'
import DatePicker from 'react-datepicker'
import { hasTimeToShow } from 'helpers/timeHelper'
import { CustomDatePickerSearch } from '@components/customInputs/CustomDatePickerSearch'

function CheckInInput() {
  const { initial_date, final_date } = useSelector(
    (state) => state.searchedRooms.data.searchParams
  )

  const dispatch = useDispatch()
  const { isFocus, handleBlur, handleFocus } = useFocus()

  return (
    <InputContainer theme={{ isFocus }} className="hero__image-search-input">
      <Label>
        <DateContainer>
          <DatePicker
            selected={hasTimeToShow(initial_date)}
            placeholderText="Quand ?"
            name="initial_date"
            onChange={(date) => dispatch(setInitialDate(date))}
            selectsStart
            startDate={hasTimeToShow(initial_date)}
            endDate={hasTimeToShow(final_date)}
            minDate={new Date()}
            dateFormat="dd/MM/yyyy"
            title={'Arrivée'}
            className={'map-time-input'}
            customInput={<CustomDatePickerSearch />}
          />
        </DateContainer>
      </Label>
    </InputContainer>
  )
}

export default CheckInInput
