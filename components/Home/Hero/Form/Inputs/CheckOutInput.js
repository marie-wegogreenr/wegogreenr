import * as React from 'react'

/* Context */
import { useSelector, useDispatch } from 'react-redux'
import { setFinalDate } from 'redux/ducks/hebergements/search/actions'
import DatePicker from 'react-datepicker'

/* Hooks */
import { useFocus } from 'hooks'

/* Styles */
import {
  Label,
  InputContainer,
  Input,
  InputWithIcon,
  DateContainer
} from './styles'
import { hasTimeToShow } from 'helpers/timeHelper'
import { CustomDatePickerSearch } from '@components/customInputs/CustomDatePickerSearch'

function CheckOutInput() {
  const { initial_date, final_date } = useSelector(
    (state) => state.searchedRooms.data.searchParams
  )

  const dispatch = useDispatch()
  const { isFocus, handleBlur, handleFocus } = useFocus()

  // Helpers functions
  const handleChangeDate = (e) => {
    const newDate = e.target.value
    dispatch(setFinalDate(newDate))
  }

  return (
    <InputContainer theme={{ isFocus }} className="hero__image-search-input">
      <Label>
        <DateContainer>
          <DatePicker
            placeholderText="Quand ?"
            selected={hasTimeToShow(
              initial_date > final_date ? initial_date : final_date
            )}
            name="final_date"
            onChange={(date) => dispatch(setFinalDate(date))}
            selectsEnd
            startDate={hasTimeToShow(initial_date)}
            endDate={hasTimeToShow(final_date)}
            dateFormat="dd/MM/yyyy"
            minDate={hasTimeToShow(initial_date)}
            title={'Départ'}
            className={'map-time-input'}
            customInput={<CustomDatePickerSearch />}
          />
        </DateContainer>
      </Label>
    </InputContainer>
  )
}

export default CheckOutInput
