import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Styles */
import { ModalContainer } from './styles'
import { useDispatch, useSelector } from 'react-redux'
import { setTraverllersQuantity } from '@ducks/hebergements/search/actions'
/* Fixtures */
const travellers = [
  {
    title: 'Adultes',
    text: '+ de 13 ans',
    id: 'adults'
  },
  {
    title: 'Enfants',
    text: '2 - 12 ans',
    id: 'children'
  },
  {
    title: 'Bébés',
    text: '- de 2 ans',
    id: 'babies'
  }
]

function ModalNumberTravelers({ show, handleChange }) {
  const {
    searchParams: { travellers_quantity }
  } = useSelector((state) => state.searchedRooms.data)
  // dispatch
  const dispatch = useDispatch()
  const handleTravelersCounter = (id, action) => {
    let Travelers = { ...travellers_quantity }
    Travelers[id] = action === 'plus' ? Travelers[id] + 1 : Travelers[id] - 1

    if (id === 'adults' || id === 'children') {
      Travelers['total'] =
        action === 'plus' ? Travelers['total'] + 1 : Travelers['total'] - 1
    }

    dispatch(setTraverllersQuantity(Travelers))
    if (handleChange) handleChange()
  }

  return (
    <ModalContainer style={{ zIndex: 10 }} hidden={!show}>
      {travellers.map(({ id, text, title }) => (
        <div
          key={id}
          className="d-flex justify-content-between align-items-center mb-3"
        >
          <div>
            <p className="mb-0">
              <strong>{title}</strong>
            </p>

            <p className="mb-0 text-muted">{text}</p>
          </div>

          <div>
            <button
              disabled={travellers_quantity[id] === 0}
              onClick={() => handleTravelersCounter(id, 'less')}
              className="btn btn-circle border"
              type="button"
            >
              {'-'}
            </button>
            <span className="mx-2">{travellers_quantity[id]}</span>
            <button
              onClick={() => handleTravelersCounter(id, 'plus')}
              className="btn btn-circle border"
              type="button"
            >
              {'+'}
            </button>
          </div>
        </div>
      ))}
    </ModalContainer>
  )
}

export default ModalNumberTravelers
