import * as React from 'react'
/* Next */
import { useRouter } from 'next/router'
/* Components */
import { Inputs, SearchTypes } from './Form'
import { Wrapper } from 'components'
/* Redux */
import { useSelector, useDispatch } from 'react-redux'
/* Styles */
import {
  Container,
  Content,
  Title,
  Subtitle,
  Header,
  SearchForm
} from './styles'
/* Constants */
import { routes } from 'constants/index'
import { useWindowSize } from 'hooks/useWindowSize'
import { cleanFilters } from '@ducks/hebergements/search/actions'

/* MAIN COMPONENT */
function Hero() {
  const { searchParams } = useSelector((state) => state.searchedRooms.data)
  const dispatch = useDispatch()
  // router
  const router = useRouter()

  const { width } = useWindowSize()
  const [isMobile, setIsMobile] = React.useState(false)

  React.useEffect(() => {
    if (width <= 575) {
      setIsMobile(true)
    } else {
      setIsMobile(false)
    }
  }, [width])

  // Helper functions
  const handleSubmit = (e) => {
    e.preventDefault()
    dispatch(cleanFilters())
    router.push(routes.SEARCHED_ROOMS)
  }

  return (
    <Container>
      <Wrapper>
        <Content className="py-0 py-xl-5">
          <Header className="mt-0 mt-xl-2">
            <img
              className="hero__image-img"
              loading="lazy"
              src="./images/logo/logo-wgg-phrase.png"
              alt="hero-logo-phrase"
            />
          </Header>

          <SearchForm
            onSubmit={handleSubmit}
            className="hero__image-search-form"
          >
            <SearchTypes />
            <Inputs />
          </SearchForm>
        </Content>
      </Wrapper>
    </Container>
  )
}

export default Hero
