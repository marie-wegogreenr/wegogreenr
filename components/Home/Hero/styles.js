import styled from '@emotion/styled'
import { sizes } from 'styles/theme'

export const Container = styled.section`
  background: linear-gradient(
      180deg,
      rgba(0, 0, 0, 0.63) 12.2%,
      rgba(53, 83, 77, 0) 109.19%
    ),
    url('./images/homepage/homepage_3.png');
  background-repeat: no-repeat !important;
  background-size: cover !important;
  background-position: center bottom !important;
  ${
    '' /* height: max(750px, min(85vh, calc((564, 9) / (1128, 16) * 100vw))); */
  }
  height: 86vh;
  ${'' /* height: max(820px, min(85vh, 50vw)); */}
  color: var(--color-white);
  @media (max-height: 700px) {
    height: 100vh;
  }
  @media (max-width: 1211px) {
    height: 80vh;
    @media (max-height: 700px) {
      height: 100vh;
    }
  }
  @media (max-width: 912px) {
    ${'' /* height: max(812px, min(85vh, 50vw)); */}
  }
  @media (max-width: 575px) {
    height: 100vh;
    background: linear-gradient(
        180deg,
        rgba(0, 0, 0, 0.63) 12.2%,
        rgba(53, 83, 77, 0) 109.19%
      ),
      url('./images/homepage/homepage-mobile__.jpg');
    background-position: 5% 40 !important;
  }
  @media (max-width: 390px) {
    background-size: 325% !important;
    background-position: 18.5% 50% !important;
  }
`

export const Content = styled.div`
  text-align: center;
`

export const Title = styled.h1`
  font-family: var(--font-fraunces);
  font-weight: 700;
  font-size: 48px;

  @media (max-width: 786px) {
    & {
      font-size: 3rem;
    }
  }

  @media (max-width: 405px) {
    margin-bottom: 0;
  }
`

export const Subtitle = styled.h2`
  font-family: 'Montserrat';
  font-weight: 700;
  font-size: 28px;
  line-height: 37px;

  @media (max-width: 786px) {
    & {
      font-size: 1.5rem;
    }
  }
`

export const Header = styled.div`
  padding: 10rem 0 4.33rem;

  @media (max-height: 700px) {
    padding: 6.5rem 0 4rem;
  }

  @media (max-width: 912px) {
    padding: 10rem 0 8rem;
    @media screen and (max-height: 1000px) {
      padding: 8rem 0 4rem;
    }
    @media screen and (max-height: 850px) {
      padding: 6rem 0 3rem;
    }
    @media screen and (max-height: 740px) {
      padding: 5rem 0 2rem;
    }
  }
  @media (max-width: 575px) {
    padding: 10.33rem 0 9.4rem;
    @media screen and (max-height: 800px) {
      padding: 7rem 0 6rem;
    }
    @media screen and (max-height: 669px) {
      padding: 5rem 0 4rem;
    }
  }
`

export const SearchForm = styled.form`
  @media screen and (max-width: 912px) {
    padding: 0 3rem;
  }
  @media screen and (max-width: 575px) {
    padding: 0 2.33rem;
  }
`

/* FOOTER */
export const Footer = styled.div`
  background-color: var(--color-primary);
`
