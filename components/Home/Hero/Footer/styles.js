import styled from '@emotion/styled'

export const Container = styled.div`
  background-color: var(--color-primary);
  color: var(--color-white);
`

export const Content = styled.div`
  height: 100%;
`

export const ItemList = styled.ul`
  height: 100%;
  padding: 2rem 0;
  padding-left: 0;
  margin-bottom: 0;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(280px, 1fr));
  row-gap: 1rem;
  column-gap: 1rem;

  @media screen and (max-height: 820px) {
    padding: calc(6% - 3.67rem) 0;
  }

  @media screen and (max-width: 1211px) {
    padding: 2% 0;
    grid-template-columns: auto auto;
    grid-template-rows: auto auto;
  }
  @media screen and (max-width: 767px) {
    padding: 2% 0;
    grid-template-columns: auto;
    grid-template-rows: auto auto auto auto;
  }
  @media screen and (max-width: 1187px) {
  }
  ${
    '' /* @media (min-width: 893px) and (max-width: 1187px) {
    grid-template-columns: 49% 49%;
  } */
  }
`

export const Item = styled.li`
  width: 280px;
  display: flex;
  align-items: center;
  @media (min-width: 0px) and (max-width: 1211px) {
    justify-self: center;
  }
`

export const Text = styled.p`
  margin-bottom: 0;
  margin-left: 1rem;
  font-size: 15px;
  font-family: var(--font-montserrat);
  color: var(--color-white);
  font-weight: 500;
  line-height: 20px;
`
