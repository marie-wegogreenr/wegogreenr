import * as React from 'react'
/* Components */
import { Leaf } from 'components/Icons'
import FranceMap from 'public/images/icons/franceMap.svg'
import Hours24 from 'public/images/icons/24Hours.svg'
import LabelGreenScore from 'public/images/icons/label-certification-green-host.svg'
import SecureHost from 'public/images/icons/paiement-securise-2.svg'
/* Fixtures */
import { heroFooterItems } from 'fixtures'
/* Styles */
import { Content, Item, Text, ItemList } from './styles'
import { useWindowSize } from 'hooks/useWindowSize'

function Footer() {
  /*  Custom hooks  */
  const { width } = useWindowSize()
  /*  Local states  */
  const [isMobile, setIsMobile] = React.useState(false)
  /*  Effects  */
  React.useEffect(() => {
    if (width <= 767) {
      setIsMobile(true)
    } else {
      setIsMobile(false)
    }
  }, [width])
  /*  Helper functions  */
  const setIcon = (iconName, iconProps) => {
    switch (iconName) {
      case 'france':
        return FranceMap
      case 'payment':
        return SecureHost
      case 'anulation':
        return Hours24
      case 'verify':
        return LabelGreenScore
      default:
        return null
    }
  }

  return (
    <>
      {!isMobile && (
        <div className="bg-primary footer__hero-container">
          <div className="container-xxl footer__hero-subcontainer">
            <Content>
              <ItemList>
                {React.Children.toArray(
                  heroFooterItems.map((item) => (
                    <Item>
                      <img
                        src={setIcon(item.icon)}
                        width={`${item.width || '55'}`}
                        className={
                          item.width
                            ? 'footer__hero-img--spaced'
                            : 'footer__hero-img'
                        }
                      />
                      <Text>{item.text}</Text>
                    </Item>
                  ))
                )}
              </ItemList>
            </Content>
          </div>
        </div>
      )}
    </>
  )
}

export default Footer
