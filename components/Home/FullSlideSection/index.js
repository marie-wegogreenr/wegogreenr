import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import { regionSlides } from 'fixtures'
import { Divider } from '@components/Icons'
import Slider from 'react-slick'

function FullSlide() {
  const sliderSettings = {
    dots: true,
    arrows: false,
    adaptiveHeight: true,
    dotsClass: 'slick-dots custom-slick-dots',
    responsive: [
      {
        breakpoint: 575,
        dotsClass: '',
        arrows: false
      }
    ]
  }

  return (
    <Slider {...sliderSettings} className="slider-regions-homepage">
      {regionSlides.map((review, index) => (
        <div class="custom-slider-container">
          <div class="full-slider-content">
            <span className="custom-slider-container-title">
              (Re) Visitez nos belles régions avec We Go GreenR
            </span>
            <h2 className="custom-slider-container-main-title">
              Parenthèse éco friendly en Auvergne-Rhône-Alpes
            </h2>
            <p className="full-slider-content-text">
              Entre montagnes et lacs, la région Auvergne-Rhône-Alpes possède
              une grande palette de destinations pour profiter d’un séjour où
              l’on réveil ses sens et où l’on retrouve le sens...
            </p>
            <a
              class="btn btn-secondary full-slider-content-btn"
              href={`${process.env.NEXT_PUBLIC_URL}/region-auvergne-rhone-alpes`}
            >
              Découvrir de nouveaux horizons
            </a>
          </div>
          <img src={review.image} className="full-slider-image " />
        </div>
      ))}
    </Slider>
  )
}

export default FullSlide
