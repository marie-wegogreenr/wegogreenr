import * as React from 'react'
/* Redux */
import {
  setSearchLocation,
  addEquipmentsFilter,
  searchRoomsByMapBounds
} from '@ducks/hebergements/search/actions'
import { useDispatch, useSelector } from 'react-redux'
/* Hooks */
import { useLocation } from 'hooks'
/* Next */
import Link from 'next/link'
import { useRouter } from 'next/router'
/* Constants */
import { routes, DEFAULT_FRANCE } from 'constants/index'
import photos from './photos'

function Thematic({ thematics = [] }) {
  const LABEL_YOUR_LOCATION = 'Votre emplacement'
  // redux - state
  const {
    data: {
      searchParams,
      searchParams: { lat: _lat, lng }
    }
  } = useSelector((state) => state.searchedRooms)

  if (!thematics.length) {
    return null
  }
  // hooks
  const { lon, lat } = useLocation()
  // dispatch
  const distpatch = useDispatch()
  // router
  const router = useRouter()

  const gotoMapWithTypeFilter = (value, path = 'search') => {
    if (Number.isInteger(value)) value = value.toString()
    var updateSearchParams = {
      ...searchParams,
      lat: DEFAULT_FRANCE.lat,
      lng: DEFAULT_FRANCE.lng,
      locationName: DEFAULT_FRANCE.locationName,
      zoom: DEFAULT_FRANCE.zoom,
      types: [...searchParams.types, value]
    }
    distpatch(setSearchLocation(updateSearchParams))
    distpatch(searchRoomsByMapBounds(updateSearchParams))
    router.push(path)
  }

  const showAllRoomsMap = () => {
    const coords = {
      zoom: DEFAULT_FRANCE.zoom,
      locationName: DEFAULT_FRANCE.locationName,
      lat: DEFAULT_FRANCE.lat,
      lng: DEFAULT_FRANCE.lng
    }
    distpatch(setSearchLocation(coords))
    router.push(routes.SEARCHED_ROOMS)
  }
  const targetIds = [20, 21, 15, 18, 3]

  const filter = thematics.filter((item) => targetIds.includes(item.id))

  return (
    <section className="thematic__container bg-yellow-light">
      <div className="thematic__wrapper container-xxl">
        <div className="thematic__header mb-4 d-md-flex justify-content-between align-items-center">
          <h2 className="font-fraunces color-primary size-h2">
            Nos hébergements
          </h2>

          <div className="d-flex align-items-center div-button">
            <button
              className="btn btn-outline-secondary btn-home-1 thematic__header-btn"
              onClick={() => showAllRoomsMap()}
            >
              Voir tous les hébergements
            </button>
          </div>
        </div>
        <div className="thematic__grid">
          {filter.map((thematic, index) => {
            return (
              <a
                className="thematic__item shadow"
                onClick={() =>
                  gotoMapWithTypeFilter(thematic.id, thematic.path)
                }
              >
                <figure>
                  <img
                    width="300"
                    height="300"
                    src={photos[thematic.id].image}
                  />
                </figure>

                <div className="thematic__item__content">
                  <p className="thematic__text color-white mb-0 text-center weight-bold size-h3 font-fraunces">
                    {thematic.name}
                  </p>
                </div>
              </a>
            )
          })}
        </div>
      </div>
    </section>
  )
}

export default Thematic
