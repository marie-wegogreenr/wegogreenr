import ChambreHote from 'public/images/hebergements/chambredhote.png'
import Ecolodge from 'public/images/hebergements/ecolodge.png'
import Gite from 'public/images/hebergements/Gite.png'
import Hotel from 'public/images/hebergements/Hotel-l.png'
import Insolite from 'public/images/hebergements/insolite.png'

const phothos = {
  3: {
    image: Insolite
  },
  15: {
    image: ChambreHote
  },
  18: {
    image: Ecolodge
  },
  20: {
    image: Gite
  },
  21: {
    image: Hotel
  }
}

export default phothos
