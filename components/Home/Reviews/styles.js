import styled from '@emotion/styled'

export const Container = styled.section``
export const Content = styled.div``
export const ReviewList = styled.ul``
export const Review = styled.li``
export const ReviewTitle = styled.h3``
export const ReviewDescription = styled.p``
export const Logo = styled.span``
