import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import { reviews } from 'fixtures'
import { Divider } from '@components/Icons'
import Slider from 'react-slick'

/* Styles */
import goldenQuotes from 'public/images/icons/home/gold-quotes.svg'

function Reviwes() {
  const sliderSettings = {
    // autoplay: true,
    dots: true,
    responsive: [
      {
        breakpoint: 575,
        settings: {
          arrows: false
        }
      }
    ]
  }

  return (
    <section className="reviews__container p-0 py-sm-4">
      <div className="container pt-4 pt-sm-5 pb-0 pb-sm-5 px-2 px-sm-5">
        <Slider {...sliderSettings}>
          {reviews.map((review, index) => (
            <div
              key={index}
              className={`text-center position-relative`}
              style={{ height: '300px' }}
            >
              <div
                className="carousel-home-content row"
                // style={{ bottom: '50%', transform: 'translateY(50%)' }}
              >
                <div className="col-1 col-sm-2 d-flex justify-content-end align-items-start pe-0">
                  <img
                    className="reviews__container-left-quote"
                    src={goldenQuotes}
                    width="50"
                  />
                </div>
                <div className="col-10 col-sm-8 d-flex justify-content-center">
                  {/* <h5 className="color-secondary font-fraunces h1 weight-bold">
                    {review.title}
                  </h5> */}
                  <p
                    style={{ lineHeight: 1.4, margin: '15px auto' }}
                    className="reviews__container-text font-montserrat weight-bold fz-20"
                  >
                    {review.text}
                  </p>
                </div>
                <div className="col-1 col-sm-2 d-flex justify-content-start align-items-end ps-0">
                  <img
                    className="reviews__container-right-quote"
                    src={goldenQuotes}
                    width="50"
                  />
                </div>
                <div className="col-12 text-center">
                  <img
                    className="mb-0 mt-2 mt-sm-5"
                    src={review.logo}
                    width="100"
                    style={{ margin: '0 auto' }}
                  />
                </div>
              </div>
            </div>
          ))}
        </Slider>
      </div>
    </section>
  )
}

export default Reviwes
