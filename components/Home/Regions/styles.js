import styled from '@emotion/styled'
import { sizes } from 'styles/theme'

export const Container = styled.section`
  padding: 4rem 0;
  background-color: var(--color-yellow-light);
`

export const Content = styled.div`
  & .slick-prev:before,
  & .slick-next:before {
    color: black;
  }

  @media screen and (max-width: 991px) {
    .slick-arrow {
      /* top: 50% !important; */
    }
    .slick-prev {
      transform: translate(2.73rem);
    }
    .slick-next {
      transform: translate(-2.73rem) rotate(180deg);
    }
  }
  @media screen and (max-width: 767px) {
    .slick-prev {
      transform: translate(2rem);
    }
    .slick-next {
      transform: translate(-2rem) rotate(180deg);
    }
  }
  @media screen and (max-width: 575px) {
    .slick-prev {
      transform: translate(2rem, -50%);
    }
    .slick-next {
      transform: translate(-2rem, -50%) rotate(180deg);
    }
  }
`

export const Header = styled.div`
  @media screen and (max-width: 575px) {
    position: relative;
  }
`

export const Title = styled.h1`
  font-family: var(--font-playfair);
  font-size: ${sizes['text-4xl']};
  font-weight: 700;
  color: var(--color-primary);
  line-height: 2rem;
`

export const TitleSection = styled.h2`
  font-family: var(--font-playfair);
  font-size: ${sizes['text-4xl']};
  font-weight: 700;
  color: var(--color-primary);
  line-height: 2.75rem;

  @media screen and (max-width: 575px) {
    margin: 0 0 1rem !important;
    font-size: 2rem !important;
  }
`

export const Grid = styled.div``

export const ItemList = styled.ul`
  list-style: none;
  padding-left: 0;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(500px, 1fr));
  gap: 1rem 0;

  @media (max-width: 991px) {
    padding: 0 2.2rem;
  }
  @media (max-width: 786px) {
    & {
      grid-template-columns: 1fr;
    }
  }
  @media (max-width: 575px) {
    padding: 0 3.53rem;
  }
  @media (max-width: 350px) {
    padding: 0 2rem;
  }
`

export const Item = styled.li`
  margin: 0 0.5rem;
  position: relative;
  min-height: 230px;
  height: 20vh;
  border-radius: 0.67rem;
  overflow: hidden;
  cursor: pointer;

  @media (max-width: 786px) {
    margin: 0 auto;
  }
  @media (max-width: 575px) {
    min-height: unset;
    max-height: 6.47rem;
    border-radius: 0.56rem;
  }

  &::before {
    transition: 100ms all ease-in;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    content: '';
    background: rgba(0, 0, 0, 0.2);
  }

  &:hover:before {
    background: rgba(0, 0, 0, 0.05);
  }
`

export const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`

export const City = styled.h3`
  font-family: var(--font-playfair);
  font-weight: 700;
  font-size: ${sizes['text-2xl']};
  color: var(--color-white);

  @media screen and (max-width: 575px) {
    font-size: 1.41rem !important;
    text-align: center;
  }
`

export const PlacesNumber = styled.p`
  margin-bottom: 0;
  font-family: var(--font-montserrat);
  font-size: ${sizes['text-lg']};
  color: var(--color-white);
  font-weight: var(--weight-light);
`

export const ItemContent = styled.div`
  position: absolute;
  top: 1rem;
  right: 1rem;
  left: 1rem;
  bottom: 1rem;
  color: var(--color-white);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
