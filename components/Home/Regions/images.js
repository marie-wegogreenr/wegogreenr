import Auvergne from 'public/images/regions/Auvergne.jpg'
import Bourgogne from 'public/images/regions/Bourgogne.jpg'
import Bretagne from 'public/images/regions/Bretagne.jpg'
import CentreVal from 'public/images/regions/Centre Val de Loire.png'
import Corse from 'public/images/regions/Corse.jpg'
import GrandEst from 'public/images/regions/Grand Est.jpg'
import HautDeFrance from 'public/images/regions/Haut de France.png'
import Normandie from 'public/images/regions/Normandie.jpg'
import NouvelleAquitanie from 'public/images/regions/Nouvelle-Aquitaine.jpg'
import Occtianie from 'public/images/regions/Occtianie.jpg'
import PaysDeLaLoire from 'public/images/regions/Pays de la Loire.jpg'
import Provence from 'public/images/regions/Provence.jpg'

const RegionImages = {
  1: {
    image: Auvergne
  },
  2: {
    image: Bourgogne
  },
  3: {
    image: Bretagne
  },
  4: {
    image: CentreVal
  },
  5: {
    image: Corse
  },
  6: {
    image: GrandEst
  },
  7: {
    image: HautDeFrance
  },
  8: {
    image: null
  },
  9: {
    image: Normandie
  },
  10: {
    image: NouvelleAquitanie
  },
  11: {
    image: Occtianie
  },
  12: {
    image: PaysDeLaLoire
  },
  13: {
    image: Provence
  }
}

export default RegionImages
