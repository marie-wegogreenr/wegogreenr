import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Next */
import Link from 'next/link'
import { useRouter } from 'next/router'
/* Redux */
import { getAllRegions } from '@ducks/regions/allRegions'
import { useDispatch, useSelector } from 'react-redux'
import { setSearchLocation } from 'redux/ducks/hebergements/search/actions'
/* Components */
import Slider from 'react-slick'
import RegionImages from './images'
/* Constants */
import { routes, DEFAULT_FRANCE, DEFAULT_REGION_ZOOM } from 'constants/index'
/* Styles */
import {
  City,
  Content,
  Grid,
  Header,
  Image,
  Item,
  ItemContent,
  ItemList,
  PlacesNumber,
  TitleSection
} from './styles'
import { Loader } from 'components/'
import { useWindowSize } from 'hooks/useWindowSize'

function Regions() {
  const excludedRegions = [8, 14, 15, 16, 18, 17, 19]

  // redux state
  const { allRegions } = useSelector((state) => state)
  // dispatch
  const dispatch = useDispatch()
  //router
  const router = useRouter()
  const { width } = useWindowSize()
  // computed functions
  const adjustRegions = (regionsFiltered) => {
    let array = []
    if (Array.isArray(regionsFiltered)) {
      regionsFiltered.map((item) => {
        switch (item.id) {
          case 3:
            item.lng = -2.8386
            break
          case 4:
            item.lng = 1.7853
            break
          case 12:
            item.lng = -0.823889
            break
          default:
            break
        }
        array.push(item)
      })
    }
    return array
  }
  const regions = React.useMemo(() => {
    const SIZE = 4
    let allRegionsFiltered = allRegions.data?.filter(
      (region) => !excludedRegions.includes(region.id)
    )

    const allRegionsAdjusted = adjustRegions(allRegionsFiltered)

    allRegionsAdjusted?.sort((a, b) => a.position - b.position)
    const splitedRegions =
      allRegionsAdjusted?.reduce((acc, _, i, array) => {
        if (!(i % SIZE)) {
          acc.push(array.slice(i, i + SIZE))
        }
        return acc
      }, []) || []

    return splitedRegions
  }, [allRegions.data])

  const showAllRoomsMap = () => {
    const coords = {
      zoom: DEFAULT_FRANCE.zoom,
      locationName: DEFAULT_FRANCE.locationName,
      lat: DEFAULT_FRANCE.lat,
      lng: DEFAULT_FRANCE.lng
    }
    dispatch(setSearchLocation(coords))
    router.push(routes.SEARCHED_ROOMS)
  }

  const gotoRegion = (region) => {
    const coords = {
      zoom: DEFAULT_REGION_ZOOM,
      locationName: region.lat && region.name ? region.name : 'Autre site',
      lat: region.lat || DEFAULT_FRANCE.lat,
      lng: region.lng || DEFAULT_FRANCE.lng
    }
    dispatch(setSearchLocation(coords))
    router.push(region.path)
  }

  // effects
  React.useEffect(() => {
    if (allRegions.status === 'IDLE') {
      dispatch(getAllRegions())
    }
  }, [])

  const settings = {
    responsive: [
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  }

  return (
    <div className="bg-yellow-light" style={{ marginBottom: '4rem' }}>
      <div className="container-xxl ">
        <Content>
          <div className="mb-4">
            <Header className="d-flex flex-column flex-lg-row justify-content-between align-items-center text-center">
              <TitleSection className="size-h2 m-0">
                {width < 576 ? `Par région` : `Aux quatre coins de l'hexagone`}
              </TitleSection>
              <div>
                <button
                  onClick={() => showAllRoomsMap()}
                  className="btn btn-outline-secondary btn-home-1 mt-lg-0 mt-4 homepage-regions__btn"
                  style={{ padding: '10px 20px' }}
                >
                  Voir tous les hébergements
                </button>
              </div>
            </Header>
          </div>

          {allRegions.status === 'LOADING' && <Loader />}

          {allRegions.status === 'FAILURE' && <p>Server internal error.</p>}

          {allRegions.status === 'SUCCESS' && (
            <Slider {...settings}>
              {regions.map((section, index) => (
                <Grid key={index}>
                  <ItemList>
                    {section
                      //.filter((region) => !excludedRegions.includes(region.id))
                      .map((region) => {
                        return (
                          <a key={region.id} onClick={() => gotoRegion(region)}>
                            <Item>
                              <Image
                                src={`${
                                  RegionImages[region.id]?.image ||
                                  RegionImages[1]?.image
                                }`}
                              />
                              <ItemContent>
                                <City className="size-h3 subtitle-regions">
                                  {region.name}
                                </City>
                                {/* <PlacesNumber>20 Hébergements</PlacesNumber> */}
                              </ItemContent>
                            </Item>
                          </a>
                        )
                      })}
                  </ItemList>
                </Grid>
              ))}
            </Slider>
          )}
        </Content>
      </div>
    </div>
  )
}

export default Regions
