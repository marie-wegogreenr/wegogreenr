import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import { Divider } from '@components/Icons'

function Surprised() {
  return (
    <section className="surprised__container bg-yellow-light">
      <div className="container-xxl">
        <div className="surprised__content">
          <div className="surprised__column surprised__column--body">
            <h1 className="color-primary size-h2">Je me laisse surprendre</h1>
            <Divider style={{ maxWidth: '100%' }} />
            <p className="mt-4 size-text">ENCART A VOIR PLUS TARD</p>

            <button className="btn btn-outline-secondary">
              Remplir le questionnaire
            </button>
          </div>

          <div className="surprised__column mt-4 mt-md-0">
            <div className="surprised__column--grid">
              <figure className="mb-0 rounded-3 overflow-hidden shadow">
                <img
                  height="300"
                  width="300"
                  src="https://picsum.photos/200?random=1"
                />
              </figure>
              <figure className="mb-0 rounded-3 overflow-hidden shadow">
                <img
                  height="300"
                  width="300"
                  src="https://picsum.photos/400?random=2"
                />
              </figure>
              <figure className="mb-0 rounded-3 overflow-hidden shadow">
                <img
                  height="300"
                  width="300"
                  src="https://picsum.photos/200?random=3"
                />
              </figure>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Surprised
