import * as React from 'react'
/* Styles */
import {
  Content,
  Header,
  IconContainer,
  Item,
  ItemList,
  SubtitleH3,
  Text,
  TextNoMargin,
  TitleH2
} from './styles'
import { ourSpiritItems } from 'fixtures'
import Link from 'next/link'

function OurSpirit() {
  const data = {
    title: `Des voyages qui ont du sens`,
    mainText1: `Vivre l’expérience We Go GreenR, au-delà de choisir un hébergement engagé dans une transition écologique, c’est partir à la rencontre d’hôtes passionnés et qui ont à coeur de transmettre !`,
    mainText2: `Découvrez leur savoir-faire, partagez des valeurs communes, pratiquez des activités slow, arpentez des territoires (in)connus... Goûter à la douceur de vire, naturellement !`,
    btn: `On vous en dit plus ?`
  }

  return (
    <section className="bg-yellow-light">
      <div className="our-spirit__container">
        <div className="our-spirit__content">
          <div className="our-spirit__title-wrapper">
            <h2 className="our-spirit__title-text">{data.title}</h2>
            <p className="our-spirit__main-text">{data.mainText1}</p>
            <p className="our-spirit__main-text">{data.mainText2}</p>
          </div>

          <ul className="our-spirit__list">
            {React.Children.toArray(
              ourSpiritItems.map((item) => {
                const { color, title, description, Icon } = item
                return (
                  <li>
                    <IconContainer theme={{ color: color }}>
                      <img src={Icon} className="our-spirit__list-img" />
                      {/* <Icon width="50px" height="50px" /> */}
                    </IconContainer>
                    <h3 className="our-spirit__list-subtitle">{title}</h3>
                    <p className="our-spirit__list-text">{description}</p>
                  </li>
                )
              })
            )}
          </ul>
          <Link href="/demarche">
            <button className="btn btn-secondary font-montserrat weight-semibold px-4 py-2 btn-home-1 our-spirit__content-btn">
              {data.btn}
            </button>
          </Link>
        </div>
      </div>
    </section>
  )
}

export default OurSpirit
