import styled from '@emotion/styled'
import { sizes } from 'styles/theme'

export const Container = styled.section`
  padding: 4rem 0;
  background-color: var(--color-yellow-light);
`

export const Text = styled.p`
  font-family: var(--font-montserrat);
  font-weight: 400;
  color: var(--color-white);
`

export const IconContainer = styled.div`
  background-color: ${({ theme }) => theme.color};
  width: 100px;
  height: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  margin: 0 auto;

  @media screen and (max-width: 575px) {
    width: 159px;
    height: 159px;
  }
`
