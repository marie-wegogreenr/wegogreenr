import Icon1 from 'public/images/cart-cadeau/Picto1.png'
import Icon2 from 'public/images/cart-cadeau/Picto2.png'
import Icon3 from 'public/images/cart-cadeau/Picto3.png'
import Icon4 from 'public/images/cart-cadeau/Picto4.png'

export default function CartCadeauJustification() {
  const data = {
    title1: `Pourquoi choisir la carte cadeau `,
    title2: `We Go GreenR?`,
    blocks: [
      {
        icon: Icon1,
        title: `Offrez des souvenirs`,
        text: `Enfin un cadeau utile et non matériel… L'essentiel est là, offrez la possibilité à ceux que vous aimez de se créer des moments qui n'appartiendront qu'à eux.

        En pleine nature, au bord de l'océan ou dans un petit village pittoresque, il ne tient qu'à un clic de leur faire vivre une expérience mémorable. `
      },
      {
        icon: Icon2,
        title: `Valable 2 ans`,
        text: `Elle pourra être utilisée au moment opportun. Utilisable en ligne sur l'ensemble de nos hébergements, vos proches trouveront forcément leur bonheur.`
      },
      {
        icon: Icon3,
        title: `Petit ou grand budget`,
        text: `Il y en a pour tous les porte-monnaies. Nos cartes cadeaux sont disponibles à partir de 20€. N'hésitez pas à nous contacter pour toute demande spéciale.`
      },
      {
        icon: Icon4,
        title: `(Presque) Zéro déchet`,
        text: `Nous concentrons nos efforts à minimiser l'empreinte carbone des voyageurs. Place à la carte cadeau digitale à imprimer chez vous. Personnalisez-la : Un petit mot doux, une belle enveloppe, le tour est joué.`
      }
    ]
  }

  return (
    <section className="cart-cadeu-page-justify regions-page-section">
      <h2 className="cart-cadeu-page-justify__title cart-cadeau-secondary-subtitle">
        <span className="cart-cadeu-page-justify__title-span">
          {data.title1}
        </span>
        <span className="cart-cadeu-page-justify__title-span">
          {data.title2}
        </span>
      </h2>
      <div className="cart-cadeu-page-justify__blocks">
        {data.blocks.map((item, index) => (
          <div className="cart-cadeu-page-justify__block">
            <div className="cart-cadeu-page-justify__block-wrapimg">
              <img
                src={item.icon}
                alt={`cart-cadeu-page-justify__block-img-${index}`}
                className={`cart-cadeu-page-justify__block-img${
                  index === 0
                    ? '--rectangular'
                    : index === 1
                    ? '--circular'
                    : '--square'
                }`}
              />
            </div>
            <h3 className="cart-cadeu-page-justify__block-title">
              {item.title}
            </h3>
            <p className="cart-cadeu-page-justify__block-text cart-cadeau-text">
              {item.text}
            </p>
          </div>
        ))}
      </div>
    </section>
  )
}
