import { ChevronRight } from '@components/Icons'

export default function CartCadeauActivities() {
  const data = {
    title: `Explorez We Go GreenR`,
    images: [
      {
        imgUrl: `https://cdn.wallpapersafari.com/81/8/h3jn6w.jpg`,
        text: `Explorer les hébergements`
      },
      {
        imgUrl: `http://www.wollemipine.com/images/wp_wallpaper8_800x600.jpg`,
        text: `Explorer les activités`
      }
    ]
  }

  return (
    <section className="cart-cadeau-page-activities regions-page-section">
      <h2 className="cart-cadeau-page-activities__title regions-main-subtitle">
        {data.title}
      </h2>
      <div className="cart-cadeau-page-activities__activities">
        {data.images.map((item) => (
          <div
            className="cart-cadeau-page-activities__block"
            style={{ backgroundImage: `url(${item.imgUrl})` }}
          >
            <p className="cart-cadeau-page-activities__block-text">
              {item.text}
            </p>
            <div className="cart-cadeau-page-activities__block-wrapper-icon">
              <ChevronRight className="cart-cadeau-page-activities__block-icon" />
            </div>
          </div>
        ))}
      </div>
    </section>
  )
}
