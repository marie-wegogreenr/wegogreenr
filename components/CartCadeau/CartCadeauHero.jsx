import Photo1 from 'public/images/cart-cadeau/Photo1.jpg'
import Photo2 from 'public/images/cart-cadeau/Photo2.jpg'
import Photo3 from 'public/images/cart-cadeau/Photo3.jpg'

export default function CartCadeauHero() {
  const data = {
    texts: {
      title1: `Un cadeau qui a du sens`,
      title2: null,
      text: `Offrir une carte cadeau We Go GreenR c’est offrir un moment de partage et des instants précieux où l’on prend le temps de flâner, de contempler et de savourer.\n

      Tiny house, hôtel, cabane dans les bois, écolodge, chambre d'hôtes ... Faites (re)découvrir une façon de voyager où le plaisir sera sans compromis.`,
      button: `J’offre une carte cadeau`
    },
    images: [{ imgUrl: Photo1 }, { imgUrl: Photo2 }, { imgUrl: Photo3 }]
  }

  return (
    <section className="cart-cadeu-page-hero regions-page-section">
      <div className="cart-cadeu-page-hero__wrapper-text">
        <h2 className="cart-cadeu-page-hero__wrapper-text-title cart-cadeau-main-subtitle">
          <span className="cart-cadeu-page-hero__wrapper-text-span">
            {data.texts.title1}
          </span>
          <span className="cart-cadeu-page-hero__wrapper-text-span">
            {data.texts.title2}
          </span>
        </h2>
        <p className="cart-cadeu-page-hero__wrapper-text-text cart-cadeau-text">
          {data.texts.text}
        </p>
        <a
          href="https://giftup.app/place-order/bb8da400-0391-4a67-9e76-0541aefd2ee6"
          target="_blank"
          className="btn btn-secondary cart-cadeu-page-hero__wrapper-text-button cart-cadeau-btn"
        >
          {data.texts.button}
        </a>
      </div>
      <div className="cart-cadeu-page-hero__wrapper-images">
        {data.images?.map((item, index) => (
          <img
            src={item.imgUrl}
            alt={`cart-cadeu-page-hero__wrapper-images-${index}`}
            className="cart-cadeu-page-hero__wrapper-images-img"
          />
        ))}
      </div>
    </section>
  )
}
