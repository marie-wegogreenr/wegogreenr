import { Divider } from '@components/Icons'
import Slider from 'react-slick'
import One from 'public/images/cart-cadeau/1.png'
import Two from 'public/images/cart-cadeau/2.png'
import Three from 'public/images/cart-cadeau/3.png'
import Four from 'public/images/cart-cadeau/4.png'
import Five from 'public/images/cart-cadeau/5.png'
import Six from 'public/images/cart-cadeau/6.png'
import Seven from 'public/images/cart-cadeau/7.png'

export default function CartCadeauExperience() {
  const data = {
    title: `Offrez des séjours qui ont du sens`,
    text1: `"Plutôt qu’un cadeau qui finira au fond du placard, j'ai préféré offrir à ma sœur le choix de vivre le séjour dont elle avait vraiment envie !

    Elle a pu choisir de passer un week-end dans une tente safari au bord de l’eau, un moment au vert pour déconnecter"`,
    text2: `Marie`,
    button: `J'offre un moment de bonheur`,
    images: [
      { index: 0, content: Four },
      { index: 1, content: Five },
      { index: 2, content: Six },
      { index: 3, content: Seven },
      { index: 4, content: One },
      { index: 5, content: Two },
      { index: 6, content: Three }
    ]
  }

  const settingsSlider = {
    centerMode: true,
    centerPadding: '135px',
    dots: false,
    slidesToShow: 5,
    slidesToScroll: 1,
    infinite: true,
    arrows: false,
    responsive: [
      {
        breakpoint: 1600,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          initialSlide: 1,
          centerPadding: '120px'
        }
      },
      {
        breakpoint: 748,
        settings: {
          slidesToShow: 1,
          initialSlide: 1,
          centerPadding: '100px'
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          initialSlide: 1,
          centerPadding: '50px'
        }
      },
      {
        breakpoint: 380,
        settings: {
          slidesToShow: 1,
          initialSlide: 1,
          centerPadding: '0px'
        }
      }
    ]
  }

  return (
    <section className="cart-cadeau-page-experience regions-page-section">
      <div className="cart-cadeau-page-experience__wrapper-text">
        <h2 className="cart-cadeau-page-experience__title cart-cadeau-secondary-subtitle">
          {data.title}
        </h2>
        <Divider className="cart-cadeau-page-experience__divider" />
        <p className="cart-cadeau-page-experience__text cart-cadeau-text">
          <span className="cart-cadeau-page-experience__text-span">
            {data.text1}
          </span>
          <span className="cart-cadeau-page-experience__text-span">
            {data.text2}
          </span>
        </p>
        <a
          href="https://giftup.app/place-order/bb8da400-0391-4a67-9e76-0541aefd2ee6"
          target="_blank"
          className="btn btn-secondary cart-cadeau-page-experience__btn regions-btn cart-cadeau-btn"
        >
          {data.button}
        </a>
      </div>
      <Slider {...settingsSlider}>
        {data.images.map((item, index) => (
          <div className="cart-cadeau-page-experience__images">
            <img
              src={item.content}
              alt={`cart-cadeau-page-experience__img-${index}`}
              className="cart-cadeau-page-experience__img"
            />
          </div>
        ))}
      </Slider>
    </section>
  )
}
