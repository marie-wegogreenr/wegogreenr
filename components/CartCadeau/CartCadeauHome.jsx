import Link from 'next/link'
import Gift from 'public/images/cart-cadeau/gift.png'

export default function CartCadeauHome() {
  const data = {
    imgUrl: Gift,
    title1: `Le cadeau qui`,
    title2: ` a du sens`,
    text: `Offrir une carte cadeau We Go GreenR c’est offrir un moment de partage et des instants précieux où l’on prend le temps de flâner, de contempler et de savourer...`,
    btn: `J'offre une carte cadeau`
  }

  return (
    <section className="cart-cadeu-home">
      <img src={data.imgUrl} className="cart-cadeu-home__img" />
      <div className="cart-cadeu-home__block">
        <h2 className="cart-cadeu-home__title cart-cadeau-secondary-subtitle">
          <span className="cart-cadeu-home__title-span">{data.title1}</span>
          <span className="cart-cadeu-home__title-span">{data.title2}</span>
        </h2>
        <p className="cart-cadeu-home__text cart-cadeau-text">{data.text}</p>
      </div>
      <Link href="/carte-cadeau">
        <a className="btn btn-secondary cart-cadeu-home__btn cart-cadeau-btn">
          {data.btn}
        </a>
      </Link>
    </section>
  )
}
