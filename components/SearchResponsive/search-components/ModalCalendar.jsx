import * as React from 'react'
/* Calendar */
import Calendar from 'react-calendar'
import 'react-calendar/dist/Calendar.css'
/* Redux */
import { useDispatch, useSelector } from 'react-redux'
/* actions */
import {
  setFinalDate,
  setInitialDate,
  initSearching,
  setSearchModalState
} from '@ducks/hebergements/search/actions'
/* Icons */
import { ChevronLeft } from 'components/Icons'
/* moment js*/
import moment from 'moment'

export const ModalCalendar = ({ openCalendar = false, handleOpenState }) => {
  // state - redux
  const {
    data: {
      searchParams: { initial_date, final_date }
    }
  } = useSelector((state) => state.searchedRooms)

  let countDisplayMonths = [...Array(3).keys()]
  const [isCalendarOpen, handleCalendarState] = React.useState(openCalendar)
  const [daysSelected, handleDaySelectedState] = React.useState([])
  const [monthsDisplay, handleMonthsDisplayState] =
    React.useState(countDisplayMonths)

  React.useEffect(() => {
    handleCalendarState(openCalendar)
  }, [openCalendar])

  // dispatch
  const dispatch = useDispatch()

  const addMoreMonthsDisplay = () => {
    let lastItem = monthsDisplay.length
    handleMonthsDisplayState([...monthsDisplay, lastItem])
  }

  const getMonthByItem = (item) => {
    if (item === undefined || item === null) return
    let date = new Date()
    date.setMonth(date.getMonth() + item)
    return date
  }

  const onCloseCalendar = () => {
    handleCalendarState(false)
    handleOpenState(false)
    dispatch(setSearchModalState(false))
    dispatch(initSearching())
  }

  const setDaysOnRedux = ({ minDate, maxDate }) => {
    dispatch(setInitialDate(minDate))
    dispatch(setFinalDate(maxDate))
  }

  const onChangeDay = (value) => {
    let valueString = value.toString()
    let firstDate = '',
      secondDate = '',
      minDate = '',
      maxDate = ''
    let copyDaysState = [valueString, ...daysSelected]
    if (copyDaysState.length === 3) copyDaysState.pop()
    if (copyDaysState.length === 2) {
      firstDate = safeDateGenerator(copyDaysState[0])
      secondDate = safeDateGenerator(copyDaysState[1])
      minDate = Math.min(firstDate, secondDate)
      maxDate = Math.max(firstDate, secondDate)
    }
    if (copyDaysState.length === 1) {
      firstDate = safeDateGenerator(copyDaysState[0])
      secondDate = ''
      minDate = firstDate
      maxDate = ''
    }
    setDaysOnRedux({ minDate, maxDate })
    handleDaySelectedState(copyDaysState)
  }

  const safeDateGenerator = (date) => {
    try {
      if (!date) return Date.now()
      return new Date(date)
    } catch (error) {
      return Date.now()
    }
  }

  const cleanCalendar = () => {
    handleDaySelectedState([])
    dispatch(setInitialDate(''))
    dispatch(setFinalDate(''))
    dispatch(initSearching(''))
  }
  return (
    <div
      className={`responsive-calendar__container ${
        isCalendarOpen ? '' : 'responsive-calendar__hidden'
      }`}
    >
      <div className="responsive-calendar__closeicon_container">
        <div
          className="responsive-calendar__closeicon"
          onClick={() => onCloseCalendar()}
        >
          <ChevronLeft width="20" height="20" />
        </div>
      </div>
      <div className="responsive-calendar__title-container">
        {/* <span className="responsive-calendar__title">Quand seras-tu là?</span> */}
      </div>
      {monthsDisplay.map((item, index) => (
        <div className="responsive-calendar__calendar-container" key={index}>
          <div className="responsive-calendar__month_container">
            <span>{moment(getMonthByItem(item)).format('MMM YYYY')}</span>
          </div>
          <Calendar
            onClickDay={onChangeDay}
            activeStartDate={getMonthByItem(item)}
            minDate={new Date()}
            showNavigation={false}
            returnValue="range"
            showNeighboringMonth={false}
            tileClassName={({ date, view }) => {
              let dateString = date.toString()
              if (daysSelected.some((day) => day === dateString)) {
                return 'date date-disabled'
              } else {
                return 'date'
              }
            }}
            className={`responsive-calendar__calendar-style`}
          />
        </div>
      ))}

      <button
        className="responsive-calendar__button-more-months"
        onClick={() => onCloseCalendar()}
        style={{ marginTop: '1rem', width: '50%' }}
      >
        <span className="responsive-calendar__button-more-months-text">
          Rechercher
        </span>
      </button>
      <div className="responsive-calendar__buttons-container">
        <button
          className="responsive-calendar__button-del-dates"
          onClick={() => cleanCalendar()}
        >
          <span className="responsive-calendar__button-del-dates-text">
            Effacer
          </span>
        </button>

        <button
          className="responsive-calendar__button-more-months"
          onClick={() => addMoreMonthsDisplay()}
        >
          <span className="responsive-calendar__button-more-months-text">
            Afficher plus de dates
          </span>
        </button>
      </div>
    </div>
  )
}
