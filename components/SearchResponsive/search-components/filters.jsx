import * as React from 'react'
/* Redux */
import { useDispatch, useSelector } from 'react-redux'
/* actions */
import {
  initSearching,
  cleanFilters,
  setSearchModalState,
  setSearchLocation,
  setInitialDate,
  setFinalDate,
  setTraverllersQuantity
} from '@ducks/hebergements/search/actions'
/* Icons */
import { CloseSolid, ChevronLeft } from 'components/Icons'
/* Components */

import {
  EquipmentFilter,
  ActivitiesFilter,
  TypesFilter,
  PriceFilter
} from 'components/Search'
export const ResponsiveFilters = ({ isFilterOpen = false, closeFilter }) => {
  const { searchParams } = useSelector((state) => state.searchedRooms.data)

  const [isOpen, handleFiltersState] = React.useState(isFilterOpen)
  const [filterType, handleFiltersTypeState] = React.useState('')
  const filterTypes = [
    {
      name: 'Équipements',
      label: 'equipments'
    },
    {
      name: 'A faire sur place',
      label: 'activities'
    },
    {
      name: 'Type de logement',
      label: 'types'
    },
    {
      name: 'Prix',
      label: 'max_price'
    }
  ]

  React.useEffect(() => {
    handleFiltersState(isFilterOpen)
  }, [isFilterOpen])

  // dispatch
  const dispatch = useDispatch()

  const validateCheckboxState = (name, item) => {
    if (name === 'Prix')
      return Boolean(searchParams.min_price || searchParams.max_price || false)
    if (Array.isArray(item)) return Boolean(item.length)
    return Boolean(item)
  }

  const onSelectFilter = (item) => {
    handleFiltersTypeState(item)
    if (item === 'Prix') return dispatch(setSearchModalState(false))
    dispatch(setSearchModalState(true))
  }

  const closeFilterModal = () => {
    handleFiltersState(false)
    closeFilter()
  }

  const onSaveFilters = () => {
    handleFiltersTypeState('')
    handleFiltersState(false)
    closeFilter()
    dispatch(initSearching())
  }

  const onCloseModal = () => {
    handleFiltersTypeState('')
    dispatch(setSearchModalState(false))
  }

  const clearFilters = () => {
    const coords = {
      lat: 46.543749602738565,
      lng: 2.142333984375,
      locationName: 'France',
      zoom: 6
    }
    /*  Erase form bar data  */
    dispatch(setSearchLocation(coords))
    dispatch(setInitialDate(''))
    dispatch(setFinalDate(''))
    dispatch(setTraverllersQuantity({ adults: 0, children: 0 }))
    /*  Erase filters  */
    dispatch(cleanFilters())
    dispatch(initSearching())
  }
  return (
    <div hidden={!isOpen} className={`responsive-filter__container`}>
      <div className="responsive-calendar__closeicon_container">
        <div
          className="responsive-calendar__closeicon"
          onClick={() => closeFilterModal()}
        >
          <ChevronLeft width="20" height="20" />
        </div>
        <span className="responsive-calendar__title">Filtres</span>
        <div />
      </div>
      {/* <div className="responsive-calendar__title-container">
      </div> */}
      {filterTypes.map((item, index) => (
        <div
          className="responsive-filter__filter-label-container"
          key={index}
          onClick={() => onSelectFilter(item.name)}
        >
          <span className="responsive-filter__title">{item.name}</span>
          <input
            type="checkbox"
            className="form-check-input input-check-blue-dark fs-1"
            id="conditions"
            checked={validateCheckboxState(item.name, searchParams[item.label])}
          />
        </div>
      ))}

      <div className="responsive-calendar__buttons-container">
        <button
          className="responsive-calendar__button-del-dates"
          onClick={() => clearFilters()}
        >
          <span className="responsive-calendar__button-del-dates-text">
            Effacer
          </span>
        </button>

        <button
          className="responsive-calendar__button-more-months"
          style={{ backgroundColor: filterType === 'Prix' && 'gray' }}
          onClick={() => onSaveFilters()}
          disabled={filterType === 'Prix'}
        >
          <span className="responsive-calendar__button-more-months-text">
            Sauvegarder
          </span>
        </button>
      </div>
      <ModalEquipements
        isEquipementsOpen={filterType === 'Équipements'}
        onCloseModal={onCloseModal}
      />
      <ModalActivities
        isActivitiesOpen={filterType === 'A faire sur place'}
        onCloseModal={onCloseModal}
      />
      <ModalTypes
        isTypesOpen={filterType === 'Type de logement'}
        onCloseModal={onCloseModal}
      />
      <ModalPrice
        isPricesOpen={filterType === 'Prix'}
        onCloseModal={onCloseModal}
      />
    </div>
  )
}

const ModalEquipements = ({ isEquipementsOpen, onCloseModal }) => {
  const [isOpen, handleOpenState] = React.useState(false)
  React.useEffect(() => {
    handleOpenState(isEquipementsOpen)
  }, [isEquipementsOpen])
  // dispatch
  const dispatch = useDispatch()

  const closeModal = () => {
    handleOpenState(false)
    onCloseModal()
  }
  return (
    <div hidden={!isOpen} className={`responsive-filter__container`}>
      <div className="responsive-calendar__closeicon_container">
        <div
          className="responsive-calendar__closeicon"
          onClick={() => closeModal()}
        >
          <CloseSolid width="20" height="20" />
        </div>
        <span className="responsive-calendar__title">Équipements</span>
        <div />
      </div>
      <div className="responsive-filter__filter-label-types-container">
        <EquipmentFilter />
      </div>
    </div>
  )
}

const ModalActivities = ({ isActivitiesOpen, onCloseModal }) => {
  const [isOpen, handleOpenState] = React.useState(false)
  React.useEffect(() => {
    handleOpenState(isActivitiesOpen)
  }, [isActivitiesOpen])

  const closeModal = () => {
    handleOpenState(false)
    onCloseModal()
  }
  return (
    <div hidden={!isOpen} className={`responsive-filter__container`}>
      <div className="responsive-calendar__closeicon_container">
        <div
          className="responsive-calendar__closeicon"
          onClick={() => closeModal()}
        >
          <CloseSolid width="20" height="20" />
        </div>
        <span className="responsive-calendar__title">A faire sur place</span>
        <div />
      </div>
      <div className="responsive-filter__filter-label-types-container">
        <ActivitiesFilter />
      </div>
    </div>
  )
}

const ModalTypes = ({ isTypesOpen, onCloseModal }) => {
  const [isOpen, handleOpenState] = React.useState(false)
  React.useEffect(() => {
    handleOpenState(isTypesOpen)
  }, [isTypesOpen])

  const closeModal = () => {
    handleOpenState(false)
    onCloseModal()
  }

  return (
    <div hidden={!isOpen} className={`responsive-filter__container`}>
      <div className="responsive-calendar__closeicon_container">
        <div
          className="responsive-calendar__closeicon"
          onClick={() => closeModal()}
        >
          <CloseSolid width="20" height="20" />
        </div>
        <span className="responsive-calendar__title">Type de logement</span>
        <div />
      </div>
      <div className="responsive-filter__filter-label-types-container">
        <TypesFilter />
      </div>
    </div>
  )
}

const ModalPrice = ({ isPricesOpen, onCloseModal }) => {
  const [isOpen, handleOpenState] = React.useState(false)
  React.useEffect(() => {
    handleOpenState(isPricesOpen)
  }, [isPricesOpen])

  const closeModal = () => {
    handleOpenState(false)
    onCloseModal()
  }
  return (
    <div hidden={!isOpen} className={`responsive-filter__container`}>
      <div className="responsive-calendar__closeicon_container">
        <div
          className="responsive-calendar__closeicon"
          onClick={() => closeModal()}
        >
          <CloseSolid width="20" height="20" />
        </div>
        <span className="responsive-calendar__title">Prix</span>
        <div />
      </div>
      <div className="responsive-filter__filter-label-types-container">
        <PriceFilter />
      </div>
    </div>
  )
}
