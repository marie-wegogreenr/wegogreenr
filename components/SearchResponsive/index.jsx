import * as React from 'react'
import AutoComplete from 'react-google-autocomplete'
/* Next */
import { useRouter } from 'next/router'
/* Custom hooks */
import { useClickOutside } from 'hooks'
/* Components */
import Navigation from '@components/navigation'
import ModalNumberTravelers from '@components/Home/Hero/Form/Inputs/ModalNumberTravelers'
import { ModalCalendar, ResponsiveFilters } from './search-components'
/* Transitions */
import { CSSTransition } from 'react-transition-group'
/* Helpers */
import { intervalDateFormat } from 'helpers/formatIntervalDate'

/* Constants */
import { routes, DEFAULT_CITY_ZOOM } from 'constants/index'
/* Redux */
import {
  checkFields,
  setSearchLocation,
  setFinalDate,
  setInitialDate,
  setTraverllersQuantity,
  initSearching,
  setSearchModalState
} from '@ducks/hebergements/search/actions'
import { useDispatch, useSelector } from 'react-redux'
/* Icons */
import {
  People,
  Calendar as CalendarIcon,
  CloseSolid,
  ChevronLeft,
  Filter,
  Search as SearchIcon
} from 'components/Icons'

function SearchFields({ handleSearchActiveState, onScrollTop }) {
  const {
    data: {
      searchParams: { initial_date, final_date, locationName }
    }
  } = useSelector((state) => state.searchedRooms)

  return (
    <div
      className="responsive-search__container"
      style={{
        top: '110px'
      }}
    >
      <div
        onClick={() => handleSearchActiveState(true)}
        style={{
          borderRadius: '40px',
          width: 'auto',
          borderWidth: '0',
          borderColor: 'transparent'
        }}
        className="responsive-search__input-container"
      >
        <ChevronLeft width="15" height="15" />
        <span className="responsive-search__location-name">{locationName}</span>
        <span className="responsive-search__datetime">
          {intervalDateFormat(initial_date, final_date)}
        </span>
        <div
          onClick={() => handleSearchActiveState(true)}
          className="responsive-search__search-icon"
        >
          <SearchIcon width="17" height="17" />
        </div>
        <div className="responsive-search__filter-icon">
          <Filter width="17" height="17" />
        </div>
      </div>
    </div>
  )
}

export const ResponsiveSearch = ({ onScrollTop }) => {
  // state - redux
  const {
    data: {
      searchParams: {
        initial_date,
        final_date,
        locationName,
        travellers_quantity
      }
    },
    status
  } = useSelector((state) => state.searchedRooms)

  const [isSearchActive, handleSearchActiveState] = React.useState(false)

  return (
    <>
      <Navigation />
      {isSearchActive ? (
        <ChangeSearchParams
          handleSearchActiveState={handleSearchActiveState}
          onScrollTop={onScrollTop}
        />
      ) : (
        <CSSTransition
          in={!isSearchActive}
          timeout={200}
          classNames="transition-responsive-map-search"
        >
          <SearchFields
            handleSearchActiveState={handleSearchActiveState}
            onScrollTop={onScrollTop}
          />
        </CSSTransition>
      )}
    </>
  )
}

const ChangeSearchParams = ({ handleSearchActiveState, onScrollTop }) => {
  // state - redux
  const {
    data: {
      searchParams: {
        initial_date,
        final_date,
        locationName,
        travellers_quantity
      }
    },
    status
  } = useSelector((state) => state.searchedRooms)

  const [isPlaceSelected, setIsPlacedSelected] = React.useState(true)
  const [locationValue, handleLocationState] = React.useState(locationName)
  const [showTravelers, setShowTravelers] = React.useState(false)
  const [travelers, handleTravelersState] = React.useState(travellers_quantity)
  const [isAddedTravelers, handleAddedTravelersState] = React.useState(false)
  const [isCloseModalTravelers, hadleCloseModalTravelers] = React.useState(true)
  const [openCalendar, handleCalendarState] = React.useState(false)
  const [isFilterOpen, handleFiltersState] = React.useState(false)

  const domNodeTravelers = useClickOutside(() => {
    setShowTravelers(false)
    if (isAddedTravelers && !isCloseModalTravelers) {
      handleAddedTravelersState(false)
      hadleCloseModalTravelers(true)
      dispatch(initSearching())
    }
  })
  // dispatch
  const dispatch = useDispatch()
  // Router
  const router = useRouter()

  /*  Effects */
  React.useEffect(() => {
    handleLocationState(locationName)
  }, [locationName])

  const handleChangeTravelers = (id, value) => {
    handleAddedTravelersState(true)
    hadleCloseModalTravelers(false)
  }

  const onChangeLocation = (e) => {
    handleLocationState(e.target.value)
    setIsPlacedSelected(false)
  }

  const handleOpenCalendarModal = () => {
    dispatch(setSearchModalState(true))
    handleCalendarState(true)
  }

  const closeInputs = () => {
    handleSearchActiveState(false)
    handleFiltersState(false)
  }

  const closeFilter = () => {
    handleFiltersState(false)
  }

  return (
    <div className="responsive-change-search__container">
      <div className="responsive-change-search__containeraction-icons">
        <div
          className="responsive-change-search__close-icon"
          onClick={() => closeInputs()}
        >
          <CloseSolid width="17" height="17" />
        </div>
        <div
          className="responsive-change-search__filter-icon"
          onClick={() => handleFiltersState(true)}
        >
          <Filter width="17" height="17" />
        </div>
      </div>
      <div>
        <AutoComplete
          apiKey={process.env.NEXT_PUBLIC_GOOGLE_MAPS_KEY}
          options={{
            componentRestrictions: { country: 'fr' },
            types: ['(regions)']
          }}
          onPlaceSelected={(value) => {
            setIsPlacedSelected(true)
            handleLocationState(value.formatted_address)
            const coords = {
              lat: value.geometry.location.lat(),
              lng: value.geometry.location.lng(),
              locationName: value.formatted_address,
              zoom: DEFAULT_CITY_ZOOM
            }
            dispatch(setSearchLocation(coords))
          }}
          onChange={onChangeLocation}
          defaultValue={locationName}
          value={locationValue}
          className="responsive-change-search__location-input"
          placeholder="Où allez-vous?"
        />
        <div className="responsive-search__horizontal-divider" />
        <div
          className="responsive-change-search__condition-input"
          onClick={() => handleOpenCalendarModal()}
        >
          <span className="responsive-change-search__condition-text">
            {intervalDateFormat(initial_date, final_date)}
          </span>
        </div>
        <div className="responsive-search__horizontal-divider" />
        <div
          className="responsive-change-search__condition-input"
          onClick={() => setShowTravelers(true)}
        >
          <span className="responsive-change-search__condition-text">
            {travellers_quantity.total} les hôtes
          </span>
        </div>
      </div>

      <CSSTransition
        in={isFilterOpen}
        timeout={200}
        classNames="transition-responsive-map-search"
      >
        <ModalCalendar
          openCalendar={openCalendar}
          handleOpenState={handleCalendarState}
        />
      </CSSTransition>

      <CSSTransition
        in={isFilterOpen}
        timeout={200}
        classNames="transition-responsive-map-search"
      >
        <ResponsiveFilters
          isFilterOpen={isFilterOpen}
          closeFilter={closeFilter}
        />
      </CSSTransition>

      <div
        className={`${showTravelers ? 'd-flex' : 'd-none'}`}
        ref={domNodeTravelers}
      >
        <ModalNumberTravelers
          show={showTravelers}
          fields={travelers}
          handleChange={handleChangeTravelers}
        />
      </div>
      <div
        className="responsive-search__search-icon"
        style={{ position: 'relative' }}
      >
        <span className="searh-text" style={{ margin: '0 2rem' }}>
          Rechercher
        </span>
        <SearchIcon
          width="17"
          height="17"
          style={{ position: 'absolute', right: '6%', top: '19%' }}
        />
      </div>
    </div>
  )
}
