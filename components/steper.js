import React from 'react'
import { consoleLog } from '/utils/logConsole'

import Link from 'next/link'
import Image from 'next/image'

class Steper extends React.Component {
  constructor(props) {
    super(props)

    if (this.props.selected === 1) {
      this.state = {
        circle_one: 'circle-selected',
        text_one: 'steper-text-selected',
        one: '✓',
        circle_two: 'circle',
        text_two: 'steper-text',
        two: '',
        circle_three: 'circle',
        text_three: 'steper-text',
        three: '',
        circle_four: 'circle',
        text_four: 'steper-text',
        four: '',
        circle_five: 'circle',
        text_five: 'steper-text',
        five: '',
        isShowSidebar: this.props.isShowSidebar
      }
    } else if (this.props.selected === 2) {
      this.state = {
        circle_one: 'circle-selected',
        text_one: 'steper-text',
        one: '✓',
        circle_two: 'circle-selected',
        text_two: 'steper-text-selected',
        two: '✓',
        circle_three: 'circle',
        text_three: 'steper-text',
        three: '',
        circle_four: 'circle',
        text_four: 'steper-text',
        four: '',
        circle_five: 'circle',
        text_five: 'steper-text',
        five: '',
        isShowSidebar: this.props.isShowSidebar
      }
    } else if (this.props.selected === 3) {
      this.state = {
        circle_one: 'circle-selected',
        text_one: 'steper-text',
        one: '✓',
        circle_two: 'circle-selected',
        text_two: 'steper-text',
        two: '✓',
        circle_three: 'circle-selected',
        text_three: 'steper-text-selected',
        three: '✓',
        circle_four: 'circle',
        text_four: 'steper-text',
        four: '',
        circle_five: 'circle',
        text_five: 'steper-text',
        five: '',
        isShowSidebar: this.props.isShowSidebar
      }
    } else if (this.props.selected === 4) {
      this.state = {
        circle_one: 'circle-selected',
        text_one: 'steper-text',
        one: '✓',
        circle_two: 'circle-selected',
        text_two: 'steper-text',
        two: '✓',
        circle_three: 'circle-selected',
        text_three: 'steper-text',
        three: '✓',
        circle_four: 'circle-selected',
        text_four: 'steper-text-selected',
        four: '✓',
        circle_five: 'circle',
        text_five: 'steper-text',
        five: '',
        isShowSidebar: this.props.isShowSidebar
      }
    } else if (this.props.selected === 5) {
      this.state = {
        circle_one: 'circle-selected',
        text_one: 'steper-text',
        one: '✓',
        circle_two: 'circle-selected',
        text_two: 'steper-text',
        two: '✓',
        circle_three: 'circle-selected',
        text_three: 'steper-text',
        three: '✓',
        circle_four: 'circle-selected',
        text_four: 'steper-text',
        four: '✓',
        circle_five: 'circle-selected',
        text_five: 'steper-text-selected',
        five: '✓',
        isShowSidebar: this.props.isShowSidebar
      }
    }

    this.handleSidebarShow = this.handleSidebarShow.bind(this)
  }

  handleSidebarShow() {
    this.setState({ ...this.state, isShowSidebar: 'hide-sidebar-menu' })
  }

  render() {
    return (
      <div
        className={
          'col-12 col-lg-3 background-onboarding container-onboarding-left ' +
          this.state.isShowSidebar
        }
        id="side-menu"
      >
        <div className="row">
          <div className="close-menu d-flex justify-content-end">
            <a
              href="javascript:void(0)"
              className="link-white"
              onClick={this.handleSidebarShow}
            >
              &times;
            </a>
          </div>
          <div className="col-4 mt-2">
            <Image
              src="/images/logo_white.svg"
              alt="Logo"
              width={75}
              height={85}
            />
          </div>
          <div className="col-8 d-flex justify-content-center">
            <Link href="/user/dashboard">
              <a className="link-white">Quitter et Sauvegarder</a>
            </Link>
          </div>
          <div className="step">
            <div className="step-line"></div>
            <ul>
              <li className={this.state.text_one}>
                <span className={this.state.circle_one}>{this.state.one}</span>1
                - Hôtes
              </li>
              <li className={this.state.text_two}>
                <span className={this.state.circle_two}>{this.state.two}</span>2
                - GreenScore
              </li>
              <li className={this.state.text_three}>
                <span className={this.state.circle_three}>
                  {this.state.three}
                </span>
                3 - Établissement
              </li>
              <li className={this.state.text_four}>
                <span className={this.state.circle_four}>
                  {this.state.four}
                </span>
                4 - Hébergements
              </li>
              <li className={this.state.text_five}>
                <span className={this.state.circle_five}>
                  {this.state.five}
                </span>
                5 - Activités (bientôt disponible)
              </li>
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

export default Steper
