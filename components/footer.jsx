import React, { useEffect, useState } from 'react'
import Link from 'next/link'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faFacebookF,
  faInstagram,
  faPinterestP
} from '@fortawesome/free-brands-svg-icons'
import { useToast } from '../hooks/useToast'
import NewsletterSubscribe from './Newsletter/NewsletterSubscribe'
import { LogotypeExtended } from './Icons/LogotypeExtended'
import { useRouter } from 'next/router'
import { useDispatch } from 'react-redux'
import { cleanFilters } from '@ducks/hebergements/search/actions'

const Footer = (props) => {
  const [notify] = useToast()
  const router = useRouter()
  const dispatch = useDispatch()
  const [class_footer, setClassFooter] = useState('footer')
  const [showFooter, setShowFooter] = useState(false)
  // const [email, setEmail] = useState('')

  library.add(faFacebookF, faInstagram, faPinterestP)

  useEffect(() => {
    if (props.type == 'gray') {
      setClassFooter('footer footer-gray')
      setShowFooter(true)
    } else {
      setClassFooter('footer footer-green')
      setShowFooter(true)
    }
  }, [])

  return (
    <>
      {showFooter && (
        <footer className={class_footer}>
          <div className="container-footer container-xxl">
            <NewsletterSubscribe />
            <hr className="footer-hr1" />
            <div className="row footer-secod-row">
              <div className="container-footer__info">
                <LogotypeExtended fill="white" width="150px" />
                <p className="container-footer__info-text">
                  We Go GreenR vous facilite l'accès à un tourisme plus
                  authentique et plus responsable. Pour que chaque petit geste à
                  votre échelle fasse changer le monde à grande échelle.
                </p>
                <div className="row">
                  <div className="col-12 networks-container">
                    <div className="social-network-icon-container">
                      <a
                        href="https://www.facebook.com/wegogreenr/"
                        target="_blank"
                      >
                        <FontAwesomeIcon
                          icon={['fab', 'facebook-f']}
                          size="5x"
                          className="icon facebook"
                        />
                      </a>
                    </div>
                    <div className="social-network-icon-container">
                      <a
                        href="https://www.instagram.com/wegogreenr/"
                        target="_blank"
                      >
                        <FontAwesomeIcon
                          icon={['fab', 'instagram']}
                          size="5x"
                          className="icon instagram"
                        />
                      </a>
                    </div>
                    <div className="social-network-icon-container">
                      <a
                        href="https://www.pinterest.fr/wegogreenr/"
                        target="_blank"
                      >
                        <FontAwesomeIcon
                          icon={['fab', 'pinterest-p']}
                          size="5x"
                          className="icon pinterest"
                        />
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-2 col-xl-2 menus-container">
                <h4>Services</h4>
                <ul>
                  <li>
                    <Link href="/search">
                      <a
                        onClick={(e) => {
                          e.preventDefault()
                          dispatch(cleanFilters())
                          router.push('/search')
                        }}
                      >
                        Hébergements
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/carte-cadeau">
                      <a>Carte cadeau</a>
                    </Link>
                  </li>
                  {/* <li>Activités</li>
                  <li>Carte cadeau</li> */}
                </ul>
              </div>
              <div className="col-12 col-md-2 col-xl-2 menus-container">
                <h4>Concept</h4>
                <ul>
                  {/* <li>Manifeste</li>
                  <li>Concept</li>
                  <li>Greenscore</li>
                  <li>Financement Solidaire</li>
                  <li>Projet Solidaire</li> */}
                  <li>
                    <Link href="/demarche">
                      <a>Démarche</a>
                    </Link>
                  </li>
                </ul>
              </div>
              <div className="col-12 col-md-2 col-xl-2 menus-container">
                <h4>À propos</h4>
                <ul>
                  {/* <li>Presse</li>
                  <li>Jobs</li> */}
                  <li>
                    <a
                      href="https://jobs.makesense.org/projects/we-go-greenr-3547/"
                      target="_blank"
                    >
                      Jobs
                    </a>
                  </li>
                  <li>
                    <a href="https://blog.wegogreenr.com/" target="_blank">
                      Blog
                    </a>
                  </li>
                  <li>
                    <Link href="/contacter">
                      <a>Contact</a>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
            <hr className="footer-hr2" />
            <div className="row footer-last-row">
              <div className="col-12 col-md-6 col-lg-6 col-xl-6">
                <div className="row">
                  <div className="col-12 col-md-6 col-lg-6 col-xl-6 center-row">
                    <Link href="/donnees-personnelles">
                      <a target="_blank">Politique de confidentialité</a>
                    </Link>
                  </div>
                  <div className="col-12 col-md-6 col-lg-6 col-xl-6 center-row">
                    <Link href="/conditions-generales">
                      <a target="_blank">CGU</a>
                    </Link>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-6 col-lg-6 col-xl-6 d-flex justify-content-end center-row">
                <p>{`@Wegogreenr - ${new Date().getFullYear()} - Tous droits réservés`}</p>
              </div>
            </div>
          </div>
        </footer>
      )}
    </>
  )
}

export default Footer
