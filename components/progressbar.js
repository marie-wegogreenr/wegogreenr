import React from 'react'
import { consoleLog } from '/utils/logConsole'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'

class ProgressBar extends React.Component {
  constructor(props) {
    super(props)
    library.add(faChevronRight)
    if (this.props.page == 'establissement') {
      if (this.props.step === 1) {
        this.state = {
          class_one_step: 'first_step_progress next-step',
          class_two_step: 'middle_step_progress',
          class_three_step: 'middle_step_progress',
          class_four_step: 'middle_step_progress',
          class_five_step: 'middle_step_progress',
          class_six_step: 'middle_step_progress',
          class_seven_step: 'last_step_progress'
        }
      } else if (this.props.step === 2) {
        this.state = {
          class_one_step: 'first_step_progress actual-step',
          class_two_step: 'middle_step_progress next-step',
          class_three_step: 'middle_step_progress',
          class_four_step: 'middle_step_progress',
          class_five_step: 'middle_step_progress',
          class_six_step: 'middle_step_progress',
          class_seven_step: 'last_step_progress'
        }
      } else if (this.props.step === 3) {
        this.state = {
          class_one_step: 'first_step_progress actual-step',
          class_two_step: 'middle_step_progress actual-step',
          class_three_step: 'middle_step_progress next-step',
          class_four_step: 'middle_step_progress',
          class_five_step: 'middle_step_progress',
          class_six_step: 'middle_step_progress',
          class_seven_step: 'last_step_progress'
        }
      } else if (this.props.step === 4) {
        this.state = {
          class_one_step: 'first_step_progress actual-step',
          class_two_step: 'middle_step_progress actual-step',
          class_three_step: 'middle_step_progress actual-step',
          class_four_step: 'middle_step_progress next-step',
          class_five_step: 'middle_step_progress',
          class_six_step: 'middle_step_progress',
          class_seven_step: 'last_step_progress'
        }
      } else if (this.props.step === 5) {
        this.state = {
          class_one_step: 'first_step_progress actual-step',
          class_two_step: 'middle_step_progress actual-step',
          class_three_step: 'middle_step_progress actual-step',
          class_four_step: 'middle_step_progress actual-step',
          class_five_step: 'middle_step_progress next-step',
          class_six_step: 'middle_step_progress ',
          class_seven_step: 'last_step_progress'
        }
      } else if (this.props.step === 6) {
        this.state = {
          class_one_step: 'first_step_progress actual-step',
          class_two_step: 'middle_step_progress actual-step',
          class_three_step: 'middle_step_progress actual-step',
          class_four_step: 'middle_step_progress actual-step',
          class_five_step: 'middle_step_progress actual-step',
          class_six_step: 'middle_step_progress next-step',
          class_seven_step: 'last_step_progress'
        }
      } else if (this.props.step === 7) {
        this.state = {
          class_one_step: 'first_step_progress actual-step',
          class_two_step: 'middle_step_progress actual-step',
          class_three_step: 'middle_step_progress actual-step',
          class_four_step: 'middle_step_progress actual-step',
          class_five_step: 'middle_step_progress actual-step',
          class_six_step: 'middle_step_progress actual-step',
          class_seven_step: 'last_step_progress next-step'
        }
      }
    } else {
      if (this.props.step === 1) {
        this.state = {
          class_one_step: 'first_step_progress next-step',
          class_two_step: 'middle_step_progress',
          class_three_step: 'middle_step_progress',
          class_four_step: 'middle_step_progress',
          class_five_step: 'middle_step_progress',
          class_six_step: 'last_step_progress'
        }
      } else if (this.props.step === 2) {
        this.state = {
          class_one_step: 'first_step_progress actual-step',
          class_two_step: 'middle_step_progress next-step',
          class_three_step: 'middle_step_progress',
          class_four_step: 'middle_step_progress',
          class_five_step: 'middle_step_progress',
          class_six_step: 'last_step_progress'
        }
      } else if (this.props.step === 3) {
        this.state = {
          class_one_step: 'first_step_progress actual-step',
          class_two_step: 'middle_step_progress actual-step',
          class_three_step: 'middle_step_progress next-step',
          class_four_step: 'middle_step_progress',
          class_five_step: 'middle_step_progress',
          class_six_step: 'last_step_progress'
        }
      } else if (this.props.step === 4) {
        this.state = {
          class_one_step: 'first_step_progress actual-step',
          class_two_step: 'middle_step_progress actual-step',
          class_three_step: 'middle_step_progress actual-step',
          class_four_step: 'middle_step_progress next-step',
          class_five_step: 'middle_step_progress',
          class_six_step: 'last_step_progress'
        }
      } else if (this.props.step === 5) {
        this.state = {
          class_one_step: 'first_step_progress actual-step',
          class_two_step: 'middle_step_progress actual-step',
          class_three_step: 'middle_step_progress actual-step',
          class_four_step: 'middle_step_progress actual-step',
          class_five_step: 'middle_step_progress next-step',
          class_six_step: 'last_step_progress '
        }
      } else if (this.props.step === 6) {
        this.state = {
          class_one_step: 'first_step_progress actual-step',
          class_two_step: 'middle_step_progress actual-step',
          class_three_step: 'middle_step_progress actual-step',
          class_four_step: 'middle_step_progress actual-step',
          class_five_step: 'middle_step_progress actual-step',
          class_six_step: 'last_step_progress next-step'
        }
      }
    }
  }

  render() {
    return (
      <div className="container-progressbar">
        <div className="row progressbar">
          <div className="col-12 col-sm-12 col-md-12 col-xl-5">
            <h1 className="background-white progressbar-title">
              {this.props.title}
            </h1>
          </div>
          <div className="col-12 col-sm-12 col-md-12 col-xl-7 justify-content-end d-flex progressbar-container">
            {this.props.page == 'establissement' && (
              <div>
                <div className="row justify-content-end d-flex">
                  <div className={this.state.class_one_step}></div>
                  <div className={this.state.class_two_step}></div>
                  <div className={this.state.class_three_step}></div>
                  <div className={this.state.class_four_step}></div>
                  <div className={this.state.class_five_step}></div>
                  <div className={this.state.class_six_step}></div>
                  <div className={this.state.class_seven_step}></div>
                </div>
                <div className="text-step mt-1 row">
                  <div className="col-6">
                    <p style={{ fontSize: '13px' }}>{this.props.first_text}</p>
                  </div>
                  <div className="col-6">
                    <div className="float-right text_last_step">
                      <p style={{ fontSize: '13px' }}>
                        {this.props.last_text}{' '}
                        <span>
                          {this.props.last_text !== '' && (
                            <FontAwesomeIcon
                              icon="chevron-right"
                              size="5x"
                              className="icon-progressbar"
                            />
                          )}
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            )}

            {this.props.page !== 'establissement' && (
              <div>
                <div className="row justify-content-end d-flex">
                  <div className={this.state.class_one_step}></div>
                  <div className={this.state.class_two_step}></div>
                  <div className={this.state.class_three_step}></div>
                  <div className={this.state.class_four_step}></div>
                  <div className={this.state.class_five_step}></div>
                  <div className={this.state.class_six_step}></div>
                </div>
                <div className="text-step mt-1 row">
                  <div className="col-6">
                    <p style={{ fontSize: '13px' }}>{this.props.first_text}</p>
                  </div>
                  <div className="col-6">
                    <div className="float-right text_last_step">
                      <p style={{ fontSize: '13px' }}>
                        {this.props.last_text}{' '}
                        <span>
                          {this.props.last_text !== '' && (
                            <FontAwesomeIcon
                              icon="chevron-right"
                              size="5x"
                              className="icon-progressbar"
                            />
                          )}
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    )
  }
}

export default ProgressBar
