import { useEffect, useState } from 'react'
import { useToast } from 'hooks/useToast'
import { eventListObjectTriggered } from 'helpers/dataLayer/lists'

const NewsletterForm = ({ status, message, onValidated, from }) => {
  /*  Local states  */
  const [error, setError] = useState(null)
  const [email, setEmail] = useState(null)
  const [notify] = useToast()
  /*  Effects  */
  useEffect(() => {
    if (status === 'success') {
      eventListObjectTriggered.subscribeNewsletter.callback()
      notify('Vous vous êtes inscrit avec succès à la newsletter', 'success')
    } else if (status === 'error') {
      if (message.includes('already subscribed')) {
        notify('Cet e-mail est déjà inscrit', 'error')
      } else if (message.includes('too many recent signup requests')) {
        notify('Vous avez essayé trop de fois avec le même e-mail', 'error')
      } else {
        notify('Il y a eu une erreur. Essayez à nouveau', 'error')
      }
    }
  }, [status])

  /*  Handle Functions  */
  const handleFormSubmit = (e) => {
    e.preventDefault()
    setError(null)

    if (!email) {
      setError('Please enter a valid email address')
      return null
    }

    const isFormValidated = onValidated({ EMAIL: email })
    /* On success return true */
    return email && email.indexOf('@') > -1 && isFormValidated
  }

  const handleUpdateEmail = async (event) => {
    setEmail(event.target.value)
  }

  return (
    <div className="row footer-first-row">
      {from !== 'homepage-section' && (
        <div className="col-12 col-md-6 col-lg-6 col-xl-6 mt-5 mb-5">
          <h3>Inscrivez-vous à notre newsletter</h3>
        </div>
      )}
      <div
        className={`${
          from === 'homepage-section'
            ? 'newsletter-container__newsletter-input'
            : 'col-12 col-md-6 col-lg-6 col-xl-6 justify-content-end mt-5 mb-5'
        } d-flex responsive-margin-top`}
      >
        <div className="container-inscription">
          <input
            type="text"
            className="container-inscription-input"
            value={email}
            defaultValue={email}
            onChange={handleUpdateEmail}
            placeholder="E-mail"
          />
          <button
            className={`container-inscription-button${
              status === 'sending' ? '--disabled' : ''
            }`}
            type="button"
            onClick={handleFormSubmit}
          >
            {status === 'sending' ? `En cours...` : `S'inscrire`}
          </button>
        </div>
      </div>
    </div>
  )
}

export default NewsletterForm
