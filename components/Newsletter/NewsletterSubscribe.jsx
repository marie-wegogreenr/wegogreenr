import MailchimpSubscribe from 'react-mailchimp-subscribe'
import NewsletterForm from './NewsletterForm'
import NewsletterCard from '@components/dashboard_host/compte/NewsletterCard'

const NewsletterSubscribe = ({ from }) => {
  /*  Constants */
  const MAILCHIMP_URL = `${process.env.NEXT_PUBLIC_MAILCHIMP_URL}`

  return (
    <MailchimpSubscribe
      url={MAILCHIMP_URL}
      render={(props) => {
        const { subscribe, status, message } = props || {}
        return from === 'dashboard' ? (
          <NewsletterCard
            status={status}
            message={message}
            onValidated={(formData) => subscribe(formData)}
          />
        ) : (
          <NewsletterForm
            from={from}
            status={status}
            message={message}
            onValidated={(formData) => subscribe(formData)}
          />
        )
      }}
    />
  )
}

export default NewsletterSubscribe
