import React from 'react'
import Mail from 'public/images/icons/home/newsletter-11.png'
import NewsletterSubscribe from './NewsletterSubscribe'
import { useWindowSize } from 'hooks/useWindowSize'

function NewsletterSection() {
  const { width } = useWindowSize()
  const [isMobile, setIsMobile] = React.useState(false)

  React.useEffect(() => {
    if (width <= 767) {
      setIsMobile(true)
    } else {
      setIsMobile(false)
    }
  }, [width])

  const data = {
    title: `Recevez tous les mois nos plus belles pépites green pour vous évader et (re)découvrir la France`,
    text: `Rejoignez la communauté des voyageurs responsables`
  }

  return (
    <>
      {!isMobile && (
        <section className="newsletter-container">
          <img
            src={Mail}
            className="newsletter-container__icon"
            alt="newsletter-container__icon"
          />
          <h2 className="newsletter-container__title">{data.title}</h2>
          <NewsletterSubscribe from="homepage-section" />
          <p className="newsletter-container__text">{data.text}</p>
        </section>
      )}
    </>
  )
}

export default NewsletterSection
