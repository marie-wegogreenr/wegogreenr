/* React */
import * as React from 'react'
/* Components */
import { Card } from 'components'
import Slider from 'react-slick'
import { useDispatch, useSelector } from 'react-redux'
import { getHebergementTypes } from '@ducks/hebergements/types/actions'

function CardList({
  className,
  title,
  rooms = [],
  slidesToShow = 4,
  from,
  titleClass = ''
}) {
  /*  Dispatch  */
  const dispatch = useDispatch()
  /*  Global states  */
  const { types } = useSelector((state) => state.hebergementTypes.data)
  /*  Effects  */
  React.useEffect(() => {
    if (types) {
      if (!types.length) dispatch(getHebergementTypes())
    }
  }, [])

  /*  Constants  */
  const arraySize = rooms?.length || 0
  const slides = arraySize > slidesToShow ? slidesToShow : arraySize
  const settings = {
    dots: false,
    slidesToShow: slides,
    slidesToScroll: 1,
    infinite: true,
    arrows: true,
    responsive: [
      {
        breakpoint: 1400,
        settings: {
          touchMove: true
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: slides - 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  }

  return (
    <div className={`card-list ${className}`}>
      <div className="px-0 mb-4">
        <h2
          className={`font-secondary color-primary single__title-name ${titleClass}`}
          style={{ textAlign: from === 'single' ? 'center' : 'auto' }}
        >
          {title}
        </h2>
      </div>
      <div className="px-0 card_list">
        {Boolean(rooms?.length) ? (
          <Slider {...settings}>
            {React.Children.toArray(
              rooms?.map((room) => <Card {...room} types={types} />)
            )}
          </Slider>
        ) : (
          <p>Il n'y a pas de chambres pour le moment.</p>
        )}
      </div>
    </div>
  )
}

export default CardList
