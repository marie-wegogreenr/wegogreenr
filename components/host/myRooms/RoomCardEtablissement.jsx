import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Next */
import Image from 'next/image'
import Link from 'next/link'

/* Components */
import { User } from '@components/Icons'
import { routes } from 'constants/index'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faStar as farFaStar } from '@fortawesome/free-regular-svg-icons'
import {
  faStar as fasFaStar,
  faChevronRight
} from '@fortawesome/free-solid-svg-icons'

import Cookies from 'universal-cookie'

function RoomCard({ id, name, description, from, key, principal_image = '' }) {
  library.add(farFaStar, fasFaStar, faChevronRight)

  const [showOriginalDescription, setShowOriginalDescription] =
    React.useState(true)
  const [splitDescription, setSplitDescripttion] = React.useState('')
  const [originalDescription, setOriginalDescription] =
    React.useState(description)
  const [actualDescription, setActualDescription] = React.useState('')

  React.useEffect(() => {
    const descriptionSplit = description?.slice(0, 100)
    setSplitDescripttion(descriptionSplit)
    setActualDescription(descriptionSplit)
  }, [])

  const changeDescription = async () => {
    if (showOriginalDescription == true) {
      setActualDescription(originalDescription)
      setShowOriginalDescription(false)
    } else {
      setActualDescription(splitDescription)
      setShowOriginalDescription(true)
    }
  }

  return (
    <div id={key}>
      <div className="bg-white rounded-3 overflow-hidden shadow">
        <div className="card-image" style={{ marginRight: 0 }}>
          <figure className="mb-0">
            <img
              width={100 + '%'}
              height={300}
              src={
                principal_image
                  ? 'https://wegogreenr-laravel.s3.eu-west-2.amazonaws.com/' +
                    principal_image
                  : 'https://picsum.photos/200'
              }
              className="image-hebergement"
            />
          </figure>
        </div>

        <div className="p-3 card-info" style={{ minHeight: '190px' }}>
          <div className="mb-2">
            <h2
              className="font-montserrat weight-bold h4 m-0 text-center"
              style={{ fontSize: '20px' }}
            >
              {name || 'Establishment name'}
            </h2>
          </div>
          <div>
            <p className="text-justify">{actualDescription || 'Description'}</p>
            <button onClick={() => changeDescription()}>
              See {showOriginalDescription ? 'more' : 'less'}
            </button>
          </div>

          <div className="card-modifier">
            <Link
              href={{
                pathname: routes.ADMIN_ETABLISHMENT_BY_ID,
                query: { id }
              }}
            >
              <a className="color-secondary">
                Modifier
                <FontAwesomeIcon
                  icon="chevron-right"
                  size="5x"
                  className="icon-progressbar"
                />
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default RoomCard
