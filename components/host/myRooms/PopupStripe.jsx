import React from 'react'
import { registerStripeBankData } from 'services/userServices'
import { useToast } from 'hooks/useToast'

function PopupStripe({ setShowPopup, from }) {
  /*  Custom hooks  */
  const [notify] = useToast()
  /*  Handle functions  */
  const validateUrl = (res) => {
    if (res.login_url) {
      window.open(`${res.login_url}`, '_blank')
    } else {
      window.open(`${res.url}`, '_blank')
    }
  }
  const handleClick = async () => {
    let res
    try {
      setShowPopup(false)
      res = await registerStripeBankData()
      validateUrl(res.data)
    } catch (error) {
      notify(`Il y a eu une erreur d'accès`, 'error')
    }
  }

  return (
    <div
      className={`host-room-card__popup-stripe${
        from === 'room' ? '--alternative' : ''
      }`}
    >
      <div
        class="locationPopUp__close host-room-card__popup-stripe-close"
        onClick={() => setShowPopup(false)}
      >
        X
      </div>
      <p className="host-room-card__popup-stripe-text">
        Pour publier une annonce, veuillez d'abord renseigner vos coordonnées
        bancaires en cliquant sur le bouton ci-dessous
      </p>
      <a
        className="button host-room-card__popup-stripe-link"
        onClick={handleClick}
      >
        Remplir les données
      </a>
      <div className="host-room-card__popup-stripe-triangle"></div>
    </div>
  )
}

export default PopupStripe
