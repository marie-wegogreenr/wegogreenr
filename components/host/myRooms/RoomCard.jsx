import * as React from 'react'
/* Next */
import Link from 'next/link'
/* Components */
import { routes } from 'constants/index'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faStar as farFaStar } from '@fortawesome/free-regular-svg-icons'
import {
  faStar as fasFaStar,
  faChevronRight
} from '@fortawesome/free-solid-svg-icons'

import noImage from '../../../public/images/not-image.svg'
import { updateRoomStatus } from 'services/hebergementsService'
import { useToast } from 'hooks/useToast'
import { toast } from 'react-toastify'
import PopupStripe from './PopupStripe'
import { modifyOutstandingRoom } from 'services/roomServicesService'

function RoomCard({
  internalName,
  peopleCapacity = '1',
  basicPrice = '0',
  id,
  active,
  outstanding,
  principal_image,
  from,
  key,
  slug,
  type_id
}) {
  library.add(farFaStar, fasFaStar, faChevronRight)
  /*  Local states  */
  const [fields, setFields] = React.useState({
    statusActive: active,
    outstanding
  })
  const [notify] = useToast()
  const [showPopup, setShowPopup] = React.useState(false)

  /*  Handle functions  */
  const outstandingHebergement = async () => {
    if (from == 'admin') {
      setFields({ ...fields, outstanding: !fields.outstanding })

      try {
        const response = await modifyOutstandingRoom(id)

        if (
          response.code == 200 &&
          response.data.message === 'Room modified successfuly'
        ) {
          notify('Changement réussi', 'success')
        } else {
          notify('Il y a eu un problème, veuillez réessayer plus tard', 'error')
        }
      } catch (error) {
        notify('Il y a eu un problème, veuillez réessayer plus tard', 'error')
      }
    }
  }

  const changeStatus = async () => {
    if (!type_id) {
      notify(`Vous devez sélectionner le type d'hébergement`, 'error')
      return
    }
    setFields({ ...fields, statusActive: !fields.statusActive })
    const { Message, fails } = await updateRoomStatus(id)
    if (Message === 'You must insert minimum fields for activation') {
      setShowPopup(fails.stripe === null || fails.stripe === '' ? true : false)

      toast.warning(
        <div>
          vous devez effectuer les actions suivantes afin d'activer cet espace:
          <br />
          <br />
          {`${fails.name === null ? '-  ajouter un nom public' : ''}`}
          {fails.name === null && <br />}
          {`${fails.price === null ? '- ajouter le prix' : ''}`}
          {fails.price === null && <br />}
          {`${
            fails.stripe === null || fails.stripe === ''
              ? '-  associez vos coordonnées bancaires à wegogreenr'
              : ''
          }`}
          {fails.images < 3 && <br />}
          {`${
            fails.images < 3 ? '-  ajoutez au moins 3 images à cette pièce' : ''
          }`}
        </div>,
        {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 8000
        }
      )
      setFields({ ...fields, statusActive: 0 })
    } else {
      notify('changement réussi', 'success')
    }
  }

  return (
    <div id={key} className="w-100 host-room-card">
      <div className="bg-white rounded-3 overflow-hidden shadow w-100">
        <div className="card-image w-100" style={{ marginRight: 0 }}>
          <img
            height={250}
            src={
              principal_image
                ? process.env.NEXT_PUBLIC_AMAZON_IMGS + principal_image
                : noImage
            }
            className="image-hebergement w-100"
          />
          {from == 'admin' && (
            <FontAwesomeIcon
              icon={fields.outstanding == 0 ? ['far', 'star'] : ['fas', 'star']}
              className="icon-star"
              data-toggle="tooltip"
              data-placement="right"
              title={
                fields.outstanding == 0
                  ? 'non mis en évidence'
                  : 'mis en évidence'
              }
              onClick={outstandingHebergement}
            />
          )}
        </div>

        <div
          className="p-3 card-info my-rooms__card-info w-100"
          style={{ minHeight: '220px' }}
        >
          {/*{from == 'host' ? (*/}
          <>
            <div className="mb-2">
              <h2
                className="font-montserrat weight-bold h4 m-0 text-center my-rooms__card-info-title"
                style={{ fontSize: '20px' }}
              >
                {internalName || 'Room name'}
              </h2>
            </div>
            <div className="d-flex align-items-center">
              <img src="/images/user_icon.svg" alt="user icon" />
              <p className="mb-0 ml-2">
                Personnes: <span>{peopleCapacity || '-'}</span>
              </p>
            </div>

            <div className="d-flex justify-content-between align-items-end mb-2">
              <div className="my-2">
                <p className="d-flex flex-column font-montserrat m-0">
                  <span>A partir de</span>
                  <span>
                    <strong style={{ fontSize: '20px' }}>
                      {basicPrice !== null
                        ? basicPrice?.toString().replace('.', ',')
                        : 0}
                      €
                    </strong>
                    /nuit
                  </span>
                </p>
              </div>
              <div className="card-modifier card-modifier--flex">
                <Link
                  href={{
                    pathname:
                      from == 'admin'
                        ? routes.ADMIN_MY_HEBERGEMENT_BY_ID
                        : routes.HOST_MY_HEBERGEMENT_BY_ID,
                    query: { id }
                  }}
                >
                  <a className="color-secondary">
                    Modifier
                    <FontAwesomeIcon
                      icon="chevron-right"
                      size="5x"
                      className="icon-progressbar"
                    />
                  </a>
                </Link>
                <Link href={`mes-hebergements/preview/${slug}`}>
                  <a className="color-secondary" target="_blank">
                    Visualisez
                    <FontAwesomeIcon
                      icon="chevron-right"
                      size="5x"
                      className="icon-progressbar"
                    />
                  </a>
                </Link>
              </div>
            </div>
          </>

          <div className="border-top py-2">
            <p className="text-center mb-0 d-flex align-items-center justify-content-center">
              <div className="custom-control custom-switch">
                <input
                  type="checkbox"
                  className="custom-control-input"
                  id={id}
                  checked={fields.statusActive}
                  onClick={changeStatus}
                />
                <label
                  className="custom-control-label ml-2"
                  htmlFor={id}
                  style={{ cursor: 'pointer' }}
                >
                  {fields.statusActive ? 'Publiée' : 'Non Publiée'}
                </label>
              </div>
            </p>
          </div>
        </div>
      </div>

      {showPopup ? <PopupStripe setShowPopup={setShowPopup} /> : null}
    </div>
  )
}

export default RoomCard
