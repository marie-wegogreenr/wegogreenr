import * as React from 'react'
/* Components */
import { ReservationRow, ReservationColumn } from '.'
import { dateFromDb } from 'helpers/timeHelper'
import { useWindowSize } from 'hooks/useWindowSize'

function Table({ reservations }) {
  const items = [
    { title: 'Hébergements ', center: false },
    { title: 'Voyageurs', center: true },
    { title: 'Durée', center: true },
    { title: 'Prix total', center: true },
    { title: 'Dates', center: true },
    { title: 'Statut', center: true },
    { title: '', center: true }
  ]

  const filteredByDate = reservations.sort(
    (a, b) =>
      Math.abs(Date.now() - new Date(a.arrival_date)) -
      Math.abs(Date.now() - new Date(b.arrival_date))
  )

  const [isMobile, setIsMobile] = React.useState(false)
  const { width } = useWindowSize()

  const validateIsMobile = () => {
    if (width < 1200) {
      setIsMobile(true)
    } else {
      setIsMobile(false)
    }
  }

  React.useEffect(() => {
    validateIsMobile()
  }, [width])

  return filteredByDate.length > 0 ? (
    isMobile ? (
      <>
        {filteredByDate.map((reservation, index) => {
          return (
            <>
              <table className="table__vertical" key={`table-column-${index}`}>
                <ReservationColumn
                  headings={items}
                  reservation={reservation}
                  position={index}
                />
              </table>
            </>
          )
        })}
      </>
    ) : (
      <table className="table">
        <thead>
          <tr>
            {items.map((item, index) => (
              <th
                className={`border-0 py-4 align-middle no-wrap weight-regular ${
                  item.center ? 'text-center' : ''
                }`}
                scope="col"
                key={`table-head-${index}`}
              >
                {item.title}
              </th>
            ))}
          </tr>
        </thead>
        <tbody className="bg-white rounded-3">
          {filteredByDate.map((reservation, index) => {
            const {
              id,
              number_of_adults,
              arrival_date,
              departure_date,
              status,
              room_price,
              room,
              created_at
            } = reservation
            return (
              <ReservationRow
                key={id}
                id={id}
                name={room.public_name}
                numberTravelers={number_of_adults}
                arriveDate={arrival_date}
                departDate={departure_date}
                status={status}
                price={room_price}
                reservation={reservation}
                position={index}
                hebergementSize={reservations.length - 1}
                created_at={created_at}
              />
            )
          })}
        </tbody>
      </table>
    )
  ) : (
    <div className="w-100 text-left py-3 sm-py-5 mt-5">
      <p className="subtitle-dashboard weight-bold font-fraunces">
        Vous n'avez pas encore de réservation
      </p>
    </div>
  )
}

export default Table
