import React, { useState } from 'react'
import { formatDate, getDaysAndWeekends } from 'utils'
import ReservationRowModal from './ReservationRowModal'
import moment from 'moment'
import { DropdownReservationHost } from '@components/dropdown/DropdownReservationHost'
import { getBackgroundStyle, getStatusMessage } from 'helpers/utilsHelper'

function getStatus(number) {
  switch (number) {
    case 0:
      return { text: 'En attente de paiement', color: 'primary' }

    case 1:
      return { text: 'Payée', color: 'success' }

    case 2:
      return { text: 'Annulées', color: 'warning' }
    case 4:
      return { text: 'Annulées', color: 'warning' }
    case 5:
      return { text: 'Annulées', color: 'warning' }
    default:
      return { text: 'En attente de paiement', color: 'primary' }
  }
}

/* MAIN COMPONENT */
function Row({
  numberTravelers,
  arriveDate,
  departDate,
  status,
  id,
  price,
  name,
  reservation,
  position,
  hebergementSize,
  created_at
}) {
  const [modal, setModal] = useState(false)
  const [modalOpen, setModalOpen] = React.useState(false)

  const [weekdays, weekends] = getDaysAndWeekends(arriveDate, departDate)

  let thClass
  if (position == 0) {
    thClass = 'align-middle text-center td_mes_reservations td_border_top'
  } else if (hebergementSize == position) {
    thClass = 'align-middle text-center td_mes_reservations td_border_bottom'
  } else {
    thClass = 'align-middle text-center td_mes_reservations '
  }
  return (
    <tr className="td_table">
      <td className={thClass}>
        <div className="d-flex align-items-center td_table__main-wrapper">
          <figure className="mr-3 mb-0 rounded-3 overflow-hidden shadow td_table__main-wrapper-img">
            <img
              style={{ objectFit: 'cover' }}
              width="120"
              height="90"
              src={
                reservation.principal_image == null
                  ? 'https://picsum.photos/200'
                  : 'https://wegogreenr-laravel.s3.eu-west-2.amazonaws.com/' +
                    reservation.principal_image
              }
              alt={
                reservation.principal_image
                  ? `${reservation.principal_image}`
                  : `reservation-main-image`
              }
              className="td_table__main-wrapper-image"
            />
          </figure>
          <p className="m-0 td_table__main-wrapper-text">
            <strong>{name}</strong>
          </p>
        </div>
      </td>

      <td className={thClass}>
        <p className="m-0">{numberTravelers} Pers.</p>
      </td>

      <td className={thClass}>
        <p className="m-0 no-wrap">{weekdays + weekends} Nuits</p>
      </td>

      <td className={`${thClass} td_table__price-text`}>
        <p className="m-0">{price?.toString().replace('.', ',')} € </p>
      </td>

      <td className={thClass}>
        <p className="m-0 no-wrap">
          {/* {formatDate(new Date(arriveDate))} -{' '}
          {formatDate(new Date(departDate))} */}
          {`${moment(arriveDate).format('DD/MM/YYYY')} - ${moment(
            departDate
          ).format('DD/MM/YYYY')}`}
        </p>
      </td>

      <td className={thClass}>
        <span
          className={`custom-badge badge badge-color-${getBackgroundStyle(
            reservation.status
          )} mb-0`}
        >
          {getStatusMessage(reservation.status)}
        </span>
      </td>
      <td className={thClass}>
        <div className="col-12 col-md-2 p-0 m-0">
          <DropdownReservationHost
            openModal={setModalOpen}
            reservation={reservation}
          />
          {modalOpen && (
            <ReservationRowModal
              setModal={setModalOpen}
              reservation={reservation}
              getStatus={getStatus}
            />
          )}
        </div>
      </td>
    </tr>
  )
}

export default Row
