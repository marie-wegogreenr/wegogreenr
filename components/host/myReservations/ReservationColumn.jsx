import { DropdownReservationHost } from '@components/dropdown/DropdownReservationHost'
import { formatDate, getDaysAndWeekends } from 'helpers/timeHelper'
import { getBackgroundStyle, getStatusMessage } from 'helpers/utilsHelper'
import React, { useState } from 'react'
import ReservationRowModal from './ReservationRowModal'

function ReservationColumn({ headings, reservation, position }) {
  const [modalOpen, setModalOpen] = React.useState(false)
  const {
    id,
    number_of_adults,
    arrival_date,
    departure_date,
    status,
    room_price,
    room,
    created_at
  } = reservation

  const [weekdays, weekends] = getDaysAndWeekends(arrival_date, departure_date)

  const [modal, setModal] = useState(false)

  function getStatus(number) {
    switch (number) {
      case 0:
        return { text: 'En attente de paiement', color: 'primary' }
      case 1:
        return { text: 'Payée', color: 'success' }
      case 2:
        return { text: 'Annulées', color: 'warning' }
      case 4:
        return { text: 'Annulées', color: 'warning' }
      case 5:
        return { text: 'Annulées', color: 'warning' }
      default:
        return { text: 'En attente de paiement', color: 'primary' }
    }
  }

  return (
    <>
      <tr className="table__vertical-row">
        <th className="table__vertical-th">{headings[0].title}</th>
        <td className="table__vertical-td">
          <div className="d-flex flex-column justify-content-center">
            <p className="table__vertical-td-text table__vertical-text--small">
              <strong>{room.public_name}</strong>
            </p>
            <img
              style={{ objectFit: 'cover' }}
              className="mr-3 mb-0 rounded-3 overflow-hidden shadow table__vertical-td-img"
              width="150"
              height="90"
              src={
                reservation.principal_image == null
                  ? 'https://picsum.photos/200'
                  : 'https://wegogreenr-laravel.s3.eu-west-2.amazonaws.com/' +
                    reservation.principal_image
              }
            />
          </div>
        </td>
      </tr>

      <tr className="table__vertical-row">
        <th className="table__vertical-th">{headings[1].title}</th>
        <td className="table__vertical-td">
          <p className="table__vertical-text--small">
            {number_of_adults} Pers.
          </p>
        </td>
      </tr>

      <tr className="table__vertical-row">
        <th className="table__vertical-th">{headings[2].title}</th>
        <td className="table__vertical-td">
          <p className="table__vertical-text--small">
            {weekdays + weekends} Nuits
          </p>
        </td>
      </tr>

      <tr className="table__vertical-row">
        <th className="table__vertical-th">{headings[3].title}</th>
        <td className="table__vertical-td">
          <p className="table__vertical-text--small">
            {room_price?.toString().replace('.', ',')} €
          </p>
        </td>
      </tr>

      <tr className="table__vertical-row">
        <th className="table__vertical-th">{headings[4].title}</th>
        <td className="table__vertical-td">
          <p className="table__vertical-text--small">
            {formatDate(new Date(arrival_date))} -{' '}
            {formatDate(new Date(departure_date))}
          </p>
        </td>
      </tr>

      <tr className="table__vertical-row">
        <th className="table__vertical-th">{headings[5].title}</th>
        <td className="table__vertical-td">
          <span
            className={`custom-badge badge badge-color-${getBackgroundStyle(
              reservation.status
            )} mb-0`}
          >
            {getStatusMessage(reservation.status)}
          </span>
        </td>
      </tr>

      <tr className="table__vertical-row">
        <th className="table__vertical-th"></th>
        <td className="table__vertical-td">
          <div className="col-12 col-md-2 p-0">
            <DropdownReservationHost
              openModal={setModal}
              reservation={reservation}
            />
            {modal && (
              <ReservationRowModal
                setModal={setModal}
                reservation={reservation}
                getStatus={getStatus}
              />
            )}
          </div>
        </td>
      </tr>
    </>
  )
}

export default ReservationColumn
