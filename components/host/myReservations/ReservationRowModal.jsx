import React, { useRef, useState } from 'react'
//HOOKS
import { useClickOutside } from 'hooks'
//UTILS
import moment from 'moment'
import { adjustNumberWithComma, getStatusMessage } from 'helpers/utilsHelper'
import { Warning } from '@components/Icons'
import { calcelReservationDays } from 'helpers/ReservationHelper'
import CancelReservation from '@components/dashboard_user/reservations/LocationPopUp/CancelReservation'

const ReservationRowModal = ({ setModal, reservation, getStatus }) => {
  let domNode = useClickOutside(() => {
    setModal(false)
  })

  let {
    id,
    code,
    number_of_adults,
    number_of_children,
    arrival_date,
    departure_date,
    status,
    payment,
    room_price,
    room: {
      public_name,
      establishment: { cancel_max_days }
    },
    created_at
  } = reservation

  const [iscanceled, setIscanceled] = useState(
    getStatusMessage(status) === 'Annulée'
  )
  const [cancelRes, setCancelRes] = useState(false)
  const canCancelReservation = calcelReservationDays(
    cancel_max_days,
    arrival_date
  )

  return (
    <div className="locationPopUp" ref={domNode}>
      <div className="locationPopUp__close" onClick={() => setModal(false)}>
        X
      </div>
      <div className="wrapper-8">
        <div className="text-left">
          <h2 className="h2-dashboard font-montserrat">
            Détail de la réservation
          </h2>
          <img
            src={`${process.env.NEXT_PUBLIC_AMAZON_IMGS}${reservation.principal_image}`}
            alt=""
            height="250"
            className="w-100 obj-fit-cover"
          />
          <p className="subtitle-dashboard weight-bold my-4">{public_name}</p>
        </div>
        <div className="border-bottom"></div>
        <div className="d-flex justify-content-between mt-4">
          <p className="weight-bold paragraph">ID de la réservation</p>
          <p className="paragraph">{code}</p>
        </div>
        <div className="d-flex justify-content-between">
          <p className="weight-bold paragraph">Réservation faite le</p>
          <p className="paragraph">
            {moment(created_at).format('dddd, Do MMMM  YYYY')}
          </p>
        </div>
        <div className="d-flex justify-content-between">
          <p className="weight-bold paragraph">Voyageurs</p>
          <p className=" paragraph">
            {`${number_of_adults} ${
              number_of_adults === 0 || number_of_adults > 1
                ? 'adultes'
                : 'adult'
            } , ${number_of_children} ${
              number_of_children === 0 || number_of_children > 1
                ? 'enfants'
                : 'enfant'
            }`}
          </p>
        </div>
        <div className="d-flex justify-content-between">
          <p className="weight-bold paragraph">Dates &amp; heures </p>
          <p className="paragraph"></p>
        </div>
        <div className="d-flex justify-content-between">
          <div className="w-50 mr-3">
            <div className="locationPopUp__card text-center d-flex justify-content-center flex-column">
              <b className="text-block weight-bold">Arrivée</b>
              <p className="paragraph">
                {moment(arrival_date).format('dddd, Do MMMM  YYYY')}
              </p>
            </div>
          </div>
          <div className="w-50">
            <div className="locationPopUp__card text-center d-flex justify-content-center flex-column">
              <b className="text-block weight-bold">Départ</b>
              <p className="paragraph">
                {moment(departure_date).format('dddd, Do MMMM  YYYY')}
              </p>
            </div>
          </div>
        </div>
        <div className="border-bottom mt-3"></div>
        <div className="d-flex justify-content-between mt-4">
          <p className="weight-bold paragraph">Statut </p>
          <p className="paragraph">{getStatusMessage(status)}</p>
        </div>
        <p className="subtitle-dashboard weight-bold mt-5">
          Informations de paiement
        </p>
        <div className="d-flex justify-content-between mt-4 mb-5">
          <p className="weight-bold paragraph">Total </p>
          <p className="paragraph">
            {adjustNumberWithComma(room_price)} &euro;
          </p>
        </div>
        {!iscanceled && (
          <div className="d-flex justify-content-between align-items-center bg-gray_lighter p-3 my-5 ">
            {!cancelRes && (
              <span className="d-flex align-items-center">
                <Warning width={25} />
                <p className="weight-bold m-0 ml-3 text">
                  Réservation annulée?
                </p>
              </span>
            )}

            <CancelReservation
              setCancelRes={setCancelRes}
              reservationID={id}
              canCancel={canCancelReservation}
              cancelDays={cancel_max_days}
              setIscanceled={setIscanceled}
              type={'host'}
            />
          </div>
        )}
      </div>
    </div>
  )
}

export default ReservationRowModal
