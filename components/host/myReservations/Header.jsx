import * as React from 'react'
/* Components */
import { Search } from '@components/Icons'

const filters = [
  {
    text: 'Toutes',
    id: 'toutes'
  },
  {
    text: 'A venir',
    id: 'en_ligne'
  },
  {
    text: 'Passées',
    id: 'passées'
  },
  {
    text: 'Annulées',
    id: 'annulé'
  }
]

function Header({ filterBy, setFilterBy }) {
  const handleChangeFilter = (id) => {
    setFilterBy(id)
  }

  return (
    <header className="d-flex justify-content-between align-items-center pb-4 pt-2 flex-wrap">
      <h1 className="color-primary title mes-reservations__title">
        Mes réservations
      </h1>
      <div className="d-flex align-items-center mes-reservations__header">
        <div className="rounded-pill bg-white border w-100">
          <ul className="mes-reservations__tab-list mb-0">
            {filters.map(({ id, text }) => (
              <li key={id}>
                <button
                  onClick={() => handleChangeFilter(id)}
                  className={` py-2 rounded-pill border-0 mes-reservations__tab-button ${
                    filterBy === id ? 'btn-secondary' : 'bg-transparent'
                  }`}
                >
                  <span className="mes-reservations__tab-button__text">
                    {text}
                  </span>
                </button>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </header>
  )
}

export default Header
