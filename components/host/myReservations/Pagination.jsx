import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

function Pagination({ max = 0, nextPage, currentPage, prevPage }) {
  return (
    <nav className="my-4">
      <div className="d-flex align-items-center justify-content-center">
        <button
          disabled={currentPage <= 0}
          onClick={prevPage}
          type="button"
          style={{ width: '3rem', height: '3rem' }}
          className="border-0 mx-2 rounded-circle bg-white text-center p-0"
        >
          <span className="host__pagination-left-btn">‹</span>
        </button>

        <span style={{ padding: '0px 10px', fontSize: '20px' }}>
          {currentPage + 1 || 1}
        </span>

        <span style={{ padding: '0px 10px', fontSize: '20px' }}> / </span>

        <span style={{ padding: '0px 10px', fontSize: '20px' }}>
          {max || 1}
        </span>

        <button
          disabled={currentPage + 1 >= max}
          onClick={nextPage}
          type="button"
          style={{ width: '3rem', height: '3rem' }}
          className="border-0 mx-2 rounded-circle bg-white text-center p-0"
        >
          <span className="host__pagination-right-btn">›</span>
        </button>
      </div>
    </nav>
  )
}

export default Pagination
