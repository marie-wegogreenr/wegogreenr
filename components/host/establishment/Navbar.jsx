import * as React from 'react'

/* Fixtures */
import { hostEstablishmentTabs as tabs } from 'fixtures'

function Navbar() {
  return (
    <nav className="py-1">
      <ul
        className="nav nav-pills establishment-tab tab__container tab__scrollbar"
        id="pills-tab"
        role="tablist"
      >
        {tabs.map((item, index) => (
          <li
            key={item.id}
            className="nav-item text-nowrap"
            role="presentation"
          >
            <button
              className={`nav-link ${!index ? 'active' : ''}`}
              id={`pill-${item.id}-tab`}
              data-bs-toggle="pill"
              data-bs-target={`#pills-${item.id}`}
              type="button"
              role="tab"
              aria-controls={item.id}
              aria-selected="true"
            >
              {item.tabText}
            </button>
          </li>
        ))}
      </ul>
    </nav>
  )
}

export default Navbar
