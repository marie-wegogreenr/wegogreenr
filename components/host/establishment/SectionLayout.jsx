import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

function SectionLayout({
  children,
  title,
  titleAlternative,
  fromGreenscore,
  onModify,
  isModifyMode = false,
  hideModifyButton = false
}) {
  return (
    /* CONTAINER */
    <section className="bg-white mt-4 rounded-3">
      {/* CONTENT */}
      <div className={fromGreenscore ? 'py-4' : 'p-4'}>
        {/* HEADER */}
        <div
          className={
            titleAlternative
              ? 'form-room__question-wrapper'
              : 'd-flex justify-content-between align-items-center mb-4'
          }
        >
          {titleAlternative ? (
            <h3 className="mb-0 font-montserrat form-room__prix-title">
              {titleAlternative}
            </h3>
          ) : (
            <h3 className="mb-0 font-montserrat">{title}</h3>
          )}
          {!hideModifyButton && (
            <div className="form-room__question-edit">
              {isModifyMode ? (
                <button
                  type="button"
                  onClick={onModify}
                  className="btn btn-outline-gray border-0 d-flex justify-content-between align-items-center"
                >
                  <span className="mr-2">x</span>
                  <u>Annuler</u>
                </button>
              ) : (
                <button
                  type="button"
                  onClick={onModify}
                  className="btn btn-outline-secondary border-0"
                >
                  <u>Editer</u>
                </button>
              )}
            </div>
          )}
        </div>

        {/* BODY */}
        <div className="">{children}</div>
      </div>
    </section>
  )
}

export default SectionLayout
