import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Component */
import { Breakfast, OtherService, Languages } from './ServiceSections'

/* Redux */
import { getAllBreakfastTypes } from '@ducks/host/allBreakfastTypes'
import { getAllLanguages } from '@ducks/host/allLanguages'
import { getAllServices } from '@ducks/host/allServices'
import { getEstablishmentLanguagesById } from '@ducks/host/establishmentLanguagesById'
import { getEstablishmentServicesById } from '@ducks/host/establishmentServicesById'
import { useDispatch, useSelector } from 'react-redux'

function Services() {
  // redux
  const dispatch = useDispatch()
  const {
    allBreakfastTypes,
    allLanguages,
    allServices,
    establishment,
    establishmentLanguagesById,
    establishmentServicesById
  } = useSelector((state) => state)
  const establishmentId = establishment.data?.id
  const [commonServices, setCommonServices] = React.useState([])

  // effects
  React.useEffect(() => {
    if (allBreakfastTypes.status === 'IDLE') {
      dispatch(getAllBreakfastTypes())
    }

    if (allServices.status === 'IDLE') {
      dispatch(getAllServices())
    }

    if (allLanguages.status === 'IDLE') {
      dispatch(getAllLanguages())
    }
  }, [])

  React.useEffect(() => {
    if (establishmentId) {
      dispatch(getEstablishmentServicesById(establishmentId))
      dispatch(getEstablishmentLanguagesById(establishmentId))
    }
  }, [establishmentId])
  React.useEffect(() => {
    if (allServices.data !== null) {
      const deyuner = allServices.data.find((s) => s.id === 5)
      const dinner = allServices.data.find((s) => s.id === 6)
      const servicesFiltered = allServices.data.filter((s) => {
        return !(s.id === 1 || s.id === 5 || s.id === 6)
      })

      setCommonServices([deyuner, dinner, ...servicesFiltered])
    }
  }, [allServices])

  return (
    <React.Fragment>
      <Breakfast />
      {commonServices.length > 0 &&
        commonServices.map((s) => (
          <OtherService title={s.name} serviceId={s.id} />
        ))}
      <Languages />
    </React.Fragment>
  )
}

export default Services
