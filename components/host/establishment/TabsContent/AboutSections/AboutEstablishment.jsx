import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Redux */
import { updateEstablishment } from '@ducks/host/establishment/services'
import { useDispatch, useSelector } from 'react-redux'

/* Components */
import { SectionLayout } from '../../'
import { Loader } from 'components/'

function AboutEstablishment(props) {
  // states
  const [text, setText] = React.useState('')
  const [isModifyMode, setIsModifyMode] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(false)

  // redux state
  const { data, status } = useSelector((state) => state.establishment)
  const { user } = useSelector((state) => state.user)
  const userId = user?.id
  const dispatch = useDispatch()

  // effects
  React.useEffect(() => {
    getInitialData()
  }, [data])

  const getInitialData = () => {
    if (status === 'SUCCESS') {
      setText(data.description)
    }
  }

  // helper methods
  const handleModify = () => {
    setIsModifyMode(!isModifyMode)
    if (!isModifyMode == false) {
      getInitialData()
    }
  }

  const handleChange = (event) => {
    const { value } = event.target
    setText(value)
  }

  const handleSubmit = (event) => {
    event.preventDefault()

    const wasTextModified = text !== data.description

    if (!wasTextModified) return

    setIsLoading(true)

    const dataToUpdate = {
      ...data,
      description: text
    }

    dispatch(updateEstablishment(data.id, dataToUpdate))
      .then(() => setIsModifyMode(false))
      .finally(() => setIsLoading(false))
  }

  const areRequestsPending = () => {
    return status === 'LOADING' || !userId
  }

  return (
    <SectionLayout
      title="À propos de l'établissement"
      isModifyMode={isModifyMode}
      onModify={handleModify}
    >
      <form onSubmit={handleSubmit}>
        <h3
          className="mb-3 font-montserrat"
          style={{ color: '#363636', fontSize: '16px' }}
        >
          Description générale de votre établissement
        </h3>

        {areRequestsPending() && <Loader />}

        {status === 'FAILURE' && <p>Error</p>}

        {status === 'SUCCESS' &&
          (isModifyMode ? (
            <React.Fragment>
              <textarea
                onChange={handleChange}
                className="form-control font-montserrat"
                placeholder="Description générale de votre établissement"
                style={{ height: '200px' }}
                value={text}
                disabled={isLoading}
              />

              <div className="mt-4">
                <button disabled={isLoading} className="btn btn-secondary">
                  {isLoading ? 'Loading...' : 'Enregistrer'}
                </button>
              </div>
            </React.Fragment>
          ) : (
            <p className="text-muted">{text}</p>
          ))}
      </form>
    </SectionLayout>
  )
}

export default AboutEstablishment
