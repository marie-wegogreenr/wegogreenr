import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import { SectionLayout } from '@components/host/establishment'
import { Users } from '@components/Icons'

/* Redux */
import { useDispatch, useSelector } from 'react-redux'
import {
  getEstablishmentEquipmentsById,
  updateEstablishmentEquipmentsById
} from '@ducks/host/establishmentEquipmentsById/services'
import { Loader } from 'components/'

function EstablishmentEquipments() {
  // states
  const [isModifyMode, setIsModifyMode] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(false)
  const [modifiedInputs, setModifiedInputs] = React.useState({})

  // redux
  const {
    establishmentEquipmentsById,
    establishment,
    allEstablishmentEquipments,
    user: { user }
  } = useSelector((state) => state)
  const userId = user?.id

  // dispatch
  const dispatch = useDispatch()

  // helper methods
  const handleModify = () => {
    setIsModifyMode(!isModifyMode)
  }

  const areRequestsPending = () => {
    return (
      establishmentEquipmentsById.status === 'LOADING' ||
      establishment.status === 'LOADING' ||
      allEstablishmentEquipments.status === 'LOADING' ||
      !userId
    )
  }

  const wereRequestsSuccessful = () => {
    return (
      establishmentEquipmentsById.status === 'SUCCESS' &&
      establishment.status === 'SUCCESS' &&
      allEstablishmentEquipments.status === 'SUCCESS'
    )
  }

  const isCheked = (id) => {
    return establishmentEquipmentsById.data.some((e) => {
      return e.equipment_id === id
    })
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    const establishmentId = establishment.data.id
    setIsLoading(true)

    dispatch(updateEstablishmentEquipmentsById(establishmentId, modifiedInputs))
      .then(() => {
        return dispatch(getEstablishmentEquipmentsById(establishmentId))
      })
      .finally(() => {
        setIsModifyMode(false)
        setIsLoading(false)
      })
  }

  const handleChange = (event) => {
    const { value, checked } = event.target
    const valueInNumber = Number(value)

    setModifiedInputs((prev) => ({ ...prev, [valueInNumber]: checked }))
  }

  return (
    <SectionLayout
      title="Équipements de l’établissement"
      onModify={handleModify}
      isModifyMode={isModifyMode}
    >
      {areRequestsPending() && <Loader />}

      {wereRequestsSuccessful() && (
        <form onSubmit={handleSubmit}>
          <ul className="row w-100 list-unstyled">
            {isModifyMode
              ? allEstablishmentEquipments.data.map((equipment) => (
                  <li
                    key={`all-inputs-${equipment.id}`}
                    className="col-lg-6 col-md-6 mb-4"
                  >
                    <div className="form-check d-flex align-items-center">
                      <input
                        className="form-check-input input-check-blue-dark input-check-size-18"
                        type="checkbox"
                        onChange={handleChange}
                        defaultChecked={isCheked(equipment.id)}
                        defaultValue={equipment.id}
                        disabled={isLoading}
                        id={`${equipment.name
                          .replace(/\s/g, '_')
                          .toLowerCase()}_${equipment.id}`}
                      />
                      <label
                        className="form-check-label"
                        htmlFor={`${equipment.name
                          .replace(/\s/g, '_')
                          .toLowerCase()}_${equipment.id}`}
                      >
                        <img
                          src={`${process.env.NEXT_PUBLIC_AMAZON_IMGS}${equipment.icon}`}
                          alt="Icon"
                          width="40px"
                          height="40px"
                        />
                        {equipment.name}
                      </label>
                    </div>
                  </li>
                ))
              : establishmentEquipmentsById.data.map(
                  ({ equipment, equipment_id }) => (
                    <li
                      key={`current-inputs-${equipment_id}`}
                      className="col-lg-6 col-md-6"
                    >
                      <div className="d-flex align-items-center mb-4">
                        <span className="bg-primary p-1 rounded-2">
                          <img
                            src={`${process.env.NEXT_PUBLIC_AMAZON_IMGS}${equipment.icon_alt}`}
                            alt="Icon"
                            width="40px"
                            height="40px"
                          />
                        </span>
                        <p className="ml-2 mb-0 font-montserrat">
                          <strong>{equipment.name}</strong>
                        </p>
                      </div>
                    </li>
                  )
                )}
          </ul>

          {isModifyMode && (
            <div className="mt-4">
              <button disabled={isLoading} className="btn btn-secondary">
                {isLoading ? 'Loading...' : 'Enregistrer'}
              </button>
            </div>
          )}
        </form>
      )}
    </SectionLayout>
  )
}

export default EstablishmentEquipments
