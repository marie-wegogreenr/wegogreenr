/* React */
import * as React from 'react'
/* Redux */
import { getEstablishment } from '@ducks/host/establishment'
import { useDispatch, useSelector } from 'react-redux'
/* Components */
import { BasicInfo, Location, ContactHost } from './EstablishmentInfoSections'
import { getCountries } from '@ducks/host/countries'
import { getEstablishmentTypes } from '@ducks/host/establishmentTypes'

function EstablishmentInfo(props) {
  /*  Dispatch  */
  const dispatch = useDispatch()

  /*  Global states  */
  const {
    establishment,
    countries,
    establishmentTypes,
    user: { user }
  } = useSelector((state) => state)
  const userId = user?.id
  /*  Effects  */
  React.useEffect(() => {
    if (countries.status === 'IDLE') {
      dispatch(getCountries())
    }
  }, [])

  React.useEffect(() => {
    if (props.from == 'admin') {
      dispatch(getEstablishment(props.id_etablisement, props.from))
    } else {
      dispatch(getEstablishment(userId, props.from))
    }

    if (establishmentTypes.status === 'IDLE' && userId) {
      dispatch(getEstablishmentTypes())
    }
  }, [userId])

  return (
    <React.Fragment>
      <BasicInfo />
      <Location />
      <ContactHost />
    </React.Fragment>
  )
}

export default EstablishmentInfo
