import * as React from 'react'
/* Components */
import { SectionLayout } from '../../'
import { Loader } from 'components/'
import Map from '@components/Onboarding/Map'
/* Packages */
import Autocomplete from 'react-google-autocomplete'
import _ from 'lodash'
import debounce from 'lodash.debounce'
/* Redux */
import { useDispatch, useSelector } from 'react-redux'
import { updateEstablishment } from '@ducks/host/establishment/services'
/* Hooks */
import { useToast } from 'hooks/useToast'
/* Services & Helpers */
import { getAddressCoordinates } from 'services/etablissementService'
import { findRegionAndCity } from 'helpers/localizationHelper'

function Location() {
  /* Notification */
  const [notify] = useToast()
  /* Local states */
  const [fields, setFields] = React.useState({
    canSearch: false,
    address: '',
    lat: '',
    lng: '',
    street_number: '',
    zip_code: '',
    country_id: 1,
    city: '',
    city_selected: '',
    region_id: '',
    region_name: '',
    city_name: '',
    center: {
      lat: 46.2245,
      lng: 2.2123
    },
    showMap: true
  })
  const [isModifyMode, setIsModifyMode] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(false)
  /* Global states */
  const {
    establishment,
    countries,
    user: { user }
  } = useSelector((state) => state)
  const userId = user?.id
  const dispatch = useDispatch()
  /* Effects */
  React.useEffect(() => {
    getInitialData()
  }, [establishment])

  React.useEffect(async () => {
    debouncedSearch(fields)
  }, [fields.address])

  /* Handle functions */
  const handleSearchAddress = async (data) => {
    if (data.canSearch) {
      let address = `${data.address !== '' ? `${data.address}+` : ''}France`

      try {
        const response = await getAddressCoordinates(address)

        if (response.status !== 'ZERO_RESULTS') {
          const location = response.results[0].geometry.location

          const { region_name, city_name } = findRegionAndCity(
            response.results[0].address_components
          )
          setFields({
            ...data,
            lat: location.lat,
            lng: location.lng,
            region_name,
            city_name,
            center: {
              lat: location.lat,
              lng: location.lng
            }
          })
        } else {
          notify(`Aucune adresse trouvée`, 'error')
        }
      } catch (error) {
        console.error(error)
      }
    }
  }

  const debouncedSearch = React.useRef(
    debounce((fields) => handleSearchAddress(fields), 3000)
  ).current

  const getInitialData = () => {
    if (establishment.status === 'SUCCESS') {
      const { address, lat, lng, country_id, city_name, region_name } =
        establishment.data
      setFields({
        ...fields,
        address,
        lat,
        lng,
        city_name,
        region_name,
        country_id,
        center: {
          lat: lat,
          lng: lng
        }
      })
    }
  }

  const handleModify = () => {
    setIsModifyMode(!isModifyMode)
    if (!isModifyMode == false) {
      setFields({ ...fields, street_number: '' })
      setTimeout(() => {
        getInitialData()
      }, 1000)
    }
  }

  const setPlace = (address, LocationData) => {
    const {
      address_components,
      geometry: { location }
    } = LocationData

    const lat = location.lat()
    const lng = location.lng()
    const { region_name, city_name } = findRegionAndCity(address_components)
    setFields({
      ...fields,
      lat,
      lng,
      address,
      region_name,
      city_name,
      canSearch: false,
      center: {
        lat,
        lng
      }
    })
  }

  const handleChange = (value, name) => {
    setFields({
      ...fields,
      [name]: value,
      canSearch: true
    })
  }

  const areRequestsPending = () => {
    return (
      establishment.status === 'LOADING' ||
      countries.status === 'LOADING' ||
      !userId
    )
  }

  const wereRequestsSuccessful = () => {
    return establishment.status === 'SUCCESS' && countries.status === 'SUCCESS'
  }

  const didRequestsFail = () => {
    return establishment.status === 'FAILURE' || countries.status === 'FAILURE'
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    setIsLoading(true)

    // if(!fields.country_id) {
    //   setFields({
    //     ...fields,
    //     country_id: 1
    //   })
    // }

    const { id: establishmentId, ...rest } = establishment.data
    const {
      zip_code,
      region_id,
      street_number,
      address,
      country_id,
      city_name,
      region_name,
      lat,
      lng,
      city
    } = fields

    const dataToUpdate = {
      ...rest,
      id: establishmentId,
      region_id,
      zip_code,
      city_name,
      region_name,
      street_number,
      address,
      lat,
      lng,
      country_id,
      city,
      is_active: rest.active
    }

    dispatch(updateEstablishment(establishmentId, dataToUpdate))
      .then(() => {
        setIsModifyMode(false)
        notify(`Modifications sauvegardées`, 'success')
      })
      .finally(() => setIsLoading(false))
  }

  return (
    <SectionLayout
      title="Localisation"
      onModify={handleModify}
      isModifyMode={isModifyMode}
    >
      {areRequestsPending() && <Loader />}

      {didRequestsFail() && <p>Error</p>}

      {wereRequestsSuccessful() && (
        <form onSubmit={handleSubmit} autocomplete="nope">
          <div className="row mb-4">
            <div className="col-12">
              <label htmlFor="n_rue" className="form-label weight-bold">
                Adresse
              </label>
              <Autocomplete
                apiKey={process.env.NEXT_PUBLIC_GOOGLE_MAPS_KEY}
                name="address"
                options={{
                  componentRestrictions: { country: 'fr' },
                  types: ['geocode']
                }}
                onPlaceSelected={({ formatted_address, ...rest }) => {
                  setPlace(formatted_address, rest)
                }}
                onChange={(event) => {
                  handleChange(event.target.value, 'address')
                }}
                onKeyPress={(e) => {
                  e.key === 'Enter' && e.preventDefault()
                }}
                defaultValue={fields.address ? fields.address : ''}
                value={fields.address}
                disabled={!isModifyMode || isLoading}
                className="form-control p-4"
                placeholder="25 Rue des Pyramides, 75001"
              />
            </div>
          </div>
          <div className="row mb-4">
            <div className="col-12 col-md-6 mb-3">
              <label htmlFor="n_rue" className="form-label weight-bold">
                Région
              </label>
              <input
                type="text"
                className="form-control p-4"
                value={fields.region_name}
                readOnly
              />
            </div>
            <div className="col-12 col-md-6 mb-3">
              <label htmlFor="adresse" className="form-label weight-bold">
                Ville
              </label>
              <input
                type="text"
                className="form-control p-4"
                value={fields.city_name}
                readOnly
              />
            </div>
          </div>
          <div className=" row">
            <div
              className="card-map mb-3"
              style={{ width: 100 + '%', height: 400 + 'px' }}
            >
              {fields.showMap ? <Map center={fields.center} /> : <Loader />}
            </div>
          </div>

          {isModifyMode && (
            <button
              disabled={isLoading}
              type="submit"
              className="btn btn-secondary"
            >
              {isLoading ? 'Loading...' : 'Enregistrer'}
            </button>
          )}
        </form>
      )}
    </SectionLayout>
  )
}

export default Location
