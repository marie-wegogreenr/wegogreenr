import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Redux */
import { useDispatch, useSelector } from 'react-redux'

/* Component */
import { SectionLayout } from '../../'
import { updateEstablishment } from '@ducks/host/establishment/services'
import { Loader } from 'components/'
import { validateOnlyNumber } from 'helpers/utilsHelper'
import { useToast } from 'hooks/useToast'

function ContactHost() {
  // states
  const [notify] = useToast()
  const [isLoading, setIsLoading] = React.useState(false)
  const [isModifyMode, setIsModifyMode] = React.useState(false)
  const [fields, setFields] = React.useState({
    name: '',
    lastname: '',
    email: '',
    phone: ''
  })

  // redux state
  const {
    establishment,
    user: { user }
  } = useSelector((state) => state)
  const userId = user?.id
  const dispatch = useDispatch()

  // effects
  React.useEffect(() => {
    getInitialData()
  }, [establishment])

  const getInitialData = () => {
    if (establishment.status === 'SUCCESS') {
      const { contact_name, contact_lastname, contact_phone, contact_email } =
        establishment.data

      setFields({
        ...fields,
        name: contact_name,
        lastname: contact_lastname,
        phone: contact_phone,
        email: contact_email
      })
    }
  }

  // helper methods
  const handleModify = () => {
    setIsModifyMode(!isModifyMode)
    if (!isModifyMode == false) {
      getInitialData()
    }
  }

  const handleChange = (event) => {
    const { name, value } = event.target

    if (event.target.type === 'tel') {
      const isNumber = validateOnlyNumber(event.target.value)
      setFields({
        ...fields,
        [name]: isNumber ? value : value.slice(0, -1)
      })
    } else {
      setFields({
        ...fields,
        [name]: value
      })
    }
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    setIsLoading(true)

    const { id: establishmentId, ...rest } = establishment.data
    const { email, phone, name, lastname } = fields

    const dataToUpdate = {
      ...rest,
      id: establishmentId,
      contact_email: email,
      contact_lastname: lastname,
      contact_phone: phone,
      contact_name: name
    }

    dispatch(updateEstablishment(establishmentId, dataToUpdate))
      .then(() => {
        notify(`Modifications sauvegardées`, 'success')
        setIsModifyMode(false)
      })
      .finally(() => {
        setIsLoading(false)
      })
  }

  const areRequestPending = () => {
    return establishment.status === 'LOADING' || !userId
  }

  return (
    <SectionLayout
      title="Contact du gestionnaire"
      isModifyMode={isModifyMode}
      onModify={handleModify}
    >
      {areRequestPending() && <Loader />}

      {establishment.status === 'FAILURE' && <p>Error</p>}

      {establishment.status === 'SUCCESS' && (
        <form onSubmit={handleSubmit}>
          <div className="row">
            <div className="col-12 col-md-6 mb-3">
              <label
                htmlFor="nom_gestionnaire"
                className="form-label weight-bold"
              >
                Prénom du gestionnaire
              </label>
              <input
                onChange={handleChange}
                type="text"
                className="form-control p-4"
                id="nom_gestionnaire"
                name="name"
                value={fields.name}
                disabled={isLoading || !isModifyMode}
                placeholder="Sam"
              />
            </div>

            <div className="col-12 col-md-6 mb-3">
              <label
                htmlFor="prénom_gestionnaire"
                className="form-label weight-bold"
              >
                Nom du gestionnaire
              </label>
              <input
                type="text"
                className="form-control p-4"
                id="prénom_gestionnaire"
                onChange={handleChange}
                name="lastname"
                value={fields.lastname}
                disabled={isLoading || !isModifyMode}
                placeholder="Smith"
              />
            </div>
          </div>

          <div className="row">
            <div className="col-12 col-md-6 mb-3">
              <label htmlFor="email" className="form-label weight-bold">
                Email
              </label>
              <input
                type="email"
                className="form-control p-4"
                id="email"
                name="email"
                onChange={handleChange}
                value={fields.email}
                disabled={isLoading || !isModifyMode}
                placeholder="Sam"
              />
            </div>

            <div className="col-12 col-md-6 mb-3">
              <label htmlFor="téléphone" className="form-label weight-bold">
                Téléphone
              </label>
              <input
                type="tel"
                className="form-control p-4"
                id="téléphone"
                name="phone"
                onChange={handleChange}
                value={fields.phone}
                disabled={isLoading || !isModifyMode}
                placeholder="Smith"
              />
            </div>
          </div>

          {isModifyMode && (
            <button
              disabled={isLoading}
              type="submit"
              className="btn btn-secondary"
            >
              {isLoading ? 'Loading...' : 'Enregistrer'}
            </button>
          )}
        </form>
      )}
    </SectionLayout>
  )
}

export default ContactHost
