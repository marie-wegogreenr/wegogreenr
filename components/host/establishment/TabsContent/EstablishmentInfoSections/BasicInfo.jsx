import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import { SectionLayout } from '../../'
import { Loader } from 'components/'

/* Redux */
import { useDispatch, useSelector } from 'react-redux'
import { updateEstablishment } from '@ducks/host/establishment/services'
import { useToast } from 'hooks/useToast'

function BasicInfo() {
  // states
  const [isLoading, setIsLoading] = React.useState(false)
  const [isModifyMode, setIsModifyMode] = React.useState(false)
  const [fields, setFields] = React.useState({
    name: '',
    type_id: ''
  })

  const [notify] = useToast()
  // dispatch
  const dispatch = useDispatch()

  // redux state
  const {
    establishmentTypes,
    establishment,
    user: { user }
  } = useSelector((state) => state)
  const userId = user?.id

  // effects
  React.useEffect(() => {
    if (establishment.status === 'SUCCESS') {
      const { name, type_id } = establishment?.data

      setFields({ ...fields, name, type_id })
    }
  }, [establishment])

  const getInitialData = () => {
    if (establishment.status === 'SUCCESS') {
      const { name, type_id } = establishment?.data

      setFields({ ...fields, name, type_id })
    }
  }

  // helper methods
  const handleModify = () => {
    setIsModifyMode(!isModifyMode)
    if (!isModifyMode == false) {
      getInitialData()
    }
  }

  const handleChange = (event) => {
    const { value, name } = event.target
    setFields({
      ...fields,
      [name]: value
    })
  }

  const areRequestsPending = () => {
    return (
      establishmentTypes.status === 'LOADING' ||
      establishment.status === 'LOADING' ||
      !userId
    )
  }

  const wereRequestSuccessful = () => {
    return (
      establishmentTypes.status === 'SUCCESS' &&
      establishment.status === 'SUCCESS'
    )
  }

  const didRequestsFail = () => {
    return (
      establishmentTypes.status === 'FAILURE' ||
      establishment.status === 'FAILURE'
    )
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    setIsLoading(true)

    const { id: establishmentId, ...rest } = establishment.data
    const { name, type_id } = fields
    const dataToUpdate = {
      ...rest,
      id: establishmentId,
      name,
      type_id
    }

    dispatch(updateEstablishment(establishmentId, dataToUpdate))
      .then(() => {
        setIsModifyMode(false)
        notify(`Modifications sauvegardées`, 'success')
      })
      .finally(() => setIsLoading(false))
  }

  return (
    <SectionLayout
      title="Infos basiques"
      onModify={handleModify}
      isModifyMode={isModifyMode}
    >
      {areRequestsPending() && <Loader />}

      {didRequestsFail() && <p>Error</p>}

      {wereRequestSuccessful() && (
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label
              htmlFor="nom_etablissement"
              className="form-label weight-bold"
            >
              Nom de l'établissement
            </label>
            <input
              type="text"
              className="form-control p-4"
              id="nom_etablissement"
              name="name"
              onChange={handleChange}
              value={fields.name}
              disabled={!isModifyMode || isLoading}
              placeholder="La Rêvolution: roulotte pour  2 (voire 3)"
            />
          </div>

          <div className="mb-3">
            <label
              htmlFor="type_d_établissement"
              className="form-label weight-bold"
            >
              Type d'établissement
            </label>
            <select
              type="text"
              className="form-control border rounded-1 px-4"
              id="type_d_établissement"
              placeholder="Logement entier"
              onChange={handleChange}
              name="type_id"
              value={fields.type_id}
              disabled={!isModifyMode || isLoading}
            >
              <option value="void">Sélectionnez une option</option>
              {establishmentTypes.data.map((type) => (
                <option key={type.id} value={type.id}>
                  {type.name}
                </option>
              ))}
            </select>
          </div>

          {/* {establishment.data.origin === 4 && (
            <div className="mb-3">
              <label htmlFor="prix" className="form-label weight-bold">
                Prix de référence
              </label>
              <input
                type="text"
                className="form-control p-4"
                onWheel={(e) => e.target.blur()}
                id="prix"
                name="prix"
                onChange={handleChange}
                value={fields.name}
                disabled={!isModifyMode || isLoading}
                placeholder="La Rêvolution: roulotte pour  2 (voire 3)"
              />
            </div>
          )} */}

          {isModifyMode && (
            <button
              disabled={isLoading}
              type="submit"
              className="btn btn-secondary"
            >
              {isLoading ? 'Loading...' : 'Enregistrer'}
            </button>
          )}
        </form>
      )}
    </SectionLayout>
  )
}

export default BasicInfo
