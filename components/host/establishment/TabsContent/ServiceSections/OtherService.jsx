import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import { ServiceInputs, ServiceRow } from '.'
import { SectionLayout } from '@components/host/establishment'
import { Loader } from 'components/'

/* Redux */
import { useDispatch, useSelector } from 'react-redux'
import { updateEstablishmentServicesById } from '@ducks/host/establishmentServicesById/services'

function OtherService({ serviceId, title }) {
  const [isLoading, setIsloading] = React.useState(false)
  const [isModifyMode, setIsModifyMode] = React.useState(false)
  const [fields, setFields] = React.useState({
    [serviceId]: {
      establishmentServiceId: null,
      checked: false,
      price: 0
    }
  })

  // redux
  const {
    allServices,
    establishmentServicesById,
    establishment,
    user: { user }
  } = useSelector((state) => state)
  const userId = user?.id || null

  const dispatch = useDispatch()

  // helper methods
  const handleModify = () => {
    setIsModifyMode(!isModifyMode)
    if (!isModifyMode == false) {
      getInitialData()
    }
  }

  const handleChangeChecked = () => {
    setFields({
      ...fields,
      [serviceId]: {
        ...fields[serviceId],
        checked: !fields[serviceId].checked
      }
    })
  }

  const handleChangePrice = (event) => {
    const { value } = event.target

    setFields({
      ...fields,
      [serviceId]: {
        ...fields[serviceId],
        price: value
      }
    })
  }

  const getService = () => {
    return establishmentServicesById.data?.find((service) => {
      return service.service_id === serviceId
    })
  }

  // requests status
  const isResquestsLoading = () => {
    return (
      allServices.status === 'LOADING' ||
      establishmentServicesById.status === 'LOADING' ||
      establishment.status === 'LOADING' ||
      !userId
    )
  }

  const isRequestsSuccessful = () => {
    return (
      allServices.status === 'SUCCESS' &&
      establishmentServicesById.status === 'SUCCESS' &&
      establishment.status === 'SUCCESS' &&
      userId
    )
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    const establishmentId = establishment.data?.id
    setIsloading(true)

    dispatch(
      updateEstablishmentServicesById(establishmentId, fields, serviceId)
    ).then(() => {
      setIsloading(false)
      setIsModifyMode(false)
    })
  }

  React.useEffect(() => {
    getInitialData()
  }, [getService()])

  const getInitialData = () => {
    if (isRequestsSuccessful())
      setFields((prev) => ({
        [serviceId]: {
          ...prev[serviceId],
          establishmentServiceId: getService()?.id,
          price: getService()?.price || '0',
          checked: !!getService()
        }
      }))
  }

  return (
    <SectionLayout
      title={title}
      onModify={handleModify}
      isModifyMode={isModifyMode}
    >
      {isResquestsLoading() && <Loader />}

      {isRequestsSuccessful() &&
        (isModifyMode ? (
          <form onSubmit={handleSubmit}>
            <ServiceInputs
              handleChangeChecked={handleChangeChecked}
              isLoading={isLoading}
              handleChangePrice={handleChangePrice}
              name=""
              id={serviceId}
              fields={fields}
            />
            <button
              disabled={isLoading}
              type="submit"
              className="btn btn-secondary"
            >
              {isLoading ? 'Loading...' : 'Enregistrer'}
            </button>
          </form>
        ) : (
          <>
            <ServiceRow
              name=""
              text={!!getService() ? 'Oui' : 'Non'}
              price={getService()?.price || '0'}
            />
          </>
        ))}
    </SectionLayout>
  )
}

export default OtherService
