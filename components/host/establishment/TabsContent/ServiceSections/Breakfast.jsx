import Services from '@components/dashboard_host/etablissement/services'
import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Redux */
import { useDispatch, useSelector } from 'react-redux'
import { updateEstablishmentServicesById } from '@ducks/host/establishmentServicesById/services'

/* Component */
import { ServiceRow, ServiceInputs } from '.'
import { SectionLayout } from '../../'
import { Loader } from 'components/'
import {
  breakfastTypes,
  getEtablissementServicesById
} from 'services/roomServicesService'

function Breakfast() {
  const [isLoading, setIsLoading] = React.useState(false)
  const [isModifyMode, setIsModifyMode] = React.useState(false)
  const [fields, setFields] = React.useState({})
  const [allBreakfast, setAllBreakfast] = React.useState([])
  const [myBreakfasts, setMyBreakfasts] = React.useState([])
  // redux
  const { establishment } = useSelector((state) => state)
  const establishmentId = establishment.data?.id || null
  const dispatch = useDispatch()

  React.useEffect(async () => {
    const [breakfast, myServices] = await Promise.all([
      breakfastTypes(),
      getEtablissementServicesById(establishmentId)
    ])
    const fiterMyBreakfasts = myServices.filter((b) => b.service_id === 1)
    setAllBreakfast(breakfast)
    setMyBreakfasts(fiterMyBreakfasts)
  }, [])

  const handleModify = () => {
    setIsModifyMode(!isModifyMode)
    if (!isModifyMode == false) {
      getInitialData()
    }
  }

  const handleChangeChecked = (breakfastId) => {
    setFields({
      ...fields,
      [breakfastId]: {
        ...fields[breakfastId],
        checked: !fields[breakfastId].checked
      }
    })
  }

  const handleChangePrice = (event, breakfastId) => {
    const { value } = event.target

    setFields({
      ...fields,
      [breakfastId]: {
        ...fields[breakfastId],
        price: value
      }
    })
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    setIsLoading(true)
    const BREAKFAST_ID = 1

    dispatch(
      updateEstablishmentServicesById(establishmentId, fields, BREAKFAST_ID)
    ).then(() => {
      setIsLoading(false)
      setIsModifyMode(false)
    })
  }

  React.useEffect(() => {
    getInitialData()
  }, [allBreakfast, myBreakfasts])

  const getInitialData = () => {
    if (allBreakfast.length !== 0) {
      for (const b of allBreakfast) {
        setFields((prev) => ({
          ...prev,
          [b.id]: {
            checked: false,
            price: 0,
            breakfastTypeId: b?.id || '',
            name: b.name
          }
        }))
      }

      for (const b of myBreakfasts) {
        setFields((prev) => ({
          ...prev,
          [b.breakfast_type_id]: {
            ...prev[b.breakfast_type_id],
            establishmentServiceId: b.id,
            checked: true,
            price: Number(b.price) || 0
          }
        }))
      }
    }
  }

  return (
    <SectionLayout
      onModify={handleModify}
      isModifyMode={isModifyMode}
      title="Petit-déjeuner"
    >
      {/* <Services /> */}
      {allBreakfast.length === 0 && <Loader />}

      {allBreakfast.length !== 0 &&
        (isModifyMode ? (
          <form onSubmit={handleSubmit}>
            {allBreakfast.map((breakfast) => (
              <ServiceInputs
                key={breakfast.id}
                id={breakfast.id}
                name={breakfast.name}
                fields={fields}
                isLoading={isLoading}
                handleChangeChecked={handleChangeChecked}
                handleChangePrice={handleChangePrice}
              />
            ))}
            <button
              disabled={isLoading}
              type="submit"
              className="btn btn-secondary"
            >
              {isLoading ? 'Loading...' : 'Enregistrer'}
            </button>
          </form>
        ) : (
          <ul className="list-unstyled">
            {Object.values(fields).map((breakfast) => {
              return (
                <>
                  <ServiceRow
                    key={breakfast.breakfastTypeId}
                    name={breakfast.name}
                    text={
                      breakfast.checked && breakfast?.price !== 0
                        ? 'Optionnel'
                        : breakfast.checked && breakfast?.price === 0
                        ? 'Iclus'
                        : 'Non inclus'
                    }
                    price={breakfast?.price || '0'}
                  />
                </>
              )
            })}
          </ul>
        ))}
    </SectionLayout>
  )
}

export default Breakfast
