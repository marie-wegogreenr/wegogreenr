import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

function ServiceRow({ name, price, text }) {
  return (
    <li className="d-flex justify-content-between align-items-center py-3 border-bottom">
      <p className="mb-0 font-montserrat">
        <span className="weight-semibold">
          {name} {name ? ':' : ''}
        </span>{' '}
        <span>{text}</span>
      </p>
      <p className="mb-0 font-montserrat">
        <span className="weight-semibold">Prix : </span>
        <span>{price}</span>€
      </p>
    </li>
  )
}

export default ServiceRow
