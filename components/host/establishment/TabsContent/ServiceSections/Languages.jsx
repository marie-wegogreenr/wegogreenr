import { updateEstablishmentLanguagesById } from '@ducks/host/establishmentLanguagesById/services'
import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Redux */
import { useDispatch, useSelector } from 'react-redux'

/* Components */
import { Loader } from 'components/'
import { SectionLayout } from '../../'

/* ********************* MAIN COMPONENT ************************* */
function Languages() {
  /* ********** STATES ************ */
  const [isLoading, setIsLoading] = React.useState(false)
  const [isModifyMode, setIsModifyMode] = React.useState(false)
  const [fields, setFields] = React.useState({})

  const {
    allLanguages,
    establishmentLanguagesById,
    establishment,
    user: { user }
  } = useSelector((state) => state)
  const dispatch = useDispatch()

  const userId = user?.id
  const establishmentId = establishment.data?.id

  // helper methods
  const handleModify = () => {
    setIsModifyMode(!isModifyMode)
  }

  const getLanguage = (languageId) => {
    return establishmentLanguagesById.data.find(
      (l) => l.language_id === languageId
    )
  }

  const handleChange = (event) => {
    const { value, checked } = event.target

    setFields({
      ...fields,
      [value]: checked
    })
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    setIsLoading(true)

    dispatch(updateEstablishmentLanguagesById(establishmentId, fields))
      .then(() => {
        setIsModifyMode(false)
      })
      .finally(() => {
        setIsLoading(false)
      })
  }

  /* ************* REQUESTS  ************** */
  const isRequestsLoading = () => {
    return (
      allLanguages.status === 'LOADING' ||
      establishmentLanguagesById.status === 'LOADING' ||
      establishment.status === 'LOADING' ||
      !userId
    )
  }

  const isRequestsSuccessful = () => {
    return (
      allLanguages.status === 'SUCCESS' &&
      establishmentLanguagesById.status === 'SUCCESS' &&
      establishment.status === 'SUCCESS' &&
      !!userId
    )
  }

  return (
    <SectionLayout
      onModify={handleModify}
      isModifyMode={isModifyMode}
      title="Langues parlées"
    >
      {isRequestsLoading() && <Loader />}

      {isRequestsSuccessful() &&
        (isModifyMode ? (
          <form onSubmit={handleSubmit}>
            <ul className="list-unstyled container">
              {allLanguages.data.map((language) => (
                <li
                  key={`languages-${language.id}`}
                  className="col-lg-4 col-md-6 w-100"
                >
                  <div className="form-check d-flex align-content-center">
                    <input
                      className="form-check-input input-check-blue-dark input-check-size-18"
                      type="checkbox"
                      value={language.id}
                      onChange={handleChange}
                      defaultChecked={!!getLanguage(language.id)}
                      disabled={isLoading}
                    />
                    <label className="form-check-label">
                      <p className="text-capitalize">{language.name}</p>
                    </label>
                  </div>
                </li>
              ))}
            </ul>
            <button
              disabled={isLoading}
              type="submit"
              className="btn btn-secondary"
            >
              {isLoading ? 'Loading...' : 'Enregistrer'}
            </button>
          </form>
        ) : (
          <ul className="list-unstyled host-dashboard__languages">
            {establishmentLanguagesById.data.map(({ language }) => (
              <li key={language.id} className="mr-3">
                <p className="text-capitalize">{language.name}</p>
              </li>
            ))}
          </ul>
        ))}
    </SectionLayout>
  )
}

export default Languages
