import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

function ServiceInputs({
  isLoading,
  fields,
  name,
  id,
  handleChangeChecked,
  handleChangePrice
}) {
  return (
    <div className="row mb-5 mb-sm-2">
      <div className="col-12 col-sm-6">
        <p className="mb-0 weight-semibold mb-2">{name || ''}</p>

        <div
          className="d-flex justify-content-between"
          style={{ marginTop: name == '' ? '28px' : '' }}
        >
          <div className="form-check form-check-inline ml-0 w-100">
            <label className="d-flex justify-content-center align-items-center w-100 p-3 bg-light">
              <input
                className="form-check-input radio-check-20"
                type="radio"
                onChange={() => handleChangeChecked(id)}
                name={'breakfast_' + id}
                value={id}
                checked={fields[id].checked}
                disabled={isLoading}
              />

              <span>Oui</span>
            </label>
          </div>

          <div className="form-check form-check-inline ml-0 w-100 mr-0">
            <label className="d-flex justify-content-center align-items-center w-100 p-3 bg-light">
              <input
                className="form-check-input radio-check-20"
                type="radio"
                onChange={() => handleChangeChecked(id)}
                name={'breakfast_' + id}
                value={id}
                checked={!fields[id].checked}
                disabled={isLoading}
              />

              <span>Non</span>
            </label>
          </div>
        </div>
      </div>
      <div className="col-12 col-sm-6">
        <div className="">
          <label className="weight-semibold" htmlFor={'prix_breakfast_' + id}>
            Prix
          </label>
          <input
            type="number"
            onWheel={(e) => e.target.blur()}
            min="0"
            step=".01"
            className="form-control py-4"
            id={'prix_breakfast_' + id}
            name={'prix_breakfast_' + id}
            aria-describedby="emailHelp"
            value={fields[id].price}
            disabled={!fields[id].checked || isLoading}
            onChange={(event) => handleChangePrice(event, id)}
          />
        </div>
      </div>
    </div>
  )
}

export default ServiceInputs
