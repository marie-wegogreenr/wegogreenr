import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Redux */
import { useDispatch, useSelector } from 'react-redux'
import { getAllEstablishmentEquipments } from '@ducks/host/allEstablishmentEquipments'
import { getEstablishmentEquipmentsById } from '@ducks/host/establishmentEquipmentsById'

/* Components */
import { AboutEstablishment, EstablishmentEquipments } from './AboutSections'

function About(props) {
  const {
    allEstablishmentEquipments,
    establishmentEquipmentsById,
    establishment,
    user: { user }
  } = useSelector((state) => state)
  const userId = user?.id

  const establishmentId = establishment.data?.id

  const dispatch = useDispatch()

  React.useEffect(() => {
    if (allEstablishmentEquipments.status === 'IDLE') {
      dispatch(getAllEstablishmentEquipments())
    }
  }, [])

  React.useEffect(() => {
    if (
      establishmentEquipmentsById.status === 'IDLE' &&
      establishmentId &&
      userId
    ) {
      dispatch(getEstablishmentEquipmentsById(establishmentId))
    }
  }, [establishmentId, userId])

  return (
    <React.Fragment>
      <AboutEstablishment />
      <EstablishmentEquipments />
    </React.Fragment>
  )
}

export default About
