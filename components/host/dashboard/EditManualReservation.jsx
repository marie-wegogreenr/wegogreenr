import { useToast } from 'hooks/useToast'
import * as React from 'react'
import { updateHebergement } from 'services/hebergementsService'
import { useDispatch, useSelector } from 'react-redux'
import { updateHebergementProp } from '@ducks/hebergements/actions'
import { formatSlug } from 'helpers/roomHelper'

function EditManualReservation() {
  const room = useSelector((store) => store.hebergement)
  const dispatch = useDispatch()
  const [editMode, setEditMode] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(false)
  const [inputValue, setInputValue] = React.useState(room.reservation_mode)
  const [notify] = useToast()

  const saveRoomChanges = async () => {
    setIsLoading(true)

    const { establishment, room_image, ...dataToUpdate } = room

    const { message } = await updateHebergement(
      { ...dataToUpdate, reservation_mode: parseInt(inputValue) },
      room.id
    )
    if (message === 'Room updated successfuly!') {
      dispatch(
        updateHebergementProp({
          reservation_mode: parseInt(inputValue)
        })
      )

      setEditMode(!editMode)
      notify('Informations mises à jour', 'success')
    } else {
      notify('il y a eu un problème, veuillez réessayer plus tard', 'error')
    }
    setIsLoading(false)
  }

  return (
    <section className="bg-white mt-4 rounded-3 p-4">
      <div className={'d-flex justify-content-between align-items-center mb-4'}>
        <h3 className="mb-0 font-montserrat">Type de réservation</h3>

        <div className="form-room__question-edit">
          {editMode ? (
            <button
              type="button"
              onClick={() => setEditMode(!editMode)}
              className="btn btn-outline-gray border-0 d-flex justify-content-between align-items-center"
            >
              <span className="mr-2">x</span>
              <u>Annuler</u>
            </button>
          ) : (
            <button
              type="button"
              onClick={() => setEditMode(!editMode)}
              className="btn btn-outline-secondary border-0"
            >
              <u>Editer</u>
            </button>
          )}
        </div>
      </div>

      <div className="">
        <div>
          <div className="card card-body">
            <div className="row">
              <div className="form-check mt-3 col-6">
                <div className="row">
                  <div className="col-10">
                    <label htmlFor="type_reservation">
                      <b>Réservation instantanée</b>
                    </label>
                    <p>
                      Ce que nos voyageurs recherchent en priorité. Les
                      réservations sont automatiquement acceptées et ajoutées à
                      votre calendrier. Vous recevez un email avec toutes les
                      informations de la réservation.
                    </p>
                  </div>
                  <div className="col-2" style={{ textAlign: 'end' }}>
                    <input
                      disabled={!editMode}
                      className="form-check-input"
                      type="radio"
                      name="type_reservation"
                      id="type_reservation"
                      value="0"
                      checked={parseInt(inputValue) === 0}
                      onChange={(e) => {
                        setInputValue(e.target.value)
                      }}
                      required
                    />
                  </div>
                </div>
              </div>
              <div className="form-check mt-3 col-6">
                <div className="row">
                  <div className="col-10">
                    <label htmlFor="type_reservation">
                      <b>Demande de réservation</b>
                    </label>
                    <p>
                      Vous êtes informé par email d'une nouvelle demande de
                      réservation : vous avez 24h pour l'accepter ou la refuser.
                      Au-delà, celle-ci est automatiquement annulée.
                    </p>
                  </div>
                  <div className="col-2" style={{ textAlign: 'end' }}>
                    <input
                      disabled={!editMode}
                      className="form-check-input"
                      type="radio"
                      name="type_reservation"
                      id="type_no_reservation"
                      checked={parseInt(inputValue) === 1}
                      value="1"
                      onChange={(e) => {
                        setInputValue(e.target.value)
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {editMode && (
        <div className="mt-4 py-4 border-top">
          {/* {help !== null && (
            <div>
              <h4 className="color-secondary">Conseil : </h4>
              <p>{help}</p>
            </div>
          )} */}

          <button
            disabled={isLoading}
            type="button"
            className="btn btn-secondary"
            onClick={saveRoomChanges}
          >
            {isLoading ? 'Loading...' : 'Sauvegarder'}
          </button>
        </div>
      )}
    </section>
  )
}

export default EditManualReservation
