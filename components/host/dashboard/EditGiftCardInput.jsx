import { updateHebergementProp } from '@ducks/hebergements/actions'
import { useToast } from 'hooks/useToast'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { updateHebergement } from 'services/hebergementsService'

function EditGiftCardInput({ title, description }) {
  /*  Global states  */
  const room = useSelector((store) => store.hebergement)
  const dispatch = useDispatch()
  const router = useRouter()
  /*  Local states  */
  const [editMode, setEditMode] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [acceptCoupon, setAcceptCoupon] = useState('0')
  const [notify] = useToast()
  /*  Effects  */
  useEffect(() => {
    if (room?.accept_coupon !== undefined) {
      setAcceptCoupon(room?.accept_coupon)
    }
  }, [room])

  const saveChanges = async () => {
    setIsLoading(true)
    const { establishment, room_image, ...dataToUpdate } = room

    const { message } = await updateHebergement(
      {
        ...dataToUpdate,
        accept_coupon: acceptCoupon
      },
      room.id
    )

    if (message === 'Room updated successfuly!') {
      dispatch(
        updateHebergementProp({
          accept_coupon: acceptCoupon
        })
      )

      setEditMode(!editMode)
      notify('Informations mises à jour', 'success')
    } else {
      setIsLoading(false)
      setInputValue(room[name])
      notify('il y a eu un problème, veuillez réessayer plus tard', 'error')
    }
    setIsLoading(false)
    try {
    } catch (error) {
      notify('il y a eu un problème, veuillez réessayer plus tard', 'error')
    } finally {
      setIsLoading(false)
      setEditMode(!editMode)
    }
  }

  const handleClick = () => {
    if (!acceptCoupon || acceptCoupon === '0') {
      setAcceptCoupon('1')
    } else {
      setAcceptCoupon('0')
    }
  }

  return (
    <div className="bg-white mt-4 rounded-3 p-4">
      <div className={'d-flex justify-content-between align-items-center mb-4'}>
        <h3 className="mb-0 font-montserrat">{title}</h3>
        <div className="form-room__question-edit">
          {editMode ? (
            <button
              type="button"
              onClick={() => setEditMode(!editMode)}
              className="btn btn-outline-gray border-0 d-flex justify-content-between align-items-center"
            >
              <span className="mr-2">x</span>
              <u>Annuler</u>
            </button>
          ) : (
            <button
              type="button"
              onClick={() => setEditMode(!editMode)}
              className="btn btn-outline-secondary border-0"
            >
              <u>Editer</u>
            </button>
          )}
        </div>
      </div>
      <p>{description}</p>
      <span
        className={`color-white ${
          !editMode
            ? 'bg-gray'
            : acceptCoupon === '1' || acceptCoupon === 1
            ? 'bg-primary'
            : 'bg-gray'
        } form-control w-25`}
      >
        {acceptCoupon === '1' || acceptCoupon === 1 ? 'Actif' : 'Inactif'}
      </span>
      {editMode && (
        <>
          <div
            className={`custom-control custom-switch ${
              router.asPath.includes('admin') ? 'mt-2' : ''
            }`}
          >
            <input
              type="checkbox"
              className="custom-control-input"
              id="accept-coupon"
              checked={acceptCoupon === '1' || acceptCoupon === 1}
              disabled={!editMode}
            />
            <label
              className={`${
                router.asPath.includes('admin')
                  ? 'custom-control-labels'
                  : 'custom-control-label'
              } ml-2 ${
                router.asPath.includes('admin') &&
                (acceptCoupon === '1' || acceptCoupon === 1)
                  ? 'custom-control-labels--active'
                  : ''
              }`}
              htmlFor="accept-coupon"
              style={{ cursor: 'pointer' }}
              onClick={handleClick}
              disabled={!editMode}
            ></label>
          </div>

          <div className="mt-4 py-4 border-top">
            <button
              disabled={isLoading}
              type="button"
              className="btn btn-secondary"
              onClick={saveChanges}
            >
              {isLoading ? 'Loading...' : 'Sauvegarder'}
            </button>
          </div>
        </>
      )}
    </div>
  )
}

export default EditGiftCardInput
