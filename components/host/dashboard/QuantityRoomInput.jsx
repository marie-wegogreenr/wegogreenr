import { useToast } from 'hooks/useToast'
import * as React from 'react'
import { updateHebergement } from 'services/hebergementsService'
import { useDispatch, useSelector } from 'react-redux'
import { updateHebergementProp } from '@ducks/hebergements/actions'
import { formatSlug } from 'helpers/roomHelper'

function QuantityRoomInput({ title, name, subtitle }) {
  const room = useSelector((store) => store.hebergement)
  const dispatch = useDispatch()
  const [editMode, setEditMode] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(false)
  const [inputValue, setInputValue] = React.useState(room[name])
  const [notify] = useToast()
  React.useEffect(() => {
    if (!room.isLoading) {
      setInputValue(room[name])
    }
  }, [room.isLoading])

  const saveRoomChanges = async () => {
    setIsLoading(true)

    const { establishment, room_image, ...dataToUpdate } = room

    const { message } = await updateHebergement(
      { ...dataToUpdate, [name]: inputValue },
      room.id
    )
    if (message === 'Room updated successfuly!') {
      dispatch(
        updateHebergementProp({
          [name]: inputValue
        })
      )

      setEditMode(!editMode)
      notify('Informations mises à jour', 'success')
    } else {
      setInputValue(room[name])
      notify('il y a eu un problème, veuillez réessayer plus tard', 'error')
    }
    setIsLoading(false)
  }

  return (
    <section className="bg-white mt-4 rounded-3 p-4">
      <div className={'d-flex justify-content-between align-items-center mb-4'}>
        <h3 className="mb-0 font-montserrat">{title}</h3>

        <div className="form-room__question-edit">
          {editMode ? (
            <button
              type="button"
              onClick={() => setEditMode(!editMode)}
              className="btn btn-outline-gray border-0 d-flex justify-content-between align-items-center"
            >
              <span className="mr-2">x</span>
              <u>Annuler</u>
            </button>
          ) : (
            <button
              type="button"
              onClick={() => setEditMode(!editMode)}
              className="btn btn-outline-secondary border-0"
            >
              <u>Editer</u>
            </button>
          )}
        </div>
      </div>

      <div className="">
        {editMode ? (
          <>
            <div className="bg-light p-3 d-flex justify-content-between align-items-center">
              <div>
                <p className="mb-0">
                  <strong>{subtitle}</strong>
                </p>
              </div>

              <div className="d-flex align-items-center">
                <button
                  className="btn bg-white btn-circle"
                  type="button"
                  name="rooms"
                  onClick={() => {
                    let input = inputValue
                    if (input > 0) input = input -= 1
                    setInputValue(input)
                  }}
                  disabled={!editMode || isLoading}
                >
                  <span>{'-'}</span>
                </button>

                <input
                  disabled={isLoading || isLoading}
                  style={{ width: '3rem' }}
                  className="form-control quantity border-0 text-center input-spin-hide bg-transparent mx-2"
                  min="0"
                  id="rooms"
                  name="rooms"
                  type="number"
                  onWheel={(e) => e.target.blur()}
                  value={inputValue}
                  readOnly
                />

                <button
                  disabled={isLoading}
                  type="button"
                  className="btn bg-white btn-circle"
                  name="rooms"
                  onClick={() => {
                    let input = inputValue
                    input = input += 1
                    setInputValue(input)
                  }}
                  type="button"
                  disabled={!editMode}
                >
                  <span>{'+'}</span>
                </button>
              </div>
            </div>
          </>
        ) : (
          <div className="d-flex justify-content-between align-items-center">
            <p className="mb-0">
              <strong>{subtitle}</strong>
            </p>

            <p className="mb-0">{inputValue}</p>
          </div>
        )}
      </div>

      {editMode && (
        <div className="mt-4 py-4 border-top">
          <button
            disabled={isLoading}
            type="button"
            className="btn btn-secondary"
            onClick={saveRoomChanges}
          >
            {isLoading ? 'Loading...' : 'Sauvegarder'}
          </button>
        </div>
      )}
    </section>
  )
}

export default QuantityRoomInput
