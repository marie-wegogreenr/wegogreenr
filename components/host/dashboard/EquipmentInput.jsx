import { useToast } from 'hooks/useToast'
import * as React from 'react'
import { addEquipment, deleleEquipment } from 'services/hebergementsService'

function QuantityRoomInput({ title, equipments, id }) {
  const [editMode, setEditMode] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(false)
  const [inputsValue, setInputsValue] = React.useState(equipments)
  const [notify] = useToast()

  const saveRoomChanges = async () => {
    setIsLoading(true)
    let UpdateArray = []
    for (const item of inputsValue) {
      if (item.checked !== item.intialState) {
        if (item.checked) {
          const body = {
            room_id: id,
            type_id: item.id
          }
          const { equipment } = await addEquipment(body)
          UpdateArray.push({
            localId: item.id,
            newId: equipment.id
          })
        } else {
          await deleleEquipment(item.room_equipment_id)
          UpdateArray.push({ localId: item.id })
        }
      }
    }
    updateEquipment(UpdateArray)
    notify('Équipement mis à jour', 'success')
    setEditMode(!editMode)
    setIsLoading(false)
  }

  const handleChekedPosition = (event) => {
    const change = inputsValue.map((item) =>
      item.id === parseInt(event.target.value)
        ? { ...item, checked: !item.checked }
        : item
    )
    setInputsValue(change)
  }

  const updateEquipment = (changes) => {
    const change = inputsValue.map((item) => {
      const toUpdate = changes.find((c) => c.localId === item?.id)
      if (toUpdate === undefined) {
        return item
      } else {
        return {
          ...item,
          room_equipment_id: toUpdate.newId !== undefined ? toUpdate.newId : '',
          intialState: !item.intialState
        }
      }
    })
    setInputsValue(change)
  }

  return (
    <section className="bg-white mt-4 rounded-3 p-4">
      <div className={'d-flex justify-content-between align-items-center mb-4'}>
        <h3 className="mb-0 font-montserrat">{title}</h3>

        <div className="form-room__question-edit">
          {editMode ? (
            <button
              type="button"
              disabled={isLoading}
              onClick={() => setEditMode(!editMode)}
              className="btn btn-outline-gray border-0 d-flex justify-content-between align-items-center"
            >
              <span className="mr-2">x</span>
              <u>Annuler</u>
            </button>
          ) : (
            <button
              type="button"
              disabled={isLoading}
              onClick={() => setEditMode(!editMode)}
              className="btn btn-outline-secondary border-0"
            >
              <u>Editer</u>
            </button>
          )}
        </div>
      </div>

      <div className="row">
        {inputsValue.map((value_room, index) => (
          <div className="col-12 col-sm-6 col-md-4" key={index}>
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                value={value_room.id}
                id={value_room.id}
                onClick={handleChekedPosition}
                checked={value_room.checked}
                disabled={!editMode || isLoading}
              />
              <label className="form-check-label" htmlFor={value_room.id}>
                {value_room.name}
              </label>
            </div>
          </div>
        ))}
      </div>

      {editMode && (
        <div className="mt-4 py-4 border-top">
          <button
            disabled={isLoading}
            type="button"
            className="btn btn-secondary"
            onClick={saveRoomChanges}
          >
            {isLoading ? 'Loading...' : 'Sauvegarder'}
          </button>
        </div>
      )}
    </section>
  )
}

export default QuantityRoomInput
