import { useToast } from 'hooks/useToast'
import * as React from 'react'
import { updateHebergement } from 'services/hebergementsService'
import { useDispatch, useSelector } from 'react-redux'
import { updateHebergementProp } from '@ducks/hebergements/actions'
import { formatSlug } from 'helpers/roomHelper'

function SectionLayout({
  title,
  type,
  name,
  placeholder = null,
  help = null,
  from
}) {
  const room = useSelector((store) => store.hebergement)
  const dispatch = useDispatch()
  const [editMode, setEditMode] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(false)
  const [inputValue, setInputValue] = React.useState(room[name])
  const [notify] = useToast()
  React.useEffect(() => {
    if (!room.isLoading) {
      setInputValue(room[name])
    }
  }, [room.isLoading])

  const saveRoomChanges = async () => {
    if (from === 'strengths' || from === 'description') {
      if (inputValue.length > 1000) {
        notify(
          `La longueur maximale 
          ${
            from === 'strengths'
              ? 'des points forts'
              : from === 'description'
              ? 'de la description'
              : ''
          } est de 1000 caractères`,
          'error'
        )
        return
      }
      if (inputValue.length <= 0) {
        notify(
          `Le champ 
          ${
            from === 'strengths'
              ? 'des points forts'
              : from === 'description'
              ? 'de la description'
              : ''
          } est obligatoire`,
          'error'
        )
        return
      }
    }

    setIsLoading(true)

    const { establishment, room_image, ...dataToUpdate } = room

    const { message } = await updateHebergement(
      { ...dataToUpdate, [name]: inputValue },
      room.id
    )
    if (message === 'Room updated successfuly!') {
      dispatch(
        updateHebergementProp({
          [name]: inputValue
        })
      )

      setEditMode(!editMode)
      notify('Informations mises à jour', 'success')
    } else {
      setInputValue(room[name])
      notify('il y a eu un problème, veuillez réessayer plus tard', 'error')
    }
    setIsLoading(false)
  }

  const handleEditMode = () => {
    if (inputValue.length <= 0) {
      notify(
        `Le champ 
        ${
          from === 'strengths'
            ? 'des points forts'
            : from === 'description'
            ? 'de la description'
            : ''
        } est obligatoire`,
        'error'
      )
      return
    }
    setEditMode(!editMode)
  }

  return (
    <section className="bg-white mt-4 rounded-3 p-4">
      <div className={'d-flex justify-content-between align-items-center mb-4'}>
        <h3 className="mb-0 font-montserrat">{title}</h3>
        <div className="form-room__question-edit">
          {editMode ? (
            <button
              type="button"
              onClick={handleEditMode}
              className="btn btn-outline-gray border-0 d-flex justify-content-between align-items-center"
            >
              <span className="mr-2">x</span>
              <u>Annuler</u>
            </button>
          ) : (
            <button
              type="button"
              onClick={() => setEditMode(!editMode)}
              className="btn btn-outline-secondary border-0"
            >
              <u>Editer</u>
            </button>
          )}
        </div>
      </div>

      {(from === 'strengths' || from === 'description') && editMode && (
        <span
          className={`form-room__question-edit-char ${
            !inputValue ? '0' : inputValue.length > 1000 ? 'color-red' : ''
          }`}
        >{`${!inputValue ? '0' : inputValue.length}/1000`}</span>
      )}
      <div className="">
        <div>
          {type === 'textarea' && (
            <textarea
              className="form-control"
              name={name}
              rows="5"
              required
              defaultValue={inputValue}
              disabled={!editMode || isLoading}
              onChange={(e) => {
                setInputValue(e.target.value)
              }}
            />
          )}
          {type !== 'textarea' && (
            <input
              style={{ height: '3.5rem' }}
              type={type}
              className="form-control"
              name={name}
              defaultValue={inputValue}
              placeholder={placeholder}
              disabled={!editMode || isLoading}
              onChange={(e) => {
                setInputValue(e.target.value)
              }}
            />
          )}
        </div>
      </div>

      {editMode && (
        <div className="mt-4 py-4 border-top">
          {help !== null && (
            <div>
              <h4 className="color-secondary">Conseil : </h4>
              <p>{help}</p>
            </div>
          )}

          <button
            disabled={isLoading}
            type="button"
            className="btn btn-secondary"
            onClick={saveRoomChanges}
          >
            {isLoading ? 'Loading...' : 'Sauvegarder'}
          </button>
        </div>
      )}
    </section>
  )
}

export default SectionLayout
