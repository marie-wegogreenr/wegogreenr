import { useToast } from 'hooks/useToast'
import * as React from 'react'
import { updateHebergement } from 'services/hebergementsService'
import { useDispatch, useSelector } from 'react-redux'
import { updateHebergementProp } from '@ducks/hebergements/actions'

function EditDiscountInput({}) {
  const room = useSelector((store) => store.hebergement)
  const dispatch = useDispatch()
  const [editMode, setEditMode] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(false)
  const [weekDiscount, setweekDiscount] = React.useState(
    room['seven_days_discount']
  )
  const [monthDiscount, setmonthDiscount] = React.useState(
    room['month_discount']
  )
  const [notify] = useToast()

  const saveRoomChanges = async () => {
    setIsLoading(true)

    const { establishment, room_image, ...dataToUpdate } = room

    const { message } = await updateHebergement(
      {
        ...dataToUpdate,
        seven_days_discount: Number(weekDiscount),
        month_discount: Number(monthDiscount)
      },
      room.id
    )
    if (message === 'Room updated successfuly!') {
      dispatch(
        updateHebergementProp({
          seven_days_discount: Number(weekDiscount),
          month_discount: Number(monthDiscount)
        })
      )

      setEditMode(!editMode)
      notify('Informations mises à jour', 'success')
    } else {
      setIsLoading(false)
      setInputValue(room[name])
      notify('il y a eu un problème, veuillez réessayer plus tard', 'error')
    }
    setIsLoading(false)
  }

  return (
    <section className="bg-white mt-4 rounded-3 p-4">
      <div className={'d-flex justify-content-between align-items-center mb-4'}>
        <h3 className="mb-0 font-montserrat">
          {' '}
          Combien de temps les voyageurs peuvent-ils rester ?
        </h3>

        <div className="form-room__question-edit">
          {editMode ? (
            <button
              type="button"
              onClick={() => setEditMode(!editMode)}
              className="btn btn-outline-gray border-0 d-flex justify-content-between align-items-center"
            >
              <span className="mr-2">x</span>
              <u>Annuler</u>
            </button>
          ) : (
            <button
              type="button"
              onClick={() => setEditMode(!editMode)}
              className="btn btn-outline-secondary border-0"
            >
              <u>Editer</u>
            </button>
          )}
        </div>
      </div>

      <div className="">
        <p className="form-room__prix-text">
          Incitez vos voyageurs à rester plus longtemps en proposant des tarifs
          nuitées dégressifs.
        </p>

        <div className="row">
          <div className="col-12 col-sm-6">
            <div className="form-group">
              <label
                htmlFor="seven_days_discount"
                className="form-room__prix-text"
              >
                Réduction pour tous les 7 jours
              </label>
              <div className="input-group input-group-lg">
                <input
                  type="number"
                  onWheel={(e) => e.target.blur()}
                  className="form-control"
                  id="seven_days_discount"
                  name="seven_days_discount"
                  aria-describedby="seven_days_discount_help"
                  placeholder=""
                  defaultValue={weekDiscount !== null ? weekDiscount : ''}
                  min="0"
                  disabled={!editMode}
                  onChange={(e) => {
                    setweekDiscount(e.target.value)
                  }}
                />
              </div>
              <small
                id="seven_days_discount_help"
                className="form-text text-muted form-room__prix-text--small"
              >
                Conseil : 15%
              </small>
            </div>
          </div>
          <div className="col-12 col-sm-6">
            <div className="form-group">
              <label htmlFor="month_discount" className="form-room__prix-text">
                Remise pour chaque mois
              </label>
              <div className="input-group input-group-lg">
                <input
                  type="number"
                  onWheel={(e) => e.target.blur()}
                  className="form-control"
                  id="month_discount"
                  name="month_discount"
                  aria-describedby="month_discount_help"
                  placeholder=""
                  defaultValue={monthDiscount !== null ? monthDiscount : ''}
                  min="0"
                  disabled={!editMode}
                  onChange={(e) => {
                    setmonthDiscount(e.target.value)
                  }}
                />
              </div>
              <small
                id="month_discount_help"
                className="form-text text-muted form-room__prix-text--small"
              >
                Conseil : 15%
              </small>
            </div>
          </div>
        </div>
      </div>

      {editMode && (
        <div className="mt-4 py-4 border-top">
          <div>
            <h4 className="color-secondary">Conseil : </h4>
            <p className="form-room__prix-text">
              Avec l’avènement du travail à distance, certains voyageurs vont
              privilégier des lieux où ils pourront rester plusieurs semaines.
            </p>
          </div>
          <button
            disabled={isLoading}
            type="button"
            className="btn btn-secondary"
            onClick={saveRoomChanges}
          >
            {isLoading ? 'Loading...' : 'Sauvegarder'}
          </button>
        </div>
      )}
    </section>
  )
}

export default EditDiscountInput
