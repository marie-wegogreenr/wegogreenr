import * as React from 'react'

/* Next */
import Link from 'next/link'
import { formatDate } from 'utils'
import ReservationRowModal from './../myReservations/ReservationRowModal'
import moment from 'moment'
import { getBackgroundStyle, getStatusMessage } from 'helpers/utilsHelper'
import { DropdownReservationHost } from '@components/dropdown/DropdownReservationHost'
// moment.locale('fr')

function ReservationToValidate({
  name,
  initialDate,
  finalDate,
  peopleCapacity,
  bedType,
  imageRoom,
  reservation
}) {
  function getStatus(number) {
    switch (number) {
      case 0:
        return { text: 'Sans payer', color: 'primary' }

      case 1:
        return { text: 'Payé', color: 'success' }

      case 2:
        return { text: 'Annulées', color: 'danger' }
    }
  }

  const [modalOpen, setModalOpen] = React.useState(false)

  return (
    <div className="row reservation__validate">
      <div className="col-12 col-md-3 p-0">
        <div className="d-flex align-items-center mb-md-0 mb-2">
          <figure className="mb-0 mr-3">
            <img
              width="60"
              height="60"
              className="rounded-circle"
              src={imageRoom}
            />
          </figure>

          <div>
            <p className="mb-0">
              <strong>{name}</strong>
            </p>
          </div>
        </div>
      </div>

      <div className="col-12 col-md-6 p-0">
        <div className="mb-md-0 mb-2">
          <p className="mb-0 color-gray reservation__validate-text">
            Info. Réservation
          </p>
          <div className="d-flex">
            <p className="mb-0 reservation__validate-text">
              <span>{peopleCapacity}</span>{' '}
              <span>{`${peopleCapacity > 1 ? 'voyageurs' : 'voyageur'}`}</span>
            </p>

            <div className="border-left mx-2" />

            <p className="mb-0 reservation__validate-text">
              {moment(initialDate).format('DD/MM/YYYY')}
            </p>
            <p className="mb-0 ml-4 reservation__validate-text">
              {moment(finalDate).format('DD/MM/YYYY')}
            </p>
            <div className="border-left mx-2" />
            <span
              className={`custom-badge badge badge-color-${getBackgroundStyle(
                reservation.status
              )} mb-0`}
            >
              {getStatusMessage(reservation.status)}
            </span>
            <div className="border-left mx-2" />

            <p className="mb-0">{bedType}</p>
          </div>
        </div>
      </div>

      <div className="col-12 col-md-3 p-0">
        <div className="col-12 col-md-2">
          <DropdownReservationHost
            openModal={setModalOpen}
            reservation={reservation}
          />
          {modalOpen && (
            <ReservationRowModal
              setModal={setModalOpen}
              reservation={reservation}
            />
          )}
        </div>
      </div>
    </div>
  )
}

export default ReservationToValidate
