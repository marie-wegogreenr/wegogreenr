import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Next */
import Link from 'next/link'

function Message({ finalDate, initialDate, name, peopleCapacity, bedType }) {
  return (
    <div className="row mb-5">
      <div className="col-12 col-md-3">
        <div className="d-flex align-items-center mb-md-0 mb-2">
          <figure className="mb-0 mr-3">
            <img
              width="60"
              height="60"
              className="rounded-circle"
              src="https://picsum.photos/200"
            />
          </figure>

          <div>
            <p className="mb-0">
              <strong>{name}</strong>
              <span className="d-block color-gray">Il y a un jour</span>
            </p>
          </div>
        </div>
      </div>

      <div className="col-12 col-md-6">
        <div className="mb-md-0 mb-2">
          <p className="mb-0 color-gray">Info. Réservation</p>
          <div className="d-flex">
            <p className="mb-0">
              <span>{peopleCapacity}</span> <span>guests</span>
            </p>

            <div className="border-left mx-2" />

            <p className="mb-0">
              {initialDate} - {finalDate}
            </p>

            <div className="border-left mx-2" />

            <p className="mb-0">{bedType}</p>
          </div>
        </div>
      </div>
      <div className="col-12 col-md-3 align-self-end">
        <div className="mb-3">
          <Link href="#">
            <a className="btn btn-outline-secondary border-0">Répondre</a>
          </Link>
        </div>
      </div>
    </div>
  )
}

export default Message
