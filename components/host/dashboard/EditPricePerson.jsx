import { useToast } from 'hooks/useToast'
import * as React from 'react'
import { updateHebergement } from 'services/hebergementsService'
import { useDispatch, useSelector } from 'react-redux'
import { updateHebergementProp } from '@ducks/hebergements/actions'
import {
  adjustTypeToNumber,
  convertPrices,
  inputOnlyNumberComma,
  validatePrices
} from 'helpers/utilsHelper'

function EditPricePerson({
  title,
  name,
  subtitle,
  placeholder = null,
  help = null
}) {
  const room = useSelector((store) => store.hebergement)
  const dispatch = useDispatch()
  const [editMode, setEditMode] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(false)
  const [inputValue, setInputValue] = React.useState(
    room[name]?.toString().replace('.', ',')
  )

  const [peoplequantity, setPeoplequantity] = React.useState(
    room['extra_people_capacity']
  )
  const [notify] = useToast()
  React.useEffect(() => {
    if (!room.isLoading) {
      setInputValue(room[name]?.toString().replace('.', ','))
    }
  }, [room.isLoading])

  const saveRoomChanges = async () => {
    setIsLoading(true)
    const valid = validatePrices(inputValue.toString().replace('.', ','))
    if (peoplequantity <= 0 || peoplequantity > room['people_capacity']) {
      setIsLoading(false)
      return
    }

    if (!valid) {
      notify(
        `Corrigez les prix. Vous ne pouvez pas écrire plus d'une virgule.`,
        'error'
      )
      setIsLoading(false)
      return
    } else {
      setInputValue(parseFloat(convertPrices(inputValue)))
    }

    const {
      establishment,
      room_image,
      basic_price,
      weekend_price,
      ...dataToUpdate
    } = room

    const _basic_price = adjustTypeToNumber(basic_price)
    const _weekend_price = adjustTypeToNumber(weekend_price)

    const { message } = await updateHebergement(
      {
        ...dataToUpdate,
        basic_price: _basic_price,
        weekend_price: _weekend_price,
        extra_people_capacity: peoplequantity,
        [name]: parseFloat(convertPrices(inputValue))
      },
      room.id
    )
    if (message === 'Room updated successfuly!') {
      dispatch(
        updateHebergementProp({
          basic_price: _basic_price,
          weekend_price: _weekend_price,
          extra_people_capacity: peoplequantity,
          [name]: inputValue
        })
      )

      setEditMode(!editMode)
      notify('Informations mises à jour', 'success')
    } else {
      setIsLoading(false)
      setInputValue(room[name]?.toString().replace('.', ','))
      notify('il y a eu un problème, veuillez réessayer plus tard', 'error')
    }
    setIsLoading(false)
  }

  const handleInputChange = (e) => {
    const isNumberWithComma = inputOnlyNumberComma(e.target.value)
    if (isNumberWithComma) {
      setInputValue(e.target.value)
    } else {
      setInputValue(e.target.value.slice(0, -1))
    }
  }

  return (
    <section className="bg-white mt-4 rounded-3 p-4">
      <div className={'d-flex justify-content-between align-items-center mb-4'}>
        <h3 className="mb-0 font-montserrat">{title}</h3>

        <div className="form-room__question-edit">
          {editMode ? (
            <button
              type="button"
              onClick={() => {
                setEditMode(!editMode)
                setPeoplequantity(room['extra_people_capacity'])
                setInputValue(room[name]?.toString().replace('.', ','))
              }}
              className="btn btn-outline-gray border-0 d-flex justify-content-between align-items-center"
            >
              <span className="mr-2">x</span>
              <u>Annuler</u>
            </button>
          ) : (
            <button
              type="button"
              onClick={() => setEditMode(!editMode)}
              className="btn btn-outline-secondary border-0"
            >
              <u>Editer</u>
            </button>
          )}
        </div>
      </div>

      <div className="">
        <p className="form-room__prix-text "> Ces prix sont valables pour :</p>
        <div className="row">
          {peoplequantity === '0' && peoplequantity !== '' && (
            <small className="form-room__prix-text mb-2 text-danger">
              Le nombre de personnes doit être supérieur à 0
            </small>
          )}
          {peoplequantity > room['people_capacity'] && (
            <small className="form-room__prix-text mb-2 text-danger">
              Veuillez choisir un nombre ne dépassant pas la capacité maximale
              de voyageurs
            </small>
          )}
          <div className="col-12 col-md-8">
            <div className="input-group input-group-lg rounded">
              <input
                type="text"
                onWheel={(e) => e.target.blur()}
                className="form-control border border-right-0 text-center"
                name={name}
                placeholder=""
                value={peoplequantity}
                min="1"
                oninput="validity.valid||(value='')"
                step="1"
                disabled={!editMode}
                onChange={(e) => {
                  setPeoplequantity(e.target.value)
                }}
              />
              <div className="input-group-prepend">
                <label htmlFor="price_base" className="input-group-text border">
                  voyageurs
                </label>
              </div>
            </div>
          </div>
        </div>
        <p className="form-room__prix-text mt-4">
          Frais par voyageur additionnel :
        </p>
        <div className="row">
          <div className="col-12 col-md-8">
            <div className="input-group input-group-lg rounded">
              <input
                type="text"
                onWheel={(e) => e.target.blur()}
                className="form-control border border-right-0 text-center"
                name={name}
                placeholder=""
                value={inputValue?.toString().replace('.', ',')}
                defaultValue={inputValue?.toString().replace('.', ',')}
                min="1"
                oninput="validity.valid||(value='')"
                step=".01"
                disabled={!editMode}
                onChange={handleInputChange}
              />
              <div className="input-group-prepend">
                <label htmlFor="price_base" className="input-group-text border">
                  € / Nuit
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      {editMode && (
        <div className="mt-4 py-4 border-top">
          {help !== null && (
            <div>
              <h4 className="color-secondary">Conseil : </h4>
              {help}
            </div>
          )}

          <button
            disabled={isLoading || !inputValue || inputValue == 0}
            type="button"
            className="btn btn-secondary"
            onClick={saveRoomChanges}
          >
            {isLoading ? 'Loading...' : 'Sauvegarder'}
          </button>
        </div>
      )}
    </section>
  )
}

export default EditPricePerson
