import { useToast } from 'hooks/useToast'
import * as React from 'react'
import { updateHebergement } from 'services/hebergementsService'
import { useDispatch, useSelector } from 'react-redux'
import { updateHebergementProp } from '@ducks/hebergements/actions'

function EditNuitsInput({}) {
  const room = useSelector((store) => store.hebergement)
  const dispatch = useDispatch()
  const [editMode, setEditMode] = React.useState(false)
  const [isLoading, setIsLoading] = React.useState(false)
  const [minNight, setminNight] = React.useState(room['min_nigth'])
  const [maxNight, setmaxNight] = React.useState(room['max_nigth'])
  const [notify] = useToast()

  const saveRoomChanges = async () => {
    setIsLoading(true)

    const { establishment, room_image, ...dataToUpdate } = room

    const { message } = await updateHebergement(
      {
        ...dataToUpdate,
        min_nigth: Number(minNight),
        max_nigth: Number(maxNight)
      },
      room.id
    )
    if (message === 'Room updated successfuly!') {
      dispatch(
        updateHebergementProp({
          min_nigth: Number(minNight),
          max_nigth: Number(maxNight)
        })
      )

      setEditMode(!editMode)
      notify('Informations mises à jour', 'success')
    } else {
      setIsLoading(false)
      setInputValue(room[name])
      notify('il y a eu un problème, veuillez réessayer plus tard', 'error')
    }
    setIsLoading(false)
  }

  return (
    <section className="bg-white mt-4 rounded-3 p-4">
      <div className={'d-flex justify-content-between align-items-center mb-4'}>
        <h3 className="mb-0 font-montserrat">
          {' '}
          Combien de temps les voyageurs peuvent-ils rester ?
        </h3>

        <div className="form-room__question-edit">
          {editMode ? (
            <button
              type="button"
              onClick={() => setEditMode(!editMode)}
              className="btn btn-outline-gray border-0 d-flex justify-content-between align-items-center"
            >
              <span className="mr-2">x</span>
              <u>Annuler</u>
            </button>
          ) : (
            <button
              type="button"
              onClick={() => setEditMode(!editMode)}
              className="btn btn-outline-secondary border-0"
            >
              <u>Editer</u>
            </button>
          )}
        </div>
      </div>

      <div className="">
        <p className="form-room__prix-text">
          Définissez le nombre minimum et maximum de nuits réservables lors
          d’une unique réservation.
        </p>
        <div className="col-12 row form-room__limit-nights">
          <div className="col-12 col-xl-5 card-button-nights mt-3 me-2">
            <div className="row">
              <div className="col-8 mt-3 input-night">
                <input
                  type="number"
                  onWheel={(e) => e.target.blur()}
                  value={minNight}
                  className="number-styled-input"
                  disabled={!editMode}
                  onChange={(e) => {
                    setminNight(parseInt(e.target.value))
                  }}
                />
                <p className="form-room__prix-text  ml-2">
                  {`${minNight <= 1 ? 'Nuit' : 'Nuits'} minimum`}
                </p>
              </div>
              <div className="col-4 buttons-night">
                <button
                  className="button-left"
                  type="button"
                  name="nights_min"
                  onClick={(e) => {
                    let input = minNight
                    if (input > 1) input = input - 1
                    setminNight(input)
                  }}
                  disabled={!editMode}
                >
                  -
                </button>
                <button
                  className="button-right"
                  type="button"
                  name="nights_min"
                  onClick={(e) => {
                    let input = minNight
                    if (maxNight > minNight) input = input + 1
                    setminNight(input)
                  }}
                  disabled={!editMode}
                >
                  +
                </button>
              </div>
            </div>
          </div>
          <div className="col-12 col-xl-5 card-button-nights mt-3">
            <div className="row">
              <div className="col-8 mt-3 input-night">
                <input
                  type="number"
                  onWheel={(e) => e.target.blur()}
                  value={maxNight}
                  className="number-styled-input"
                  disabled={!editMode}
                  onChange={(e) => {
                    setmaxNight(parseInt(e.target.value))
                  }}
                />
                <p className="form-room__prix-text ml-2">
                  {`${maxNight <= 1 ? 'Nuit' : 'Nuits'} maximum`}
                </p>
              </div>
              <div className="col-4 buttons-night">
                <button
                  className="button-left"
                  type="button"
                  name="nights_max"
                  onClick={(e) => {
                    let input = maxNight
                    if (input > 0) input = input - 1
                    setmaxNight(input)
                  }}
                  disabled={!editMode}
                >
                  -
                </button>
                <button
                  className="button-right"
                  type="button"
                  name="nights_max"
                  onClick={(e) => {
                    let input = maxNight
                    input = input + 1
                    setmaxNight(input)
                  }}
                  disabled={!editMode}
                >
                  +
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      {editMode && (
        <div className="mt-4 py-4 border-top">
          <div>
            <h4 className="color-secondary">Conseil : </h4>
            <p className="form-room__prix-text">
              Avec l’avènement du travail à distance, certains voyageurs vont
              privilégier des lieux où ils pourront rester plusieurs semaines.
            </p>
          </div>
          <button
            disabled={isLoading}
            type="button"
            className="btn btn-secondary"
            onClick={saveRoomChanges}
          >
            {isLoading ? 'Loading...' : 'Sauvegarder'}
          </button>
        </div>
      )}
    </section>
  )
}

export default EditNuitsInput
