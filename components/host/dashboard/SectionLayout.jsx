import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Nextjs */
import Link from 'next/link'

function LayoutSections({ children, title, route }) {
  return (
    <div className="host-dashboard__pane p-4 bg-white rounded-3">
      {/* HEADER */}
      <div className="d-flex justify-content-between align-items-center pb-4 subtitle-dashboard-wrapper">
        <h2
          className="subtitle-dashboard weight-bold mr-0 mr-lg-4 font-montserrat"
          style={{ color: 'black' }}
        >
          {title}
        </h2>

        <div>
          <Link href={route}>
            <a className="btn btn-outline-secondary btn-home-1">Voir tout</a>
          </Link>
        </div>
      </div>

      <div className="border-bottom" />

      {/* CONTENT */}
      <div className="host-dashboard__pane__content mt-4 overflow-fixed">
        {children}
      </div>
    </div>
  )
}

export default LayoutSections
