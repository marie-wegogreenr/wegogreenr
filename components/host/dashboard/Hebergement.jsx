import * as React from 'react'
import noImage from './../../../public/images/not-image.svg'
/* Next */
import Link from 'next/link'
import { adjustNumberWithComma } from 'helpers/utilsHelper'

function Hebergement({ name, price, id, principal_image, active }) {
  return (
    <div className="row m-0 mb-4" style={{ alignItems: 'center' }}>
      <div className="col-4 col-sm-3 col-md-2 col-xl-3 order-2 order-sm-1 ps-0">
        <figure className="mb-0 shadow mr-2 w-100">
          <img
            style={{ objectFit: 'cover' }}
            className="rounded-3 w-100"
            width="92"
            height="75"
            src={
              principal_image === undefined
                ? noImage
                : 'https://wegogreenr-laravel.s3.eu-west-2.amazonaws.com/' +
                  principal_image
            }
          />
        </figure>
      </div>
      <div className="col-8 col-sm-6 col-xl-5 order-3 order-sm-2 pe-0">
        <div className="mr-2">
          {active == 1 ? (
            <span className="badge badge-color-success">Publiée</span>
          ) : (
            <span className="badge badge-color-warning">Non publiée</span>
          )}
          <p
            className="mb-0"
            style={{ lineHeight: 1.15 + 'em', marginTop: 5 + 'px' }}
          >
            <strong>{name === null ? 'Room name' : name}</strong>
          </p>

          <p className="mb-0" style={{ fontSize: 14 + 'px', fontWeight: 500 }}>
            <span>{price === null ? 0 : adjustNumberWithComma(price)}</span>€ x
            nuit
          </p>
        </div>
      </div>

      <div
        className="col-12 col-sm-3 col-xl-4 order-1 order-sm-3 p-0"
        style={{ textAlign: 'right' }}
      >
        <Link href={'/host/mes-hebergements/' + id}>
          <a className="border-0" style={{ margin: 0, textDecoration: 'none' }}>
            Voir chambre
          </a>
        </Link>
      </div>
    </div>
  )
}

export default Hebergement
