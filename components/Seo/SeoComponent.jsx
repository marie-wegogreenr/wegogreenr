import Head from 'next/head'
export const SeoComponent = ({ seo }) => {
  return (
    <Head>
      <title key="metaTitle">{`${seo.metaTitle}  ${seo.description}`}</title>
      <meta property="og:title" content={seo.title} key="ogtitle" />
      <meta
        property="og:description"
        content={seo.description}
        key="ogdescription"
      />
      <meta property="title" content={seo.title} key="title" />
      <meta
        property="description"
        content={seo.description}
        key="description"
      />
    </Head>
  )
}
