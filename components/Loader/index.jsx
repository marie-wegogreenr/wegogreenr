import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Styles */
import { Container } from './loader.styles'

function Loader() {
  return (
    <Container>
      <div className="lds-facebook">
        <div />
        <div />
        <div />
      </div>
    </Container>
  )
}

export default Loader
