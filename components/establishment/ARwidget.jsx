import Head from 'next/head'

const ARwidget = ({ code, reference }) => {
  return (
    <>
      <Head>
        <script
          type="text/javascript"
          src="//gadget.open-system.fr/widgets-libs/rel/noyau-1.0.min.js"
        ></script>
        <script type="text/javascript">
          {(function () {
            setTimeout(() => {
              var widgetProduit = window.AllianceReseaux?.Widget.Instance(
                'Produit',
                {
                  idPanier: 'Ul1RUlI',
                  idIntegration: 1457,
                  langue: 'fr',
                  ui: code
                }
              )
              widgetProduit.Initialise()
            }, 2000)
          })()}
        </script>
      </Head>
      <div className="py-4">
        <div id={`widget-produit-${code}`} ref={reference}></div>
      </div>
    </>
  )
}

export default ARwidget
