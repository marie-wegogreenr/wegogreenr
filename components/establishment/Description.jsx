import * as React from 'react'
import { consoleLog } from '/utils/logConsole'

/* Components */
import {
  Arrive,
  Cutlery,
  Depart,
  DisabledPerson,
  Home,
  Pet,
  Users
} from 'components/Icons'

import dynamic from 'next/dynamic'

import { getImageURL } from 'utils'
import { findBreakfastCategory } from 'helpers/roomServicesHelper'

const DynamicMap = dynamic(() => import('@components/Maps/Map'), { ssr: false })

function Description({
  establishment,
  services,
  conditions,
  conditionsStablishment,
  widgetReference
}) {
  const { description, lat, lng, contact_name, name } = establishment
  const BreakFastCategory = findBreakfastCategory(services)
  const [animalCondition, setAnimalCondition] = React.useState(false)

  React.useEffect(() => {
    let conditionArray = []

    if (conditions && conditionsStablishment) {
      if (conditions.length > 0 && conditionsStablishment.length > 0) {
        conditionArray = conditionsStablishment.filter(
          (item) => item.condition.id === conditions[2].id
        )
      }
    }

    const conditionAnimal = conditionArray.length > 0 ? true : false

    setAnimalCondition(conditionAnimal)
  }, [])

  return (
    <>
      <div className="d-md-flex justify-content-between align-items-center">
        <div>
          <h2 className="font-fraunces size-h2">Description</h2>
          <h3 className="font-montserrat weight-semibold lh-20 fz-18">
            {name}
          </h3>
        </div>

        <div className="d-flex align-items-center">
          <button
            style={{ padding: '0 6rem 0 6rem' }}
            className="btn btn-secondary mr-5 py-4 "
            onClick={() => {
              widgetReference.current.scrollIntoView()
            }}
          >
            Réserver
          </button>
          <img
            className="rounded-circle mr-2 navbar-nav__profile-image"
            src={getImageURL(establishment?.user?.photo_url)}
            height="50px"
            width="50px"
          />

          <h5 className="mb-0 font-montserrat  ms-2 text">{contact_name}</h5>
        </div>
      </div>

      <div className="border-bottom my-1-5" />

      <div>
        <p className="font-montserrat fz-15">{description}</p>
      </div>

      <div className="border-bottom my-1-5" />

      <div className="row">
        <div className="col-md-6">
          <div className="mb-3 fz-15 d-flex align-items-center">
            <Arrive className="mr-10" height="20px" width="20px" />
            <span className="font-montserrat color-black">
              Arrivée : {establishment?.max_entry_time?.slice(0, 2)}h
            </span>
          </div>

          <div className="mb-3 fz-15 d-flex align-items-center">
            <Cutlery height="20px" width="20px" className="mr-10" />
            <span className="font-montserrat color-black">
              Petit-déjeuner : {BreakFastCategory}
            </span>
          </div>
        </div>
        <div className="col-md-6">
          <div className="mb-3 fz-15 d-flex align-items-center">
            <Depart height="20px" width="20px" className="mr-10" />
            <span className="font-montserrat color-black">
              Départ : {establishment?.max_exit_time?.slice(0, 2)}h
            </span>
          </div>
          <div className="mb-3 fz-15 d-flex align-items-center">
            <Pet className="mr-10" width="20px" height="20px" />
            <span className="font-montserrat color-black">
              Animaux : {animalCondition ? 'autorisés' : 'non autorisés'}
            </span>
          </div>
        </div>
      </div>

      {/* <div className="mt-4">
        <h4 className="mb-3 font-montserrat fz-20">Ses points forts</h4>

        <div>
          <p className="font-montserrat">strengths</p>
        </div>
      </div> */}

      <div className=" bg-light my-4">
        <div className="" style={{ height: '385px' }}>
          <DynamicMap center={{ lat, lng }} />
        </div>
      </div>
      <div className="card bg-primary text-white card-white">
        <div className="">
          <div className="d-flex align-items-center">
            <img
              width="92px"
              height="92px"
              className="rounded-circle navbar-nav__profile-image"
              src={getImageURL(establishment?.user?.photo_url)}
              alt=""
            />
            <div className="ml-4">
              <h5 className="card-title fz-28 mb-0 weight-bold color-white">
                {establishment?.user?.name}
              </h5>
            </div>
          </div>

          <p className="mt-4 text-white font-montserrat paragraph ">
            {establishment?.user?.host_information?.presentation}
          </p>
        </div>
      </div>
    </>
  )
}

export default Description
