import React, { useRef, useState } from 'react'
import Slider from 'react-slick'
import { useEffect } from 'react'

const SliderPictures = ({
  images = [],
  id,
  heigth = 150,
  width = 291,
  className = '',
  extraSettings,
  indexSelected = 0
}) => {
  const [imagesDimensions, setImagesDimensions] = useState(images)
  const slider1 = useRef()

  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    arrows: true,
    adaptiveHeight: true,
    slickGoTo: indexSelected,
    initialSlide: indexSelected,
    ...extraSettings,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          dots: false
        }
      }
    ]
  }

  useEffect(() => {
    slider1.current.slickGoTo(indexSelected)
  }, [])

  return (
    <>
      <div className="modal-content modal-content__slider-photos">
        <div className="search-modal-body modal-body">
          <Slider ref={(slider) => (slider1.current = slider)} {...settings}>
            {imagesDimensions.map((item, index) => (
              <div key={index} className="d-flex justify-content-center">
                <img
                  id={`slider_picture_${index}`}
                  className={`rounded-3 ${className} slick-slide__img-modal`}
                  src={item}
                  alt={`slider_picture_${index}`}
                />
              </div>
            ))}
          </Slider>
        </div>
      </div>
      <div className="text-center py-4"></div>
    </>
  )
}

export default SliderPictures
