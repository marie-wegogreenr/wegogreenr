import Link from 'next/link'
import React from 'react'
import { consoleLog } from '/utils/logConsole'
import UserCard from '../conversations/UserCard'
import MessageCard from './MessageCard'

const lorem = 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'

const Conversations = () => {
  return (
    <div className="card-white  ">
      <p className="subtitle-dashboard weight-bold">Mes conversations</p>
      <div className="mt-5 ">
        <MessageCard name="juan" message={lorem} />
        <div className="border-bottom mb-3"></div>
        <MessageCard name="juan" message={lorem} />
        <div className="border-bottom mb-3"></div>
        <MessageCard name="juan" message={lorem} />
      </div>
      <div className="text-center mt-4">
        <Link href="/user/mes-conversations">
          <a className="weight-semibold underline-0">Accéder à la messagerie</a>
        </Link>
      </div>
    </div>
  )
}

export default Conversations
