import Link from 'next/link'
import React, { useEffect } from 'react'
import { consoleLog } from '/utils/logConsole'
import ReservationsContent from './ReservationsContent'
import ReservationsTabs from './ReservationsTabs'
//redux
import { useDispatch, useSelector } from 'react-redux'
import {
  getUserReservationsAction,
  getUserReservationsAditionalAction
} from '@ducks/userDuck'

const Reservations = () => {
  const dispatch = useDispatch()

  const storeUser = useSelector((store) => store.user.user.id)
  const reservationsLoaded = useSelector((store) => store.user.reservations)

  useEffect(() => {
    dispatch(getUserReservationsAction())
  }, [storeUser])

  useEffect(() => {
    //consoleLog(reservationsLoaded, 'reser loaded')
    if (reservationsLoaded.length > 0) {
      dispatch(getUserReservationsAditionalAction())
    }
  }, [reservationsLoaded])

  return (
    <div className="row justify-content-between reservation-user">
      <div className="col-12 px-0 d-flex align-items-center justify-content-between flex-column flex-lg-row">
        <div className="d-flex align-items-center flex-column flex-md-row flex-lg-row">
          <p className="subtitle-dashboard weight-bold mb-0 mr-0 mr-lg-4">
            Mes réservations
          </p>
          <Link href="/user/mes-reservations">
            <a className="fz-15 underline-0 weight-semibold  mt-lg-1">
              Voir toutes les réservations
            </a>
          </Link>
        </div>
        <div>
          <ReservationsTabs />
        </div>
      </div>
      <div className="reservation-user__divider"></div>
      <ReservationsContent />
    </div>
  )
}

export default Reservations
