import React from 'react'
import { consoleLog } from '/utils/logConsole'
import LocationCard from './LocationCard'
import { dateFromDb } from 'helpers/timeHelper'

const ReservationsListPrevious = ({ data = [] }) => {
  const today = new Date()

  return (
    <>
      {data &&
        data.length > 0 &&
        data.map((item, index) => {
          if (
            today.getTime() < dateFromDb(item.departure_date).getTime() ||
            index >= 5
          )
            return <div key={index}></div>

          return <LocationCard data={item} key={index} />
        })}
    </>
  )
}

export default ReservationsListPrevious
