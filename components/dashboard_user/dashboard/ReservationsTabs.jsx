import React from 'react'
import { consoleLog } from '/utils/logConsole'

const ReservationsTabs = () => {
  return (
    <ul className="nav  mt-2" id="myTab" role="tablist">
      <li className="nav-item text-block">
        <a
          className="nav-link active color-black weight-bold"
          id="home-tab"
          data-toggle="tab"
          href="#home"
          role="tab"
          aria-controls="home"
          aria-selected="true"
        >
          A venir
        </a>
      </li>
      <li className="nav-item">
        <a
          className="nav-link text-block color-black weight-bold"
          id="profile-tab"
          data-toggle="tab"
          href="#profile"
          role="tab"
          aria-controls="profile"
          aria-selected="false"
        >
          Passées
        </a>
      </li>
    </ul>
  )
}

export default ReservationsTabs
