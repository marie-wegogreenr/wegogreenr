import Image from 'next/image'
import Link from 'next/link'
import React from 'react'
import { consoleLog } from '/utils/logConsole'

//icons
import { EmptyCalendar, SingleUser, Coin, Heart } from '@components/Icons'

import { cutText } from 'utils'
import { getStatusMessage } from 'helpers/utilsHelper'

const LocationCard = ({ data }) => {
  const {
    arrival_date,
    departure_date,
    number_of_adults,
    number_of_children,
    room_price,
    room: { public_name, ...all },
    status,
    code,
    images,
    payment
  } = data

  const totalUsers = number_of_adults + number_of_children

  function formatDate(date) {
    const newDate = date.split('-')
    return `${newDate[2].slice(0, 2)}/${newDate[1]}/${newDate[0]}`
  }

  return (
    <div className="row d-flex justify-content-between card-white px-1 user-locationCard">
      <div className="user-locationCard-image">
        {images && (
          <Image
            src={`${process.env.NEXT_PUBLIC_AMAZON_IMGS}${images[0]?.image?.url}`}
            width={293}
            height={150}
            className="rounded-2 w-100 obj-fit-cover"
          />
        )}
      </div>
      <div className="user-locationCard-info">
        <p className="subtitle weight-bold">{cutText(public_name, 40)}</p>
        <span className="d-flex align-items-center mt-4">
          <EmptyCalendar width="20" height="20" />
          <p className="mb-0 mt-0 ml-2">
            Début : {formatDate(arrival_date)} - Fin : &nbsp;
            {formatDate(departure_date)}
          </p>
        </span>
        <span className="d-flex align-items-center mt-1">
          <SingleUser width="20" height="20" />
          <p className="mb-0 mt-0 ml-2">
            Voyageurs : {totalUsers} personne{totalUsers > 1 && 's'}{' '}
          </p>
        </span>
        <span className="d-flex align-items-center mt-1">
          <Coin width="20" height="20" />
          <p className="mb-0 mt-0 ml-2">Prix total : {room_price} &euro;</p>
        </span>
        <span className="d-flex align-items-center mt-1">
          <Heart width="20" height="20" />
          <p className="mb-0 mt-0 ml-2">Statut : {getStatusMessage(status)}</p>
        </span>
      </div>
      <div className="user-locationCard-button d-flex align-items-center justify-content-center p-0">
        <Link href={`/user/mes-reservations?node=${code}`}>
          <button className="btn btn-outline-secondary btn-home-1 btn-block  mx-4 mt-4 mt-lg-0  text weight-semibold">
            Afficher le détail
          </button>
        </Link>
      </div>
    </div>
  )
}

export default LocationCard
