import Image from 'next/image'
import React from 'react'
import { consoleLog } from '/utils/logConsole'

const MessageCard = ({ name, message, photo }) => {
  return (
    <div className="col-12 rounded my-2">
      <div className="row display-flex align-items-center">
        <div className="col-2 ml-0 px-0">
          <figure>
            <Image
              className="rounded-circle obj-fit-cover"
              src="http://dummyimage.com/499x315.jpg/ff4444/ffffff"
              width={55}
              height={55}
            />
          </figure>
        </div>
        <div className="col-8 col-sm-8">
          <p className="text-block weight-bold m-0">{name}</p>
          <p className="paragraph">{message}</p>
        </div>
        <div className="col-1">
          <div className="rounded-circle p-1 bg-secondary d-flex align-items-center justify-content-center color-white weight-semibold message-notification">
            1
          </div>
        </div>
      </div>
    </div>
  )
}

export default MessageCard
