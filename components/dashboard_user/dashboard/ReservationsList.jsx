import React from 'react'
import { consoleLog } from '/utils/logConsole'
import LocationCard from './LocationCard'

import moment from 'moment'

const ReservationsList = ({ data = [] }) => {
  const today = moment(new Date()).format('YYYY-MM-DD 00:00:00')

  let count = 0
  return (
    <>
      {data &&
        data.length > 0 &&
        data.map((item, index) => {
          if (
            count < 3 &&
            today <= moment(item.arrival_date).format('YYYY-MM-DD 00:00:00')
          ) {
            count++
            return <LocationCard data={item} key={index} />
          }
        })}
    </>
  )
}

export default ReservationsList
