import Link from 'next/link'
import React from 'react'
import { consoleLog } from '/utils/logConsole'
//icons
import { HouseWithLeaf } from '@components/Icons'

const InfoCard = ({
  title,
  content,
  link,
  bgColor = 'primary',
  textColor = 'white'
}) => {
  return (
    <div className={`bg-${bgColor} p-5 rounded-3 `}>
      <HouseWithLeaf color={`${textColor}`} stroke={textColor} width={45} />
      <h3 className={`color-${textColor} subtitle-dashboard font-montserrat`}>
        {title}
      </h3>
      <p className={`color-${textColor} mt-3`}>{content}</p>
      <Link href={link}>
        <a className={`color-${textColor} underline-0 weight-semibold`}>
          Savoir plus &gt;{' '}
        </a>
      </Link>
    </div>
  )
}

export default InfoCard
