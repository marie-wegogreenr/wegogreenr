import Image from 'next/image'
import React from 'react'
import { consoleLog } from '/utils/logConsole'
import { useSelector } from 'react-redux'
import Link from 'next/link'
import { useState } from 'react'

const ProfileCard = () => {
  const store = useSelector((state) => state.user.user)
  const { name, photo_url } = store

  const handleDefaultImage = (e) => {
    e.target.src = '/images/not-image.svg'
  }

  return (
    <div className="row card-white align-items-center px-lg-4 px-0  user-profile__card">
      <div className="col-lg-9 col-12 d-flex align-items-center flex-column flex-lg-row">
        <div className="user-profile__card-photo">
          <img
            className="rounded-circle obj-fit-cover"
            src={`${process.env.NEXT_PUBLIC_AMAZON_IMGS}${photo_url}`}
            width={120}
            height={120}
            onError={handleDefaultImage}
          />
        </div>
        <div className="col-12 col-lg-9 px-0 pl-lg-2 mt-4 ml-0 ml-lg-2">
          <p className="h2 weight-bold font-fraunces color-primary">
            Bonjour {name}
          </p>
          <p className="paragraph mt-3">
            Vous trouverez ici toutes vos réservations à venir et passées.{' '}
            <br />
            Toute l’équipe de We Go GreenR vous souhaite de merveilleux séjours
            !
          </p>
        </div>
      </div>
      <div className="col-lg-3 col-12 p-0">
        <Link href="/user/mon-profil">
          <button className="btn btn-outline-secondary btn-block btn-home-1 text weight-semibold">
            Modifier mon profil
          </button>
        </Link>
      </div>
    </div>
  )
}

export default ProfileCard
