import React from 'react'
import { consoleLog } from '/utils/logConsole'
import ReservationsList from './ReservationsList'
import { useSelector } from 'react-redux'
import ReservationsListPrevious from './ReservationsListPrevious'
import Link from 'next/link'

const ReservationsContent = () => {
  const store = useSelector((store) => store.user.reservations)
  const storeStatus = useSelector((store) => store.user.status)

  return (
    <div className="tab-content" id="myTabContent">
      {store.length < 1 ? (
        <div className="reservation-user-empty">
          <p className="subtitle-dashboard weight-bold font-fraunces">
            Vous n'avez pas encore de réservation
          </p>
          <Link href="/">
            <button className="btn btn-secondary btn-home-1 weight-semibold paragraph reservation-user-empty__button">
              Je réserve mon prochain séjour
            </button>
          </Link>
        </div>
      ) : (
        <>
          <div
            className="tab-pane fade show active"
            id="home"
            role="tabpanel"
            aria-labelledby="home-tab"
            style={{ paddingBottom: '60px' }}
          >
            <ReservationsList data={store} />
          </div>
          <div
            className="tab-pane fade"
            id="profile"
            role="tabpanel"
            aria-labelledby="profile-tab"
            style={{ paddingBottom: '60px' }}
          >
            <ReservationsListPrevious data={store} />
          </div>
        </>
      )}
    </div>
  )
}

export default ReservationsContent
