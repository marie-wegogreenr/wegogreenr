import Image from 'next/image'
import React from 'react'
import { consoleLog } from '/utils/logConsole'

const userCard = ({ name, message, photo }) => {
  return (
    <div className="col-12 rounded my-2">
      <div className="row display-flex align-items-center">
        <div className="col-2">
          <figure>
            <Image
              className="rounded-circle"
              src="http://dummyimage.com/499x315.jpg/ff4444/ffffff"
              width={30}
              height={30}
            />
          </figure>
        </div>
        <div className="col-9">
          <h3>{name}</h3>
          <p>{message}</p>
        </div>
        <div className="col-1">
          <span>1</span>
        </div>
      </div>
    </div>
  )
}

export default userCard
