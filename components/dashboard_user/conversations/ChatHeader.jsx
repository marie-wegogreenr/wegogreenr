import Image from 'next/image'
import React from 'react'
import { consoleLog } from '/utils/logConsole'

const ChatHeader = ({ img, name, place }) => {
  const { image, users, date, location_name } = place

  return (
    <div className="row justify-content-between border">
      <div className="col-6">
        <Image
          className="rounded-circle"
          src="http://dummyimage.com/499x315.jpg/ff4444/ffffff"
          width={50}
          height={50}
        />
        {name}
      </div>
      <div className="col-4">
        <div className="row">
          <div className="col-4">
            <Image className="rounded" src={image} width={100} height={50} />
          </div>
          <div className="col-8">
            <p>{location_name}</p>
            <p>
              {users} guest | {date}
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ChatHeader
