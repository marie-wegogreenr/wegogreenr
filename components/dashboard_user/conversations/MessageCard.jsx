import Image from 'next/image'
import React from 'react'
import { consoleLog } from '/utils/logConsole'

const MessageCard = ({ name, message, time, img }) => {
  return (
    <div className="col-6 card rounded my-4">
      <div className="row display-flex align-items-center">
        <div className="col-2 rounded-cicrle">
          <figure>
            <Image
              className="rounded-circle"
              src="http://dummyimage.com/499x315.jpg/ff4444/ffffff"
              width={30}
              height={30}
            />
          </figure>
        </div>
        <div className="col-9">
          <div className="row justify-content-between line-items">
            <span className="col-2">{name}</span>
            <span className="col-2">{time}</span>
          </div>
          <p>{message}</p>
        </div>
      </div>
    </div>
  )
}

export default MessageCard
