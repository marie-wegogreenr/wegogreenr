import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { useState } from 'react'
import { consoleLog } from '/utils/logConsole'
//styles
import style from './style.module.scss'

const InputMessage = () => {
  const [message, setMessage] = useState({
    message: ''
  })

  const HandleSubmit = (e) => {
    //send or manipulate the message hgere
    //consoleLog(message)
    setMessage({ ...message, message: '' })
  }

  const HandleChange = (e) => {
    setMessage({ ...message, [e.target.name]: e.target.value })
  }

  return (
    <label className={style.inputMessage}>
      <input
        value={message.message}
        name="message"
        type="text"
        onChange={HandleChange}
      />
      <input type="file" name="file" id="file" />
      <button onClick={HandleSubmit}>
        <FontAwesomeIcon icon="bell" size="6x" />
      </button>
    </label>
  )
}

export default InputMessage
