import React from 'react'
import { consoleLog } from '/utils/logConsole'
import MessageCard from './MessageCard'
//STYLES
import style from './style.module.scss'

const lorem = 'lorem ipsusm dolor sit amet'
// const time = new Date
const time = '15:05'

const ChatContainer = () => {
  return (
    <div className={`p-5 ${style.chatContainer}`}>
      <MessageCard name="juan" message={lorem} time={time} />
      <MessageCard name="angelo" message={lorem} time={time} />
      <MessageCard name="juan" message={lorem} time={time} />
      <MessageCard name="angelo" message={lorem} time={time} />
      <MessageCard name="juan" message={lorem} time={time} />
      <MessageCard name="angelo" message={lorem} time={time} />
      <MessageCard name="juan" message={lorem} time={time} />
      <MessageCard name="angelo" message={lorem} time={time} />
      <MessageCard name="juan" message={lorem} time={time} />
      <MessageCard name="angelo" message={lorem} time={time} />
    </div>
  )
}

export default ChatContainer
