import { useEffect, useState } from 'react'
import LocationCard from './LocationCard'
import { dateFromDb } from 'helpers/timeHelper'

const FutureReservations = ({ reservations = [] }) => {
  /*  Local states  */
  const [allReservations, setAllResevations] = useState([])
  /*  Effects  */
  useEffect(() => {
    if (reservations.length > 0) {
      setAllResevations(
        reservations
          .filter(
            (item) =>
              dateFromDb(item.departure_date).getTime() >= new Date().getTime()
          )
          .sort((a, b) => a.status - b.status)
      )
    }
  }, [reservations])

  return (
    <>
      {reservations &&
        allReservations &&
        allReservations?.map((item, index) => (
          <LocationCard data={item} key={index} />
        ))}
    </>
  )
}

export default FutureReservations
