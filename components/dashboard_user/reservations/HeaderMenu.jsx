import React from 'react'
import { consoleLog } from '/utils/logConsole'

const HeaderMenu = () => {
  return (
    <ul className="nav reservation-tabs" id="myTab" role="tablist">
      <li className="nav-item">
        <a
          className="nav-link active paragraph"
          id="home-tab"
          data-toggle="tab"
          href="#home"
          role="tab"
          aria-controls="home"
          aria-selected="true"
        >
          A venir
        </a>
      </li>
      <li className="nav-item">
        <a
          className="nav-link paragraph"
          id="profile-tab"
          data-toggle="tab"
          href="#profile"
          role="tab"
          aria-controls="profile"
          aria-selected="false"
        >
          Passées
        </a>
      </li>
    </ul>
  )
}

export default HeaderMenu
