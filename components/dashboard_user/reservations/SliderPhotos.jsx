import Image from 'next/image'
import React from 'react'
import notImage from 'public/images/image-not-found.png'
import Slider from 'react-slick'

const SliderPhotos = ({
  images = [],
  id,
  className = '',
  extraSettings,
  from
}) => {
  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    arrows: true,
    ...extraSettings
  }

  const handleImages = (arrImages) => {
    return (
      <Slider {...settings}>
        {arrImages.map((item, index) => (
          <div key={index}>
            <img
              className={
                from === 'user-reservation'
                  ? `${className} location-card-res__slider-img`
                  : `d-block w-100 rounded-3 obj-fit-cover ${className}`
              }
              src={item}
              alt={`slide-photos-image-${index}`}
              // onError={handleDefaultImage}
            />
          </div>
        ))}
      </Slider>
    )
  }
  const handleDefaultImage = (e) => {
    e.target.src = '/images/not-image.svg'
  }

  return (
    <Slider {...settings}>
      {images.length === 0 ? (
        <img
          className={
            from === 'user-reservation'
              ? `${className} location-card-res__slider-img`
              : `d-block w-100 rounded-3 obj-fit-cover ${className}`
          }
          src={notImage}
          alt={`slide-photos-image-no-image`}
          // onError={handleDefaultImage}
        />
      ) : (
        handleImages(images)
      )}
    </Slider>
  )
}

export default SliderPhotos
