import React from 'react'
import { consoleLog } from '/utils/logConsole'

const DateInput = ({ state, onchange }) => {
  return (
    <input type="date" onChange={onchange} value={state.date} name="date" />
  )
}

export default DateInput
