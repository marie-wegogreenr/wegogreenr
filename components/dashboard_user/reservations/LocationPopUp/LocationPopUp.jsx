import { Warning } from '@components/Icons'
import Image from 'next/image'
import React, { useEffect, useState } from 'react'
import moment from 'moment'

import SliderPhotos from '../SliderPhotos'
import CancelReservation from './CancelReservation'
import dynamic from 'next/dynamic'
import LoadingState from './LoadingState'
//HOOKS
import { useClickOutside } from 'hooks'
const Map = dynamic(() => import('./Map'), { ssr: false })
import getImageURL from 'utils/getImageURL'
import { getStatusMessage } from 'helpers/utilsHelper'
import { calcelReservationDays } from 'helpers/ReservationHelper'
import { useToast } from 'hooks/useToast'
import CancellationConditions from './CancellationConditions'
import { roundTimeToNearest } from 'helpers/timeHelper'

const LocationPopUp = ({ data, setState, state }) => {
  if (!data.establishment) {
    return <LoadingState />
  }

  let dataChecked = data

  if (dataChecked.establishment.city === null) {
    dataChecked.establishment.city = { lat: null, lng: null }
  }
  const {
    id,
    code,
    number_of_adults,
    room_price,
    number_of_children,
    arrival_date,
    departure_date,
    room: { public_name },
    establishment: {
      address,
      street_number,
      city: { lat: cityLat, lng: cityLng },
      city_name,
      name,
      min_entry_time,
      max_entry_time,
      max_exit_time,
      user: { name: host_name, lastname: host_lastName, photo_url },
      lat,
      lng,
      cancel_max_days
    },
    payment,
    images,
    status
  } = dataChecked

  let roomImages
  if (images) {
    roomImages = images.map(
      (item) => process.env.NEXT_PUBLIC_AMAZON_IMGS + item.image.url
    )
  }

  /*  States  */
  const [iscanceled, setIscanceled] = useState(
    getStatusMessage(status) === 'Annulée'
  )
  const [cancelRes, setCancelRes] = useState(false)
  const [notify] = useToast()
  const [timeLimits, setTimeLimits] = useState({})
  const [mapCoords, setMapCoords] = useState({
    _lat: lat ? lat : cityLat ? cityLat : 40.2245,
    _lng: lng ? lng : cityLng ? cityLng : 2.2123
  })

  /*  Effects  */
  useEffect(() => {
    setTimeLimits({
      arrivalMin: roundTimeToNearest(min_entry_time),
      arrivalMax: roundTimeToNearest(max_entry_time),
      departure: roundTimeToNearest(max_exit_time)
    })
  }, [])

  useEffect(() => {
    setMapCoords({
      _lat: lat ? lat : cityLat ? cityLat : 40.2245,
      _lng: lng ? lng : cityLng ? cityLng : 2.2123
    })
  }, [])

  /*  Additional constants  */
  const totalUsers = number_of_adults + number_of_children
  const mockImages = ['https://picsum.photos/400', 'https://picsum.photos/400']
  const canCancelReservation = calcelReservationDays(
    cancel_max_days,
    arrival_date
  )

  function cutHour(hour) {
    return `${hour.slice(0, 2)}h`
  }

  const closeNode = useClickOutside(() => {
    setState(false)
  })

  if (data && data.establishment)
    return (
      <div className={`locationPopUp`} ref={closeNode}>
        <div className="locationPopUp__close" onClick={() => setState(!state)}>
          X
        </div>
        <div className="wrapper-8 slick-custom mb-4 ">
          <h2 className="font-montserrat">Votre réservation</h2>
          <SliderPhotos
            images={roomImages}
            id={`-slider`}
            heigth={307}
            width={649}
          />
          {status === 3 && (
            <span className="locationPopUp__statusMessage ">
              Cette réservation sera confirmée par l'hébergeur, une notification
              sera envoyé sous 24h.
            </span>
          )}
          <p className="subtitle-dashboard weight-bold my-4">{public_name}</p>
          <div className="border-bottom"></div>
          <div className="d-flex justify-content-between mt-4">
            <p className="weight-bold paragraph m-0">ID de la réservation</p>
            <p className="paragraph m-0">{code}</p>
          </div>
          <div className="d-flex justify-content-between mt-2">
            <p className="weight-bold paragraph m-0">Voyageurs</p>
            <p className="paragraph m-0">
              {`${number_of_adults} ${
                number_of_adults === 0 || number_of_adults > 1
                  ? 'adultes'
                  : 'adult'
              } , ${number_of_children} ${
                number_of_children === 0 || number_of_children > 1
                  ? 'enfants'
                  : 'enfant'
              }`}
            </p>
          </div>
          <div className="d-flex justify-content-between mt-2">
            <p className="weight-bold paragraph m-0">Dates &amp; heures </p>
            <p className="paragraph m-0"></p>
          </div>
          <div className="d-flex justify-content-between mt-4 pt-2">
            <div className="w-50 mr-3">
              <div className="locationPopUp__card text-center d-flex justify-content-center flex-column">
                <b className="text-block weight-bold">Arrivée</b>
                <p className="paragraph">
                  {moment(arrival_date).format('dddd, Do MMMM  YYYY')}
                </p>
                <p className="h2-dashboard weight-bold">
                  {timeLimits.arrivalMin}h / {timeLimits.arrivalMax}h
                </p>
              </div>
            </div>
            <div className="w-50">
              <div className="locationPopUp__card text-center d-flex justify-content-center flex-column">
                <b className="text-block weight-bold">Départ</b>
                <p className="paragraph">
                  {moment(departure_date).format('dddd, Do MMMM  YYYY')}
                </p>
                <p className="h2-dashboard weight-bold">
                  {timeLimits.departure}h
                </p>
              </div>
            </div>
          </div>
          <p className="subtitle-dashboard weight-bold mt-5">Votre Logement</p>
          <div className="border-bottom"></div>
          <div className="d-flex justify-content-between align-items-center mt-3">
            <b className="text-block weight-bold">Hôte</b>
            <div className="d-flex align-items-center">
              <Image
                width={50}
                height={50}
                className="rounded-circle"
                // src={`${process.env.NEXT_PUBLIC_AMAZON_IMGS}${photo_url}`}
                src={getImageURL(photo_url)}
              />
              <p className="my-0 ms-2">
                {host_name && host_name.split(' ')[0]}{' '}
                {host_lastName && host_lastName.split(' ')[0]}
              </p>
            </div>
          </div>
          <div className="d-flex justify-content-between my-2">
            <b className="text-block weight-bold ">à l'établissement</b>
            <p>{data.establishment.name}</p>
          </div>
          <div className="d-flex justify-content-between my-2">
            <b className="text-block weight-bold ">Type de logement</b>
            <p className="py-2 px-4 rounded-pill border-0 btn-primary">
              {data.establishment.type.name}
            </p>
          </div>
          <div className="my-2">
            <b className="text-block weight-bold">Adresse</b>
            <p className="paragraph mt-2">
              {address},{street_number} {city_name}
            </p>
          </div>
          <div className="locationPopUp__map">
            <div className="locationPopUp__map-container">
              {data && data.establishment && mapCoords && (
                <Map center={mapCoords} reservation={data} />
              )}
            </div>
          </div>
          <p className="subtitle-dashboard weight-bold mt-5">
            Récapitulatif du paiement
          </p>
          <div className="border-bottom"></div>
          <div className="d-flex justify-content-between align-items-center my-4">
            <b className="h2-dashboard weight-bold">Total</b>
            <p className="h2-dashboard weight-bold m-0">
              {room_price?.toString().replace('.', ',')} &euro;
            </p>
          </div>
          <div className="border-bottom"></div>
          <CancellationConditions conditions={data} />
          <hr />
          {!iscanceled && (
            <div className="locationPopUp__cancel">
              {!cancelRes && (
                <span className="locationPopUp__cancel-title">
                  <Warning width={22} height={22} />
                  <p className="weight-bold m-0 ml-3 text">
                    Réservation annulée?
                  </p>
                </span>
              )}
              <CancelReservation
                setCancelRes={setCancelRes}
                reservationID={id}
                canCancel={canCancelReservation}
                cancelDays={cancel_max_days}
                setIscanceled={setIscanceled}
                type={'traveler'}
              />
            </div>
          )}
          {/* <AditionalInfo /> */}
        </div>
      </div>
    )
}

export default LocationPopUp
