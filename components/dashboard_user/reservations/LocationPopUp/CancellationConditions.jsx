import {
  fixedAnswerByDefault,
  fixedAnswerForUser
} from 'constants/cancellationConditions'

function CancellationConditions({ conditions, from }) {
  return (
    <>
      <div className="cancellation__popup">
        <p
          className={
            from === 'host'
              ? 'subtitle-dashboard weight-bold mt-5 cancellation__popup-title--host'
              : 'subtitle-dashboard weight-bold mt-5 cancellation__popup-title'
          }
        >
          Conditions d'annulation
        </p>

        {conditions &&
        conditions?.days_quantity &&
        conditions?.penality_percentage ? (
          <>
            <p className="cancellation__popup-subtitle color-gray-dark">
              <strong>{`Jusqu’à ${conditions?.days_quantity} ${
                conditions?.days_quantity === 1 ? 'jour' : 'jours'
              } avant votre séjour`}</strong>
            </p>
            <p>{fixedAnswerForUser}</p>

            <p className="cancellation__popup-subtitle color-gray-dark">
              <strong>{`Moins de ${conditions?.days_quantity} ${
                conditions?.days_quantity === 1 ? 'jour' : 'jours'
              } avant votre séjour ou non-présentation`}</strong>
            </p>
            {conditions?.penality_percentage === 100 ? (
              <p>Non remboursable</p>
            ) : (
              <p>{`Votre réservation est remboursée à hauteur de ${conditions?.penality_percentage}%`}</p>
            )}
          </>
        ) : (
          <>
            <p>{fixedAnswerByDefault.option1}</p>
            <p>{fixedAnswerByDefault.option2}</p>
          </>
        )}
      </div>
    </>
  )
}

export default CancellationConditions
