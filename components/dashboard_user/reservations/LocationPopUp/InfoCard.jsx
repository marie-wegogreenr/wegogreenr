import React from 'react'
import { consoleLog } from '/utils/logConsole'

const InfoCard = ({ text, link = '#', linkText }) => {
  return (
    <div className="mx-1 user_reservations__infoCard bg-gray_lighter mb-2 mb-lg-5 mt-3">
      <div className=" border-0 mt-3 text-center">
        <p className="weight-bold text">{text}</p>
        <a className="weight-bold text" href={link}>
          {linkText}
        </a>
      </div>
    </div>
  )
}

export default InfoCard
