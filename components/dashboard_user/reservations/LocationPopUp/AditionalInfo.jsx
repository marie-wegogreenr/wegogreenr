import React from 'react'
import { consoleLog } from '/utils/logConsole'
import InfoCard from './InfoCard'

const AditionalInfo = () => {
  return (
    <>
      <div className="border-bottom mt-5">
        <p className="subtitle-dashboard weight-bold">Informations pratiques</p>
        <div className="border-bottom mt-4"></div>
      </div>
      <p className="text mt-2">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus
        autem minima impedit maiores corrupti accusantium officiis, at eum vel
        iusto. Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi,
        distinctio?
      </p>
      <div className="d-flex justify-content-between flex-column flex-lg-row user_reservations__cards">
        <InfoCard
          text="Besoin d’en savoir plus sur les équipements  ?"
          linkText="Afficher le detail"
        />
        <InfoCard
          text="Consultez toutes les activités possibles sur place"
          linkText="Afficher le detail"
        />
        <InfoCard
          text="Vous avez une autre question ?"
          linkText="Afficher le detail"
        />
      </div>
    </>
  )
}

export default AditionalInfo
