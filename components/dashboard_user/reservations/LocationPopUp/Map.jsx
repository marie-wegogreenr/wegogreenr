import React from 'react'
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import 'leaflet/dist/leaflet.css'
import 'leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css'
import 'leaflet-defaulticon-compatibility'

const containerStyle = {
  width: '100%',
  height: '100%'
}

function Map({ center: { _lat, _lng }, reservation }) {
  const {
    establishment: { address, street_number, city }
  } = reservation

  return (
    <MapContainer
      center={[_lat, _lng]}
      zoom={8}
      scrollWheelZoom={false}
      style={{ height: '100%', width: '100%' }}
    >
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={[_lat, _lng]} animate={true}>
        <Popup>
          {address}&nbsp;{street_number}&nbsp;{city?.name}
          <br />
          <a
            target="_blank"
            href={`https://www.google.com/maps/dir//${_lat},${_lng}/@${_lat},${_lng}`}
          >
            see on google maps
          </a>
        </Popup>
      </Marker>
    </MapContainer>
  )
}

export default React.memo(Map)
