import React from 'react'
import { getTokenFromCookie } from 'utils'
import { getUserReservationsAction } from '@ducks/userDuck'
import { useDispatch, useSelector } from 'react-redux'
import { useToast } from 'hooks/useToast'
import { getReservationsByHostId } from '@ducks/host/reservationsByHostId'

const CancelReservation = ({
  setCancelRes,
  reservationID,
  canCancel,
  cancelDays,
  setIscanceled,
  type
}) => {
  const [notify] = useToast()
  const dispatch = useDispatch()
  const {
    user: { user }
  } = useSelector((state) => state)

  const handleSubmit = () => {
    const token = getTokenFromCookie()

    const options = {
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'DELETE',
      body: JSON.stringify({
        id: reservationID,
        type
      })
    }

    fetch(
      `${process.env.NEXT_PUBLIC_API_URL}reservations/${reservationID}`,
      options
    )
      .then((res) => res.json())
      .then((data) => {
        notify(`la réservation a été annulée avec succès`, 'success')
        dispatch(getUserReservationsAction())
        dispatch(getReservationsByHostId(user.id))
        setIscanceled(true)
      })
      .catch((err) =>
        notify(`échec de l'annulation de la réservation, réessayez`, 'error')
      )
  }

  return (
    <>
      <div className="locationPopUp__cancel-body">
        <p className="locationPopUp__cancel-question">
          Confirmer l'annulation de votre réservation ?
        </p>
        <div className="locationPopUp__cancel-btn-wrapper">
          {canCancel && (
            <>
              <button
                className="btn btn-secondary locationPopUp__cancel-btn"
                onClick={handleSubmit}
                disabled={!canCancel}
              >
                Oui je veux annuler
              </button>
            </>
          )}
          {!canCancel && (
            <p className="m-0">can't cancel {cancelDays} days before</p>
          )}
        </div>
      </div>
    </>
  )
}

export default CancelReservation
