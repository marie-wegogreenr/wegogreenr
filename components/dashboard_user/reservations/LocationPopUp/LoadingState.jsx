import React from 'react'
import { consoleLog } from '/utils/logConsole'
import Loader from '@components/Loader'

const LoadingState = () => {
  return (
    <div className={`locationPopUp`}>
      <Loader />
      <div className="locationPopUp__close" onClick={() => setState(!state)}>
        X
      </div>
      <div className="wrapper-8">
        <h2 className="font-montserrat">Votre réservation</h2>
        <p className="subtitle-dashboard weight-bold my-4">...</p>
        <div className="border-bottom"></div>
        <div className="d-flex justify-content-between mt-4">
          <p className="weight-bold paragraph">Code de reservation</p>
          <p className="paragraph">...</p>
        </div>
        <div className="d-flex justify-content-between">
          <p className="weight-bold paragraph">Voyageurs</p>
          <p className=" paragraph">... adults, ... enfant</p>
        </div>
        <div className="d-flex justify-content-between">
          <p className="weight-bold paragraph">Dates &amp; heures </p>
          <p className="paragraph"></p>
        </div>
        <div className="d-flex justify-content-between">
          <div className="w-50 mr-3">
            <div className="locationPopUp__card text-center d-flex justify-content-center flex-column">
              <b className="text-block weight-bold">Arrivée</b>
              <p className="paragraph">---</p>
              <p className="h2-dashboard weight-bold">---</p>
            </div>
          </div>
          <div className="w-50">
            <div className="locationPopUp__card text-center d-flex justify-content-center flex-column">
              <b className="text-block weight-bold">Départ</b>
              <p className="paragraph">....</p>
              <p className="h2-dashboard weight-bold">...</p>
            </div>
          </div>
        </div>
        <p className="subtitle-dashboard weight-bold mt-5">Votre Logement</p>
        <div className="border-bottom"></div>
        <div className="d-flex justify-content-between align-items-center mt-3">
          <b className="text-block weight-bold">Responsable</b>
          <div className="d-flex align-items-center">
            <p className="m-0"></p>
          </div>
        </div>
        <div className="d-flex justify-content-between my-3">
          <b className="text-block weight-bold ">Type de logement</b>
          <p>{}</p>
        </div>
        <div className="my-3">
          <b className="text-block weight-bold">Adresse</b>
          <p className="paragraph mt-2">... ... ...</p>
        </div>
        <div className="locationPopUp__map">
          <div className="locationPopUp__map-container"></div>
          <button className="btn btn-secondary w-100 mt-2 p-3 weight-bold text">
            Envoyer un message a l'etablissement
          </button>
        </div>
        <p className="subtitle-dashboard weight-bold mt-5">
          Informations de paiement
        </p>
        <div className="border-bottom"></div>
        <div className="d-flex justify-content-between align-items-center my-4">
          <b className="h2-dashboard weight-bold">Total</b>
          <p className="h2-dashboard weight-bold">&euro;</p>
        </div>
        <div className="border-bottom"></div>
      </div>
    </div>
  )
}

export default LoadingState
