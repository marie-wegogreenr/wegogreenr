/*  React  */
import { useState, useEffect } from 'react'
/*  Next  */
import Image from 'next/image'
import { useRouter } from 'next/router'
/*  Redux  */
import { useSelector } from 'react-redux'
/*  Components  */
import { SingleUser } from '@components/Icons'
import LocationPopUp from './LocationPopUp/LocationPopUp'
import SliderPhotos from './SliderPhotos'
/*  Images  */
import emptyCalendar from 'public/images/icon_emptyCalendar.png'
/*  Utils */
import { getImageURL, cutText } from 'utils'

const LocationCard = ({ data }) => {
  const {
    id,
    code,
    number_of_adults,
    number_of_children,
    arrival_date,
    departure_date,
    payment,
    room_price,
    status,
    room: { public_name },
    images
  } = data || {}

  const router = useRouter()
  /*  Global states  */
  const reservationsData = useSelector((store) => store.user.reservations)
  /*  Local states  */
  const [popUp, setPopUp] = useState(false)
  const [roomImages, setRoomImages] = useState([])
  const [currentStatus, setCurrentStatus] = useState('')

  const totalUsers = number_of_adults + number_of_children

  /*  Effects  */
  useEffect(() => {
    setRoomImages(images?.map((item) => getImageURL(item.image.url)))
  }, [images])

  useEffect(() => {
    const [{ status: thisStatus }] = reservationsData.filter(
      (item) => item.id === id
    )
    setCurrentStatus(thisStatus)
  }, [reservationsData])

  useEffect(() => {
    if (router.query.node == code && typeof window !== undefined && data) {
      setPopUp(true)
    }
  }, [])

  /*  Functions  */
  function getStatus(number) {
    switch (number) {
      case 0:
        return { text: 'En attente de paiement', color: 'alternative' }

      case 1:
        return { text: 'Approuvé', color: 'success' }

      case 2:
        return { text: 'Annulée', color: 'warning' }

      case 3:
        return { text: `En attente d'approbation`, color: 'warning' }

      case 4:
        return { text: `Annulée`, color: 'warning' }
    }
  }

  function formatDate(date) {
    const newDate = date.split('-')
    return `${newDate[2].slice(0, 2)}/${newDate[1]}/${newDate[0]}`
  }

  return (
    <>
      <div className={`location-card-res background-white`}>
        <div className="location-card-res__slider">
          <SliderPhotos images={roomImages} id={code} from="user-reservation" />
        </div>
        <div className="location-card-res__main">
          <h2 className="weight-bold font-montserrat subtitle-dashboard lh-22">
            {cutText(public_name, 70)}
          </h2>
          <span
            style={{
              width: '39px',
              borderBottom: '1px solid',
              display: 'block'
            }}
          ></span>
          <div className="location-card-res__icons mt-3">
            <span className="d-flex align-items-center">
              <SingleUser width={20} height={20} />
              <p className="weight-bold ml-3 text-block">
                Voyageurs: {totalUsers}
              </p>
            </span>
            <span className="d-flex align-items-center">
              <Image src={emptyCalendar} width={20} height={20} />
              <div className="d-flex">
                <p className="weight-bold ml-3 text-block">
                  {formatDate(arrival_date)}
                </p>
                <p className="ms-3 ms-lg-1">-&nbsp;</p>
                <p className="weight-bold ml-3 ml-lg-0 text-block">
                  {formatDate(departure_date)}
                </p>
              </div>
            </span>
          </div>
        </div>
        <div className="location-card-res__status">
          <p className="location-card-res__status-title">État</p>
          <span className="location-card-res__status-text">
            {getStatus(currentStatus)?.text}
          </span>
        </div>
        <div className="location-card-res__price">
          <p className="location-card-res__price-title">Coût total</p>
          <span className="weight-bold text-block">
            {room_price?.toString().replace('.', ',')} &euro;
          </span>
        </div>
        <div className="location-card-res__link">
          <a
            onClick={() => setPopUp(!popUp)}
            href="#"
            className="weight-bold text-block"
            id={`node-${code}`}
          >
            Afficher le détail
          </a>
        </div>
      </div>

      {popUp && data && (
        <LocationPopUp data={data} setState={setPopUp} state={popUp} />
      )}
    </>
  )
}

export default LocationCard
