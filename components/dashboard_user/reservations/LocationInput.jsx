import React from 'react'
import { consoleLog } from '/utils/logConsole'

const mockData = ['bogota', 'cali', 'santa marta']

const LocationInput = ({ state, onchange }) => {
  return (
    <select name="location" onChange={onchange} value={state.location}>
      {mockData.map((item, index) => (
        <option key={index}>{item}</option>
      ))}
    </select>
  )
}

export default LocationInput
