/*  React  */
import { useEffect, useState } from 'react'
/*  Redux  */
import { useSelector } from 'react-redux'
/*  Components  */
import FutureReservations from './FutureReservations'
import PreviousReservations from './PreviousReservations'

const Reservations = ({ filters }) => {
  /*  Global states  */
  const data = useSelector((store) => store.user.reservations)
  const status = useSelector((store) => store.user.status)
  /*  Local states  */
  const [resData, setResData] = useState([])
  /*  Effects  */
  useEffect(() => {
    setResData(data)
  }, [])

  return (
    <div className="tab-content" id="myTabContent">
      <div
        className="tab-pane fade show active"
        id="home"
        role="tabpanel"
        aria-labelledby="home-tab"
      >
        <FutureReservations reservations={resData} filters={filters} />
      </div>
      <div
        className="tab-pane fade"
        id="profile"
        role="tabpanel"
        aria-labelledby="profile-tab"
      >
        <PreviousReservations reservations={resData} filters={filters} />
      </div>
    </div>
  )
}

export default Reservations
