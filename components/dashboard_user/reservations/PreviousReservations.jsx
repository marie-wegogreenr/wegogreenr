import React from 'react'
import { consoleLog } from '/utils/logConsole'
import LocationCard from './LocationCard'
import { dateFromDb } from 'helpers/timeHelper'

const PreviousReservations = ({ reservations, filters }) => {
  const today = new Date()

  let allReservations = reservations

  //filter by status to9 show the active ones first
  allReservations = allReservations.sort((a, b) => a.status - b.status)

  if (filters.date) {
    const filterDate = dateFromDb(filters.date).getTime()
    allReservations = reservations.filter((item) => {
      if (dateFromDb(item.departure_date).getTime() < filterDate) return item
    })
  }

  return (
    <>
      {reservations &&
        allReservations.map((item, index) => {
          if (dateFromDb(item.departure_date).getTime() <= today.getTime()) {
            return <LocationCard data={item} key={index} />
          }
        })}
    </>
  )
}

export default PreviousReservations
