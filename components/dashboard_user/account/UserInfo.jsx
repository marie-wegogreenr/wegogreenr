import React, { useEffect, useState } from 'react'
import PhothoInput from './inputs/PhothoInput'
import NameInput from './inputs/NameInput'
import GenreInput from './inputs/GenreInput'

import { useSelector } from 'react-redux'
import BirthdayInput from './inputs/BirthdayInput'
import TextInput from './inputs/TextInput'
import AdressInput from './inputs/AdressInput'
import PassWordInput from './inputs/PassWordInput'

const UserInfo = () => {
  const user = useSelector((store) => store.user.user)

  const {
    name,
    lastname,
    birthday,
    email,
    phone,
    country,
    address,
    country_id,
    postal_code,
    photo_url,
    gender,
    city
  } = user

  const [profilePhoto, setProfilePhoto] = useState(null)

  useEffect(() => {
    setProfilePhoto(photo_url)
  }, [photo_url])

  return (
    <div className="card-white p-4 p-lg-5">
      <h3 className="weight-bold font-montserrat subtitle-dashboard">
        Informations personnelles
      </h3>
      <div className="border-bottom mt-4 mb-4"></div>
      <PhothoInput
        profilePicture={
          profilePhoto
            ? `${process.env.NEXT_PUBLIC_AMAZON_IMGS}${profilePhoto}`
            : null
        }
        from="user-dashboard-profile"
      />
      <NameInput name={name} lastname={lastname} />
      <GenreInput genre={gender} />
      <BirthdayInput birthday={birthday} />
      <TextInput label="Email" value={email} formName="email" />
      <TextInput label="Téléphone" formName="phone" value={phone} type="tel" />
      <AdressInput
        address={address}
        country={country}
        country_id={country_id}
        postalCode={postal_code}
        city={city}
      />
      <PassWordInput />
    </div>
  )
}

export default UserInfo
