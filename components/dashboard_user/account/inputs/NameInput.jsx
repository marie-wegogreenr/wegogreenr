import React, { useEffect, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
//REDUX
import { useDispatch } from 'react-redux'
import { updateUserInfoAction } from '@ducks/userDuck'
//ICONS
import { CircleClose } from '@components/Icons'
import { useToast } from 'hooks/useToast'

const NameInput = ({ name, lastname }) => {
  const dispatch = useDispatch()

  const [isEditing, setIsEditing] = useState(false)
  const [notify] = useToast()
  const [form, setForm] = useState({})

  useEffect(() => {
    setForm({
      name,
      lastname
    })
  }, [name])

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  const handleSubmit = () => {
    if (form.lastname !== lastname)
      dispatch(updateUserInfoAction('lastname', form.lastname))
    if (form.name !== name) dispatch(updateUserInfoAction('name', form.name))
    notify(`Modifications sauvegardées`, 'success')
    setIsEditing(false)
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'} mt-4 .user-info__name-input`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <p className="text-block font-montserrat weight-bold m-0">Nom</p>
            <p className="paragraph mt-2">
              {name} {lastname}
            </p>
          </div>
          <a
            className="weight-semibold text"
            onClick={() => setIsEditing(true)}
            href="#"
          >
            Modifier
          </a>
        </div>
      </div>
      {isEditing && name != undefined && (
        <>
          <div className="d-flex justify-content-between mt-4">
            <p className="subtitle-dashboard font-montserrat weight-bold m-0">
              Nom
            </p>
            <a
              className="weight-semibold text color-black"
              href="#"
              onClick={() => setIsEditing(false)}
            >
              <span className="mr-2">
                <CircleClose />
              </span>
              Annuler
            </a>
          </div>
          <div className="d-flex flex-column flex-lg-row mt-4">
            <div className="user-info__input-field--divided-lg">
              <p className="text-block weight-bold">Prénom</p>
              <input
                type="text"
                name="name"
                value={form.name}
                onChange={handleChange}
                className="w-100"
              />
            </div>
            <div className="user-info__input-field--divided-lg">
              <p className="text-block weight-bold">Nom</p>
              <input
                type="text"
                name="lastname"
                value={form.lastname}
                onChange={handleChange}
                className="w-100 form-control"
              />
            </div>
          </div>
          <button
            onClick={handleSubmit}
            className="btn btn-secondary paragraph weight-semibold py-3 user-info__save-button"
          >
            Sauvegarder
          </button>
          <div className="border-bottom my-5"></div>
        </>
      )}
    </>
  )
}

export default NameInput
