import React, { useEffect, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
//REDUX
import { useDispatch } from 'react-redux'
import { updateUserInfoAction } from '@ducks/userDuck'
//ICONS
import { CircleClose } from '@components/Icons'
import { useToast } from 'hooks/useToast'
import { validateOnlyNumber } from 'helpers/utilsHelper'
import { useRouter } from 'next/router'

const TextInput = ({ label, value = '', formName, editable = true, type }) => {
  const dispatch = useDispatch()
  const router = useRouter()
  const [isEditing, setIsEditing] = useState(false)
  const [notify] = useToast()
  const [form, setForm] = useState({})

  useEffect(() => {
    setForm({
      [formName]: value
    })
  }, [value])

  const handleChange = (e) => {
    if (e.target.type === 'tel') {
      const isNumber = validateOnlyNumber(e.target.value)
      setForm({
        ...form,
        [e.target.name]: isNumber ? e.target.value : e.target.value.slice(0, -1)
      })
    } else {
      setForm({ ...form, [e.target.name]: e.target.value })
    }
  }
  const reload = () => {
    setTimeout(() => {
      router.reload()
    }, 500)
  }
  const handleSubmit = () => {
    if (form[formName] !== value)
      dispatch(updateUserInfoAction(formName, form[formName]))
    notify(`Modifications sauvegardées`, 'success')
    setIsEditing(false)
    reload()
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'} mt-3`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <p className="text-block weight-bold m-0">{label}</p>
            <p className="paragraph mt-2">{value}</p>
          </div>
          <a
            className="text weight-semibold"
            onClick={() => setIsEditing(true)}
            href="#"
          >
            {editable && 'Modifier'}
          </a>
        </div>
      </div>

      {isEditing && (
        <>
          <div className="d-flex justify-content-between mt-3">
            <p className="subtitle-dashboard weight-bold">{label}</p>
            <a
              className="color-black text weight-bold"
              href="#"
              onClick={() => setIsEditing(false)}
            >
              <span className="mr-2">
                <CircleClose />
              </span>
              Annuler
            </a>
          </div>
          <div className="d-flex">
            <div className="w-100">
              <input
                type={type}
                name={formName}
                value={form[formName]}
                onChange={handleChange}
                className="user-info__input-field--divided-normal form-control"
              />
            </div>
          </div>
          <button
            onClick={handleSubmit}
            className="btn btn-secondary py-3 user-info__save-button weight-bold paragraph"
          >
            Sauvegarder
          </button>
          <div className="border-bottom my-5"></div>
        </>
      )}
    </>
  )
}

export default TextInput
