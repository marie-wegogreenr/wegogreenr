import React, { useState, useEffect } from 'react'
import { consoleLog } from '/utils/logConsole'
import checkIcon from 'public/images/check.png'
import Image from 'next/image'
//redux
import { useDispatch, useSelector } from 'react-redux'
import {
  logUserAction,
  updateUserInfoAction,
  unAuthUserAction
} from '@ducks/userDuck'
//assets
import { CircleClose } from '@components/Icons'
import { useToast } from 'hooks/useToast'

const PassWordInput = () => {
  const [notify] = useToast()
  const dispatch = useDispatch()
  const user = useSelector((store) => store.user.user)
  const isAuth = useSelector((store) => store.user.auth)

  const { email, name } = user

  const [isEditing, setIsEditing] = useState(false)
  const [form, setForm] = useState({
    password: '',
    passwordConfirm: '',
    old_password: ''
  })

  let passwordStength = 'Weak'
  let passwordColor = 'red'

  let passwordDisabled = true

  useEffect(() => {
    if (isAuth) {
      dispatch(updateUserInfoAction('password', form.password))
      notify(`Modifications sauvegardées`, 'success')
      dispatch(unAuthUserAction())
    }
  }, [isAuth])

  function PassworLength() {
    return form.password && form.password.length > 8
  }

  function emailNameCheck() {
    const regex = `(${email})|(${name})`
    return RegExp(regex).test(form.password)
  }

  function PasswordEqual() {
    return form.password === form.passwordConfirm
  }

  function PasswordNumberSymbol() {
    const regex = /[$-/:-?{-~!"^_@`\[\]]/
    return RegExp(regex).test(form.password)
  }

  function checkPasswordStength() {
    let length = PassworLength()
    let symbol = PasswordNumberSymbol()
    let emailAndName = emailNameCheck()

    length || symbol || !emailAndName ? (passwordStength = 'Weak') : ''
    ;(length || symbol) && (symbol || !emailAndName)
      ? ((passwordStength = 'Medium'), (passwordColor = 'gray-dark'))
      : ''
    length && symbol && !emailAndName
      ? ((passwordStength = 'Strong'),
        (passwordDisabled = false),
        (passwordColor = 'primary'))
      : ''
  }
  checkPasswordStength()

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  const handleSubmit = () => {
    if (PasswordEqual()) {
      dispatch(
        logUserAction({
          newPassword: form.password,
          password: form.old_password
        })
      )
      setIsEditing(false)
    } else notify('les mots de passe ne sont pas égaux', 'error')
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'} mt-3`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <p className="text-block m-0 weight-bold">Mot de passe</p>
            <p className="paragraph mt-2">***********</p>
          </div>
          <a
            className="text weight-semibold"
            onClick={() => setIsEditing(true)}
            href="#"
          >
            Modifier
          </a>
        </div>
      </div>

      {isEditing && (
        <>
          <div className="d-flex justify-content-between align-items-center">
            <p className="subtitle-dashboard weight-bold m-0">Mot de passe</p>
            <a
              className="text weight-semibold color-black"
              href="#"
              onClick={() => setIsEditing(false)}
            >
              <span className="mr-2">
                <CircleClose />
              </span>
              Annuler
            </a>
          </div>
          <div className="d-flex mt-3">
            <div className="w-100">
              <p className="text weight-bold">Actual Mot de passe</p>
              <input
                type="password"
                name="old_password"
                value={form.old_password}
                placeholder="votre mot de passe"
                onChange={handleChange}
                className="form-control user-info__input-field--medium"
              />
            </div>
          </div>
          <div className="d-flex flex-column flex-lg-row">
            <div className="w-100">
              <p className="text weight-bold">Mot de passe</p>
              <input
                type="password"
                name="password"
                value={form.name}
                placeholder="Saisissez votre mot de passe"
                onChange={handleChange}
                className="form-control user-info__input-field--medium"
              />
            </div>
            <div className="ml-0 ml-lg-3 w-100">
              <p className="text weight-bold">Confirmez le mot de passe</p>
              <input
                type="password"
                name="passwordConfirm"
                value={form.lastname}
                placeholder="Répétez votre mot de passe"
                onChange={handleChange}
                className="form-control user-info__input-field--medium"
              />
            </div>
          </div>
          <div>
            <p className="small weight-semibold">
              Fiabilité du mot de passe : &nbsp;
              <b className={`color-${passwordColor} weight-semibold`}>
                {passwordStength}
              </b>
            </p>
            <div className="d-flex align-items-center">
              {!emailNameCheck() && (
                <div className="d-flex">
                  <Image src={checkIcon} width={20} height={20} />
                </div>
              )}
              <p className="m-0 ml-2">
                Ne doit pas contenir votre nom ni votre adresse e-mail
              </p>
            </div>
            <div className="d-flex align-items-center mt-3">
              {PassworLength() && (
                <span className="d-flex">
                  <Image src={checkIcon} width={20} height={20} />
                </span>
              )}
              <p className="m-0 ml-2">Au moins 8 caractères</p>
            </div>
            <div className="d-flex align-items-center mt-3">
              {PasswordNumberSymbol() && (
                <span className="d-flex">
                  <Image src={checkIcon} width={20} height={20} />
                </span>
              )}
              <p className="m-0 ml-2">Contient un chiffre ou un symbole</p>
            </div>
          </div>
          <button
            onClick={handleSubmit}
            className="btn btn-secondary user-info__save-button py-3 my-4 paragraph weight-semibold"
            disabled={passwordDisabled}
          >
            Sauvegarder
          </button>
        </>
      )}
    </>
  )
}

export default PassWordInput
