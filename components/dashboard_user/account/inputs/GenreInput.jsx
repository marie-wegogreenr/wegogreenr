import React, { useState, useEffect } from 'react'
import { consoleLog } from '/utils/logConsole'
import genres from './../../../../constants/genres'
import { CircleClose } from '@components/Icons'
//REDUX
import { updateUserInfoAction, getUserDataAction } from '@ducks/userDuck'
import { useDispatch } from 'react-redux'
import { useToast } from 'hooks/useToast'

const GenreInput = ({ genre }) => {
  const dispatch = useDispatch()

  const [isEditing, setIsEditing] = useState(false)
  const [notify] = useToast()
  const [form, setForm] = useState({})

  useEffect(() => {
    setForm({
      genre
    })
  }, [])

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  const handleSubmit = () => {
    dispatch(updateUserInfoAction('gender', form.genre))
    dispatch(getUserDataAction())
    notify(`Modifications sauvegardées`, 'success')
    setIsEditing(false)
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'} mt-3`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <p className="text-block m-0 weight-bold">Genre</p>
            <p className="paragraph mt-2">{genre}</p>
          </div>
          <a
            className="weight-semibold text"
            onClick={() => setIsEditing(true)}
            href="#"
          >
            Modifier
          </a>
        </div>
      </div>

      {isEditing && (
        <>
          <div className="d-flex justify-content-between mt-3">
            <p className="subtitle-dashboard weight-bold">Genre</p>
            <a
              className="weight-bold text color-black"
              href="#"
              onClick={() => setIsEditing(false)}
            >
              <span className="mr-2">
                <CircleClose />
              </span>
              Annuler
            </a>
          </div>
          <div className="d-flex">
            <div className="w-100">
              <select
                name="genre"
                className="user-info__input-field mt-2 form-control user-info__input-field-gender"
                onChange={handleChange}
              >
                <option value={form.genre}>{form.genre}</option>
                {genres.map((item, index) => (
                  <option onChange={handleChange} value={item} key={index}>
                    {item}
                  </option>
                ))}
              </select>
            </div>
          </div>
          <button
            onClick={handleSubmit}
            className="btn btn-secondary weight-bold user-info__save-button paragraph py-3"
          >
            Sauvegarder
          </button>
          <div className="border-bottom my-5"></div>
        </>
      )}
    </>
  )
}

export default GenreInput
