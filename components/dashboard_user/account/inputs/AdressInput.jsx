import React, { useEffect, useState } from 'react'
import { consoleLog } from '/utils/logConsole'
import countries from 'constants/countries'
//redux
import { useDispatch } from 'react-redux'
import { updateUserInfoAction } from '@ducks/userDuck'
//ASSETS
import { CircleClose } from '@components/Icons'
import Cookies from 'universal-cookie'

import SearchAlgoia from '@components/Algolia/SearchAlgolia'
import { useToast } from 'hooks/useToast'

const AdressInput = ({
  country,
  address = '',
  address2 = '',
  extraAdress,
  postalCode = '',
  city,
  country_id
}) => {
  const dispatch = useDispatch()

  const [isEditing, setIsEditing] = useState(false)

  const [form, setForm] = useState({})
  const [notify] = useToast()
  useEffect(async () => {
    const cookies = new Cookies()
    //Get countries
    const res_countrie = await fetch(
      process.env.NEXT_PUBLIC_API_URL + 'countries',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookies.get('tk_user')
        },
        method: 'GET'
      }
    )
    const json_countries = await res_countrie.json()
    setForm({
      country,
      address,
      address2,
      extraAdress,
      postalCode,
      city,
      country_id,
      countries: json_countries
    })
  }, [country, address, extraAdress, postalCode, city])

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  const onSelectOption = (selected_value) => {
    //consoleLog(selected_value)
    setForm({
      ...form,
      city: selected_value.name
    })
  }

  const handleChangeVille = (event) => {
    const { value } = event.target

    if (typeof value === 'string') {
      setForm({ ...form, city: value })
    }
  }

  const handleSubmit = () => {
    if (form.address !== address)
      dispatch(updateUserInfoAction('address', `${form.address}`))
    if (form.postalCode !== postalCode)
      dispatch(updateUserInfoAction('postal_code', `${form.postalCode}`))
    if (form.country !== country)
      dispatch(updateUserInfoAction('country_id', `${form.country_id}`))
    if (form.city !== city)
      dispatch(updateUserInfoAction('city', `${form.city}`))
    notify(`Modifications sauvegardées`, 'success')
    setIsEditing(false)
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'} mt-3`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <p className="text-block m-0 weight-bold">Adresse postale</p>
            <p className="paragraph mt-2">
              {form.address} {form.postalCode} {form.city} {form.country?.name}
            </p>
          </div>
          <a
            className="text weight-bold"
            onClick={() => setIsEditing(true)}
            href="#"
          >
            Modifier
          </a>
        </div>
      </div>

      {isEditing && (
        <>
          <div className="d-flex justify-content-between mt-3">
            <p className="weight-bold subtitle-dashboard">Adresse postale</p>
            <a
              className="weight-bold text color-black"
              href="#"
              onClick={() => setIsEditing(false)}
            >
              <span className="mr-2">
                <CircleClose />
              </span>
              Annuler
            </a>
          </div>
          <div className="row">
            <div className="col-12 col-md-6">
              <p className="text-block weight-bold">Pays</p>
              <select
                defaultValue=""
                className="form-control"
                name="country_id"
                onChange={handleChange}
                value={form.country_id}
              >
                <option value=""></option>
                {form.country_id
                  ? form?.countries.map((value, index) => {
                      if (form.country_id == value.id) {
                        return (
                          <option key={index} value={value.id} selected>
                            {value.name}
                          </option>
                        )
                      } else {
                        return (
                          <option key={index} value={value.id}>
                            {value.name}
                          </option>
                        )
                      }
                    })
                  : form.countries.map((value, index) => {
                      return (
                        <option key={index} value={value.id}>
                          {value.name}
                        </option>
                      )
                    })}
              </select>
            </div>
            <div className="col-12 col-md-6 form-group">
              <p className="text-block weight-bold">Ville</p>
              <SearchAlgoia
                onChange={handleChangeVille}
                inputValue={form.city || ''}
                index="cities"
                placeholder="Ville"
                attribute="name"
                onSelectOption={onSelectOption}
                defaultValue={form.city}
              />
            </div>
          </div>
          <div className="user-info__input-field-wrapper">
            <div className="user-info__input-field--divided">
              <p className="text-block weight-bold">Adresse</p>
              <input
                type="text"
                name="address"
                value={form.address}
                onChange={handleChange}
                className="w-100"
              />
            </div>
            <div className="user-info__input-field--divided">
              <p className="text-block weight-bold">Code postal</p>
              <input
                type="text"
                name="postalCode"
                value={form.postalCode}
                onChange={handleChange}
                className="w-100"
              />
            </div>
          </div>
          <button
            onClick={handleSubmit}
            className="btn btn-secondary weight-bold paragraph user-info__save-button py-3 mb-4"
          >
            Sauvegarder
          </button>
          <div className="border-bottom my-5"></div>
        </>
      )}
    </>
  )
}

export default AdressInput
