import React, { useRef, useState } from 'react'
import Image from 'next/image'
import { getTokenFromCookie } from 'utils'
import { useSelector, useDispatch } from 'react-redux'
import moment from 'moment'
import { getUserDataAction } from '@ducks/userDuck'
import { useToast } from 'hooks/useToast'
import noImage from '../../../../public/images/not-image.svg'

const PhothoInput = ({ profilePicture, from }) => {
  const [notify] = useToast()
  const dispatch = useDispatch()
  const [photo, setPhoto] = useState({
    photo: null,
    url: null
  })
  const [showInput, setShowInput] = useState(false)

  const inputFileRef = useRef()

  const user = useSelector((store) => store.user.user)
  const {
    phone,
    name,
    email,
    birthday,
    lastname,
    address,
    country_id,
    gender,
    postal_code
  } = user

  const handleSubmit = (e) => {
    e.preventDefault()
    const formData = new FormData()
    const defaultDate = moment().subtract(18, 'years').format('YYYY-MM-DD')
    formData.append('photo', photo.photo)
    formData.append('establishment_id', 9)
    formData.append('name', name)
    formData.append('birthday', birthday !== null ? birthday : defaultDate)
    formData.append('email', email)
    formData.append('lastname', lastname)
    address ? formData.append('address', address) : ''
    country_id ? formData.append('country_id', country_id) : ''
    phone ? formData.append('phone', phone) : ''
    gender ? formData.append('gender', gender) : ''
    postal_code ? formData.append('postal_code', postal_code) : ''

    const token = getTokenFromCookie()

    const options = {
      headers: {
        Authorization: `Bearer ${token}`
      },
      method: 'POST',
      body: formData
    }

    fetch(`${process.env.NEXT_PUBLIC_API_URL}update-user`, options)
      .then((res) => res.json())
      .then(async (data) => {
        notify(`La photo a été mise à jour avec succès`, 'success')
        dispatch(getUserDataAction())
        setShowInput(!showInput)
      })
      .catch((err) => {
        notify('Erreur de mise à jour de la photo de profil :(', 'error')
        setPhoto({})
        setShowInput(!showInput)
      })
  }

  const handleChange = (e) => {
    if (e.target.files[0].size > 2000000) {
      notify('la taille du fichier ne doit pas dépasser 2 Mo', 'warning')

      setPhoto({
        photo: null
      })
      return false
    }

    setPhoto({
      photo: e.target.files[0],
      url: URL.createObjectURL(e.target.files[0])
    })

    setShowInput(!showInput)
  }

  return (
    <form encType="multipart/form-data" onSubmit={handleSubmit}>
      <div
        className={`d-flex align-items-center justify-content-between ${
          from === 'user-dashboard-profile' ? 'user-info__photo-input' : ''
        }`}
      >
        {showInput && (
          <div
            style={{ height: '92px' }}
            className={`${
              from === 'user-dashboard-profile'
                ? 'user-info__photo-input__img'
                : ''
            }`}
          >
            <img
              width="92"
              height="92"
              src={photo.url ? photo.url : noImage}
              className={`rounded-circle img-fluid h-100 obj-fit-cover`}
            />
          </div>
        )}
        {!showInput && (
          <img
            src={!profilePicture ? noImage : profilePicture}
            className={`rounded-circle img-fluid obj-fit-cover user-info__photo-input__img--fixed ${
              from === 'user-dashboard-profile'
                ? 'user-info__photo-input__img'
                : ''
            }`}
          />
        )}

        {!showInput && (
          <a
            className={`text weight-semibold ${
              from === 'user-dashboard-profile'
                ? 'user-info__photo-input__link '
                : ''
            }`}
            href="#"
            onClick={() => inputFileRef.current.click()}
          >
            <input
              ref={inputFileRef}
              type="file"
              onChange={handleChange}
              accept="image/jpeg, image/jpg, image/png"
              className="d-none"
            />
            Modifier / Ajouter une photo
          </a>
        )}
        {showInput && (
          <input
            type="submit"
            value="Sauvegarder la photo"
            className={`btn btn-secondary py-2 mb-0 ${
              from === 'user-dashboard-profile'
                ? 'user-info__photo-input__link'
                : ''
            }`}
          />
        )}
      </div>
    </form>
  )
}

export default PhothoInput
