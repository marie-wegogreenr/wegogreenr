import React, { useEffect, useState } from 'react'
import moment from 'moment'
//REDUX
import { updateUserInfoAction } from '@ducks/userDuck'
import { useDispatch } from 'react-redux'
//ICONS
import { CircleClose } from '@components/Icons'
import { useToast } from 'hooks/useToast'
/* Dependencies */
import DatePicker from 'react-datepicker'
import { CustomNormalDatePicker } from '@components/customInputs/CustomNormalDatePicker'
import { registerLocale, setDefaultLocale } from 'react-datepicker'
import fr from 'date-fns/locale/fr'
import { formatDate, formatSafari } from 'helpers/timeHelper'
registerLocale('fr', fr)
setDefaultLocale('fr')

const BirthdayInput = ({ birthday = '2020-02-01' }) => {
  const dispatch = useDispatch()
  const [notify] = useToast()
  const [isEditing, setIsEditing] = useState(false)
  const [form, setForm] = useState({})

  useEffect(() => {
    setForm({
      birthday
    })
  }, [birthday])

  const handleSubmit = () => {
    dispatch(updateUserInfoAction('birthday', form.birthday))
    notify(`Modifications sauvegardées`, 'success')
    setIsEditing(false)
  }

  return (
    <>
      <div className={`${isEditing && 'd-none'} mt-3`}>
        <div className={`d-flex justify-content-between`}>
          <div>
            <p className="text-block weight-bold m-0">Date de naissance</p>
            <p className="paragraph mt-2">
              {form.birthday &&
                moment(form.birthday + ' 00:00').format('dddd, Do MMMM  YYYY')}
            </p>
          </div>
          <a
            className="text weight-semibold"
            onClick={() => setIsEditing(true)}
            href="#"
          >
            Modifier
          </a>
        </div>
      </div>

      {isEditing && (
        <>
          <div className="d-flex justify-content-between mt-3">
            <p className="subtitle-dashboard weight-bold color-black">
              Date de naissance
            </p>
            <a
              className="color-black text weight-bold"
              href="#"
              onClick={() => setIsEditing(false)}
            >
              <span className="mr-2">
                <CircleClose />
              </span>
              Annuler
            </a>
          </div>
          <div className="d-flex">
            <div className="w-50 modalPriceContent___dates--profile">
              <DatePicker
                className="form-control date-inputs rounded-right modalPriceContent__date-picker"
                selected={new Date(form.birthday)}
                onChange={(date) =>
                  setForm({ ...form, birthday: formatDate(date) })
                }
                dateFormat="MM/dd/yyyy"
                peekNextMonth
                showMonthDropdown
                showYearDropdown
                dropdownMode="select"
                customInput={<CustomNormalDatePicker />}
              />
            </div>
          </div>
          <button
            onClick={handleSubmit}
            className="btn btn-secondary py-3 weight-bold paragraph user-info__save-button"
          >
            Sauvegarder
          </button>
          <div className="border-bottom my-5"></div>
        </>
      )}
    </>
  )
}

export default BirthdayInput
