import Link from 'next/link'
import React from 'react'
import { consoleLog } from '/utils/logConsole'

const CardWithIcon = ({ icon, title, content, link, redirect }) => {
  return (
    <div className="card-white pl-4 pl-lg-5">
      {icon}
      <p className="subtitle-dashboard weight-bold mt-4">{title}</p>
      <p>{content}</p>
      {link && (
        <Link href={redirect}>
          <a className="paragraph weight-semibold">{link}</a>
        </Link>
      )}
    </div>
  )
}

export default CardWithIcon
