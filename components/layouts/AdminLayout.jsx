import Container from '@components/layouts/container'
import SideMenu from '@components/dashboard_admin/sideMenu/SideMenu'
import React from 'react'
import { Children } from 'react'
import withAuthAdmin from 'HOC/withAuthAdmin'
import AdminNav from '@components/NavigationComponents/AdminNav'
import { useRouter } from 'next/router'
import Head from 'next/head'

function AdminLayout({ children, title, isReturn }) {
  const Router = useRouter()
  return (
    <>
      <Head>
        <link
          rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossOrigin="anonymous"
        />
      </Head>
      <AdminNav />
      <div className="AdminPageContainer">
        <SideMenu />
        <div className="rightAdminScreen">
          <div className="adminTitleSection">
            <h1>{title}</h1>

            {isReturn && (
              <button
                className="btn btn-secondary"
                onClick={() => {
                  Router.back()
                }}
              >
                retour
              </button>
            )}
          </div>

          <hr />
          {children}
        </div>
      </div>
    </>
  )
}

export default withAuthAdmin(AdminLayout)
