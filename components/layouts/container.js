import Navigation from '../navigation'
import Footer from '../footer'
import Head from 'next/head'
import React from 'react'
import { consoleLog } from '/utils/logConsole'
import Cookies from 'universal-cookie'
import Router from 'next/router'
import fetch from 'isomorphic-fetch'
import Auth from '../Auth'

class Container extends React.Component {
  constructor(props) {
    super(props)
    const cookies = new Cookies()
    this.state = {
      token_user: cookies.get('tk_user'),
      url: [],
      showFooter: false,
      showHeader: false,
      colorFooter: 'green'
    }
  }

  async componentDidMount() {
    const cookies = new Cookies()
    let url = window.location.href.split('/')

    let _this = this
    if (url[3] === 'register' || url[3] === 'login') {
    } else if (url[3] === 'user' || url[3] === 'host' || url[3] == 'admin') {
      if (this.state.token_user === undefined) {
        Router.push('/login')
      }
      this.setState({
        ...this.state,
        colorFooter: 'gray',
        showFooter: true,
        showHeader: true,
        url: window.location.href.split('/')
      })
    } else if (
      url[3] === 'hebergements' ||
      url[3] == 'contacter' ||
      url[3] == '404' ||
      url[3] == 'conditions-generales' ||
      url[3] == 'demarche' ||
      url[3] == 'ccv' ||
      url[3] == 'devenir-hote' ||
      url[3] == 'press' ||
      url[3].match(/search*/)
    ) {
      this.setState({
        ...this.state,
        colorFooter: 'green',
        showFooter: true,
        showHeader: true,
        url: window.location.href.split('/')
      })
    } else if (url[3] === 'onboarding') {
      if (url[4] == 'finish-onboarding') {
        this.setState({
          ...this.state,
          colorFooter: 'green',
          showFooter: true,
          showHeader: true,
          url: window.location.href.split('/')
        })
      } else {
        this.setState({
          ...this.state,
          showFooter: false,
          showHeader: true,
          colorFooter: 'green',
          url: window.location.href.split('/')
        })
      }
      if (this.state.token_user === undefined) {
        Router.push('/login')
      } else {
        let _this = this
      }
    } else if (url[3] === '') {
      this.setState({
        ...this.state,
        colorFooter: 'green',
        showFooter: true,
        showHeader: true,
        url: window.location.href.split('/')
      })
    }
  }

  render() {
    return (
      <div className="main__layout">
        <Head>
          <title>We Go GreenR</title>
          <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
            crossOrigin="anonymous"
          />
        </Head>
        <Auth actualUrl={this.state.url[3]} />
        {this.state.showHeader &&
          this.state.url[3] !== 'register' &&
          this.state.url[3] !== 'login' &&
          this.state.url[3] !== 'forget_password' &&
          this.state.url[3] !== 'forget_password_form' &&
          this.state.url[3] !== 'email' &&
          this.state.url[3] !== 'demarche' &&
          this.state.url[3] !== 'onboarding' && <Navigation />}
        <>{this.props.children}</>
        {this.state.showFooter && <Footer type={this.state.colorFooter} />}
      </div>
    )
  }
}

export default Container
