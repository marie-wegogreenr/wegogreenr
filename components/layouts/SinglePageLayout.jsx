import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { evaluateEvent } from 'helpers/dataLayer'
import { eventListObjectTriggered } from 'helpers/dataLayer/lists'

export default function SinglePageLayout({ children }) {
  const user = useSelector((store) => store.user.user)
  const [eventSent, setEventSent] = useState(false)

  useEffect(() => {
    const timer = setTimeout(() => {
      if (!eventSent) {
        evaluateEvent(user)
        if (user?.id) {
          eventListObjectTriggered.loggedUser.callback(user?.id)
        }
        setEventSent(true)
      }
    }, 2000)

    return () => clearTimeout(timer)
  }, [user])

  return <div>{children}</div>
}
