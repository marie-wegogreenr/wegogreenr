import React from 'react'
import Select, { components } from 'react-select'

const Control = ({ children, ...props }) => {
  const { onOptionClick, prefix, id } = props.selectProps
  const style = { cursor: 'pointer' }

  return (
    <components.Control {...props}>
      <span style={style}>{prefix}</span>
      {children}
    </components.Control>
  )
}

export const CustomSelect = (props) => {
  const styles = {
    control: (css) => ({
      ...css,
      ...props.style
    })
  }

  return (
    <Select
      {...props}
      onChange={props.onChange}
      components={{ Control }}
      id={props?.id}
      options={props.options}
      styles={styles}
    />
  )
}
