import { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'

import { useToast } from 'hooks/useToast'
import { setReservationStatus } from 'services/hebergementsService'
import { updateReservationStatus } from '@ducks/host/reservationsByHostId/actions'
import { useDispatch } from 'react-redux'

export function DropdownReservationHost({ openModal, reservation }) {
  const dispatch = useDispatch()
  const [showOptions, setShowOptions] = useState(false)
  const [notify] = useToast()

  const HandleReservationStatus = async (reservation_id, accept) => {
    try {
      await setReservationStatus({ reservation_id, accept })
      notify('changement de statut réussi', 'success')
      dispatch(
        updateReservationStatus({
          id: reservation_id,
          status: accept ? 1 : 2
        })
      )
    } catch (error) {
      notify('il y a eu un problème. veuillez réessayer plus tard', 'error')
    }
    setShowOptions(!showOptions)
  }

  const showDetail = () => {
    openModal(true)
    setShowOptions(!showOptions)
  }
  return (
    <>
      <div class="dropdown">
        <button
          class="dropdown-toggle btn btn-outline-secondary"
          type="button"
          id="dropdownMenu2"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          <span className="mr-2">options</span>
          <FontAwesomeIcon icon={faChevronDown} className={'icon-width'} />
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
          {reservation.status === 3 && (
            <button
              className="dropdown-item"
              onClick={() => {
                HandleReservationStatus(reservation.id, 1)
              }}
            >
              <span>accepter la réservation</span>
            </button>
          )}
          {reservation.status === 3 && (
            <button
              className="dropdown-item"
              onClick={() => {
                HandleReservationStatus(reservation.id, 0)
              }}
            >
              <span>refuser la réservation</span>
            </button>
          )}
          <button className="dropdown-item" onClick={showDetail}>
            <span>Voir détail</span>
          </button>
        </div>
      </div>
    </>
  )
}
