import React, { useState } from 'react'
import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import useLogout from 'hooks/useLogout'
import FullLoading from '@components/FullLoading/FullLoading'

export default function DropdownDahboardMenu({
  hasRol = null,
  whereIs = null,
  handleLogout
}) {
  const logout = useLogout()
  const [clickLogout, setClickLogout] = useState(false)
  return (
    <>
      <ul
        className={`dropdown-menu dropdown-menu-right ${
          typeof window !== undefined && window.location.href.includes('admin')
            ? 'right-16'
            : ''
        }`}
        role="menu"
        aria-labelledby="dLabel"
      >
        <Link
          href={whereIs === 'user' ? '/user/mon-profil' : '/host/mon-profil'}
        >
          <li className="dropdown-item" style={{ cursor: 'pointer' }}>
            <a className="link-black-without-underline">Mon compte</a>
          </li>
        </Link>
        <div className="dropdown-divider"></div>

        {whereIs === 'user' && (
          <Link href={hasRol ? '/host/dashboard' : '/onboarding/presentation'}>
            <li className="dropdown-item">
              <FontAwesomeIcon
                icon={['fas', 'exchange-alt']}
                size="1x"
                className="icon-passer"
              />

              <a className="link-black-without-underline">
                Passer en mode hôte
              </a>
            </li>
          </Link>
        )}

        {whereIs === 'host' && (
          <Link href="/user/dashboard">
            <li className="dropdown-item" style={{ cursor: 'pointer' }}>
              <FontAwesomeIcon
                icon={['fas', 'exchange-alt']}
                size="1x"
                className="icon-passer"
              />

              <a className="link-black-without-underline">
                Passer en mode voyageur
              </a>
            </li>
          </Link>
        )}

        <div className="dropdown-divider"></div>
        <Link href="/contacter">
          <li className="dropdown-item" style={{ cursor: 'pointer' }}>
            <a className="link-black-without-underline">Aide</a>
          </li>
        </Link>
        <a
          onClick={(e) => {
            e.preventDefault()
            setClickLogout(true)
            logout()
          }}
          href="#"
          className="link-black-without-underline"
        >
          <li className="dropdown-item" style={{ cursor: 'pointer' }}>
            Se déconnecter
          </li>
        </a>
      </ul>

      {typeof window !== undefined &&
        window.location.href.includes('admin') &&
        clickLogout && (
          <FullLoading isLoading={true} isOpen={true} isClear={false} />
        )}
    </>
  )
}
