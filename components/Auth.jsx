import React, { useEffect } from 'react'
import { consoleLog } from '/utils/logConsole'
import { useDispatch, useSelector } from 'react-redux'
import { getUserDataAction } from '../redux/ducks/userDuck'
import Router from 'next/router'

import { getTextByIdAction } from '@ducks/locales/actions'

const Auth = (props) => {
  const { actualUrl } = props

  const dispatch = useDispatch()

  //first mount
  useEffect(() => {
    // dispatch(getUserDataAction())
    dispatch(getTextByIdAction(1))
    dispatch(getTextByIdAction(2))
    validateUser()
  }, [])

  const store = useSelector((store) => store.user.user)

  const validateUser = () => {
    if (actualUrl == 'login' || actualUrl == 'register') {
      let isAdminFound = store.roles.find((rol) => rol.name == 'admin')
      store.roles.forEach((rol) => {
        if (rol.name !== 'admin') {
          Router.push('/user/dashboard')
        } else {
          Router.push('/admin')
        }
      })
    }
  }

  return <></>
}

export default Auth
