import axios from 'axios'
import {
  delWithStatus,
  get,
  postFiles,
  postWithStatus
} from '../helpers/httpConectionHelper'
import { headerWithToken } from '../helpers/utilsHelper'

export const getRoomImages = (id) => {
  return get(`room-images/${id}`, headerWithToken())
}

export const deleteRoomImages = (id) => {
  return delWithStatus(`room-images/${id}`, headerWithToken())
}

export const updateRoomImagePosition = (id, order) => {
  return postWithStatus(`room-images/${id}`, { order }, headerWithToken())
}
export const saveRoomImage = (body) => {
  return postFiles(`room-images`, body, headerWithToken())
}

export const saveRoomImage2 = async (body, callback) => {
  const response = await axios.request({
    method: 'post',
    url: `${process.env.NEXT_PUBLIC_API_URL}room-images`,
    data: body,
    headers: {
      ...headerWithToken()
    },
    onUploadProgress: (p) => {
      const percent = (p.loaded / p.total) * 100
      const order = body.get('order')
      callback(order, percent)
    }
  })

  return response.data
}

export const getEtablissementImages = (id) => {
  return get(`establishment-images/${id}`, headerWithToken())
}

export const deleteEtablissementImages = (id) => {
  return delWithStatus(`establishment-images/${id}`, headerWithToken())
}

export const updateEtablissementImagePosition = (id, order) => {
  return postWithStatus(
    `establishment-images/${id}`,
    { order },
    headerWithToken()
  )
}
export const saveEtablissementImage = (body) => {
  return postFiles(`establishment-images`, body, headerWithToken())
}

export const saveEtablissementImage2 = async (body, callback) => {
  const response = await axios.request({
    method: 'post',
    url: `${process.env.NEXT_PUBLIC_API_URL}establishment-images`,
    data: body,
    headers: {
      ...headerWithToken()
    },
    onUploadProgress: (p) => {
      const percent = (p.loaded / p.total) * 100
      const order = body.get('order')
      callback(order, percent)
    }
  })

  return response.data
}
