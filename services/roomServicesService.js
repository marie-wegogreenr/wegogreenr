import {
  delWithStatus,
  get,
  getWithStatus,
  postWithStatus,
  put
} from '../helpers/httpConectionHelper'
import { headerWithToken } from '../helpers/utilsHelper'

export const createNewService = (body) => {
  return postWithStatus(`additional-services`, body, headerWithToken())
}

export const getCustomServices = (establishment_id) => {
  return get(`additional-services/${establishment_id}`)
}

export const breakfastTypes = () => {
  return get(`breakfast-types`, headerWithToken())
}
export const getEtablissementServicesById = (establishment_id) => {
  return get(`establishment-services/${establishment_id}`, headerWithToken())
}

export const deleteCustomServices = (establishment_id) => {
  return delWithStatus(
    `additional-services/${establishment_id}`,
    headerWithToken()
  )
}

export const UpdateCustomService = (establishment_id, body) => {
  return put(`additional-services/${establishment_id}`, body, headerWithToken())
}

export const getRoomAvailabilities = (room_id) => {
  return getWithStatus(`availabilities/${room_id}`, headerWithToken())
}

export const getCalendarOrNot = (room_id) => {
  return getWithStatus(`calendar-or-not/${room_id}`, headerWithToken())
}

export const getRoomsById = (id) => {
  return getWithStatus(`rooms/${id}`, headerWithToken())
}

export const modifyOutstandingRoom = (room_id) => {
  return getWithStatus(`add-outsanding/${room_id}`, headerWithToken())
}
