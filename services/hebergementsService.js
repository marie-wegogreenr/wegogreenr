import {
  del,
  delWithStatus,
  get,
  post,
  put,
  postWithStatus,
  postExport,
  putWithStatus
} from '../helpers/httpConectionHelper'
import { headerWithToken } from '../helpers/utilsHelper'
import moment from 'moment'

export const updateRoomStatus = async (roomId) => {
  return get(`disable-room/${roomId}`, headerWithToken())
}

export const getHebergement = async (id) => {
  return await get(`rooms/${id}`, headerWithToken())
}

export const updateHebergement = async (body, id) => {
  return await put(`rooms/${id}`, body, headerWithToken())
}

export const getIcals = (id) => {
  return get(`ical/${id}`, headerWithToken())
}

export const getUnavailableDays = (id) => {
  return get(`availabilities/${id}`, headerWithToken())
}

export const setToAvailable = (body, id) => {
  return del(`availabilities/${id}`, body, headerWithToken())
}

export const setToUnavailable = (body) => {
  return post(`availabilities`, body, headerWithToken())
}

export const deleteSpecialAvailabilities = (body) => {
  return post(`delete-special-availabilities`, body, headerWithToken())
}

export const addSpecialAvailability = (body) => {
  return post(`room-special-availabilities`, body, headerWithToken())
}

export const updateSpecialAvailability = (body, id) => {
  return put(`room-special-availabilities/${id}`, body, headerWithToken())
}

export const deleteSpecialAvailability = (id) => {
  return del(`room-special-availabilities/${id}`, '', headerWithToken())
}

export const deleleEtablissementImage = (id) => {
  return del(`establishment-images/${id}`, {}, headerWithToken())
}

export const orderImage = (id, body) => {
  return post(`establishment-images/${id}`, body, headerWithToken())
}

export const getDescriptionConditions = () => {
  return get(`conditions`, headerWithToken())
}

export const getDescriptionConditionsStablishment = (id) => {
  return get(`establishment-conditions/${id}`, headerWithToken())
}

export const getRoomTypes = () => {
  return get(`room-types`, headerWithToken())
}

export const deleleEquipment = (id) => {
  return del(`room-equipments/${id}`, {}, headerWithToken())
}

export const addEquipment = (body) => {
  return post(`room-equipments`, body, headerWithToken())
}

export const getEquipments = (id) => {
  return get(`room-equipments/${id}`, headerWithToken())
}

export const getRoomEquipmentsTypes = () => {
  return get(`room-equipment-types`, headerWithToken())
}

export const getRoomEquipmentsCategories = () => {
  return get(`room-equipment-type-categories`, headerWithToken())
}

export const getAdminRooms = async (page, body = {}) => {
  return await post(`rooms-search?page=${page}`, body, headerWithToken())
}

export const highlightHebergement = (body) => {
  return postWithStatus(`add-multiple-outstanding`, body, headerWithToken())
}

export const setReservationStatus = (body) => {
  return post(`accept-reservation`, body, headerWithToken())
}

export const exportHebergementData = () => {
  return postExport(
    `rooms-export`,
    {},
    headerWithToken(),
    `hebergement_${moment(new Date()).format('DD-MM-YYYY')}`
  )
}

export const exportEstablishmentData = () => {
  return postExport(
    `establishments-export`,
    {},
    headerWithToken(),
    `etablissement_${moment(new Date()).format('DD-MM-YYYY')}`
  )
}

export const updateStep5 = (id, body) => {
  return putWithStatus(`room-pricing/${id}`, body, headerWithToken())
}
