import {
  post,
  postExport,
  postWithStatus,
  put,
  putWithStatus
} from 'helpers/httpConectionHelper'
import { headerWithToken } from '../helpers/utilsHelper'
import moment from 'moment'

export const getRoomPag = (pag, body = {}) => {
  return post(`accommodations-search?page=${pag}`, body, headerWithToken())
}

export const updateRoom = (data) => {
  const { id, ...body } = data
  return putWithStatus(`accommodations/${id}`, body, headerWithToken())
}

export const activateRooms = (data) => {
  const { id, ...body } = data
  return postWithStatus(`activate-rooms`, body, headerWithToken())
}

export const changeAbracadaroomStatus = (data) => {
  const { id, ...body } = data
  return postWithStatus(`update-status`, body, headerWithToken())
}

export const filterByStatus = (data) => {
  return postWithStatus(`filter-by-status`, data, headerWithToken())
}

export const exportAbracadaroomData = () => {
  return postExport(
    `abracadaroom-export`,
    {},
    headerWithToken(),
    `abracadaroom_${moment(new Date()).format('DD-MM-YYYY')}`
  )
}

export const updateGreenScore = (data) => {
  return putWithStatus(`update-greenscore`, data, headerWithToken())
}
