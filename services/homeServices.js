import { post, postWithStatus } from '../helpers/httpConectionHelper'
import { headerWithToken } from '../helpers/utilsHelper'

export const roomNearMe = (body) => {
  return postWithStatus(`publication-availabilities`, body)
}

export const roomsByRegion = (body) => {
  return postWithStatus(`publication-availabilities2`, body)
}

export const blogArticlePosts = () => {
  return get(`${process.env.NEXT_PUBLIC_BLOG_URL}wp-json/wp/v2/posts`)
}
