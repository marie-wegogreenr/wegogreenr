import { get, post } from '../helpers/httpConectionHelper'
import { headerWithToken } from '../helpers/utilsHelper'

export const getGreenscorePoints = async (userId) => {
  return get(`calculate-points/${userId}`, headerWithToken())
}

export const checkGreenscore = async (userId) => {
  return get(`greenscore/${userId}`, headerWithToken())
}

export const saveResponse = async (body) => {
  return post(`establishment-response`, body, headerWithToken())
}

export const deleteResponse = async (body) => {
  return post(`delete-establishment-response`, body, headerWithToken())
}
