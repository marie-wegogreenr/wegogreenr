import { get, post, put } from '../helpers/httpConectionHelper'
import { headerWithToken } from '../helpers/utilsHelper'

export const getEstablishmentClasifications = () => {
  return get(`establishment-clasifications`, headerWithToken())
}

export const getCountries = () => {
  return get(`countries`, headerWithToken())
}

export const establismentTypes = () => {
  return get(`establishment-types`, headerWithToken())
}

export const createNewEstablishment = (body) => {
  return post(`establishments`, body, headerWithToken())
}

export const createNewEstablishmentLang = (body) => {
  return post(`establishment-languages`, body, headerWithToken())
}

export const deleteEstablishmentLang = (body) => {
  return post(`delete-establishment-language`, body, headerWithToken())
}

export const updateEstablishment = (body, id) => {
  return put(`establishments/${id}`, body, headerWithToken())
}

//greenscore

export const setGreenScore = (body) => {
  return post(`greenscore`, body, headerWithToken())
}
