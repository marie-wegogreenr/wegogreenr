import {
  delWithStatus,
  generalGet,
  get,
  get2,
  getWithStatus,
  post,
  postWithStatus
} from 'helpers/httpConectionHelper'
import { getTokenFromCookie, headerWithToken } from '../helpers/utilsHelper'

export const getEtablissement = (userId, token) => {
  return get(`establishments/${userId}`, headerWithToken(token))
}

export const getRooms = (establishmentId) => {
  return get(`establishment-rooms/${establishmentId}`, headerWithToken())
}

export const getAddressCoordinates = (address) => {
  return generalGet(
    `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${process.env.NEXT_PUBLIC_GOOGLE_MAPS_KEY_2}`
  )
}

export const getAddressFromCoordinates = (lat, lng) => {
  return generalGet(
    `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${process.env.NEXT_PUBLIC_GOOGLE_MAPS_KEY_2}`
  )
}

export const getEstablishmentEquipments = (establishmentId) => {
  return get(`establishment-equipments/${establishmentId}`, headerWithToken())
}

export const getCancellationConditions = (establishmentId) => {
  return get(`cancellation-conditions/${establishmentId}`, headerWithToken())
}

export const getCancellationConditionsStatus = (establishmentId) => {
  return getWithStatus(
    `cancellation-conditions/${establishmentId}`,
    headerWithToken()
  )
}

export const addCancellationConditions = (body) => {
  return postWithStatus(`cancellation-conditions`, body, headerWithToken())
}

export const deleteCancellationConditions = (conditionId) => {
  return delWithStatus(
    `cancellation-conditions/${conditionId}`,
    headerWithToken()
  )
}
export const getAllEtablissements = (id, body) => {
  return post(`establishments-search?page=${id}`, body, headerWithToken())
}

export const getConditions = () => {
  return get(`conditions`, headerWithToken())
}

export const getEstablishmentConditions = (establishmentId) => {
  return get(`establishment-conditions/${establishmentId}`, headerWithToken())
}
