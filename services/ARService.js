import {
  getWithStatus,
  postWithStatus,
  post
} from 'helpers/httpConectionHelper'
import { headerWithToken } from '../helpers/utilsHelper'

export const getAREtablissement = (body) => {
  return postWithStatus(`get-my-ar-establishments`, body, headerWithToken())
}

export const setAREtablissement = (body) => {
  return postWithStatus(`link-ar-establishment`, body, headerWithToken())
}

export const getARSingleEtablissement = (establishmentId) => {
  return getWithStatus(
    `ar-establishments/${establishmentId}`,
    headerWithToken()
  )
}

export const activeAREtablissement = (body) => {
  return post(`active-ar-establishments`, body, headerWithToken())
}
