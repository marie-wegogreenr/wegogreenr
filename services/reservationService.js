import {
  get,
  post,
  postWithStatus,
  postExport
} from '../helpers/httpConectionHelper'
import { headerWithToken } from '../helpers/utilsHelper'
import moment from 'moment'

export const createReservation = async (body) => {
  return postWithStatus(`reservations`, body, headerWithToken())
}

export const generatePaymentLink = async (body) => {
  return postWithStatus(`purchase`, body, headerWithToken())
}

export const confirmPayment = async (body) => {
  return postWithStatus(`change-reservation-status`, body, headerWithToken())
}

export const getAdminReservations = (page, body = {}) => {
  return post(`all-reservations?page=${page}`, body, headerWithToken())
}

export const exportReservationsData = () => {
  return postExport(
    `reservations-export`,
    {},
    headerWithToken(),
    `reservations_${moment(new Date()).format('DD-MM-YYYY')}`
  )
}

export const validateGiftCardCode = (body = {}) => {
  return postWithStatus(`giftup-validation`, body)
}
