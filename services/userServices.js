import {
  get,
  getWithStatus,
  postWithStatus,
  post,
  postExport
} from 'helpers/httpConectionHelper'
import { headerWithToken } from 'helpers/utilsHelper'
import moment from 'moment'

export const getUserData = (token = null) => {
  return get(`auth/user`, headerWithToken(token))
}

export const getUserDataWithStatus = (token = null) => {
  return getWithStatus(`auth/user`, headerWithToken(token))
}

export const login = (body) => {
  return postWithStatus(`auth/login`, body)
}

export const logOut = () => {
  return getWithStatus(`auth/logout`, headerWithToken())
}

export const getUsers = (page, body = {}) => {
  return post(`users?page=${page}`, body, headerWithToken())
}

export const resendVerification = (body) => {
  return postWithStatus(`resend-verification`, body)
}

export const registerStripeBankData = () => {
  return getWithStatus(`becomeToHost`, headerWithToken())
}

export const exportUsersData = () => {
  return postExport(
    `users-export`,
    {},
    headerWithToken(),
    `users_${moment(new Date()).format('DD-MM-YYYY')}`
  )
}
