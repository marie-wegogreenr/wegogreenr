import {
  EstablishmentInfo,
  About,
  Services
} from 'components/host/establishment/TabsContent'

import Terms from 'components/dashboard_host/etablissement/terms'
import Taxe from 'components/dashboard_host/etablissement/taxe'
import Conditions from 'components/dashboard_host/etablissement/conditions'
import Photos from 'components/dashboard_host/etablissement/photos'
import GreenScore from 'components/dashboard_host/etablissement/greenscore'
import { Integrations } from '@components/dashboard_host/etablissement/Integrations'

const tabs = [
  {
    id: 'info_etablissement',
    tabText: 'Info établissement',
    Body: EstablishmentInfo
  },
  {
    id: 'a_propos',
    tabText: 'À propos',
    Body: About
  },
  {
    id: 'services',
    tabText: 'Services',
    Body: Services
  },
  {
    id: 'terms',
    tabText: 'Conditions',
    Body: Terms
  },
  /* Hide temporary taxes 
  {
    id: 'taxe_de_sejour',
    tabText: 'Taxe de séjour',
    Body: Taxe
  },
  */
  {
    id: 'conditions_d_annulation',
    tabText: 'Conditions d’annulation',
    Body: Conditions
  },
  {
    id: 'photos',
    tabText: 'Photos',
    Body: Photos
  },
  // {
  //   id: 'integrations',
  //   tabText: 'Intégrations',
  //   Body: Integrations
  // },
  {
    id: 'greenscore',
    tabText: 'GreenScore',
    Body: GreenScore
  }
]

export default tabs
