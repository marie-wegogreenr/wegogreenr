import { Conditions } from 'components/Hebergements/Single/Sections/PracticalInfo/Conditions'
import { Tarif } from 'components/Hebergements/Single/Sections/PracticalInfo/Tarif'
import { Extra } from 'components/Hebergements/Single/Sections/PracticalInfo/Extra'

export const practicalInfoTabs = [
  {
    title: 'Le tarif comprend',
    id: 'le_tarif_comprend',
    content: Tarif
  },
  {
    title: 'Extras / Suppléments',
    id: 'extra',
    content: Extra
  },
  {
    title: 'Conditions de réservation',
    id: 'conditions_de_reservation',
    content: Conditions
  }
]

export default practicalInfoTabs
