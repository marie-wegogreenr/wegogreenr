export const commitments = [
  {
    text: 'Accessibilité en transports en commun'
  },
  {
    text: 'Acteurs du Tourisme Durable (ATD)'
  },
  {
    text: 'Environnement très fleuri / pollenisateur'
  },
  {
    text: 'Label Tourisme et Handicap'
  },
  {
    text: 'Installation d’économiseurs d’eau'
  },
  {
    text: "Produits d'entretien fait maison"
  },
  {
    text: 'NF Environnement'
  },
  {
    text: 'Potager'
  },
  {
    text: 'Tri Sélectif'
  }
]

export default commitments
