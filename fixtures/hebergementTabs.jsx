import Info from 'components/dashboard_host/hebergements/info'
import Lits from 'components/dashboard_host/hebergements/lits'
import CalendarHebergement from '@components/dashboard_host/hebergements/calendar'
import Equipments from 'components/dashboard_host/hebergements/equipments'
import Prix from '@components/dashboard_host/hebergements/prix'
import Photos from 'components/dashboard_host/hebergements/photos'

export const tabs = [
  {
    id: 'info',
    Component: Info,
    text: 'Infos générales'
  },
  {
    id: 'beds',
    Component: Lits,
    text: 'Lits et chambres'
  },
  {
    id: 'calendar',
    Component: CalendarHebergement,
    text: 'Calendrier et disponibilités'
  },
  {
    id: 'equipments',
    Component: Equipments,
    text: 'Équipements'
  },
  {
    id: 'price',
    Component: Prix,
    text: 'Prix'
  },
  {
    id: 'photos',
    Component: Photos,
    text: 'Photos'
  }
  /* {
    id: 'reservation',
    Component: Reservation,
    text: 'Reservation'
  } */
]
