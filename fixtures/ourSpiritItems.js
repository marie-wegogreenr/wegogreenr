import { Leaf2, Heart, User, Tree } from 'components/Icons'
import { colors } from 'styles/theme'

import hotes from 'public/images/icons/our spirit/terre-feuille-11.png'
import cadre from 'public/images/icons/our spirit/feuille-gs-11.png'
import charme from 'public/images/icons/our spirit/soleil-12.png'
import comme from 'public/images/icons/our spirit/terre-feuille-11.png'

const items = [
  {
    title: `Douceur de vivre écoresponsable`,
    description: `Prendre le temps, de partager,  de bien manger mais aussi d'arpenter les villages cachés, d'immortaliser des clichés, de se lover dans un endroit douillet et pourquoi pas de se laisser un peu porter … ​`,
    color: colors.white,
    Icon: charme
  },
  {
    title: `Le GreenScore pour passeport`,
    description: `Pour être référencés chez nous, nos hôtes passent le GreenScore notre indicateur écoresponsable. Bien plus qu’une liste de critères, il mesure l’engagement concret de nos hôtes.`,
    color: colors.white,
    Icon: cadre
  },
  {
    title: `Une empreinte écologique réduite`,
    description: `En séjournant en France avec nous, vous faites le choix de favoriser un tourisme plus local en réduisant votre impact, et en respectant l'environnement.`,
    color: colors.white,
    Icon: hotes
  }
]

export default items
