import image1 from 'public/images/main-slider/1.png'

const regionSlide = [
  {
    text: "Court-circuiter le tourisme de masse, on ne pouvait pas mieux tomber dans le courage et dans l'audace",
    image: image1
  }
]

export default regionSlide
