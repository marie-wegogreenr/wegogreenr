const radios = [
  {
    text: 'Hébergements',
    checked: true,
    value: 'accommodation'
  },
  // {
  //   text: 'Activités',
  //   checked: false,
  //   value: 'activities'
  // },
  {
    text: 'Proche de chez moi',
    checked: false,
    value: 'close_to_me'
  }
]

export default radios
