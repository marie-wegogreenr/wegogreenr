import { routes } from 'constants/index'

const navRoutes = [
  {
    text: 'Mon compte',
    route: routes.HOST_DASHBOARD,
    iconSource: '/images/icons/box_icon.png',
    iconSourceAlt: '/images/icons/box_icon_orange.png',
    type: 'host'
  },
  {
    text: 'Établissement',
    route: routes.HOST_ESTABLISHMENT,
    iconSource: '/images/icons/house_icon.png',
    iconSourceAlt: '/images/icons/house_icon_orange.png',
    type: 'host'
  },
  {
    text: 'Mes Hébergements',
    route: routes.HOST_MY_HEBERGEMENTS,
    iconSource: '/images/icons/house_icon_2.png',
    iconSourceAlt: '/images/icons/house_icon_2_orange.png',
    type: 'host'
  },
  {
    text: 'Mes Réservations',
    route: routes.HOST_MY_RESERVATIONS,
    iconSource: '/images/icons/check_icon.png',
    iconSourceAlt: '/images/icons/check_icon_orange.png',
    type: 'host'
  },
  {
    text: 'Mon compte',
    route: routes.USER_DASHBOARD,
    iconSource: '/images/icons/box_icon.png',
    iconSourceAlt: '/images/icons/box_icon_orange.png',
    type: 'user'
  },
  {
    text: 'Mes Réservations',
    route: routes.USER_MY_RESERVATIONS,
    iconSource: '/images/icons/check_icon.png',
    iconSourceAlt: '/images/icons/check_icon_orange.png',
    type: 'user'
  },
  {
    text: 'Mon compte',
    route: routes.ADMIN_DASHBOARD,
    iconSource: '/images/icons/box_icon.png',
    iconSourceAlt: '/images/icons/box_icon_orange.png',
    type: 'admin'
  },
  {
    text: 'Réservations',
    route: routes.ADMIN_MY_RESERVATIONS,
    iconSource: '/images/icons/check_icon.png',
    iconSourceAlt: '/images/icons/check_icon_orange.png',
    type: 'admin'
  },
  {
    text: 'Établissements',
    route: routes.ADMIN_ETABLISHMENT,
    iconSource: '/images/icons/house_icon.png',
    iconSourceAlt: '/images/icons/house_icon_orange.png',
    type: 'admin'
  },
  {
    text: 'Hébergements',
    route: routes.ADMIN_MY_HEBERGEMENTS,
    iconSource: '/images/icons/house_icon_2.png',
    iconSourceAlt: '/images/icons/house_icon_2_orange.png',
    type: 'admin'
  }
]

export default navRoutes
