import cosmopolitan from 'public/images/icons/home/logo-cosmopolitan.png'
import europe from 'public/images/icons/home/logo-europe1.png'
import maddyness from 'public/images/icons/home/logo-maddyness.png'
import goodGods from 'public/images/icons/home/logo-thegoodgoods.png'
import eclaire from 'public/images/icons/home/logo-eclaireurscanalplus2.png'
import lemonde from 'public/images/icons/home/logo-lemonde.png'

const reviews = [
  {
    title: 'Europe 1',
    text: "Court-circuiter le tourisme de masse, on ne pouvait pas mieux tomber dans le courage et dans l'audace",
    logo: europe
  },
  {
    title: 'Cosmopolitan',
    text: 'Les locations de rêve où partir cet été',
    logo: cosmopolitan
  },
  {
    title: 'Le Monde',
    text: 'Des hébergements au plus près de la nature',
    logo: lemonde
  },
  {
    title: 'Les éclaireurs Canal +',
    text: 'We Go GreenR vous accompagne dans le choix de vos séjours écolo, en vous aidant à sélectionner hébergement et activité écopositives',
    logo: eclaire
  },
  {
    title: 'Maddyness',
    text: 'La solution pour partir en vacances sereinement',
    logo: maddyness
  },
  {
    title: 'The Good Goods',
    text: 'Le plus ? Les hôtes sont des amoureux de leur terroir, heureux de partager leurs connaissances',
    logo: goodGods
  }
]

export default reviews
