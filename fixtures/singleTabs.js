import Activities from 'components/Hebergements/Single/Sections/Activities'
import Commitments from 'components/Hebergements/Single/Sections/Commitments'
import Description from 'components/Hebergements/Single/Sections/Description'
import Equipments from 'components/Hebergements/Single/Sections/Equipments'
import PracticalInfo from 'components/Hebergements/Single/Sections/PracticalInfo'

export const tabs = [
  {
    title: 'Description',
    id: 'description',
    Content: Description
  },
  {
    title: 'Équipement',
    id: 'equipement',
    Content: Equipments
  },
  {
    title: 'Activités proches',
    id: 'activites_proches',
    Content: Activities
  },
  {
    title: 'Engagement éco',
    id: 'engagement_eco_responsable',
    Content: Commitments
  },
  {
    title: 'Infos pratiques',
    id: 'infos_pratiques',
    Content: PracticalInfo
  }
]

export default tabs
