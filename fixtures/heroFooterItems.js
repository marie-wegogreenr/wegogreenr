const footerItems = [
  {
    icon: 'verify',
    text: 'Une sélection éco-friendly vérifiée'
  },
  {
    icon: 'payment',
    text: 'Un paiement 100% sécurisé'
  },
  {
    icon: 'anulation',
    text: 'Une annulation COVID 19 gratuite'
  },
  {
    icon: 'france',
    text: 'Une entreprise française ',
    width: '40'
  }
]

export default footerItems
