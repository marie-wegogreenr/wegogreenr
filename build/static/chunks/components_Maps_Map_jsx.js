;(self['webpackChunk_N_E'] = self['webpackChunk_N_E'] || []).push([
  ['components_Maps_Map_jsx'],
  {
    /***/ './components/Maps/Map.jsx':
      /*!*********************************!*\
  !*** ./components/Maps/Map.jsx ***!
  \*********************************/
      /***/ function (module, __webpack_exports__, __webpack_require__) {
        'use strict'
        __webpack_require__.r(__webpack_exports__)
        /* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
          /*! react/jsx-dev-runtime */ './node_modules/react/jsx-dev-runtime.js'
        )
        /* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/ __webpack_require__.n(
          react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__
        )
        /* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
          /*! react */ './node_modules/react/index.js'
        )
        /* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/ __webpack_require__.n(
          react__WEBPACK_IMPORTED_MODULE_1__
        )
        /* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
          /*! react-leaflet */ './node_modules/react-leaflet/esm/index.js'
        )
        /* harmony import */ var leaflet_dist_leaflet_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
          /*! leaflet/dist/leaflet.css */ './node_modules/leaflet/dist/leaflet.css'
        )
        /* harmony import */ var leaflet_dist_leaflet_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/ __webpack_require__.n(
          leaflet_dist_leaflet_css__WEBPACK_IMPORTED_MODULE_2__
        )
        /* harmony import */ var leaflet_defaulticon_compatibility_dist_leaflet_defaulticon_compatibility_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
          /*! leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css */ './node_modules/leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css'
        )
        /* harmony import */ var leaflet_defaulticon_compatibility_dist_leaflet_defaulticon_compatibility_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/ __webpack_require__.n(
          leaflet_defaulticon_compatibility_dist_leaflet_defaulticon_compatibility_css__WEBPACK_IMPORTED_MODULE_3__
        )
        /* harmony import */ var leaflet_defaulticon_compatibility__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
          /*! leaflet-defaulticon-compatibility */ './node_modules/leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.js'
        )
        /* harmony import */ var leaflet_defaulticon_compatibility__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/ __webpack_require__.n(
          leaflet_defaulticon_compatibility__WEBPACK_IMPORTED_MODULE_4__
        )
        /* module decorator */ module = __webpack_require__.hmd(module)

        var _jsxFileName =
          'C:\\laragon\\www\\wegogreenr\\front\\components\\Maps\\Map.jsx'

        const containerStyle = {
          width: '100%',
          height: '100%'
        }

        function Map({
          center = {
            lat: 46.2245,
            lng: 2.2123
          }
        }) {
          const { lat, lng } = center
          if (!lat || !lng) return 'Latitude or Longitude are not found.'
          return /*#__PURE__*/ (0,
          react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(
            react_leaflet__WEBPACK_IMPORTED_MODULE_5__.MapContainer,
            {
              center: [lat, lng],
              zoom: 8,
              scrollWheelZoom: false,
              style: {
                height: '100%',
                width: '100%'
              },
              children: [
                /*#__PURE__*/ (0,
                react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(
                  react_leaflet__WEBPACK_IMPORTED_MODULE_5__.TileLayer,
                  {
                    attribution:
                      '\xA9 <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
                    url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
                  },
                  void 0,
                  false,
                  {
                    fileName: _jsxFileName,
                    lineNumber: 27,
                    columnNumber: 7
                  },
                  this
                ),
                /*#__PURE__*/ (0,
                react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(
                  react_leaflet__WEBPACK_IMPORTED_MODULE_5__.Marker,
                  {
                    position: [lat, lng],
                    animate: true
                  },
                  void 0,
                  false,
                  {
                    fileName: _jsxFileName,
                    lineNumber: 31,
                    columnNumber: 7
                  },
                  this
                )
              ]
            },
            void 0,
            true,
            {
              fileName: _jsxFileName,
              lineNumber: 21,
              columnNumber: 5
            },
            this
          )
        }

        _c = Map
        /* harmony default export */ __webpack_exports__[
          'default'
        ] = _c2 = /*#__PURE__*/ react__WEBPACK_IMPORTED_MODULE_1___default().memo(
          Map
        )

        var _c, _c2

        $RefreshReg$(_c, 'Map')
        $RefreshReg$(_c2, '%default%')

        var _a, _b
        // Legacy CSS implementations will `eval` browser code in a Node.js context
        // to extract CSS. For backwards compatibility, we need to check we're in a
        // browser context before continuing.
        if (
          typeof self !== 'undefined' &&
          // AMP / No-JS mode does not inject these helpers:
          '$RefreshHelpers$' in self
        ) {
          var currentExports = module.__proto__.exports
          var prevExports =
            (_b =
              (_a = module.hot.data) === null || _a === void 0
                ? void 0
                : _a.prevExports) !== null && _b !== void 0
              ? _b
              : null
          // This cannot happen in MainTemplate because the exports mismatch between
          // templating and execution.
          self.$RefreshHelpers$.registerExportsForReactRefresh(
            currentExports,
            module.id
          )
          // A module can be accepted automatically based on its exports, e.g. when
          // it is a Refresh Boundary.
          if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
              data.prevExports = currentExports
            })
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept()
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
              // A boundary can become ineligible if its exports are incompatible
              // with the previous exports.
              //
              // For example, if you add/remove/change exports, we'll want to
              // re-execute the importing modules, and force those components to
              // re-render. Similarly, if you convert a class component to a
              // function, we want to invalidate the boundary.
              if (
                self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(
                  prevExports,
                  currentExports
                )
              ) {
                module.hot.invalidate()
              } else {
                self.$RefreshHelpers$.scheduleUpdate()
              }
            }
          } else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null
            if (isNoLongerABoundary) {
              module.hot.invalidate()
            }
          }
        }

        /***/
      }
  }
])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9NYXBzL01hcC5qc3giXSwibmFtZXMiOlsiY29udGFpbmVyU3R5bGUiLCJ3aWR0aCIsImhlaWdodCIsIk1hcCIsImNlbnRlciIsImxhdCIsImxuZyIsIlJlYWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsTUFBTUEsY0FBYyxHQUFHO0FBQ3JCQyxPQUFLLEVBQUUsTUFEYztBQUVyQkMsUUFBTSxFQUFFO0FBRmEsQ0FBdkI7O0FBS0EsU0FBU0MsR0FBVCxDQUFhO0FBQ1hDLFFBQU0sR0FBRztBQUNQQyxPQUFHLEVBQUUsT0FERTtBQUVQQyxPQUFHLEVBQUU7QUFGRTtBQURFLENBQWIsRUFLRztBQUNELFFBQU07QUFBRUQsT0FBRjtBQUFPQztBQUFQLE1BQWVGLE1BQXJCO0FBQ0EsTUFBSSxDQUFDQyxHQUFELElBQVEsQ0FBQ0MsR0FBYixFQUFrQixPQUFPLHNDQUFQO0FBQ2xCLHNCQUNFLDhEQUFDLHVEQUFEO0FBQ0UsVUFBTSxFQUFFLENBQUNELEdBQUQsRUFBTUMsR0FBTixDQURWO0FBRUUsUUFBSSxFQUFFLENBRlI7QUFHRSxtQkFBZSxFQUFFLEtBSG5CO0FBSUUsU0FBSyxFQUFFO0FBQUVKLFlBQU0sRUFBRSxNQUFWO0FBQWtCRCxXQUFLLEVBQUU7QUFBekIsS0FKVDtBQUFBLDRCQU1FLDhEQUFDLG9EQUFEO0FBQ0UsaUJBQVcsRUFBQywwRUFEZDtBQUVFLFNBQUcsRUFBQztBQUZOO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFORixlQVVFLDhEQUFDLGlEQUFEO0FBQVEsY0FBUSxFQUFFLENBQUNJLEdBQUQsRUFBTUMsR0FBTixDQUFsQjtBQUE4QixhQUFPLEVBQUU7QUFBdkM7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQVZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBY0Q7O0tBdEJRSCxHO0FBd0JULCtEQUFlLG1CQUFBSSxpREFBQSxDQUFXSixHQUFYLENBQWYiLCJmaWxlIjoic3RhdGljL2NodW5rcy9jb21wb25lbnRzX01hcHNfTWFwX2pzeC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCdcclxuaW1wb3J0IHsgTWFwQ29udGFpbmVyLCBUaWxlTGF5ZXIsIE1hcmtlciB9IGZyb20gJ3JlYWN0LWxlYWZsZXQnXHJcbmltcG9ydCAnbGVhZmxldC9kaXN0L2xlYWZsZXQuY3NzJ1xyXG5pbXBvcnQgJ2xlYWZsZXQtZGVmYXVsdGljb24tY29tcGF0aWJpbGl0eS9kaXN0L2xlYWZsZXQtZGVmYXVsdGljb24tY29tcGF0aWJpbGl0eS5jc3MnXHJcbmltcG9ydCAnbGVhZmxldC1kZWZhdWx0aWNvbi1jb21wYXRpYmlsaXR5J1xyXG5cclxuY29uc3QgY29udGFpbmVyU3R5bGUgPSB7XHJcbiAgd2lkdGg6ICcxMDAlJyxcclxuICBoZWlnaHQ6ICcxMDAlJ1xyXG59XHJcblxyXG5mdW5jdGlvbiBNYXAoe1xyXG4gIGNlbnRlciA9IHtcclxuICAgIGxhdDogNDYuMjI0NSxcclxuICAgIGxuZzogMi4yMTIzXHJcbiAgfVxyXG59KSB7XHJcbiAgY29uc3QgeyBsYXQsIGxuZyB9ID0gY2VudGVyXHJcbiAgaWYgKCFsYXQgfHwgIWxuZykgcmV0dXJuICdMYXRpdHVkZSBvciBMb25naXR1ZGUgYXJlIG5vdCBmb3VuZC4nXHJcbiAgcmV0dXJuIChcclxuICAgIDxNYXBDb250YWluZXJcclxuICAgICAgY2VudGVyPXtbbGF0LCBsbmddfVxyXG4gICAgICB6b29tPXs4fVxyXG4gICAgICBzY3JvbGxXaGVlbFpvb209e2ZhbHNlfVxyXG4gICAgICBzdHlsZT17eyBoZWlnaHQ6ICcxMDAlJywgd2lkdGg6ICcxMDAlJyB9fVxyXG4gICAgPlxyXG4gICAgICA8VGlsZUxheWVyXHJcbiAgICAgICAgYXR0cmlidXRpb249JyZjb3B5OyA8YSBocmVmPVwiaHR0cDovL29zbS5vcmcvY29weXJpZ2h0XCI+T3BlblN0cmVldE1hcDwvYT4gY29udHJpYnV0b3JzJ1xyXG4gICAgICAgIHVybD1cImh0dHBzOi8ve3N9LnRpbGUub3BlbnN0cmVldG1hcC5vcmcve3p9L3t4fS97eX0ucG5nXCJcclxuICAgICAgLz5cclxuICAgICAgPE1hcmtlciBwb3NpdGlvbj17W2xhdCwgbG5nXX0gYW5pbWF0ZT17dHJ1ZX0gLz5cclxuICAgIDwvTWFwQ29udGFpbmVyPlxyXG4gIClcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgUmVhY3QubWVtbyhNYXApXHJcbiJdLCJzb3VyY2VSb290IjoiIn0=
