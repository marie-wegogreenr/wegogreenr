exports.id = 'components_Maps_Map_jsx'
exports.ids = ['components_Maps_Map_jsx']
exports.modules = {
  /***/ './components/Maps/Map.jsx':
    /*!*********************************!*\
  !*** ./components/Maps/Map.jsx ***!
  \*********************************/
    /***/ function (
      __unused_webpack_module,
      __webpack_exports__,
      __webpack_require__
    ) {
      'use strict'
      __webpack_require__.r(__webpack_exports__)
      /* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
        /*! react/jsx-dev-runtime */ 'react/jsx-dev-runtime'
      )
      /* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/ __webpack_require__.n(
        react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__
      )
      /* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
        /*! react */ 'react'
      )
      /* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/ __webpack_require__.n(
        react__WEBPACK_IMPORTED_MODULE_1__
      )
      /* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
        /*! react-leaflet */ 'react-leaflet'
      )
      /* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/ __webpack_require__.n(
        react_leaflet__WEBPACK_IMPORTED_MODULE_2__
      )
      /* harmony import */ var leaflet_dist_leaflet_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
        /*! leaflet/dist/leaflet.css */ './node_modules/leaflet/dist/leaflet.css'
      )
      /* harmony import */ var leaflet_dist_leaflet_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/ __webpack_require__.n(
        leaflet_dist_leaflet_css__WEBPACK_IMPORTED_MODULE_3__
      )
      /* harmony import */ var leaflet_defaulticon_compatibility_dist_leaflet_defaulticon_compatibility_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
        /*! leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css */ './node_modules/leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css'
      )
      /* harmony import */ var leaflet_defaulticon_compatibility_dist_leaflet_defaulticon_compatibility_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/ __webpack_require__.n(
        leaflet_defaulticon_compatibility_dist_leaflet_defaulticon_compatibility_css__WEBPACK_IMPORTED_MODULE_4__
      )
      /* harmony import */ var leaflet_defaulticon_compatibility__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
        /*! leaflet-defaulticon-compatibility */ 'leaflet-defaulticon-compatibility'
      )
      /* harmony import */ var leaflet_defaulticon_compatibility__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/ __webpack_require__.n(
        leaflet_defaulticon_compatibility__WEBPACK_IMPORTED_MODULE_5__
      )

      var _jsxFileName =
        'C:\\laragon\\www\\wegogreenr\\front\\components\\Maps\\Map.jsx'

      const containerStyle = {
        width: '100%',
        height: '100%'
      }

      function Map({
        center = {
          lat: 46.2245,
          lng: 2.2123
        }
      }) {
        const { lat, lng } = center
        if (!lat || !lng) return 'Latitude or Longitude are not found.'
        return /*#__PURE__*/ (0,
        react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(
          react_leaflet__WEBPACK_IMPORTED_MODULE_2__.MapContainer,
          {
            center: [lat, lng],
            zoom: 8,
            scrollWheelZoom: false,
            style: {
              height: '100%',
              width: '100%'
            },
            children: [
              /*#__PURE__*/ (0,
              react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(
                react_leaflet__WEBPACK_IMPORTED_MODULE_2__.TileLayer,
                {
                  attribution:
                    '\xA9 <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
                  url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
                },
                void 0,
                false,
                {
                  fileName: _jsxFileName,
                  lineNumber: 27,
                  columnNumber: 7
                },
                this
              ),
              /*#__PURE__*/ (0,
              react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(
                react_leaflet__WEBPACK_IMPORTED_MODULE_2__.Marker,
                {
                  position: [lat, lng],
                  animate: true
                },
                void 0,
                false,
                {
                  fileName: _jsxFileName,
                  lineNumber: 31,
                  columnNumber: 7
                },
                this
              )
            ]
          },
          void 0,
          true,
          {
            fileName: _jsxFileName,
            lineNumber: 21,
            columnNumber: 5
          },
          this
        )
      }

      /* harmony default export */ __webpack_exports__[
        'default'
      ] = /*#__PURE__*/ react__WEBPACK_IMPORTED_MODULE_1___default().memo(Map)

      /***/
    },

  /***/ './node_modules/leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css':
    /*!***************************************************************************************************!*\
  !*** ./node_modules/leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.css ***!
  \***************************************************************************************************/
    /***/ function () {
      /***/
    },

  /***/ './node_modules/leaflet/dist/leaflet.css':
    /*!***********************************************!*\
  !*** ./node_modules/leaflet/dist/leaflet.css ***!
  \***********************************************/
    /***/ function () {
      /***/
    }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9mcm9udC8uL2NvbXBvbmVudHMvTWFwcy9NYXAuanN4Il0sIm5hbWVzIjpbImNvbnRhaW5lclN0eWxlIiwid2lkdGgiLCJoZWlnaHQiLCJNYXAiLCJjZW50ZXIiLCJsYXQiLCJsbmciLCJSZWFjdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsTUFBTUEsY0FBYyxHQUFHO0FBQ3JCQyxPQUFLLEVBQUUsTUFEYztBQUVyQkMsUUFBTSxFQUFFO0FBRmEsQ0FBdkI7O0FBS0EsU0FBU0MsR0FBVCxDQUFhO0FBQ1hDLFFBQU0sR0FBRztBQUNQQyxPQUFHLEVBQUUsT0FERTtBQUVQQyxPQUFHLEVBQUU7QUFGRTtBQURFLENBQWIsRUFLRztBQUNELFFBQU07QUFBRUQsT0FBRjtBQUFPQztBQUFQLE1BQWVGLE1BQXJCO0FBQ0EsTUFBSSxDQUFDQyxHQUFELElBQVEsQ0FBQ0MsR0FBYixFQUFrQixPQUFPLHNDQUFQO0FBQ2xCLHNCQUNFLDhEQUFDLHVEQUFEO0FBQ0UsVUFBTSxFQUFFLENBQUNELEdBQUQsRUFBTUMsR0FBTixDQURWO0FBRUUsUUFBSSxFQUFFLENBRlI7QUFHRSxtQkFBZSxFQUFFLEtBSG5CO0FBSUUsU0FBSyxFQUFFO0FBQUVKLFlBQU0sRUFBRSxNQUFWO0FBQWtCRCxXQUFLLEVBQUU7QUFBekIsS0FKVDtBQUFBLDRCQU1FLDhEQUFDLG9EQUFEO0FBQ0UsaUJBQVcsRUFBQywwRUFEZDtBQUVFLFNBQUcsRUFBQztBQUZOO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFORixlQVVFLDhEQUFDLGlEQUFEO0FBQVEsY0FBUSxFQUFFLENBQUNJLEdBQUQsRUFBTUMsR0FBTixDQUFsQjtBQUE4QixhQUFPLEVBQUU7QUFBdkM7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQVZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBY0Q7O0FBRUQsNEVBQWVDLGlEQUFBLENBQVdKLEdBQVgsQ0FBZixFIiwiZmlsZSI6ImNvbXBvbmVudHNfTWFwc19NYXBfanN4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0J1xyXG5pbXBvcnQgeyBNYXBDb250YWluZXIsIFRpbGVMYXllciwgTWFya2VyIH0gZnJvbSAncmVhY3QtbGVhZmxldCdcclxuaW1wb3J0ICdsZWFmbGV0L2Rpc3QvbGVhZmxldC5jc3MnXHJcbmltcG9ydCAnbGVhZmxldC1kZWZhdWx0aWNvbi1jb21wYXRpYmlsaXR5L2Rpc3QvbGVhZmxldC1kZWZhdWx0aWNvbi1jb21wYXRpYmlsaXR5LmNzcydcclxuaW1wb3J0ICdsZWFmbGV0LWRlZmF1bHRpY29uLWNvbXBhdGliaWxpdHknXHJcblxyXG5jb25zdCBjb250YWluZXJTdHlsZSA9IHtcclxuICB3aWR0aDogJzEwMCUnLFxyXG4gIGhlaWdodDogJzEwMCUnXHJcbn1cclxuXHJcbmZ1bmN0aW9uIE1hcCh7XHJcbiAgY2VudGVyID0ge1xyXG4gICAgbGF0OiA0Ni4yMjQ1LFxyXG4gICAgbG5nOiAyLjIxMjNcclxuICB9XHJcbn0pIHtcclxuICBjb25zdCB7IGxhdCwgbG5nIH0gPSBjZW50ZXJcclxuICBpZiAoIWxhdCB8fCAhbG5nKSByZXR1cm4gJ0xhdGl0dWRlIG9yIExvbmdpdHVkZSBhcmUgbm90IGZvdW5kLidcclxuICByZXR1cm4gKFxyXG4gICAgPE1hcENvbnRhaW5lclxyXG4gICAgICBjZW50ZXI9e1tsYXQsIGxuZ119XHJcbiAgICAgIHpvb209ezh9XHJcbiAgICAgIHNjcm9sbFdoZWVsWm9vbT17ZmFsc2V9XHJcbiAgICAgIHN0eWxlPXt7IGhlaWdodDogJzEwMCUnLCB3aWR0aDogJzEwMCUnIH19XHJcbiAgICA+XHJcbiAgICAgIDxUaWxlTGF5ZXJcclxuICAgICAgICBhdHRyaWJ1dGlvbj0nJmNvcHk7IDxhIGhyZWY9XCJodHRwOi8vb3NtLm9yZy9jb3B5cmlnaHRcIj5PcGVuU3RyZWV0TWFwPC9hPiBjb250cmlidXRvcnMnXHJcbiAgICAgICAgdXJsPVwiaHR0cHM6Ly97c30udGlsZS5vcGVuc3RyZWV0bWFwLm9yZy97en0ve3h9L3t5fS5wbmdcIlxyXG4gICAgICAvPlxyXG4gICAgICA8TWFya2VyIHBvc2l0aW9uPXtbbGF0LCBsbmddfSBhbmltYXRlPXt0cnVlfSAvPlxyXG4gICAgPC9NYXBDb250YWluZXI+XHJcbiAgKVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBSZWFjdC5tZW1vKE1hcClcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==
